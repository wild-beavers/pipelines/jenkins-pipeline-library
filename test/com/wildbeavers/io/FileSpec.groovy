package com.wildbeavers.io


import org.junit.Test
import spock.lang.Specification

class FileSpec extends Specification {
  File subject

  def setup() {
    this.subject = new File()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == File
  }

  @Test
  def "it can guess mime types"() {
    when:
    def mime = this.subject.guessMimeType('tesT.json')

    then:
    mime == 'text/json'

    when:
    mime = this.subject.guessMimeType('tesT.tar.gz')

    then:
    mime == 'application/gzip'

    when:
    mime = this.subject.guessMimeType('tesT.yaml')

    then:
    mime == 'text/yaml'

    when:
    mime = this.subject.guessMimeType('test.txt')

    then:
    mime == 'text/plain'
  }
}
