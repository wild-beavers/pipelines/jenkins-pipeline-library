package com.wildbeavers.io


import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.PrintProxy
import org.junit.Test
import spock.lang.Specification

class DebuggerSpec extends Specification {
  private PrintProxy printProxy = Mock()
  private EnvironmentVariableProxy environmentVariableProxy = Mock()

  private Debugger subject

  def setup() {
    this.environmentVariableProxy.exist('DEBUG') >> true

    this.subject = new Debugger(this.printProxy, environmentVariableProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == Debugger
  }

  @Test
  def "it does not a debug information if debug env var is false"() {
    when:
    this.environmentVariableProxy.exist('DEBUG') >> false
    this.subject.printDebug('test debug')

    then:
    0 * this.printProxy.println(_)
  }

  @Test
  def "it can print information whatever the debug status"() {
    when:
    this.subject.print('test debug')

    then:
    1 * this.printProxy.println('test debug')
  }

  @Test
  def "it can print a debug information"() {
    when:
    this.subject.printDebug('test debug')

    then:
    1 * this.printProxy.println('test debug')
  }

  def "it does not print a warning message if DEBUG environment variable is set and debugVarExists returns environmentVariableProxy result"() {
    when:
    def result = this.subject.debugVarExists()

    then:
    result
  }

  def "it does not print a warning message if DEBUG environment variable is not set and debugVarExists returns environmentVariableProxy result"() {
    when:
    def result = this.subject.debugVarExists()

    then:
    this.environmentVariableProxy.exist('DEBUG') >> false

    !result
  }

  def "it runs a condition, displays nothing if condition is true"() {
    when:
    def result = this.subject.testCondition(true, 'DEBUG MESSAGE')

    then:
    result
    0 * this.printProxy.println(_)
  }

  def "it runs a condition, displays debug if condition is false"() {
    when:
    def result = this.subject.testCondition(false, 'DEBUG MESSAGE')

    then:
    !result
    1 * this.printProxy.println('DEBUG MESSAGE')
  }
}
