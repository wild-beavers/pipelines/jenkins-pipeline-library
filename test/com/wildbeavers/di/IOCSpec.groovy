package com.wildbeavers.di

import com.wildbeavers.IOCRefreshableTestClass
import org.junit.Test
import spock.lang.Specification

class IOCSpec extends Specification {
  void cleanup() {
    IOC.deregisterAll()
  }

  @Test
  def "it registers global variables"() {
    when:
    IOC.registerGlobal('test', 'value')

    then:
    'value' == IOC.get('test')
  }

  @Test
  def "it registers services that are instantiated at every call"() {
    when:
    IOC.register('ioc', {
      return new IOC()
    })

    then:
    IOC.isRegistered('ioc')
    !IOC.isRegisteredAsSingleton('ioc')
    IOC.isRegisteredAsAnything('ioc')
    IOC.get('ioc') != IOC.get('ioc')
  }

  @Test
  def "it registers singletons that are only instantiated once"() {
    when:
    IOC.registerSingleton('ioc', {
      return new IOC()
    })

    then:
    !IOC.isRegistered('ioc')
    IOC.isRegisteredAsSingleton('ioc')
    IOC.isRegisteredAsAnything('ioc')
    IOC.get('ioc') == IOC.get('ioc')
  }

  @Test
  def "it overrides singletons and any previously instantiated object will be different from new ones"() {
    when:
    IOC.registerSingleton('ioc', {
      return new IOC()
    })
    def instance = IOC.get('ioc')
    IOC.registerSingleton('ioc', {
      return new IOC()
    })

    then:
    instance != IOC.get('ioc')
    instance.getClass() == IOC
  }

  @Test
  def "it resets its instance, then all registered instances are are removed, thus new instantiations are different from old ones"() {
    given:
    IOC.registerSingleton('ioc', {
      return new IOC()
    })
    def instanceOld = IOC.get('ioc')

    when:
    IOC.reset()

    then:
    instanceOld != IOC.get('ioc')
  }

  void setup() {

  }

  @Test
  def "it refreshes a registered and instantiated singleton"() {
    when:
    IOC.registerSingleton(Object, {
      return 'I AM A SERVICE'
    })
    IOC.registerSingleton(IOCRefreshableTestClass, {
      return new IOCRefreshableTestClass(IOC.get(Object))
    })

    then:
    !IOC.get(IOCRefreshableTestClass).refreshed
    IOC.get(IOCRefreshableTestClass).instance == 'I AM A SERVICE'

    when:
    IOC.refreshSingleton(Object, {
      return 'I REPLACED ORIGINAL SERVICE'
    })

    then:
    IOC.get(IOCRefreshableTestClass).refreshed
    IOC.get(IOCRefreshableTestClass).instance == 'I REPLACED ORIGINAL SERVICE'
  }

  @Test
  def "it refreshes a registered singleton event when it was not instantiated"() {
    given:
    IOC.registerSingleton('ioc', {
      return new Throwable()
    })
    IOC.get('ioc')

    when:
    IOC.registerSingleton('refreshable', {
      return new IOCRefreshableTestClass(IOC.get('ioc'))
    })
    IOC.refreshSingleton('ioc', {
      return new String()
    })

    then:
    IOC.get('refreshable').instance.getClass() == String
  }

  @Test
  def "it clears one of its singleton instances"() {
    when:
    IOC.registerSingleton('reset2', {
      return new Throwable()
    })
    IOC.get('reset2')
    IOC.resetSingleton('reset2')

    then:
    !IOC.isInstantiated('reset2')
  }

  @Test
  def "it registers a factory that behave like a singleton"() {
    when:
    IOC.registerFactory(CanRegister)

    then:
    IOC.getFactory(CanRegister) == IOC.get(CanRegister.getName() + 'Factory')
    IOC.getFactory(CanRegister).getClass() == GenericFactory
    !IOC.isRegistered(CanRegister.getName() + 'Factory')
    IOC.isRegisteredAsSingleton(CanRegister.getName() + 'Factory')
    IOC.isRegisteredAsAnything(CanRegister.getName() + 'Factory')
  }

  @Test
  def "it returns its debug information"() {
    when:
    IOC.registerSingleton('this_is_a_class', {
      return new Throwable()
    })
    IOC.register('this_is_another_class', {
      return new Throwable()
    })
    IOC.registerFactory(CanRegister)

    then:
    IOC.debug() =~ /this_is_a_class/
    IOC.debug() =~ /this_is_another_class/
    IOC.debug() =~ /CanRegister/
  }

  @Test
  def "it passes unlimited variables when getting an instance"() {
    when:
    IOC.registerSingleton('ioc1', { one ->
      return new IOC()
    })
    IOC.registerSingleton('ioc2', { one, two ->
      return new IOC()
    })
    IOC.registerSingleton('ioc3', { one, two, three ->
      return new IOC()
    })
    IOC.registerSingleton('ioc4', { one, two, three, four ->
      return new IOC()
    })
    IOC.register('iocr1', { one ->
      return new IOC()
    })
    IOC.register('iocr2', { one, two ->
      return new IOC()
    })
    IOC.register('iocr3', { one, two, three ->
      return new IOC()
    })
    IOC.register('iocr4', { one, two, three, four ->
      return new IOC()
    })

    then:
    IOC.get('ioc1', 1)
    IOC.get('ioc2', 1, 2)
    IOC.get('ioc3', 1, 2, 3)
    IOC.get('ioc4', 1, 2, 3, 4)

    IOC.get('iocr1', 1)
    IOC.get('iocr2', 1, 2)
    IOC.get('iocr3', 1, 2, 3)
    IOC.get('iocr4', 1, 2, 3, 4)
  }

  @Test
  def "it registers product-line closures to be registered in factory just-in-time"() {
    given:
    IOC.registerFactory(String)
    IOC.bindProductLine(String, 'the_string', { return 'I AM THE STRING' })
    IOC.bindProductLine(String, 'the_string_the_return', { return 'STRING2' })
    IOC.bindProductLine(String, 'with_param', { it -> return it })

    when:
    def factory = IOC.getFactory(String)

    then:
    factory.instantiate('the_string') == 'I AM THE STRING'
    factory.instantiate('the_string_the_return') == 'STRING2'
    factory.instantiate('with_param', 'this param works!') == 'this param works!'
  }

  @Test
  def "it throws an exception when trying to get unregistered service or global"() {
    when:
    IOC.get('notExistingClass')

    then:
    thrown(IOCException)
  }

  @Test
  def "it throws an exception when trying to register product-line to a non-existing reference"() {
    when:
    IOC.bindProductLine(String, 'the_string', { return 'I AM THE STRING' })

    then:
    def e = thrown(IOCException)
    e.getMessage() =~ /No factory was registered with the name “java.lang.String”/
  }

  @Test
  def "it throws an exception when trying to register product-line to an already instantiated factory"() {
    given:
    IOC.registerFactory(String)

    when:
    IOC.getFactory(String)

    IOC.bindProductLine(String, 'the_string', { return 'I AM THE STRING' })

    then:
    def e = thrown(IOCException)
    e.getMessage() =~ /Attempted to bind a factory closure named “the_string” to the “java.lang.String” factory, but this factory is already instantiated/
  }
}
