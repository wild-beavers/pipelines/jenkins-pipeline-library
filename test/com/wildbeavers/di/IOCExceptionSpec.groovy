package com.wildbeavers.di

import org.junit.Test
import spock.lang.Specification

class IOCExceptionSpec extends Specification {
  IOCException subject

  def setup() {
    this.subject = new IOCException('test')
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == IOCException
  }
}
