package com.wildbeavers.di

import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import org.junit.Test
import spock.lang.Specification

class GenericFactorySpec extends Specification {
  ContainerPodmanEventData containerPodmanEventData = new ContainerPodmanEventData()
  ContainerDockerEventData containerDockerEventData = new ContainerDockerEventData()

  GenericFactory subject

  def setup() {
    this.subject = new GenericFactory<ContainerEventData>(ContainerEventData)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == GenericFactory
  }

  @Test
  def "it can register closures"() {
    expect:
    this.subject.register('test', { return true })
  }

  @Test
  def "it can register closures with arguments"() {
    given:
    GenericFactory<Boolean> factory = new GenericFactory<Boolean>(Boolean)

    when:
    factory.register('test', { Boolean arg -> return arg })

    then:
    factory.instantiate('test', true) == true
    factory.instantiate('test', false) == false
  }

  @Test
  def "it returns if it supports a name or not"() {
    when:
    this.subject.register('test', { return true })

    then:
    this.subject.support('test')
    !this.subject.support('nope')
  }

  @Test
  def "it throws an exception when trying to instantiate a Closure that does not return a ContainerEventData"() {
    when:
    this.subject.register('test', { return true })
    this.subject.instantiate('test')

    then:
    def e = thrown(IOCException)
    e =~ /“test” name was registered in the GenericFactory “class com.wildbeavers.container.event_data.ContainerEventData” factory but does not return a valid object./
  }

  @Test
  def "it throws an exception when trying to instatiate a non registered tool"() {
    when:
    this.subject.register('test', { return true })
    this.subject.instantiate('nope')

    then:
    def e = thrown(RuntimeException)
    e =~ /A GenericFactory instance of “class com.wildbeavers.container.event_data.ContainerEventData” does not support “nope”. Supported product lines: “test”/
  }

  @Test
  def "it instantiate a registered ContainerEventData"() {
    when:
    this.subject.register('podman', { return this.containerPodmanEventData })
    this.subject.register('docker', { return this.containerDockerEventData })

    then:
    this.subject.instantiate('podman') == this.containerPodmanEventData
    this.subject.instantiate('docker') == this.containerDockerEventData
  }

  @Test
  def "it returns all its supported names"() {
    when:
    this.subject.register('podman', { return this.containerPodmanEventData })
    this.subject.register('podman2', { return this.containerPodmanEventData })
    this.subject.register('test', { return 'invalid' })

    then:
    ['podman', 'podman2', 'test'] == this.subject.getAllSupportedNames()
  }
}
