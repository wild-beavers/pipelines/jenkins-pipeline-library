package com.wildbeavers

import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable

class IOCRefreshableTestClass implements IOCRefreshable {
  public Boolean refreshed = false
  public String instance

  IOCRefreshableTestClass(String instance) {
    this.instance = instance
  }

  @Override
  void refreshDependencies() {
    this.refreshed = true
    this.instance = IOC.get(Object)
  }
}
