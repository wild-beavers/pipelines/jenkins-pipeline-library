package com.wildbeavers.wrapper.terraform

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.terraform.listener.TerraformInitListener
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformInitWrapper
import com.wildbeavers.observer.AbstractEventListener
import org.junit.Test
import spock.lang.Specification

class AWSTerraformInitWrapperSpec extends Specification {
  GroovyScriptMock context = Mock()
  AbstractEventListener randomListener = Mock()
  TerraformInitListener initListener = Mock()
  AWSTerraformInitWrapper subject

  def setup() {
    this.subject = new AWSTerraformInitWrapper(this.context, 'creds')
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == AWSTerraformInitWrapper
  }

  @Test
  def "it supports Terraform init listener"() {
    when:
    def result = this.subject.support(this.initListener)

    then:
    result
  }

  @Test
  def "it does not support other listener"() {
    when:
    def result = this.subject.support(this.randomListener)

    then:
    !result
  }
}
