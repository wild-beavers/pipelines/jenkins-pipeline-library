package com.wildbeavers.container.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class ContainerCommandSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new ContainerCommand('docker test').getClass() == ContainerCommand
  }

  @Test
  def "it have a description"() {
    expect:
    new ContainerCommand('podman test').getDescription() == 'a string matching /^(docker|podman|dockerx) [a-z _-]+$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new ContainerCommand(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^(docker|podman|dockerx) [a-z _-]+$/.'

    where:
    value                           || expectedException
    'podman_not_valid'              || ValidationException
    'podman build && rogue command' || ValidationException
    'podman'                        || ValidationException
    'docker || exit 0'              || ValidationException
    'rogue command # docker build'  || ValidationException
    'ls'                            || ValidationException
    'make'                          || ValidationException
    './configure'                   || ValidationException
    'terraform apply'               || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new ContainerCommand(value)

    then:
    noExceptionThrown()

    where:
    value                          || expectedException
    'docker build'                 || false
    'docker tag'                   || false
    'docker alias'                 || false
    'docker manifest push'         || false
    'docker dockerx manifest push' || false
    'docker dockerx build'         || false
    'podman build'                 || false
    'podman push'                  || false
    'podman tag'                   || false
    'podman some-alias'            || false
  }
}
