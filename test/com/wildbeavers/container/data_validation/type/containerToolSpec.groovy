package com.wildbeavers.container.data_validation.type


import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class containerToolSpec extends Specification {
  ContainerTool subject

  def setup() {
    this.subject = new ContainerTool('podman')
  }

  @Test
  def "it can be instantiated"() {
    expect:
    new ContainerTool('docker').getClass() == ContainerTool
  }

  @Test
  def "it have a description"() {
    expect:
    this.subject.getDescription() == 'one of these possible values: docker, podman.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new ContainerTool('wrong')

    then:
    def e = thrown(ValidationException)
    e =~ /“wrong” is not one of these possible values: docker, podman./
  }
}
