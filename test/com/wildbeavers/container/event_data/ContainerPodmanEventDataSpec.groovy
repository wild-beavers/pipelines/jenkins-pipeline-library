package com.wildbeavers.container.event_data

import com.wildbeavers.container.data_validation.type.ContainerCommand
import org.junit.Test
import spock.lang.Specification

class ContainerPodmanEventDataSpec extends Specification {
  private ContainerPodmanEventData subject

  def setup() {
    this.subject = new ContainerPodmanEventData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerPodmanEventData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getTool() == ContainerPodmanEventData.DEFAULT_TOOL
    this.subject.getAuthfile() == ContainerPodmanEventData.DEFAULT_AUTHFILE
    this.subject.getMakefilePath() == ContainerPodmanEventData.DEFAULT_MAKEFILE_PATH
    this.subject.getManifestFormat() == ContainerPodmanEventData.DEFAULT_MANIFEST_FORMAT
    this.subject.getBuildCommand() == ContainerPodmanEventData.DEFAULT_BUILD_COMMAND
    this.subject.getBuildPlatforms() == ContainerPodmanEventData.DEFAULT_BUILD_PLATFORMS
    !this.subject.getBuildCompress()
    this.subject.getBuildTarget() == ContainerPodmanEventData.DEFAULT_BUILD_TARGET
    !this.subject.getBuildQuiet()
    this.subject.getTagCommand() == ContainerPodmanEventData.DEFAULT_TAG_COMMAND
    this.subject.getInspectEnabled()
    this.subject.getInspectCommand() == ContainerPodmanEventData.DEFAULT_INSPECT_COMMAND
    this.subject.getPushCommand() == ContainerPodmanEventData.DEFAULT_PUSH_COMMAND
    this.subject.getPushDestinations() == []
    this.subject.getPushCertDir() == ''
    this.subject.getPushDigestfile() == ''
    this.subject.getPushEncryptionKey() == ''
    this.subject.getPushEncryptLayer() == ''
    this.subject.getPushFormat() == ''
    !this.subject.getPushQuiet()
    this.subject.getPushSignBy() == ''
    this.subject.getPushSignBySigstore() == ''
    this.subject.getPushSignBySigstorePrivateKey() == ''
    this.subject.getPushSignPassphraseFile() == ''
    this.subject.getAlias() == ''
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setAuthfile('authfile')
    this.subject.setMakefilePath('./somewhere/Dockerfile')
    this.subject.setManifestName('testmanifest')
    this.subject.setManifestFormat('docker')
    this.subject.setBuildPlatforms(['linux/arm64'])
    this.subject.setBuildCompress(true)
    this.subject.setBuildCacheFrom('another_image')
    this.subject.setBuildCommand(new ContainerCommand('docker buildx build'))
    this.subject.setBuildArgs(['--verbose': ''])
    this.subject.setBuildArgFile('somewhere/args')
    this.subject.setBuildNetwork('host')
    this.subject.setBuildPull('newer')
    this.subject.setBuildTag('my_image')
    this.subject.setBuildQuiet(true)
    this.subject.setBuildTarget('to/docker/path')
    this.subject.setTagCommand(new ContainerCommand('docker tagalias'))
    this.subject.setInspectEnabled(false)
    this.subject.setInspectCommand(new ContainerCommand('podman manifest inspect'))
    this.subject.setPushCommand(new ContainerCommand('podman alias'))
    this.subject.setPushDestinations(['docker.io/test', 'public.ecr.aws/test'])
    this.subject.setPushCertDir('/to/certidir')
    this.subject.setPushDigestfile('digestfile')
    this.subject.setPushEncryptionKey('/to/encryption_key')
    this.subject.setPushEncryptLayer('layers')
    this.subject.setPushFormat('v2s2')
    this.subject.setPushQuiet(true)
    this.subject.setPushSignBy('me')
    this.subject.setPushSignBySigstore('sigstore')
    this.subject.setPushSignBySigstorePrivateKey('to/private_key')
    this.subject.setPushSignPassphraseFile('/to/passphrase')

    then:
    this.subject.getAuthfile() == 'authfile'
    this.subject.getMakefilePath() == './somewhere/Dockerfile'
    this.subject.getManifestName() == 'testmanifest'
    this.subject.getManifestFormat() == 'docker'
    this.subject.getBuildPlatforms() == ['linux/arm64']
    !this.subject.getBuildCompress()
    this.subject.getBuildCacheFrom() == 'another_image'
    this.subject.getBuildCommand() == 'docker buildx build'
    this.subject.getBuildArgs() == ['--verbose': '']
    this.subject.getBuildArgFile() == 'somewhere/args'
    this.subject.getBuildNetwork() == 'host'
    this.subject.getBuildPull() == 'newer'
    this.subject.getBuildTag() == 'my_image'
    this.subject.getBuildQuiet()
    this.subject.getBuildTarget() == 'to/docker/path'
    this.subject.getTagCommand() == 'docker tagalias'
    !this.subject.getInspectEnabled()
    this.subject.getInspectCommand() == 'podman manifest inspect'
    this.subject.getPushCommand() == 'podman alias'
    this.subject.getPushDestinations() == ['docker.io/test', 'public.ecr.aws/test']
    this.subject.getPushCertDir() == '/to/certidir'
    this.subject.getPushDigestfile() == 'digestfile'
    this.subject.getPushEncryptionKey() == '/to/encryption_key'
    this.subject.getPushEncryptLayer() == 'layers'
    this.subject.getPushFormat() == 'v2s2'
    this.subject.getPushQuiet()
    this.subject.getPushSignBy() == 'me'
    this.subject.getPushSignBySigstore() == 'sigstore'
    this.subject.getPushSignBySigstorePrivateKey() == 'to/private_key'
    this.subject.getPushSignPassphraseFile() == '/to/passphrase'
    this.subject.getAlias() == 'testmanifest'
  }
}
