package com.wildbeavers.container.event_data

import com.wildbeavers.container.data_validation.type.ContainerCommand
import org.junit.Test
import spock.lang.Specification

class ContainerDockerEventDataSpec extends Specification {
  private ContainerDockerEventData subject

  def setup() {
    this.subject = new ContainerDockerEventData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerDockerEventData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getTool() == ContainerDockerEventData.DEFAULT_TOOL
    this.subject.getAuthfile() == ''
    this.subject.getMakefilePath() == ContainerDockerEventData.DEFAULT_MAKEFILE_PATH
    this.subject.getManifestName() == ''
    this.subject.getManifestFormat() == ''
    this.subject.getBuildCommand() == ContainerDockerEventData.DEFAULT_BUILD_COMMAND
    this.subject.getBuildPlatforms() == ContainerDockerEventData.DEFAULT_BUILD_PLATFORMS
    this.subject.getBuildCompress() == ContainerDockerEventData.DEFAULT_BUILD_COMPRESS
    this.subject.getBuildTarget() == ContainerDockerEventData.DEFAULT_BUILD_TARGET
    this.subject.getBuildArgFile() == ''
    !this.subject.getBuildQuiet()
    this.subject.getTagCommand() == ContainerDockerEventData.DEFAULT_TAG_COMMAND
    this.subject.getInspectEnabled()
    this.subject.getInspectCommand() == ContainerDockerEventData.DEFAULT_INSPECT_COMMAND
    this.subject.getPushCommand() == ContainerDockerEventData.DEFAULT_PUSH_COMMAND
    this.subject.getPushDestinations() == []
    this.subject.getPushCertDir() == ''
    this.subject.getPushDigestfile() == ''
    this.subject.getPushEncryptionKey() == ''
    this.subject.getPushEncryptLayer() == ''
    this.subject.getPushFormat() == ''
    !this.subject.getPushQuiet()
    this.subject.getPushSignBy() == ''
    this.subject.getPushSignBySigstore() == ''
    this.subject.getPushSignBySigstorePrivateKey() == ''
    this.subject.getPushSignPassphraseFile() == ''
    this.subject.getAlias() == ''
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setAuthfile('authfile')
    this.subject.setMakefilePath('./somewhere/Dockerfile')
    this.subject.setManifestName('testmanifest')
    this.subject.setManifestFormat('docker')
    this.subject.setBuildPlatforms(['linux/arm64'])
    this.subject.setBuildCompress(false)
    this.subject.setBuildCacheFrom('another_image')
    this.subject.setBuildCommand(new ContainerCommand('docker buildx build'))
    this.subject.setBuildArgs(['--verbose': ''])
    this.subject.setBuildArgFile('somewhere/args')
    this.subject.setBuildNetwork('host')
    this.subject.setBuildPull('newer')
    this.subject.setBuildTag('my_image')
    this.subject.setBuildQuiet(true)
    this.subject.setBuildTarget('to/docker/path')
    this.subject.setTagCommand(new ContainerCommand('docker tag_alias'))
    this.subject.setInspectEnabled(false)
    this.subject.setInspectCommand(new ContainerCommand('docker ls'))
    this.subject.setPushCommand(new ContainerCommand('docker push_alias'))
    this.subject.setPushDestinations(['docker.io/test', 'public.ecr.aws/test'])
    this.subject.setPushCertDir('/to/certidir')
    this.subject.setPushDigestfile('digestfile')
    this.subject.setPushEncryptionKey('/to/encryption_key')
    this.subject.setPushEncryptLayer('layers')
    this.subject.setPushFormat('v2s2')
    this.subject.setPushQuiet(true)
    this.subject.setPushSignBy('me')
    this.subject.setPushSignBySigstore('sigstore')
    this.subject.setPushSignPassphraseFile('/to/passphrase')

    then:
    this.subject.getAuthfile() == ''
    this.subject.getMakefilePath() == './somewhere/Dockerfile'
    this.subject.getManifestName() == ''
    this.subject.getManifestFormat() == ''
    this.subject.getBuildPlatforms() == ['linux/arm64']
    !this.subject.getBuildCompress()
    this.subject.getBuildCacheFrom() == 'another_image'
    this.subject.getBuildCommand() == 'docker buildx build'
    this.subject.getBuildArgs() == ['--verbose': '']
    this.subject.getBuildArgFile() == ''
    this.subject.getBuildNetwork() == 'host'
    this.subject.getBuildPull() == 'newer'
    this.subject.getBuildTag() == 'my_image'
    this.subject.getBuildQuiet()
    this.subject.getBuildTarget() == 'to/docker/path'
    this.subject.getTagCommand() == 'docker tag_alias'
    !this.subject.getInspectEnabled()
    this.subject.getInspectCommand() == 'docker ls'
    this.subject.getPushCommand() == 'docker push_alias'
    this.subject.getPushDestinations() == ['docker.io/test', 'public.ecr.aws/test']
    this.subject.getPushCertDir() == ''
    this.subject.getPushDigestfile() == ''
    this.subject.getPushEncryptionKey() == ''
    this.subject.getPushEncryptLayer() == ''
    this.subject.getPushFormat() == ''
    this.subject.getPushQuiet()
    this.subject.getPushSignBy() == ''
    this.subject.getPushSignBySigstore() == ''
    this.subject.getPushSignBySigstorePrivateKey() == ''
    this.subject.getPushSignPassphraseFile() == ''
    this.subject.getAlias() == 'my_image'
  }
}
