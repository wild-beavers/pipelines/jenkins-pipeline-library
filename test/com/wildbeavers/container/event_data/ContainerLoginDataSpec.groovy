package com.wildbeavers.container.event_data


import com.wildbeavers.container.data.ContainerRegistryOption
import org.junit.Test
import spock.lang.Specification

class ContainerLoginDataSpec extends Specification {
  private ContainerRegistryOption containerRegistryOption1 = Spy()
  private ContainerRegistryOption containerRegistryOption2 = Spy()
  private ContainerLoginData subject

  def setup() {
    this.subject = new ContainerLoginData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerLoginData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getTool() == ContainerLoginData.DEFAULT_TOOL
    this.subject.getEnabled()
    this.subject.isEnabled()
    this.subject.getAuthfile() == ''
    this.subject.getContainerRegistryOptions() == []
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setTool('podman')
    this.subject.setEnabled(false)
    this.subject.setAuthfile('/path/to/auth.json')
    this.subject.setContainerRegistryOptions([this.containerRegistryOption1])

    then:
    this.subject.getTool() == 'podman'
    !this.subject.getEnabled()
    !this.subject.isEnabled()
    this.subject.getAuthfile() == '/path/to/auth.json'
    this.subject.getContainerRegistryOptions() == [this.containerRegistryOption1]
  }

  @Test
  def "it adds a registry option"() {
    when:
    this.subject.addContainerRegistryOption(this.containerRegistryOption1)
    this.subject.addContainerRegistryOption(this.containerRegistryOption2)

    then:
    this.subject.getContainerRegistryOptions() == [this.containerRegistryOption1, this.containerRegistryOption2]
  }
}
