package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.ParallelProxy
import org.junit.Test
import spock.lang.Specification

class ContainerPushListenerSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()
  private ScmInfo scmInfo = Mock()
  private ParallelProxy parallelProxy = Mock()

  ContainerPodmanEventData containerEventData = Spy()
  RunnerEventData runnerEventData = Spy()

  ContainerPushListener subject

  def setup() {
    this.containerEventData.getPushDestinations() >> ['destination1', 'destination2']
    this.containerEventData.getPushDigestfile() >> 'digestfile'
    this.containerEventData.getAuthfile() >> 'authfile'
    this.containerEventData.getPushQuiet() >> true
    this.containerEventData.getPushFormat() >> 'oci'
    this.scmInfo.isPublishableAsAnything() >> true
    this.runnerEventData.getMainEventData() >> this.containerEventData
    this.subject = new ContainerPushListener(this.executeProxy, this.scmInfo, this.parallelProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerPushListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [RunnerEvents.DEPLOY] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it does not run if the revision is not publishable"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.scmInfo.isPublishableAsAnything() >> false

    !result
  }

  @Test
  def "it does not run if the configuration does not contain any push destinations"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.containerEventData.getPushDestinations() >> null
    !result

    when:
    result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.containerEventData.getPushDestinations() >> []
    !result
  }

  @Test
  def "it runs when revision is publishable and destinations are set"() {
    expect:
    this.subject.shouldRun(this.runnerEventData)
  }

  @Test
  def "it runs in parallel with a set of options"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.parallelProxy.parallel(_)
    result == this.runnerEventData
  }

  @Test
  def "it runs in parallel with a set of options, using manifest instead of tag"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerEventData.getBuildPlatforms() >> ['amd64', 'arm64']
    this.containerEventData.getManifestName() >> 'manifest'

    1 * this.parallelProxy.parallel(_)
    result == this.runnerEventData
  }
}
