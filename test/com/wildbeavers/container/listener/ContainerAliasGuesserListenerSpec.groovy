package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ContainerAliasGuesserListenerSpec extends Specification {
  private ScmInfo scmInfo = Mock()

  ContainerPodmanEventData containerEventData = Spy()
  RunnerEventData runnerEventData = Spy()

  ContainerAliasGuesserListener subject

  def setup() {
    this.containerEventData.getAlias() >> ''
    this.containerEventData.getBuildPlatforms() >> null
    this.scmInfo.getRevisionCommitId() >> 'some_commit'
    this.runnerEventData.getMainEventData() >> this.containerEventData
    this.subject = new ContainerAliasGuesserListener(this.scmInfo)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerAliasGuesserListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [RunnerEvents.PRE_COMPILE] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it does not run if the user has set a manifest name or a build tag"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.containerEventData.getAlias() >> 'something'

    !result
  }

  @Test
  def "it does run when no manifest nor build tag were defined by the user"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)
    then:
    this.containerEventData.getAlias() >> ''

    result

    when:
    result = this.subject.shouldRun(this.runnerEventData)
    then:
    this.containerEventData.getAlias() >> null

    result
  }

  def "it sets build tag with commit ID when build platform is not set"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.containerEventData.setBuildTag('some_commit')
    0 * this.containerEventData.setManifestName(_)
    result

    when:
    result = this.subject.run(this.runnerEventData)

    then:
    this.containerEventData.getBuildPlatforms() >> []

    1 * this.containerEventData.setBuildTag('some_commit')
    0 * this.containerEventData.setManifestName(_)
    result
  }

  def "it sets manifest name with commit ID when build platform exists"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerEventData.getBuildPlatforms() >> ['something']

    1 * this.containerEventData.setManifestName('some_commit')
    0 * this.containerEventData.setBuildTag(_)
    result
  }
}
