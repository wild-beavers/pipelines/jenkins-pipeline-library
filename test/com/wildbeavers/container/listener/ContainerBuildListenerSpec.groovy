package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class ContainerBuildListenerSpec extends Specification {
  ExecuteProxy executeProxy = Mock()

  ContainerPodmanEventData containerEventData = Spy()
  RunnerEventData runnerEventData = Spy()

  ContainerBuildListener subject

  def setup() {
    this.runnerEventData.getMainEventData() >> this.containerEventData
    this.subject = new ContainerBuildListener(this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerBuildListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [RunnerEvents.COMPILE] == this.subject.listenTo()
  }

  @Test
  def "it executes a build command with default options"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.executeProxy.executeWithTail('podman build --rm --file Containerfile --format oci --platform linux/amd64 --platform linux/arm64 .')
    result == this.runnerEventData
  }

  @Test
  def "it executes a build command with manifest file"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerEventData.getManifestName() >> 'manifest'

    1 * this.executeProxy.executeWithTail('podman build --rm --file Containerfile --format oci --platform linux/amd64 --platform linux/arm64 --manifest manifest .')
    result == this.runnerEventData
  }

  @Test
  def "it executes a build command without build platform and alternative options"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerEventData.getAuthfile() >> 'authfile'
    this.containerEventData.getBuildPlatforms() >> null
    this.containerEventData.getBuildCacheFrom() >> 'https://cache.somewhere'
    this.containerEventData.getBuildQuiet() >> true

    1 * this.executeProxy.executeWithTail('podman build --rm --authfile authfile --file Containerfile --format oci --cache-from https://cache.somewhere --quiet .')
    result == this.runnerEventData
  }


  @Test
  def "it executes a build command with build arguments and alternative options"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerEventData.getAuthfile() >> '${XDG_RUNTIME_DIR}/containers/auth.json'
    this.containerEventData.getMakefilePath() >> 'Dockerfile'
    this.containerEventData.getBuildPull() >> 'newer'
    this.containerEventData.getBuildTag() >> 'my_image'
    this.containerEventData.getManifestName() >> 'manifest'
    this.containerEventData.getBuildNetwork() >> 'host'
    this.containerEventData.getBuildPlatforms() >> ['linux/arm64']
    this.containerEventData.getBuildArgs() >> [HEY: 'hey', example: 'test', this: '', unused: null]

    1 * this.executeProxy.executeWithTail('podman build --rm --authfile ${XDG_RUNTIME_DIR}/containers/auth.json --file Dockerfile --format oci --platform linux/arm64 --network host --pull newer --tag my_image --manifest manifest --build-arg HEY=\'hey\' --build-arg example=\'test\' .')
    result == this.runnerEventData
  }
}
