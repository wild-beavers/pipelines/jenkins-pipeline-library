package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class ContainerInspectListenerSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()

  ContainerPodmanEventData containerEventData = Spy()
  RunnerEventData runnerEventData = Spy()

  ContainerInspectListener subject

  def setup() {
    this.containerEventData.getInspectEnabled() >> true
    this.containerEventData.getInspectCommand() >> 'podman do_inspect'
    this.containerEventData.getAlias() >> 'myimage'
    this.runnerEventData.getMainEventData() >> this.containerEventData
    this.subject = new ContainerInspectListener(this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerInspectListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [RunnerEvents.PRE_DEPLOY] == this.subject.listenTo()
  }

  @Test
  def "it explains why it does not run"() {
    expect:
    ['Container inspection is disabled.'] == this.subject.explainNoRun(this.runnerEventData)
  }

  @Test
  def "it does not run if the inspect is disabled"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.containerEventData.getInspectEnabled() >> false

    !result
  }

  @Test
  def "it does run when inspect is enabled"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    result
  }

  @Test
  def "it inspects the built container image"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.executeProxy.executeWithTail('podman do_inspect myimage')
    result == this.runnerEventData
  }
}
