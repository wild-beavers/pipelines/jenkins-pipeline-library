package com.wildbeavers.container.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ContainerDataValidationListenerSpec extends Specification {
  private GenericFactory<ContainerEventData> containerEventDataFactory = Mock()
  private GenericFactory<LinterData> linterDataGenericFactory = Mock()
  private DataValidatorDecorator DataValidatorDecorator = new DataValidatorDecorator()

  private ControllerEventData controllerEventData = Spy()
  private RunnerEventData runnerEventData = Spy()

  private ContainerDataValidationListener subject

  def setup() {
    this.linterDataGenericFactory.instantiate('hadolint') >> { return new LinterData(new ContainerOptions()) }
    this.containerEventDataFactory.instantiate('docker') >> { return new ContainerDockerEventData() }
    this.containerEventDataFactory.instantiate('podman') >> { return new ContainerPodmanEventData() }
    this.controllerEventData.getRunnerEventData() >> this.runnerEventData
    this.controllerEventData.getRawConfig() >> [
      container_tool         : 'docker',
      container_makefilePath : 'makefile',
      container_authfile     : 'another_authfile',
      container_buildTag     : 'custom',
      container_buildTarget  : 'subfolder',
      container_buildCompress: false,
      container_buildNetwork : 'host',
      linter_data            : [
        'some_data'
      ]
    ]
    this.subject = new ContainerDataValidationListener(this.containerEventDataFactory, this.linterDataGenericFactory, this.DataValidatorDecorator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.controllerEventData)

    then:
    result == this.controllerEventData
    ContainerDockerEventData == result.getRunnerEventData().getMainEventData().getClass()
    '' == result.getRunnerEventData().getMainEventData().getAuthfile()
    'custom' == result.getRunnerEventData().getMainEventData().getBuildTag()
    'subfolder' == result.getRunnerEventData().getMainEventData().getBuildTarget()
    'docker build' == result.getRunnerEventData().getMainEventData().getBuildCommand()
    'docker' == result.getRunnerEventData().getMainEventData().getTool()
    !result.getRunnerEventData().getMainEventData().getBuildCompress()
    'host' == result.getRunnerEventData().getMainEventData().getBuildNetwork()
    ContainerEventData.DEFAULT_BUILD_PLATFORMS == result.getRunnerEventData().getMainEventData().getBuildPlatforms()
    !result.getRunnerEventData().getMainEventData().getBuildQuiet()
  }

  @Test
  def "it checks and validate alt data"() {
    when:
    def result = this.subject.run(this.controllerEventData)

    then:
    this.controllerEventData.getRawConfig() >> [
      container_tool                        : 'podman',
      container_makefilePath                : 'makefile',
      container_manifestName                : 'manifest',
      container_manifestFormat              : 'oci',

      container_buildPlatforms              : ['linux/amd64'],
      container_buildArgFile                : 'argfile',
      container_buildCacheFrom              : 'cache',
      container_buildCommand                : 'podman alt build',
      container_buildPull                   : 'newer',
      container_buildTag                    : 'test',
      container_buildQuiet                  : true,
      container_buildArgs                   : [arg: 'true'],

      container_tagCommand                  : 'podman alias_tag',

      container_pushCommand                 : 'docker alias_push',
      container_inspectEnabled              : false,
      container_inspectCommand              : 'docker manifest inspect',
      container_pushDestinations            : ['docker.io/test'],
      container_pushCertDir                 : 'to/certdir',
      container_pushDigestfile              : 'to/digestfile',
      container_pushEncryptLayer            : 'layers',
      container_pushEncryptionKey           : 'to/encryptionkey',
      container_pushFormat                  : 'oci',
      container_pushQuiet                   : true,
      container_pushSignBy                  : 'signby',
      container_pushSignBySigstore          : 'to/sigstore',
      container_pushSignBySigstorePrivateKey: 'to/sigstore/privatekey',
      container_pushSignPassphraseFile      : 'to/passphrase',
    ]

    result == this.controllerEventData
    ContainerPodmanEventData == result.getRunnerEventData().getMainEventData().getClass()
    ContainerPodmanEventData.DEFAULT_AUTHFILE == result.getRunnerEventData().getMainEventData().getAuthfile()
    'makefile' == result.getRunnerEventData().getMainEventData().getMakefilePath()
    'manifest' == result.getRunnerEventData().getMainEventData().getManifestName()
    'oci' == result.getRunnerEventData().getMainEventData().getManifestFormat()
    ['linux/amd64'] == result.getRunnerEventData().getMainEventData().getBuildPlatforms()
    'test' == result.getRunnerEventData().getMainEventData().getBuildTag()
    'argfile' == result.getRunnerEventData().getMainEventData().getBuildArgFile()
    'cache' == result.getRunnerEventData().getMainEventData().getBuildCacheFrom()
    'podman alt build' == result.getRunnerEventData().getMainEventData().getBuildCommand()
    'newer' == result.getRunnerEventData().getMainEventData().getBuildPull()
    [arg: 'true'] == result.getRunnerEventData().getMainEventData().getBuildArgs()
    'podman' == result.getRunnerEventData().getMainEventData().getTool()
    result.getRunnerEventData().getMainEventData().getBuildQuiet()
    'podman alias_tag' == result.getRunnerEventData().getMainEventData().getTagCommand()
    !result.getRunnerEventData().getMainEventData().getInspectEnabled()
    'docker manifest inspect' == result.getRunnerEventData().getMainEventData().getInspectCommand()
    'docker alias_push' == result.getRunnerEventData().getMainEventData().getPushCommand()
    ['docker.io/test'] == result.getRunnerEventData().getMainEventData().getPushDestinations()
    'to/certdir' == result.getRunnerEventData().getMainEventData().getPushCertDir()
    'to/digestfile' == result.getRunnerEventData().getMainEventData().getPushDigestfile()
    'layers' == result.getRunnerEventData().getMainEventData().getPushEncryptLayer()
    'to/encryptionkey' == result.getRunnerEventData().getMainEventData().getPushEncryptionKey()
    'oci' == result.getRunnerEventData().getMainEventData().getPushFormat()
    result.getRunnerEventData().getMainEventData().getPushQuiet()
    'signby' == result.getRunnerEventData().getMainEventData().getPushSignBy()
    'to/sigstore' == result.getRunnerEventData().getMainEventData().getPushSignBySigstore()
    'to/sigstore/privatekey' == result.getRunnerEventData().getMainEventData().getPushSignBySigstorePrivateKey()
    'to/passphrase' == result.getRunnerEventData().getMainEventData().getPushSignPassphraseFile()
    '< makefile' == result.getRunnerEventData().getLinterData()[0].getCommandArguments()
    'docker.io/docker:latest' == result.getRunnerEventData().getLinterData()[0].getContainerOptions().getImage()
  }

  @Test
  def "it setups default linter data if no linter data is passed"() {
    when:
    def result = this.subject.run(this.controllerEventData)

    then:
    this.runnerEventData.getLinterData() >> []

    '< makefile' == result.getRunnerEventData().getLinterData()[0].getCommandArguments()
    'docker.io/docker:latest' == result.getRunnerEventData().getLinterData()[0].getContainerOptions().getImage()
  }

  @Test
  def "it does not setup linter data if linter data is passed"() {
    when:
    def result = this.subject.run(this.controllerEventData)

    then:
    this.runnerEventData.getLinterData() >> ['something']

    [] == result.getRunnerEventData().getLinterData()
  }
}
