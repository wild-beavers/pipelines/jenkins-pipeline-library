package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class ContainerTagListenerSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()
  private ScmInfo scmInfo = Mock()

  ContainerPodmanEventData containerEventData = Spy()
  RunnerEventData runnerEventData = Spy()

  ContainerTagListener subject

  def setup() {
    this.containerEventData.getBuildCommand() >> 'docker'
    this.containerEventData.getAlias() >> 'test'
    this.scmInfo.isPublishableAsAnything() >> true
    this.scmInfo.isPublishable() >> true
    this.scmInfo.getRevisionCommitId() >> 'some_commit'
    this.scmInfo.extractPreReleaseVersion() >> '1-dev1'
    this.scmInfo.getAllRevisionSemverAndFloatingTags() >> ['1', '1.0', '1.0.1', 'latest']
    this.runnerEventData.getMainEventData() >> this.containerEventData
    this.subject = new ContainerTagListener(this.scmInfo, this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerTagListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [RunnerEvents.PRE_DEPLOY] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it explains why it does not run"() {
    expect:
    ['Container was not tagged, because it’s not publishable.'] == this.subject.explainNoRun(this.runnerEventData)
  }

  @Test
  def "it does not run if the revision is not publishable"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.scmInfo.isPublishableAsAnything() >> false

    !result
  }

  @Test
  def "it runs when revision is publishable"() {
    expect:
    this.subject.shouldRun(this.runnerEventData)
  }

  @Test
  def "it tags with all the tags when revision is publishable"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.executeProxy.executeWithTail('podman tag test 1')
    1 * this.executeProxy.executeWithTail('podman tag test 1.0')
    1 * this.executeProxy.executeWithTail('podman tag test 1.0.1')
    1 * this.executeProxy.executeWithTail('podman tag test latest')
    1 * this.executeProxy.executeWithTail('podman tag test some_commit')
    0 * this.executeProxy.executeWithTail('podman tag test 1-dev1')

    result == this.runnerEventData
  }

  @Test
  def "it only tags with pre-release tag if code is publishable as dev version"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.scmInfo.isPublishable() >> false

    0 * this.executeProxy.executeWithTail('podman tag test 1')
    0 * this.executeProxy.executeWithTail('podman tag test 1.0')
    0 * this.executeProxy.executeWithTail('podman tag test 1.0.1')
    0 * this.executeProxy.executeWithTail('podman tag test latest')
    1 * this.executeProxy.executeWithTail('podman tag test some_commit')
    1 * this.executeProxy.executeWithTail('podman tag test 1-dev1')

    result == this.runnerEventData
  }
}
