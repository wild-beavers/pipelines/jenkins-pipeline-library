package com.wildbeavers

import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.generic.IsValidatorType

class DataValidatorDecorator extends DataValidator {
  Object validate(rawData, Class requiredType) {
    if (!requiredType.isInstance(rawData) && requiredType in IsValidatorType) {
      rawData = requiredType.newInstance(rawData)
    }

    return rawData
  }

  def validate(Map<String, ?> rawMap, String key, Class requiredType, Object defaultValue = null, List<String> mapScopes = []) {
    if (!rawMap.containsKey(key)) {
      if (null == defaultValue) {
        return null
      }

      return this.validate(defaultValue, requiredType)
    }

    return this.validate(rawMap[key], requiredType)
  }
}
