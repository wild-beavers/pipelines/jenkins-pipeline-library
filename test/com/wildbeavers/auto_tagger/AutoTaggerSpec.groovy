package com.wildbeavers.auto_tagger


import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.data_manager.ScmInfoManager
import com.wildbeavers.di.IOC
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.FileProxy
import org.junit.Test
import spock.lang.Specification

import java.util.regex.Pattern

class AutoTaggerSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()
  private Debugger debugger = Mock()
  private FileProxy fileProxy = Mock()
  private ScmInfo scmInfo = Mock()
  private ScmInfoManager scmInfoManager = Mock()

  private CharSequence versionFile = GroovyMock()
  private Pattern pattern = GroovyMock()
  private CommandResult changelogMessageCommandResult = Mock()
  private CommandResult findLastKnownTagCommandResult = Mock()
  private AutoTagger subject

  def setup() {
    this.debugger.testCondition(_, _) >> { Boolean condition, CharSequence message ->
      return condition
    }
    this.versionFile.find(this.pattern) >> '1.1.0'
    this.fileProxy.read('VERSION_FILE') >> this.versionFile
    this.fileProxy.exists({ it != 'VERSION_FILE' }) >> false
    this.fileProxy.exists('VERSION_FILE') >> true
    this.scmInfo.isRevisionLatest() >> true
    this.scmInfo.isCurrentBranchDefault() >> true
    this.scmInfo.isRevisionProperlyTagged(_) >> false
    this.scmInfo.isRemoteRevisionProperlyTagged(_) >> false
    this.scmInfo.isRevisionSemverTagged() >> false
    this.scmInfo.getRemoteLatestSemverTag() >> '1.0.0'
    this.scmInfo.getAllRemoteTags() >> ['1.0.0', '1.0', '1', 'latest']
    this.scmInfo.getAllLocalTags() >> ['1.0.0', '1.0', '1', 'latest']
    this.scmInfo.getAllRevisionTags() >> []
    this.scmInfo.getAllRemoteRevisionTags() >> []
    this.scmInfo.getRemoteName() >> 'origin'
    this.findLastKnownTagCommandResult.getStdout() >> '1'
    this.changelogMessageCommandResult.getStdout() >> "- feat: new commit\n- refactor: some changes"
    this.executeProxy.executeAndAllowError({ it =~ /^cat 'VERSION_FILE' \| grep -n -E/ }) >> this.findLastKnownTagCommandResult
    this.executeProxy.execute({ it =~ /^cat 'VERSION_FILE' \| sed -e/ }) >> this.changelogMessageCommandResult

    this.subject = new AutoTagger(this.executeProxy, this.debugger, this.fileProxy, this.scmInfo, this.scmInfoManager)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == AutoTagger
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it does not auto-tag if changelog file does not exists"() {
    expect:
    !this.subject.canAutoTag('DOES_NOT_EXISTS', true)
  }

  @Test
  def "it does not auto-tag if revision is not latest"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.isRevisionLatest() >> false

    !result
  }

  @Test
  def "it does not auto-tag if revision is not on the default branch"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.isCurrentBranchDefault() >> false

    !result
  }

  @Test
  def "it does not auto-tag if revision is already properly tagged"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.isRevisionProperlyTagged(true) >> true
    this.scmInfo.isRemoteRevisionProperlyTagged(true) >> true

    !result

    when:
    result = this.subject.canAutoTag('VERSION_FILE', false)

    then:
    this.scmInfo.isRevisionProperlyTagged(false) >> true
    this.scmInfo.isRemoteRevisionProperlyTagged(false) >> true

    !result
  }

  @Test
  def "it does not auto-tag if revision semver tag is an old tag in the changelog"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.isRevisionSemverTagged() >> true
    this.findLastKnownTagCommandResult.getStdout() >> '10'

    !result
  }

  @Test
  def "it does not auto-tag if remote revision does not have a semver tag"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.getRemoteLatestSemverTag() >> ''

    !result
  }

  @Test
  def "it does not auto-tag if local tags are not the same as remote tags"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.getAllLocalTags() >> ['1.0.0']

    !result

    when:
    result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.getAllRemoteTags() >> ['1.0.0']

    !result
  }

  @Test
  def "it does not auto-tag if local revision tags are not the same as remote revision tags"() {
    when:
    def result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.getAllRevisionTags() >> ['1.0.0']

    !result

    when:
    result = this.subject.canAutoTag('VERSION_FILE', true)

    then:
    this.scmInfo.getAllRemoteRevisionTags() >> ['1.0.0']

    !result
  }

  @Test
  def "it does auto-tag if all conditions are met"() {
    expect:
    this.subject.canAutoTag('VERSION_FILE', true)
  }

  @Test
  def "it does auto-tag adding all needed tags and removing floating tags"() {
    when:
    this.subject.autoTag('VERSION_FILE', this.pattern, true)

    then:
    1 * this.executeProxy.execute('git tag -d 1 latest')
    1 * this.executeProxy.execute("git tag -a '1' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a '1.1.0' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a '1.1' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a 'latest' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute('git push --delete origin 1 latest && git push --tags')
    0 * this.executeProxy.execute('git push --tags')
    1 * this.scmInfoManager.refreshScmInfoLocalTags(this.scmInfo)
    1 * this.scmInfoManager.refreshScmInfoRemoteTags(this.scmInfo)
  }

  @Test
  def "it does not do anything if there are no tag to add"() {
    when:
    this.subject.autoTag('VERSION_FILE', this.pattern, true)

    then:
    this.scmInfo.getAllRevisionTags() >> ['1.1.0', '1.1', '1', 'latest']

    0 * this.executeProxy.execute('git tag -d 1 latest')
    0 * this.executeProxy.execute("git tag -a '1' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute("git tag -a '1.1.0' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute("git tag -a '1.1' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute("git tag -a 'latest' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute('git push --delete origin 1 latest && git push --tags')
    0 * this.executeProxy.execute('git push --tags')
    0 * this.scmInfoManager.refreshScmInfoLocalTags(this.scmInfo)
    0 * this.scmInfoManager.refreshScmInfoRemoteTags(this.scmInfo)
  }

  @Test
  def "it does add tag but do not delete any if there are none to delete"() {
    when:
    this.subject.autoTag('VERSION_FILE', this.pattern, true)

    then:
    this.scmInfo.getAllLocalTags() >> ['1.0.0']

    0 * this.executeProxy.execute('git tag -d 1 latest')
    1 * this.executeProxy.execute("git tag -a '1' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a '1.1.0' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a '1.1' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a 'latest' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute('git push --delete origin 1 latest && git push --tags')
    1 * this.executeProxy.execute('git push --tags')
    1 * this.scmInfoManager.refreshScmInfoLocalTags(this.scmInfo)
    1 * this.scmInfoManager.refreshScmInfoRemoteTags(this.scmInfo)
  }

  @Test
  def "it partially add required tag while not deleting existing ones"() {
    when:
    this.subject.autoTag('VERSION_FILE', this.pattern, true)

    then:
    this.scmInfo.getAllRevisionTags() >> ['1.1.0', 'latest']

    1 * this.executeProxy.execute('git tag -d 1')
    1 * this.executeProxy.execute("git tag -a '1' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute("git tag -a '1.1.0' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute("git tag -a '1.1' -m '- feat: new commit\n- refactor: some changes'")
    0 * this.executeProxy.execute("git tag -a 'latest' -m '- feat: new commit\n- refactor: some changes'")
    1 * this.executeProxy.execute('git push --delete origin 1 && git push --tags')
    0 * this.executeProxy.execute('git push --tags')
    1 * this.scmInfoManager.refreshScmInfoLocalTags(this.scmInfo)
    1 * this.scmInfoManager.refreshScmInfoRemoteTags(this.scmInfo)
  }
}
