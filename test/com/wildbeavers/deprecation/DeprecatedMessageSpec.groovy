package com.wildbeavers.deprecation

import org.junit.Test
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class DeprecatedMessageSpec extends Specification {
  DeprecatedMessage subject
  String oldFunction = 'old'
  String newFunction = 'new'
  LocalDateTime now = LocalDate.now() << LocalTime.now()
  LocalDateTime futureDate = now + 3600 * 24
  LocalDateTime passedDate = now - 3600 * 24

  def setup() {
    this.subject = new DeprecatedMessage()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DeprecatedMessage
  }

  @Test
  def "it can displays a warning related to a deprecated function"() {
    when:
    def buffer = new ByteArrayOutputStream()
    System.out = new PrintStream(buffer)

    and:
    this.subject.displayWarningDeprecatedFunction(this.oldFunction, this.newFunction, LocalDate.parse(this.futureDate.format("dd-MM-yyyy"), "dd-MM-yyyy"))

    then:
    buffer.toString() =~ /DEPRECATION WARNING/
    buffer.toString() =~ /old.*is now deprecated/
    buffer.toString() =~ /replaced by.*new/
  }

  @Test
  def "it can display and throw an error related to a deprecated function"() {
    when:
    def buffer = new ByteArrayOutputStream()
    System.out = new PrintStream(buffer)

    and:
    this.subject.throwErrorDeletedFunction(this.oldFunction, this.newFunction, LocalDate.parse(this.futureDate.format("dd-MM-yyyy"), "dd-MM-yyyy"))

    then:
    buffer.toString() =~ /DELETION ERROR/
    buffer.toString() =~ /old.*is deleted/
    buffer.toString() =~ /replaced by.*new/
    def e = thrown(Exception)
    e.message =~ /“old” is deleted/
    e.message =~ /replaced by “new”/
  }
}
