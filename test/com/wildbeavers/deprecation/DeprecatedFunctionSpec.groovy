package com.wildbeavers.deprecation

import org.junit.Test
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class DeprecatedFunctionSpec extends Specification {
  DeprecatedMessage deprecatedMessage = Mock()
  DeprecatedFunction subject
  Closure legacyCloser = {
    return 'legacy'
  }
  String oldFunction = 'old'
  String newFunction = 'new'
  LocalDateTime now = LocalDate.now() << LocalTime.now()
  LocalDateTime futureDate = now + 3600 * 24
  LocalDateTime passedDate = now - 3600 * 24

  def setup() {
    this.subject = new DeprecatedFunction(this.deprecatedMessage)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DeprecatedFunction
  }

  @Test
  def "it displays a warning when deprecated date is not passed"() {
    when:
    def result = this.subject.execute(this.legacyCloser, this.oldFunction, this.newFunction, futureDate.format("dd-MM-yyyy"))

    then:
    1 * this.deprecatedMessage.displayWarningDeprecatedFunction(this.oldFunction, this.newFunction, LocalDate.parse(this.futureDate.format("dd-MM-yyyy"), "dd-MM-yyyy"))
    0 * this.deprecatedMessage.throwErrorDeletedFunction(*_)
    result == 'legacy'
  }

  @Test
  def "it throws deprecation error when deprecated date is passed"() {
    when:
    this.subject.execute(this.legacyCloser, this.oldFunction, this.newFunction, passedDate.format("dd-MM-yyyy"))

    then:
    0 * this.deprecatedMessage.displayWarningDeprecatedFunction(*_)
    1 * this.deprecatedMessage.throwErrorDeletedFunction(this.oldFunction, this.newFunction, LocalDate.parse(this.passedDate.format("dd-MM-yyyy"), "dd-MM-yyyy"))
  }
}
