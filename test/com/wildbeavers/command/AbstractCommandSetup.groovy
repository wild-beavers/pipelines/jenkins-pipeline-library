package com.wildbeavers.command

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.data.OptionString
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger
import spock.lang.Specification

class AbstractCommandSetup extends Specification {
  OptionString optionString = Mock()
  OptionStringFactory optionStringFactory = Mock()
  ContainerRunner containerRunner = Mock()
  Debugger debugger = Mock()
  HasContainerOptions dockerOptionData = Mock()
  ContainerOptions containerOptions = Mock()

  def setup() {
    this.debugger.debugVarExists() >> true
    this.optionString.toString() >> '-param 1'
    this.containerOptions.getImage() >> 'image'
    this.containerOptions.getVolumes() >> [:]
    this.containerOptions.getEnvironmentVariables() >> ['env': 'env']
    this.containerOptions.getNetwork() >> 'network'
    this.containerOptions.getRunHasDaemon() >> false
    this.containerOptions.getName() >> 'name'
    this.containerOptions.getEntrypoint() >> null
    this.containerOptions.getRemoveAfterRun() >> null
    this.dockerOptionData.getContainerOptions() >> this.containerOptions
    this.optionStringFactory.getOptionString() >> this.optionString
  }
}
