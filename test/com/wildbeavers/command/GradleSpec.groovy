package com.wildbeavers.command

import com.wildbeavers.exception.CommandBuildingException
import org.junit.Test

class GradleSpec extends AbstractCommandSetup {
  Gradle subject

  def setup() {
    this.subject = new Gradle(this.optionStringFactory, this.containerRunner, this.debugger)
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == Gradle
  }

  @Test
  def "throws an exception when passed subcommand is not valid"() {
    when:
    this.subject.run('dummy', ['-arg': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Subcommand “dummy” is not valid/
  }

  @Test
  def "throws an exception when passed sub-argument is not valid"() {
    when:
    this.subject.run('check', ['-invalid': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-invalid” is not valid/
  }

  @Test
  def "throws an exception when passed sub-argument does not have valid value"() {
    when:
    this.subject.run('check', ['--console': 'invalid'], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “--console” does not have a valid value \(given: “invalid”\)/
  }

  @Test
  def "it throws an exception if the command run fails for any reason"() {
    given:
    this.containerRunner.run(*_) >> {
      throw new Exception()
    }

    when:
    this.subject.run('check', ['-i': true], this.dockerOptionData)

    then:
    thrown(Exception)
  }

  @Test
  def "runs a command"() {
    when:
    this.subject.run('check', ['-i': true, '--console': 'rich', '-Dorg.gradle.debug': true], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-Dorg.gradle.debug', true, Boolean)
    1 * this.optionStringFactory.addOption('--console', 'rich', CharSequence)
    1 * this.optionStringFactory.addOption('-i', '')
    1 * this.containerRunner.run(this.containerOptions, '-param 1 check -param 1')
  }

  @Test
  def "it runs with prefixing the main command"() {
    when:
    this.subject.runWithPrefix('check', ['-i': true, '--console': 'rich', '-Dorg.gradle.debug': true], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-Dorg.gradle.debug', true, Boolean)
    1 * this.optionStringFactory.addOption('--console', 'rich', CharSequence)
    1 * this.optionStringFactory.addOption('-i', '')
    1 * this.containerRunner.run(this.containerOptions, 'gradle -param 1 check -param 1')
  }

  def "it can show its documentation"() {
    when:
    def result = this.subject.getDocumentation()

    then:
    result =~ /COMMAND: gradle/
    result =~ /-help/
    result =~ /description: Shows a help message with all available CLI options/
    result =~ /-i/
  }

  def "it can show a subcommand documentation"() {
    when:
    def result = this.subject.getDocumentation('check')

    then:
    result =~ /COMMAND: gradle check/
  }
}
