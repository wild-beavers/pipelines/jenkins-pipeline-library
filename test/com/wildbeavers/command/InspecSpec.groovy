package com.wildbeavers.command

import com.wildbeavers.exception.CommandBuildingException
import org.junit.Test

import static com.wildbeavers.command.AbstractCommand.COMMAND_TARGET

class InspecSpec extends AbstractCommandSetup {
  Inspec subject

  def setup() {
    this.subject = new Inspec(this.optionStringFactory, this.containerRunner, this.debugger)
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == Inspec
  }

  @Test
  def "throws an exception when passed subcommand is not valid"() {
    when:
    this.subject.run('dummy', ['-arg': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Subcommand “dummy” is not valid/
  }

  @Test
  def "throws an exception when passed sub-argument is not valid"() {
    when:
    this.subject.run('exec', ['-invalid': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-invalid” is not valid/
  }

  @Test
  def "it throws an exception if the command run fails for any reason"() {
    given:
    this.containerRunner.run(*_) >> {
      throw new Exception()
    }

    when:
    this.subject.run('exec', ['--insecure': true], this.dockerOptionData)

    then:
    thrown(Exception)
  }

  @Test
  def "throws an exception when passed sub-argument does not have valid value"() {
    when:
    this.subject.run('exec', ['-b': 'invalid'], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-b” does not have a valid value \(given: “invalid”\)/
  }

  @Test
  def "runs a command"() {
    when:
    this.subject.run('exec', ['-b': 'ssh', '--insecure': true, (COMMAND_TARGET): 'target'], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-b', 'ssh', CharSequence)
    1 * this.optionStringFactory.addOption('--insecure', '')
    1 * this.optionStringFactory.addCommandTarget('target')
    1 * this.containerRunner.run(this.containerOptions,  '-param 1 exec -param 1')
  }

  @Test
  def "it runs with prefixing the main command"() {
    when:
    this.subject.runWithPrefix('exec', ['-b': 'ssh', '--insecure': true, (COMMAND_TARGET): 'target'], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-b', 'ssh', CharSequence)
    1 * this.optionStringFactory.addOption('--insecure', '')
    1 * this.optionStringFactory.addCommandTarget('target')
    1 * this.containerRunner.run(this.containerOptions, 'inspec -param 1 exec -param 1')
  }

  def "it can show its documentation"() {
    when:
    def result = this.subject.getDocumentation()

    then:
    result =~ /COMMAND: inspec/
  }

  def "it can show a subcommand documentation"() {
    when:
    def result = this.subject.getDocumentation('exec')

    then:
    result =~ /COMMAND: inspec exec/
    result =~ /--backend/
    result =~ /description: Choose a backend/
    result =~ /--password/
  }
}
