package com.wildbeavers.event_data

import com.wildbeavers.data_validation.type.PositiveOrZeroInt
import com.wildbeavers.data_validation.type.TimeUnit
import com.wildbeavers.decorator.WildMap
import org.junit.Test
import spock.lang.Specification

class ControllerEventDataSpec extends Specification {
  private RunnerEventData runnerEventData = Mock()
  private WildMap rawConfig = Mock()

  private ControllerEventData subject

  def setup() {
    this.subject = new ControllerEventData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ControllerEventData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getJobNodeLabel() == ''
    this.subject.getJobProperties() == []
    this.subject.getJobTimeoutTime() == 10
    this.subject.getJobTimeoutUnit() == 'HOURS'

    !this.subject.getDebuggingEnabled()
    !this.subject.getDebuggingRuntimeToggleEnabled()
    this.subject.getDebuggingRuntimeToggleTimeout() == 10

    this.subject.getRunnerEventData() == null

    this.subject.getRawConfig() == null
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setJobNodeLabel('node')
    this.subject.setJobProperties(['some', 'properties'])
    this.subject.setJobTimeoutTime(new PositiveOrZeroInt(300))
    this.subject.setJobTimeoutUnit(new TimeUnit('MINUTES'))

    this.subject.setDebuggingEnabled(true)
    this.subject.setDebuggingRuntimeToggleEnabled(true)
    this.subject.setDebuggingRuntimeToggleTimeout(new PositiveOrZeroInt(30))

    this.subject.setRunnerEventData(this.runnerEventData)

    this.subject.setRawConfig(this.rawConfig)

    then:
    this.subject.getJobNodeLabel() == 'node'
    this.subject.getJobProperties() == ['some', 'properties']
    this.subject.getJobTimeoutTime() == 300
    this.subject.getJobTimeoutUnit() == 'MINUTES'

    this.subject.getDebuggingEnabled()
    this.subject.getDebuggingRuntimeToggleEnabled()
    this.subject.getDebuggingRuntimeToggleTimeout() == 30

    this.subject.getRunnerEventData() == this.runnerEventData

    this.subject.getRawConfig() == this.rawConfig
  }
}
