package com.wildbeavers.event_data


import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.HasScmData
import com.wildbeavers.data.ScmAutoTagMethod
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.data_validation.type.GitCloneURL
import com.wildbeavers.data_validation.type.Version
import com.wildbeavers.helper.SemverHelper
import org.junit.Test
import spock.lang.Specification

class RunnerEventDataSpec extends Specification {
  private ContainerOptions dockerOptions = Mock()

  private LinterData linterData
  private FileStandardData fileStandardData
  private FileStandardData fileStandardDataAlt
  private ArtifactFetchingData artifactFetchingData
  private ArtifactPublisherData artifactPublisherData

  private RunnerEventData subject

  def setup() {
    this.subject = new RunnerEventData(this.fileStandardData)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == RunnerEventData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getScmSshConfigDirectory() == null

    this.subject.getScmAlternativeCheckoutCredentialID() == ''
    this.subject.getScmAlternativeCheckoutDirectory() == null
    this.subject.getScmAlternativeCheckoutRepositoryURL() == null
    this.subject.getScmAlternativeCheckoutTag() == null
    this.subject.getScmFloatingTagsEnabled()
    this.subject.getScmAutoTagEnabled()
    this.subject.getScmAutoTagMethod() == ScmAutoTagMethod.TAG_ANNOTATED
    this.subject.getScmAutoTagChangelogFilePath() == HasScmData.DEFAULT_CHANGELOG_FILE_PATH
    this.subject.getScmAutoTagLastTagRegex().toString() == (~"(?!^#+\\s+)(${SemverHelper.SEMVER_REGEXP_NOT_DELIMITED})").toString()

    this.subject.getTagCheckEnabled() == true
    this.subject.getTagCheckOnlyMainBranch() == true
    this.subject.getTagCheckRegex().toString() == (~(/(^latest$)|/ + SemverHelper.SEMVER_REGEXP_NO_PRERELEASE)).toString()

    this.subject.getLinterEnabled() == false
    this.subject.isLinterEnabled() == false
    this.subject.getLinterData() == []
    this.subject.getArtifactFetchingData() == null

    this.subject.getFileStandardData() == this.fileStandardData
    this.subject.isFileStandardEnabled()
    !this.subject.isFileStandardIgnoreDefault()

    !this.subject.getCleanupDelayEnabled()
    this.subject.getCleanupDelayButtonLabel() == 'Proceed'
    this.subject.getCleanupDelayTime() == 10
    this.subject.getCleanupDelayContinueIfExpired()
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setScmSshConfigDirectory('my-ssh-path')

    this.subject.setScmAlternativeCheckoutCredentialID('cred-if-checkout')
    this.subject.setScmAlternativeCheckoutDirectory(new ForwardRelativeLinuxFullPath('some/directory'))
    this.subject.setScmAlternativeCheckoutRepositoryURL(new GitCloneURL('git@gitlab.com:wild-beavers/pipelines/jenkins-pipeline-library.git'))
    this.subject.setScmAlternativeCheckoutTag(new Version('v1.0.0'))
    this.subject.setScmFloatingTagsEnabled(false)
    this.subject.setScmAutoTagEnabled(false)
    this.subject.setScmAutoTagMethod(ScmAutoTagMethod.TAG)
    this.subject.setScmAutoTagChangelogFilePath(new ForwardRelativeLinuxFullPath('change/changelog.rst'))
    this.subject.setScmAutoTagLastTagRegex(~/=> \d/)

    this.subject.setArtifactFetchingData(this.artifactFetchingData)
    this.subject.setArtifactPublisherData(this.artifactPublisherData)

    this.subject.setGitSSHCommand('-o ok')

    this.subject.setCleanupDelayEnabled(true)
    this.subject.setCleanupDelayButtonLabel('Go Ahead!')
    this.subject.setCleanupDelayHeader('Waiting before cleanup')
    this.subject.setCleanupDelayMessage('Whenever you are ready, press the button')
    this.subject.setCleanupDelayTime(300)
    this.subject.setCleanupDelayContinueIfExpired(false)

    this.subject.setLinterEnabled(true)
    this.subject.setLinterData([this.linterData])

    this.subject.setFileStandardEnabled(false)
    this.subject.setFileStandardIgnoreDefault(true)
    this.subject.setFileStandardData(this.fileStandardDataAlt)

    this.subject.setTagCheckEnabled(false)
    this.subject.setTagCheckOnlyMainBranch(false)
    this.subject.setTagCheckRegex(~/^[0-9]+$/)

    then:
    this.subject.getScmSshConfigDirectory() == 'my-ssh-path'

    this.subject.getScmAlternativeCheckoutCredentialID() == 'cred-if-checkout'
    this.subject.getScmAlternativeCheckoutDirectory() == 'some/directory'
    this.subject.getScmAlternativeCheckoutRepositoryURL() == 'git@gitlab.com:wild-beavers/pipelines/jenkins-pipeline-library.git'
    this.subject.getScmAlternativeCheckoutTag() == 'v1.0.0'
    !this.subject.getScmFloatingTagsEnabled()
    !this.subject.getScmAutoTagEnabled()
    this.subject.getScmAutoTagMethod() == ScmAutoTagMethod.TAG
    this.subject.getScmAutoTagChangelogFilePath() == 'change/changelog.rst'
    this.subject.getScmAutoTagLastTagRegex().toString() == /=> \d/

    this.artifactPublisherData == this.subject.getArtifactPublisherData()
    this.artifactFetchingData == this.subject.getArtifactFetchingData()

    '-o ok' == this.subject.getGitSSHCommand()

    this.subject.getLinterEnabled()
    this.subject.isLinterEnabled()
    [this.linterData] == this.subject.getLinterData()

    !this.subject.isFileStandardEnabled()
    this.subject.isFileStandardIgnoreDefault()
    this.fileStandardDataAlt == this.subject.getFileStandardData()

    this.subject.getCleanupDelayEnabled()
    this.subject.getCleanupDelayButtonLabel() == 'Go Ahead!'
    this.subject.getCleanupDelayHeader() == 'Waiting before cleanup'
    this.subject.getCleanupDelayMessage() == 'Whenever you are ready, press the button'
    this.subject.getCleanupDelayTime() == 300
    !this.subject.getCleanupDelayContinueIfExpired()

    !this.subject.getTagCheckEnabled()
    !this.subject.getTagCheckOnlyMainBranch()
    this.subject.getTagCheckRegex().toString() == /^[0-9]+$/
  }
}
