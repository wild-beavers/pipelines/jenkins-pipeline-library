package com.wildbeavers.archiver


import com.wildbeavers.exception.ConfigurationException
import org.junit.Test
import spock.lang.Specification

class ArchiverCollectionSpec extends Specification {
  ArchiverCollection subject
  IsArchiver archiver = Mock()
  IsArchiver archiver2 = Mock()
  IsArchiver archiver3 = Mock()

  def setup() {
    this.archiver.getSupportedAlgorithm() >> 'rar'
    this.archiver2.getSupportedAlgorithm() >> 'zip'
    this.archiver3.getSupportedAlgorithm() >> 'tar.gz'

    this.subject = new ArchiverCollection()
    this.subject.addArchiver(this.archiver)
    this.subject.addArchiver(this.archiver2)
    this.subject.addArchiver(this.archiver3)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArchiverCollection
  }

  @Test
  def "it can store and retrieve all its archivers, with no duplicates"() {
    when:
    this.subject.addArchiver(this.archiver2)

    then:
    this.subject.getAllArchivers().contains(this.archiver)
    this.subject.getAllArchivers().contains(this.archiver2)
    this.subject.getAllArchivers().contains(this.archiver3)
    this.subject.getAllArchivers().size() == 3
  }

  @Test
  def "it can retrieve a specific archiver that supports a specific archive"() {
    when:
    this.archiver.supportArchive('archive.rar') >> true
    this.archiver2.supportArchive('archive.zip') >> true
    def result = this.subject.getArchiverByArchiveName('archive.zip')

    then:
    result == this.archiver2
  }

  @Test
  def "it throws an exception when trying to get an archiver with an unsupported archive name"() {
    when:
    this.archiver.supportArchive(_) >> false
    this.archiver2.supportArchive(_) >> false
    this.archiver3.supportArchive(_) >> false
    this.subject.getArchiverByArchiveName('archive.zip')

    then:
    thrown(ConfigurationException)
  }
}
