package com.wildbeavers.archiver


import org.junit.Test
import spock.lang.Specification

class ArchiverSpec extends Specification {
  Archiver subject
  ArchiverCollection archiverCollection = Mock()
  IsArchiver archiver = Mock()

  def setup() {
    this.subject = new Archiver(this.archiverCollection)
    this.archiverCollection.getArchiverByArchiveName('archive.zip') >> this.archiver
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == Archiver
  }

  @Test
  def "it can compress an archive using the correct archiver implementation"() {
    when:
    this.subject.compress(['file', 'to', 'archive'], 'archive.zip')

    then:
    1 * this.archiver.compress(['file', 'to', 'archive'], 'archive.zip')
  }

  @Test
  def "it can uncompress an archive using the correct archiver implementation"() {
    when:
    this.subject.uncompress('archive.zip', 'destination')

    then:
    1 * this.archiver.uncompress('archive.zip', 'destination')
  }
}
