package com.wildbeavers.archiver

import com.wildbeavers.data.CommandResult
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class DirectoryMoveArchiverSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()

  private CommandResult commandResult = Mock()
  private CommandResult testCommandResult = Mock()

  private DirectoryMoveArchiver subject

  def setup() {
    this.commandResult.getStatusCode() >> 0
    this.executeProxy.execute(_) >> this.commandResult
    this.testCommandResult.getStatusCode() >> 0
    this.executeProxy.executeAndAllowError(_) >> this.testCommandResult

    this.subject = new DirectoryMoveArchiver(this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DirectoryMoveArchiver
  }

  @Test
  def "it can move multiple directories"() {
    when:
    this.subject.compress(['source_path', 'another'], 'destination_path')

    then:
    1 * this.executeProxy.execute('mv \'source_path\' \'destination_path\'')
    1 * this.executeProxy.execute('mv \'another\' \'destination_path\'')
  }

  @Test
  def "it can move a single directory"() {
    when:
    this.subject.compress(['source_path'], 'destination_path')

    then:
    1 * this.executeProxy.execute('mv \'source_path\' \'destination_path\'')
  }

  @Test
  def "it move directories in the opposite direction files"() {
    when:
    this.subject.uncompress('somewhere', '/path/to/destiny')

    then:
    1 * this.executeProxy.execute('mv \'somewhere\' \'/path/to/destiny\'')
  }

  @Test
  def "it checks whether or not requirements are met"() {
    expect:
    this.subject.requirementsInstalled()
  }

  @Test
  def "it says when it supports a given archive name"() {
    expect:
    this.subject.supportArchive('supported')
  }

  @Test
  def "it says when it does not supports a given archive name"() {
    when:
    def result = this.subject.supportArchive('unsupported')

    then:
    this.testCommandResult.getStatusCode() >> 1

    !result
  }

  @Test
  def "it gives the algorithm it supports"() {
    expect:
    DirectoryMoveArchiver.SUPPORTED_ALGORITHM == this.subject.getSupportedAlgorithm()
  }
}
