package com.wildbeavers.archiver

import com.wildbeavers.data.CommandResult
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class TarGzArchiverSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()

  private CommandResult commandResult = Mock()

  private TarGzArchiver subject

  def setup() {
    this.commandResult.getStatusCode() >> 0
    this.executeProxy.execute(_) >> this.commandResult
    this.executeProxy.executeAndAllowError(_) >> this.commandResult
    this.subject = new TarGzArchiver(this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TarGzArchiver
  }

  @Test
  def "it can compress files"() {
    when:
    this.subject.compress(['test', 'test 2'], 'path.tar.gz')

    then:
    1 * this.executeProxy.execute('find * -name test -o -name test 2 | tar -czf path.tar.gz -T -')
  }

  @Test
  def "it can uncompress files"() {
    when:
    this.subject.uncompress('path.tar.gz', '/path/to/destiny')

    then:
    1 * this.executeProxy.execute({
      it =~ /path\.tar\.gz/ && it =~ /\/path\/to\/destiny/
    })
  }

  @Test
  def "it checks whether or not requirements are met"() {
    expect:
    this.subject.requirementsInstalled()
  }

  @Test
  def "it says when it supports a given archive name"() {
    expect:
    this.subject.supportArchive('archive.tar.gz')
  }

  @Test
  def "it says when it does not supports a given archive name"() {
    expect:
    !this.subject.supportArchive('unsupported.7z')
  }

  @Test
  def "it gives the algorithm it supports"() {
    expect:
    TarGzArchiver.SUPPORTED_ALGORITHM == this.subject.getSupportedAlgorithm()
  }
}
