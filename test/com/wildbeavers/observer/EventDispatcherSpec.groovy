package com.wildbeavers.observer

import com.wildbeavers.di.IOC
import com.wildbeavers.io.Debugger
import com.wildbeavers.listener.wrapper.CanWrapListener
import org.junit.Test
import spock.lang.Specification

class DummyListener extends EventListener<EventDataInterface> {
  @Override
  EventDataInterface run(EventDataInterface eventData) {
    return eventData
  }

  @Override
  ArrayList<String> listenTo() {
    return ['TEST']
  }
}

class EventDispatcherSpec extends Specification {
  EventListenerBag eventListenerBag = Mock()
  Debugger debugger = Mock()

  EventDataInterface eventData = Mock()
  EventListener listener1 = Mock()
  EventListener listener2 = Mock()
  EventListener listener3 = Mock()
  CanWrapListener wrapper1 = Mock()
  CanWrapListener wrapper2 = Mock()

  EventDispatcher<EventDataInterface> subject

  def setup() {
    this.wrapper1.support(this.listener1) >> true
    this.wrapper2.support(this.listener2) >> true
    this.eventListenerBag.findOrderedListenersForEvent('TEST') >> [this.listener1, this.listener3, this.listener2]
    this.eventListenerBag.findOrderedListenersForEvent('TEST_NONE') >> []

    this.listener1.getDescription() >> 'I AM LISTENER 1'
    this.listener2.getDescription() >> 'I AM LISTENER 2'
    this.listener3.getDescription() >> 'I AM LISTENER 3'
    this.listener1.stopPropagationAfterRun() >> false
    this.listener2.stopPropagationAfterRun() >> false
    this.listener3.stopPropagationAfterRun() >> false
    this.listener1.shouldRun(_) >> true
    this.listener2.shouldRun(_) >> true
    this.listener3.shouldRun(_) >> true

    this.subject = new EventDispatcher(this.eventListenerBag, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == EventDispatcher
  }

  @Test
  def "it attaches event listeners to itself"() {
    when:
    this.subject.attach(this.listener1)
    this.subject.attach(this.listener3)

    then:
    1 * this.eventListenerBag.addEventListener(this.listener1)
    0 * this.eventListenerBag.addEventListener(this.listener2)
    1 * this.eventListenerBag.addEventListener(this.listener3)
  }

  @Test
  def "it attaches event listeners to itself via classname"() {
    IOC.registerSingleton(DummyListener.getName(), {
      return new DummyListener()
    })

    when:
    this.subject.attachClass(DummyListener)

    then:
    1 * this.eventListenerBag.addEventListener({ it.getClass().getName() == 'com.wildbeavers.observer.DummyListener' })
  }


  @Test
  def "it attaches event listeners to itself via multiple classnames"() {
    IOC.registerSingleton(DummyListener.getName(), {
      return new DummyListener()
    })

    when:
    this.subject.attachClasses([DummyListener])

    then:
    1 * this.eventListenerBag.addEventListener({ it.getClass().getName() == 'com.wildbeavers.observer.DummyListener' })
  }

  @Test
  def "it attaches event wrapper to itself"() {
    expect:
    this.subject == this.subject.attachWrapper(this.wrapper1)
  }

  @Test
  def "it attaches event wrapper to itself, without duplication"() {
    expect:
    this.subject == this.subject.attachWrapper(this.wrapper1)
    this.subject == this.subject.attachWrapper(this.wrapper2)
    this.subject == this.subject.attachWrapper(this.wrapper1)
  }

  @Test
  def "it attaches multiple event wrappers to itself at the same time"() {
    expect:
    this.subject == this.subject.attachWrappers([this.wrapper1, this.wrapper2])
  }

  @Test
  def "it dispatches events with listeners previously attached: explains no run, show description, run found listeners"() {
    when:
    def result = this.subject.dispatch('TEST', this.eventData)

    then:
    this.listener3.shouldRun(this.eventData) >> false
    this.listener2.getDescription(this.eventData) >> null

    1 * this.listener3.explainNoRun(this.eventData) >> ['NoRUN']
    1 * this.debugger.print('I AM LISTENER 1')
    2 * this.debugger.print({ it =~ /~~~> Running/})
    2 * this.debugger.print({ it =~ /~~~> End of/})
    1 * this.debugger.print('NoRUN')
    1 * this.listener1.run({ it =~ /EventDataInterface/ })
    1 * this.listener2.run({ it =~ /EventDataInterface/ })
    0 * this.debugger.print({ it =~ /xxx> Failed/})
    0 * this.listener3.run(_)
    result == this.eventData
  }

  @Test
  def "it stops running event if one of them stops propagation events"() {
    when:
    def result = this.subject.dispatch('TEST', this.eventData)

    then:
    this.listener1.stopPropagationAfterRun() >> true

    0 * this.listener1.explainNoRun(this.eventData)
    0 * this.listener2.explainNoRun(this.eventData)
    0 * this.listener3.explainNoRun(this.eventData)
    1 * this.debugger.print('I AM LISTENER 1')
    1 * this.listener1.run({ it =~ /EventDataInterface/ })
    0 * this.listener2.run(_)
    0 * this.listener3.run(_)
    result == this.eventData
  }

  @Test
  def "it passes on eventData depending on what listeners returns"() {
    EventDataInterface returnedEventData = Mock()

    when:
    def result = this.subject.dispatch('TEST', this.eventData)

    then:
    1 * this.listener1.run({ it =~ /EventDataInterface/ }) >> returnedEventData
    1 * this.listener2.run(returnedEventData)
    1 * this.listener3.run(returnedEventData)
    returnedEventData == result
  }

  @Test
  def "it runs no listeners if none of them listens to given event"() {
    when:
    def result = this.subject.dispatch('TEST_NONE', this.eventData)

    then:
    0 * this.listener1.run(_)
    0 * this.listener2.run(_)
    0 * this.listener3.run(_)
    0 * this.debugger.print(_)
    this.eventData == result
  }

  @Test
  def "it runs wrapper instead of listeners when wrappers are set to replace specific listeners"() {
    this.subject.attachWrappers([this.wrapper1, this.wrapper2])

    when:
    def result = this.subject.dispatch('TEST', this.eventData)

    then:
    0 * this.listener1.run(_)
    1 * this.wrapper1.wrap(this.listener1, this.eventData)
    0 * this.listener2.run(_)
    1 * this.wrapper2.wrap(this.listener2, this.eventData)
    1 * this.listener3.run({ it =~ /EventDataInterface/ })
    this.eventData == result
  }

  @Test
  def "it passes on eventData depending on what wrappers returns"() {
    EventDataInterface returnedEventData = Mock()
    this.subject.attachWrappers([this.wrapper1, this.wrapper2])

    when:
    def result = this.subject.dispatch('TEST', this.eventData)

    then:
    1 * this.wrapper1.wrap(this.listener1, this.eventData) >> returnedEventData
    1 * this.wrapper2.wrap(this.listener2, returnedEventData)
    1 * this.listener3.run(returnedEventData)
    returnedEventData == result
  }

  @Test
  def "it ignores exceptions when intructed and keep running subsequent listeners"() {
    when:
    this.subject.dispatch('TEST', this.eventData)

    then:
    1 * this.listener1.run(_) >> { throw new Exception("Nope") }
    0 * this.listener2.run(_)
    0 * this.listener3.run(_)
    thrown(RuntimeException)

    when:
    this.subject.dispatch('TEST', this.eventData, false)

    then:
    1 * this.listener1.run(_) >> { throw new Exception("Nope") }
    1 * this.listener2.run(_)
    1 * this.listener3.run(_)
    thrown(RuntimeException)
  }
}
