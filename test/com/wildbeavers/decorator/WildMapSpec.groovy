package com.wildbeavers.decorator

import org.junit.Test
import spock.lang.Specification

class WildMapSpec extends Specification {
  WildMap subject
  Map mapToCheck = [some: null, other: true]

  def setup() {
    this.subject = new WildMap(this.mapToCheck)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == WildMap
  }

  @Test
  def "it throws an exception if the key specified is empty"() {
    when:
    this.subject.check(
      '',
      CharSequence,
      ''
    )

    then:
    thrown(Exception)
  }

  @Test
  def "it throws an exception when checking a Map without the required key if an error message is defined"() {
    when:
    this.subject.check(
      'notExist',
      CharSequence,
      '',
      'KEY IS UNDEFINED'
    )

    then:
    def e = thrown(Exception)
    e.message == 'KEY IS UNDEFINED'
  }

  @Test
  def "it throws an exception when checking a Map with a default value not matching the expected type"() {
    when:
    this.subject.check(
      'exists',
      Boolean,
      'ouch'
    )

    then:
    def e = thrown(Exception)
    e.message =~ 'Default value not not match the expected type'
  }


  @Test
  def "it throws an exception if the key element is not of the required type"() {
    when:
    this.subject.check(
      'other',
      CharSequence,
      ''
    )

    then:
    thrown(Exception)
  }

  @Test
  def "it sets Map key to the default value if the key is null or does not exists"() {
    when:
    this.subject.check(
      'some',
      CharSequence,
      'default'
    )

    then:
    this.subject.some == 'default'

    when:
    this.subject.check(
      'notExisting',
      CharSequence,
      'default2'
    )

    then:
    this.subject.notExisting == 'default2'
  }

  @Test
  def "it returns an unchanged Map if all checks passes"() {
    when:
    this.subject.check(
      'other',
      Boolean,
      false,
      'not existing:'
    )

    then:
    this.subject.toMapString() == this.mapToCheck.toMapString()
  }

  @Test
  def "it accepts null values as default anyway"() {
    when:
    this.subject.check(
      'other',
      Boolean,
      null
    )

    then:
    this.subject.toMapString() == this.mapToCheck.toMapString()
  }
}
