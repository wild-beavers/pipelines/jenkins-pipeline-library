package com.wildbeavers.data_manager


import org.junit.Test
import spock.lang.Specification
import com.wildbeavers.container.data.ContainerOptions

class ContainerOptionsManagerSpec extends Specification {
  private ContainerOptionsManager subject

  def setup() {
    this.subject = new ContainerOptionsManager()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerOptionsManager
  }

  @Test
  def "it create a ContainerOptions"() {
    when:
    def result = this.subject.create([
      imageName           : "dockerImage",
      imageTag            : "",
      fqdn                : "",
      volumes             : [foo: "bar"],
      environmentVariables: [bar: "baz"],
      network             : "host",
      runHasDaemon        : true,
      name                : "foo",
      entrypoint          : "bash",
      removeAfterRun      : false,
      privileged          : true
    ])

    then:
    result.getClass() == ContainerOptions
    result.getImage() == "dockerImage"
    result.getVolumes() == [foo: "bar"]
    result.getEnvironmentVariables() == [bar: "baz"]
    result.getNetwork() == "host"
    result.getRunHasDaemon()
    result.getName() == "foo"
    result.getEntrypoint() == "bash"
    !result.getRemoveAfterRun()
    result.getPrivileged()
    noExceptionThrown()
  }

  @Test
  def "it throw an exception when an config key doesn't exists "() {
    when:
    this.subject.create([foo: "bar"])

    then:
    def e = thrown(Exception)
    e.getMessage() =~ /configuration key .* is not a supported as a container option/
  }

  @Test
  def "it return an empty ContainerOptions when config is empty"() {
    when:
    def result = this.subject.create([:])

    then:
    result.getClass() == ContainerOptions
    noExceptionThrown()
  }
}
