package com.wildbeavers.data_manager

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.ExecuteProxy
import hudson.plugins.git.GitSCM
import hudson.plugins.git.UserRemoteConfig
import org.junit.Test
import spock.lang.Specification

class ScmInfoManagerSpec extends Specification {
  private CommandResult commandResult = Mock()
  private GitSCM gitSCM = Mock()
  private UserRemoteConfig userRemoteConfig = GroovyMock()

  private ScmInfo sourceScmInfo = GroovyMock()

  private GroovyScriptMock context = Spy()
  private Debugger debugger = Mock()
  private ExecuteProxy executeProxy = Mock()
  private EnvironmentVariableProxy environmentVariableProxy = Mock()
  private ScmInfoManager subject

  def setup() {
    this.userRemoteConfig.getUrl() >> 'https://bitbucket.org/Client/butane-ssh2-project'
    this.userRemoteConfig.getRefspec() >> 'refs/remotes/origin/test/my_branch'
    this.gitSCM.getUserRemoteConfigs() >> [this.userRemoteConfig]
    this.commandResult.getStdout() >> 'result!'
    this.executeProxy.executeAndAllowError(_) >> this.commandResult
    this.environmentVariableProxy.get('BRANCH_IS_PRIMARY') >> false
    this.environmentVariableProxy.get('BRANCH_NAME') >> 'test/my_branch'
    this.context.scm >> this.gitSCM
    this.debugger.debugVarExists() >> true
    this.subject = new ScmInfoManager(this.context, this.debugger, this.executeProxy, this.environmentVariableProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ScmInfoManager
  }

  @Test
  def "it refreshes the SCMInfo data object"() {
    when:
    IOC.registerSingleton(ScmInfo, {
      return new String()
    })
    def stringResult = IOC.get(ScmInfo)
    this.subject.refreshScmInfo()

    then:
    stringResult.getClass() == String
    IOC.get(ScmInfo).getClass() == ScmInfo
  }

  @Test
  def "it refreshes the SCMInfo data object, but only the local tags"() {
    when:
    IOC.registerSingleton(ScmInfo, {
      return new String()
    })
    def stringResult = IOC.get(ScmInfo)
    this.subject.refreshScmInfoLocalTags(this.sourceScmInfo)

    then:
    stringResult.getClass() == String
    IOC.get(ScmInfo).getClass() == ScmInfo
    0 * this.sourceScmInfo.getAllLocalTags()
    0 * this.sourceScmInfo.getAllRevisionTags()
  }

  @Test
  def "it refreshes the SCMInfo data object, but only the remote tags"() {
    when:
    IOC.registerSingleton(ScmInfo, {
      return new String()
    })
    def stringResult = IOC.get(ScmInfo)
    this.subject.refreshScmInfoRemoteTags(this.sourceScmInfo)

    then:
    stringResult.getClass() == String
    IOC.get(ScmInfo).getClass() == ScmInfo
    0 * this.sourceScmInfo.getAllRemoteRevisionTags()
    0 * this.sourceScmInfo.getAllRemoteTags()
  }

  // Since most of this class is private shell commands, it’s hard an barely useful to unit test further.
}
