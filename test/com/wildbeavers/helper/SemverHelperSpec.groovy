package com.wildbeavers.helper

import org.junit.Test
import spock.lang.Specification

class SemverHelperSpec extends Specification {
  @Test
  def "it can extract major tag"() {
    expect:
    SemverHelper.getMajorTag(value) == expected_string
    SemverHelper.getMajorVersionNumber(value) == expected_int

    where:
    value         | expected_string | expected_int
    '1.2.3'       | '1'             | 1
    '1.2.3-2'     | '1'             | 1
    '1.2.3-rc3'   | '1'             | 1
    '99.17.253'   | '99'            | 99
    '0.3.0'       | '0'             | 0
    '1'           | null            | null
    ''            | null            | null
    'invalid'     | null            | null
    'my1.2.3_253' | null            | null
    '1.2.3.close' | null            | null
  }

  @Test
  def "it can extract minor tag"() {
    expect:
    SemverHelper.getMinorTag(value) == expected_string
    SemverHelper.getMinorVersionNumber(value) == expected_int

    where:
    value         | expected_string | expected_int
    '1.2.3'       | '1.2'           | 2
    '1.2.3-2'     | '1.2'           | 2
    '1.2.3-rc3'   | '1.2'           | 2
    '99.17.253'   | '99.17'         | 17
    '0.3.0'       | '0.3'           | 3
    '1'           | null            | null
    ''            | null            | null
    'invalid'     | null            | null
    'my1.2.3_253' | null            | null
    '1.2.3.close' | null            | null
  }

  @Test
  def "it can extract patch tag"() {
    expect:
    SemverHelper.getPatchTag(value) == expected_string
    SemverHelper.getPatchVersionNumber(value) == expected_int

    where:
    value         | expected_string | expected_int
    '1.2.3'       | '1.2.3'         | 3
    '1.2.3-2'     | '1.2.3'         | 3
    '1.2.3-rc3'   | '1.2.3'         | 3
    '99.17.253'   | '99.17.253'     | 253
    '0.3.0'       | '0.3.0'         | 0
    '1'           | null            | null
    ''            | null            | null
    'invalid'     | null            | null
    'my1.2.3_253' | null            | null
    '1.2.3.close' | null            | null
  }

  @Test
  def "it can extract pre-release part"() {
    expect:
    SemverHelper.getPreReleasePart(value) == expected_string
    SemverHelper.containsPreRelease(value) == expected_bool

    where:
    value             | expected_string | expected_bool
    '1.2.3'           | null            | false
    '1.2.3-2'         | null            | false
    '1.2.3-rc3'       | 'rc3'           | true
    '99.17.253-dev10' | 'dev10'         | true
    '0.3.0'           | null            | false
    '1'               | null            | false
    ''                | null            | false
    'invalid'         | null            | false
    'my1.2.3_253'     | null            | false
    '1.2.3.close'     | null            | false
  }

  @Test
  def "it can check if a string follows semver standards"() {
    expect:
    SemverHelper.getMetadataPart(value) == expected_string

    where:
    value          | expected_string
    '1.2.3'        | null
    '1.2.3-2'      | '2'
    '1.2.3-rc3'    | null
    '99.17.253-10' | '10'
    '0.3.0'        | null
    '1'            | null
    ''             | null
    'invalid'      | null
    'my1.2.3_253'  | null
    '1.2.3.close'  | null
  }
}
