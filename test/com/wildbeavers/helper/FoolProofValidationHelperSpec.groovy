package com.wildbeavers.helper

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.proxy.TimeoutProxy
import org.junit.Test
import spock.lang.Specification

class FoolProofValidationHelperSpec extends Specification {
  GroovyScriptMock context = Mock()
  TimeoutProxy timeoutProxy = Mock()

  FoolProofValidationHelper subject

  def setup() {
    this.subject = new FoolProofValidationHelper(this.context, this.timeoutProxy, new Random())
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == FoolProofValidationHelper
  }
}
