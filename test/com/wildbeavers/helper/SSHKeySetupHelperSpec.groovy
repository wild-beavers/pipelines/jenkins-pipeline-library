package com.wildbeavers.helper


import com.wildbeavers.data.CommandResult
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class SSHKeySetupHelperSpec extends Specification {
  CommandResult commandResult = Mock()
  CommandResult failCommandResult = Mock()

  Debugger debugger = Mock()
  EnvironmentVariableProxy environmentVariableProxy = Mock()
  ExecuteProxy executeProxy = Mock()
  SSHKeySetupHelper subject

  def setup() {
    this.failCommandResult.getStatusCode() >> 1
    this.commandResult.getStatusCode() >> 0
    this.commandResult.getStdout() >> ""
    this.executeProxy.executeAndAllowError(_) >> this.commandResult
    this.executeProxy.execute(_) >> this.commandResult
    this.debugger.debugVarExists() >> true
    this.subject = new SSHKeySetupHelper(this.debugger, this.executeProxy, this.environmentVariableProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == SSHKeySetupHelper
  }

  @Test
  def "it setup SSH private key from Jenkins secret variables"() {
    when:
    def result = this.subject.setupPrivateKeyWithEnvVars('KEY', 'PASS', ['host-key'], 'destination')

    then:
    result == 'destination'
    1 * this.environmentVariableProxy.set('SSH_ASKPASS', 'destination/getSSHKeyPassword')
    1 * this.debugger.print({ it =~ /SSH Agent is available/ })
  }

  @Test
  def "it setup SSH private key from Jenkins secret variables with SSH agent when available"() {
    when:

    def result = this.subject.setupPrivateKeyWithEnvVars('KEY', 'PASS', ['host-key'], 'destination')

    then:
    this.executeProxy.executeAndAllowError({ it =~ /command -v ssh-add/ }) >> this.failCommandResult
    result == 'destination'
    1 * this.environmentVariableProxy.set('SSH_ASKPASS', 'destination/getSSHKeyPassword')
    1 * this.debugger.print({ it =~ /SSH Agent is not available/ })
  }

  @Test
  def "it cleanups the SSH private key created from Jenkins secret variables"() {
    when:
    this.subject.cleanupPrivateKeyWithEnvVars('destination')

    then:
    this.executeProxy.execute('rm -rf "destination"')
    1 * this.environmentVariableProxy.unset('SSH_ASKPASS')
  }
}
