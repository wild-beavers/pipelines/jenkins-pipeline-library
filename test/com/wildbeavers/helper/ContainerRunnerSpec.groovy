package com.wildbeavers.helper

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.CommandResult
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class ContainerRunnerSpec extends Specification {
  private Debugger debugger = Mock()
  private ExecuteProxy executeProxy = Mock()

  private CommandResult pullCommandResult = Mock()
  private CommandResult runCommandResult = Mock()
  private ContainerOptions containerOptions = Spy()

  private ContainerRunner subject

  def setup() {
    this.containerOptions.getImage() >> 'image'
    this.containerOptions.getFallbackCommand() >> 'fallback'
    this.containerOptions.getTool() >> 'docker'
    this.containerOptions.getQuiet() >> false
    this.pullCommandResult.getStdout() >> 'the docker exists'
    this.runCommandResult.getStdout() >> 'run result'
    this.executeProxy.executeAndAllowError('docker inspect "image" 2> /dev/null') >> this.pullCommandResult
    this.executeProxy.execute({ it =~ /^docker run/ }) >> this.runCommandResult
    this.executeProxy.executeWithTail({ it =~ /^docker run/ }) >> this.runCommandResult

    this.subject = new ContainerRunner(this.debugger, this.executeProxy, 'baseline')
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerRunner
  }

  @Test
  def "it pulls a container if it does not exists on current system"() {
    when:
    this.subject.pull(this.containerOptions)

    then:
    this.pullCommandResult.getStatusCode() >> 1

    1 * this.executeProxy.execute('docker pull image')
    0 * this.debugger.printDebug(_)
  }

  @Test
  def "it pulls a container if the force pull is set to true"() {
    when:
    this.subject.pull(this.containerOptions, true)
    this.subject.run(this.containerOptions, 'test', true)

    then:
    this.pullCommandResult.getStatusCode() >> 0

    2 * this.executeProxy.execute('docker pull image')
    0 * this.debugger.printDebug(_)
  }

  @Test
  def "it does not pull a container if it exists on current system"() {
    when:
    this.subject.pull(this.containerOptions)

    then:
    this.pullCommandResult.getStatusCode() >> 0

    0 * this.executeProxy.execute('docker pull image')
    1 * this.debugger.printDebug('Did not pull image. Image already exists on current context.')
  }

  @Test
  def "it does not pull a container if no image is provided"() {
    when:
    this.subject.pull(this.containerOptions, true)

    then:
    this.pullCommandResult.getStatusCode() >> 1
    this.containerOptions.getImage() >> ''

    0 * this.executeProxy.execute({ it =~ /^docker pull/ })
    1 * this.debugger.print('Unable to pull, no image was provided')
  }

  @Test
  def "it runs a container command"() {
    when:
    def result = this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    1 * this.executeProxy.executeWithTail('docker run -w /data -v "baseline":"/data" --rm image this-command --with-args') >> this.runCommandResult
    0 * this.debugger.print(_)
    0 * this.executeProxy.execute({ it =~ /^docker run/ })
    result == this.runCommandResult
  }

  @Test
  def "it runs a container command quietly"() {
    when:
    def result = this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.containerOptions.getQuiet() >> true

    0 * this.debugger.print('docker run -w /data -v "baseline":"/data" --rm image this-command --with-args')
    1 * this.executeProxy.executeSecret('docker run -w /data -v "baseline":"/data" --rm image this-command --with-args') >> this.runCommandResult
    0 * this.executeProxy.executeWithTail({ it =~ /^docker run/ })
    result == this.runCommandResult
  }

  @Test
  def "it runs a fallback command instead of container one when container runtime is not installed"() {
    when:
    def result = this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.executeProxy.execute("which docker") >> { throw new Exception('not docker installed') }

    1 * this.debugger.printDebug('docker is not available in the current context. Assuming “fallback” is installed.')
    1 * this.executeProxy.executeWithTail('fallback this-command --with-args') >> this.runCommandResult
    0 * this.debugger.print(_)
    0 * this.executeProxy.execute({ it =~ /^docker run/ })
    result == this.runCommandResult
  }

  @Test
  def "it runs a fallback command, without duplicating the command, instead of container one when container runtime is not installed"() {
    when:
    def result = this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.executeProxy.execute("which docker") >> { throw new Exception('not docker installed') }
    this.containerOptions.getFallbackCommand() >> 'this-command'

    1 * this.debugger.printDebug('docker is not available in the current context. Assuming “this-command” is installed.')
    1 * this.executeProxy.executeWithTail('this-command --with-args') >> this.runCommandResult
    0 * this.debugger.print(_)
    0 * this.executeProxy.execute({ it =~ /^docker run/ })
    result == this.runCommandResult
  }

  @Test
  def "it throws an exception when neither container runtime is installed nor fallback command"() {
    when:
    this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.executeProxy.execute("which docker") >> { throw new Exception('not docker installed') }
    this.executeProxy.execute("which fallback") >> { throw new Exception('not docker installed') }

    def e = thrown(Exception)
    e =~ /However, “fallback” is not installed in the current context/
  }

  @Test
  def "it throws an exception when neither container runtime is installed nor fallback command"() {
    when:
    this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.executeProxy.execute("which docker") >> { throw new Exception('not docker installed') }
    this.containerOptions.getFallbackCommand() >> ''

    def e = thrown(Exception)
    e =~ /However, no fallback command was provided./
  }

  @Test
  def "it runs a fallback command instead of container one when no image is provided"() {
    when:
    def result = this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.containerOptions.getImage() >> ''

    1 * this.debugger.printDebug('No container image was provided. Assuming “fallback” is installed.')
    1 * this.executeProxy.executeWithTail('fallback this-command --with-args') >> this.runCommandResult
    0 * this.debugger.print(_)
    0 * this.executeProxy.execute({ it =~ /^docker run/ })
    result == this.runCommandResult
  }

  @Test
  def "it runs a container command with various options"() {
    when:
    def result = this.subject.run(this.containerOptions, 'this-command --with-args')

    then:
    this.containerOptions.getTool() >> 'podman'
    this.containerOptions.getLogDriver() >> 'none'
    this.containerOptions.getPull() >> 'always'
    this.containerOptions.getReadOnly() >> true
    this.containerOptions.getTransientStore() >> true
    this.containerOptions.getRemoveAfterRun() >> false
    this.containerOptions.getBasepathAsWorkdir() >> false
    this.containerOptions.getPrivileged() >> true
    this.containerOptions.getRunHasDaemon() >> true
    this.containerOptions.getEntrypoint() >> 'entrypoint'
    this.containerOptions.getName() >> 'test'
    this.containerOptions.getUserns() >> 'keep-id'
    this.containerOptions.getNetwork() >> 'host'
    this.containerOptions.getVolumes() >> [some: 'volume']
    this.containerOptions.getEnvironmentVariables() >> [env: 'test', 'PREFIX_*': null]

    1 * this.executeProxy.executeWithTail('podman run --privileged --transient-store --read-only -d --entrypoint entrypoint --name test --userns keep-id --network host --pull always --log-driver none -v "some":"volume" -e "env"="test" -e "PREFIX_*" image this-command --with-args') >> this.runCommandResult
    0 * this.executeProxy.execute({ it =~ /^podman run/ })
    0 * this.executeProxy.execute({ it =~ /^docker run/ })
    result == this.runCommandResult
  }

}
