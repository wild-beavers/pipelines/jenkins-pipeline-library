//package com.wildbeavers.helper
//
//import com.wildbeavers.credentials.CredentialLoader
//import com.wildbeavers.credentials.data.CredentialType
//import com.wildbeavers.credentials.data.SecretType
//import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
//import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
//import com.wildbeavers.credentials.exposer.CredentialExposerFetcher
//import com.wildbeavers.credentials.exposer.CredentialExposerType
//import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer
//import com.wildbeavers.credentials.exposer.impl.FileCredentialExposer
//import com.wildbeavers.credentials.provider.CredentialBackendProviderFetcher
//import com.wildbeavers.credentials.provider.CredentialProviderType
//import com.wildbeavers.credentials.provider.impl.JenkinsCredentialProvider
//import com.wildbeavers.credentials.data.Secret
//import org.junit.Test
//import spock.lang.Specification
//
//class CredentialHelperSpec extends Specification {
//  CredentialBackendProviderFetcher credentialProviderFetcher = Mock()
//  CredentialExposerFetcher credentialExposerFetcher = Mock()
//  JenkinsCredentialProvider jenkinsCredentialProvider = Mock()
//  UsernamePasswordCredential usernamePasswordCredential = Mock()
//  SshPrivateKeyCredential sshPrivateKeyCredential = Mock()
//  EnvironmentVariableCredentialExposer environmentVariableCredentialExposer = Mock()
//  Secret username = Mock()
//  Secret password = Mock()
//  Secret passphrase = Mock()
//  Secret privateKey = Mock()
//  FileCredentialExposer fileCredentialExposer = Mock()
//
//  CredentialLoader subject
//
//  def setup() {
//    this.subject = new CredentialLoader(this.credentialProviderFetcher, this.credentialExposerFetcher)
//  }
//
//  @Test
//  def "it can be instantiated"() {
//    expect:
//    this.subject.getClass() == CredentialLoader
//  }
//
//  @Test
//  def "it expose and return a UsernamePasswordCredential class when loading load username password"() {
//    when:
//    def result = this.subject.loadUsernamePassword('test-credential')
//
//    then:
//    1 * this.credentialProviderFetcher.fetch(CredentialProviderType.JENKINS) >> this.jenkinsCredentialProvider
//    1 * this.jenkinsCredentialProvider.provide('test-credential', CredentialType.USERNAME_PASSWORD) >> this.usernamePasswordCredential
//    2 * this.credentialExposerFetcher.fetch(CredentialExposerType.ENVIRONMENT_VARIABLE) >> this.environmentVariableCredentialExposer
//    1 * this.environmentVariableCredentialExposer.expose(this.username, 'test_credential_username')
//    1 * this.environmentVariableCredentialExposer.expose(this.password, 'test_credential_password')
//    1 * this.username.getSecType() >> SecretType.USERNAME
//    1 * this.password.getSecType() >> SecretType.PASSWORD
//    1 * this.usernamePasswordCredential.getUsername() >> this.username
//    1 * this.usernamePasswordCredential.getPassword() >> this.password
//    result === this.usernamePasswordCredential
//    noExceptionThrown()
//  }
//
//  @Test
//  def "it expose with custom location name and return a UsernamePasswordCredential class when loading load username password\""() {
//    when:
//    def result = this.subject.loadUsernamePassword('test-credential', 'usernameLocation', 'passwordLocation')
//
//    then:
//    1 * this.credentialProviderFetcher.fetch(CredentialProviderType.JENKINS) >> this.jenkinsCredentialProvider
//    1 * this.jenkinsCredentialProvider.provide('test-credential', CredentialType.USERNAME_PASSWORD) >> this.usernamePasswordCredential
//    2 * this.credentialExposerFetcher.fetch(CredentialExposerType.ENVIRONMENT_VARIABLE) >> this.environmentVariableCredentialExposer
//    1 * this.environmentVariableCredentialExposer.expose(this.username, 'usernameLocation')
//    1 * this.environmentVariableCredentialExposer.expose(this.password, 'passwordLocation')
//    0 * this.username.getSecType() >> SecretType.USERNAME
//    0 * this.password.getSecType() >> SecretType.PASSWORD
//    1 * this.usernamePasswordCredential.getUsername() >> this.username
//    1 * this.usernamePasswordCredential.getPassword() >> this.password
//    result === this.usernamePasswordCredential
//    noExceptionThrown()
//  }
//
//  @Test
//  def "it expose and return a SshPrivateKeyCredential class when loading load ssh private key"() {
//    when:
//    def result = this.subject.loadSshPrivateKey('test-credential')
//
//    then:
//    1 * this.credentialProviderFetcher.fetch(CredentialProviderType.JENKINS) >> this.jenkinsCredentialProvider
//    1 * this.jenkinsCredentialProvider.provide('test-credential', CredentialType.SSH_PRIVATE_KEY) >> this.sshPrivateKeyCredential
//    2 * this.credentialExposerFetcher.fetch(CredentialExposerType.ENVIRONMENT_VARIABLE) >> this.environmentVariableCredentialExposer
//    1 * this.credentialExposerFetcher.fetch(CredentialExposerType.FILE) >> this.fileCredentialExposer
//    1 * this.environmentVariableCredentialExposer.expose(this.username, 'test_credential_username')
//    1 * this.environmentVariableCredentialExposer.expose(this.passphrase, 'test_credential_passphrase')
//    1 * this.fileCredentialExposer.expose(this.privateKey, 'test_credential_private_key')
//    1 * this.username.getSecType() >> SecretType.USERNAME
//    1 * this.passphrase.getSecType() >> SecretType.PASSPHRASE
//    1 * this.privateKey.getSecType() >> SecretType.SSH_PRIVATE_KEY
//    1 * this.sshPrivateKeyCredential.getUsername() >> this.username
//    1 * this.sshPrivateKeyCredential.getPassphrase() >> this.passphrase
//    1 * this.sshPrivateKeyCredential.getPrivateKey() >> this.privateKey
//    result === this.sshPrivateKeyCredential
//    noExceptionThrown()
//  }
//
//  @Test
//  def "it expose with custom location name and return a SshPrivateKeyCredential class when loading load ssh private key"() {
//    when:
//    def result = this.subject.loadSshPrivateKey('test-credential', 'usernameLocation', 'passphraseLocation', 'privateKeyLocation')
//
//    then:
//    1 * this.credentialProviderFetcher.fetch(CredentialProviderType.JENKINS) >> this.jenkinsCredentialProvider
//    1 * this.jenkinsCredentialProvider.provide('test-credential', CredentialType.SSH_PRIVATE_KEY) >> this.sshPrivateKeyCredential
//    2 * this.credentialExposerFetcher.fetch(CredentialExposerType.ENVIRONMENT_VARIABLE) >> this.environmentVariableCredentialExposer
//    1 * this.credentialExposerFetcher.fetch(CredentialExposerType.FILE) >> this.fileCredentialExposer
//    1 * this.environmentVariableCredentialExposer.expose(this.username, 'usernameLocation')
//    1 * this.environmentVariableCredentialExposer.expose(this.passphrase, 'passphraseLocation')
//    1 * this.fileCredentialExposer.expose(this.privateKey, 'privateKeyLocation')
//    0 * this.username.getSecType() >> SecretType.USERNAME
//    0 * this.passphrase.getSecType() >> SecretType.PASSPHRASE
//    0 * this.privateKey.getSecType() >> SecretType.SSH_PRIVATE_KEY
//    1 * this.sshPrivateKeyCredential.getUsername() >> this.username
//    1 * this.sshPrivateKeyCredential.getPassphrase() >> this.passphrase
//    1 * this.sshPrivateKeyCredential.getPrivateKey() >> this.privateKey
//    result === this.sshPrivateKeyCredential
//    noExceptionThrown()
//  }
//}
