package com.wildbeavers.listener


import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.CommandResult
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.data.pre_commit.PreCommitData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.FileProxy
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

class PreCommitListenerSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()
  private FileProxy fileProxy = Mock()
  private Debugger debugger = Mock()
  private ContainerRunner containerRunner = Mock()

  CommandResult commandResult = Mock()
  ContainerOptions containerOptions = Spy()
  PreCommitData preCommitData = Spy()
  RunnerEventData event = Spy()

  PreCommitListener subject

  def setup() {
    this.containerOptions.getImage() >> 'docker'
    this.event.getPreCommitData() >> this.preCommitData
    this.preCommitData.getContainerOptions() >> this.containerOptions
    this.preCommitData.getRunCommandArguments() >> 'pre-commit run --with-stuff'
    this.preCommitData.isEnabled() >> true
    this.fileProxy.exists('.pre-commit-config.yaml') >> true
    this.fileProxy.exists('.pre-commit-config.yml') >> false
    this.commandResult.getStdout() >> 'some diffs'
    this.executeProxy.execute('git diff') >> this.commandResult

    this.subject = new PreCommitListener(this.executeProxy, this.fileProxy, this.debugger, this.containerRunner)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PreCommitListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [RunnerEvents.STATIC_ANALYSIS] == this.subject.listenTo()
  }

  @Test
  def "it is not run if pre commit configuration file does not exists"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.fileProxy.exists('.pre-commit-config.yaml') >> false

    !result
  }

  @Test
  def "it is not run if pre commit is not enabled"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.preCommitData.isEnabled() >> false

    !result
  }

  @Test
  def "it runs with .yml file instead of .yaml"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.fileProxy.exists('.pre-commit-config.yaml') >> false
    this.fileProxy.exists('.pre-commit-config.yml') >> true

    result
  }

  @Test
  def "it runs a pre-commit command"() {
    when:
    def result = this.subject.run(this.event)

    then:
    1 * this.containerRunner.run(this.containerOptions, 'pre-commit run --with-stuff')
    result == this.event
  }

  @Test
  def "it shows a git diff when an exception occurs to see what changes happened"() {
    when:
    this.subject.run(this.event)

    then:
    1 * this.containerRunner.run(this.containerOptions, 'pre-commit run --with-stuff') >> {
      throw new CommandExecutionException("ok")
    }

    1 * this.debugger.printDebug('some diffs')
    thrown(Exception)
  }
}
