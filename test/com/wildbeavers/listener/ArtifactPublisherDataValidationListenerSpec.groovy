package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ArtifactPublisherDataValidationListenerSpec extends Specification {
  private GenericFactory<ArtifactPublisherData> artifactPublisherDataGenericFactory = Mock()

  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()
  private ArtifactPublisherDataValidationListener subject

  def setup() {
    this.artifactPublisherDataGenericFactory.instantiate() >> { return new ArtifactPublisherData() }
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> [
      artifactPublisher_pathsToExclude      : ['foo'],
      artifactPublisher_compressionEnabled  : false,
      artifactPublisher_compressionAlgorithm: 'gz',
      artifactPublisher_baseUrl             : 's3://foo/bar',
      artifactPublisher_version             : '1.2.3',
      artifactPublisher_accessorName        : 'aws'
    ]
    this.subject = new ArtifactPublisherDataValidationListener(this.dataValidator, this.artifactPublisherDataGenericFactory)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactPublisherDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    ArtifactPublisherData == result.getRunnerEventData().getArtifactPublisherData().getClass()
    result.getRunnerEventData().getArtifactPublisherData().getBaseUrl() == 's3://foo/bar'
    result.getRunnerEventData().getArtifactPublisherData().getPathToExclude() == ['foo']
    result.getRunnerEventData().getArtifactPublisherData().getCompressionEnabled() == false
    result.getRunnerEventData().getArtifactPublisherData().getCompressionAlgorithm() == 'gz'
    result.getRunnerEventData().getArtifactPublisherData().getVersion().toString() == '1.2.3'
    result.getRunnerEventData().getArtifactPublisherData().getAccessorName() == 'aws'
  }

  @Test
  def "it does not register any artifact data if there are none"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [:]

    result == this.event
    result.getRunnerEventData().getArtifactPublisherData().getAccessorName() == null
    result.getRunnerEventData().getArtifactPublisherData().getBaseUrl() == null
    result.getRunnerEventData().getArtifactPublisherData().getVersion() == null
  }
}
