package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.TimeoutProxy
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException
import org.junit.Test
import spock.lang.Specification

class CleanupDelayListenerSpec extends Specification {
  private Debugger debugger = Mock()
  private TimeoutProxy timeoutProxy = Mock()
  private GroovyScriptMock context = Spy()

  private RunnerEventData eventData = Mock()
  private FlowInterruptedException flowInterruptedException = GroovyMock()

  private CleanupDelayListener subject

  def setup() {
    this.timeoutProxy.timeout(_, _, _) >> { arg1, arg2, arg3 -> arg3() }

    this.eventData.getCleanupDelayHeader() >> 'HEADER MESSAGE'
    this.eventData.getCleanupDelayMessage() >> 'Message'
    this.eventData.getCleanupDelayButtonLabel() >> 'OK'
    this.eventData.getCleanupDelayEnabled() >> true
    this.eventData.getCleanupDelayContinueIfExpired() >> true
    this.eventData.getCleanupDelayTime() >> 100

    this.subject = new CleanupDelayListener(this.context, this.timeoutProxy, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == CleanupDelayListener
  }

  @Test
  def "it listens to all the lint, tests and static analysis events"() {
    expect:
    [RunnerEvents.POST_DEPLOY] == this.subject.listenTo()
  }

  @Test
  def "it runs very late"() {
    expect:
    9999 == this.subject.getOrder()
  }

  @Test
  def "it throws an exception if the event data is null"() {
    when:
    this.eventData = null

    this.subject.run(this.eventData)

    then:
    def e = thrown(RuntimeException)
    e.getMessage() =~ /Cannot delay cleanup/
  }

  @Test
  def "it does not run when toggle is off"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getCleanupDelayEnabled() >> false

    !result
  }

  @Test
  def "it waits for a given amount of time before proceeding"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    1 * this.timeoutProxy.timeout(100, { it.toString() == 'MINUTES' }, _) >> { arg1, arg2, arg3 -> arg3() }
    1 * this.debugger.print('HEADER MESSAGE')
    1 * this.context.input(message: 'Message', ok: 'OK', submitterParameter: 'approver')
    result == this.eventData
  }

  @Test
  def "it waits for a given amount of time before proceeding, but throws an exception if user did not input confirmation"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.eventData.getCleanupDelayContinueIfExpired() >> false
    1 * this.timeoutProxy.timeout(100, { it.toString() == 'MINUTES' }, _) >> {
      throw this.flowInterruptedException
    }

    thrown(FlowInterruptedException)
  }

  @Test
  def "it throws exception in all cases, if it is not specifically a timeout exception"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.eventData.getCleanupDelayContinueIfExpired() >> true
    1 * this.timeoutProxy.timeout(100, { it.toString() == 'MINUTES' }, _) >> {
      throw new Exception('oops')
    }

    thrown(Exception)
  }

  @Test
  def "it does not throw an exception if the user option accept timeout as a normal workflow"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.eventData.getCleanupDelayContinueIfExpired() >> true
    1 * this.timeoutProxy.timeout(100, { it.toString() == 'MINUTES' }, _) >> {
      throw this.flowInterruptedException
    }

    noExceptionThrown()
  }
}
