package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ArtifactFetcherDataValidationListenerSpec extends Specification {
  private GenericFactory<ArtifactFetchingData> artifactFetchingDataGenericFactory = Mock()
  private GenericFactory<ArtifactData> artifactDataGenericFactory = Mock()

  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()
  private ArtifactFetcherDataValidationListener subject

  def setup() {
    this.artifactFetchingDataGenericFactory.instantiate() >> { return new ArtifactFetchingData() }
    this.artifactDataGenericFactory.instantiate() >> { return new ArtifactData() }
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> [
      artifactFetching_artifacts: [
        [
          url    : 'https://path.to/some_artifact',
          version: '1.2.3',
        ],
        [
          url         : 's3://some-s3-somewhere/data.zip',
          accessorName: 'custom',
        ],
      ],
    ]
    this.subject = new ArtifactFetcherDataValidationListener(this.dataValidator, this.artifactFetchingDataGenericFactory, this.artifactDataGenericFactory)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactFetcherDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    ArtifactFetchingData == result.getRunnerEventData().getArtifactFetchingData().getClass()
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData()[0].getUrl() == 'https://path.to/some_artifact'
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData()[0].getVersion() == '1.2.3'
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData()[0].getAccessorName() == ''
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData()[1].getUrl() == 's3://some-s3-somewhere/data.zip'
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData()[1].getVersion() == ''
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData()[1].getAccessorName() == 'custom'
  }

  @Test
  def "it does not register any artifact data if there are none"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [:]

    result == this.event
    result.getRunnerEventData().getArtifactFetchingData().getArtifactData().size() == 0
  }
}
