package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class FileStandardDataValidationListenerSpec extends Specification {
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  ControllerEventData event = Spy()
  FileStandardData fileStandardData = Spy()
  RunnerEventData runnerEventData = Spy()
  FileStandardDataValidationListener subject

  def setup() {
    this.runnerEventData.getFileStandardData() >> this.fileStandardData
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> [
      fileStandard_mandatoryFiles: [
        'mandatory',
        'this/should/be.there',
      ],
      fileStandard_forbiddenFiles: [
        'no.txt'
      ]
    ]
    this.subject = new FileStandardDataValidationListener(this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == FileStandardDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    this.runnerEventData.isFileStandardEnabled()
    this.fileStandardData.getMandatoryFiles().contains('mandatory')
    this.fileStandardData.getMandatoryFiles().contains('this/should/be.there')
    this.fileStandardData.getMandatoryFiles().contains('LICENSE')
    this.fileStandardData.getMandatoryFiles().size() == 4
    this.fileStandardData.getForbiddenFiles().size() == 1
    this.fileStandardData.getForbiddenFiles().contains('no.txt')
  }

  @Test
  def "it does not add default files if relevant toggle is enabled"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.runnerEventData.isFileStandardIgnoreDefault() >> true

    result == this.event
    this.runnerEventData.isFileStandardEnabled()
    this.fileStandardData.getMandatoryFiles().contains('mandatory')
    this.fileStandardData.getMandatoryFiles().contains('this/should/be.there')
    !this.fileStandardData.getMandatoryFiles().contains('LICENSE')
    this.fileStandardData.getMandatoryFiles().size() == 2
    this.fileStandardData.getForbiddenFiles().size() == 1
    this.fileStandardData.getForbiddenFiles().contains('no.txt')
  }

  @Test
  def "it checks and validate alternative data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [
      fileStandard_enabled       : false,
      fileStandard_ignoreDefault : true,
      fileStandard_mandatoryFiles: [
        'this/should/be.there',
      ],
      fileStandard_forbiddenFiles: [
        'no.txt'
      ]
    ]

    result == this.event
    !this.runnerEventData.isFileStandardEnabled()
    this.runnerEventData.isFileStandardIgnoreDefault()
    this.fileStandardData.getMandatoryFiles().contains('this/should/be.there')
    !this.fileStandardData.getMandatoryFiles().contains('LICENSE')
    this.fileStandardData.getMandatoryFiles().size() == 1
    this.fileStandardData.getForbiddenFiles().size() == 1
    this.fileStandardData.getForbiddenFiles().contains('no.txt')
  }

  @Test
  def "it runs early to allow any other data validation to add and ovverides over the defaults"() {
    expect:
    100 == this.subject.getOrder()
  }
}
