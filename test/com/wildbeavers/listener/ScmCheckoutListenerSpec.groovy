package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.data.CommandResult
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import hudson.plugins.git.GitSCM
import hudson.plugins.git.UserRemoteConfig
import hudson.plugins.git.extensions.GitSCMExtension
import hudson.plugins.git.extensions.GitSCMExtensionDescriptor
import hudson.util.DescribableList
import org.junit.Test
import spock.lang.Specification

class ScmCheckoutListenerSpec extends Specification {
  private GroovyScriptMock context = Mock()
  private ExecuteProxy executeProxy = Mock()

  private UserRemoteConfig userRemoteConfig = Mock()
  private CommandResult commandResult = Mock()
  private GitSCM gitSCM = Mock()
  private RunnerEventData eventData = Mock()

  private ScmCheckoutListener subject

  def setup() {
    DescribableList<GitSCMExtension, GitSCMExtensionDescriptor> extensions = [:]
    this.eventData.getScmAlternativeCheckoutTag() >> null
    this.userRemoteConfig.getName() >> 'the_remote'
    this.userRemoteConfig.getCredentialsId() >> 'creds'
    this.userRemoteConfig.getUrl() >> 'https://example.com'
    this.userRemoteConfig.getRefspec() >> '+refs/heads/*:refs/remotes/origin/*'
    this.gitSCM.branches >> ['master']
    this.gitSCM.extensions >> extensions
    this.gitSCM.getUserRemoteConfigs() >> [this.userRemoteConfig]
    this.context.scm >> this.gitSCM
    this.executeProxy.execute(*_) >> this.commandResult
    this.executeProxy.executeAndAllowError(*_) >> this.commandResult
    this.subject = new ScmCheckoutListener(this.context, this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ScmCheckoutListener
  }

  @Test
  def "it listens to the prepare pipeline event"() {
    expect:
    [RunnerEvents.PRE_PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it checkouts normally"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    result == eventData
    1 * this.context.checkout({ it ->
      it.userRemoteConfigs[0].credentialsId == 'creds'
      it.userRemoteConfigs[0].url == 'https://example.com'
      it.userRemoteConfigs[0].name == 'the_remote'
    })
    0 * this.context.execute({
      it.script == "git checkout 1.0.0"
    })
  }

  @Test
  def "it throws an exception if the event data is null"() {
    when:
    this.eventData = null

    this.subject.run(this.eventData)

    then:
    def e = thrown(RuntimeException)
    e.getMessage() =~ /Cannot checkout/
  }

  @Test
  def "it checkouts with alternative data"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    this.eventData.getScmAlternativeCheckoutRepositoryURL() >> 'https://example.com/alt.git'
    this.eventData.getScmAlternativeCheckoutCredentialID() >> 'altcreds'
    this.eventData.getScmAlternativeCheckoutTag() >> ''

    result == this.eventData
    1 * this.context.checkout({ it ->
      it.userRemoteConfigs[0].credentialsId == 'altcreds'
      it.userRemoteConfigs[0].url == 'https://example.com/alt.git'
    })
  }

  @Test
  def "it checkouts from a specific tag if required"() {
    when:
    eventData.getScmAlternativeCheckoutTag() >> '1.0.0'
    def result = this.subject.run(this.eventData)

    then:
    result == eventData
    1 * this.context.checkout(_)
  }

  @Test
  def "it throws an error if the checkout tag required does not exist"() {
    when:
    this.eventData.getScmAlternativeCheckoutTag() >> '1.0.0'
    this.commandResult.getStdout() >> ''
    this.subject.run(this.eventData)

    then:
    thrown(Exception)
    0 * this.context.execute({
      it.script == "git checkout 1.0.0"
    })
  }

  @Test
  def "it executes at high priority"() {
    expect:
    this.subject.getOrder() <= 15
  }
}
