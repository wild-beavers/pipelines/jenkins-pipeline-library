package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.ContainerRegistryOption
import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ContainerLoginDataValidationListenerSpec extends Specification {
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()
  private GenericFactory<ContainerLoginData> containerLoginDataGenericFactory = Mock()
  private GenericFactory<ContainerRegistryOption> containerRegistryOptionDataGenericFactory = Mock()

  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()

  Map containerRunCredentialData = [
    containerLogin_enabled            : true,
    containerLogin_containerRegistries: [
      [
        credentialProcessUsername                    : 'aws',
        credentialProcessContainerRunCommandArguments: 'get-password',
        fqdn                                         : 'example.org',
        credentialProcessContainerOptions            : [
          fqdn     : 'docker.io',
          imageName: 'aws-cli',
          imageTag : 'latest',
          tool     : 'podman',
        ]
      ]
    ],
  ]

  Map credentialData = [
    containerLogin_enabled            : true,
    containerLogin_tool               : 'podman',
    containerLogin_authfile           : '/path/to/auth.json',
    containerLogin_containerRegistries: [
      [
        credentialId: 'credential',
        fqdn        : 'test.org',
      ],
      [
        credentialId: 'credential2',
        fqdn        : 'another-reg.cloud',
      ],
    ],
  ]

  Map disabledCredentialData = [
    containerLogin_enabled: false,
  ]

  ContainerLoginDataValidationListener subject

  def setup() {
    this.containerRegistryOptionDataGenericFactory.instantiate() >> { return new ContainerRegistryOption(new ContainerOptions()) }
    this.containerLoginDataGenericFactory.instantiate() >> { return new ContainerLoginData() }
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> this.credentialData
    this.subject = new ContainerLoginDataValidationListener(this.dataValidator, this.containerLoginDataGenericFactory, this.containerRegistryOptionDataGenericFactory)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerLoginDataValidationListener
  }

  @Test
  def "it listens to the a conteoller event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    ContainerLoginData == result.getRunnerEventData().getContainerLoginData().getClass()
    result.getRunnerEventData().getContainerLoginData().isEnabled()
    '/path/to/auth.json' == result.getRunnerEventData().getContainerLoginData().getAuthfile()
    'podman' == result.getRunnerEventData().getContainerLoginData().getTool().toString()
    'credential' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getCredentialId()
    'credential2' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[1].getCredentialId()
    'test.org' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getFqdn()
    'another-reg.cloud' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[1].getFqdn()
    result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].hasCredentialId()
    result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[1].hasCredentialId()
  }

  @Test
  def "it checks and validate data with credential process instead of credentialID"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> this.containerRunCredentialData

    result == this.event
    ContainerLoginData == result.getRunnerEventData().getContainerLoginData().getClass()
    result.getRunnerEventData().getContainerLoginData().isEnabled()
    ContainerLoginData.DEFAULT_TOOL == result.getRunnerEventData().getContainerLoginData().getTool().toString()
    'example.org' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getFqdn()
    !result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].hasCredentialId()
    'aws' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getCredentialProcessUsername()
    'get-password' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getCredentialProcessContainerRunCommandArguments()
    'docker.io/aws-cli:latest' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getContainerOptions().getImage()
    'podman' == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions()[0].getContainerOptions().getTool().toString()
  }


  @Test
  def "it checks and validate empty data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> this.disabledCredentialData

    result == this.event
    ContainerLoginData == result.getRunnerEventData().getContainerLoginData().getClass()
    !result.getRunnerEventData().getContainerLoginData().isEnabled()
    ContainerLoginData.DEFAULT_TOOL == result.getRunnerEventData().getContainerLoginData().getTool().toString()
    0 == result.getRunnerEventData().getContainerLoginData().getContainerRegistryOptions().size()
  }
}
