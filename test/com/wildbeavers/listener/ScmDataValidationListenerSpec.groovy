package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.data.ScmAutoTagMethod
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ScmDataValidationListenerSpec extends Specification {
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()
  private ScmDataValidationListener subject

  def setup() {
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> [
      scm_alternativeCheckoutCredentialID : 'checkout-pass',
      scm_alternativeCheckoutDirectory    : 'tmp/checkout',
      scm_alternativeCheckoutRepositoryURL: 'https://git.example.net/group/repo.git',
      scm_alternativeCheckoutTag          : 'v1',
      scm_floatingTagsEnabled             : true,
      scm_autoTagEnabled                  : false,
      scm_autoTagMethod                   : ScmAutoTagMethod.TAG,
      scm_autoTagChangelogFilePath        : 'VERSION',
      scm_autoTagLastTagRegex             : ~/v\d+/,
    ]
    this.subject = new ScmDataValidationListener(this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ScmDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    result.getRunnerEventData().getScmAlternativeCheckoutCredentialID() == 'checkout-pass'
    result.getRunnerEventData().getScmAlternativeCheckoutDirectory() == 'tmp/checkout'
    result.getRunnerEventData().getScmAlternativeCheckoutRepositoryURL() == 'https://git.example.net/group/repo.git'
    result.getRunnerEventData().getScmAlternativeCheckoutTag() == 'v1'
    result.getRunnerEventData().getScmFloatingTagsEnabled()
    !result.getRunnerEventData().getScmAutoTagEnabled()
    result.getRunnerEventData().getScmAutoTagMethod() == ScmAutoTagMethod.TAG
    result.getRunnerEventData().getScmAutoTagChangelogFilePath() == 'VERSION'
    result.getRunnerEventData().getScmAutoTagLastTagRegex().toString() == /v\d+/
  }

  @Test
  def "it uses default data when user options are empty"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [:]

    result == this.event
    result.getRunnerEventData().getScmAlternativeCheckoutCredentialID() == ''
    result.getRunnerEventData().getScmAlternativeCheckoutDirectory() == null
    result.getRunnerEventData().getScmAlternativeCheckoutRepositoryURL() == null
    result.getRunnerEventData().getScmAlternativeCheckoutTag() == null
  }
}
