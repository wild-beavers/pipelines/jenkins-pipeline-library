package com.wildbeavers.listener


import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.listener.HeaderDisplayerListener
import org.junit.Test
import spock.lang.Specification

class HeaderDisplayerListenerSpec extends Specification {
  Debugger debugger = Mock()

  RunnerEventData eventData = Mock()

  HeaderDisplayerListener subject

  def setup() {
    this.eventData.getHeaderMessage() >> 'this is a header'

    this.subject = new HeaderDisplayerListener(this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == HeaderDisplayerListener
  }

  @Test
  def "it listens to the pipeline pre-prepare event"() {
    expect:
    [RunnerEvents.PRE_PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it runs early in PRE_PREPARE"() {
    expect:
    5 == this.subject.getOrder()
  }

  @Test
  def "it does not run if header message is empty mandatory files"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getHeaderMessage() >> ''

    !result
  }

  @Test
  def "it displays a header when it’s not empty"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    1 * this.debugger.print('this is a header')
    result == this.eventData
  }
}
