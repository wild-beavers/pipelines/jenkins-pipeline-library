package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class CleanupDelayDataValidationListenerSpec extends Specification {
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()
  private CleanupDelayDataValidationListener subject

  def setup() {
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> [
      cleanupDelay_enabled    : true,
      cleanupDelay_time       : 30,
      cleanupDelay_message    : 'Are you ready?',
      cleanupDelay_header     : 'The cleanup is delayed',
      cleanupDelay_buttonLabel: 'letsgo',
    ]
    this.subject = new CleanupDelayDataValidationListener(this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == CleanupDelayDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    result.getRunnerEventData().getCleanupDelayEnabled()
    result.getRunnerEventData().getCleanupDelayButtonLabel() == 'letsgo'
    result.getRunnerEventData().getCleanupDelayMessage() == 'Are you ready?'
    result.getRunnerEventData().getCleanupDelayHeader() == 'The cleanup is delayed'
    result.getRunnerEventData().getCleanupDelayTime() == 30
  }
}
