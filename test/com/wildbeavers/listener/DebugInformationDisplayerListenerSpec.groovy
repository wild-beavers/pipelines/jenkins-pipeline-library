package com.wildbeavers.listener

import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

class DebugInformationDisplayerListenerSpec extends Specification {
  Debugger debugger = Mock()
  ExecuteProxy executeProxy = Mock()
  ControllerEventData event = Mock()
  DebugInformationDisplayerListener subject

  def setup() {
    this.debugger.debugVarExists() >> true
    this.event.getRawConfig() >> [this: 'is_a_dummy_conf']

    this.subject = new DebugInformationDisplayerListener(this.executeProxy, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DebugInformationDisplayerListener
  }

  @Test
  def "it listens to the pre-prepare pipeline event"() {
    expect:
    [ControllerEvents.PRE_RUNNER] == this.subject.listenTo()
  }

  @Test
  def "it is not run if debug var does not exists"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.debugger.debugVarExists() >> false
    !result
  }

  @Test
  def "it displays raw user configuration"() {
    when:
    def result = this.subject.run(this.event)

    then:
    1 * this.debugger.printDebug([this: 'is_a_dummy_conf'])
    result == this.event
  }

  @Test
  def "it runs version checking if tools are installed"() {
    when:
    def result = this.subject.run(this.event)

    then:
    1 * this.executeProxy.execute('docker version')
    1 * this.executeProxy.execute('podman version')
    1 * this.executeProxy.execute('java -version')
    1 * this.executeProxy.execute('groovy -version')
    result == this.event
  }

  @Test
  def "it does not check tool versions if they are not installed"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.executeProxy.execute('which docker') >> {
      throw new Exception()
    }
    this.executeProxy.execute('which podman') >> {
      throw new Exception()
    }
    this.executeProxy.execute('which java') >> {
      throw new Exception()
    }
    this.executeProxy.execute('which groovy') >> {
      throw new Exception()
    }

    0 * this.executeProxy.execute('docker version')
    0 * this.executeProxy.execute('podman version')
    0 * this.executeProxy.execute('java -version')
    0 * this.executeProxy.execute('groovy -version')
    result == this.event
  }
}
