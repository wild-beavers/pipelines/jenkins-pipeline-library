package com.wildbeavers.listener

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.listener.LinterListener
import org.junit.Test
import spock.lang.Specification

class LinterListenerSpec extends Specification {
  private ContainerRunner containerRunner = Mock()

  private RunnerEventData event = Spy()
  private ContainerOptions containerOptions = Spy()
  private LinterData linterDataOne = Spy()
  private LinterData linterDataTwo = Spy()

  private LinterListener subject

  def setup() {
    this.linterDataOne.getCommandArguments() >> 'command one -args'
    this.linterDataOne.getContainerOptions() >> this.containerOptions
    this.linterDataTwo.getCommandArguments() >> 'command two -args'
    this.linterDataTwo.getContainerOptions() >> this.containerOptions
    this.event.isLinterEnabled() >> true
    this.event.getLinterData() >> [this.linterDataOne, this.linterDataTwo]
    this.subject = new LinterListener(this.containerRunner)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == LinterListener
  }

  @Test
  def "it listens to the a runner event"() {
    expect:
    [RunnerEvents.LINT] == this.subject.listenTo()
  }

  @Test
  def "it does run if linter is enabled & data set"() {
    expect:
    this.subject.shouldRun(this.event)
  }

  @Test
  def "it does not run if linter is not enabled"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.event.isLinterEnabled() >> false

    !result
  }

  @Test
  def "it does not run if linter data is empty"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.event.getLinterData() >> null

    !result

    when:
    result = this.subject.shouldRun(this.event)

    then:
    this.event.getLinterData() >> []

    !result
  }

  @Test
  def "it runs different linters"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    1 * this.containerRunner.run(this.containerOptions, 'command one -args')
    1 * this.containerRunner.run(this.containerOptions, 'command two -args')
  }
}
