package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class LinterDataValidationListenerSpec extends Specification {
  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()

  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()
  private GenericFactory<LinterData> linterDataFactory = Mock()

  Map linterData = [
    linter_enabled: true,
    linter_data   : [
      [
        containerTool           : 'podman',
        containerFqdn           : 'docker.io',
        containerImageName      : 'aws-cli',
        containerImageTag       : 'latest',
        containerEntrypoint     : 'entrypoint',
        containerPrivileged     : true,
        containerFallbackCommand: 'command',
        commandArguments        : '-some -args',
      ]
    ],
  ]

  Map linterDataEmpty = [
    linter_enabled: true,
    linter_data   : [],
  ]

  Map disabledLinterData = [
    linter_enabled: false,
  ]

  LinterDataValidationListener subject

  def setup() {
    this.linterDataFactory.instantiate() >> new LinterData(new ContainerOptions())
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> this.linterData
    this.subject = new LinterDataValidationListener(this.linterDataFactory, this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == LinterDataValidationListener
  }

  @Test
  def "it runs earlier than most listeners"() {
    expect:
    this.subject.getOrder() == 100
  }

  @Test
  def "it listens to the a conteoller event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it does not run if linter is not enabled"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.event.getRawConfig() >> this.disabledLinterData

    !result
  }

  @Test
  def "it does not run if linter is enabled"() {
    expect:
    this.subject.shouldRun(this.event)
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    result.getRunnerEventData().getLinterEnabled()
    '-some -args' == result.getRunnerEventData().getLinterData()[0].getCommandArguments()
    'command' == result.getRunnerEventData().getLinterData()[0].getContainerOptions().getFallbackCommand()
    'podman' == result.getRunnerEventData().getLinterData()[0].getContainerOptions().getTool().toString()
    'docker.io/aws-cli:latest' == result.getRunnerEventData().getLinterData()[0].getContainerOptions().getImage()
    'entrypoint' == result.getRunnerEventData().getLinterData()[0].getContainerOptions().getEntrypoint()
  }

  @Test
  def "it checks and validate data without any linter data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> this.linterDataEmpty

    result == this.event
    [] == result.getRunnerEventData().getLinterData()
    result.getRunnerEventData().isLinterEnabled()
  }
}
