package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

class JobPropertiesListenerSpec extends Specification {
  private GroovyScriptMock context = Spy()
  private Debugger debugger = Mock()

  private ControllerEventData eventData = Mock()

  private JobPropertiesListener subject

  def setup() {
    this.context.logRotator(_) >> "logRotatorInstance"
    this.context.cron(_) >> "cron"
    this.context.pipelineTriggers(["cron"]) >> "pipelineTriggersInstance"
    this.context.buildDiscarder("logRotatorInstance") >> "buildDiscarder"
    this.subject = new JobPropertiesListener(this.context, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == JobPropertiesListener
  }

  @Test
  def "it listens to the prepare pipeline event"() {
    expect:
    [ControllerEvents.PRE_RUNNER] == this.subject.listenTo()
  }

  @Test
  def "it setup current job properties with defaults"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    result == eventData
    1 * this.context.properties(["buildDiscarder", "pipelineTriggersInstance"])
  }
}
