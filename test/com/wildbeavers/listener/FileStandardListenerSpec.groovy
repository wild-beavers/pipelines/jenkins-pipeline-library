package com.wildbeavers.listener

import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.FileProxy
import org.junit.Test
import spock.lang.Specification

class FileStandardListenerSpec extends Specification {
  private FileProxy fileProxy = Mock()

  private FileStandardData fileStandardData = Spy()
  private RunnerEventData eventData = Spy()

  private FileStandardListener subject

  def setup() {
    this.fileStandardData.getMandatoryFiles() >> ['mandatory.txt']
    this.fileStandardData.getForbiddenFiles() >> ['forbidden.doc', 'no']
    this.fileProxy.exists('mandatory.txt') >> true
    this.fileProxy.exists('forbidden.doc') >> false
    this.fileProxy.exists('no') >> false
    this.eventData.getFileStandardData() >> this.fileStandardData
    this.eventData.isFileStandardEnabled() >> true

    this.subject = new FileStandardListener(this.fileProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == FileStandardListener
  }

  @Test
  def "it listens to the pipeline pre-prepare event"() {
    expect:
    [RunnerEvents.STATIC_ANALYSIS] == this.subject.listenTo()
  }

  @Test
  def "it runs when file standard is enabled and it contains some files"() {
    when:
    def result = this.subject.shouldRun(this.eventData)
    def explanations = this.subject.explainNoRun(this.eventData)

    then:
    result
    explanations.size() == 0
  }

  @Test
  def "it does not run if fileStandard is disabled"() {
    when:
    def result = this.subject.shouldRun(this.eventData)
    def explanations = this.subject.explainNoRun(this.eventData)

    then:
    this.eventData.isFileStandardEnabled() >> false

    explanations.contains('FileStandard step was explicitly disabled.')
    explanations.size() == 1
    !result
  }

  @Test
  def "it does not run if there are not mandatory nor forbidden files to check"() {
    when:
    def result = this.subject.shouldRun(this.eventData)
    def explanations = this.subject.explainNoRun(this.eventData)

    then:
    this.fileStandardData.getMandatoryFiles() >> []
    this.fileStandardData.getForbiddenFiles() >> []

    explanations.contains('No mandatory nor forbidden files were given.')
    explanations.size() == 1
    !result
  }

  @Test
  def "it gives all the explanations if the listeners does not run for multiple reasons"() {
    when:
    def explanations = this.subject.explainNoRun(this.eventData)

    then:
    this.eventData.isFileStandardEnabled() >> false
    this.fileStandardData.getMandatoryFiles() >> []
    this.fileStandardData.getForbiddenFiles() >> []

    explanations.contains('FileStandard step was explicitly disabled.')
    explanations.contains('No mandatory nor forbidden files were given.')
    explanations.size() == 2
  }

  @Test
  def "it runs without errors when mandatory files exists and forbidden dont"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    result == this.eventData
  }

  @Test
  def "it throws an exception with all the issues"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.fileProxy.exists('mandatory.txt') >> false
    this.fileProxy.exists('forbidden.doc') >> true

    def e = thrown(RuntimeException)
    e.getMessage() == 'FileStandard validations failed:\nA file “mandatory.txt” is mandatory for this kind of project but it was not found.\nThe file “forbidden.doc” is forbidden for this kind of project but it was found.'
  }
}
