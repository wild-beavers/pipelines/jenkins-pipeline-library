package com.wildbeavers.listener

import com.wildbeavers.data_manager.ScmInfoManager
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ScmInfoRefresherListenerSpec extends Specification {
  private ScmInfoManager scmInfoManager = Mock()

  private RunnerEventData eventData

  private ScmInfoRefresherListener subject

  def setup() {
    this.subject = new ScmInfoRefresherListener(this.scmInfoManager)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ScmInfoRefresherListener
  }

  @Test
  def "it listens PRE_PREPARE events"() {
    expect:
    [RunnerEvents.PRE_PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it refreshes ScmInfo data object"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    result == eventData
    1 * this.scmInfoManager.refreshScmInfo()
  }

  @Test
  def "it executes at high priority"() {
    expect:
    this.subject.getOrder() > 10
    this.subject.getOrder() <= 20
  }
}
