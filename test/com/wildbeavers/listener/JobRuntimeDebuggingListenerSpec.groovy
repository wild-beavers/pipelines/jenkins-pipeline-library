package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.TimeoutProxy
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException
import org.junit.Test
import spock.lang.Specification

class JobRuntimeDebuggingListenerSpec extends Specification {
  private GroovyScriptMock context = Spy()
  private TimeoutProxy timeoutProxy = Mock()
  private EnvironmentVariableProxy environmentVariableProxy = Mock()

  private FlowInterruptedException timeoutException = GroovyMock()
  private ControllerEventData eventData = Mock()

  private JobRuntimeDebuggingListener subject

  def setup() {
    this.timeoutProxy.timeout(*_) >> { a1, a2, a3 -> a3() }
    this.eventData.getDebuggingEnabled() >> false
    this.eventData.getDebuggingRuntimeToggleEnabled() >> true
    this.eventData.getDebuggingRuntimeToggleTimeout() >> 10

    this.subject = new JobRuntimeDebuggingListener(this.context, this.timeoutProxy, this.environmentVariableProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == JobRuntimeDebuggingListener
  }

  @Test
  def "it listens to the pre-runner pipeline event"() {
    expect:
    [ControllerEvents.PRE_RUNNER] == this.subject.listenTo()
  }

  @Test
  def "it does not run if not debugging nor debugging runtime are enabled"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getDebuggingEnabled() >> false
    this.eventData.getDebuggingRuntimeToggleEnabled() >> false

    !result
  }

  @Test
  def "it does if debugging is enabled"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getDebuggingEnabled() >> true
    this.eventData.getDebuggingRuntimeToggleEnabled() >> false

    result
  }

  @Test
  def "it does if runtime debugging toggle is enabled"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getDebuggingEnabled() >> false
    this.eventData.getDebuggingRuntimeToggleEnabled() >> true

    result
  }

  @Test
  def "it does run if both debugging and runtime debugging toggle are enabled"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getDebuggingEnabled() >> true
    this.eventData.getDebuggingRuntimeToggleEnabled() >> true

    result
  }

  @Test
  def "it sets debugger mode when debugging is enabled and ignores runtime toggle"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    this.eventData.getDebuggingEnabled() >> true

    result == this.eventData
    1 * this.environmentVariableProxy.set('DEBUG', 'true')
    0 * this.context.input(*_)
  }

  @Test
  def "it sets debugger mode when debugging is disabled"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    result == this.eventData
    1 * this.environmentVariableProxy.set('DEBUG', 'true')
    1 * this.timeoutProxy.timeout(10, { it.toString() == 'SECONDS' }, _) >> { arg1, arg2, arg3 -> arg3() }
    1 * this.context.input(*_)
  }

  @Test
  def "it ignores timeouts and do not set debug mode in this case"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    this.timeoutProxy.timeout(10, _, _) >> { throw this.timeoutException }

    result == this.eventData
    0 * this.environmentVariableProxy.set('DEBUG', 'true')
    0 * this.context.input(*_)
    noExceptionThrown()
  }

  @Test
  def "it does not ignore standard exceptions"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.timeoutProxy.timeout(*_) >> { throw new Exception("oops") }

    0 * this.environmentVariableProxy.set('DEBUG', 'true')
    0 * this.context.input(*_)
    thrown(Exception)
  }
}
