package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.data.DependencyInstallerData
import com.wildbeavers.dependency_installer.DependencyInstaller
import com.wildbeavers.dependency_installer.DependencyInstallerFetcher
import com.wildbeavers.dependency_installer.DependencyInstallerType
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

class DependencyInstallerListenerSpec extends Specification {
  GroovyScriptMock context = Mock()
  Debugger debugger = Mock()
  DependencyInstallerFetcher dependencyInstallerFetcher = Mock()
  RunnerEventData event = Mock()
  DependencyInstallerData dependencyInstallerData = Mock()
  DependencyInstaller dependencyInstaller = Mock()

  DependencyInstallerListener subject

  def setup() {
    this.dependencyInstallerFetcher.fetch(DependencyInstallerType.VENV) >> this.dependencyInstaller
    this.event.getDependencyInstallerData() >> [(DependencyInstallerType.VENV): this.dependencyInstallerData]

    this.subject = new DependencyInstallerListener(this.context, this.debugger, this.dependencyInstallerFetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DependencyInstallerListener
  }

  @Test
  def "it listens to the PREPARE pipeline event"() {
    expect:
    [RunnerEvents.PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it runs if dependency installer config is set"() {
    when:
    def result = this.subject.run(this.event)

    then:
    1 * this.dependencyInstaller.install(this.dependencyInstallerData)
    result == this.event
  }

  @Test
  def "it does not run when dependency installer config is empty"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    1 * this.event.getDependencyInstallerData() >> [:]

    !result
  }
}
