package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class TagCheckerValidationListenerSpec extends Specification {
  private ControllerEventData event = Spy()
  private RunnerEventData runnerEventData = Spy()

  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()
  private GenericFactory<LinterData> linterDataFactory = Mock()

  private TagCheckerValidationListener subject

  Map data = [
    tagCheck_enabled       : true,
    tagCheck_onlyMainBranch: false,
    tagCheck_regex         : ~/v[0-9]+/,
  ]

  Map dataEmpty = [:]

  Map dataDisabled = [
    tagCheck_enabled: false,
  ]


  def setup() {
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> this.data
    this.subject = new TagCheckerValidationListener(this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TagCheckerValidationListener
  }

  @Test
  def "it runs earlier than most listeners"() {
    expect:
    this.subject.getOrder() == 100
  }

  @Test
  def "it listens to the a data validation event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it does not run if function is not enabled"() {
    when:
    def result = this.subject.shouldRun(this.event)

    then:
    this.event.getRawConfig() >> this.dataDisabled

    !result
  }

  @Test
  def "it does run if function is enabled"() {
    expect:
    this.subject.shouldRun(this.event)
    this.subject.shouldRun(this.event)
  }

  def "it does run if by default"() {
    when:
    def result  = this.subject.shouldRun(this.event)

    then:
    this.event.getRawConfig() >> this.dataEmpty

    result
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    result.getRunnerEventData().getTagCheckEnabled()
    !result.getRunnerEventData().getTagCheckOnlyMainBranch()
    result.getRunnerEventData().getTagCheckRegex().toString() == /v[0-9]+/
  }

  @Test
  def "it checks and validate data without any linter data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> this.dataEmpty

    result == this.event
    result.getRunnerEventData().getTagCheckEnabled()
    result.getRunnerEventData().getTagCheckOnlyMainBranch()
  }
}
