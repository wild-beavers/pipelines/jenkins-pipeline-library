package com.wildbeavers.listener

import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.HasTagCheckerData
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.helper.SemverHelper
import org.junit.Test
import spock.lang.Specification

class TagCheckerListenerSpec extends Specification {
  private ScmInfo scmInfo = GroovyMock()

  private HasTagCheckerData eventData = Mock()

  private TagCheckerListener subject

  def setup() {
    this.eventData.getTagCheckEnabled() >> true
    this.eventData.getTagCheckOnlyMainBranch() >> true
    this.eventData.getTagCheckRegex() >> ~(/(^latest$)|/ + SemverHelper.SEMVER_REGEXP_NO_PRERELEASE)
    this.scmInfo.isCurrentBranchDefault() >> true
    this.scmInfo.getAllRevisionTags() >> (['0', '0.1', '0.1.0', '0.2', '0.2.0', 'latest'] as LinkedHashSet<CharSequence>)

    this.subject = new TagCheckerListener(this.scmInfo)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TagCheckerListener
  }

  @Test
  def "it listens to the a linter event"() {
    expect:
    [RunnerEvents.LINT] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it does not run if option is disabled"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getTagCheckEnabled() >> false

    !result
  }

  @Test
  def "it does not run if current branch is not default"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.scmInfo.isCurrentBranchDefault() >> false

    !result
  }

  @Test
  def "it does run when all conditions are met"() {
    expect:
    this.subject.shouldRun(this.eventData)
  }

  @Test
  def "it does run if current branch is not default, if correct option is set"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getTagCheckOnlyMainBranch() >> false

    result
  }

  @Test
  def "it explains why it does not run"() {
    expect:
    ['Tag checks are disabled.'] == this.subject.explainNoRun(this.eventData)
  }

  @Test
  def "it runs and validate all tags are correct"() {
    expect:
    this.subject.run(this.eventData) == this.eventData
  }

  @Test
  def "it runs and validate all tags are with alternative regular expression"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    this.scmInfo.getAllRevisionTags() >> (['v0', 'v1', 'v2'] as LinkedHashSet<CharSequence>)
    this.eventData.getTagCheckRegex() >> ~/^v\d+$/

    result == this.eventData
  }

  @Test
  def "it detects offending tags"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.scmInfo.getAllRevisionTags() >> (['0', '0.1', 'v0.1.0', '0.2', 'master', '0.2.0', '0.2.0-dev1'] as LinkedHashSet<CharSequence>)

    Exception e = thrown(RuntimeException)
    e.getMessage() == /Some non-compliant tags were found in this project: v0.1.0, master, 0.2.0-dev1/
  }

  @Test
  def "it detects offending tags"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.scmInfo.getAllRevisionTags() >> []
  }

  @Test
  def "it runs very early"() {
    expect:
    this.subject.getOrder() == -10
  }
}
