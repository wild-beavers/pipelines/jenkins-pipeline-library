package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.ContainerRegistryOption
import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.data.CommandResult
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class ContainerLoginListenerSpec extends Specification {
  private GroovyScriptMock context = Spy()
  private ExecuteProxy executeProxy = Mock()
  private ContainerRunner containerRunner = Mock()

  RunnerEventData runnerEventData = Spy()
  ContainerLoginData containerLoginData = Spy()
  ContainerRegistryOption containerRegistryOption1 = Spy()
  ContainerOptions containerOptions1 = Spy()
  CommandResult commandResult = Mock()

  ContainerLoginListener subject

  def setup() {
    this.commandResult.getStdout() >> 'very_secret'
    this.containerRunner.run(*_) >> this.commandResult
    this.containerRegistryOption1.hasCredentialId() >> true
    this.containerRegistryOption1.getCredentialProcessContainerOptions() >> this.containerOptions1
    this.containerLoginData.isEnabled() >> true
    this.containerLoginData.getContainerRegistryOptions() >> [this.containerRegistryOption1]
    this.runnerEventData.getContainerLoginData() >> this.containerLoginData
    this.subject = new ContainerLoginListener(this.context, this.executeProxy, this.containerRunner)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerLoginListener
  }

  @Test
  def "it listens to the a conteoller event"() {
    expect:
    [RunnerEvents.PRE_PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it runs when enabled and container registry options are set"() {
    expect:
    this.subject.shouldRun(this.runnerEventData)
  }

  @Test
  def "it does not run when disabled"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.containerLoginData.isEnabled() >> false

    !result
  }

  @Test
  def "it does not run registry options is empty"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.containerLoginData.getContainerRegistryOptions() >> []

    !result
  }

  @Test
  def "it logins to registry using credential ID"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.executeProxy.basicExecute('echo "$CONTAINER_REGISTRY_PASSWORD" | docker login --username $CONTAINER_REGISTRY_USERNAME --password-stdin docker.io', false, false)
    result == this.runnerEventData
  }

  @Test
  def "it logins to registry using credential ID and alternative options"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerLoginData.getAuthfile() >> '/path/to/auth.json'
    this.containerLoginData.getTool() >> 'podman'
    this.containerRegistryOption1.getFqdn() >> 'test.org'

    1 * this.executeProxy.basicExecute('echo "$CONTAINER_REGISTRY_PASSWORD" | podman login --username $CONTAINER_REGISTRY_USERNAME --password-stdin --authfile /path/to/auth.json test.org', false, false)
    result == this.runnerEventData
  }

  @Test
  def "it logins to registry using credential process"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.containerRegistryOption1.hasCredentialId() >> false
    this.containerRegistryOption1.getCredentialProcessUsername() >> 'user'

    1 * this.executeProxy.basicExecute('echo "very_secret" | docker login --username user --password-stdin docker.io', false, false)
    result == this.runnerEventData
  }
}
