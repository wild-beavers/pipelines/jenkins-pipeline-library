package com.wildbeavers.listener

import com.wildbeavers.archiver.Archiver
import com.wildbeavers.artifact.ArtifactStore
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.ParallelProxy
import org.junit.Test
import spock.lang.Specification

class ArtifactPublisherListenerSpec extends Specification {
  private Debugger debugger = Mock()
  private Archiver archiver = Mock()
  private ExecuteProxy executeProxy = Mock()
  private ArtifactStore artifactStore = Mock()
  private ParallelProxy parallelProxy = Mock()
  private ContainerRunner containerRunner = Mock()
  private ScmInfo scmInfo = Mock()
  private GenericFactory<ArtifactData> artifactDataGenericFactory = Mock()
  private GenericFactory<ArtifactPublisherData> artifactPublisherDataGenericFactory = Mock()

  private CommandResult commandResult = Mock()
  private RunnerEventData eventData = Mock()
  private ArtifactData artifactData = Spy()
  private ArtifactPublisherData artifactPublisherData = Spy()

  private ArtifactPublisherListener subject

  def setup() {
    this.artifactDataGenericFactory.instantiate() >> this.artifactData
    this.commandResult.getStdout() >> 'list of files exclude'
    this.eventData.getArtifactPublisherData() >> this.artifactPublisherData
    this.artifactPublisherData.getBaseUrl() >> 's3://path/to/somewhere'
    this.artifactPublisherData.getCompressionAlgorithm() >> 'zip'
    this.artifactPublisherData.getPathToExclude() >> ['exclude']
    this.scmInfo.isPublishableAsAnything() >> true
    this.scmInfo.getAllRevisionSemverAndFloatingTags() >> ['major', 'minor', 'patch', 'master']
    this.scmInfo.getRepositoryName() >> 'repo'
    this.executeProxy.execute('ls') >> this.commandResult
    this.debugger.debugVarExists() >> true
    this.debugger.testCondition(_, _) >> { Boolean condition, CharSequence message ->
      return condition
    }

    this.subject = new ArtifactPublisherListener(this.debugger, this.archiver, this.executeProxy, this.artifactStore, this.parallelProxy, this.containerRunner, this.scmInfo, this.artifactDataGenericFactory)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactPublisherListener
  }

  @Test
  def "it listens to the pipeline publish event"() {
    expect:
    [RunnerEvents.PUBLISH] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it does not run if eventData does not have a artifact destination base URL"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.artifactPublisherData.getBaseUrl() >> ''

    1 * this.debugger.testCondition(false, {
      it.contains('no artifacts base URL given')
    }) >> false
    !result

    when:
    result = this.subject.shouldRun(this.eventData)

    then:
    this.artifactPublisherData.getBaseUrl() >> null

    1 * this.debugger.testCondition(false, {
      it.contains('no artifacts base URL given')
    }) >> false
    !result
  }

  @Test
  def "it does not run if current revision is not publishable"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.scmInfo.isPublishableAsAnything() >> false

    1 * this.debugger.testCondition(false, {
      it.contains('the current revision is not publishable')
    }) >> false
    !result
  }

  @Test
  def "it does run if current revision is publishable and artifacts base URL is set"() {
    expect:
    this.subject.shouldRun(this.eventData)
  }

  @Test
  def "it pulls container image before running in parallel"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    1 * this.artifactStore.preUploadActions(this.artifactPublisherData.getBaseUrl(), _, this.artifactData)
    result == this.eventData
  }

  @Test
  def "It compresses files and publishes artifacts with all the version when the code is publishable"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    1 * this.archiver.compress(['list', 'of', 'files'], _)
    1 * this.parallelProxy.parallel({ it.containsKey('patch') && it.containsKey('minor') && it.containsKey('major') && it.containsKey('master') }) >> { args -> args[0].each { it.value() } }
    1 * this.artifactStore.uploadArtifact('s3://path/to/somewhere/repo/patch.zip', 'temp.zip', this.artifactData)
    1 * this.artifactStore.uploadArtifact('s3://path/to/somewhere/repo/minor.zip', 'temp.zip', this.artifactData)
    1 * this.artifactStore.uploadArtifact('s3://path/to/somewhere/repo/major.zip', 'temp.zip', this.artifactData)
    1 * this.artifactStore.uploadArtifact('s3://path/to/somewhere/repo/master.zip', 'temp.zip', this.artifactData)
    result == this.eventData
  }

  @Test
  def "It publishes a single pre-release tag"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    this.scmInfo.getAllRevisionSemverAndFloatingTags() >> ['0.4.2-dev1']
    this.artifactPublisherData.getCompressionAlgorithm() >> 'gz'

    1 * this.archiver.compress(['list', 'of', 'files'], _)
    1 * this.parallelProxy.parallel({ it.containsKey('0.4.2-dev1') }) >> { args -> args[0].each { it.value() } }
    1 * this.artifactStore.uploadArtifact('s3://path/to/somewhere/repo/0.4.2-dev1.gz', 'temp.gz', this.artifactData)
    result == this.eventData
  }
}
