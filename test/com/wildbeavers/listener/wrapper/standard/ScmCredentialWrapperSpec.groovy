package com.wildbeavers.listener.wrapper.standard

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.SSHKeySetupHelper
import com.wildbeavers.listener.AutoTaggerListener
import com.wildbeavers.listener.ScmInfoRefresherListener
import com.wildbeavers.observer.EventListener
import hudson.plugins.git.GitSCM
import hudson.plugins.git.UserRemoteConfig
import org.junit.Test
import spock.lang.Specification

class ScmCredentialWrapperSpec extends Specification {
  private SSHKeySetupHelper sshKeySetupHelper = Mock()
  private UserRemoteConfig userRemoteConfigs = Mock()
  private GroovyScriptMock context = Spy()

  private GitSCM gitSCM = Mock()
  private AutoTaggerListener autoTaggerListener = Mock(AutoTaggerListener)
  private ScmInfoRefresherListener scmInfoRefresherListener = Mock(ScmInfoRefresherListener)
  private EventListener randomListener = Mock()

  private RunnerEventData eventData = Mock()

  private ScmCredentialWrapper subject

  def setup() {
    this.userRemoteConfigs.getCredentialsId() >> 'credential_id'
    this.userRemoteConfigs.getUrl() >> 'git@gitlab.com:wild-beavers/pipelines/jenkins-pipeline-library.git'
    this.context.scm >> this.gitSCM
    this.gitSCM.getUserRemoteConfigs() >> [this.userRemoteConfigs]
    this.context.sshUserPrivateKey(*_) >> [CVS_SSH_KEY_FILE: 'ssh', CVS_SSH_PASSPHRASE: 'passphrase', CVS_SSH_USERNAME: 'user']
    this.context.gitUsernamePassword(*_) >> [pass: 'git username password']
    this.subject = new ScmCredentialWrapper(this.sshKeySetupHelper, this.context, ['ssh-host-key'])
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ScmCredentialWrapper
  }

  @Test
  def "it supports AutoTaggerListener and CheckoutListener"() {
    expect:
    this.subject.support(this.autoTaggerListener)
    this.subject.support(this.scmInfoRefresherListener)
    !this.subject.support(this.randomListener)
  }


  @Test
  def "it does not support projects without authentication required"() {
    when:
    def result = this.subject.isPipelineSupportingAuthentication()

    then:
    this.gitSCM.getUserRemoteConfigs() >> []

    !result

    when:
    result = this.subject.isPipelineSupportingAuthentication()

    then:
    this.gitSCM.getUserRemoteConfigs() >> null

    !result

    when:
    result = this.subject.isPipelineSupportingAuthentication()

    then:
    this.gitSCM.getUserRemoteConfigs() >> [this.userRemoteConfigs]
    this.userRemoteConfigs.getCredentialsId() >> ''

    !result

    when:
    result = this.subject.isPipelineSupportingAuthentication()

    then:
    this.gitSCM.getUserRemoteConfigs() >> [this.userRemoteConfigs]
    this.userRemoteConfigs.getCredentialsId() >> null

    !result
  }

  @Test
  def "it does not support project not supporting scm"() {
    when:
    def result = this.subject.support(this.autoTaggerListener)

    then:
    this.userRemoteConfigs.getCredentialsId() >> { throw new MissingMethodException('getCredentialsId', UserRemoteConfig) }

    !result

    when:
    result = this.subject.support(this.autoTaggerListener)

    then:
    this.userRemoteConfigs.getCredentialsId() >> { throw new MissingPropertyException('getCredentialsId', UserRemoteConfig) }

    !result
  }

  @Test
  def "it wraps a listener in a project using HTTPS"() {
    when:
    def result = this.subject.wrap(this.autoTaggerListener, this.eventData)

    then:
    this.userRemoteConfigs.getUrl() >> 'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library.git'

    1 * this.context.withCredentials([[pass: 'git username password']], _)
    1 * this.autoTaggerListener.run(this.eventData) >> this.eventData
    0 * this.eventData.setScmSshConfigDirectory(_)
    result == this.eventData
  }

  @Test
  def "it wraps a listener in a project using SSH"() {
    when:
    def result = this.subject.wrap(this.autoTaggerListener, this.eventData)

    then:
    1 * this.context.withCredentials([[CVS_SSH_KEY_FILE: 'ssh', CVS_SSH_PASSPHRASE: 'passphrase', CVS_SSH_USERNAME: 'user']], _)
    1 * this.autoTaggerListener.run(this.eventData) >> this.eventData
    1 * this.eventData.setScmSshConfigDirectory('ssh_${BUILD_NUMBER}')
    result == this.eventData
  }
}
