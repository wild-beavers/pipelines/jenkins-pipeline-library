package com.wildbeavers.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.auto_tagger.AutoTagger
import com.wildbeavers.data.ScmAutoTagMethod
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.exception.ConfigurationException
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

import java.util.regex.Pattern

class AutoTaggerListenerSpec extends Specification {
  private AutoTagger autoTagger = Mock()
  private Debugger debugger = Mock()
  private GroovyScriptMock context = Spy()

  private Pattern pattern = GroovyMock()
  private RunnerEventData eventData = Mock()

  private AutoTaggerListener subject

  def setup() {
    this.eventData.getScmAutoTagChangelogFilePath() >> 'VERSION'
    this.eventData.getScmFloatingTagsEnabled() >> true
    this.eventData.getScmAutoTagEnabled() >> true
    this.eventData.getScmAutoTagLastTagRegex() >> this.pattern
    this.autoTagger.canAutoTag('VERSION', true) >> true
    this.eventData.getScmAutoTagMethod() >> ScmAutoTagMethod.TAG_ANNOTATED

//    this.autoTagger.canAutoTag() >> true
//    this.debugger.debugVarExists() >> true
    this.subject = new AutoTaggerListener(this.autoTagger, this.debugger, this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == AutoTaggerListener
  }

  @Test
  def "it listens to the prepare pipeline event"() {
    expect:
    [RunnerEvents.POST_PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it does not run if the auto tagger cannot auto tag"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.autoTagger.canAutoTag('VERSION', true) >> false

    !result
  }

  @Test
  def "it does not run if the auto-tag is disabled by user options"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getScmAutoTagEnabled() >> false

    !result
  }

  @Test
  def "it runs when all requirements are met"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    result
  }

  @Test
  def "it explains why it did not run"() {
    when:
    def result = this.subject.explainNoRun(this.eventData)

    then:
    result == ['This commit is a candidate to be publishable, and therefore could be tagged, but auto-tag eligibility checks failed. For more details, enable DEBUG mode.']
  }

  @Test
  def "it throws an exception if the AutoTagging method is not supported"() {
    when:
    this.subject.run(this.eventData)

    then:
    this.eventData.getScmAutoTagMethod() >> ScmAutoTagMethod.TAG

    def e = thrown(ConfigurationException)
    e.getMessage() =~ /auto tagger only support the type/
  }

  @Test
  def "it runs with TAG_ANNOTATED method, while locking"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    result == this.eventData
    1 * this.context.lock('auto-tagger', _)
    1 * this.autoTagger.autoTag('VERSION', this.pattern, true)
  }
}
