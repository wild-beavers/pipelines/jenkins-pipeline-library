package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import org.junit.Test
import spock.lang.Specification

class JobDataValidationListenerSpec extends Specification {
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  private ControllerEventData event = Spy()

  private JobDataValidationListener subject

  def setup() {
    this.event.getRawConfig() >> [
      job_keyIgnored                   : true,
      job_properties                   : ['some', 'properties'],
      job_nodeLabel                    : 'my_node',
      job_timeoutTime                  : 300,
      job_timeoutUnit                  : 'MINUTES',
      job_debuggingEnabled             : true,
      job_debuggingRuntimeToggleEnabled: true,
      job_debuggingRuntimeToggleTimeout: 50,
    ]
    this.subject = new JobDataValidationListener(this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == JobDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data with empty, default configuration"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [:]

    result == this.event
    this.event.getJobTimeoutTime() == 10
    this.event.getJobTimeoutUnit() == 'HOURS'
    this.event.getJobNodeLabel() == ''
    this.event.getJobProperties() == []
    !this.event.getDebuggingEnabled()
    !this.event.getDebuggingRuntimeToggleEnabled()
    this.event.getDebuggingRuntimeToggleTimeout() == 10
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    this.event.getJobTimeoutTime() == 300
    this.event.getJobTimeoutUnit() == 'MINUTES'
    this.event.getJobNodeLabel() == 'my_node'
    this.event.getJobProperties() == ['some', 'properties']
    this.event.getDebuggingEnabled()
    this.event.getDebuggingRuntimeToggleEnabled()
    this.event.getDebuggingRuntimeToggleTimeout() == 50
  }
}
