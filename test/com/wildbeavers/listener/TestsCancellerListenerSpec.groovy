package com.wildbeavers.listener

import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.listener.TestsCancellerListener
import org.junit.Test
import spock.lang.Specification

class TestsCancellerListenerSpec extends Specification {
  Debugger debugger = Mock()
  ScmInfo scmInfo = Mock()

  RunnerEventData eventData = Mock()

  TestsCancellerListener subject

  def setup() {
    this.scmInfo.isPublishable() >> true
    this.eventData.getTestsCancellerEnabled() >> true

    this.subject = new TestsCancellerListener(this.debugger, this.scmInfo)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TestsCancellerListener
  }

  @Test
  def "it listens to all the lint, tests and static analysis events"() {
    expect:
    [
      RunnerEvents.PRE_LINT,
      RunnerEvents.LINT,
      RunnerEvents.POST_LINT,
      RunnerEvents.PRE_STATIC_ANALYSIS,
      RunnerEvents.STATIC_ANALYSIS,
      RunnerEvents.POST_STATIC_ANALYSIS,
      RunnerEvents.PRE_TEST_UNIT,
      RunnerEvents.TEST_UNIT,
      RunnerEvents.POST_TEST_UNIT,
    ] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it runs very early"() {
    expect:
    1 == this.subject.getOrder()
  }

  @Test
  def "it does not run when project is not publishable"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.scmInfo.isPublishable() >> false

    !result
  }

  @Test
  def "it does not run when testCanceller pipeline config is set to false"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.eventData.getTestsCancellerEnabled() >> false

    !result
  }

  @Test
  def "it runs when project is publishable and test canceller is enabled"() {
    expect:
    this.subject.shouldRun(this.eventData)
  }

  @Test
  def "it prevents propagation and display a debug message"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    1 * this.debugger.printDebug({ it.contains('tests are hindered') })
    this.subject.stopPropagationAfterRun()
    result == this.eventData
  }
}
