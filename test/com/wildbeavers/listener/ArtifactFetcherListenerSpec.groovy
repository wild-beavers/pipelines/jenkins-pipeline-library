package com.wildbeavers.listener

import com.wildbeavers.archiver.Archiver
import com.wildbeavers.artifact.ArtifactStore
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.ParallelProxy
import org.junit.Test
import spock.lang.Specification

class ArtifactFetcherListenerSpec extends Specification {
  private Debugger debugger = Mock()
  private ArtifactStore artifactStore = Mock()
  private Archiver archiver = Mock()
  private ParallelProxy parallelProxy = Mock()

  private RunnerEventData eventData = Mock()
  private ArtifactFetchingData artifactFetchingData = Spy()
  private ArtifactData artifactData1 = Spy()
  private ArtifactData artifactData2 = Spy()
  private ArtifactData artifactData3 = Spy()

  private ArtifactFetcherListener subject

  def setup() {
    this.artifactData1.setUrl(new ShallowURL('s3://some/another/artifact.json'))
    this.artifactData1.getVersion() >> '345'
    this.artifactData2.setUrl(new ShallowURL('s3://test/1.tar.gz'))
    this.artifactData3.setUrl(new ShallowURL('s3://test'))

    this.debugger.debugVarExists() >> true
    this.eventData.getArtifactFetchingData() >> this.artifactFetchingData
    this.artifactFetchingData.getArtifactData() >> [this.artifactData1, this.artifactData2, this.artifactData3]
    this.subject = new ArtifactFetcherListener(this.debugger, this.artifactStore, this.archiver, this.parallelProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactFetcherListener
  }

  @Test
  def "it listens to the prepare pipeline event"() {
    expect:
    [RunnerEvents.PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it explains why it does not run"() {
    expect:
    this.subject.explainNoRun(this.eventData) == ['No artifacts to fetch were provided.']
  }

  @Test
  def "it does not run if there isn't any artifact to fetch"() {
    when:
    def result = this.subject.shouldRun(this.eventData)

    then:
    this.artifactFetchingData.getArtifactData() >> []

    !result
  }

  @Test
  def "it does runs downloads the required artifacts in parallel"() {
    when:
    def result = this.subject.run(this.eventData)

    then:
    1 * this.artifactStore.preDownloadActions({ it.contains('another/345/artifact.json') }, this.artifactData1)
    1 * this.artifactStore.preDownloadActions({ it.contains('1.tar.gz') }, this.artifactData2)
    1 * this.parallelProxy.parallel({ it.containsKey('another/345/artifact.json') && it.containsKey('1.tar.gz') && !it.containsKey('') })
    1 * this.debugger.printDebug('Ignoring s3://test. Empty URI path.')
    result == this.eventData
  }

  @Test
  def "it downloads and uncompresses artifacts that are archives"() {
    when:
    this.subject.fetch('tempDir', this.artifactData1)

    then:
    1 * this.artifactStore.downloadArtifact('tempDir', this.artifactData1)
    1 * this.archiver.uncompress('tempDir', 'another')
  }
}
