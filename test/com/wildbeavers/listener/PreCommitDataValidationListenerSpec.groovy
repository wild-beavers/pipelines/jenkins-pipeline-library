package com.wildbeavers.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.data.pre_commit.PreCommitData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class PreCommitDataValidationListenerSpec extends Specification {
  private GenericFactory<PreCommitData> preCommitDataGenericFactory = Mock()
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  ControllerEventData event = Spy()
  RunnerEventData runnerEventData = Spy()
  PreCommitDataValidationListener subject

  def setup() {
    this.preCommitDataGenericFactory.instantiate() >> { return new PreCommitData(new ContainerOptions(PreCommitData.DEFAULT_CONTAINER_TOOL, PreCommitData.DEFAULT_CONTAINER_FQDN, PreCommitData.DEFAULT_CONTAINER_IMAGE_NAME, PreCommitData.DEFAULT_CONTAINER_IMAGE_TAG)) }
    this.event.getRunnerEventData() >> this.runnerEventData
    this.event.getRawConfig() >> [
      preCommit_enabled          : false,
      preCommit_containerImageTag: 'custom',
    ]
    this.subject = new PreCommitDataValidationListener(this.preCommitDataGenericFactory, this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PreCommitDataValidationListener
  }

  @Test
  def "it listens to the a pipeline event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it checks and validate data"() {
    when:
    def result = this.subject.run(this.event)

    then:
    result == this.event
    PreCommitData == result.getRunnerEventData().getPreCommitData().getClass()
    !result.getRunnerEventData().getPreCommitData().isEnabled()
    'registry.gitlab.com/wild-beavers/docker/pre-commit:custom' == result.getRunnerEventData().getPreCommitData().getContainerOptions().getImage()
    !result.getRunnerEventData().getPreCommitData().getContainerOptions().getPrivileged()
    [:] == result.getRunnerEventData().getPreCommitData().getContainerOptions().getVolumes()
  }

  @Test
  def "it sets additional mount when registry login is enabled"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [
      preCommit_containerVolumes           : [something: 'there'],
      preCommit_containerConfigJsonFilePath: '/custom/path',
      containerLogin_enabled               : true,
      containerLogin_containerRegistries   : ['something'],
      containerLogin_authfile              : '/not/default',
    ]

    result == this.event
    [something: 'there', '/not/default': '/home/podman/.docker/config.json'] == result.getRunnerEventData().getPreCommitData().getContainerOptions().getVolumes()
  }

  @Test
  def "it sets additional mount when registry login is enabled but uses default authfile if not provided"() {
    when:
    def result = this.subject.run(this.event)

    then:
    this.event.getRawConfig() >> [
      preCommit_containerVolumes           : [something: 'there'],
      preCommit_containerConfigJsonFilePath: '/custom/path',
      containerLogin_enabled               : true,
      containerLogin_containerRegistries   : ['something'],
    ]

    result == this.event
    [something: 'there', (ContainerDockerEventData.DEFAULT_AUTHFILE): '/home/podman/.docker/config.json'] == result.getRunnerEventData().getPreCommitData().getContainerOptions().getVolumes()
  }
}
