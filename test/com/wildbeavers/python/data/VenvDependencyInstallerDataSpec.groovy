package com.wildbeavers.python.data

import com.wildbeavers.container.data.ContainerOptions
import org.junit.Test
import spock.lang.Specification

class VenvDependencyInstallerDataSpec extends Specification {
  private ContainerOptions dockerOptions

  private VenvDependencyInstallerData subject

  def setup() {
    this.subject = new VenvDependencyInstallerData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == VenvDependencyInstallerData
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setSourceLocalFolderPath("localPath")
    this.subject.setRequirementFiles(["requiremensFile.txt"])
    this.subject.setRequirementEnvironments(["dev"])
    this.subject.setPackageNames(["twine"])
    this.subject.setPathEnvironmentVariable("/usr/local/bin:/bin")
    this.subject.setDestinationDockerFolderPath("/opt/venv-test")
    this.subject.setInstalled(true)
    this.subject.setContainerOptions(this.dockerOptions)

    then:
    this.subject.getSourceLocalFolderPath() == "localPath"
    this.subject.getRequirementFiles() == ["requiremensFile.txt"]
    this.subject.getRequirementEnvironments() == ["dev"]
    this.subject.getPackageNames() == ["twine"]
    this.subject.getPathEnvironmentVariable() == "/usr/local/bin:/bin"
    this.subject.getDestinationDockerFolderPath() == "/opt/venv-test"
    this.subject.isInstalled() == true
    this.subject.getContainerOptions() === this.dockerOptions
  }

  @Test
  def "it has default values"() {
    expect:
    this.subject.getDestinationDockerFolderPath() == VenvDependencyInstallerData.DESTINATION_DOCKER_FOLDER_PATH
    this.subject.getSourceLocalFolderPath() == VenvDependencyInstallerData.SOURCE_LOCAL_FOLDER_PATH
    this.subject.getRequirementFiles() == []
    this.subject.getRequirementEnvironments() == []
    this.subject.getPackageNames() == []
    !this.subject.isInstalled()
  }
}
