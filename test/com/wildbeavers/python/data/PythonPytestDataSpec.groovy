package com.wildbeavers.python.data

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.python.tests.PythonTestsType
import org.junit.Test
import spock.lang.Specification

class PythonPytestDataSpec extends Specification {
  private ContainerOptions dockerOptions = Mock()

  private PythonPytestData subject

  def setup() {
    this.subject = new PythonPytestData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonPytestData
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setAppendCommand(false)
    this.subject.setArguments(['foo': 'bar'])
    this.subject.setContainerOptions(this.dockerOptions)
    this.subject.setName('foo')

    then:
    ! this.subject.getAppendCommand()
    this.subject.getArguments() == ['foo': 'bar']
    this.subject.getContainerOptions() === this.dockerOptions
    this.subject.getDataName() == 'foo'
    this.subject.getType() == PythonTestsType.PYTEST
  }
}
