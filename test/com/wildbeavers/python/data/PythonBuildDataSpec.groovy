package com.wildbeavers.python.data


import com.wildbeavers.container.data.ContainerOptions
import org.junit.Test
import spock.lang.Specification

class PythonBuildDataSpec extends Specification {
  private ContainerOptions dockerOptions = Mock()

  private PythonBuildData subject

  def setup() {
    this.subject = new PythonBuildData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonBuildData
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setAppendCommand(false)
    this.subject.setArguments(['foo': 'bar'])
    this.subject.setContainerOptions(this.dockerOptions)

    then:
    ! this.subject.getAppendCommand()
    this.subject.getArguments() == ['foo': 'bar']
    this.subject.getContainerOptions() === this.dockerOptions
  }
}
