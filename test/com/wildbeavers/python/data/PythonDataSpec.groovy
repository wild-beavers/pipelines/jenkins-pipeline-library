package com.wildbeavers.python.data

import com.wildbeavers.python.builder.PythonBuilderType
import org.junit.Test
import spock.lang.Specification

class PythonDataSpec extends Specification {
  private HasPythonBuilderData hasPythonBuilderData = Mock()
  private HasPythonTestsData hasPythonTestsData = Mock()

  private PythonData subject

  def setup() {
    this.subject = new PythonData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonData
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setPythonBuilderData(hasPythonBuilderData)
    this.subject.setPythonBuilderType(PythonBuilderType.PYTHON_BUILD)
    this.subject.setPythonTestsDatas([hasPythonTestsData])

    then:
    this.subject.getPythonBuilderType() == PythonBuilderType.PYTHON_BUILD
    this.subject.getPythonBuilderData() === hasPythonBuilderData
    this.subject.getPythonTestsDatas() == [hasPythonTestsData]
  }

  @Test
  def "it adds a python tests data"() {
    when:
    HasPythonTestsData hasPythonTestsData2 = Mock()
    this.subject.addPythonTestsData(hasPythonTestsData)
    this.subject.addPythonTestsData(hasPythonTestsData2)

    then:
    this.subject.getPythonTestsDatas() == [hasPythonTestsData, hasPythonTestsData2]
  }

  @Test
  def "it gets a python test datas by its name"() {
    when:
    this.subject.addPythonTestsData(hasPythonTestsData)
    def result = this.subject.getPythonTestsDataByName('foo')

    then:
    result === hasPythonTestsData
    1 * hasPythonTestsData.getDataName() >> 'foo'
    noExceptionThrown()
  }

  @Test
  def "it throw returns null when the python tests data doesn't exist"() {
    when:
    this.subject.addPythonTestsData(hasPythonTestsData)
    def result = this.subject.getPythonTestsDataByName('foo')

    then:
    1 * hasPythonTestsData.getDataName() >> 'bar'
    null == result
  }
}
