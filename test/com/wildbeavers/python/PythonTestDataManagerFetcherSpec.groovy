package com.wildbeavers.python

import com.wildbeavers.composite.Fetcher
import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.python.tests.PythonTestsType
import org.junit.Test
import spock.lang.Specification

class PythonTestDataManagerFetcherSpec extends Specification {
  private FetcherDeprecated fetcher = Mock()
  private CanManagedPythonData canManagedPythonData = Mock()

  private PythonTestDataManagerFetcher subject

  def setup() {
    subject = new PythonTestDataManagerFetcher(this.fetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonTestDataManagerFetcher
  }

  @Test
  def "it registers a PythonBuildDataManagerFetcher"() {
    when:
    this.subject.register(PythonTestsType.PYTEST, this.canManagedPythonData)

    then:
    1 * this.fetcher.register(PythonTestsType.PYTEST, this.canManagedPythonData)
  }

  @Test
  def "it fetches an existing type"() {
    when:
    def result = this.subject.fetch(PythonTestsType.PYTEST)

    then:
    1 * this.fetcher.fetch(PythonTestsType.PYTEST) >> this.canManagedPythonData
    result === this.canManagedPythonData
  }
}
