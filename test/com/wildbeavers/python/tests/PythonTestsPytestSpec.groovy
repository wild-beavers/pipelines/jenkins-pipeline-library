package com.wildbeavers.python.tests

import com.wildbeavers.python.command.Pytest
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.python.data.HasPythonTestsData
import org.junit.Test
import spock.lang.Specification

class PythonTestsPytestSpec extends Specification {
  private Pytest pytest = Mock()
  private HasPythonTestsData hasPythonTestsData = Mock()
  private EmptyScmInfo emptyScmInfo = Mock()

  private PythonTestsPytest subject

  def setup() {
    this.subject = new PythonTestsPytest(this.pytest)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonTestsPytest
  }

  @Test
  def "it tests a python project with a prefix"() {
    when:
    this.subject.test(this.hasPythonTestsData)

    then:
    1 * this.hasPythonTestsData.getArguments() >> [:]
    1 * this.hasPythonTestsData.getAppendCommand() >> true
    1 * this.pytest.runWithPrefix('', [:], this.hasPythonTestsData)
    0 * this.pytest.run(_)
    noExceptionThrown()
  }

  @Test
  def "it tests a python project with a prefix"() {
    when:
    this.subject.test(this.hasPythonTestsData)

    then:
    1 * this.hasPythonTestsData.getArguments() >> [:]
    1 * this.hasPythonTestsData.getAppendCommand() >> false
    1 * this.pytest.run('', [:], this.hasPythonTestsData)
    0 * this.pytest.runWithPrefix(_)
    noExceptionThrown()
  }

  @Test
  def "it always returns true when calling canBuild"() {
    expect:
    this.subject.canTest(emptyScmInfo)
  }
}
