package com.wildbeavers.python.tests


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.data.EmptyScmInfo
import org.junit.Test
import spock.lang.Specification

class PythonTestsFetcherSpec extends Specification {
  private FetcherDeprecated fetcher = Mock()
  private PythonTestsPytest pythonTestsPytest = Mock()
  private EmptyScmInfo emptyScmInfo = Mock()

  private PythonTestsFetcher subject

  def setup() {
    subject = new PythonTestsFetcher(this.fetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonTestsFetcher
  }

  @Test
  def "it registers a PythonTests"() {
    when:
    this.subject.register(PythonTestsType.PYTEST, this.pythonTestsPytest)

    then:
    1 * this.fetcher.register(PythonTestsType.PYTEST, this.pythonTestsPytest)
  }

  @Test
  def "it fetches an existing type"() {
    when:
    def result = this.subject.fetch(PythonTestsType.PYTEST)

    then:
    1 * this.fetcher.fetch(PythonTestsType.PYTEST) >> this.pythonTestsPytest
    result === this.pythonTestsPytest
  }

  @Test
  def "it fetches an existing type that can test"() {
    when:
    this.subject.register(PythonTestsType.PYTEST, this.pythonTestsPytest)
    def result = this.subject.fetch(PythonTestsType.PYTEST, this.emptyScmInfo)

    then:
    2 * this.fetcher.fetch(PythonTestsType.PYTEST) >> this.pythonTestsPytest
    1 * this.pythonTestsPytest.canTest(emptyScmInfo) >> true
    result === this.pythonTestsPytest
  }

  @Test
  def "it returns null when it can fetch a test but this test can't test it"() {
    when:
    this.subject.register(PythonTestsType.PYTEST, this.pythonTestsPytest)
    def result = this.subject.fetch(PythonTestsType.PYTEST, this.emptyScmInfo)

    then:
    1 * this.fetcher.fetch(PythonTestsType.PYTEST) >> this.pythonTestsPytest
    1 * this.pythonTestsPytest.canTest(emptyScmInfo) >> false
    result == null
  }
}
