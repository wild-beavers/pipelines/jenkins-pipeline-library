package com.wildbeavers.python

import com.wildbeavers.composite.Fetcher
import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.python.builder.PythonBuilderType
import org.junit.Test
import spock.lang.Specification

class PythonBuildDataManagerFetcherSpec extends Specification {
  private FetcherDeprecated fetcher = Mock()
  private CanManagedPythonData canManagedPythonBuilderData = Mock()

  private PythonBuildDataManagerFetcher subject

  def setup() {
    subject = new PythonBuildDataManagerFetcher(this.fetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonBuildDataManagerFetcher
  }

  @Test
  def "it registers a PythonBuildDataManagerFetcher"() {
    when:
    this.subject.register(PythonBuilderType.PYTHON_BUILD, this.canManagedPythonBuilderData)

    then:
    1 * this.fetcher.register(PythonBuilderType.PYTHON_BUILD, this.canManagedPythonBuilderData)
  }

  @Test
  def "it fetches an existing type"() {
    when:
    def result = this.subject.fetch(PythonBuilderType.PYTHON_BUILD)

    then:
    1 * this.fetcher.fetch(PythonBuilderType.PYTHON_BUILD) >> this.canManagedPythonBuilderData
    result === this.canManagedPythonBuilderData
  }
}
