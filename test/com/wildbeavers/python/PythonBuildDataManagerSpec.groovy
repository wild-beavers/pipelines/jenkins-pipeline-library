package com.wildbeavers.python


import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.data.PythonBuildData
import org.junit.Test
import spock.lang.Specification

class PythonBuildDataManagerSpec extends Specification {
  private ContainerOptionsManager dockerOptionsManager = Mock()
  private ContainerOptions dockerOptions = Mock()

  private PythonBuildDataManager subject

  def setup() {
    this.subject = new PythonBuildDataManager(this.dockerOptionsManager)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonBuildDataManager
  }

  @Test
  def "it creates a PythonBuildData"() {
    when:
    def result = this.subject.create([
      appendCommand : false,
      arguments     : ['foo': 'bar'],
      dockerOptions : ['foo': 'bar']
    ]) as PythonBuildData

    then:
    1 * this.dockerOptionsManager.create(['foo': 'bar']) >> this.dockerOptions
    0 * this.dockerOptionsManager.create([:])
    1 * this.dockerOptions.hasImage() >> true
    0 * this.dockerOptions.setImage(_)
    result.getClass() == PythonBuildData
    result.getArguments() == ['foo': 'bar']
    ! result.getAppendCommand()
    result.getContainerOptions() === this.dockerOptions
    noExceptionThrown()
  }

  @Test
  def "it throws an exception when an config key doesn't exists "() {
    when:
    this.subject.create([foo: "bar"])

    then:
    0 * this.dockerOptionsManager.create(_)
    0 * this.dockerOptions.hasImage()
    0 * this.dockerOptions.setImage(_)
    def e = thrown(Exception)
    e.getMessage() =~ /configuration key .* is not a supported for python build./
  }

  @Test
  def "it returns a PythonBuildData with minimal dockerOption when config is empty"() {
    when:
    def result = this.subject.create([:])

    then:
    1 * this.dockerOptionsManager.create([:]) >> this.dockerOptions
    1 * this.dockerOptions.hasImage() >> false
    1 * this.dockerOptions.setImage('docker.io/library/python:latest')
    result.getClass() == PythonBuildData
    result.getArguments() == [:]
    result.getAppendCommand()
    result.getContainerOptions() === this.dockerOptions
    noExceptionThrown()
  }

  @Test
  def "it sets the docker image when the configuration doesn't specify it"() {
    when:
    def result = this.subject.create([
      dockerOptions: [foo: 'bar']
    ])

    then:
    1 * this.dockerOptionsManager.create([foo: 'bar']) >> this.dockerOptions
    0 * this.dockerOptionsManager.create([:])
    1 * this.dockerOptions.hasImage() >> false
    1 * this.dockerOptions.setImage('docker.io/library/python:latest')
    result.getClass() == PythonBuildData
    result.getArguments() == [:]
    result.getAppendCommand()
    result.getContainerOptions() === this.dockerOptions
    noExceptionThrown()
  }

  @Test
  def "it returns its type"() {
    when:
    def result = this.subject.getType()

    then:
    result == PythonBuilderType.PYTHON_BUILD
  }
}
