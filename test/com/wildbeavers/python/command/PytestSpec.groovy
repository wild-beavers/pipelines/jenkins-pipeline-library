package com.wildbeavers.python.command

import com.wildbeavers.command.AbstractCommandSetup
import com.wildbeavers.exception.CommandBuildingException
import org.junit.Test

class PytestSpec extends AbstractCommandSetup {
  Pytest subject

  def setup() {
    this.subject = new Pytest(this.optionStringFactory, this.containerRunner, this.debugger)
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == Pytest
  }

  @Test
  def "throws an exception when passed subcommand is not valid"() {
    when:
    this.subject.run('dummy', ['-arg': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Subcommand “dummy” is not valid/
  }

  @Test
  def "throws an exception when passed argument is not valid"() {
    when:
    this.subject.run('', ['-invalid': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-invalid” is not valid/
  }

  @Test
  def "it throws an exception if the command run fails for any reason"() {
    given:
    this.containerRunner.run(*_) >> {
      throw new Exception()
    }

    when:
    this.subject.run('', ['-h': true], this.dockerOptionData)

    then:
    thrown(Exception)
  }

  @Test
  def "runs a command"() {
    when:
    this.subject.run('', ['--cov': true, '-k': 'foo'], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-k', 'foo', CharSequence)
    1 * this.optionStringFactory.addOption('--cov', '')
    1 * this.containerRunner.run(this.containerOptions, '-param 1')
  }

  @Test
  def "it runs with prefixing the main command"() {
    when:
    this.subject.runWithPrefix('', ['--cov': true, '-k': 'foo'], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-k', 'foo', CharSequence)
    1 * this.optionStringFactory.addOption('--cov', '')
    1 * this.containerRunner.run(this.containerOptions, 'pytest -param 1')
  }

  def "it can show its documentation"() {
    when:
    def result = this.subject.getDocumentation()

    then:
    result =~ /COMMAND: pytest/
    result =~ /-h/
    result =~ /description: Show help message and configuration info./
  }

  def "it can show a subcommand documentation"() {
    when:
    def result = this.subject.getDocumentation('')

    then:
    result =~ /COMMAND: pytest/
  }
}
