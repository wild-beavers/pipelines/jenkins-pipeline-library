package com.wildbeavers.python.builder

import com.wildbeavers.python.command.PythonBuild
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.python.data.HasPythonBuilderData
import org.junit.Test
import spock.lang.Specification

class PythonBuildBuilderSpec extends Specification {
  private PythonBuild pythonBuild = Mock()
  private HasPythonBuilderData hasPythonBuilderData = Mock()
  private EmptyScmInfo emptyScmInfo = Mock()

  private PythonBuildBuilder subject

  def setup() {
    this.subject = new PythonBuildBuilder(this.pythonBuild)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonBuildBuilder
  }

  @Test
  def "it builds a python project with a prefix"() {
    when:
    this.subject.build(this.hasPythonBuilderData)

    then:
    1 * this.hasPythonBuilderData.getArguments() >> [:]
    1 * this.hasPythonBuilderData.getAppendCommand() >> true
    1 * this.pythonBuild.runWithPrefix('', [:], this.hasPythonBuilderData)
    0 * this.pythonBuild.run(_)
    noExceptionThrown()
  }

  @Test
  def "it builds a python project with a prefix"() {
    when:
    this.subject.build(this.hasPythonBuilderData)

    then:
    1 * this.hasPythonBuilderData.getArguments() >> [:]
    1 * this.hasPythonBuilderData.getAppendCommand() >> false
    1 * this.pythonBuild.run('', [:], this.hasPythonBuilderData)
    0 * this.pythonBuild.runWithPrefix(_)
    noExceptionThrown()
  }

  @Test
  def "it always returns true when calling canBuild"() {
    expect:
    this.subject.canBuild(emptyScmInfo)
  }
}
