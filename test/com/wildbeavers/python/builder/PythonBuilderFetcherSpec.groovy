package com.wildbeavers.python.builder


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.data.EmptyScmInfo
import org.junit.Test
import spock.lang.Specification

class PythonBuilderFetcherSpec extends Specification {
  private FetcherDeprecated fetcher = Mock()
  private PythonBuilder pythonBuilder = Mock()
  private EmptyScmInfo emptyScmInfo = Mock()

  private PythonBuilderFetcher subject

  def setup() {
    subject = new PythonBuilderFetcher(this.fetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonBuilderFetcher
  }

  @Test
  def "it registers a PythonBuilder"() {
    when:
    this.subject.register(PythonBuilderType.PYTHON_BUILD, this.pythonBuilder)

    then:
    1 * this.fetcher.register(PythonBuilderType.PYTHON_BUILD, this.pythonBuilder)
  }

  @Test
  def "it fetches an existing type"() {
    when:
    def result = this.subject.fetch(PythonBuilderType.PYTHON_BUILD)

    then:
    1 * this.fetcher.fetch(PythonBuilderType.PYTHON_BUILD) >> this.pythonBuilder
    result === this.pythonBuilder
  }

  @Test
  def "it fetches an existing type that can be build"() {
    when:
    def result = this.subject.fetch(PythonBuilderType.PYTHON_BUILD, emptyScmInfo)

    then:
    2 * this.fetcher.fetch(PythonBuilderType.PYTHON_BUILD) >> this.pythonBuilder
    1 * this.pythonBuilder.canBuild(emptyScmInfo) >> true
    result === this.pythonBuilder
  }

  @Test
  def "it returns null when it can fetch a builder but this builder can't build it"() {
    when:
    def result = this.subject.fetch(PythonBuilderType.PYTHON_BUILD, emptyScmInfo)

    then:
    1 * this.fetcher.fetch(PythonBuilderType.PYTHON_BUILD) >> this.pythonBuilder
    1 * this.pythonBuilder.canBuild(emptyScmInfo) >> false
    result == null
  }
}
