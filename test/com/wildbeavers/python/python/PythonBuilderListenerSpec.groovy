package com.wildbeavers.python.python

import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.python.builder.PythonBuilder
import com.wildbeavers.python.builder.PythonBuilderFetcher
import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.listener.PythonBuilderListener
import org.junit.Test
import spock.lang.Specification

class PythonBuilderListenerSpec extends Specification {
  private PythonBuilderFetcher pythonBuilderFetcher = Mock()
  private Debugger debugger = Mock()
  private EmptyScmInfo emptyScmInfo = Mock()

  private RunnerEventData eventDataInterface = Mock()
  private PythonData pythonData = Mock()
  private PythonBuilder pythonBuilder = Mock()

  PythonBuilderListener subject

  def setup() {
    this.subject = new PythonBuilderListener(this.pythonBuilderFetcher, this.debugger, this.emptyScmInfo)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonBuilderListener
  }

  @Test
  def "it listens to the pipeline build event"() {
    expect:
    [RunnerEvents.BUILD] == this.subject.listenTo()
  }

  @Test
  def "it runs the build"() {
    when:
    def result = this.subject.run(this.eventDataInterface)

    then:
    1 * this.eventDataInterface.getMainEventData() >> this.pythonData
    2 * this.pythonData.getPythonBuilderType() >> PythonBuilderType.PYTHON_BUILD
    0 * this.debugger.printDebug(_)
    1 * this.pythonBuilderFetcher.fetch(PythonBuilderType.PYTHON_BUILD, this.emptyScmInfo) >> this.pythonBuilder
    1 * this.pythonBuilder.build(_)
    result == this.eventDataInterface
  }

  @Test
  def "it does nothing when no builder type are passed and display a message"() {
    when:
    def result = this.subject.run(this.eventDataInterface)

    then:
    1 * this.eventDataInterface.getMainEventData() >> this.pythonData
    1 * this.pythonData.getPythonBuilderType() >> null
    1 * this.debugger.printDebug({it =~ /No builder type found/})
    0 * this.pythonBuilderFetcher.fetch(_)
    0 * this.pythonBuilder.build(_)
    result == this.eventDataInterface
  }
}
