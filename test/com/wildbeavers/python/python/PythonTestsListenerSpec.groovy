package com.wildbeavers.python.python

import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.io.Debugger
import com.wildbeavers.python.data.HasPythonTestsData
import com.wildbeavers.python.listener.PythonTestsListener
import com.wildbeavers.python.tests.PythonTestsFetcher
import com.wildbeavers.python.tests.PythonTestsPytest
import com.wildbeavers.python.tests.PythonTestsType
import org.junit.Test
import spock.lang.Specification

class PythonTestsListenerSpec extends Specification {
  private PythonTestsFetcher pythonTestsFetcher = Mock()
  private Debugger debugger = Mock()
  private EmptyScmInfo emptyScmInfo = Mock()

  private RunnerEventData eventDataInterface = Mock()
  private PythonData pythonData = Mock()
  private HasPythonTestsData pythonTestsData = Mock()
  private PythonTestsPytest pythonTestsPytest = Mock()

  PythonTestsListener subject

  def setup() {
    this.subject = new PythonTestsListener(this.pythonTestsFetcher, this.debugger, this.emptyScmInfo)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PythonTestsListener
  }

  @Test
  def "it listens to the pipeline build event"() {
    expect:
    [RunnerEvents.TEST_UNIT] == this.subject.listenTo()
  }

  @Test
  def "it runs the tests"() {
    when:
    def result = this.subject.run(this.eventDataInterface)

    then:
    1 * this.eventDataInterface.getMainEventData() >> this.pythonData
    3 * this.pythonData.getPythonTestsDatas() >> [pythonTestsData]
    1 * this.pythonTestsData.getType() >> PythonTestsType.PYTEST
    0 * this.debugger.printDebug(_)
    1 * this.pythonTestsFetcher.fetch(PythonTestsType.PYTEST, this.emptyScmInfo) >> pythonTestsPytest
    1 * this.pythonTestsPytest.test(_)
    result == this.eventDataInterface
  }

  @Test
  def "it does nothing when no tests data are passed and display a message"() {
    when:
    def result = this.subject.run(this.eventDataInterface)

    then:
    1 * this.eventDataInterface.getMainEventData() >> this.pythonData
    1 * this.pythonData.getPythonTestsDatas() >> null
    1 * this.debugger.printDebug({
      it =~ /No tests configuration found/
    })
    0 * this.pythonTestsFetcher.fetch(_)
    0 * this.pythonTestsPytest.test(_)
    result == this.eventDataInterface
  }

  @Test
  def "it does nothing when no tests data are passed and display a message"() {
    when:
    def result = this.subject.run(this.eventDataInterface)

    then:
    1 * this.eventDataInterface.getMainEventData() >> this.pythonData
    2 * this.pythonData.getPythonTestsDatas() >> []
    1 * this.debugger.printDebug({
      it =~ /No tests configuration found/
    })
    0 * this.pythonTestsFetcher.fetch(_)
    0 * this.pythonTestsPytest.test(_)
    result == this.eventDataInterface
  }
}
