package com.wildbeavers.bash.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.bash.event_data.BashEventData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class BashDataValidationListenerSpec extends Specification {
  private GenericFactory<BashEventData> bashEventDataGenericFactory = Mock()
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  Map config = [:]
  private ControllerEventData controllerEventData = Spy()
  private RunnerEventData runnerEventData = Spy()

  private BashDataValidationListener subject

  def setup() {
    this.bashEventDataGenericFactory.instantiate() >> { return new BashEventData() }
    this.controllerEventData.getRawConfig() >> this.config
    this.controllerEventData.getRunnerEventData() >> this.runnerEventData

    this.subject = new BashDataValidationListener(this.bashEventDataGenericFactory, this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == BashDataValidationListener
  }

  @Test
  def "it listens to the prepare event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it validates raw input and fills event data object"() {
    when:
    def result = this.subject.run(this.controllerEventData)

    then:
    BashEventData == this.runnerEventData.getMainEventData().getClass()
    result == this.controllerEventData
  }
}
