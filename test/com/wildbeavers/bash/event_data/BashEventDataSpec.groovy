package com.wildbeavers.bash.event_data


import org.junit.Test
import spock.lang.Specification

class BashEventDataSpec extends Specification {
  private BashEventData subject

  def setup() {
    this.subject = new BashEventData()
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == BashEventData
  }
}
