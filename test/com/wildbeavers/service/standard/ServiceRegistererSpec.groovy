package com.wildbeavers.service

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.archiver.Archiver
import com.wildbeavers.archiver.ArchiverCollection
import com.wildbeavers.archiver.TarGzArchiver
import com.wildbeavers.artifact.AWSS3
import com.wildbeavers.artifact.ArtifactStore
import com.wildbeavers.artifact.ArtifactStoreCollection
import com.wildbeavers.artifact.Git
import com.wildbeavers.auto_tagger.AutoTagger
import com.wildbeavers.bash.event_data.BashEventData
import com.wildbeavers.bash.listener.BashDataValidationListener
import com.wildbeavers.butane.command.Butane
import com.wildbeavers.butane.event_data.ButaneEventData
import com.wildbeavers.butane.listener.ButaneDataValidationListener
import com.wildbeavers.butane.listener.ButaneToIgnitionListener
import com.wildbeavers.command.Inspec
import com.wildbeavers.composite.Fetcher
import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.ContainerRegistryOption
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.container.listener.*
import com.wildbeavers.credentials.CredentialLoader
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.AWSAccessKeyCredential
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UnknownSecretCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import com.wildbeavers.credentials.exposer.CredentialExposer
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer
import com.wildbeavers.credentials.exposer.impl.FileCredentialExposer
import com.wildbeavers.credentials.provider.impl.AWSSecretsManagerCredentialProvider
import com.wildbeavers.credentials.provider.impl.JenkinsCredentialProvider
import com.wildbeavers.credentials.provider.impl.JenkinsProviderAdapter
import com.wildbeavers.credentials.transformer.JenkinsCredentialDataTransformer
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.data.pre_commit.PreCommitData
import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.data_manager.ScmInfoManager
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.dependency_installer.DependencyInstallerDataManagerFetcher
import com.wildbeavers.dependency_installer.DependencyInstallerFetcher
import com.wildbeavers.deprecation.DeprecatedFunction
import com.wildbeavers.deprecation.DeprecatedMessage
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.di.IOC
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.helper.CurrentUser
import com.wildbeavers.helper.FoolProofValidationHelper
import com.wildbeavers.helper.SSHKeySetupHelper
import com.wildbeavers.io.Debugger
import com.wildbeavers.io.File
import com.wildbeavers.listener.*
import com.wildbeavers.listener.wrapper.standard.ScmCredentialWrapper
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformApplyWrapper
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformInitSSHWrapper
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformInitWrapper
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.observer.EventListenerBag
import com.wildbeavers.proxy.*
import com.wildbeavers.python.PythonBuildDataManager
import com.wildbeavers.python.PythonBuildDataManagerFetcher
import com.wildbeavers.python.PythonPytestDataManager
import com.wildbeavers.python.PythonTestDataManagerFetcher
import com.wildbeavers.python.builder.PythonBuildBuilder
import com.wildbeavers.python.builder.PythonBuilderFetcher
import com.wildbeavers.python.command.Pytest
import com.wildbeavers.python.command.PythonBuild
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.python.dependency_installer.Venv
import com.wildbeavers.python.dependency_installer.VenvDependencyInstallerDataManager
import com.wildbeavers.python.listener.PythonBuilderListener
import com.wildbeavers.python.listener.PythonTestsListener
import com.wildbeavers.python.tests.PythonTestsFetcher
import com.wildbeavers.python.tests.PythonTestsPytest
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.terraform.listener.*
import com.wildbeavers.undefined.data.UndefinedData
import org.junit.Test
import spock.lang.Specification

class ServiceRegistererSpec extends Specification {
  GroovyScriptMock context = Mock()

  @Test
  def "it can registers all classes"() {
    given:
    !ServiceRegisterer.alreadyRegistered

    when:
    ServiceRegisterer.registerAllClasses(this.context)

    then:
    ServiceRegisterer.alreadyRegistered
  }

  @Test
  def "it can registers all globals"() {
    expect:
    ServiceRegisterer.registerAllGlobals()
  }

  @Test
  def "it can init"() {
    expect:
    ServiceRegisterer.init(this.context)
    ServiceRegisterer.init(this.context)
  }

  @Test
  def "it can instantiate core framework classes: commands"() {
    expect:
    IOC.get(Butane).getClass() == Butane
    IOC.get(Inspec).getClass() == Inspec
    IOC.get(Pytest).getClass() == Pytest
    IOC.get(PythonBuild).getClass() == PythonBuild
    IOC.get(Terraform).getClass() == Terraform
  }

  @Test
  def "it can instantiate core framework classes: composite"() {
    expect:
    IOC.get(Fetcher).getClass() == Fetcher
    IOC.get(FetcherDeprecated).getClass() == FetcherDeprecated
  }

  @Test
  def "it can instantiate core framework classes: data"() {
    expect:
    IOC.getFactory(ArtifactData).getClass() == GenericFactory<ArtifactData>
    IOC.getFactory(ArtifactData).instantiate('').getClass() == ArtifactData
    IOC.get(ArtifactData).getClass() == ArtifactData
    IOC.getFactory(ArtifactFetchingData).getClass() == GenericFactory<ArtifactFetchingData>
    IOC.getFactory(ArtifactFetchingData).instantiate('').getClass() == ArtifactFetchingData
    IOC.get(ArtifactFetchingData).getClass() == ArtifactFetchingData
    IOC.get(ArtifactPublisherData).getClass() == ArtifactPublisherData
    IOC.getFactory(ArtifactPublisherData).getClass() == GenericFactory<ArtifactPublisherData>
    IOC.getFactory(ArtifactPublisherData).instantiate('').getClass() == ArtifactPublisherData
    IOC.getFactory(ContainerLoginData).getClass() == GenericFactory<ContainerLoginData>
    IOC.getFactory(ContainerLoginData).instantiate('').getClass() == ContainerLoginData
    IOC.get(ContainerLoginData).getClass() == ContainerLoginData
    IOC.getFactory(ContainerOptions).getClass() == GenericFactory<ContainerOptions>
    IOC.get(ContainerOptions).getClass() == ContainerOptions
    IOC.getFactory(ContainerOptions).instantiate('').getClass() == ContainerOptions
    IOC.getFactory(ContainerOptions).instantiate('pre-commit').getClass() == ContainerOptions
    IOC.getFactory(ContainerOptions).instantiate('hadolint').getClass() == ContainerOptions
    IOC.getFactory(ContainerOptions).instantiate('terraform').getClass() == ContainerOptions
    IOC.getFactory(ContainerOptions).instantiate('aws').getClass() == ContainerOptions
    IOC.getFactory(ContainerOptions).instantiate('butane').getClass() == ContainerOptions
    IOC.getFactory(ContainerRegistryOption).getClass() == GenericFactory<ContainerRegistryOption>
    IOC.get(ContainerRegistryOption).getClass() == ContainerRegistryOption
    IOC.getFactory(ContainerRegistryOption).instantiate('').getClass() == ContainerRegistryOption
    IOC.getFactory(FileStandardData).getClass() == GenericFactory<FileStandardData>
    IOC.getFactory(FileStandardData).instantiate('').getClass() == FileStandardData
    IOC.get(FileStandardData).getClass() == FileStandardData
    IOC.getFactory(LinterData).getClass() == GenericFactory<LinterData>
    IOC.get(LinterData, IOC.get(ContainerOptions)).getClass() == LinterData
    IOC.getFactory(LinterData).instantiate('').getClass() == LinterData
    IOC.getFactory(LinterData).instantiate('hadolint').getClass() == LinterData
    IOC.get(PreCommitData).getClass() == PreCommitData
    IOC.getFactory(PreCommitData).getClass() == GenericFactory<PreCommitData>
    IOC.getFactory(PreCommitData).instantiate('').getClass() == PreCommitData
    IOC.get(ScmInfo).getClass() == EmptyScmInfo
  }

  @Test
  def "it can instantiate core framework classes: data manager"() {
    expect:
    IOC.get(ScmInfoManager).getClass() == ScmInfoManager
    IOC.get(ContainerOptionsManager).getClass() == ContainerOptionsManager
  }

  @Test
  def "it can instantiate core framework classes: helpers"() {
    expect:
    IOC.get(AutoTagger).getClass() == AutoTagger
    IOC.get(ContainerRunner, 'test').getClass() == ContainerRunner
    IOC.get(File).getClass() == File

    IOC.get(FoolProofValidationHelper).getClass() == FoolProofValidationHelper
    IOC.get(SSHKeySetupHelper).getClass() == SSHKeySetupHelper

    IOC.get(OptionStringFactory).getClass() == OptionStringFactory
  }

  @Test
  def "it can instantiate core framework classes: IO"() {
    expect:
    IOC.get(Debugger).getClass() == Debugger
    IOC.get(File).getClass() == File
  }

  @Test
  def "it can instantiate core framework classes: observer"() {
    expect:
    IOC.get(EventDispatcher).getClass() == EventDispatcher
    IOC.get(EventListenerBag).getClass() == EventListenerBag
  }

  @Test
  def "it can instantiate core framework classes: proxy"() {
    expect:
    IOC.get(CurrentUser).getClass() == CurrentUser
    IOC.get(EnvironmentVariableProxy).getClass() == EnvironmentVariableProxy
    IOC.get(ExecuteProxy).getClass() == ExecuteProxy
    IOC.get(FileProxy).getClass() == FileProxy
    IOC.get(ParallelProxy).getClass() == ParallelProxy
    IOC.get(PrintProxy).getClass() == PrintProxy
    IOC.get(TimeoutProxy).getClass() == TimeoutProxy
  }

  @Test
  def "it can instantiate component classes: archiver"() {
    expect:
    IOC.get(Archiver).getClass() == Archiver
    IOC.get(ArchiverCollection).getClass() == ArchiverCollection

    IOC.get(TarGzArchiver).getClass() == TarGzArchiver
  }

  @Test
  def "it can instantiate component classes: artifacts"() {
    expect:
    IOC.get(ArtifactStore).getClass() == ArtifactStore
    IOC.get(ArtifactStoreCollection).getClass() == ArtifactStoreCollection

    IOC.get(AWSS3).getClass() == AWSS3
    IOC.get(Git).getClass() == Git

    IOC.get(ArtifactFetcherDataValidationListener).getClass() == ArtifactFetcherDataValidationListener
    IOC.get(ArtifactFetcherListener).getClass() == ArtifactFetcherListener
    IOC.get(ArtifactPublisherDataValidationListener).getClass() == ArtifactPublisherDataValidationListener
    IOC.get(ArtifactPublisherListener).getClass() == ArtifactPublisherListener
  }

  @Test
  def "it can instantiate component classes: credential"() {
    expect:
    IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location')).getClass() == Secret
    IOC.getFactory(Secret).getClass() == GenericFactory
    IOC.getFactory(Secret).instantiate('unknown', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('sshPrivateKeyUsername', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('sshPassphrase', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('sshPrivateKey', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('username', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('password', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('awsAccessKeyId', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('awsAccessSecretKey', 'secret!!!').getClass() == Secret
    IOC.getFactory(Secret).instantiate('token', 'secret!!!').getClass() == Secret

    IOC.get(SshPrivateKeyCredential, IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location')), IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location')), IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location'))).getClass() == SshPrivateKeyCredential
    IOC.get(UnknownSecretCredential, IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location'))).getClass() == UnknownSecretCredential
    IOC.get(UsernamePasswordCredential, IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location')), IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location'))).getClass() == UsernamePasswordCredential
    IOC.get(AWSAccessKeyCredential, IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location')), IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location'))).getClass() == AWSAccessKeyCredential
    IOC.get(TokenCredential, IOC.get(Secret, 'test', 'test', new ExpositionLocation('some:location'))).getClass() == TokenCredential
    IOC.getFactory(SecretBag).getClass() == GenericFactory
    IOC.getFactory(SecretBag).instantiate('SshPrivateKeyCredential', IOC.getFactory(Secret).instantiate('username', 'secret!!!')).getClass() == SshPrivateKeyCredential
    IOC.getFactory(SecretBag).instantiate('UnknownSecretCredential', IOC.getFactory(Secret).instantiate('username', 'secret!!!')).getClass() == UnknownSecretCredential
    IOC.getFactory(SecretBag).instantiate('UsernamePasswordCredential', IOC.getFactory(Secret).instantiate('username', 'secret!!!'), IOC.getFactory(Secret).instantiate('password', 'secret!!!')).getClass() == UsernamePasswordCredential
    IOC.getFactory(SecretBag).instantiate('AWSAccessKeyCredential', IOC.getFactory(Secret).instantiate('username', 'secret!!!'), IOC.getFactory(Secret).instantiate('username', 'secret!!!')).getClass() == AWSAccessKeyCredential
    IOC.getFactory(SecretBag).instantiate('TokenCredential', IOC.getFactory(Secret).instantiate('username', 'secret!!!')).getClass() == TokenCredential

    IOC.get(FileCredentialExposer).getClass() == FileCredentialExposer
    IOC.get(EnvironmentVariableCredentialExposer).getClass() == EnvironmentVariableCredentialExposer
    IOC.get(CredentialExposer).getClass() == CredentialExposer

    IOC.get(JenkinsCredentialDataTransformer).getClass() == JenkinsCredentialDataTransformer

    IOC.get(AWSSecretsManagerCredentialProvider, 'aws', '123456789012', 'us-east-1').getClass() == AWSSecretsManagerCredentialProvider
    IOC.get(JenkinsCredentialProvider).getClass() == JenkinsCredentialProvider
    IOC.get(JenkinsProviderAdapter).getClass() == JenkinsProviderAdapter

    IOC.get(CredentialLoader).getClass() == CredentialLoader
  }

  @Test
  def "it can instantiate component classes: data validation"() {
    expect:
    IOC.get(DataValidator).getClass() == DataValidator
  }

  @Test
  def "it can instantiate component classes: dependency installer"() {
    expect:
    IOC.get(DependencyInstallerDataManagerFetcher).getClass() == DependencyInstallerDataManagerFetcher
    IOC.get(DependencyInstallerFetcher).getClass() == DependencyInstallerFetcher

    IOC.get(DependencyInstallerListener).getClass() == DependencyInstallerListener

    IOC.get(Venv).getClass() == Venv
    IOC.get(VenvDependencyInstallerData).getClass() == VenvDependencyInstallerData
    IOC.get(VenvDependencyInstallerDataManager).getClass() == VenvDependencyInstallerDataManager
  }

  @Test
  def "it can instantiate component classes: deprecation"() {
    expect:
    IOC.get(DeprecatedFunction).getClass() == DeprecatedFunction
    IOC.get(DeprecatedMessage).getClass() == DeprecatedMessage
  }

  @Test
  def "it can instantiate component classes: event listener wrappers"() {
    expect:
    IOC.get(ScmCredentialWrapper, ['test']).getClass() == ScmCredentialWrapper
  }

  @Test
  def "it can instantiate component classes: event listeners"() {
    expect:
    IOC.get(AutoTaggerListener).getClass() == AutoTaggerListener
    IOC.get(CleanupDelayDataValidationListener).getClass() == CleanupDelayDataValidationListener
    IOC.get(CleanupDelayListener).getClass() == CleanupDelayListener
    IOC.get(ContainerLoginDataValidationListener).getClass() == ContainerLoginDataValidationListener
    IOC.get(ContainerLoginListener).getClass() == ContainerLoginListener
    IOC.get(DebugInformationDisplayerListener).getClass() == DebugInformationDisplayerListener
    IOC.get(FileStandardDataValidationListener).getClass() == FileStandardDataValidationListener
    IOC.get(FileStandardListener).getClass() == FileStandardListener
    IOC.get(HeaderDisplayerListener).getClass() == HeaderDisplayerListener
    IOC.get(JobDataValidationListener).getClass() == JobDataValidationListener
    IOC.get(JobPropertiesListener).getClass() == JobPropertiesListener
    IOC.get(JobRuntimeDebuggingListener).getClass() == JobRuntimeDebuggingListener
    IOC.get(LinterDataValidationListener).getClass() == LinterDataValidationListener
    IOC.get(LinterListener).getClass() == LinterListener
    IOC.get(PreCommitDataValidationListener).getClass() == PreCommitDataValidationListener
    IOC.get(PreCommitListener).getClass() == PreCommitListener
    IOC.get(ScmCheckoutListener).getClass() == ScmCheckoutListener
    IOC.get(ScmDataValidationListener).getClass() == ScmDataValidationListener
    IOC.get(ScmInfoRefresherListener).getClass() == ScmInfoRefresherListener
    IOC.get(TagCheckerListener).getClass() == TagCheckerListener
    IOC.get(TagCheckerValidationListener).getClass() == TagCheckerValidationListener
    IOC.get(TestsCancellerListener).getClass() == TestsCancellerListener
    IOC.get(WorkspaceCleanupListener).getClass() == WorkspaceCleanupListener
  }

  @Test
  def "it can instantiate bash-pipeline related classes"() {
    expect:
    IOC.get(BashEventData).getClass() == BashEventData
    IOC.getFactory(BashEventData).instantiate('').getClass() == BashEventData

    IOC.get(BashDataValidationListener).getClass() == BashDataValidationListener
  }

  @Test
  def "it can instantiate butane-pipeline related classes"() {
    expect:
    IOC.get(ButaneEventData).getClass() == ButaneEventData
    IOC.getFactory(ButaneEventData).instantiate('').getClass() == ButaneEventData

    IOC.get(ButaneDataValidationListener).getClass() == ButaneDataValidationListener
    IOC.get(ButaneToIgnitionListener).getClass() == ButaneToIgnitionListener
  }

  @Test
  def "it can instantiate container-pipeline related classes"() {
    expect:
    IOC.getFactory(ContainerEventData).getClass() == GenericFactory<ContainerEventData>
    IOC.getFactory(ContainerEventData).instantiate('podman').getClass() == ContainerPodmanEventData
    IOC.getFactory(ContainerEventData).instantiate('docker').getClass() == ContainerDockerEventData

    IOC.get(ContainerAliasGuesserListener).getClass() == ContainerAliasGuesserListener
    IOC.get(ContainerBuildListener).getClass() == ContainerBuildListener
    IOC.get(ContainerDataValidationListener).getClass() == ContainerDataValidationListener
    IOC.get(ContainerInspectListener).getClass() == ContainerInspectListener
    IOC.get(ContainerPushListener).getClass() == ContainerPushListener
    IOC.get(ContainerTagListener).getClass() == ContainerTagListener
  }

  @Test
  def "it can instantiate python-pipeline related classes"() {
    expect:
    IOC.get(PythonData).getClass() == PythonData

    IOC.get(PythonBuildDataManager).getClass() == PythonBuildDataManager
    IOC.get(PythonBuildDataManagerFetcher).getClass() == PythonBuildDataManagerFetcher
    IOC.get(PythonPytestDataManager).getClass() == PythonPytestDataManager
    IOC.get(PythonTestDataManagerFetcher).getClass() == PythonTestDataManagerFetcher

    IOC.get(PythonBuildBuilder).getClass() == PythonBuildBuilder
    IOC.get(PythonBuilderFetcher).getClass() == PythonBuilderFetcher
    IOC.get(PythonTestsFetcher).getClass() == PythonTestsFetcher
    IOC.get(PythonTestsPytest).getClass() == PythonTestsPytest

    IOC.get(PythonBuilderListener).getClass() == PythonBuilderListener
    IOC.get(PythonTestsListener).getClass() == PythonTestsListener
  }

  @Test
  def "it can instantiate terraform-pipeline related classes"() {
    expect:
    IOC.get(TerraformEventData).getClass() == TerraformEventData
    IOC.getFactory(TerraformEventData).instantiate('').getClass() == TerraformEventData

    IOC.get(AWSTerraformApplyWrapper, 'test', 'test', 'test').getClass() == AWSTerraformApplyWrapper
    IOC.get(AWSTerraformInitSSHWrapper, 'test', 'test', ['test']).getClass() == AWSTerraformInitSSHWrapper
    IOC.get(AWSTerraformInitWrapper, 'test').getClass() == AWSTerraformInitWrapper

    IOC.get(TerraformApplyListener).getClass() == TerraformApplyListener
    IOC.get(TerraformArtifactCleanerListener).getClass() == TerraformArtifactCleanerListener
    IOC.get(TerraformDataValidationListener).getClass() == TerraformDataValidationListener
    IOC.get(TerraformDestroyListener).getClass() == TerraformDestroyListener
    IOC.get(TerraformDiscoverCommandTargetsListener).getClass() == TerraformDiscoverCommandTargetsListener
    IOC.get(TerraformFmtListener).getClass() == TerraformFmtListener
    IOC.get(TerraformInitListener).getClass() == TerraformInitListener
    IOC.get(TerraformValidateListener).getClass() == TerraformValidateListener
    IOC.get(TerraformWorkspaceSelectListener).getClass() == TerraformWorkspaceSelectListener
  }

  @Test
  def "it can instantiate undefined-pipeline related classes"() {
    expect:
    IOC.get(UndefinedData).getClass() == UndefinedData
  }
}
