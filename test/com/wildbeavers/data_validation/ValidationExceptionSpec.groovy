package com.wildbeavers.data_validation


import org.junit.Test
import spock.lang.Specification

class ValidationExceptionSpec extends Specification {
  private ValidationException subject

  def setup() {
    this.subject = new ValidationException('test')
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ValidationException
  }
}
