package com.wildbeavers.data_validation

import com.wildbeavers.data_validation.type.GitCloneURL
import com.wildbeavers.data_validation.type.ShallowURL
import org.junit.Test
import spock.lang.Specification

class GitCloneUrlSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new ShallowURL('https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library.git').getClass() == ShallowURL
  }

  @Test
  def "it have a description"() {
    expect:
    new GitCloneURL('git@gitlab.com:wild-beavers/pipelines/jenkins-pipeline-library.git').getDescription() == 'a string matching /^((git@([^:]+):([^/]+)/([^.]+))|(^(?:(?:(https?|git))://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\\u00a1-\\uffff](?:-)*)*(?:[a-z0-9\\u00a1-\\uffff])+)(?:\\.(?:[a-z0-9\\u00a1-\\uffff](?:-)*)*(?:[a-z0-9\\u00a1-\\uffff])+)*(?:\\.(?:[a-z0-9\\u00a1-\\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?))\\.git$/.'
  }

  @Test
  def "it throws an exception when instantiated with an incorrect value"() {
    when:
    new GitCloneURL(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^((git@([^:]+):([^/]+)/([^.]+))|(^(?:(?:(https?|git))://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\\u00a1-\\uffff](?:-)*)*(?:[a-z0-9\\u00a1-\\uffff])+)(?:\\.(?:[a-z0-9\\u00a1-\\uffff](?:-)*)*(?:[a-z0-9\\u00a1-\\uffff])+)*(?:\\.(?:[a-z0-9\\u00a1-\\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?))\\.git$/.'

    where:
    value                           || expectedException
    '//'                            || ValidationException
    ''                              || ValidationException
    'random-string'                 || ValidationException
    'toto.git'                      || ValidationException
    'https://.git'                  || ValidationException
    'git@://.git'                   || ValidationException
    'http://ok.git'                 || ValidationException
    'https://github.com/user/repo/' || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new GitCloneURL(value)

    then:
    noExceptionThrown()

    where:
    value                                                                    | _
    'https://github.com/user/repo.git'                                       | _
    'http://git.example.com/projects/myproject.git'                          | _
    'https://bitbucket.org/user/some-repository.git'                         | _
    'http://gitlab.com/user/repo.git'                                        | _
    'https://git.example.net/group/repo_to_somewhere.git'                    | _
    'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library.git' | _
    'git@git.example.com:user/repo.git'                                      | _
    'git@github.com:user/repo.git'                                           | _
    'git@gitlab.com:user/repo.git'                                           | _
    'git@bitbucket.org:user/repo.git'                                        | _
    'git://git.example.net/group/repo/to-somwhere.git'                       | _
  }
}
