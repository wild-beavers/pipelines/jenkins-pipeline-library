package com.wildbeavers.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class VersionSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new Version('1').getClass() == Version
  }

  @Test
  def "it have a description"() {
    expect:
    new Version('1.2.3-3').getDescription() == 'a string matching /^(|[\\da-z][\\da-z\\.\\-\\+]{0,63})$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new Version(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^(|[\\da-z][\\da-z\\.\\-\\+]{0,63})$/.'

    where:
    value    || expectedException
    '-1'     || ValidationException
    '323/2'  || ValidationException
    'A'      || ValidationException
    '#234'   || ValidationException
    '.23.34' || ValidationException
    'MASTER' || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new Version(value)

    then:
    noExceptionThrown()

    where:
    value                                      || expectedException
    ''                                         || false
    '0'                                        || false
    '1.2.3'                                    || false
    'a'                                        || false
    '1-3'                                      || false
    '7.0-dev1'                                 || false
    '2023.04-42'                               || false
    '1+1'                                      || false
    '2023-11-07'                               || false
    'bb4070915e9abcf4cf7b7d462351957c7930c8b9' || false
    'latest'                                   || false
    'master'                                   || false
    '2454.334.12'                              || false
  }
}
