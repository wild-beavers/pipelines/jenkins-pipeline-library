package com.wildbeavers.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class AWSRegionSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new AWSRegion('us-east-1').getClass() == AWSRegion
  }

  @Test
  def "it have a description"() {
    expect:
    new AWSRegion('us-east-1').getDescription() == 'a string matching /^(af|il|ap|ca|eu|me|sa|us|cn|us-gov|us-iso|us-isob)-(central|north|(north(?:east|west))|south|south(?:east|west)|east|west)-\\d{1}$/.'
  }

  @Test
  def "it throws an exception when instantiated with an incorrect value"() {
    when:
    new AWSRegion(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^(af|il|ap|ca|eu|me|sa|us|cn|us-gov|us-iso|us-isob)-(central|north|(north(?:east|west))|south|south(?:east|west)|east|west)-\\d{1}$/.'

    where:
    value           || expectedException
    '//'            || ValidationException
    ''              || ValidationException
    'random-string' || ValidationException
    'so-random-1'   || ValidationException
    'us-nowhere-3'  || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new AWSRegion(value)

    then:
    noExceptionThrown()

    where:
    value            | _
    'us-east-2'      | _
    'af-south-1'     | _
    'ap-northeast-3' | _
    'ap-south-2'     | _
    'ap-southeast-4' | _
    'ca-central-1'   | _
    'eu-north-2'     | _
    'il-central-1'   | _
    'me-south-1'     | _
    'us-gov-east-1'  | _
    'us-iso-east-1'  | _
    'us-isob-east-1' | _
    'cn-northwest-1' | _
  }
}
