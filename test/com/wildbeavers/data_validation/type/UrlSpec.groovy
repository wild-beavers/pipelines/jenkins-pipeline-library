package com.wildbeavers.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class UrlSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new Url('https://cran.r-project.org/web/packages/rex/vignettes/url_parsing.html').getClass() == Url
  }

  @Test
  def "it have a description"() {
    expect:
    new Url('https://mathiasbynens.be/demo/url-regex').getDescription() == 'a string matching /^(?:(?:(https?|ftp))://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)(?:\\.(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)*(?:\\.(?:[a-z0-9\u00a1-\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new Url(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^(?:(?:(https?|ftp))://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)(?:\\.(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)*(?:\\.(?:[a-z0-9\u00a1-\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?$/.'

    where:
    value                                       || expectedException
    'http:/example.com'                         || ValidationException
    'http://'                                   || ValidationException
    'http://.'                                  || ValidationException
    'http://..'                                 || ValidationException
    'http://../'                                || ValidationException
    'http://?'                                  || ValidationException
    'http://??'                                 || ValidationException
    'http://??/'                                || ValidationException
    'http://#'                                  || ValidationException
    'http://##'                                 || ValidationException
    'http://##/'                                || ValidationException
    'http://foo.bar?q=Spaces should be encoded' || ValidationException
    '//'                                        || ValidationException
    '//a'                                       || ValidationException
    '///a'                                      || ValidationException
    '///'                                       || ValidationException
    'http:///a'                                 || ValidationException
    'foo.com'                                   || ValidationException
    'rdar://1234'                               || ValidationException
    'h://test'                                  || ValidationException
    'http:// shouldfail.com'                    || ValidationException
    ':// should fail'                           || ValidationException
    'http://foo.bar/foo(bar)baz quux'           || ValidationException
    'http://-error-.invalid/'                   || ValidationException
    'http://-a.b.co'                            || ValidationException
    'http://a.b-.co'                            || ValidationException
    'http://0.0.0.0'                            || ValidationException
    'http://3628126748'                         || ValidationException
    'http://.www.foo.bar/'                      || ValidationException
    'http://www.foo.bar./'                      || ValidationException
    'http://.www.foo.bar./'                     || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new Url(value)

    then:
    noExceptionThrown()

    where:
    value                                               || expectedException
    'http://foo.com/blah_blah'                          || false
    'http://foo.com/blah_blah/'                         || false
    'http://foo.com/blah_blah_(wikipedia)'              || false
    'http://foo.com/blah_blah_(wikipedia)_(again)'      || false
    'http://www.example.com/wpstyle/?p=364'             || false
    'https://www.example.com/foo/?bar=baz&inga=42&quux' || false
    'http://✪df.ws/123'                                 || false
    'http://userid:password@example.com:8080'           || false
    'http://userid:password@example.com:8080/'          || false
    'http://userid@example.com'                         || false
    'http://userid@example.com/'                        || false
    'http://userid@example.com:8080'                    || false
    'http://userid@example.com:8080/'                   || false
    'http://userid:password@example.com'                || false
    'http://userid:password@example.com/'               || false
    'http://➡.ws/䨹'                                    || false
    'http://⌘.ws'                                       || false
    'http://⌘.ws/'                                      || false
    'http://foo.com/blah_(wikipedia)#cite-1'            || false
    'http://foo.com/blah_(wikipedia)_blah#cite-1'       || false
    'http://foo.com/unicode_(✪)_in_parens'              || false
    'http://foo.com/(something)?after=parens'           || false
    'http://☺.damowmow.com/'                            || false
    'http://code.google.com/events/#&product=browser'   || false
    'http://j.mp'                                       || false
    'http://foo.bar/?q=Test%20URL-encoded%20stuff'      || false
    'http://مثال.إختبار'                                || false
    'http://例子.测试'                                  || false
    'http://1337.net'                                   || false
    'http://a.b-c.de'                                   || false
    'http://223.255.255.254'                            || false
  }
}
