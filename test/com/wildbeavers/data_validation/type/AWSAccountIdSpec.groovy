package com.wildbeavers.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class AWSAccountIdSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new AWSAccountId('123456789012').getClass() == AWSAccountId
  }

  @Test
  def "it have a description"() {
    expect:
    new AWSAccountId('123456789012').getDescription() == 'a string matching /^[0-9]{12}$/.'
  }

  @Test
  def "it throws an exception when instantiated with an incorrect value"() {
    when:
    new AWSAccountId(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^[0-9]{12}$/.'

    where:
    value            || expectedException
    '//'             || ValidationException
    ''               || ValidationException
    'random-string'  || ValidationException
    '12345678901234' || ValidationException
    '12345678901'    || ValidationException
    '22821127.414'   || ValidationException

  }

  @Test
  def "it accepts valid values"() {
    when:
    new AWSAccountId(value)

    then:
    noExceptionThrown()

    where:
    value          | _
    '228211272412' | _
  }
}
