package com.wildbeavers.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class ForwardRelativeLinuxFullPathSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new ForwardRelativeLinuxFullPath('test').getClass() == ForwardRelativeLinuxFullPath
  }

  @Test
  def "it have a description"() {
    expect:
    new ForwardRelativeLinuxFullPath('test').getDescription() == 'a string matching /^(?!.*/$|.*\\.\\..*|^\\.$)([ -~]+)$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new ForwardRelativeLinuxFullPath(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^(?!.*/$|.*\\.\\..*|^\\.$)([ -~]+)$/.'

    where:
    value                                || expectedException   | expectedMessage
    '../behind.txt'                      || ValidationException | 'no userName'
    'somewhere/../../../tricky-backward' || ValidationException | 'no userName'
    '/finish/with/slash/'                || ValidationException | 'no user'
    '.'                                  || ValidationException | 'no user'
  }

  @Test
  def "it accepts valid values"() {
    when:
    new ForwardRelativeLinuxFullPath(value)

    then:
    noExceptionThrown()

    where:
    value                     || expectedException
    '.gitignore'              || false
    '.pre-commit-config.yaml' || false
    '/this/is/valid.txt'      || false
    './should/work'           || false
    '   will/be.trimmed'      || false
  }
}
