package com.wildbeavers.data_validation.type

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class ShallowUrlSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new ShallowURL('something://this-is-valid/somefile.html').getClass() == ShallowURL
  }

  @Test
  def "it have a description"() {
    expect:
    new ShallowURL('https://mathiasbynens.be/demo/url-regex').getDescription() == 'a string matching /^(?:[a-z]+[a-z\\d]*)://(?:[a-z\\d][^/\\n]+[a-z\\d])(?:[^@\\ \\$:;,\\n]*)$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new ShallowURL(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^(?:[a-z]+[a-z\\d]*)://(?:[a-z\\d][^/\\n]+[a-z\\d])(?:[^@\\ \\$:;,\\n]*)$/.'

    where:
    value                   || expectedException
    '//'                    || ValidationException
    '//a'                   || ValidationException
    '///a'                  || ValidationException
    '///'                   || ValidationException
    'http:///a'             || ValidationException
    'foo.com'               || ValidationException
    'http://.www.foo.bar./' || ValidationException
    ''                      || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new ShallowURL(value)

    then:
    noExceptionThrown()

    where:
    value                                                     | _
    'https://some.site/artifact'                              | _
    'http://foo.com/blah_blah'                                | _
    'ssh://git@gitlab.com:some/ecosystem-aws-games.git?ref=8' | _
    's3://s3b-somewhere/file.gz'                              | _
    'https://us-east-1.console.aws.amazon.com/'               | _
  }

  @Test
  def "it returns URI value from URL"() {
    expect:
    result == new ShallowURL(value).getPath()

    where:
    value                                                     | result
    'http://foo.com/blah_blah'                                | '/blah_blah'
    'ssh://git@gitlab.com:some/ecosystem-aws-games.git?ref=8' | '/ecosystem-aws-games.git'
    's3://s3b-somewhere/path/to/file.gz'                      | '/path/to/file.gz'
    's3://just-domain'                                        | ''
  }

  @Test
  def "it returns host value from URL"() {
    expect:
    result == new ShallowURL(value).getHost()

    where:
    value                                                   | result
    'http://foo.com/blah_blah'                              | 'foo.com'
    'https://gitlab.com:1080/ecosystem-aws-games.git?ref=8' | 'gitlab.com'
    's3://s3b-somewhere/path/to/file.gz'                    | 's3b-somewhere'
    's3://just-domain'                                      | 'just-domain'
  }

  @Test
  def "it returns scheme value from URL"() {
    expect:
    result == new ShallowURL(value).getScheme()

    where:
    value                                                     | result
    'http://foo.com/blah_blah'                                | 'http'
    'ssh://git@gitlab.com:some/ecosystem-aws-games.git?ref=8' | 'ssh'
    's3://s3b-somewhere/path/to/file.gz'                      | 's3'
    's3://just-domain'                                        | 's3'
  }


  @Test
  def "it returns query value from URL"() {
    expect:
    result == new ShallowURL(value).getQuery()

    where:
    value                                                     | result
    'http://foo.com/blah_blah'                                | null
    'ssh://git@gitlab.com:some/ecosystem-aws-games.git?ref=8' | 'ref=8'
    's3://s3b-somewhere/path/to/file.gz'                      | null
    's3://just-domain'                                        | null
  }
}
