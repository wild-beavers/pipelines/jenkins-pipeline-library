package com.wildbeavers.data_validation

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data_validation.type.ContainerTool
import org.junit.Test
import spock.lang.Specification

class DataValidatorSpec extends Specification {
  private ContainerOptions containerOptions = Spy()

  private DataValidator subject

  def setup() {
    this.subject = new DataValidator()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DataValidator
  }

  @Test
  def "it checks that a raw data if of specific class"() {
    expect:
    this.subject.validate("String", String) == "String"
    this.subject.validate(42, Integer) == 42
    this.subject.validate(this.containerOptions, ContainerOptions) == this.containerOptions
    this.subject.validate([test: 'ok'], Map<String, String>) == [test: 'ok']
    this.subject.validate([1, 2, 3], List<String>) == [1, 2, 3] // Because generic type is compile-time only
  }

  @Test
  def "it checks and re-instantiate object of type IsValidatorType"() {
    when:
    def result = this.subject.validate('docker', ContainerTool)

    then:
    result.getClass() == ContainerTool
    result.toString() == 'docker'
  }

  @Test
  def "it throws a validation error if types does not match when object is not a IsValidatorType"() {
    when:
    this.subject.validate('docker', Integer)

    then:
    def e = thrown(ValidationException)
    e =~ /“docker” has an incorrect type: not a “java.lang.Integer”/
  }

  @Test
  def "it checks a specific value from a map"() {
    expect:
    this.subject.validate([value: "String"], 'value', String) == "String"
    this.subject.validateOrNull([value: "String"], 'value', String) == "String"
    this.subject.validate([value: 42], 'value', Integer) == 42
    this.subject.validateOrNull([value: 42], 'value', Integer) == 42
    this.subject.validate([value: this.containerOptions], 'value', ContainerOptions) == this.containerOptions
    this.subject.validateOrNull([value: this.containerOptions], 'value', ContainerOptions) == this.containerOptions
    this.subject.validate([value: [test: 'ok']], 'value', Map<String, String>) == [test: 'ok']
    this.subject.validateOrNull([value: [test: 'ok']], 'value', Map<String, String>) == [test: 'ok']
    this.subject.validate([value: "String"], 'value', String) == "String"
    this.subject.validateOrNull([value: "String"], 'value', String) == "String"
    this.subject.validate([value: 'docker'], 'value', ContainerTool).toString() == 'docker'
    this.subject.validateOrNull([value: 'docker'], 'value', ContainerTool).toString() == 'docker'
  }

  @Test
  def "it returns default value when a given key is not found when validating a map"() {
    expect:
    this.subject.validate([value: "String"], 'unknown', String, 'default') == 'default'
    this.subject.validate([:], 'unknown', Integer, 1) == 1
    this.subject.validate([:], 'unknown', ContainerTool, 'podman').toString() == 'podman'
  }

  @Test
  def "it throws a validation error when no default value were specified with mandatory key when validating a map"() {
    when:
    this.subject.validate([value: "String"], 'mandatory', String)

    then:
    def e = thrown(ValidationException)
    e =~ /Oops! It seems the mandatory option “mandatory” was not defined. Please pass a \[\n  mandatory: "xxx"\n\] stanza as options map when running this pipeline./

    when:
    this.subject.validate([value: "String"], 'mandatory', String, null, ["this", "scope"])

    then:
    e = thrown(ValidationException)
    e =~ /Oops! It seems the mandatory option “mandatory” was not defined. Please pass a/
  }

  @Test
  def "it throws a runtime error when the default value itself does not match the required type"() {
    when:
    this.subject.validate([:], 'optional', ContainerTool, 'oops')

    then:
    def e = thrown(RuntimeException)
    e =~ /Default value for/
    e =~ /“oops” is not one of these possible values: docker, podman./
  }

  @Test
  def "it does return null if a null value is valid in the context"() {
    when:
    def result = this.subject.validateOrNull([value: "String"], 'non_mandatory', String,)

    then:
    result == null
  }

  @Test
  def "it throws a validation error if types does not match when validating a map"() {
    when:
    this.subject.validate([tool: 'no'], 'tool', ContainerTool)

    then:
    def e = thrown(ValidationException)
    e =~ /Oops! There was a problem with this option:/
    e =~ /“no” is not one of these possible values: docker, podman./
  }
}
