package com.wildbeavers.proxy

import com.wildbeavers.GroovyScriptMock
import org.junit.Test
import spock.lang.Specification

class ParallelProxySpec extends Specification {
  GroovyScriptMock context = Spy()

  ParallelProxy subject

  def setup() {
    this.context.parallel(_) >> 'this is stdout'

    this.subject = new ParallelProxy(this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ParallelProxy
  }

  @Test
  def "it runs multiple closures in parallel"() {
    when:
    this.subject.parallel([test: { print(1) }, test2: { print(2) }])

    then:
    1 * this.context.parallel([test: { print(1) }, test2: { print(2) }])
  }
}
