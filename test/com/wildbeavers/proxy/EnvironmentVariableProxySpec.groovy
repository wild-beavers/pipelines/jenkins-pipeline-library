package com.wildbeavers.proxy

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.exception.EnvironmentVariableProxyException
import org.junit.Test
import spock.lang.Specification

class EnvironmentVariableProxySpec extends Specification {
  GroovyScriptMock context = Spy()
  PrintProxy printProxy = Mock()

  EnvironmentVariableProxy subject

  def setup() {
    this.subject = new EnvironmentVariableProxy(this.context, this.printProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == EnvironmentVariableProxy
  }

  @Test
  def "it returns true when an environment variable exist"() {
    this.context.setEnv([foo: 'bar'])

    when:
    def result = this.subject.exist('foo')

    then:
    result
    1 * this.context.getEnv()
  }

  @Test
  def "it returns false when an environment variable doesn't exist"() {
    this.context.setEnv([foo: null])

    when:
    def result = this.subject.exist('foo')

    then:
    !result
    1 * this.context.getEnv()
  }

  @Test
  def "it throw an error when input is null"() {
    when:
    def result = this.subject.exist(null)

    then:
    def e = thrown(Exception)
    e.getMessage() =~ /environment variable name can't be null/
  }

  @Test
  def "it sets an environment variable"() {
    when:
    this.subject.set('test', 'foo')

    then:
    this.context.env.test == 'foo'
    0 * this.printProxy.println({ it =~ /test environment variable already exist. It will be overridden./ })
  }

  @Test
  def "it overrides an environment variable and display a warning"() {
    this.context.setEnv([test: 'foo'])

    when:
    this.subject.set('test', 'bar')

    then:
    this.context.env.test == 'bar'
    this.context.env.test != 'foo'
    1 * this.printProxy.println({ it =~ /test environment variable already exist. It will be overridden./ })
  }

  @Test
  def "it gets an existing environment variable"() {
    this.context.setEnv([test: 'foo'])

    when:
    def result = this.subject.get('test')

    then:
    result == 'foo'
    2 * this.context.getEnv()
  }

  @Test
  def "it throws an error when the environment variable is not set"() {
    this.context.setEnv([test: null])

    when:
    this.subject.get('test')

    then:
    1 * this.context.getEnv()
    thrown(EnvironmentVariableProxyException)
  }

  @Test
  def "it returns the environment variable fallback value if not set"() {
    this.context.setEnv([test: null])

    when:
    def result = this.subject.getIfExists('test', 'fallback')

    then:
    1 * this.context.getEnv()
    'fallback' == result
  }

  @Test
  def "it returns the environment variable value if set"() {
    this.context.setEnv([test: 'foo'])

    when:
    def result = this.subject.getIfExists('test', 'fallback')

    then:
    2 * this.context.getEnv()
    'foo' == result
  }

  @Test
  def "it throws an exception when the name is null"() {
    this.context.setEnv([test: 'foo'])

    when:
    this.subject.getIfExists(null)

    then:
    0 * this.context.getEnv()
    thrown(Exception)
  }

  @Test
  def "it unsets an existing environment variable"() {
    this.context.setEnv(['test': 'foo'])

    when:
    this.subject.unset('test')

    then:
    2 * this.context.getEnv()
    0 * this.printProxy.println(_)
    this.context.env.test == null
    this.context.env.test != 'foo'
  }

  @Test
  def "it displays a warning message when trying to unset an non-existing environment variable"() {
    this.context.setEnv([test: null])

    when:
    this.subject.unset('test')

    then:
    1 * this.context.getEnv()
    0 * this.context.setEnv([test: null])
    1 * this.printProxy.println({ it =~ /environment variable doesn't exist./ })
    this.context.env.test == null
  }

  def "it set a random named environment variable"() {
    when:
    def result = this.subject.setWithRandomName('test')

    then:
    3 * this.context.getEnv()
    this.context.getEnv().keySet()[0] == result
  }

  def "it transforms a string into a valid posix environment variable"() {
    when:
    def result = EnvironmentVariableProxy.transformStringToValidPosixEnvironmentVariable('12343ddsWEdfgd_fDSQswe_s43d122')

    then:
    result =~ /[a-zA-Z_]+[a-zA-Z0-9_]*/
    noExceptionThrown()
  }

  def "it throw an error when the input caracter set is not POSIX complient"(){
    when:
    def result = EnvironmentVariableProxy.transformStringToValidPosixEnvironmentVariable('dfrty;43]ldfg')
    then:
    def e = thrown(Exception)
    e.getMessage() =~ /A POSIX environment variable must only include/

  }
}
