package com.wildbeavers.proxy

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.data.CommandResult
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

class ExecuteProxySpec extends Specification {
  private GroovyScriptMock context = Mock()
  private Debugger debugger = Mock()

  private CommandResult commandResult = Mock()

  private ExecuteProxy subject

  def setup() {
    this.context.readFile({
      it =~ /stdout/
    }) >> 'this is stdout'
    this.context.readFile({
      it =~ /stderr/
    }) >> ' this is stderr'
    this.context.readFile({
      it =~ /statuscode/
    }) >> '0'
    this.debugger.debugVarExists() >> true

    this.subject = new ExecuteProxy(this.debugger, this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ExecuteProxy
  }

  @Test
  def "it can execute a command and displays it in debug mode"() {
    when:
    def result = this.subject.execute("test command")

    then:
    1 * this.context.sh({
      it =~ /test command/
    })
    'this is stdout' == result.getStdout()
    'this is stderr' == result.getStderr()
    0 == result.getStatusCode()
    1 * this.debugger.printDebug({
      it =~ /test command/
    })
  }

  @Test
  def "it throws an error if the internal process fails"() {
    when:
    this.subject.execute("test command")

    then:
    1 * this.context.sh({
      it =~ /test command/
    }) >> {
      throw new Exception("oops")
    }
    thrown(Exception)
  }

  @Test
  def "it throws an specific exception when the command didn't return 0"() {
    when:
    this.subject.execute("test command")

    then:
    this.context.readFile({
      it =~ /statuscode/
    }) >> '1'
    thrown(CommandExecutionException)
  }

  @Test
  def "it does not throw any error if command is executed with executeAndAllowError but simply show the error"() {
    when:
    def result = this.subject.executeAndAllowError("test command")

    then:
    this.context.readFile({
      it =~ /statuscode/
    }) >> '1'
    'this is stdout' == result.getStdout()
    'this is stderr' == result.getStderr()
    1 == result.getStatusCode()
    1 * this.debugger.printDebug({
      it =~ /test command/
    })
    1 * this.debugger.printDebug({
      it =~ /A shell execution returned an error:\n/
    })
  }

  @Test
  def "it can execute while tailing the standard output/err to the system its running in"() {
    when:
    def result = this.subject.executeWithTail("test command")

    then:
    'this is stdout' == result.getStdout()
    'this is stderr' == result.getStderr()
    0 == result.getStatusCode()
    1 * this.debugger.printDebug({
      it =~ /test command/
    })
  }

  @Test
  def "it can execute using basic CPS execute command"() {
    when:
    this.subject.basicExecute("test command")

    then:
    1 * this.context.sh([script: "#!/usr/bin/bash\n    test command\n", returnStdout: false, returnStatus: false])

    when:
    this.subject.basicExecute("test command", true)

    then:
    1 * this.context.sh([script: "test command", returnStdout: false, returnStatus: false])

    when:
    this.subject.basicExecute("test command", true, false)

    then:
    1 * this.context.sh([script: "test command", returnStdout: true, returnStatus: false])


    when:
    this.subject.basicExecute("test command", true, true, true)

    then:
    1 * this.context.sh([script: "test command", returnStdout: false, returnStatus: true])
  }

  @Test
  def "it shows the command when executing with trail"() {
    when:
    this.subject.executeWithTail("test command")

    then:
    1 * this.context.sh({ !(it =~ "tee ") })
  }
}
