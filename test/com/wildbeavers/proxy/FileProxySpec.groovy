package com.wildbeavers.proxy

import com.wildbeavers.GroovyScriptMock
import org.junit.Test
import spock.lang.Specification

class FileProxySpec extends Specification {
  GroovyScriptMock context = Spy()

  FileProxy subject

  def setup() {
    this.context.fileExists('exists') >> true
    this.context.readFile(file: 'exists') >> 'this is a content'
    this.context.readFile(file: 'pattern') >> "content 1\nanother content\n"
    this.context.fileExists(_) >> false

    this.subject = new FileProxy(this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == FileProxy
  }

  @Test
  def "it checks whether a file exists"() {
    expect:
    this.subject.exists('exists')
    !this.subject.exists('/this/path/does/not/exists')
  }

  @Test
  def "it reads a file"() {
    expect:
    this.subject.read('exists') == 'this is a content'
  }

  @Test
  def "it matches a pattern within a file"() {
    expect:
    this.subject.findInFile('pattern', ~/content/)
    this.subject.findInFile('pattern', ~/another.*/)
    !this.subject.findInFile('pattern', ~/nope/)
  }
}
