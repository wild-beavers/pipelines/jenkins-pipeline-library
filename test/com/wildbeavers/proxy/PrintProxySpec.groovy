package com.wildbeavers.proxy

import com.wildbeavers.GroovyScriptMock
import org.junit.Test
import spock.lang.Specification

class PrintProxySpec extends Specification {
  private GroovyScriptMock context = Mock()

  PrintProxy subject

  def setup() {
    this.subject = new PrintProxy(this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PrintProxy
  }

  @Test
  def "it can print"() {
    when:
    this.subject.print(12)
    this.subject.print("ok")

    then:
    1 * this.context.print({ it =~ /12/ })
    1 * this.context.print({ it =~ /ok/ })
  }

  @Test
  def "it can print with line return"() {
    when:
    this.subject.println(12)
    this.subject.println("ok")

    then:
    1 * this.context.println({ it =~ /12/ })
    1 * this.context.println({ it =~ /ok/ })
  }
}
