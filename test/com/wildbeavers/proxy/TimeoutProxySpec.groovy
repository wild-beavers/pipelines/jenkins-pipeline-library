package com.wildbeavers.proxy

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.data_validation.type.TimeUnit
import org.junit.Test
import spock.lang.Specification

class TimeoutProxySpec extends Specification {
  private GroovyScriptMock context = Spy()

  private PrintProxy printProxy = Mock()

  TimeoutProxy subject

  def setup() {
    this.subject = new TimeoutProxy(this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TimeoutProxy
  }

  @Test
  def "it can set a timeout"() {
    when:
    this.subject.timeout(12, new TimeUnit('MINUTES'), { this.printProxy.print('ok!!') })

    then:
    1 * this.context.timeout([time: 12, unit: 'MINUTES'], _)
    1 * this.printProxy.print('ok!!')
  }
}
