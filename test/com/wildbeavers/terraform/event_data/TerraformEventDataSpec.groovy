package com.wildbeavers.terraform.event_data

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.decorator.WildMap
import org.junit.Test
import spock.lang.Specification

class TerraformEventDataSpec extends Specification {
  private ContainerOptions containerOptions = Spy()
  private ContainerOptions containerOptionsAlt = Spy()

  private TerraformEventData subject

  def setup() {
    this.subject = new TerraformEventData(this.containerOptions)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformEventData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getCommandTargets() == TerraformEventData.DEFAULT_COMMAND_TARGETS
    !this.subject.isDeployment()
    this.subject.getWorkspace() == ''
    this.subject.getExamplesDirectory() == TerraformEventData.DEFAULT_EXAMPLE_DIRECTORY
    this.subject.isFmtEnabled()
    this.subject.isValidateEnabled()
    this.subject.isIdempotencyCheckEnabled()
    this.subject.getValidateOptions() == [:] as WildMap
    this.subject.getFmtOptions() == [:] as WildMap
    this.subject.getInitOptions() == [:] as WildMap
    this.subject.getPlanOptions() == [:] as WildMap
    this.subject.getApplyOptions() == [:] as WildMap
    this.subject.getDestroyOptions() == [:] as WildMap
    this.subject.getContainerOptions() == this.containerOptions
    this.subject.getSensitiveResources() == []
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setCommandTargets(['command', 'target'])
    this.subject.setIsDeployment(true)
    this.subject.setWorkspace('workspace')
    this.subject.setExamplesDirectory('ex')
    this.subject.setFmtEnabled(false)
    this.subject.setValidateEnabled(false)
    this.subject.setIdempotencyCheckEnabled(false)
    this.subject.setValidateOptions([test: 'validate'] as WildMap)
    this.subject.setFmtOptions([test: 'fmt'] as WildMap)
    this.subject.setInitOptions([test: 'init'] as WildMap)
    this.subject.setPlanOptions([test: 'plan'] as WildMap)
    this.subject.setApplyOptions([test: 'apply'] as WildMap)
    this.subject.setDestroyOptions([test: 'destroy'] as WildMap)
    this.subject.setContainerOptions(this.containerOptionsAlt)
    this.subject.setSensitiveResources(['some_sensitive', 'another_sensitive'])

    then:
    ['command', 'target'] == this.subject.getCommandTargets()
    this.subject.isDeployment()
    'workspace' == this.subject.getWorkspace()
    'ex' == this.subject.getExamplesDirectory()
    !this.subject.isFmtEnabled()
    !this.subject.isValidateEnabled()
    !this.subject.isIdempotencyCheckEnabled()
    [test: 'validate'] == this.subject.getValidateOptions()
    [test: 'fmt'] == this.subject.getFmtOptions()
    [test: 'init'] == this.subject.getInitOptions()
    [test: 'plan'] == this.subject.getPlanOptions()
    [test: 'apply'] == this.subject.getApplyOptions()
    [test: 'destroy'] == this.subject.getDestroyOptions()
    this.containerOptionsAlt == this.subject.getContainerOptions()
    ['some_sensitive', 'another_sensitive'] == this.subject.getSensitiveResources()
  }
}
