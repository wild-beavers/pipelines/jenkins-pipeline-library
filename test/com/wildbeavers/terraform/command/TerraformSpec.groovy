package com.wildbeavers.terraform.command

import com.wildbeavers.command.AbstractCommandSetup
import com.wildbeavers.exception.CommandBuildingException
import org.junit.Test

class TerraformSpec extends AbstractCommandSetup {
  Terraform subject

  def setup() {
    this.subject = new Terraform(this.optionStringFactory, this.containerRunner, this.debugger)
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == Terraform
  }

  @Test
  def "throws an exception when passed subcommand is not valid"() {
    when:
    this.subject.run('dummy', ['-arg': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Subcommand “dummy” is not valid/
  }

  @Test
  def "throws an exception when passed sub-argument is not valid"() {
    when:
    this.subject.run('init', ['-invalid': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-invalid” is not valid/
  }

  @Test
  def "throws an exception when passed sub-argument does not have valid value"() {
    when:
    this.subject.run('init', ['-lockfile': 'invalid'], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-lockfile” does not have a valid value \(given: “invalid”\)/
  }

  @Test
  def "it throws an exception if the command run fails for any reason"() {
    given:
    this.containerRunner.run(*_) >> {
      throw new Exception()
    }

    when:
    this.subject.run('init', ['-chdir': 'ok', '-upgrade': true], this.dockerOptionData)

    then:
    thrown(Exception)
  }

  @Test
  def "runs a command"() {
    when:
    this.subject.run('init', ['-chdir': 'ok', '-upgrade': true, '-no-color': true], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-chdir', 'ok', CharSequence)
    1 * this.optionStringFactory.addOption('-upgrade', true, Boolean)
    1 * this.optionStringFactory.addOption('-no-color', '')
    1 * this.containerRunner.run(this.containerOptions, '-param 1 init -param 1')
  }

  @Test
  def "it runs with prefixing the main command"() {
    when:
    this.subject.runWithPrefix('init', ['-chdir': 'ok', '-upgrade': true, '-no-color': true], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-chdir', 'ok', CharSequence)
    1 * this.optionStringFactory.addOption('-upgrade', true, Boolean)
    1 * this.optionStringFactory.addOption('-no-color', '')
    1 * this.containerRunner.run(this.containerOptions, 'terraform -param 1 init -param 1')
  }

  def "it can show its documentation"() {
    when:
    def result = this.subject.getDocumentation()

    then:
    result =~ /COMMAND: terraform/
    result =~ /-help/
    result =~ /description: Show this help output/
    result =~ /-chdir/
  }

  def "it can show a subcommand documentation"() {
    when:
    def result = this.subject.getDocumentation('init')

    then:
    result =~ /COMMAND: terraform init/
    result =~ /-backend/
    result =~ /description: Configure the backend for this confi/
    result =~ /-get/
  }
}
