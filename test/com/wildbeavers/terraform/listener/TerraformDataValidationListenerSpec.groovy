package com.wildbeavers.terraform.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.terraform.event_data.TerraformEventData
import org.junit.Test
import spock.lang.Specification

class TerraformDataValidationListenerSpec extends Specification {
  private GenericFactory<TerraformEventData> terraformEventDataGenericFactory = Mock()
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  Map config = [
    terraform_enableValidate    : false,
    terraform_testInitOptions   : new WildMap([test: true]),
    terraform_deployInitOptions : new WildMap([deploy: true]),
    terraform_workspace         : 'test',
    terraform_containerFqdn     : 'fqdn.com/path',
    terraform_containerImageName: 'image',
    terraform_containerImageTag : 'latest',
  ]
  Map configDeployment = [
    terraform_isDeployment      : true,
    terraform_enableValidate    : false,
    terraform_testInitOptions   : new WildMap([test: true]),
    terraform_deployInitOptions : new WildMap([deploy: true]),
    terraform_workspace         : 'test',
    terraform_containerFqdn     : 'fqdn.com/path',
    terraform_containerImageName: 'image',
    terraform_containerImageTag : 'latest',
    terraform_sensitiveResources: ['sensitive'],
  ]
  private ControllerEventData controllerEventData = Spy()
  private RunnerEventData runnerEventData = Spy()
  private FileStandardData fileStandardData = Spy()

  private TerraformDataValidationListener subject

  def setup() {
    this.runnerEventData.isFileStandardIgnoreDefault() >> false
    this.runnerEventData.getFileStandardData() >> this.fileStandardData
    this.terraformEventDataGenericFactory.instantiate() >> { return new TerraformEventData(new ContainerOptions()) }
    this.controllerEventData.getRawConfig() >> this.config
    this.controllerEventData.getRunnerEventData() >> this.runnerEventData

    this.subject = new TerraformDataValidationListener(this.terraformEventDataGenericFactory, this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformDataValidationListener
  }

  @Test
  def "it listens to the prepare event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it validates raw input and fills event data object"() {
    when:
    this.subject.run(this.controllerEventData)

    then:
    !this.runnerEventData.getMainEventData().isValidateEnabled()
    this.runnerEventData.getMainEventData().getInitOptions() == [test: true]
    this.runnerEventData.getMainEventData().getWorkspace() == 'test'
    this.runnerEventData.getMainEventData().getContainerOptions().getImage() == 'fqdn.com/path/image:latest'
    this.runnerEventData.getMainEventData().getSensitiveResources() == []
    !this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('deploy.tf')
    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('.gitignore')
    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('main.tf')
  }

  @Test
  def "it validates raw input and fills event data object for a deployment"() {
    when:
    this.subject.run(this.controllerEventData)

    then:
    this.controllerEventData.getRawConfig() >> this.configDeployment

    !this.runnerEventData.getMainEventData().isValidateEnabled()
    this.runnerEventData.getMainEventData().getInitOptions() == [deploy: true]
    this.runnerEventData.getMainEventData().getSensitiveResources() == ['sensitive']
    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('deploy.tf')
    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('.gitignore')
    !this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('main.tf')
  }

  @Test
  def "it does not add any mandatory files when the ignore toggle is true"() {
    when:
    this.subject.run(this.controllerEventData)

    then:
    this.runnerEventData.isFileStandardIgnoreDefault() >> true

    [] == this.runnerEventData.getFileStandardData().getMandatoryFiles()
  }
}
