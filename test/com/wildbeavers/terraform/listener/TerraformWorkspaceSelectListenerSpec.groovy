package com.wildbeavers.terraform.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData
import org.junit.Test
import spock.lang.Specification

import static com.wildbeavers.command.AbstractCommand.COMMAND_TARGET

class TerraformWorkspaceSelectListenerSpec extends Specification {
  Debugger debugger = Mock()
  Terraform terraform = Mock()

  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()

  TerraformWorkspaceSelectListener subject

  def setup() {
    this.terraformEventData.isDeployment() >> true
    this.terraformEventData.getPlanOptions() >> [option: true]
    this.terraformEventData.getWorkspace() >> 'test'
    this.debugger.debugVarExists() >> false
    this.runnerEventData.getMainEventData() >> this.terraformEventData

    this.subject = new TerraformWorkspaceSelectListener(this.terraform, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformWorkspaceSelectListener
  }

  @Test
  def "it listens to the post-compile event"() {
    expect:
    [RunnerEvents.POST_COMPILE] == this.subject.listenTo()
  }

  @Test
  def "it does not run if data has no workspace set"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.terraformEventData.getWorkspace() >> ''

    !result
  }

  @Test
  def "it does not run if current revision is not a deployment"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> false

    !result
  }

  @Test
  def "it selects terraform workspace"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('workspace select', [
      (COMMAND_TARGET): '-or-create test'
    ], this.terraformEventData)
    0 * this.terraform.run(*_)
  }

  @Test
  def "it lists terraform workspaces in debug mode"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.debugger.debugVarExists() >> true

    1 * this.terraform.run('workspace list', [:], this.terraformEventData)
  }
}
