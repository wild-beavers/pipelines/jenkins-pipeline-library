package com.wildbeavers.terraform.listener

import com.wildbeavers.data.CommandResult
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.io.Debugger
import com.wildbeavers.terraform.event_data.TerraformEventData
import org.junit.Test
import spock.lang.Specification

class TerraformDiscoverCommandTargetsListenerSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()
  private Debugger debugger = Mock()

  private CommandResult lsCommandResult = Mock()
  private RunnerEventData runnerEventData = Spy()
  private TerraformEventData terraformEventData = Spy()

  TerraformDiscoverCommandTargetsListener subject

  def setup() {
    this.terraformEventData.isDeployment() >> false
    this.terraformEventData.getExamplesDirectory() >> 'tests'
    this.lsCommandResult.getStdout() >> 'target1 target2'
    this.executeProxy.execute(*_) >> this.lsCommandResult
    this.runnerEventData.getMainEventData() >> this.terraformEventData

    this.subject = new TerraformDiscoverCommandTargetsListener(this.executeProxy, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformDiscoverCommandTargetsListener
  }

  @Test
  def "it listens to the post prepare event"() {
    expect:
    [RunnerEvents.POST_PREPARE] == this.subject.listenTo()
  }

  @Test
  def "it does not run if project is deployment"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> true

    !result
  }

  @Test
  def "it does not run if project is not a deployment"() {
    expect:
    this.subject.shouldRun(this.runnerEventData)
  }

  @Test
  def "it adds all the command targets when project is a module"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraformEventData.addCommandTarget('target1')
    1 * this.terraformEventData.addCommandTarget('target2')
    ['target1', 'target2'] == this.terraformEventData.getCommandTargets()
  }

  @Test
  def "it adds no command targets when project is a module and the examples directory is empty"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.lsCommandResult.getStdout() >> ''

    ['.'] == this.terraformEventData.getCommandTargets()
  }

  @Test
  def "it sets dot command target if an script error occurs during the detection"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.executeProxy.execute(*_) >> {
      throw new CommandExecutionException('oops')
    }
    1 * this.terraformEventData.setCommandTargets(TerraformEventData.DEFAULT_COMMAND_TARGETS)
    ['.'] == this.terraformEventData.getCommandTargets()
  }

  @Test
  def "it returns unmodified event data"() {
    expect:
    this.runnerEventData == this.subject.run(this.runnerEventData)
  }
}
