package com.wildbeavers.terraform.listener


import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.terraform.event_data.TerraformEventData
import org.junit.Test
import spock.lang.Specification

class TerraformArtifactCleanerListenerSpec extends Specification {
  ExecuteProxy executeProxy = Mock()

  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()

  TerraformArtifactCleanerListener subject

  def setup() {
    this.executeProxy.execute(_) >> null
    this.terraformEventData.getCommandTargets() >> ['target', 'target2']
    this.runnerEventData.getMainEventData() >> this.terraformEventData
    this.subject = new TerraformArtifactCleanerListener(this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformArtifactCleanerListener
  }

  @Test
  def "it listens to the post cleanup event"() {
    expect:
    [RunnerEvents.POST_CLEANUP] == this.subject.listenTo()
  }

  @Test
  def "it removes the plan out file"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> true
    1 * this.executeProxy.execute({
      it.contains('rm') && it.contains(TerraformApplyListener.PLAN_OUT_FILE)
    }) >> null
  }

  @Test
  def "it removes the test statefile if project is not a deployment"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> false
    1 * executeProxy.execute({
      it.contains('rm') && it.contains("target/${TerraformApplyListener.DEFAULT_STATE_FILE}") && it.contains("target2/${TerraformApplyListener.DEFAULT_STATE_FILE}")
    })
  }
}
