package com.wildbeavers.terraform.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.FileProxy
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData
import org.junit.Test
import spock.lang.Specification

class TerraformDestroyListenerSpec extends Specification {
  FileProxy fileProxy = Mock()
  Terraform terraform = Mock()
  Debugger debugger = Mock()
  GroovyScriptMock context = Spy()

  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()

  TerraformDestroyListener subject

  def setup() {
    this.subject = new TerraformDestroyListener(this.fileProxy, this.terraform, this.debugger, this.context)
    this.terraformEventData.getCommandTargets() >> ['target', 'target2']
    this.terraformEventData.getDestroyOptions() >> [option: true]
    this.terraformEventData.isDeployment() >> false
    this.fileProxy.exists({
      it.contains('target/')
    }) >> true
    this.fileProxy.exists({
      it.contains('target2/')
    }) >> false
    this.runnerEventData.getMainEventData() >> this.terraformEventData
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformDestroyListener
  }

  @Test
  def "it listens to the cleanup event"() {
    expect:
    [RunnerEvents.CLEANUP] == this.subject.listenTo()
  }

  @Test
  def "it does not run if project is a deployment"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> true

    !result
  }

  @Test
  def "it does not terraform destroy specific targets without a statefile"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    0 * this.terraform.run('destroy', [
      '-chdir'       : 'target2',
      '-auto-approve': true,
      option         : true
    ], this.terraformEventData)
    this.debugger.printDebug({
      it.contains('Skipping terraform destroy')
    })
  }

  @Test
  def "it does not run anything if there are no command targets"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.getCommandTargets() >> []
    0 * this.terraform.run(*_)
  }

  @Test
  def "it runs terraform command"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('destroy', [
      '-chdir'       : 'target',
      '-auto-approve': true,
      '-refresh'     : 'false',
      option         : true
    ], this.terraformEventData)
  }

  @Test
  def "it tries destroy once again if first attempt failed and archive statefile"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('destroy', [
      '-chdir'       : 'target',
      '-auto-approve': true,
      '-refresh'     : 'false',
      'option'       : true
    ], this.terraformEventData) >> {
      throw new Exception('failed to destroy')
    }

    1 * this.context.archiveArtifacts(_)
    1 * this.terraform.run('destroy', [
      '-chdir'       : 'target',
      '-auto-approve': true,
      'option'       : true
    ], this.terraformEventData)
  }

  @Test
  def "it runs terraform command with overridden options"() {
    when:
    this.terraformEventData.getDestroyOptions() >> ['-random': 'new']
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('destroy', [
      '-chdir'       : 'target',
      '-auto-approve': true,
      '-refresh'     : 'false',
      '-random'      : 'new',
    ], this.terraformEventData)
  }

  @Test
  def "it returns unmodified event data"() {
    expect:
    this.runnerEventData == this.subject.run(this.runnerEventData)
  }
}
