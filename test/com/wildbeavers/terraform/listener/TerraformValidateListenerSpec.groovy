package com.wildbeavers.terraform.listener

import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.terraform.listener.TerraformValidateListener
import org.junit.Test
import spock.lang.Specification

class TerraformValidateListenerSpec extends Specification {
  Terraform terraform = Mock()

  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()

  TerraformValidateListener subject

  def setup() {
    this.terraformEventData.getCommandTargets() >> ['target', 'target2']
    this.terraformEventData.getValidateOptions() >> [option: true]
    this.terraformEventData.isValidateEnabled() >> true
    this.runnerEventData.getMainEventData() >> this.terraformEventData

    this.subject = new TerraformValidateListener(this.terraform)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformValidateListener
  }

  @Test
  def "it listens to the pre test-unit event"() {
    expect:
    [RunnerEvents.PRE_TEST_UNIT] == this.subject.listenTo()
  }

  @Test
  def "it does not run when validatedEnabled is false"() {
    when:
    def result = this.subject.shouldRun(this.runnerEventData)

    then:
    this.terraformEventData.isValidateEnabled() >> false

    !result
  }

  @Test
  def "it does not run validates if there are no command targets"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.getCommandTargets() >> []

    0 * this.terraform.run(*_)
    result == this.runnerEventData
  }

  @Test
  def "it runs terraform command"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('validate', [
      '-chdir': 'target',
      option  : true
    ], this.terraformEventData)
    1 * this.terraform.run('validate', [
      '-chdir': 'target2',
      option  : true
    ], this.terraformEventData)
  }

  @Test
  def "it runs terraform command with overridden options"() {
    when:
    this.terraformEventData.getValidateOptions() >> ['-what': 'new']
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('validate', [
      '-chdir': 'target',
      '-what' : 'new',
    ], this.terraformEventData)
    1 * this.terraform.run('validate', [
      '-chdir': 'target2',
      '-what' : 'new',
    ], this.terraformEventData)
  }

  @Test
  def "it returns unmodified event data"() {
    expect:
    this.runnerEventData == this.subject.run(this.runnerEventData)
  }
}
