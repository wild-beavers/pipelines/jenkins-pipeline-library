package com.wildbeavers.terraform.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.FoolProofValidationHelper
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.PrintProxy
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData
import org.junit.Test
import spock.lang.Specification

class TerraformApplyListenerSpec extends Specification {
  GroovyScriptMock context = Mock()
  Terraform terraform = Mock()
  FoolProofValidationHelper foolProofValidationHelper = Mock()
  Debugger debugger = Mock()
  ScmInfo scmInfo = Mock()
  PrintProxy printProxy = Mock()

  CommandResult planNoChangeCommandResult = Mock()
  CommandResult planChangeCommandResult = Mock()
  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()

  TerraformApplyListener subject

  def setup() {
    this.subject = new TerraformApplyListener(this.context, this.terraform, this.foolProofValidationHelper, this.debugger, this.scmInfo, this.printProxy)
    this.terraformEventData.getCommandTargets() >> ['target']
    this.terraformEventData.isDeployment() >> false
    this.terraformEventData.isIdempotencyCheckEnabled() >> true
    this.terraformEventData.getSensitiveResources() >> ['some_critical_thing']
    this.runnerEventData.getMainEventData() >> this.terraformEventData
    this.scmInfo.isPublishable() >> true
    this.planNoChangeCommandResult.getStdout() >> 'Infrastructure is up-to-date'
    this.planChangeCommandResult.getStdout() >> '7 changes'
    this.terraform.run('plan', *_) >> this.planChangeCommandResult
    this.terraformEventData.getApplyOptions() >> [option: true]
    this.terraformEventData.getPlanOptions() >> [option: false]
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformApplyListener
  }

  @Test
  def "it listens to the deploy event"() {
    expect:
    [RunnerEvents.DEPLOY] == this.subject.listenTo()
  }

  @Test
  def "it can refresh dependencies"() {
    when:
    IOC.registerSingleton(ScmInfo.class.getName(), {
      return new EmptyScmInfo()
    })

    this.subject.refreshDependencies()

    then:
    true
  }

  @Test
  def "it does not run apply when project is a deployment merge request"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> true
    this.scmInfo.isPublishable() >> false
    0 * this.foolProofValidationHelper.foolProof()
    0 * this.terraform.run('apply', *_)
    (1.._) * this.debugger.printDebug(*_)
  }

  @Test
  def "it does not run apply when project have no changes (module)"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('plan', *_) >> this.planNoChangeCommandResult

    this.terraformEventData.isDeployment() >> false
    this.scmInfo.isPublishable() >> false
    0 * this.foolProofValidationHelper.foolProof()
    (1.._) * this.debugger.printDebug(*_)
  }

  @Test
  def "it does not run apply when project have no changes (deployment)"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('plan', *_) >> this.planNoChangeCommandResult

    this.terraformEventData.isDeployment() >> true
    this.scmInfo.isPublishable() >> true
    0 * this.foolProofValidationHelper.foolProof()
    (1.._) * this.debugger.printDebug(*_)
  }

  @Test
  def "it does not run second plan when idempotency check is disabled"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isIdempotencyCheckEnabled() >> false
    this.terraformEventData.isDeployment() >> false
    1 * this.terraform.run('plan', *_) >> this.planChangeCommandResult
  }

  @Test
  def "it runs terraform command, plan, apply, replay plan for modules"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    2 * this.terraform.run('plan', [
      '-out'  : TerraformApplyListener.PLAN_OUT_FILE,
      '-chdir': 'target',
      option  : false
    ], this.terraformEventData) >>> [this.planChangeCommandResult, this.planNoChangeCommandResult]
    1 * this.terraform.run('apply', [
      '-chdir'     : 'target',
      commandTarget: TerraformApplyListener.PLAN_OUT_FILE,
      option       : true
    ], this.terraformEventData)
    0 * this.foolProofValidationHelper.foolProof()
    1 * this.debugger.printDebug(*_)
    0 * this.context.error(*_)
  }

  @Test
  def "it runs without a second plan when project is deployment"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('plan', *_) >> this.planChangeCommandResult

    1 * this.terraform.run('apply', [
      '-chdir'     : 'target',
      commandTarget: TerraformApplyListener.PLAN_OUT_FILE,
      option       : true
    ], this.terraformEventData)
    this.terraformEventData.isIdempotencyCheckEnabled() >> true
    this.terraformEventData.isDeployment() >> true
    1 * this.foolProofValidationHelper.foolProof()
  }


  @Test
  def "it errors out if plan replay contains changes"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraform.run('plan', *_) >> this.planChangeCommandResult
    1 * this.context.error({
      it.contains('Idempotency ERROR')
    })
  }

  @Test
  def "it saves the statefile if an error occurs during the apply"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> false
    this.terraform.run('apply', *_) >> {
      throw new Exception('message')
    }
    1 * this.context.archiveArtifacts(*_)
    def e = thrown(Exception)
    e =~ /message/

    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> true
    this.terraform.run('apply', *_) >> {
      throw new Exception('message')
    }
    0 * this.context.archiveArtifacts(*_)
    def e2 = thrown(Exception)
    e2 =~ /message/
  }

  @Test
  def "it shows a warning message when sensitive resources are to be destroyed"() {
    when:
    this.runnerEventData == this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.isDeployment() >> true
    this.planChangeCommandResult.getStdout() >> "test.some_critical_thing.this will be destroyed!"

    1 * this.printProxy.println({ it =~ /At least one resource planned for destruction/ })
    1 * this.printProxy.println({ it =~ /- some_critical_thing/ })
  }

  @Test
  def "it returns unmodified event data"() {
    expect:
    this.runnerEventData == this.subject.run(this.runnerEventData)
  }
}
