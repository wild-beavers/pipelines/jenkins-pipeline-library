package com.wildbeavers.terraform.listener

import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.terraform.listener.TerraformFmtListener
import org.junit.Test
import spock.lang.Specification

class TerraformFmtListenerSpec extends Specification {
  Terraform terraform = Mock()
  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()
  TerraformFmtListener subject

  def setup() {
    this.subject = new TerraformFmtListener(this.terraform)
    this.terraformEventData.getCommandTargets() >> ['target']
    this.terraformEventData.getFmtOptions() >> [option: true]
    this.runnerEventData.getMainEventData() >> this.terraformEventData
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformFmtListener
  }

  @Test
  def "it listens to the lint event"() {
    expect:
    [RunnerEvents.LINT] == this.subject.listenTo()
  }

  @Test
  def "it runs terraform command"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('fmt', [
      '-check'     : true,
      '-recursive' : true,
      commandTarget: '.',
      option       : true
    ], this.terraformEventData)
  }

  @Test
  def "it runs terraform command with overridden options"() {
    when:
    this.terraformEventData.getFmtOptions() >> ['-recursive': false]
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('fmt', [
      '-check'     : true,
      '-recursive' : false,
      commandTarget: '.',
    ], this.terraformEventData)
  }

  @Test
  def "it returns unmodified event data"() {
    expect:
    this.runnerEventData == this.subject.run(this.runnerEventData)
  }
}
