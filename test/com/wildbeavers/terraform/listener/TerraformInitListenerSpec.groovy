package com.wildbeavers.terraform.listener

import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.terraform.listener.TerraformInitListener
import org.junit.Test
import spock.lang.Specification

class TerraformInitListenerSpec extends Specification {
  Terraform terraform = Mock()

  RunnerEventData runnerEventData = Mock()
  TerraformEventData terraformEventData = Mock()

  TerraformInitListener subject

  def setup() {
    this.terraformEventData.getCommandTargets() >> ['target', 'target2']
    this.terraformEventData.getInitOptions() >> [option: true]
    this.runnerEventData.getMainEventData() >> this.terraformEventData

    this.subject = new TerraformInitListener(this.terraform)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TerraformInitListener
  }

  @Test
  def "it listens to the compile event"() {
    expect:
    [RunnerEvents.COMPILE] == this.subject.listenTo()
  }

  @Test
  def "it runs terraform command"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    1 * this.terraform.run('init', [
      '-chdir': 'target',
      option  : true
    ], this.terraformEventData)
    1 * this.terraform.run('init', [
      '-chdir': 'target2',
      option  : true
    ], this.terraformEventData)
  }

  @Test
  def "it does not run anything if there are no command targets"() {
    when:
    this.subject.run(this.runnerEventData)

    then:
    this.terraformEventData.getCommandTargets() >> []
    0 * this.terraform.run('init', *_)
  }

  @Test
  def "it runs terraform command with overridden options"() {
    when:
    this.terraformEventData.getInitOptions() >> ['-chdir': 'new']
    this.subject.run(this.runnerEventData)

    then:
    2 * this.terraform.run('init', [
      '-chdir': 'new',
    ], this.terraformEventData)
  }

  @Test
  def "it returns unmodified event data"() {
    expect:
    this.runnerEventData == this.subject.run(this.runnerEventData)
  }
}
