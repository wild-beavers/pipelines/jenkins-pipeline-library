package com.wildbeavers.artifact

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.File
import com.wildbeavers.proxy.FileProxy
import org.junit.Test
import spock.lang.Specification

class AWSS3Spec extends Specification {
  private FileProxy fileProxy = Mock()
  private File file = Mock()
  private ContainerRunner containerRunner = Mock()
  private GenericFactory<ContainerOptions> containerOptionsGenericFactory = Mock()
  private DataValidator dataValidator = Mock()

  private ArtifactData artifactData = Spy()
  private ContainerOptions defaultContainerOptions = Spy()
  private ContainerOptions customDockerOption = Spy()

  AWSS3 subject

  def setup() {
    this.containerOptionsGenericFactory.instantiate(AWSS3.DEFAULT_ACCESSOR_NAME) >> { this.defaultContainerOptions }
    this.containerOptionsGenericFactory.instantiate('custom') >> { this.customDockerOption }
    this.fileProxy.exists(*_) >> true
    this.file.guessMimeType(*_) >> 'some/mime'
    this.artifactData.getUrlStrict() >> new ShallowURL('s3://example/path/artifact.tar.gz')

    this.subject = new AWSS3(this.fileProxy, this.file, this.containerRunner, this.containerOptionsGenericFactory, this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == AWSS3
  }

  @Test
  def "it validates the URL given to upload artifacts"() {
    when:
    this.subject.uploadArtifact('some url', 'whatever', this.artifactData)

    then:
    1 * this.dataValidator.validate('some url', ShallowURL)
  }

  @Test
  def "it throws exceptions when the local path file to upload not found"() {
    when:
    this.subject.uploadArtifact('https://example.com/path', 'not_exists', this.artifactData)

    then:
    this.fileProxy.exists('not_exists') >> false

    thrown(FileNotFoundException)
  }

  @Test
  def "it supports only URL with s3 as scheme or an AWS host"() {
    when:
    def result = this.subject.support(this.artifactData)

    then:
    this.artifactData.getUrlStrict() >> new ShallowURL(url)

    result == expected

    where:
    url                                                | expected
    's3://test/something'                             || true
    'https://s3.amazonaws.com/some/resource/art.json' || true
    'https://example.org/something'                   || false
    'https://us-east-1.console.aws.amazon.com/'       || false
  }

  @Test
  def "it uploads an artifact"() {
    when:
    this.subject.uploadArtifact('s3://testbucket239573/file', "test", this.artifactData)

    then:
    1 * this.containerRunner.run(this.defaultContainerOptions, 's3 cp --content-type "some/mime" "test" "s3://testbucket239573/file"')
  }

  @Test
  def "it executes pre-upload actions"() {
    when:
    this.subject.preUploadActions('s3://testbucket239573/file', 'test', this.artifactData)

    then:
    1 * this.containerRunner.pull(this.defaultContainerOptions)
  }

  @Test
  def "it downloads an artifact with custom container data"() {
    when:
    this.subject.downloadArtifact("test", this.artifactData)

    then:
    this.artifactData.getAccessorName() >> 'custom'

    this.artifactData.getUrl() >> 's3://testbucket239573/file'
    1 * this.containerRunner.run(this.customDockerOption, 's3 cp "s3://testbucket239573/file" "test"')
  }

  @Test
  def "it executes pre-download actions"() {
    when:
    this.subject.preDownloadActions('localPath', this.artifactData)

    then:
    1 * this.containerRunner.pull(this.defaultContainerOptions)
  }

  @Test
  def "it executes pre-download pull with custom image"() {
    when:
    this.subject.preDownloadActions('localPath', this.artifactData)

    then:

    this.artifactData.getAccessorName() >> 'custom'
    1 * this.containerRunner.pull(this.customDockerOption)
  }
}
