package com.wildbeavers.artifact

import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.exception.ConfigurationException
import org.junit.Test
import spock.lang.Specification

class ArtifactStoreCollectionSpec extends Specification {
  private IsArtifactStore artifactStore1 = Mock()
  private IsArtifactStore artifactStore2 = Mock()
  private IsArtifactStore artifactStore3 = Mock()
  private ArtifactData artifactData1 = Mock()
  private ArtifactData artifactData2 = Mock()
  private ArtifactData artifactData3 = Mock()

  private ArtifactStoreCollection subject

  def setup() {
    this.artifactStore1.support(this.artifactData1) >> true
    this.artifactStore2.support(this.artifactData2) >> true
    this.artifactStore3.support(this.artifactData2) >> true

    this.subject = new ArtifactStoreCollection()
    this.subject.addArtifactStore(this.artifactStore1)
    this.subject.addArtifactStore(this.artifactStore2)
    this.subject.addArtifactStore(this.artifactStore3)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactStoreCollection
  }

  @Test
  def "it can store artifactStores, only once."() {
    when:
    this.subject.addArtifactStore(this.artifactStore2)

    then:
    this.subject.getAllArtifactStores().contains(this.artifactStore1)
    this.subject.getAllArtifactStores().contains(this.artifactStore2)
    this.subject.getAllArtifactStores().contains(this.artifactStore3)
    this.subject.getAllArtifactStores().size() == 3
  }

  @Test
  def "it can retrieve the first artifact store that support specific data"() {
    when:
    def result = this.subject.getSupportedArtifactStore(this.artifactData1)
    def result2 = this.subject.getSupportedArtifactStore(this.artifactData2)

    then:
    result == this.artifactStore1
    result2 == this.artifactStore2
  }

  @Test
  def "it throws an exception when trying to get an artifactStore with unsupported data"() {
    when:
    this.subject.getSupportedArtifactStore(this.artifactData3)

    then:
    def error = thrown(ConfigurationException)
    error =~ /No artifact store supports artifact data/
  }
}
