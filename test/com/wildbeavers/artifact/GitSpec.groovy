package com.wildbeavers.artifact


import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.proxy.ExecuteProxy
import org.junit.Test
import spock.lang.Specification

class GitSpec extends Specification {
  private ExecuteProxy executeProxy = Mock()

  private ArtifactData artifactData = Spy()

  private Git subject

  def setup() {
    this.artifactData.getUrlStrict() >> new ShallowURL('https://gitlab.com/test/tech/jenkins-pipeline-library.git')

    this.subject = new Git(this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == Git
  }

  @Test
  def "it supports only URL with git as scheme or an git repository"() {
    when:
    def result = this.subject.support(this.artifactData)

    then:
    this.artifactData.getUrlStrict() >> new ShallowURL(url)

    result == expected

    where:
    url                                                          | expected
    'git://example.org/some/repo'                               || true
    'https://gitlab.com/test/tech/jenkins-pipeline-library.git' || true
    's3://maybe-im-git'                                         || false
    'https://us-east-1.console.aws.amazon.com/'                 || false
  }

  @Test
  def "it push changed to a git repository (NOT IMPLEMENTED)"() {
    when:
    this.subject.uploadArtifact('some url', 'whatever', this.artifactData)

    then:
    thrown(RuntimeException)
  }

  @Test
  def "it executes pre clone actions"() {
    expect:
    this.subject.preDownloadActions('localPath', this.artifactData)
  }

  @Test
  def "it executes pre push actions"() {
    expect:
    this.subject.preUploadActions('destination', 'localPath', this.artifactData)
  }

  @Test
  def "it clones a repository"() {
    when:
    this.subject.downloadArtifact('test', this.artifactData)

    then:
    this.artifactData.getUrl() >> 'https://gitlab.com/test/tech/jenkins-pipeline-library.git'


    1 * this.executeProxy.executeWithTail('git clone "https://gitlab.com/test/tech/jenkins-pipeline-library.git" "test"')
  }

  @Test
  def "it clones a specific version of a repository"() {
    when:
    this.subject.downloadArtifact('wgf', this.artifactData)

    then:
    this.artifactData.getUrl() >> 'https://bitbucket.org/GEE_Media/python-wg-federation.git'
    this.artifactData.getVersion() >> 'my_version'

    1 * this.executeProxy.executeWithTail('git clone --depth 1 --branch my_version "https://bitbucket.org/GEE_Media/python-wg-federation.git" "wgf"')
  }
}
