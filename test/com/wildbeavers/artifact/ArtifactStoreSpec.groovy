package com.wildbeavers.artifact

import com.wildbeavers.data.artifact.ArtifactData
import org.junit.Test
import spock.lang.Specification

class ArtifactStoreSpec extends Specification {
  private ArtifactStoreCollection artifactStoreCollection = Mock()

  private IsArtifactStore artifactStore = Mock()
  private ArtifactData artifactData = Spy()

  private ArtifactStore subject

  def setup() {
    this.artifactStoreCollection.getSupportedArtifactStore(this.artifactData) >> this.artifactStore
    this.subject = new ArtifactStore(this.artifactStoreCollection)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactStore
  }

  @Test
  def "it executes pre-download actions"() {
    when:
    this.subject.preDownloadActions('localPath', this.artifactData)

    then:
    1 * this.artifactStore.preDownloadActions('localPath', this.artifactData)
  }

  @Test
  def "it can download an artifact using the correct artifact store implementation"() {
    when:
    this.subject.uploadArtifact('destURL', 'path', this.artifactData)

    then:
    1 * this.artifactStore.uploadArtifact('destURL', 'path', this.artifactData)
  }

  @Test
  def "it executes pre-upload actions"() {
    when:
    this.subject.preUploadActions('destURL', 'localPath', this.artifactData)

    then:
    1 * this.artifactStore.preUploadActions('destURL', 'localPath', this.artifactData)
  }

  @Test
  def "it can upload an artifact using the correct artifact store implementation"() {
    when:
    this.subject.downloadArtifact('path', this.artifactData)

    then:
    1 * this.artifactStore.downloadArtifact('path', this.artifactData)
  }
}
