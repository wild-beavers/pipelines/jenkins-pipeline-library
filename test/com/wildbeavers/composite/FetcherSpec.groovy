package com.wildbeavers.composite


import com.wildbeavers.exception.FetcherTypeNotFoundException
import org.junit.Test
import spock.lang.Specification

class FetchableImpl implements Fetchable<Integer, String> {
  @Override
  Boolean support(Integer integer) {
    return 10 == integer
  }

  @Override
  String getId() {
    return 'test'
  }
}

class FetcherSpec extends Specification {
  private Fetchable<Integer, String> fetchable = Mock()
  private Fetchable<Integer, String> fetchable2 = Mock()

  private Fetcher<Fetchable<Integer, String>, Integer, String> subject

  def setup() {
    this.fetchable.getId() >> 'fetchable1'
    this.fetchable2.getId() >> 'fetchable2'

    this.fetchable.support(10) >> true
    this.fetchable.support(20) >> true
    this.fetchable2.support(20) >> true

    this.subject = new Fetcher()
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == Fetcher
  }

  @Test
  def "it can register an fetchables"() {
    expect:
    this.subject.register(this.fetchable)
    this.subject.register(this.fetchable2)
  }

  @Test
  def "it tries to fetch supporting fetchable: returns nothing when nothing is registered"() {
    expect:
    this.subject.fetchTrySupporting(50) == [] as LinkedHashSet
  }

  @Test
  def "it throws an exception when trying no fetchable support the given subject"() {
    when:
    this.subject.register(this.fetchable)
    this.subject.register(this.fetchable2)
    this.subject.fetchSupporting(50)

    then:
    def e = thrown(FetcherTypeNotFoundException)
    e =~ /“50” was provided to fetch a supported interface com.wildbeavers.composite.Fetchable, but no registered objects support it/

    when:
    this.subject.fetchFirstSupporting(50)

    then:
    e = thrown(FetcherTypeNotFoundException)
    e =~ /“50” was provided to fetch a supported interface com.wildbeavers.composite.Fetchable, but no registered objects support it/
  }

  @Test
  def "it fetches supported fetchable"() {
    expect:
    this.subject.register(this.fetchable)
    this.subject.register(this.fetchable2)
    this.subject.fetchSupporting(10) == [this.fetchable] as LinkedHashSet
    this.subject.fetchFirstSupporting(10) == this.fetchable
    this.subject.fetchSupporting(20) == [this.fetchable, this.fetchable2] as LinkedHashSet
    this.subject.fetchFirstSupporting(20) == this.fetchable
  }

  @Test
  def "it fetches supported fetchable, using ID to refine disciminant"() {
    expect:
    this.subject.register(this.fetchable)
    this.subject.register(this.fetchable2)
    this.subject.fetchSupporting(20, 'fetchable2') == [this.fetchable2] as LinkedHashSet
    this.subject.fetchTrySupporting(20, 'not_existing') == [] as LinkedHashSet
  }

  @Test
  def "it returns all fetchable in order of registration"() {
    expect:
    this.subject.register(this.fetchable2)
    this.subject.register(this.fetchable)
    this.subject.fetchAll()[0] == this.fetchable2
    this.subject.fetchAll()[1] == this.fetchable
  }
}
