package com.wildbeavers.dependency_installer

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.python.dependency_installer.VenvDependencyInstallerDataManager
import org.junit.Test
import spock.lang.Specification

class VenvDependencyInstallerDataManagerSpec extends Specification {
  private ContainerOptionsManager dockerOptionsManager = Mock()
  private ContainerOptions dockerOptions = Mock()

  private VenvDependencyInstallerDataManager subject

  def setup() {
    this.subject = new VenvDependencyInstallerDataManager(this.dockerOptionsManager)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == VenvDependencyInstallerDataManager
  }

  @Test
  def "it create a DockerOptions"() {
    when:
    def result = this.subject.create([
      requirementFiles           : ["requirements.txt"],
      requirementEnvironments    : [".[dev]"],
      packageNames               : ["twine"],
      destinationDockerFolderPath: "/opt/destination",
      sourceLocalFolderPath      : "sourceFolder",
      pathEnvironmentVariable    : "PATH2",
      dockerOptions              : [:]
    ]) as VenvDependencyInstallerData

    then:
    1 * this.dockerOptionsManager.create([:]) >> this.dockerOptions
    result.getClass() == VenvDependencyInstallerData
    result.getRequirementFiles() == ["requirements.txt"]
    result.getRequirementEnvironments() == [".[dev]"]
    result.getPackageNames() == ["twine"]
    result.getPathEnvironmentVariable() == "PATH2"
    result.getContainerOptions() === this.dockerOptions
    noExceptionThrown()
  }

  @Test
  def "it throw an exception when an config key doesn't exists "() {
    when:
    this.subject.create([foo: "bar"])

    then:
    0 * this.dockerOptionsManager.create(_)
    def e = thrown(Exception)
    e.getMessage() =~ /configuration key .* is not supported for venv/
  }

  @Test
  def "it return an empty VenvDependencyInstaller when config is empty"() {
    when:
    def result = this.subject.create([:])

    then:
    0 * this.dockerOptionsManager.create(_)
    result.getClass() == VenvDependencyInstallerData
    noExceptionThrown()
  }
}
