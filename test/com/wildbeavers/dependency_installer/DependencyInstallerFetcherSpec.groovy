package com.wildbeavers.dependency_installer


import com.wildbeavers.composite.FetcherDeprecated
import org.junit.Test
import spock.lang.Specification

class DependencyInstallerFetcherSpec extends Specification {
  private FetcherDeprecated fetcher = Mock()
  private DependencyInstaller dependencyInstaller = Mock()

  private DependencyInstallerFetcher subject

  def setup() {
    subject = new DependencyInstallerFetcher(this.fetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DependencyInstallerFetcher
  }

  @Test
  def "it registers a DependencyInstaller"() {
    when:
    this.subject.register(dependencyInstaller)

    then:
    1 * this.dependencyInstaller.getType() >> DependencyInstallerType.VENV
    1 * this.fetcher.register(DependencyInstallerType.VENV, this.dependencyInstaller)
  }

  @Test
  def "it fetches an existing type"() {
    when:
    def result = this.subject.fetch(DependencyInstallerType.VENV)

    then:
    this.fetcher.fetch(DependencyInstallerType.VENV) >> this.dependencyInstaller
    result === this.dependencyInstaller
  }
}
