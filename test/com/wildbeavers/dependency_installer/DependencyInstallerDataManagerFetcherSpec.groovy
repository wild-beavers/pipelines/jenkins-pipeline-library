package com.wildbeavers.dependency_installer


import com.wildbeavers.composite.FetcherDeprecated
import org.junit.Test
import spock.lang.Specification

class DependencyInstallerDataManagerFetcherSpec extends Specification {
  private FetcherDeprecated fetcher = Mock()
  private CanManagedDependencyInstallerData dependencyInstallerDataManager = Mock()

  private DependencyInstallerDataManagerFetcher subject

  def setup() {
    subject = new DependencyInstallerDataManagerFetcher(this.fetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == DependencyInstallerDataManagerFetcher
  }

  @Test
  def "it registers a DependencyInstallerDataManager"() {
    when:
    this.subject.register(dependencyInstallerDataManager)

    then:
    1 * this.dependencyInstallerDataManager.getType() >> DependencyInstallerType.VENV
    1 * this.fetcher.register(DependencyInstallerType.VENV, this.dependencyInstallerDataManager)
  }

  @Test
  def "it fetches an existing type"() {
    when:
    def result = this.subject.fetch(DependencyInstallerType.VENV)

    then:
    1 * this.fetcher.fetch(DependencyInstallerType.VENV) >> this.dependencyInstallerDataManager
    result === this.dependencyInstallerDataManager
  }
}
