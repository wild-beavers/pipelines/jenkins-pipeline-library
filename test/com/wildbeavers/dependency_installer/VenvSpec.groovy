package com.wildbeavers.dependency_installer


import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.io.Debugger
import com.wildbeavers.python.dependency_installer.Venv
import org.junit.Test
import spock.lang.Specification

class VenvSpec extends Specification {
  private Debugger debugger = Mock()
  private ContainerRunner containerRunner = Mock()
  private ExecuteProxy executeProxy = Mock()
  private VenvDependencyInstallerData venvDependencyInstaller = Mock()
  private ContainerOptions dockerOptions = Mock()
  private ContainerOptions dockerOptionsSpy = Spy()

  private Venv subject

  def setup() {
    this.dockerOptions.getEnvironmentVariables() >> [:]
    this.dockerOptions.getVolumes() >> [:]
    subject = new Venv(this.debugger, this.containerRunner, this.executeProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == Venv
  }

  @Test
  def "it install dependencies"() {
    when:
    this.subject.install(this.venvDependencyInstaller)

    then:
    1 * this.venvDependencyInstaller.isInstalled()
    7 * this.venvDependencyInstaller.getContainerOptions() >> this.dockerOptions
    1 * this.dockerOptions.hasImage() >> true
    2 * this.venvDependencyInstaller.getSourceLocalFolderPath() >> 'localFolderPath'
    3 * this.venvDependencyInstaller.getDestinationDockerFolderPath() >> 'dockerFolderPath'
    1 * this.containerRunner.run(_, "python -m venv install dockerFolderPath", true)
    1 * this.venvDependencyInstaller.getRequirementEnvironments() >> ["dev"]
    1 * this.containerRunner.run(_,  "python -m pip install -e dev")
    1 * this.venvDependencyInstaller.getRequirementFiles() >> ["requirements.txt"]
    1 * this.containerRunner.run(_, "python -m pip install -r requirements.txt")
    1 * this.venvDependencyInstaller.getPackageNames() >> ["twine"]
    1 * this.containerRunner.run(_,  "python -m pip install twine")
    1 * this.venvDependencyInstaller.setInstalled(true)
    0 * this.debugger.print(_)
  }

  @Test
  def "it uninstall venv dependency"() {
    when:
    this.subject.uninstall(this.venvDependencyInstaller)

    then:
    1 * this.venvDependencyInstaller.isInstalled() >> true
    1 * this.venvDependencyInstaller.getSourceLocalFolderPath() >> 'localFolderPath'
    1 * this.executeProxy.execute("rm -rf localFolderPath")
    1 * this.venvDependencyInstaller.setInstalled(false)
    0 * this.debugger.print(_)
  }

  @Test
  def "it does nothing and display a message when venv dependency already install"() {
    when:
    this.subject.install(this.venvDependencyInstaller)

    then:
    1 * this.venvDependencyInstaller.isInstalled() >> true
    0 * this.venvDependencyInstaller.getContainerOptions() >> this.dockerOptions
    1 * this.venvDependencyInstaller.getSourceLocalFolderPath() >> 'localFolderPath'
    0 * this.venvDependencyInstaller.getDestinationDockerFolderPath() >> 'dockerFolderPath'
    0 * this.containerRunner.run(_, "python -m venv install dockerFolderPath", true)
    0 * this.venvDependencyInstaller.getRequirementEnvironments() >> ["dev"]
    0 * this.containerRunner.run(_, "python -m pip install -e dev")
    0 * this.venvDependencyInstaller.getRequirementFiles() >> ["requirements.txt"]
    0 * this.containerRunner.run(_, "python -m pip install -r requirements.txt")
    0 * this.venvDependencyInstaller.getPackageNames() >> ["twine"]
    0 * this.containerRunner.run(_, "python -m pip install twine")
    0 * this.venvDependencyInstaller.setInstalled(true)
    1 * this.debugger.print({ it =~ /venv is already installed at location/ })
  }

  @Test
  def "it does nothing and display a message when venv is not installed"() {
    when:
    this.subject.uninstall(this.venvDependencyInstaller)

    then:
    1 * this.venvDependencyInstaller.isInstalled() >> false
    0 * this.venvDependencyInstaller.getSourceLocalFolderPath() >> 'localFolderPath'
    0 * this.executeProxy.execute("rm -rf localFolderPath")
    0 * this.venvDependencyInstaller.setInstalled(false)
    1 * this.debugger.print({ it =~ /No .* installed/ })
  }

  @Test
  def "it alter docker option when activate venv"() {
    when:
    Venv.activateVenvInDockerOptions(this.dockerOptionsSpy, 'localFolderPath', 'dockerFolderPath')

    then:
    1 * this.dockerOptionsSpy.addVolumes([localFolderPath: "dockerFolderPath"])
    2 * this.dockerOptionsSpy.getEnvironmentVariables() >> [(Venv.PATH_ENVIRONMENT_VARIABLE_NAME): "dockerFolderPath/bin:/bin"]
    this.dockerOptionsSpy.getVolumes() == [localFolderPath: "dockerFolderPath"]
  }

  @Test
  def "it alter the existing PATH environment variable docker option when activate venv"() {
    when:
    this.dockerOptionsSpy.setEnvironmentVariables([(Venv.PATH_ENVIRONMENT_VARIABLE_NAME): "foo"])
    Venv.activateVenvInDockerOptions(this.dockerOptionsSpy, 'localFolderPath', 'dockerFolderPath')

    then:
    1 * this.dockerOptionsSpy.addVolumes([localFolderPath: "dockerFolderPath"])
    2 * this.dockerOptionsSpy.getEnvironmentVariables() >> [(Venv.PATH_ENVIRONMENT_VARIABLE_NAME): "dockerFolderPath/bin:foo"]
    this.dockerOptionsSpy.getVolumes() == [localFolderPath: "dockerFolderPath"]
  }

  @Test
  def "it alter docker option when deactivate venv"() {
    when:
    Venv.activateVenvInDockerOptions(this.dockerOptionsSpy, 'localFolderPath', 'dockerFolderPath')
    Venv.deactivateVenvInDockerOptions(this.dockerOptionsSpy, 'localFolderPath')

    then:
    this.dockerOptionsSpy.getVolumes() == [:]
    this.dockerOptionsSpy.getEnvironmentVariables() == [:]
  }
}
