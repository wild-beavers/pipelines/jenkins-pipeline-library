package com.wildbeavers.factory

import com.wildbeavers.data.OptionString
import com.wildbeavers.exception.OptionStringException
import org.apache.groovy.json.internal.LazyMap
import org.junit.Test
import spock.lang.Specification

class OptionStringFactorySpec extends Specification {
  OptionStringFactory subject

  def setup() {
    this.subject = new OptionStringFactory()
    this.subject.createOptionString('=')
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == OptionStringFactory
  }

  @Test
  def "it instantiate option string"() {
    when:
    def result = this.subject.createOptionString(' ')

    then:
    result instanceof OptionString
  }

  @Test
  def "it has getter for option string"() {
    when:
    def result = this.subject.getOptionString()

    then:
    result instanceof OptionString
  }

  @Test
  def "it adds options to OptionString"() {
    when:
    this.subject.addOption('-one', 'value')
    this.subject.addOption('-two', true, Boolean)
    this.subject.addOption('-three', ['tab1', 'tab2'], ArrayList)
    this.subject.addCommandTarget('"the target"')

    then:
    this.subject.toString() == '-one=value -two=true -three=tab1 -three=tab2 "the target"'
  }

  @Test
  def "it throws an error if optionValue is not serializable"() {
    when:
    def notSerializable = new LazyMap()
    this.subject.addOption('-one', notSerializable)

    then:
    thrown(OptionStringException)
  }

  @Test
  def "it throws an error if option string is empty because createOptionString was not called"() {
    when:
    def altSubject = new OptionStringFactory()
    altSubject.addOption('-one', 'value')

    then:
    def e = thrown(OptionStringException)
    e =~ /“createOptionString” was not called/
  }

  @Test
  def "it throws an error if option value does not match the expected type"() {
    when:
    this.subject.addOption('-one', 'value', Boolean)

    then:
    thrown(OptionStringException)
  }
}
