package com.wildbeavers.credentials.exposer.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import org.junit.Test
import spock.lang.Specification

class EnvironmentVariableCredentialExposerSpec extends Specification {
  private Debugger debugger = Mock()
  private EnvironmentVariableProxy environmentVariableProxy = Mock()

  private Secret secret = Mock()
  private ExpositionLocation existLocation = Mock()
  private ExpositionLocation newLocation = Mock()
  private ExpositionLocation alreadyExposed = Mock()

  private EnvironmentVariableCredentialExposer subject

  def setup() {
    this.existLocation.getExpositionType() >> EnvironmentVariableCredentialExposer.ALIAS
    this.existLocation.getLocation() >> '/path/exists'
    this.newLocation.getExpositionType() >> EnvironmentVariableCredentialExposer.ALIAS
    this.newLocation.getLocation() >> '/path/to/somewhere'
    this.alreadyExposed.getExpositionType() >> EnvironmentVariableCredentialExposer.ALIAS
    this.alreadyExposed.getLocation() >> '/path/somewhere/exposed'
    this.secret.getContent() >> 'the_secret'

    this.subject = new EnvironmentVariableCredentialExposer(this.environmentVariableProxy, this.debugger)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == EnvironmentVariableCredentialExposer
  }

  @Test
  def "it return its alias"() {
    expect:
    this.subject.getId() == EnvironmentVariableCredentialExposer.ALIAS
  }

  @Test
  def "it only supports a specific type of exposition location"() {
    expect:
    this.subject.support(new ExpositionLocation('environmentVariable:/some/place'))
    !this.subject.support(new ExpositionLocation('file:/some/place'))
  }

  @Test
  def "it does nothing if the exposition location is not supported"() {
    when:
    this.subject.expose(this.secret, new ExpositionLocation('nope:location'))
    this.subject.conceal(this.secret, new ExpositionLocation('nope:location'))

    then:
    0 * this.environmentVariableProxy.set(_)
    0 * this.secret.markAsExposed(_)
    0 * this.secret.markAsConcealed(_)
  }

  @Test
  def "it does not expose if the secret is already exposed in the given location"() {
    when:
    this.subject.expose(this.secret, this.alreadyExposed)

    then:
    this.secret.isExposedInLocation(this.alreadyExposed) >> true

    0 * this.environmentVariableProxy.execute(_)
    0 * this.secret.markAsExposed(_)
  }

  @Test
  def "it exposes the secret in an environment variable"() {
    when:
    this.subject.expose(this.secret, this.newLocation)

    then:
    1 * this.environmentVariableProxy.set('/path/to/somewhere', 'the_secret')
    1 * this.secret.markAsExposed(this.newLocation)
    noExceptionThrown()
  }

  @Test
  def "it shows a warning when trying to conceal a secret that does not exist at the location"() {
    when:
    this.subject.conceal(this.secret, this.newLocation)

    then:
    this.secret.isExposedInLocation(this.newLocation) >> true
    this.environmentVariableProxy.exist('/path/to/somewhere') >> false

    1 * this.debugger.print({ it =~ /WARNING: Unable to conceal .*/ })
    0 * this.environmentVariableProxy.unset(_)
    1 * this.secret.markAsConcealed(_)
  }

  @Test
  def "it conceals a secret"() {
    when:
    this.subject.conceal(this.secret, this.newLocation)

    then:
    this.secret.isExposedInLocation(this.newLocation) >> true
    this.environmentVariableProxy.exist('/path/to/somewhere') >> true

    1 * this.environmentVariableProxy.unset('/path/to/somewhere')
    0 * this.debugger.print(_)
    1 * this.secret.markAsConcealed(this.newLocation)
  }

  @Test
  def "it does not conceal if the secret is not exposed in the given location"() {
    when:
    this.subject.conceal(this.secret, this.newLocation)

    then:
    this.secret.isExposedInLocation(this.newLocation) >> false

    0 * this.environmentVariableProxy.unset(_)
    0 * this.secret.markAsConcealed(_)
  }
}
