package com.wildbeavers.credentials.exposer.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import com.wildbeavers.credentials.exception.SecretExpositionException
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.FileProxy
import org.junit.Test
import spock.lang.Specification

class FileCredentialExposerSpec extends Specification {
  private FileProxy fileProxy = Mock()
  private ExecuteProxy executeProxy = Mock()
  private Debugger debugger = Mock()
  private EnvironmentVariableProxy environmentVariableProxy = Mock()

  private Secret secret = Mock()
  private ExpositionLocation existLocation = Mock()
  private ExpositionLocation newLocation = Mock()
  private ExpositionLocation alreadyExposed = Mock()

  private FileCredentialExposer subject

  def setup() {
    this.existLocation.getExpositionType() >> FileCredentialExposer.ALIAS
    this.existLocation.getLocation() >> '/path/exists'
    this.newLocation.getExpositionType() >> FileCredentialExposer.ALIAS
    this.newLocation.getLocation() >> '/path/to/somewhere'
    this.alreadyExposed.getExpositionType() >> FileCredentialExposer.ALIAS
    this.alreadyExposed.getLocation() >> '/path/somewhere/exposed'
    this.fileProxy.exists(_) >> false
    this.environmentVariableProxy.setWithRandomName(_) >> 'random'

    this.subject = new FileCredentialExposer(this.fileProxy, this.executeProxy, this.debugger, this.environmentVariableProxy)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == FileCredentialExposer
  }

  @Test
  def "it return its alias"() {
    expect:
    this.subject.getId() == FileCredentialExposer.ALIAS
  }

  @Test
  def "it only supports a specific type of exposition location"() {
    expect:
    this.subject.support(new ExpositionLocation('file:/some/place'))
    !this.subject.support(new ExpositionLocation('environmentVariable:/some/place'))
  }

  @Test
  def "it throws an exception when trying to expose a secret in an existing file"() {
    when:
    this.subject.expose(this.secret, this.existLocation)

    then:
    this.fileProxy.exists('/path/exists') >> true

    def e = thrown(SecretExpositionException)
    e.getMessage() =~ /The secret “.+” is not mark as exposed, but a file already exists at “\/path\/exists”/
    0 * this.executeProxy.execute(_)
    0 * this.secret.markAsExposed(_)
  }

  @Test
  def "it does nothing if the exposition location is not supported"() {
    when:
    this.subject.expose(this.secret, new ExpositionLocation('nope:location'))
    this.subject.conceal(this.secret, new ExpositionLocation('nope:location'))

    then:
    0 * this.executeProxy.execute(_)
    0 * this.secret.markAsExposed(_)
    0 * this.secret.markAsConcealed(_)
  }

  @Test
  def "it does not expose if the secret is already exposed in the given location"() {
    when:
    this.subject.expose(this.secret, this.alreadyExposed)

    then:
    this.secret.isExposedInLocation(this.alreadyExposed) >> true

    0 * this.executeProxy.execute(_)
    0 * this.secret.markAsExposed(_)
  }

  @Test
  def "it exposes the secret in a file"() {
    when:
    this.subject.expose(this.secret, this.newLocation)

    then:
    1 * this.executeProxy.execute({ it =~ /echo\s+\$\{random\}\s*|\s*tee\s+"\/path\/to\/somewhere"/ })
    1 * this.executeProxy.execute({ it =~ /chmod\s+[0-7]{3}\s+"\/path\/to\/somewhere"/ })
    1 * this.secret.markAsExposed(this.newLocation)
    noExceptionThrown()
  }

  @Test
  def "it shows a warning when trying to conceal a secret that does not exist at the location"() {
    when:
    this.subject.conceal(this.secret, this.newLocation)

    then:
    this.fileProxy.exists('/path/to/somewhere') >> false
    this.secret.isExposedInLocation(this.newLocation) >> true

    1 * this.debugger.print({ it =~ /WARNING: Unable to conceal .*/ })
    0 * this.executeProxy.execute(_)
    1 * this.secret.markAsConcealed(this.newLocation)
  }

  @Test
  def "it conceals a secret"() {
    when:
    this.subject.conceal(this.secret, this.newLocation)

    then:
    this.fileProxy.exists('/path/to/somewhere') >> true
    this.secret.isExposedInLocation(this.newLocation) >> true

    0 * this.debugger.print(_)
    1 * this.executeProxy.execute({ it =~ /rm -f "\/path\/to\/somewhere"/ })
    1 * this.secret.markAsConcealed(this.newLocation)
  }

  @Test
  def "it does not conceal if the secret is not exposed in the given location"() {
    when:
    this.subject.conceal(this.secret, this.newLocation)

    then:
    this.secret.isExposedInLocation(this.newLocation) >> false

    0 * this.executeProxy.execute(_)
    0 * this.secret.markAsConcealed(_)
  }
}
