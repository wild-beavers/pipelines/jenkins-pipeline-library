package com.wildbeavers.credentials.exposer

import com.wildbeavers.composite.Fetcher
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import org.junit.Test
import spock.lang.Specification

class CredentialExposerSpec extends Specification {
  private Fetcher secretExposerFetcher = Mock()

  private SecretBag secretBag = Mock()
  private ExpositionLocation defaultExpositionLocation = new ExpositionLocation('some:place')
  private ExpositionLocation expositionLocation = new ExpositionLocation('some:place-else')
  private SecretExposer secretExposer1 = GroovyMock()
  private SecretExposer secretExposer2 = GroovyMock()
  private Secret secretAlreadyExposed = Spy(constructorArgs: ['content', 'already-exposed', this.defaultExpositionLocation])
  private Secret secretNoLocation = Spy(constructorArgs: ['content', 'no-location-set', this.defaultExpositionLocation])
  private Secret secret = Spy(constructorArgs: ['content', 'example', this.defaultExpositionLocation])

  private CredentialExposer subject

  def setup() {
    this.secretAlreadyExposed.isExposedAtAllLocations() >> true
    this.secretAlreadyExposed.hasExpositionLocations() >> true

    this.secretNoLocation.isExposedAtAllLocations() >> false
    this.secretNoLocation.hasExpositionLocations() >> false

    this.secret.isExposedAtAllLocations() >> false
    this.secret.hasExpositionLocations() >> true
    this.secret.getExpositionLocations() >> [(this.expositionLocation): false]

    this.secretBag.getSecrets() >> [one: this.secretNoLocation, two: this.secret, exposed: this.secretAlreadyExposed]

    this.secretExposerFetcher.fetchFirstSupporting(this.defaultExpositionLocation) >> this.secretExposer1
    this.secretExposerFetcher.fetchFirstSupporting(this.expositionLocation) >> this.secretExposer2

    this.subject = new CredentialExposer(this.secretExposerFetcher)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == CredentialExposer
  }

  @Test
  def "it does not exposes a secret bag with no secrets"() {
    when:
    this.subject.expose(this.secretBag)

    then:
    this.secretBag.getSecrets() >> [:]

    0 * this.secretExposerFetcher.fetchFirstSupporting(_)
    0 * this.secretExposer1.expose(_, _)
    0 * this.secretExposer2.expose(_, _)
  }

  @Test
  def "it exposes all secrets of a credential"() {
    when:
    this.subject.expose(this.secretBag)

    then:
    1 * this.secretExposer1.expose(this.secretNoLocation, this.defaultExpositionLocation)
    1 * this.secretExposer2.expose(this.secret, this.expositionLocation)
    0 * this.secretExposer1.expose(this.secretAlreadyExposed, _)
    0 * this.secretExposer2.expose(this.secretAlreadyExposed, _)
  }

  @Test
  def "it does not conceal a secret bag with no secrets"() {
    when:
    this.subject.conceal(this.secretBag)

    then:
    this.secretBag.getSecrets() >> [:]

    0 * this.secretExposerFetcher.fetchFirstSupporting(_)
    0 * this.secretExposer1.conceal(_, _)
    0 * this.secretExposer2.conceal(_, _)
  }

  @Test
  def "it conceals all secrets of a credential"() {
    when:
    this.secretNoLocation.addExpositionLocation(this.defaultExpositionLocation)
    this.subject.conceal(this.secretBag)

    then:
    1 * this.secretExposer1.conceal(this.secretNoLocation, this.defaultExpositionLocation)
    1 * this.secretExposer2.conceal(this.secret, this.expositionLocation)
    0 * this.secretExposer1.conceal(this.secretAlreadyExposed, _)
    0 * this.secretExposer2.conceal(this.secretAlreadyExposed, _)
  }
}
