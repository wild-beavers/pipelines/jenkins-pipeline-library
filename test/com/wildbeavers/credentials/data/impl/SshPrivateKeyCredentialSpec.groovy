package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import org.junit.Test
import spock.lang.Specification

class SshPrivateKeyCredentialSpec extends Specification {
  private ExpositionLocation expositionLocation = Mock()
  private Secret secret = new Secret('wowthisissecret', 'desc', this.expositionLocation)
  private Secret secret2 = new Secret('wowthisissecret2', 'desc2', this.expositionLocation)
  private Secret secret3 = new Secret('wowthisissecret3', 'desc3', this.expositionLocation)

  private SshPrivateKeyCredential subject

  def setup() {
    this.subject = new SshPrivateKeyCredential()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == SshPrivateKeyCredential
  }

  @Test
  def "it returns its ID/Alias"() {
    expect:
    this.subject.getId() == SshPrivateKeyCredential.ALIAS
  }

  @Test
  def "it returns a string version of itself without exposing any secrets"() {
    when:
    this.subject.setUsername(this.secret)

    then:
    this.subject.toString() ==~ ~/.*desc.*/
    !(this.subject.toString() ==~ ~/.*wowthisissecret.*/)
  }

  @Test
  def "it throws an exception when attempting to get a secret not yet added."() {
    when:
    this.subject."$method"()

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to get a secret with type “${type}”.*"

    where:
    method          || type
    'getUsername'   || SecretType.USERNAME
    'getPassphrase' || SecretType.PASSPHRASE
    'getPrivateKey' || SecretType.SSH_PRIVATE_KEY
  }

  @Test
  def "it throws an exception when attempting to add the same secret twice."() {
    when:
    this.subject."$method"(this.secret)
    this.subject."$method"(this.secret)

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to add a secret with type “${type}”.*"

    where:
    method          || type
    'setUsername'   || SecretType.USERNAME
    'setPassphrase' || SecretType.PASSPHRASE
    'setPrivateKey' || SecretType.SSH_PRIVATE_KEY
  }

  @Test
  def "it sets/gets specific secrets"() {
    when:
    this.subject.setUsername(this.secret)
    this.subject.setPassphrase(this.secret2)
    this.subject.setPrivateKey(this.secret3)

    then:
    this.subject.getUsername() == this.secret
    this.subject.getPassphrase() == this.secret2
    this.subject.getPrivateKey() == this.secret3

    this.subject.getSecrets() == [(SecretType.USERNAME): this.secret, (SecretType.PASSPHRASE): this.secret2, (SecretType.SSH_PRIVATE_KEY): this.secret3]
  }
}
