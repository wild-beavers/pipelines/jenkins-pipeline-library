package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import org.junit.Test
import spock.lang.Specification

class TokenCredentialSpec extends Specification {
  private ExpositionLocation expositionLocation = Mock()
  private Secret secret = new Secret('myToken', 'desc', this.expositionLocation)

  private TokenCredential subject

  def setup() {
    this.subject = new TokenCredential()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == TokenCredential
  }

  @Test
  def "it returns its ID/Alias"() {
    expect:
    this.subject.getId() == TokenCredential.ALIAS
  }

  @Test
  def "it returns a string version of itself without exposing any secrets"() {
    when:
    this.subject.setToken(this.secret)

    then:
    this.subject.toString() ==~ ~/.*desc.*/
    !(this.subject.toString() ==~ ~/.*myToken.*/)
  }

  @Test
  def "it throws an exception when attempting to get a secret not yet added."() {
    when:
    this.subject.getToken()

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to get a secret with type “TOKEN”.*"
  }

  @Test
  def "it throws an exception when attempting to add the same secret twice."() {
    when:
    this.subject.setToken(this.secret)
    this.subject.setToken(this.secret)

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to add a secret with type “TOKEN”.*"
  }

  @Test
  def "it sets/gets specific secrets"() {
    when:
    this.subject.setToken(this.secret)

    then:
    this.subject.getToken() == this.secret

    this.subject.getSecrets() == [(SecretType.TOKEN): this.secret]
  }
}
