package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import org.junit.Test
import spock.lang.Specification

class AWSAccessKeyCredentialSpec extends Specification {
  private ExpositionLocation expositionLocation = Mock()
  private Secret secret = new Secret('wowthisissecret', 'desc', this.expositionLocation)
  private Secret secret2 = new Secret('wowthisissecret2', 'desc2', this.expositionLocation)

  private AWSAccessKeyCredential subject

  def setup() {
    this.subject = new AWSAccessKeyCredential()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == AWSAccessKeyCredential
  }

  @Test
  def "it returns its ID/Alias"() {
    expect:
    this.subject.getId() == AWSAccessKeyCredential.ALIAS
  }

  @Test
  def "it returns a string version of itself without exposing any secrets"() {
    when:
    this.subject.setAccessKeyId(this.secret)

    then:
    this.subject.toString() ==~ ~/.*desc.*/
    !(this.subject.toString() ==~ ~/.*wowthisissecret.*/)
  }

  @Test
  def "it throws an exception when attempting to get a secret not yet added."() {
    when:
    this.subject."$method"()

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to get a secret with type “${type}”.*"

    where:
    method               || type
    'getAccessKeyId'     || SecretType.AWS_ACCESS_KEY_ID
    'getAccessSecretKey' || SecretType.AWS_ACCESS_SECRET_KEY
  }

  @Test
  def "it throws an exception when attempting to add the same secret twice."() {
    when:
    this.subject."$method"(this.secret)
    this.subject."$method"(this.secret)

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to add a secret with type “${type}”.*"

    where:
    method               || type
    'setAccessKeyId'     || SecretType.AWS_ACCESS_KEY_ID
    'setAccessSecretKey' || SecretType.AWS_ACCESS_SECRET_KEY
  }

  @Test
  def "it sets/gets specific secrets"() {
    when:
    this.subject.setAccessKeyId(this.secret)
    this.subject.setAccessSecretKey(this.secret2)

    then:
    this.subject.getAccessKeyId() == this.secret
    this.subject.getAccessSecretKey() == this.secret2

    this.subject.getSecrets() == [(SecretType.AWS_ACCESS_KEY_ID): this.secret, (SecretType.AWS_ACCESS_SECRET_KEY): this.secret2]
  }
}
