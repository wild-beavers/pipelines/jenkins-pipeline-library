package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import org.junit.Test
import spock.lang.Specification

class UnknownSecretCredentialSpec extends Specification {
  private ExpositionLocation expositionLocation = Mock()
  private Secret secret = new Secret('wowthisissecret', 'desc', this.expositionLocation)

  private UnknownSecretCredential subject

  def setup() {
    this.subject = new UnknownSecretCredential()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == UnknownSecretCredential
  }

  @Test
  def "it returns its ID/Alias"() {
    expect:
    this.subject.getId() == UnknownSecretCredential.ALIAS
  }

  @Test
  def "it returns a string version of itself without exposing any secrets"() {
    when:
    this.subject.setUnknownSecret(this.secret)

    then:
    this.subject.toString() ==~ ~/.*desc.*/
    !(this.subject.toString() ==~ ~/.*wowthisissecret.*/)
  }

  @Test
  def "it throws an exception when attempting to get a secret not yet added."() {
    when:
    this.subject.getUnknownSecret()

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to get a secret with type “UNKNOWN”.*"
  }

  @Test
  def "it throws an exception when attempting to add the same secret twice."() {
    when:
    this.subject.setUnknownSecret(this.secret)
    this.subject.setUnknownSecret(this.secret)

    then:
    Exception e = thrown(RuntimeException)
    e.getMessage() ==~ "Attempted to add a secret with type “UNKNOWN”.*"
  }

  @Test
  def "it sets/gets specific secrets"() {
    when:
    this.subject.setUnknownSecret(this.secret)

    then:
    this.subject.getUnknownSecret() == this.secret

    this.subject.getSecrets() == [(SecretType.UNKNOWN): this.secret]
  }
}
