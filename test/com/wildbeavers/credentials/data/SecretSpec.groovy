package com.wildbeavers.credentials.data

import com.wildbeavers.credentials.data_validation.ExpositionLocation
import org.junit.Test
import spock.lang.Specification

class SecretSpec extends Specification {
  private Secret subject

  private ExpositionLocation expositionLocation = GroovyMock()
  private ExpositionLocation expositionLocation2 = GroovyMock()
  private ExpositionLocation expositionLocation3 = GroovyMock()

  def setup() {
    this.subject = new Secret('secret', 'description', this.expositionLocation)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == Secret
  }

  @Test
  def "it returns a string view of itself without the secret"() {
    expect:
    this.subject.toString() ==~ ~/.*description.*/
    !(this.subject.toString() ==~ ~/.*secret.*/)
  }

  @Test
  def "it returns its internal values"() {
    expect:
    this.subject.getContent() == 'secret'
    this.subject.getDescription() == 'description'
    this.subject.getDefaultExpositionLocation() == this.expositionLocation
    this.subject.getExpositionLocations() == [:]
    this.subject.getFirstExposedLocation() == null
    this.subject.isExposedAtAllLocations()
    !this.subject.hasExpositionLocations()
  }

  @Test
  def "it ignores marking exposition location that have not been added"() {
    when:
    this.subject.markAsExposed(this.expositionLocation)
    this.subject.markAsConcealed(this.expositionLocation)
    this.subject.removeExpositionLocation(this.expositionLocation)

    then:
    !this.subject.hasExpositionLocations()
  }

  @Test
  def "it accept new exposition locations and manipulates them"() {
    when:
    this.subject.addExpositionLocation(this.expositionLocation2)
    this.subject.addExpositionLocation(this.expositionLocation3)
    this.subject.addExpositionLocation(this.expositionLocation3)

    then:
    this.subject.hasExpositionLocations()
    this.subject.getExpositionLocations() == [(this.expositionLocation2): false, (this.expositionLocation3): false]
    this.subject.getFirstExposedLocation() == null

    when:
    this.subject.markAsExposed(this.expositionLocation3)

    then:
    this.subject.getExpositionLocations() == [(this.expositionLocation2): false, (this.expositionLocation3): true]
    this.subject.getFirstExposedLocation() == this.expositionLocation3
    !this.subject.isExposedAtAllLocations()

    when:
    this.subject.markAsExposed(this.expositionLocation2)

    then:
    this.subject.getExpositionLocations() == [(this.expositionLocation2): true, (this.expositionLocation3): true]
    this.subject.getFirstExposedLocation() == this.expositionLocation2
    this.subject.isExposedAtAllLocations()

    when:
    this.subject.markAsConcealed(this.expositionLocation2)
    this.subject.removeExpositionLocation(this.expositionLocation3)

    then:
    this.subject.getFirstExposedLocation() == null
    this.subject.getExpositionLocations() == [(this.expositionLocation2): false]

    when:
    this.subject.addExpositionLocation('somePlace:TO_LOCATION')

    then:
    this.subject.getExpositionLocations().size() == 2
  }
}
