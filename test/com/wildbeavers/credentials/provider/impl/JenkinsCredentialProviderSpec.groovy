package com.wildbeavers.credentials.provider.impl

import com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey
import com.cloudbees.plugins.credentials.common.StandardUsernameCredentials
import com.cloudbees.plugins.credentials.impl.BaseStandardCredentials
import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl
import com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exception.CredentialUnsupported
import com.wildbeavers.credentials.transformer.JenkinsCredentialDataTransformer
import org.junit.Test
import spock.lang.Specification

class JenkinsCredentialProviderSpec extends Specification {
  private JenkinsCredentialDataTransformer jenkinsCredentialDataTransformer = Mock()
  private JenkinsProviderAdapter jenkinsProviderAdapter = Mock()

  private UsernamePasswordCredentialsImpl usernamePasswordCredentialsImpl = Mock()
  private UsernamePasswordCredential usernamePasswordCredential = Mock()
  private SshPrivateKeyCredential sshPrivateKeyCredential = Mock()
  private TokenCredential tokenCredential = Mock()
  private BasicSSHUserPrivateKey basicSSHUserPrivateKey = Mock()
  private BaseStandardCredentials gitLabApiTokenImpl = GroovyMock()
  private CredentialURL nonSupportedCredentialUrl = Mock()
  private CredentialURL usernamePasswordURL = Mock()
  private CredentialURL sshSecretURL = Mock()
  private CredentialURL gitlabTokenUrl = Mock()

  private JenkinsCredentialProvider subject

  def setup() {
    this.nonSupportedCredentialUrl.getCredentialType() >> 'unsupported'

    this.usernamePasswordURL.getCredentialType() >> UsernamePasswordCredential.ALIAS
    this.usernamePasswordURL.getCredentialId() >> 'some-pass'

    this.usernamePasswordCredentialsImpl.getId() >> 'some-pass'
    this.jenkinsProviderAdapter.lookupCredentials(StandardUsernameCredentials) >> [this.usernamePasswordCredentialsImpl]
    this.jenkinsCredentialDataTransformer.transform(this.usernamePasswordCredentialsImpl) >> this.usernamePasswordCredential

    this.sshSecretURL.getCredentialType() >> SshPrivateKeyCredential.ALIAS
    this.sshSecretURL.getCredentialId() >> 'ssh-key'

    this.basicSSHUserPrivateKey.getId() >> 'ssh-key'
    this.jenkinsProviderAdapter.lookupCredentials(BasicSSHUserPrivateKey) >> [this.basicSSHUserPrivateKey]
    this.jenkinsCredentialDataTransformer.transform(this.basicSSHUserPrivateKey) >> this.sshPrivateKeyCredential

    this.gitlabTokenUrl.getCredentialType() >> TokenCredential.ALIAS
    this.gitlabTokenUrl.getCredentialId() >> 'gitlab-token'

    this.gitLabApiTokenImpl.getId() >> 'gitlab-token'
    this.jenkinsProviderAdapter.lookupCredentials(GitLabApiTokenImpl) >> [this.gitLabApiTokenImpl, this.gitLabApiTokenImpl, this.gitLabApiTokenImpl]
    this.jenkinsCredentialDataTransformer.transformToken(this.gitLabApiTokenImpl) >> this.tokenCredential

    this.subject = new JenkinsCredentialProvider(this.jenkinsCredentialDataTransformer, this.jenkinsProviderAdapter)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == JenkinsCredentialProvider
  }

  @Test
  def "it returns its supported credential types"() {
    expect:
    this.subject.getSupportedCredentialTypes().contains(UsernamePasswordCredential.ALIAS)
    this.subject.getSupportedCredentialTypes().contains(SshPrivateKeyCredential.ALIAS)
  }

  @Test
  def "it whether is supports specific credential"() {
    expect:
    !this.subject.support(this.nonSupportedCredentialUrl)
    this.subject.support(this.usernamePasswordURL)
    this.subject.support(this.sshSecretURL)
  }

  @Test
  def "it returns its ID"() {
    expect:
    this.subject.getId() == this.subject.ALIAS
  }

  @Test
  def "it throws an exception as failsafe if when the provided credential is not supported"() {
    when:
    this.subject.provide(this.nonSupportedCredentialUrl)
    this.subject.doProvide(this.nonSupportedCredentialUrl)

    then:
    thrown(CredentialUnsupported)
  }

  @Test
  def "it provides a UsernamePassword"() {
    expect:
    this.usernamePasswordCredential == this.subject.provide(this.usernamePasswordURL)
  }

  @Test
  def "it provides a SSHPrivateKey"() {
    expect:
    this.sshPrivateKeyCredential == this.subject.provide(this.sshSecretURL)
  }

  @Test
  def "it provides a token from various sources"() {
    expect:
    this.tokenCredential == this.subject.provide(this.gitlabTokenUrl)
  }

  @Test
  def "it throws a NotFound exception when the credential ID is not found"() {
    when:
    this.subject.provide(this.usernamePasswordURL)

    then:
    this.usernamePasswordURL.getCredentialId() >> 'not-foundable'

    def e = thrown(CredentialNotFound)
    e.getMessage() =~ /Credential “not-foundable” not found in jenkins./
  }
}
