package com.wildbeavers.credentials.provider.impl

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.AWSAccessKeyCredential
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UnknownSecretCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exception.CredentialUnsupported
import com.wildbeavers.data.CommandResult
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger
import org.junit.Test
import spock.lang.Specification

class AWSSecretsManagerCredentialProviderSpec extends Specification {
  private Debugger debugger = Mock()
  private ContainerRunner containerRunner = Mock()
  private GenericFactory<Secret> secretFactory = Mock()
  private GenericFactory<SecretBag> secretBagFactory = Mock()
  private ContainerOptions containerOptions = Spy()

  private CommandResult commandResult = Mock()

  private UsernamePasswordCredential usernamePasswordCredential = Spy()
  private SshPrivateKeyCredential sshPrivateKeyCredential = Spy()
  private UnknownSecretCredential unknownSecretCredential = Spy()
  private AWSAccessKeyCredential aWSAccessKeyCredential = Spy()
  private TokenCredential tokenCredential = Spy()

  private CredentialURL usernamePasswordURL = new CredentialURL(UsernamePasswordCredential.ALIAS + ':some-pass')
  private CredentialURL sshSecretURL = new CredentialURL(SshPrivateKeyCredential.ALIAS + ':some-pass')
  private CredentialURL unknownURL = new CredentialURL(UnknownSecretCredential.ALIAS + ':some-pass')
  private CredentialURL awsAccessKeyURL = new CredentialURL(AWSAccessKeyCredential.ALIAS + ':some-pass')
  private CredentialURL tokenURL = new CredentialURL(TokenCredential.ALIAS + ':some-pass')
  private CredentialURL undefinedURL = new CredentialURL('do-not-exist:some-pass')

  private AWSSecretsManagerCredentialProvider subject

  def setup() {
    this.containerRunner.run(this.containerOptions, _) >> this.commandResult

    this.secretBagFactory.instantiate('UsernamePasswordCredential', _) >> this.usernamePasswordCredential
    this.secretBagFactory.instantiate('SshPrivateKeyCredential', _) >> this.sshPrivateKeyCredential
    this.secretBagFactory.instantiate('AWSAccessKeyCredential', _) >> this.aWSAccessKeyCredential
    this.secretBagFactory.instantiate('UnknownSecretCredential', _) >> this.unknownSecretCredential
    this.secretBagFactory.instantiate('TokenCredential', _) >> this.tokenCredential

    this.subject = new AWSSecretsManagerCredentialProvider(
      this.debugger,
      this.containerRunner,
      this.secretFactory,
      this.secretBagFactory,
      this.containerOptions,
      '123456789012',
      'us-east-1',
    )
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == AWSSecretsManagerCredentialProvider
  }

  @Test
  def "it returns its supported credential types"() {
    expect:
    this.subject.getSupportedCredentialTypes().contains(UsernamePasswordCredential.ALIAS)
    this.subject.getSupportedCredentialTypes().contains(SshPrivateKeyCredential.ALIAS)
    this.subject.getSupportedCredentialTypes().contains(AWSAccessKeyCredential.ALIAS)
    this.subject.getSupportedCredentialTypes().contains(UnknownSecretCredential.ALIAS)
    this.subject.getSupportedCredentialTypes().contains(TokenCredential.ALIAS)
  }

  @Test
  def "it returns its ID"() {
    expect:
    this.subject.getId() == this.subject.ALIAS
  }

  @Test
  def "it whether is supports specific credential"() {
    expect:
    !this.subject.support(this.undefinedURL)
    this.subject.support(this.usernamePasswordURL)
    this.subject.support(this.sshSecretURL)
    this.subject.support(this.unknownURL)
    this.subject.support(this.awsAccessKeyURL)
    this.subject.support(this.tokenURL)
  }

  @Test
  def "it throws an exception as failsafe if when the provided credential is not supported"() {
    when:
    this.subject.provide(this.undefinedURL)
    this.subject.doProvide(this.undefinedURL)

    then:
    thrown(CredentialUnsupported)
  }

  @Test
  def "it provides a UsernamePassword"() {
    when:
    def result = this.subject.provide(this.usernamePasswordURL)

    then:
    this.commandResult.getStdout() >> '{"username": "toto", "password": "tata"}'

    this.usernamePasswordCredential == result
  }

  @Test
  def "it provides a SshPrivateKeyCredential"() {
    when:
    def result = this.subject.provide(this.sshSecretURL)

    then:
    this.commandResult.getStdout() >> '{"private_key": "this is private key", "username": "toto", "passphrase": "tata"}'

    this.sshPrivateKeyCredential == result
  }

  @Test
  def "it provides a AWSAccessKeyCredential"() {
    when:
    def result = this.subject.provide(this.awsAccessKeyURL)

    then:
    this.commandResult.getStdout() >> '{"access_key_id": "an access key", "access_secret_key": "this is secret key!!"}'

    this.aWSAccessKeyCredential == result
  }

  @Test
  def "it provides a UnknownSecretCredential"() {
    when:
    def result = this.subject.provide(this.unknownURL)

    then:
    this.commandResult.getStdout() >> 'this can be anything'

    this.unknownSecretCredential == result
  }

  @Test
  def "it provides a TokenCredential"() {
    when:
    def result = this.subject.provide(this.tokenURL)

    then:
    this.commandResult.getStdout() >> '{"token": "s3cretToken"}'

    this.tokenCredential == result
  }

  @Test
  def "it gracefully transform a container run expection to a CredentialNotFound exception with a debug message"() {
    when:
    this.subject.provide(new CredentialURL(url))

    then:
    this.containerRunner.run(this.containerOptions, _) >> { throw new CommandExecutionException('FAILED!') }

    1 * this.debugger.printDebug('Credential “some-pass” not found in “awsSecretsManager”. Command failed: FAILED!')
    thrown(CredentialNotFound)

    where:
    url                                             | _
    UsernamePasswordCredential.ALIAS + ':some-pass' | _
    UnknownSecretCredential.ALIAS + ':some-pass'    | _
    AWSAccessKeyCredential.ALIAS + ':some-pass'     | _
    TokenCredential.ALIAS + ':some-pass'            | _
  }


  @Test
  def "it throws an exception when the Credentials are not unknown and command do not return valid JSON"() {
    when:
    this.subject.provide(new CredentialURL(url))

    then:
    this.commandResult.getStdout() >> not_json

    def e = thrown(CredentialUnsupported)
    e.getMessage() =~ /Cannot parse credential/
    e.getMessage() =~ /Raw credential is not a valid JSON./

    where:
    url                                             | not_json
    UsernamePasswordCredential.ALIAS + ':some-pass' | 'nope'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | '_'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | ''
    AWSAccessKeyCredential.ALIAS + ':some-pass'     | '{THIS, WILL, FAIL}'
    TokenCredential.ALIAS + ':some-pass'            | '~'
  }

  @Test
  def "it fails to fill specific credentials when required JSON keys are not found"() {
    when:
    this.subject.provide(new CredentialURL(url))

    then:
    this.commandResult.getStdout() >> bad_key_json

    def e = thrown(CredentialUnsupported)
    e.getMessage() =~ /Cannot parse credential/
    e.getMessage() =~ /“${missing_key}” key is missing from the raw credential JSON./

    where:
    url                                             | missing_key         | bad_key_json
    UsernamePasswordCredential.ALIAS + ':some-pass' | 'username'          | '{"access_key_id": "an access key", "access_secret_key": "this is secret key!!"}'
    UsernamePasswordCredential.ALIAS + ':some-pass' | 'password'          | '{"username": "username", "not_password": "bad key"}'
    UsernamePasswordCredential.ALIAS + ':some-pass' | 'password'          | '{"username": "missng password"}'
    AWSAccessKeyCredential.ALIAS + ':some-pass'     | 'access_key_id'     | '{"private_key": "this is private key", "access_secret_key": "toto", "passphrase": "tata"}'
    AWSAccessKeyCredential.ALIAS + ':some-pass'     | 'access_secret_key' | '{"access_key_id": "something"}'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | 'username'          | '{"private_key": "something", "passphrase": "close"}'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | 'username'          | '{}'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | 'private_key'       | '{"username": "missng pk"}'
    TokenCredential.ALIAS + ':some-pass'            | 'token'             | '{"not_a_token": "among_us.exe"}'
  }

  @Test
  def "it fails to fill specific credentials when required JSON values are not flat strings"() {
    when:
    this.subject.provide(new CredentialURL(url))

    then:
    this.commandResult.getStdout() >> bad_key_json

    def e = thrown(CredentialUnsupported)
    e.getMessage() =~ /Cannot parse credential/
    e.getMessage() =~ /“${error_key}” key was found but does not contain a flat string./

    where:
    url                                             | error_key           | bad_key_json
    UsernamePasswordCredential.ALIAS + ':some-pass' | 'username'          | '{"username": false, "password": "tata"}'
    UsernamePasswordCredential.ALIAS + ':some-pass' | 'password'          | '{"username": "toto", "password": ["list?"]}'
    AWSAccessKeyCredential.ALIAS + ':some-pass'     | 'access_key_id'     | '{"access_key_id": 1, "access_secret_key": 2}'
    AWSAccessKeyCredential.ALIAS + ':some-pass'     | 'access_secret_key' | '{"access_key_id": "an access key", "access_secret_key": {"access_secret_key": "nope"}}'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | 'username'          | '{"private_key": "this is private key", "username": {}, "passphrase": "tata"}'
    SshPrivateKeyCredential.ALIAS + ':some-pass'    | 'private_key'       | '{"private_key": true, "username": "toto", "passphrase": "tata"}'
    TokenCredential.ALIAS + ':some-pass'            | 'token'             | '{"token": 0}'
  }
}
