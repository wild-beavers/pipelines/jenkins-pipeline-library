package com.wildbeavers.credentials

import com.wildbeavers.composite.Fetcher
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exposer.CredentialExposer
import com.wildbeavers.credentials.provider.CredentialBackendProvider
import org.junit.Test
import spock.lang.Specification

class CredentialLoaderSpec extends Specification {
  private Fetcher<CredentialBackendProvider, CredentialURL, String> credentialBackendProviderFetcher = Mock()
  private Fetcher<CredentialBackendProvider, CredentialURL, String> credentialBackendProviderFetcher2 = Mock()
  private CredentialExposer credentialExposer = GroovyMock()

  private CredentialBackendProvider backendProvider1 = GroovyMock()
  private CredentialBackendProvider backendProvider2 = GroovyMock()
  private SecretBag credential = GroovyMock()

  private CredentialLoader subject

  def setup() {
    this.backendProvider1.getId() >> 'secret-backend-1'
    this.backendProvider1.support(new CredentialURL('is:supported')) >> true
    this.backendProvider1.support({ it.getCredentialType() != 'is' }) >> false
    this.backendProvider1.provide({ it.getCredentialId() == 'backend-1-found' }) >> this.credential
    this.backendProvider1.provide({ it.getCredentialId() != 'backend-1-found' }) >> { throw new CredentialNotFound('') }
    this.backendProvider2.getId() >> 'secret-backend-2'
    this.backendProvider2.support(new CredentialURL('is:supported')) >> true
    this.backendProvider2.support({ it.getCredentialType() != 'is' }) >> false
    this.backendProvider2.provide({ it.getCredentialId() == 'backend-2-found' }) >> this.credential
    this.backendProvider2.provide({ it.getCredentialId() != 'backend-2-found' }) >> { throw new CredentialNotFound('') }

    this.credentialBackendProviderFetcher.fetchAll() >> [this.backendProvider1, this.backendProvider2]
    this.credentialBackendProviderFetcher.fetchSupporting({ it.getCredentialType() == 'one-supported' }, _) >> ([this.backendProvider1] as LinkedHashSet<CredentialBackendProvider>)
    this.credentialBackendProviderFetcher.fetchSupporting({ it.getCredentialType() == 'two-supported' }, _) >> ([this.backendProvider1, this.backendProvider2] as LinkedHashSet<CredentialBackendProvider>)
    this.credentialBackendProviderFetcher.fetchSupporting({ it.getCredentialType() == 'not-supported' }, _) >> ([] as LinkedHashSet<CredentialBackendProvider>)
    this.credentialBackendProviderFetcher.fetchSupporting(_, 'secret-backend-one') >> ([this.backendProvider1] as LinkedHashSet<CredentialBackendProvider>)
    this.credentialBackendProviderFetcher.fetchSupporting(_, 'secret-backend-two') >> ([this.backendProvider2] as LinkedHashSet<CredentialBackendProvider>)

    this.subject = new CredentialLoader(this.credentialBackendProviderFetcher, this.credentialExposer)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == CredentialLoader
  }

  @Test
  def "it returns all the registered backend providers aliases in order"() {
    expect:
    this.subject.getSupportedProviders()[0] == 'secret-backend-1'
    this.subject.getSupportedProviders()[1] == 'secret-backend-2'
  }

  @Test
  def "it throws an exception when none of the providers can load a credentialURL"() {
    when:
    this.subject.load('backend:not-supported:id')

    then:
    def e = thrown(CredentialNotFound)
    e.getMessage() =~ /cannot be found in any of the supported credential providers/

    where:
    url                                           | _
    'two-supported:not-found'                     | _
    'not-supported:backend-1-found'               | _
    'one-supported:backend-2-found'               | _
    'secret-backend-two:whatever:backend-1-found' | _
    'none:whatever:backend-1-found'               | _
  }

  @Test
  def "it loads a credential in various cases"() {
    when:
    def result = this.subject.load(url)

    then:
    result == this.credential

    where:
    url                                           | _
    'two-supported:backend-2-found'               | _
    'two-supported:backend-1-found'               | _
    'one-supported:backend-1-found'               | _
    'secret-backend-two:whatever:backend-2-found' | _
    'secret-backend-one:whatever:backend-1-found' | _
  }

  @Test
  def "it loads and expose a credential at the same time"() {
    when:
    def result = this.subject.loadAndExpose('two-supported:backend-2-found')

    then:
    result == this.credential
    1 * this.credentialExposer.expose(this.credential)
  }

  @Test
  def "it exposes a Credential"() {
    when:
    this.subject.expose(this.credential)

    then:
    1 * this.credentialExposer.expose(this.credential)
  }

  @Test
  def "it conceals a Credential"() {
    when:
    this.subject.conceal(this.credential)

    then:
    1 * this.credentialExposer.conceal(this.credential)
  }

  @Test
  def "it changes its fetcher at runtime"() {
    when:
    this.subject.setCredentialBackendProviderFetcher(this.credentialBackendProviderFetcher2)

    then:
    this.subject.getSupportedProviders().isEmpty()
  }
}
