package com.wildbeavers.credentials.transformer

import com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey
import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl
import com.dabsquared.gitlabjenkins.connection.GitLabApiToken
import com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.di.GenericFactory
import org.junit.Test
import spock.lang.Specification

class JenkinsCredentialDataTransformerSpec extends Specification {
  private GenericFactory<Secret> secretGenericFactory = Mock()
  private GenericFactory<SecretBag> secretBagGenericFactory = Mock()

  private Secret privateKeySecret = Mock()
  private Secret passphraseSecret = Mock()
  private Secret privateKeyUsernameSecret = Mock()
  private Secret usernameSecret = Mock()
  private Secret passwordSecret = Mock()
  private Secret tokenSecret = Mock()
  private BasicSSHUserPrivateKey basicSSHUserPrivateKey = Mock()
  private UsernamePasswordCredentialsImpl usernamePasswordCredentialsImpl = Mock()
  private UsernamePasswordCredential usernamePasswordCredential = Mock()
  private SshPrivateKeyCredential sshPrivateKeyCredential = Mock()
  private TokenCredential tokenCredential = Mock()
  private GitLabApiTokenImpl GitLabApiToken = GroovyMock()

  private JenkinsCredentialDataTransformer subject

  def setup() {
    this.basicSSHUserPrivateKey.getUsername() >> 'username'
    this.basicSSHUserPrivateKey.getPassphrase() >> new hudson.util.Secret('passphrase')
    this.basicSSHUserPrivateKey.getPrivateKeys() >> ['private_key']

    this.usernamePasswordCredentialsImpl.getUsername() >> 'username'
    this.usernamePasswordCredentialsImpl.getPassword() >> new hudson.util.Secret('password')

    this.GitLabApiToken.getToken() >> new hudson.util.Secret('secrettoken')

    this.secretGenericFactory.instantiate('sshPrivateKey', 'private_key') >> this.privateKeySecret
    this.secretGenericFactory.instantiate('sshPrivateKeyUsername', 'username') >> this.privateKeyUsernameSecret
    this.secretGenericFactory.instantiate('sshPassphrase', 'passphrase') >> this.passphraseSecret
    this.secretGenericFactory.instantiate('username', 'username') >> this.usernameSecret
    this.secretGenericFactory.instantiate('password', 'password') >> this.passwordSecret
    this.secretGenericFactory.instantiate('token', 'secrettoken') >> this.tokenSecret
    this.secretBagGenericFactory.instantiate('UsernamePasswordCredential', this.usernameSecret, this.passwordSecret) >> this.usernamePasswordCredential
    this.secretBagGenericFactory.instantiate('SshPrivateKeyCredential', this.privateKeySecret, this.privateKeyUsernameSecret, this.passphraseSecret) >> this.sshPrivateKeyCredential
    this.secretBagGenericFactory.instantiate('TokenCredential', this.tokenSecret) >> this.tokenCredential

    this.subject = new JenkinsCredentialDataTransformer(this.secretGenericFactory, this.secretBagGenericFactory)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == JenkinsCredentialDataTransformer
  }

  @Test
  def "it transforms a Jenkins SSH private key in wildbeaver credential"() {
    expect:
    this.sshPrivateKeyCredential == this.subject.transform(this.basicSSHUserPrivateKey)
  }

  @Test
  def "it transforms a Jenkins username password in wildbeaver credential"() {
    expect:
    this.usernamePasswordCredential == this.subject.transform(this.usernamePasswordCredentialsImpl)
  }

  @Test
  def "it transforms a plugin’s credential to a token credential"() {
    expect:
    this.tokenCredential == this.subject.transformToken(this.GitLabApiToken)
  }
}
