package com.wildbeavers.credentials.data_validation

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class CredentialURLSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new CredentialURL('backend:type:secret-id').getClass() == CredentialURL
  }

  @Test
  def "it have a description"() {
    expect:
    new CredentialURL('backend:type:secret-id').getDescription() == 'a string matching /^([a-zA-Z_-]+:)?[a-zA-Z_-]+:[0-9a-zA-Z_/-]+$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new CredentialURL(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^([a-zA-Z_-]+:)?[a-zA-Z_-]+:[0-9a-zA-Z_/-]+$/.'

    where:
    value            || expectedException
    '//'             || ValidationException
    ':::'            || ValidationException
    'non'            || ValidationException
    'foo.com'        || ValidationException
    'è:invalid:char' || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new CredentialURL(value)

    then:
    noExceptionThrown()

    where:
    value                             | _
    'some:valid:value'                | _
    'usernamePassword:my-secret-id'   | _
    'vault:json:/path/to/some/secret' | _
    'two-supported:backend-2-found'   | _
  }

  @Test
  def "it extracts part of the URL"() {
    expect:
    provider == new CredentialURL(value).getCredentialProvider()
    type == new CredentialURL(value).getCredentialType()
    id == new CredentialURL(value).getCredentialId()

    where:
    value                             | provider | type               | id
    'some:valid:value'                | 'some'   | 'valid'            | 'value'
    'usernamePassword:my-secret-id'   | ''       | 'usernamePassword' | 'my-secret-id'
    'vault:json:/path/to/some/secret' | 'vault'  | 'json'             | '/path/to/some/secret'
  }
}
