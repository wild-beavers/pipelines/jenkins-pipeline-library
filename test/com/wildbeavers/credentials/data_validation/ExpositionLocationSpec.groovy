package com.wildbeavers.credentials.data_validation

import com.wildbeavers.data_validation.ValidationException
import org.junit.Test
import spock.lang.Specification

class ExpositionLocationSpec extends Specification {
  @Test
  def "it can be instantiated"() {
    expect:
    new ExpositionLocation('type:location').getClass() == ExpositionLocation
  }

  @Test
  def "it have a description"() {
    expect:
    new ExpositionLocation('EnvironmentVariable:MY_ENV').getDescription() == 'a string matching /^[a-zA-Z_-]+:((?!:)[ -~])+$/.'
  }

  @Test
  def "it throws an exception when instiantiated with an incorrect value"() {
    when:
    new ExpositionLocation(value)

    then:
    Exception e = thrown(expectedException)
    e.getMessage() == '“' + value + '” is not a string matching /^[a-zA-Z_-]+:((?!:)[ -~])+$/.'

    where:
    value            || expectedException
    '//'             || ValidationException
    ':::'            || ValidationException
    'non'            || ValidationException
    'foo.com'        || ValidationException
    'è:invalid_char' || ValidationException
    'this:not:valid' || ValidationException
  }

  @Test
  def "it accepts valid values"() {
    when:
    new ExpositionLocation(value)

    then:
    noExceptionThrown()

    where:
    value                             | _
    'environmentVariable:SOME_SECRET' | _
    'file:/path/to/some/secret'       | _
  }

  @Test
  def "it extracts parts of the URL"() {
    expect:
    type == new ExpositionLocation(value).getExpositionType()
    location == new ExpositionLocation(value).getLocation()

    where:
    value                               | type                  | location
    'some:valid_value'                  | 'some'                | 'valid_value'
    'environmentVariable:THIS_IS_A_VAR' | 'environmentVariable' | 'THIS_IS_A_VAR'
    'file:/path/to/some/secret'         | 'file'                | '/path/to/some/secret'
  }
}
