package com.wildbeavers

import com.wildbeavers.data.CommandResult

abstract class GroovyScriptMock extends com.cloudbees.groovy.cps.SerializableScript {
  public Map env = [:]
  public currentBuild = new RunWrapperMock()
  public scm

  Map setEnv(Map env) {
    this.env = env
  }

  Map getEnv() {
    return this.env
  }

  Object getEnv(CharSequence key) {
    return this.env[key]
  }

  void checkout(Map data) {
  }

  void error(String error) {
  }


  Object input(Map commandArguments) {
  }

  Object timeout(Map args, Closure function) {
    return function()
  }

  void unstable(String error) {
  }

  String sh(String command) {
    return null
  }

  String sh(Map commandArguments) {
    return null
  }

  void junit(Map map) {
  }

  void parallel(Map map) {
  }

  CommandResult execute(Map map) {
  }

  Boolean fileExists(String file) {
    return true
  }

  Object withCredentials(List map, Closure function) {
    return function()
  }

  Map gitUsernamePassword(Map map) {
  }

  Map usernamePassword(Map map) {
  }

  Map sshUserPrivateKey(Map map) {
  }

  Object sleep(long milliseconds) {
  }

  void post(Closure closure) {
  }

  void always(Closure closure) {
  }

  void archiveArtifacts(Map artifacts) {
  }

  void print(Object string) {
  }

  void println(Object string) {
  }

  String readFile(Map args) {
  }

  String readFile(CharSequence file) {
  }

  void setCurrentBuild(currentBuild) {
    this.currentBuild = currentBuild
  }

  RunWrapperMock getCurrentBuild() {
    return this.currentBuild
  }

  void dir(String dir, closure) {
  }

  void properties(List properties) {
  }

  Object cron(String cron) {
  }

  Object logRotator(Map param) {
  }

  Object buildDiscarder(Object param) {
  }

  Object pipelineTriggers(List params) {
  }

  void deleteDir() {
  }

  def getScm() {
    return scm
  }

  void setScm(scm) {
    this.scm = scm
  }

  void lock(Map args, Closure function) {
    function()
  }

  void lock(String name, Closure function) {
    function()
  }
}
