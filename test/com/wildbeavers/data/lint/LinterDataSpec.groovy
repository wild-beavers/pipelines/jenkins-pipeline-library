package com.wildbeavers.data.lint

import com.wildbeavers.container.data.ContainerOptions
import org.junit.Test
import spock.lang.Specification

class LinterDataSpec extends Specification {
  private ContainerOptions containerOptionsDefault = Spy()
  private ContainerOptions containerOptions = Spy()

  private LinterData subject

  def setup() {
    this.subject = new LinterData(this.containerOptionsDefault)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == LinterData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getContainerOptions() == this.containerOptionsDefault
    this.subject.getCommandArguments() == ''
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setContainerOptions(this.containerOptions)
    this.subject.setCommandArguments('-args')

    then:
    this.subject.getContainerOptions() == this.containerOptions
    this.subject.getCommandArguments() == '-args'
  }
}
