package com.wildbeavers.data

import org.junit.Test
import spock.lang.Specification

class CommandResultSpec extends Specification {
  private CommandResult subject

  def setup() {
    this.subject = new CommandResult('command', 'stdout', 'stderr', 1)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == CommandResult
  }

  @Test
  def "it returns its internal values"() {
    expect:
    this.subject.getCommand() == 'command'
    this.subject.getStdout() == 'stdout'
    this.subject.getStderr() == 'stderr'
    this.subject.getStatusCode() == 1
  }

  @Test
  def "it can show all of its value as string"() {
    expect:
    this.subject.toString() =~ /command/
    this.subject.toString() =~ /stdout/
    this.subject.toString() =~ /stderr/
    this.subject.toString() =~ /1/
  }
}
