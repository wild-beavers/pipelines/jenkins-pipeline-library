package com.wildbeavers.data.file_standard

import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import org.junit.Test
import spock.lang.Specification

class FileStandardDataSpec extends Specification {
  private FileStandardData subject

  def setup() {
    this.subject = new FileStandardData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == FileStandardData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getForbiddenFiles() == []
    this.subject.getMandatoryFiles() == []
  }


  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setForbiddenFiles(['./some/file.txt', 'forbidden/file.txt'])
    this.subject.setMandatoryFiles(['allowed.groovy'])

    then:
    this.subject.getForbiddenFiles() == ['./some/file.txt', 'forbidden/file.txt']
    this.subject.getMandatoryFiles() == ['allowed.groovy']
  }

  @Test
  def "it can add a single mandatory/forbidden file"() {
    when:
    this.subject.addForbiddenFile('this_is_forbidden')
    this.subject.addForbiddenFile(new ForwardRelativeLinuxFullPath('this_is_also_forbidden'))
    this.subject.addMandatoryFile(new ForwardRelativeLinuxFullPath('allowed.groovy'))
    this.subject.addMandatoryFile('allgood')

    then:
    this.subject.getForbiddenFiles() == ['this_is_forbidden', 'this_is_also_forbidden']
    this.subject.getMandatoryFiles() == ['allowed.groovy', 'allgood']
  }
}
