package com.wildbeavers.data

import org.junit.Test
import spock.lang.Specification

class ScmInfoSpec extends Specification {
  private ScmInfo subject

  def setup() {
    this.subject = new ScmInfo(
      '45d8d1f4dd367417df480a2f6e3d358e3c7224c8',
      '45d8d1f4dd367417df480a2f6e3d358e3c7224c8',
      '45d8d1f4dd367417df480a2f6e3d358e3c7224c8',
      'feature/new',
      'main',
      true,
      'origin',
      'jenkins-pipeline-library',
      'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library',
      ['1.0.0', '1.0', '1', 'latest'] as LinkedHashSet,
      ['1.0.0', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet,
      ['1.0.0', '1.0', '1', 'latest'] as LinkedHashSet,
      ['1.0.0', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet,
      '/some/path/to/workspace',
      ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
    )
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ScmInfo
  }

  @Test
  def "it shows a string version of itself"() {
    expect:
    this.subject.toString()
  }

  @Test
  def "it returns information about the current state of the revision and CVS"() {
    expect:
    this.subject.getRevisionCommitId() == '45d8d1f4dd367417df480a2f6e3d358e3c7224c8'
    this.subject.getLocalTipCommitId() == '45d8d1f4dd367417df480a2f6e3d358e3c7224c8'
    this.subject.getRemoteTipCommitId() == '45d8d1f4dd367417df480a2f6e3d358e3c7224c8'
    this.subject.isRevisionLatest()
    this.subject.getBranch() == 'feature/new'
    this.subject.getBranchAsDockerTag() == 'feature_new'
    this.subject.getDefaultBranch() == 'main'
    !this.subject.isCurrentBranchDefault()
    !this.subject.isCurrentCommitLatestAndOnDefaultBranch()
    this.subject.isPullRequest()
    this.subject.isMergeRequest()
    this.subject.getRemoteName() == 'origin'
    this.subject.getRepositoryName() == 'jenkins-pipeline-library'
    this.subject.getRepositoryURL() == 'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library'
    this.subject.getRevisionSemverTag() == '1.0.0'
    this.subject.getAllRevisionFloatingTags() == ['1', '1.0', 'latest'] as LinkedHashSet
    this.subject.getAllRevisionSemverAndFloatingTags() == ['1.0', '1', 'latest', '1.0.0'] as LinkedHashSet
    this.subject.getAllRevisionTags() == ['1.0.0', '1.0', '1', 'latest'] as LinkedHashSet
    this.subject.isRevisionSemverTagged()
    this.subject.isRevisionProperlyTagged()
    this.subject.isRevisionProperlyTagged(false)
    !this.subject.doesRevisionTagContainsMetadata()
    !this.subject.isRevisionTagPreRelease()
    this.subject.isRevisionTagLatest()
    this.subject.getAllLocalTags() == ['1.0.0', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet
    this.subject.getRemoteRevisionSemverTag() == '1.0.0'
    this.subject.getAllRemoteRevisionFloatingTags() == ['1', '1.0', 'latest'] as LinkedHashSet
    this.subject.getAllRemoteRevisionTags() == ['1.0.0', '1.0', '1', 'latest'] as LinkedHashSet
    this.subject.getAllRemoteTags() == ['1.0.0', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet
    this.subject.getAllRemoteRevisionSemverAndFloatingTags() == ['1.0', '1', 'latest', '1.0.0'] as LinkedHashSet
    this.subject.getRemoteRevisionMissingFloatingTags().isEmpty()
    this.subject.isRemoteRevisionProperlyTagged()
    this.subject.isRemoteRevisionProperlyTagged(false)
    !this.subject.isPublishable()
    !this.subject.isPublishableAsPreRelease()
    !this.subject.isPublishableAsAnything()
    this.subject.extractPatchVersion() == '1.0.0'
    this.subject.extractPatchVersionNumber() == 0
    this.subject.extractMinorVersion() == '1.0'
    this.subject.extractMinorVersionNumber() == 0
    this.subject.extractMajorVersion() == '1'
    this.subject.extractMajorVersionNumber() == 1
    this.subject.extractPreReleaseVersion() == ''
    this.subject.extractMetadataVersion() == ''
    this.subject.getWorkspacePath() == '/some/path/to/workspace'
    this.subject.getWorkspaceFileTree() == ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
  }

  @Test
  def "it returns information about a publishable ScmInfo object"() {
    ScmInfo publishableSubject = new ScmInfo(
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      'master',
      'master',
      false,
      'origin',
      'jenkins-pipeline-library',
      'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library',
      ['1.0.0-1', '1.0', '1', 'latest'] as LinkedHashSet,
      ['1.0.0-1', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet,
      ['1.0.0-1', '1.0', '1', 'latest'] as LinkedHashSet,
      ['1.0.0-1', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet,
      '/some/path/to/workspace',
      ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
    )

    expect:
    publishableSubject.getRevisionCommitId() == 'c2fabdfb982f140eda1eb9567e3fff956ed9e440'
    publishableSubject.getLocalTipCommitId() == 'c2fabdfb982f140eda1eb9567e3fff956ed9e440'
    publishableSubject.getRemoteTipCommitId() == 'c2fabdfb982f140eda1eb9567e3fff956ed9e440'
    publishableSubject.isRevisionLatest()
    publishableSubject.getBranch() == 'master'
    publishableSubject.getBranchAsDockerTag() == 'master'
    publishableSubject.getDefaultBranch() == 'master'
    publishableSubject.isCurrentBranchDefault()
    publishableSubject.isCurrentCommitLatestAndOnDefaultBranch()
    !publishableSubject.isPullRequest()
    !publishableSubject.isMergeRequest()
    publishableSubject.getRemoteName() == 'origin'
    publishableSubject.getRepositoryName() == 'jenkins-pipeline-library'
    publishableSubject.getRepositoryURL() == 'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library'
    publishableSubject.getRevisionSemverTag() == '1.0.0-1'
    publishableSubject.getAllRevisionFloatingTags() == ['1.0', '1', 'latest'] as LinkedHashSet
    publishableSubject.getAllRevisionSemverAndFloatingTags() == ['1.0', '1', 'latest', '1.0.0-1'] as LinkedHashSet
    publishableSubject.getAllRevisionTags() == ['1.0.0-1', '1.0', '1', 'latest'] as LinkedHashSet
    publishableSubject.isRevisionSemverTagged()
    publishableSubject.isRevisionProperlyTagged()
    publishableSubject.isRevisionProperlyTagged(false)
    publishableSubject.doesRevisionTagContainsMetadata()
    !publishableSubject.isRevisionTagPreRelease()
    publishableSubject.isRevisionTagLatest()
    publishableSubject.getAllLocalTags() == ['1.0.0-1', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet
    publishableSubject.getRemoteRevisionSemverTag() == '1.0.0-1'
    publishableSubject.getAllRemoteRevisionFloatingTags() == ['1.0', '1', 'latest'] as LinkedHashSet
    publishableSubject.getAllRemoteRevisionTags() == ['1.0.0-1', '1.0', '1', 'latest'] as LinkedHashSet
    publishableSubject.getAllRemoteTags() == ['1.0.0-1', '1.0', '1', 'latest', '0.1.0', '0.1', '0'] as LinkedHashSet
    publishableSubject.getAllRemoteRevisionSemverAndFloatingTags() == ['1.0', '1', 'latest', '1.0.0-1'] as LinkedHashSet
    publishableSubject.getRemoteRevisionMissingFloatingTags().isEmpty()
    publishableSubject.isRemoteRevisionProperlyTagged()
    publishableSubject.isRemoteRevisionProperlyTagged(false)
    publishableSubject.isPublishable()
    !publishableSubject.isPublishableAsPreRelease()
    publishableSubject.isPublishableAsAnything()
    publishableSubject.extractPatchVersion() == '1.0.0'
    publishableSubject.extractPatchVersionNumber() == 0
    publishableSubject.extractMinorVersion() == '1.0'
    publishableSubject.extractMinorVersionNumber() == 0
    publishableSubject.extractMajorVersion() == '1'
    publishableSubject.extractMajorVersionNumber() == 1
    publishableSubject.extractPreReleaseVersion() == ''
    publishableSubject.extractMetadataVersion() == '1'
    publishableSubject.getWorkspacePath() == '/some/path/to/workspace'
    publishableSubject.getWorkspaceFileTree() == ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
  }

  @Test
  def "it returns information about a publishable ScmInfo object"() {
    ScmInfo splitHeadSubject = new ScmInfo(
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      '8a5c492750437311e0e05b00f637f5efc2c9fa9e',
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      'local/test',
      'master',
      false,
      'origin',
      'jenkins-pipeline-library',
      'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library',
      ['1.1.0-dev1'] as LinkedHashSet,
      ['1.1.0-dev1', '1.0.0', '1.0', '1', 'latest'] as LinkedHashSet,
      [] as LinkedHashSet,
      ['1.2.0', '1.1.0', '1.0.0', '1.0', '1.1', '1.2', '1', 'latest'] as LinkedHashSet,
      '/some/path/to/workspace',
      ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
    )

    expect:
    splitHeadSubject.getRevisionCommitId() == 'c2fabdfb982f140eda1eb9567e3fff956ed9e440'
    splitHeadSubject.getRemoteTipCommitId() == '8a5c492750437311e0e05b00f637f5efc2c9fa9e'
    splitHeadSubject.getLocalTipCommitId() == 'c2fabdfb982f140eda1eb9567e3fff956ed9e440'
    !splitHeadSubject.isRevisionLatest()
    splitHeadSubject.getBranch() == 'local/test'
    splitHeadSubject.getBranchAsDockerTag() == 'local_test'
    splitHeadSubject.getDefaultBranch() == 'master'
    !splitHeadSubject.isCurrentBranchDefault()
    !splitHeadSubject.isCurrentCommitLatestAndOnDefaultBranch()
    !splitHeadSubject.isPullRequest()
    !splitHeadSubject.isMergeRequest()
    splitHeadSubject.getRemoteName() == 'origin'
    splitHeadSubject.getRepositoryName() == 'jenkins-pipeline-library'
    splitHeadSubject.getRepositoryURL() == 'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library'
    splitHeadSubject.getRevisionSemverTag() == '1.1.0-dev1'
    splitHeadSubject.getAllRevisionFloatingTags() == [] as LinkedHashSet
    splitHeadSubject.getAllRevisionSemverAndFloatingTags() == ['1.1.0-dev1'] as LinkedHashSet
    splitHeadSubject.getAllRevisionTags() == ['1.1.0-dev1'] as LinkedHashSet
    splitHeadSubject.isRevisionSemverTagged()
    splitHeadSubject.isRevisionProperlyTagged()
    splitHeadSubject.isRevisionProperlyTagged(false)
    !splitHeadSubject.doesRevisionTagContainsMetadata()
    splitHeadSubject.isRevisionTagPreRelease()
    !splitHeadSubject.isRevisionTagLatest()
    splitHeadSubject.getAllLocalTags() == ['1.1.0-dev1', '1.0.0', '1.0', '1', 'latest'] as LinkedHashSet
    splitHeadSubject.getRemoteRevisionSemverTag() == ''
    splitHeadSubject.getAllRemoteRevisionFloatingTags().isEmpty()
    splitHeadSubject.getAllRemoteRevisionTags().isEmpty()
    splitHeadSubject.getAllRemoteTags() == ['1.2.0', '1.1.0', '1.0.0', '1.0', '1.1', '1.2', '1', 'latest'] as LinkedHashSet
    splitHeadSubject.getAllRemoteRevisionSemverAndFloatingTags().isEmpty()
    splitHeadSubject.getRemoteRevisionMissingFloatingTags() == ['1', '1.1', 'latest'] as LinkedHashSet
    !splitHeadSubject.isRemoteRevisionProperlyTagged(false)
    !splitHeadSubject.isRemoteRevisionProperlyTagged()
    !splitHeadSubject.isPublishable()
    splitHeadSubject.isPublishableAsPreRelease()
    splitHeadSubject.isPublishableAsAnything()
    splitHeadSubject.extractPatchVersion() == '1.1.0'
    splitHeadSubject.extractPatchVersionNumber() == 0
    splitHeadSubject.extractMinorVersion() == '1.1'
    splitHeadSubject.extractMinorVersionNumber() == 1
    splitHeadSubject.extractMajorVersion() == '1'
    splitHeadSubject.extractMajorVersionNumber() == 1
    splitHeadSubject.extractPreReleaseVersion() == 'dev1'
    splitHeadSubject.extractMetadataVersion() == ''
    splitHeadSubject.getWorkspacePath() == '/some/path/to/workspace'
    splitHeadSubject.getWorkspaceFileTree() == ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
  }

  @Test
  def "it extracts empty values when there are no semver version"() {
    ScmInfo noSemver = new ScmInfo(
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      '8a5c492750437311e0e05b00f637f5efc2c9fa9e',
      'c2fabdfb982f140eda1eb9567e3fff956ed9e440',
      'local/test',
      'master',
      false,
      'origin',
      'jenkins-pipeline-library',
      'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library',
      ['no_semver'] as LinkedHashSet,
      ['no_semver'] as LinkedHashSet,
      [] as LinkedHashSet,
      ['no_semver'] as LinkedHashSet,
      '/some/path/to/workspace',
      ['/some/path/to/workspace/file1', '/some/path/to/workspace/file2']
    )

    expect:
    !noSemver.isPublishableAsPreRelease()
    !noSemver.isPublishable()
    !noSemver.isPublishableAsAnything()
    !noSemver.isRevisionSemverTagged()
    !noSemver.isRemoveRevisionSemverTagged()
    !noSemver.isRevisionTagPreRelease()
    !noSemver.doesRevisionTagContainsMetadata()
    !noSemver.isRevisionProperlyTagged()
    !noSemver.isRevisionProperlyTagged(false)
    !noSemver.isRemoteRevisionProperlyTagged()
    !noSemver.isRemoteRevisionProperlyTagged(false)
    noSemver.extractPatchVersion() == ''
    noSemver.extractPatchVersionNumber() == null
    noSemver.extractMinorVersion() == ''
    noSemver.extractMinorVersionNumber() == null
    noSemver.extractMajorVersion() == ''
    noSemver.extractMajorVersionNumber() == null
    noSemver.extractPreReleaseVersion() == ''
    noSemver.extractMetadataVersion() == ''
  }
}
