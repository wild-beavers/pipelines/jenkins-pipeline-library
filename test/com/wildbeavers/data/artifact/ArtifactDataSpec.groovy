package com.wildbeavers.data.artifact

import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.data_validation.type.Version
import org.junit.Test
import spock.lang.Specification

class ArtifactDataSpec extends Specification {
  private ArtifactData subject

  def setup() {
    this.subject = new ArtifactData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getUrl() == null
    this.subject.getVersion() == ''
    this.subject.getAccessorName() == ''
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setUrl(new ShallowURL('https://some.site/artifact'))
    this.subject.setVersion(new Version('1.2.3'))
    this.subject.setAccessorName('accessor')

    then:
    this.subject.getUrl() == 'https://some.site/artifact'
    this.subject.getUrlStrict().getClass() == ShallowURL
    this.subject.getVersion() == '1.2.3'
    this.subject.getVersionStrict().getClass() == Version
    this.subject.getAccessorName() == 'accessor'
  }

  @Test
  def "it returns its trimmed path name"() {
    when:
    this.subject.setUrl(new ShallowURL(url))

    then:
    this.subject.getTrimmedPath() == result

    where:
    url                                                                 | result
    'https://some.site/artifact'                                        | ''
    's3://s3b-somewhere/path/to/file.gz'                                | 'path/to'
    's3://just-domain'                                                  | ''
    'ssh://git@gitlab.com:some/terraform/ecosystem-aws-games.git?ref=8' | 'terraform'
    'http://example.org/path/to/directory/'                             | 'path/to/directory'
  }

  @Test
  def "it returns its artifact name"() {
    when:
    this.subject.setUrl(new ShallowURL(url))

    then:
    this.subject.getArtifactName() == result

    where:
    url                                                                 | result
    'https://some.site/artifact'                                        | 'artifact'
    's3://s3b-somewhere/path/to/file.gz'                                | 'file.gz'
    's3://just-domain'                                                  | ''
    'ssh://git@gitlab.com:some/terraform/ecosystem-aws-games.git?ref=8' | 'ecosystem-aws-games.git'
    'http://example.org/path/to/directory/'                             | ''
  }

  @Test
  def "it returns its full name"() {
    when:
    this.subject.setUrl(new ShallowURL(url))
    if (version) {
      this.subject.setVersion(new Version(version))
    }

    then:
    this.subject.toString() == result

    where:
    url                                                                 | version  | result
    'https://some.site/artifact'                                        | '1.2.3'  | '1.2.3/artifact'
    'https://some.site/artifact'                                        | null     | 'artifact'
    's3://s3b-somewhere/path/to/file.gz'                                | 'latest' | 'path/to/latest/file.gz'
    's3://s3b-somewhere/path/to/file.gz'                                | null     | 'path/to/file.gz'
    's3://just-domain'                                                  | 'latest' | 'latest'
    's3://just-domain'                                                  | null     | ''
    'ssh://git@gitlab.com:some/terraform/ecosystem-aws-games.git?ref=8' | '1'      | 'terraform/1/ecosystem-aws-games.git'
    'ssh://git@gitlab.com:some/terraform/ecosystem-aws-games.git?ref=8' | null     | 'terraform/ecosystem-aws-games.git'
    'http://example.org/path/to/directory/'                             | 'latest' | 'path/to/directory/latest'
    'http://example.org/path/to/directory/'                             | null     | 'path/to/directory'
  }
}
