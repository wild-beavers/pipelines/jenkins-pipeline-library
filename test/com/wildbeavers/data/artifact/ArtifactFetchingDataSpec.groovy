package com.wildbeavers.data.artifact


import org.junit.Test
import spock.lang.Specification

class ArtifactFetchingDataSpec extends Specification {
  private ArtifactData data1 = Spy()
  private ArtifactData data2 = Spy()

  private ArtifactFetchingData subject

  def setup() {
    this.subject = new ArtifactFetchingData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactFetchingData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getArtifactData() == []
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setArtifactData([this.data1, this.data2])

    then:
    this.subject.getArtifactData() == [this.data1, this.data2]
  }

  @Test
  def "it adds artifact data variables"() {
    when:
    this.subject.setArtifactData([this.data2])
    this.subject.addArtifactData(this.data1)

    then:
    this.subject.getArtifactData() == [this.data2, this.data1]
  }
}
