package com.wildbeavers.data.artifact

import com.wildbeavers.data_validation.type.Version
import org.junit.Test
import spock.lang.Specification

class ArtifactPublisherDataSpec extends Specification {
  private ArtifactPublisherData subject
  private Version version = new Version('1.2.3')

  def setup() {
    this.subject = new ArtifactPublisherData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ArtifactPublisherData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getBaseUrl() == null
    this.subject.getCompressionAlgorithm() == 'tar.gz'
    this.subject.getPathToExclude() == ['CHANGELOG.md', '.pre-commit-config.yaml', 'LICENSE', 'README.md', 'Jenkinsfile']
    this.subject.getAccessorName() == null
    this.subject.getVersion() == null
    this.subject.getCompressionEnabled() == true
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setAccessorName('foo')
    this.subject.setVersion(this.version)
    this.subject.setCompressionAlgorithm('gz')
    this.subject.setCompressionEnabled(false)
    this.subject.setPathToExclude(['notyou.txt'])
    this.subject.setBaseUrl('foo/bar')

    then:
    this.subject.getAccessorName() == 'foo'
    this.subject.getVersion() == this.version
    this.subject.getCompressionAlgorithm() == 'gz'
    this.subject.getCompressionEnabled() == false
    this.subject.getPathToExclude() == ['notyou.txt']
    this.subject.getBaseUrl() == 'foo/bar'
  }
}
