package com.wildbeavers.data


import org.junit.Test
import spock.lang.Specification

class EmptyScmInfoSpec extends Specification {
  private ScmInfo subject

  def setup() {
    this.subject = new EmptyScmInfo()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == EmptyScmInfo
  }

  @Test
  def "all methods return exception"() {
    when:
    this.subject."$value"()

    then:
    Exception e = thrown(Exception)
    e.getMessage() =~ /Oops! You tried to use/

    where:
    value                                       || _
    'toString'                                  || _
    'getRevisionCommitId'                       || _
    'getLocalTipCommitId'                       || _
    'getRemoteTipCommitId'                      || _
    'isRevisionLatest'                          || _
    'getBranch'                                 || _
    'getBranchAsDockerTag'                      || _
    'getDefaultBranch'                          || _
    'isCurrentBranchDefault'                    || _
    'isCurrentCommitLatestAndOnDefaultBranch'   || _
    'isPullRequest'                             || _
    'isMergeRequest'                            || _
    'getRemoteName'                             || _
    'getRepositoryName'                         || _
    'getRepositoryURL'                          || _
    'getRevisionSemverTag'                      || _
    'getAllRevisionFloatingTags'                || _
    'getAllRevisionSemverAndFloatingTags'       || _
    'getAllRevisionTags'                        || _
    'isRevisionSemverTagged'                    || _
    'isRevisionProperlyTagged'                  || _
    'getRevisionMissingFloatingTags'            || _
    'isRemoteRevisionProperlyTagged'            || _
    'getRemoteRevisionMissingFloatingTags'      || _
    'doesRevisionTagContainsMetadata'           || _
    'isRevisionTagPreRelease'                   || _
    'isRevisionTagLatest'                       || _
    'getAllLocalTags'                           || _
    'getRemoteRevisionSemverTag'                || _
    'isRemoveRevisionSemverTagged'              || _
    'getAllRemoteRevisionFloatingTags'          || _
    'getAllRemoteRevisionTags'                  || _
    'getAllRemoteTags'                          || _
    'getAllRemoteRevisionSemverAndFloatingTags' || _
    'isPublishable'                             || _
    'isPublishableAsPreRelease'                 || _
    'isPublishableAsAnything'                   || _
    'extractPatchVersion'                       || _
    'extractPatchVersionNumber'                 || _
    'extractMinorVersion'                       || _
    'extractMinorVersionNumber'                 || _
    'extractMajorVersion'                       || _
    'extractMajorVersionNumber'                 || _
    'extractPreReleaseVersion'                  || _
    'extractMetadataVersion'                    || _
    'getWorkspacePath'                          || _
    'getWorkspaceFileTree'                      || _
  }

}
