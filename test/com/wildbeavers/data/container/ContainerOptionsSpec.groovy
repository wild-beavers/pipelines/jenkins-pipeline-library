package com.wildbeavers.data.container

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.OptionString
import org.junit.Test
import spock.lang.Specification

class ContainerOptionsSpec extends Specification {
  private ContainerOptions subject

  def setup() {
    this.subject = new ContainerOptions()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerOptions
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getTool() == ContainerOptions.DEFAULT_TOOL
    this.subject.getToolOptions().getClass() == OptionString
    this.subject.getFqdn() == ContainerOptions.DEFAULT_FQDN
    this.subject.getImageName() == ContainerOptions.DEFAULT_IMAGE_NAME
    this.subject.getImageTag() == ContainerOptions.DEFAULT_IMAGE_TAG
    this.subject.hasImage()
    this.subject.getImage() == ContainerOptions.DEFAULT_FQDN + '/' + ContainerOptions.DEFAULT_IMAGE_NAME + ':' + ContainerOptions.DEFAULT_IMAGE_TAG
    this.subject.getVolumes() == [:]
    this.subject.getEnvironmentVariables() == [:]
    this.subject.getNetwork() == ''
    !this.subject.getRunHasDaemon()
    this.subject.getName() == ''
    this.subject.getEntrypoint() == ''
    this.subject.getUserns() == ''
    this.subject.getRemoveAfterRun()
    !this.subject.getPrivileged()
    this.subject.hasImage() == true
    this.subject.getBasepathAsWorkdir()
    this.subject.getFallbackCommand() == ''
    !this.subject.getQuiet()
    !this.subject.getReadOnly()
    !this.subject.getTransientStore()
    this.subject.getLogDriver() == ''
    this.subject.getPull() == ''
    !this.subject.getAssumeRuntimeInstalled()
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setTool('podman')
    this.subject.setImageName('test')
    this.subject.setImageTag('3.0.0')
    this.subject.setFqdn('example.org')
    this.subject.setImage('this has no effects and is ignored')
    this.subject.setVolumes(['first': 'mount'])
    this.subject.setEnvironmentVariables(['first': 'env'])
    this.subject.setNetwork('network')
    this.subject.setRunHasDaemon(true)
    this.subject.setName('name')
    this.subject.setEntrypoint('entrypoint')
    this.subject.setUserns('host')
    this.subject.setRemoveAfterRun(true)
    this.subject.setPrivileged(true)
    this.subject.setBasepathAsWorkdir(false)
    this.subject.setFallbackCommand('fallback')
    this.subject.setQuiet(true)
    this.subject.setReadOnly(true)
    this.subject.setTransientStore(true)
    this.subject.setLogDriver('none')
    this.subject.setPull('always')
    this.subject.setAssumeRuntimeInstalled(true)

    then:
    this.subject.getTool() == 'podman'
    this.subject.getImageName() == 'test'
    this.subject.getImageTag() == '3.0.0'
    this.subject.getFqdn() == 'example.org'
    this.subject.hasImage()
    this.subject.getVolumes() == ['first': 'mount']
    this.subject.getEnvironmentVariables() == ['first': 'env']
    this.subject.getNetwork() == 'network'
    this.subject.getRunHasDaemon()
    this.subject.getName() == 'name'
    this.subject.getEntrypoint() == 'entrypoint'
    this.subject.getUserns() == 'host'
    this.subject.getRemoveAfterRun()
    this.subject.getPrivileged()
    this.subject.hasImage() == true
    !this.subject.getBasepathAsWorkdir()
    this.subject.getFallbackCommand() == 'fallback'
    this.subject.getQuiet()
    this.subject.getReadOnly()
    this.subject.getTransientStore()
    this.subject.getLogDriver() == 'none'
    this.subject.getPull() == 'always'
    this.subject.getAssumeRuntimeInstalled()
  }

  @Test
  def "it adds environment variables"() {
    when:
    this.subject.setEnvironmentVariables([first: 1, second: 'nope'])
    this.subject.addEnvironmentVariables([third: 3, second: 2])

    then:
    this.subject.getEnvironmentVariables() == [first: 1, second: 2, third: 3]
  }

  @Test
  def "it adds volumes"() {
    when:
    this.subject.setVolumes([first: 1, second: 'nope'])
    this.subject.addVolumes([third: 3, second: 2])

    then:
    this.subject.getVolumes() == [first: 1, second: 2, third: 3]
  }

  @Test
  def "it removes a volume"() {
    when:
    this.subject.setVolumes([first: 1, second: 'nope'])
    this.subject.removeVolume('second')

    then:
    this.subject.getVolumes() == [first: 1]
  }

  @Test
  def "it removes an environment variable"() {
    when:
    this.subject.setEnvironmentVariables([first: 1, second: 'nope'])
    this.subject.removeEnvironmentVariable('second')

    then:
    this.subject.getEnvironmentVariables() == [first: 1]
  }

  @Test
  def "it does nothing when removing an unexisting additional mount"() {
    when:
    this.subject.setVolumes([first: 1, second: 'nope'])
    this.subject.removeVolume('unknown')

    then:
    this.subject.getVolumes() == [first: 1, second: 'nope']
  }

  @Test
  def "it sets up image through split data"() {
    when:
    this.subject.setImageName('this_image')
    this.subject.setImageTag('1.1.1')
    this.subject.setFqdn('example.org/toimage')

    then:
    this.subject.getImage() == 'example.org/toimage/this_image:1.1.1'
  }

  @Test
  def "it does nothing when removing an unexisting environment variable"() {
    when:
    this.subject.setEnvironmentVariables([first: 1, second: 'nope'])
    this.subject.removeEnvironmentVariable('unknown')

    then:
    this.subject.getEnvironmentVariables() == [first: 1, second: 'nope']
  }

  @Test
  def "it can set image with fqdn, name and tag directly"() {
    when:
    this.subject.setImage(' ', '', '')

    then:
    !this.subject.hasImage()

    when:
    this.subject.setImage('this', 'is', 'test')

    then:
    this.subject.hasImage()
    this.subject.getImage() == 'this/is:test'
  }
}
