package com.wildbeavers.data.container

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.ContainerRegistryOption
import org.junit.Test
import spock.lang.Specification

class ContainerRegistryOptionSpec extends Specification {
  private ContainerOptions containerOptions = Spy()
  private ContainerOptions containerOptions2 = Spy()
  private ContainerRegistryOption subject

  def setup() {
    this.subject = new ContainerRegistryOption(this.containerOptions)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ContainerRegistryOption
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getFqdn() == ContainerRegistryOption.DEFAULT_FQDN
    this.subject.getCredentialId() == ''
    this.subject.getCredentialProcessContainerRunCommandArguments() == 'echo "this is not a password"'
    this.subject.getCredentialProcessUsername() == ''
    this.subject.getCredentialProcessContainerOptions() == this.containerOptions
    this.subject.getContainerOptions() == this.containerOptions
    !this.subject.hasCredentialId()
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setFqdn('test.com')
    this.subject.setCredentialProcessContainerOptions(this.containerOptions)
    this.subject.setCredentialProcessContainerRunCommandArguments('command to get password')
    this.subject.setCredentialProcessUsername('username')
    this.subject.setCredentialId('credentials')

    then:
    this.subject.getFqdn() == 'test.com'
    this.subject.getCredentialProcessContainerOptions() == this.containerOptions
    this.subject.getContainerOptions() == this.containerOptions
    this.subject.getCredentialProcessContainerRunCommandArguments() == 'command to get password'
    this.subject.getCredentialProcessUsername() == 'username'
    this.subject.getCredentialId() == 'credentials'
    this.subject.hasCredentialId()
  }

  @Test
  def "it tests whether credentials are empty or null"() {
    when:
    this.subject.setCredentialId('')

    then:
    !this.subject.hasCredentialId()

    when:
    this.subject.setCredentialId(null)

    then:
    !this.subject.hasCredentialId()
  }


  @Test
  def "it has an working alias for credentialProcessContainerOptions"() {
    when:
    this.subject.setContainerOptions(this.containerOptions2)

    then:
    this.subject.getContainerOptions() == this.containerOptions2
    this.subject.getCredentialProcessContainerOptions() == this.containerOptions2
  }
}
