package com.wildbeavers.data.pre_commit

import com.wildbeavers.container.data.ContainerOptions
import org.junit.Test
import spock.lang.Specification

class PreCommitDataSpec extends Specification {
  private ContainerOptions containerOptions = Spy()
  private PreCommitData subject

  def setup() {
    this.subject = new PreCommitData()
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == PreCommitData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getRunCommandArguments() == PreCommitData.DEFAULT_RUN_COMMAND_ARGUMENTS
    this.subject.getEnabled()
    this.subject.isEnabled()
    this.subject.getContainerOptions() == null
  }

  @Test
  def "it sets/gets its internal values"() {
    when:
    this.subject.setRunCommandArguments('this in a test --command')
    this.subject.setEnabled(false)
    this.subject.setContainerOptions(this.containerOptions)

    then:
    !this.subject.getEnabled()
    !this.subject.isEnabled()
    this.subject.getContainerOptions() == this.containerOptions
    this.subject.getRunCommandArguments() == 'this in a test --command'
  }
}
