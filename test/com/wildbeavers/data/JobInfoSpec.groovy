package com.wildbeavers.data

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.RunWrapperMock
import org.junit.Test
import spock.lang.Specification

class JobInfoSpec extends Specification {
  private GroovyScriptMock context = Mock()
  private RunWrapperMock runWrapper = Mock()
  private JobInfo subject

  def setup() {
    this.runWrapper.getBuildCauses() >> [['userId': 'testUser']]
    this.context.currentBuild >> this.runWrapper
    this.subject = new JobInfo(this.context)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == JobInfo
  }

  @Test
  def "it displays information about itself"() {
    expect:
    this.subject.toString() =~ 'Is manually triggered'
    this.subject.toString() =~ 'Triggered user'
  }

  @Test
  def "it says when the build was manually triggered"() {
    expect:
    this.subject.isManuallyTriggered()
  }

  @Test
  def "it says when the build was not manually triggered"() {
    when:
    GroovyScriptMock context = Mock()
    RunWrapperMock runWrapper = Mock()
    runWrapper.getBuildCauses() >> [['not': 'a_user']]
    context.currentBuild >> runWrapper
    def subject = new JobInfo(context)

    then:
    ! subject.isManuallyTriggered()
    subject.getTriggeredUser() == ''
  }

  @Test
  def "it shows the user who triggered the build"() {
    expect:
    this.subject.getTriggeredUser() == 'testUser'
  }
}
