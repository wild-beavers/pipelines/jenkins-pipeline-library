package com.wildbeavers.butane.event_data


import com.wildbeavers.container.data.ContainerOptions
import org.junit.Test
import spock.lang.Specification

class ButaneEventDataSpec extends Specification {
  private ContainerOptions containerOptionsAlt = Spy()
  private ContainerOptions containerOptionsDefault = Spy()

  private ButaneEventData subject

  def setup() {
    this.subject = new ButaneEventData(this.containerOptionsDefault)
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == ButaneEventData
  }

  @Test
  def "it has default options"() {
    expect:
    this.subject.getContainerOptions() == this.containerOptionsDefault
  }

  @Test
  def "it sets and gets its values"() {
    when:
    this.subject.setContainerOptions(this.containerOptionsAlt)

    then:
    this.containerOptionsAlt == this.subject.getContainerOptions()
  }
}
