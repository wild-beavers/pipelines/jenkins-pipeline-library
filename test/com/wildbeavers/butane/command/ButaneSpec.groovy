package com.wildbeavers.butane.command


import com.wildbeavers.command.AbstractCommandSetup
import com.wildbeavers.exception.CommandBuildingException
import org.junit.Test

class ButaneSpec extends AbstractCommandSetup {
  private Butane subject

  def setup() {
    this.subject = new Butane(this.optionStringFactory, this.containerRunner, this.debugger)
  }

  @Test
  def "can be instantiated"() {
    expect:
    this.subject.getClass() == Butane
  }

  @Test
  def "throws an exception when passed subcommand is not valid"() {
    when:
    this.subject.run('dummy', ['-arg': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Subcommand “dummy” is not valid/
  }

  @Test
  def "throws an exception when passed argument is not valid"() {
    when:
    this.subject.run('', ['-invalid': 1], this.dockerOptionData)

    then:
    def e = thrown(CommandBuildingException)
    e =~ /Argument “-invalid” is not valid/
  }

  @Test
  def "it throws an exception if the command run fails for any reason"() {
    given:
    this.containerRunner.run(*_) >> {
      throw new Exception()
    }

    when:
    this.subject.run('', ['-s': true], this.dockerOptionData)

    then:
    thrown(Exception)
  }

  @Test
  def "runs a command"() {
    when:
    this.subject.run('', ['-s': true, '-o': 'test.ign'], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-o', 'test.ign', CharSequence)
    1 * this.optionStringFactory.addOption('-s', '')
    1 * this.containerRunner.run(this.containerOptions,  '-param 1')
  }

  @Test
  def "it runs with prefixing the main command"() {
    when:
    this.subject.runWithPrefix('', ['-s': true, '-o': 'test.ign'], this.dockerOptionData)

    then:
    1 * this.optionStringFactory.addOption('-o', 'test.ign', CharSequence)
    1 * this.optionStringFactory.addOption('-s', '')
    1 * this.containerRunner.run(this.containerOptions,  'butane -param 1')
  }

  def "it can show its documentation"() {
    when:
    def result = this.subject.getDocumentation()

    then:
    result =~ /COMMAND: butane/
    result =~ /-h/
    result =~ /description: Show usage and exit/
  }

  def "it can show a subcommand documentation"() {
    when:
    def result = this.subject.getDocumentation('')

    then:
    result =~ /COMMAND: butane/
  }
}
