package com.wildbeavers.butane.listener

import com.wildbeavers.DataValidatorDecorator
import com.wildbeavers.butane.event_data.ButaneEventData
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ButaneDataValidationListenerSpec extends Specification {
  private GenericFactory<ButaneEventData> butaneEventDataGenericFactory = Mock()
  private DataValidatorDecorator dataValidator = new DataValidatorDecorator()

  Map config = [
    butane_containerTool      : 'podman',
    butane_containerFqdn      : 'fqdn.com/path',
    butane_containerImageName : 'image',
    butane_containerImageTag  : 'latest',
    butane_containerPrivileged: true,
    butane_containerNetwork   : 'network',
  ]
  private ControllerEventData controllerEventData = Spy()
  private RunnerEventData runnerEventData = Spy()
  private FileStandardData fileStandardData = Spy()

  private ButaneDataValidationListener subject

  def setup() {
    this.butaneEventDataGenericFactory.instantiate() >> { return new ButaneEventData(new ContainerOptions()) }
    this.runnerEventData.isFileStandardIgnoreDefault() >> false
    this.runnerEventData.getFileStandardData() >> this.fileStandardData
    this.controllerEventData.getRawConfig() >> this.config
    this.controllerEventData.getRunnerEventData() >> this.runnerEventData

    this.subject = new ButaneDataValidationListener(this.butaneEventDataGenericFactory, this.dataValidator)
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ButaneDataValidationListener
  }

  @Test
  def "it listens to the prepare event"() {
    expect:
    [ControllerEvents.DATA_VALIDATION] == this.subject.listenTo()
  }

  @Test
  def "it validates raw input and fills event data object"() {
    when:
    this.subject.run(this.controllerEventData)

    then:
    'podman' == this.runnerEventData.getMainEventData().getContainerOptions().getTool()
    'fqdn.com/path/image:latest' == this.runnerEventData.getMainEventData().getContainerOptions().getImage()
    this.runnerEventData.getMainEventData().getContainerOptions().getPrivileged()
    'network' == this.runnerEventData.getMainEventData().getContainerOptions().getNetwork()

    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('main.yaml')
    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('.gitignore')
    this.runnerEventData.getFileStandardData().getMandatoryFiles().contains('CHANGELOG.md')
    this.runnerEventData.getFileStandardData().getForbiddenFiles().contains('main.ign')
  }

  @Test
  def "it does not add any mandatory/forbidden files when the ignore toggle is true"() {
    when:
    this.subject.run(this.controllerEventData)

    then:
    this.runnerEventData.isFileStandardIgnoreDefault() >> true

    [] == this.runnerEventData.getFileStandardData().getMandatoryFiles()
    [] == this.runnerEventData.getFileStandardData().getForbiddenFiles()
  }
}
