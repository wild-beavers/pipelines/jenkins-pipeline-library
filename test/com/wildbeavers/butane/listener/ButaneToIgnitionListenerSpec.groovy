package com.wildbeavers.butane.listener

import com.wildbeavers.GroovyScriptMock
import com.wildbeavers.butane.command.Butane
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.butane.event_data.ButaneEventData
import com.wildbeavers.event_data.RunnerEventData
import org.junit.Test
import spock.lang.Specification

class ButaneToIgnitionListenerSpec extends Specification {
  GroovyScriptMock context = Mock()
  Butane butane = Mock()
  ButaneEventData butaneEventData = Mock()
  RunnerEventData runnerEventData = Mock()
  ButaneToIgnitionListener subject

  def setup() {
    this.subject = new ButaneToIgnitionListener(this.context, this.butane)

    this.runnerEventData.getMainEventData() >> butaneEventData
  }

  @Test
  def "it can be instantiated"() {
    expect:
    this.subject.getClass() == ButaneToIgnitionListener
  }

  @Test
  def "it listens to the pipeline pre-prepare event"() {
    expect:
    [RunnerEvents.BUILD] == this.subject.listenTo()
  }

  @Test
  def "it runs a docker command to convert butane to ignition and stores its artifact"() {
    when:
    def result = this.subject.run(this.runnerEventData)

    then:
    1 * this.butane.run('', _, this.butaneEventData)
    1 * this.context.archiveArtifacts([artifacts: ButaneToIgnitionListener.IGNITION_FILE])
    1 * this.context.archiveArtifacts([artifacts: ButaneToIgnitionListener.BUTANE_FILE])
    result == this.runnerEventData
  }
}
