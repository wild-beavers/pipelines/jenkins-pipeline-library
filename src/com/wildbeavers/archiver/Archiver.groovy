package com.wildbeavers.archiver

import com.wildbeavers.data.CommandResult

/**
 * Abstract facade representing an Archiver. It will get the relevant implementation.
 *  This would ultimately need to become a packer/unpacker system, to better fit with situation when archives are not involved.
 */
class Archiver {
  private ArchiverCollection archiverCollection

  Archiver(ArchiverCollection archiverCollection) {
    this.archiverCollection = archiverCollection
  }

  /**
   * Fetch compatible Archiver and compress all $pathsToArchive together under a $archiveName
   * @param pathsToArchive Paths (maybe files or directories) to archive
   * @param archiveName Destination archive name
   * @return CommandResult
   */
  CommandResult compress(List<String> pathsToArchive, String archiveName) {
    return this.archiverCollection.getArchiverByArchiveName(archiveName).compress(pathsToArchive, archiveName)
  }

  /**
   * Fetch compatible Archiver and uncompress an archive $archivePath to a $destinationPath
   * @param archivePath Archive path and archive name to uncompress
   * @param destinationPath Destination where to uncompress the archive
   * @return CommandResult
   */
  CommandResult uncompress(String archivePath, String destinationPath) {
    return this.archiverCollection.getArchiverByArchiveName(archivePath).uncompress(archivePath, destinationPath)
  }
}
