package com.wildbeavers.archiver

import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.OptionString
import com.wildbeavers.proxy.ExecuteProxy

/**
 * Archiver implementation handling .tar.gz archives
 */
class TarGzArchiver implements IsArchiver {
  final static String SUPPORTED_ALGORITHM = 'tar.gz'

  private ExecuteProxy executeProxy

  TarGzArchiver(ExecuteProxy executeProxy) {
    this.executeProxy = executeProxy
  }

  @Override
  CommandResult compress(List<String> pathsToArchive, String archiveName) {
    OptionString optionString = new OptionString()

    optionString.addOption('-name', pathsToArchive[0])
    pathsToArchive.remove(0)
    optionString.addOption('-o -name', pathsToArchive)

    this.executeProxy.execute("find * ${optionString.toString()} | tar -czf ${archiveName} -T -")
  }

  @Override
  CommandResult uncompress(String archivePath, String destinationPath) {
    if (destinationPath != './') {
      this.executeProxy.execute("mkdir -p ${destinationPath}")
    }

    this.executeProxy.execute("tar -xf ./${archivePath} -C ${destinationPath}")
  }

  @Override
  Boolean requirementsInstalled() {
    return 0 == this.executeProxy.executeAndAllowError("which tar").getStatusCode()
  }

  @Override
  Boolean supportArchive(String archiveName) {
    return archiveName =~ /\.tar\.gz$/
  }

  @Override
  String getSupportedAlgorithm() {
    return SUPPORTED_ALGORITHM
  }
}
