package com.wildbeavers.archiver

import com.wildbeavers.data.CommandResult

/**
 *  This would ultimately need to become a packer/unpacker system, to better fit with situation when archives are not involved.
 */
interface IsArchiver {
  /**
   * Compress all $pathsToArchive together under a $archiveName
   * @param pathsToArchive Paths (maybe files or directories) to archive
   * @param archiveName Destination archive name
   * @return CommandResult
   */
  CommandResult compress(List<String> pathsToArchive, String archiveName)

  /**
   * Uncompress an archive $archivePath to a $destinationPath
   * @param archivePath Archive path and archive name to uncompress
   * @param destinationPath Destination where to uncompress the archive
   * @return CommandResult
   */
  CommandResult uncompress(String archivePath, String destinationPath)

  /**
   * Whether or not the current class supports the $archiveName for compress/uncompress
   * @param archiveName Archive to check - depends on the archive extension
   * @return
   */
  Boolean supportArchive(String archiveName)

  /**
   * Whether or not the underlying tools needed for the archiver are installed in the running context.
   * @return
   */
  Boolean requirementsInstalled()

  /**
   * Gets the name of the supported algorithm for the current class
   * @return
   */
  String getSupportedAlgorithm()
}
