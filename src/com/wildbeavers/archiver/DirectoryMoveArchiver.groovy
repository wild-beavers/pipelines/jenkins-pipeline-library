package com.wildbeavers.archiver

import com.wildbeavers.data.CommandResult
import com.wildbeavers.proxy.ExecuteProxy

/**
 * Archiver implementation handling simple directory moving
 *  This would ultimately need to become a packer/unpacker system, to better fit with situation when archives are not involved.
 */
class DirectoryMoveArchiver implements IsArchiver {
  final static String SUPPORTED_ALGORITHM = 'directory move'

  private ExecuteProxy executeProxy

  DirectoryMoveArchiver(ExecuteProxy executeProxy) {
    this.executeProxy = executeProxy
  }

  @Override
  CommandResult compress(List<String> pathsToArchive, String archiveName) {
    for (pathToArchive in pathsToArchive) {
      this.executeProxy.execute("mv '${pathToArchive}' '${archiveName}'")
    }
  }

  @Override
  CommandResult uncompress(String archivePath, String destinationPath) {
    this.executeProxy.execute("mv '${archivePath}' '${destinationPath}'")
  }

  @Override
  Boolean requirementsInstalled() {
    return true
  }

  @Override
  Boolean supportArchive(String archiveName) {
    return this.executeProxy.executeAndAllowError("test -d '${archiveName}'").getStatusCode() == 0
  }

  @Override
  String getSupportedAlgorithm() {
    return SUPPORTED_ALGORITHM
  }
}
