package com.wildbeavers.archiver


import com.wildbeavers.exception.ConfigurationException

/**
 * Collection of Archivers objects.
 */
class ArchiverCollection {
  private List<IsArchiver> archivers = []

  /**
   * Returns all implementations of Archiver.
   * @return
   */
  List<IsArchiver> getAllArchivers() {
    return this.archivers
  }

  /**
   * Return the first Archiver implementation supporting the $archiveName
   * @param archiveName
   * @return
   */
  IsArchiver getArchiverByArchiveName(String archiveName) {
    for (artifactStore in this.archivers) {
      if (artifactStore.supportArchive(archiveName)) {
        return artifactStore
      }
    }

    throw new ConfigurationException(
      "No Archive implementation support an archive named: “${archiveName}”. Supported algorithms are:\n" +
        this.getSupportedAlgorithms().join(", ") +
        "\nAdding a new algorithm is not hard! Your contribution is welcome."
    )
  }

  void addArchiver(IsArchiver archiver) {
    if (this.archivers.contains(archiver)) {
      return
    }

    this.archivers.add(archiver)
  }

  private List<String> getSupportedAlgorithms() {
    return this.archivers.collect {it.getSupportedAlgorithm() }
  }
}
