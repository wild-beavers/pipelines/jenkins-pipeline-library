package com.wildbeavers.io

import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.PrintProxy

class Debugger {
  private PrintProxy printProxy
  private EnvironmentVariableProxy environmentVariableProxy

  Debugger(PrintProxy printProxy, EnvironmentVariableProxy environmentVariableProxy) {
    this.printProxy = printProxy
    this.environmentVariableProxy = environmentVariableProxy
  }

  /**
   * Prints a content only if the debug exists.
   */
  void printDebug(Object content) {
    if (this.debugVarExists()) {
      this.printProxy.println("-------- debug --------")
      this.printProxy.println(content)
      this.printProxy.println("------ end debug ------")
    }
  }

  /**
   * Prints a content in the current context.
   */
  void print(Object content) {
    this.printProxy.println(content)
  }

  /**
   * Checks whether or not the debug variable is set.
   */
  Boolean debugVarExists() {
    return this.environmentVariableProxy.exist('DEBUG')
  }

  /**
   * Decorates a ${condition}. If failed, display ${failConditionMessage} as a debug message.
   * @return Boolean unmodified ${conditions}
   */
  Boolean testCondition(Boolean condition, CharSequence failConditionMessage) {
    if (!condition) {
      this.printDebug(failConditionMessage)
    }

    return condition
  }
}
