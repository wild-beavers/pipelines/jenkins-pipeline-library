package com.wildbeavers.io

/**
 * Allow to manipulate and get information from files in a restricted context like Jenkins
 */
class File {
  static final TYPE_GZIP = 'application/gzip'
  static final TYPE_JSON = 'text/json'
  static final TYPE_YAML = 'text/yaml'
  static final TYPE_PLAIN = 'text/plain'

  /**
   * Guess the mime type of a file.\n
   * Should be easy with the File object, but it is unavailable to use in Jenkins.
   * @param filename
   */
  String guessMimeType(String filename) {
    if (filename =~ /tar\.gz$/) {
      return TYPE_GZIP
    }

    if (filename =~ /\.json$/) {
      return TYPE_JSON
    }

    if (filename =~ /\.yaml$/) {
      return TYPE_YAML
    }

    return TYPE_PLAIN
  }
}
