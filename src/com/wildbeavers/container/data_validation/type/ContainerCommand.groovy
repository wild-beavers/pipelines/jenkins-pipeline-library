package com.wildbeavers.container.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data_validation.type.generic.Regexp

class ContainerCommand extends Regexp {
  ContainerCommand(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return "^(${ContainerDockerEventData.DEFAULT_TOOL}|${ContainerPodmanEventData.DEFAULT_TOOL}|dockerx) [a-z _-]+\$"
  }
}
