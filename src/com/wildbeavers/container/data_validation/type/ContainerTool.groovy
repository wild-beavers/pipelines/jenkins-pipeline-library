package com.wildbeavers.container.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data_validation.type.generic.Enum

class ContainerTool extends Enum {
  ContainerTool(CharSequence input) {
    super(input)
  }

  @Override
  @NonCPS
  List<CharSequence> getSupportedValues() {
    return [ContainerDockerEventData.DEFAULT_TOOL, ContainerPodmanEventData.DEFAULT_TOOL]
  }
}
