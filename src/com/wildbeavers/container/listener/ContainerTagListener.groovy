package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ExecuteProxy

/**
 * Tag a container or a manifest.
 */
class ContainerTagListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  private ScmInfo scmInfo
  private ExecuteProxy executeProxy

  ContainerTagListener(ScmInfo scmInfo, ExecuteProxy executeProxy) {
    this.scmInfo = scmInfo
    this.executeProxy = executeProxy
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_DEPLOY]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData) {
    return scmInfo.isPublishableAsAnything()
  }

  @Override
  ArrayList<CharSequence> explainNoRun(RunnerEventData eventData) {
    return ['Container was not tagged, because it’s not publishable.']
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as ContainerEventData)

    return eventData
  }


  ContainerEventData doRun(ContainerEventData eventData) {
    this.executeProxy.executeWithTail("${eventData.getTagCommand()} ${eventData.getAlias()} ${this.scmInfo.getRevisionCommitId()}")

    if (this.scmInfo.isPublishable()) {
      for (tag in this.scmInfo.getAllRevisionSemverAndFloatingTags()) {
        this.executeProxy.executeWithTail("${eventData.getTagCommand()} ${eventData.getAlias()} ${tag}")
      }
    } else {
      this.executeProxy.executeWithTail("${eventData.getTagCommand()} ${eventData.getAlias()} ${this.scmInfo.extractPreReleaseVersion()}")
    }

    return eventData
  }
}
