package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ExecuteProxy

class ContainerInspectListener extends EventListener<RunnerEventData> {
  private ExecuteProxy executeProxy

  ContainerInspectListener(ExecuteProxy executeProxy) {
    this.executeProxy = executeProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_DEPLOY]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData) {
    return eventData.getMainEventData().getInspectEnabled()
  }

  @Override
  ArrayList<CharSequence> explainNoRun(RunnerEventData eventData) {
    return ['Container inspection is disabled.']
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as ContainerEventData)

    return eventData
  }

  void doRun(ContainerEventData eventData) {
    this.executeProxy.executeWithTail("${eventData.getInspectCommand()} ${eventData.getAlias()}")
  }
}
