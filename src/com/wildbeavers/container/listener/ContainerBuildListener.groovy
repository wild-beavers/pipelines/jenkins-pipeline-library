package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.data.OptionString
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.observer.EventListener

class ContainerBuildListener extends EventListener<RunnerEventData> {
  private ExecuteProxy executeProxy

  ContainerBuildListener(ExecuteProxy executeProxy) {
    this.executeProxy = executeProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.COMPILE]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as ContainerEventData)

    return eventData
  }

  ContainerEventData doRun(ContainerEventData eventData) {
    OptionString optionString = new OptionString()

    optionString.addOption('--authfile', eventData.getAuthfile(), false)
    optionString.addOption('--file', eventData.getMakefilePath(), false)
    optionString.addOption('--format', eventData.getManifestFormat(), false)
    optionString.addOption('--platform', eventData.getBuildPlatforms(), false)
    optionString.addOption('--compress', eventData.getBuildCompress())
    optionString.addOption('--build-arg-file', eventData.getBuildArgFile(), false)
    optionString.addOption('--cache-from', eventData.getBuildCacheFrom(), false)
    optionString.addOption('--network', eventData.getBuildNetwork(), false)
    optionString.addOption('--pull', eventData.getBuildPull(), false)
    optionString.addOption('--tag', eventData.getBuildTag(), false)
    optionString.addOption('--quiet', eventData.getBuildQuiet())

    if (eventData.getBuildPlatforms()?.size() != 0 && eventData.getManifestName()) {
      optionString.addOption('--manifest', eventData.getManifestName(), false)
    }

    for (key in eventData.getBuildArgs()?.keySet()) {
      if (eventData.getBuildArgs().get(key)) {
        optionString.addOption('--build-arg', "${key}=\'${eventData.getBuildArgs().get(key)}\'")
      }
    }

    this.executeProxy.executeWithTail("${eventData.getBuildCommand()} --rm ${optionString.toString()} ${eventData.getBuildTarget()}")

    return eventData
  }
}
