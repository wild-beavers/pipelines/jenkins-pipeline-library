package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener

/**
 * Find a suiting alias for the container if not provided by the end user
 */
class ContainerAliasGuesserListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  private ScmInfo scmInfo

  ContainerAliasGuesserListener(ScmInfo scmInfo) {
    this.scmInfo = scmInfo
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_COMPILE]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData) {
    return !eventData.getMainEventData().getAlias()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as ContainerEventData)

    return eventData
  }

  void doRun(ContainerEventData eventData) {
    if (eventData.getBuildPlatforms() && eventData.getBuildPlatforms().size() != 0) {
      eventData.setManifestName(this.scmInfo.getRevisionCommitId())
    } else {
      eventData.setBuildTag(this.scmInfo.getRevisionCommitId())
    }
  }
}
