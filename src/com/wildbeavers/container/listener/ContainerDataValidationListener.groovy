package com.wildbeavers.container.listener

import com.wildbeavers.container.data_validation.type.ContainerCommand
import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for container and fill data object.
 */
class ContainerDataValidationListener extends EventListener<ControllerEventData> {
  private GenericFactory<ContainerEventData> containerEventDataFactory
  private GenericFactory<LinterData> linterDataGenericFactory
  private DataValidator dataValidator

  ContainerDataValidationListener(GenericFactory<ContainerEventData> containerEventDataFactory, GenericFactory<LinterData> linterDataGenericFactory, DataValidator dataValidator) {
    this.containerEventDataFactory = containerEventDataFactory
    this.linterDataGenericFactory = linterDataGenericFactory
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    ContainerEventData containerEventData = containerEventDataFactory.instantiate(
      this.dataValidator.validate(config, 'container_tool', ContainerTool, ContainerPodmanEventData.DEFAULT_TOOL).toString()
    )

    // Values below are not correctly validated
    containerEventData.setAuthfile(
      this.dataValidator.validate(config, 'container_authfile', String, containerEventData.getAuthfile())
    )
    containerEventData.setMakefilePath(
      this.dataValidator.validate(config, 'container_makefilePath', String, containerEventData.getMakefilePath())
    )
    containerEventData.setManifestName(
      this.dataValidator.validate(config, 'container_manifestName', String, containerEventData.getManifestName())
    )
    containerEventData.setManifestFormat(
      this.dataValidator.validate(config, 'container_manifestFormat', String, containerEventData.getManifestFormat())
    )

    containerEventData.setBuildPlatforms(
      this.dataValidator.validate(config, 'container_buildPlatforms', List, containerEventData.getBuildPlatforms())
    )
    containerEventData.setBuildCompress(
      this.dataValidator.validate(config, 'container_buildCompress', Boolean, containerEventData.getBuildCompress())
    )
    containerEventData.setBuildArgFile(
      this.dataValidator.validate(config, 'container_buildArgFile', String, containerEventData.getBuildArgFile())
    )
    containerEventData.setBuildCacheFrom(
      this.dataValidator.validate(config, 'container_buildCacheFrom', String, containerEventData.getBuildCacheFrom())
    )
    containerEventData.setBuildCommand(
      this.dataValidator.validate(config, 'container_buildCommand', ContainerCommand, containerEventData.getBuildCommand())
    )
    containerEventData.setBuildNetwork(
      this.dataValidator.validate(config, 'container_buildNetwork', String, containerEventData.getBuildNetwork())
    )
    containerEventData.setBuildPull(
      this.dataValidator.validate(config, 'container_buildPull', String, containerEventData.getBuildPull())
    )
    containerEventData.setBuildTag(
      this.dataValidator.validate(config, 'container_buildTag', String, containerEventData.getBuildTag())
    )
    containerEventData.setBuildQuiet(
      this.dataValidator.validate(config, 'container_buildQuiet', Boolean, containerEventData.getBuildQuiet())
    )
    containerEventData.setBuildTarget(
      this.dataValidator.validate(config, 'container_buildTarget', String, containerEventData.getBuildTarget())
    )
    containerEventData.setBuildArgs(
      this.dataValidator.validate(config, 'container_buildArgs', Map, containerEventData.getBuildArgs())
    )

    containerEventData.setTagCommand(
      this.dataValidator.validate(config, 'container_tagCommand', ContainerCommand, containerEventData.getTagCommand())
    )

    containerEventData.setInspectEnabled(
      this.dataValidator.validate(config, 'container_inspectEnabled', Boolean, containerEventData.getInspectEnabled())
    )
    containerEventData.setInspectCommand(
      this.dataValidator.validate(config, 'container_inspectCommand', ContainerCommand, containerEventData.getInspectCommand())
    )

    containerEventData.setPushCommand(
      this.dataValidator.validate(config, 'container_pushCommand', ContainerCommand, containerEventData.getPushCommand())
    )
    containerEventData.setPushDestinations(
      this.dataValidator.validate(config, 'container_pushDestinations', List, containerEventData.getPushDestinations())
    )
    containerEventData.setPushCertDir(
      this.dataValidator.validate(config, 'container_pushCertDir', String, containerEventData.getPushCertDir())
    )
    containerEventData.setPushDigestfile(
      this.dataValidator.validate(config, 'container_pushDigestfile', String, containerEventData.getPushDigestfile())
    )
    containerEventData.setPushEncryptLayer(
      this.dataValidator.validate(config, 'container_pushEncryptLayer', String, containerEventData.getPushEncryptLayer())
    )
    containerEventData.setPushEncryptionKey(
      this.dataValidator.validate(config, 'container_pushEncryptionKey', String, containerEventData.getPushEncryptionKey())
    )
    containerEventData.setPushFormat(
      this.dataValidator.validate(config, 'container_pushFormat', String, containerEventData.getPushFormat())
    )
    containerEventData.setPushQuiet(
      this.dataValidator.validate(config, 'container_pushQuiet', Boolean, containerEventData.getPushQuiet())
    )
    containerEventData.setPushSignBy(
      this.dataValidator.validate(config, 'container_pushSignBy', String, containerEventData.getPushSignBy())
    )
    containerEventData.setPushSignBySigstore(
      this.dataValidator.validate(config, 'container_pushSignBySigstore', String, containerEventData.getPushSignBySigstore())
    )
    containerEventData.setPushSignBySigstorePrivateKey(
      this.dataValidator.validate(config, 'container_pushSignBySigstorePrivateKey', String, containerEventData.getPushSignBySigstorePrivateKey())
    )
    containerEventData.setPushSignPassphraseFile(
      this.dataValidator.validate(config, 'container_pushSignPassphraseFile', String, containerEventData.getPushSignPassphraseFile())
    )

    if (eventData.getRunnerEventData().getLinterData() == null || eventData.getRunnerEventData().getLinterData().size() == 0) {
      LinterData linterData = this.linterDataGenericFactory.instantiate('hadolint')

      linterData.setCommandArguments("< ${containerEventData.getMakefilePath()}")

      eventData.getRunnerEventData().setLinterData([linterData])
    }

    eventData.getRunnerEventData().setMainEventData(containerEventData)

    return eventData
  }
}
