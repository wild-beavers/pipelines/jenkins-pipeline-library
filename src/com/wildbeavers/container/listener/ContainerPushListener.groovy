package com.wildbeavers.container.listener

import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.data.OptionString
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.ParallelProxy

class ContainerPushListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  private ExecuteProxy executeProxy
  private ScmInfo scmInfo
  private ParallelProxy parallelProxy

  ContainerPushListener(ExecuteProxy executeProxy, ScmInfo scmInfo, ParallelProxy parallelProxy) {
    this.executeProxy = executeProxy
    this.scmInfo = scmInfo
    this.parallelProxy = parallelProxy
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.DEPLOY]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData) {
    return this.scmInfo.isPublishableAsAnything() && eventData.getMainEventData().getPushDestinations()?.size() > 0
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as ContainerEventData)

    return eventData
  }

  void doRun(ContainerEventData eventData) {
    OptionString optionString = new OptionString()

    optionString.addOption('--cert-dir', eventData.getPushCertDir(), false)
    optionString.addOption('--authfile', eventData.getAuthfile(), false)
    optionString.addOption('--digestfile', eventData.getPushDigestfile(), false)
    optionString.addOption('--encryption-key', eventData.getPushEncryptionKey(), false)
    optionString.addOption('--encryption-layer', eventData.getPushEncryptLayer(), false)
    optionString.addOption('--format', eventData.getPushFormat(), false)
    optionString.addOption('--quiet', eventData.getPushQuiet())
    optionString.addOption('--sign-by', eventData.getPushSignBy() as String, false)
    optionString.addOption('--sign-by-sigstore', eventData.getPushSignBySigstore() as String, false)
    optionString.addOption('--sign-by-sigstore-private-key', eventData.getPushSignBySigstorePrivateKey() as String, false)
    optionString.addOption('--sign-passphrase-file', eventData.getPushSignPassphraseFile() as String, false)

    Map<String, Closure> branches = [:]
    for (destination in eventData.getPushDestinations()) {
      String destinationCopy = destination

      branches[destinationCopy] = {
        this.executeProxy.executeWithTail("${eventData.getPushCommand()} ${optionString.toString()} ${eventData.getAlias()} ${destinationCopy}:${eventData.getAlias()}")
        for (tag in this.scmInfo.getAllRevisionSemverAndFloatingTags()) {
          this.executeProxy.executeWithTail("${eventData.getPushCommand()} ${optionString.toString()} ${eventData.getAlias()} ${destinationCopy}:${tag}")
        }
      }
    }

    this.parallelProxy.parallel(branches)
  }
}
