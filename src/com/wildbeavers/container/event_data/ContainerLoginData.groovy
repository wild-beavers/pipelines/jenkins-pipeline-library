package com.wildbeavers.container.event_data

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.container.data.ContainerRegistryOption
import com.wildbeavers.container.data.HasContainerRegistryOptionsData
import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.observer.EventDataInterface

class ContainerLoginData implements EventDataInterface, HasContainerRegistryOptionsData {
  final static DEFAULT_ENABLED = true
  final static DEFAULT_TOOL = 'docker'

  private Boolean enabled = DEFAULT_ENABLED
  private ContainerTool toolCircumventCPS
  private CharSequence authfile = ''

  ContainerLoginData(CharSequence tool = DEFAULT_TOOL) {
    this.setTool(tool)
  }

  Boolean getEnabled() {
    return enabled
  }

  Boolean isEnabled() {
    return enabled
  }

  void setEnabled(Boolean enabled) {
    this.enabled = enabled
  }

  @NonCPS
  CharSequence getTool() {
    return this.toolCircumventCPS.toString()
  }

  @NonCPS
  void setTool(ContainerTool tool) {
    this.toolCircumventCPS = tool
  }

  @NonCPS
  void setTool(CharSequence tool) {
    this.toolCircumventCPS = new ContainerTool(tool)
  }

  CharSequence getAuthfile() {
    return this.authfile
  }

  void setAuthfile(CharSequence authfile) {
    this.authfile = authfile
  }

  /////
  // HasContainerRegistryOptionsData
  /////

  private List<ContainerRegistryOption> containerRegistryOptions = []

  @Override
  List<ContainerRegistryOption> getContainerRegistryOptions() {
    return this.containerRegistryOptions
  }

  @Override
  void setContainerRegistryOptions(List<ContainerRegistryOption> registryOptions) {
    this.containerRegistryOptions = registryOptions
  }

  @Override
  void addContainerRegistryOption(ContainerRegistryOption registryOption) {
    this.containerRegistryOptions.add(registryOption)
  }
}
