package com.wildbeavers.container.event_data


import com.wildbeavers.container.data_validation.type.ContainerCommand
import com.wildbeavers.observer.EventDataInterface

// Uses interfaces instead of traits, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
abstract class ContainerEventData implements EventDataInterface {
  static final DEFAULT_LINTER_CONTAINER_FQDN = 'ghcr.io/hadolint'
  static final DEFAULT_LINTER_FALLBACK_COMMAND = 'hadolint'
  static final DEFAULT_LINTER_CONTAINER_IMAGE_NAME = 'hadolint'
  static final DEFAULT_LINTER_CONTAINER_IMAGE_TAG = 'latest'
  static final DEFAULT_BUILD_COMPRESS = false
  static final DEFAULT_BUILD_QUIET = false
  static final DEFAULT_PUSH_QUIET = false
  static final DEFAULT_BUILD_PLATFORMS = ['linux/amd64', 'linux/arm64']
  static final DEFAULT_BUILD_TARGET = '.'
  static final DEFAULT_PUSH_DISPLAY_INSPECT = true

  protected String tool
  protected String authfile = ContainerDockerEventData.DEFAULT_AUTHFILE
  protected String makefilePath = ''
  protected String manifestName = ''
  protected String manifestFormat = ''

  protected List<String> buildPlatforms = DEFAULT_BUILD_PLATFORMS
  protected Boolean buildCompress = DEFAULT_BUILD_COMPRESS
  protected String buildArgFile = ''
  protected String buildCacheFrom = ''
  protected ContainerCommand buildCommand
  protected String buildNetwork = ''
  protected String buildPull = ''
  protected String buildTag = ''
  protected Boolean buildQuiet = DEFAULT_BUILD_QUIET
  protected String buildTarget = DEFAULT_BUILD_TARGET
  protected Map<String, String> buildArgs = [:]

  protected ContainerCommand tagCommand

  protected Boolean inspectEnabled = DEFAULT_PUSH_DISPLAY_INSPECT
  protected ContainerCommand inspectCommand

  protected ContainerCommand pushCommand
  protected List<String> pushDestinations = []
  protected String pushCertDir = ''
  protected String pushDigestfile = ''
  protected String pushEncryptLayer = ''
  protected String pushEncryptionKey = ''
  protected String pushFormat = ''
  protected Boolean pushQuiet = DEFAULT_PUSH_QUIET
  protected String pushSignBy = ''
  protected String pushSignBySigstore = ''
  protected String pushSignBySigstorePrivateKey = ''
  protected String pushSignPassphraseFile = ''

  String getTool() {
    return this.tool
  }

  String getAuthfile() {
    return this.authfile
  }

  void setAuthfile(String authfile) {
    this.authfile = authfile
  }

  String getMakefilePath() {
    return this.makefilePath
  }

  void setMakefilePath(String makefilePath) {
    this.makefilePath = makefilePath
  }

  String getManifestName() {
    return manifestName
  }

  void setManifestName(String manifestName) {
    this.manifestName = manifestName
  }

  String getManifestFormat() {
    return this.manifestFormat
  }

  void setManifestFormat(String manifestFormat) {
    this.manifestFormat = manifestFormat
  }

  List<String> getBuildPlatforms() {
    return this.buildPlatforms
  }

  void setBuildPlatforms(List<String> buildPlatforms) {
    this.buildPlatforms = buildPlatforms
  }

  Boolean getBuildCompress() {
    return this.buildCompress
  }

  void setBuildCompress(Boolean buildCompress) {
    this.buildCompress = buildCompress
  }

  String getBuildArgFile() {
    return this.buildArgFile
  }

  void setBuildArgFile(String buildArgFile) {
    this.buildArgFile = buildArgFile
  }

  String getBuildCacheFrom() {
    return this.buildCacheFrom
  }

  void setBuildCacheFrom(String buildCacheFrom) {
    this.buildCacheFrom = buildCacheFrom
  }

  CharSequence getBuildCommand() {
    return this.buildCommand.toString()
  }

  void setBuildCommand(ContainerCommand buildCommand) {
    this.buildCommand = buildCommand
  }

  String getBuildNetwork() {
    return this.buildNetwork
  }

  void setBuildNetwork(String buildNetwork) {
    this.buildNetwork = buildNetwork
  }

  String getBuildPull() {
    return this.buildPull
  }

  void setBuildPull(String buildPull) {
    this.buildPull = buildPull
  }

  String getBuildTag() {
    return this.buildTag
  }

  void setBuildTag(String buildTag) {
    this.buildTag = buildTag
  }

  Boolean getBuildQuiet() {
    return this.buildQuiet
  }

  void setBuildQuiet(Boolean buildQuiet) {
    this.buildQuiet = buildQuiet
  }

  String getBuildTarget() {
    return buildTarget
  }

  void setBuildTarget(String buildTarget) {
    this.buildTarget = buildTarget
  }

  Map<String, String> getBuildArgs() {
    return this.buildArgs
  }

  void setBuildArgs(Map<String, String> buildArgs) {
    this.buildArgs = buildArgs
  }

  String getTagCommand() {
    return this.tagCommand.toString()
  }

  void setTagCommand(ContainerCommand tagCommand) {
    this.tagCommand = tagCommand
  }

  Boolean getInspectEnabled() {
    return inspectEnabled
  }

  void setInspectEnabled(Boolean pushDisplayInspect) {
    this.inspectEnabled = pushDisplayInspect
  }

  CharSequence getInspectCommand() {
    return this.inspectCommand.toString()
  }

  void setInspectCommand(ContainerCommand inspectCommand) {
    this.inspectCommand = inspectCommand
  }

  CharSequence getPushCommand() {
    return this.pushCommand.toString()
  }

  void setPushCommand(ContainerCommand pushCommand) {
    this.pushCommand = pushCommand
  }

  List<String> getPushDestinations() {
    return this.pushDestinations
  }

  void setPushDestinations(List<String> pushDestinations) {
    this.pushDestinations = pushDestinations
  }

  String getPushCertDir() {
    return this.pushCertDir
  }

  void setPushCertDir(String pushCertDir) {
    this.pushCertDir = pushCertDir
  }

  String getPushDigestfile() {
    return this.pushDigestfile
  }

  void setPushDigestfile(String pushDigestfile) {
    this.pushDigestfile = pushDigestfile
  }

  String getPushEncryptLayer() {
    return this.pushEncryptLayer
  }

  void setPushEncryptLayer(String pushEncryptLayer) {
    this.pushEncryptLayer = pushEncryptLayer
  }

  String getPushEncryptionKey() {
    return this.pushEncryptionKey
  }

  void setPushEncryptionKey(String pushEncryptionKey) {
    this.pushEncryptionKey = pushEncryptionKey
  }

  String getPushFormat() {
    return this.pushFormat
  }

  void setPushFormat(String pushFormat) {
    this.pushFormat = pushFormat
  }

  Boolean getPushQuiet() {
    return this.pushQuiet
  }

  void setPushQuiet(Boolean pushQuiet) {
    this.pushQuiet = pushQuiet
  }

  String getPushSignBy() {
    return this.pushSignBy
  }

  void setPushSignBy(String pushSignBy) {
    this.pushSignBy = pushSignBy
  }

  String getPushSignBySigstore() {
    return this.pushSignBySigstore
  }

  void setPushSignBySigstore(String pushSignBySigstore) {
    this.pushSignBySigstore = pushSignBySigstore
  }

  String getPushSignBySigstorePrivateKey() {
    return this.pushSignBySigstorePrivateKey
  }

  void setPushSignBySigstorePrivateKey(String pushSignBySigstorePrivateKey) {
    this.pushSignBySigstorePrivateKey = pushSignBySigstorePrivateKey
  }

  String getPushSignPassphraseFile() {
    return this.pushSignPassphraseFile
  }

  void setPushSignPassphraseFile(String pushSignPassphraseFile) {
    this.pushSignPassphraseFile = pushSignPassphraseFile
  }

  String getAlias() {
    this.manifestName ? this.manifestName : this.buildTag
  }
}
