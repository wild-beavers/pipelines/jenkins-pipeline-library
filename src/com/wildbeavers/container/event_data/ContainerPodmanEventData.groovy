package com.wildbeavers.container.event_data

import com.wildbeavers.container.data_validation.type.ContainerCommand

/**
 * Overrides ContainerEventData with podman defaults
 */
class ContainerPodmanEventData extends ContainerEventData {
  static final DEFAULT_TOOL = 'podman'
  static final DEFAULT_MAKEFILE_PATH = 'Containerfile'
  static final DEFAULT_MANIFEST_FORMAT = 'oci'
  static final DEFAULT_AUTHFILE = null
  static final DEFAULT_BUILD_COMMAND = 'podman build'
  static final DEFAULT_TAG_COMMAND = 'podman tag'
  static final DEFAULT_INSPECT_COMMAND = 'podman inspect'
  static final DEFAULT_PUSH_COMMAND = 'podman push'

  ContainerPodmanEventData() {
    super.tool = DEFAULT_TOOL
    super.authfile = DEFAULT_AUTHFILE
    super.makefilePath = DEFAULT_MAKEFILE_PATH
    super.manifestFormat = DEFAULT_MANIFEST_FORMAT
    super.buildCommand = new ContainerCommand(DEFAULT_BUILD_COMMAND)
    super.tagCommand = new ContainerCommand(DEFAULT_TAG_COMMAND)
    super.inspectCommand = new ContainerCommand(DEFAULT_INSPECT_COMMAND)
    super.pushCommand = new ContainerCommand(DEFAULT_PUSH_COMMAND)
  }

  @Override
  Boolean getBuildCompress() {
    return false
  }
}
