package com.wildbeavers.container.event_data

import com.wildbeavers.container.data_validation.type.ContainerCommand

/**
 * Overrides ContainerEventData with docker defaults
 */
class ContainerDockerEventData extends ContainerEventData {
  static final DEFAULT_TOOL = 'docker'
  static final DEFAULT_MAKEFILE_PATH = 'Dockerfile'
  static final DEFAULT_BUILD_COMMAND = 'docker build'
  static final DEFAULT_TAG_COMMAND = 'docker tag'
  static final DEFAULT_PUSH_COMMAND = 'docker push'
  static final DEFAULT_INSPECT_COMMAND = 'docker inspect'
  final static DEFAULT_AUTHFILE = '${HOME}/.docker/config.json'

  ContainerDockerEventData() {
    super.tool = DEFAULT_TOOL
    super.makefilePath = DEFAULT_MAKEFILE_PATH
    super.buildCommand = new ContainerCommand(DEFAULT_BUILD_COMMAND)
    super.tagCommand = new ContainerCommand(DEFAULT_TAG_COMMAND)
    super.pushCommand = new ContainerCommand(DEFAULT_PUSH_COMMAND)
    super.inspectCommand = new ContainerCommand(DEFAULT_INSPECT_COMMAND)
  }

  @Override
  void setManifestName(String ignored) {}

  @Override
  String getManifestName() {
    return ''
  }

  @Override
  void setManifestFormat(String ignored) {}

  @Override
  String getManifestFormat() {
    return ''
  }

  @Override
  void setAuthfile(String ignored) {}

  @Override
  String getAuthfile() {
    return ''
  }

  @Override
  void setBuildArgFile(String ignored) {}

  @Override
  String getBuildArgFile() {
    return ''
  }

  @Override
  void setPushCertDir(String ignored) {}

  @Override
  String getPushCertDir() {
    return ''
  }

  @Override
  void setPushDigestfile(String ignored) {}

  @Override
  String getPushDigestfile() {
    return ''
  }

  void setPushEncryptLayer(String ignored) {}

  @Override
  String getPushEncryptLayer() {
    return ''
  }

  void setPushEncryptionKey(String ignored) {}

  @Override
  String getPushEncryptionKey() {
    return ''
  }

  void setPushFormat(String ignored) {}

  @Override
  String getPushFormat() {
    return ''
  }

  @Override
  void setPushSignBy(String ignored) {}

  @Override
  String getPushSignBy() {
    return ''
  }

  @Override
  void setPushSignBySigstore(String ignored) {}

  @Override
  String getPushSignBySigstore() {
    return ''
  }

  @Override
  void setPushSignBySigstorePrivateKey(String ignored) {}

  @Override
  String getPushSignBySigstorePrivateKey() {
    return ''
  }

  @Override
  void setPushSignPassphraseFile(String ignored) {}

  @Override
  String getPushSignPassphraseFile() {
    return ''
  }
}
