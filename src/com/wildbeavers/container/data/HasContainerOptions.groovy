package com.wildbeavers.container.data
// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasContainerOptions {
  ContainerOptions getContainerOptions()

  void setContainerOptions(ContainerOptions dockerOptions)
}
