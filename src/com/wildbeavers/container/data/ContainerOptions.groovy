package com.wildbeavers.container.data

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.data.OptionString

import java.nio.file.Paths

class ContainerOptions {
  final static DEFAULT_TOOL = 'docker'
  final static DEFAULT_FQDN = 'docker.io'
  final static DEFAULT_IMAGE_NAME = 'docker'
  final static DEFAULT_IMAGE_TAG = 'latest'

  private ContainerTool toolCircumventCPS
  private OptionString toolOptions = new OptionString()
  private CharSequence fqdn = ''
  private CharSequence imageName = ''
  private CharSequence imageTag = ''
  private CharSequence entrypoint = ''
  private CharSequence name = ''
  private CharSequence network = ''
  private CharSequence userns = ''
  private CharSequence pull = ''
  private CharSequence logDriver = ''
  private Map<CharSequence, CharSequence> volumes = [:]
  private Map<CharSequence, CharSequence> environmentVariables = [:]
  private Boolean privileged = false
  private Boolean removeAfterRun = true
  private Boolean runHasDaemon = false
  private Boolean readOnly = false
  private Boolean transientStore = false

  private Boolean basepathAsWorkdir = true
  private CharSequence fallbackCommand = ''
  private Boolean quiet = false
  private Boolean assumeRuntimeInstalled = false

  ContainerOptions(CharSequence tool = DEFAULT_TOOL, CharSequence fqdn = DEFAULT_FQDN, CharSequence imageName = DEFAULT_IMAGE_NAME, CharSequence imageTag = DEFAULT_IMAGE_TAG) {
    this.setTool(tool)
    this.fqdn = fqdn
    this.imageName = imageName
    this.imageTag = imageTag
  }

  @NonCPS
  CharSequence getTool() {
    return this.toolCircumventCPS.toString()
  }

  @NonCPS
  void setTool(ContainerTool tool) {
    this.toolCircumventCPS = tool
  }

  @NonCPS
  void setTool(CharSequence tool) {
    this.toolCircumventCPS = new ContainerTool(tool)
  }

  OptionString getToolOptions() {
    return toolOptions
  }

  CharSequence getFqdn() {
    return this.fqdn
  }

  void setFqdn(CharSequence fqdn) {
    this.fqdn = fqdn
  }

  CharSequence getImageName() {
    return imageName
  }

  void setImageName(CharSequence imageName) {
    this.imageName = imageName
  }

  CharSequence getImageTag() {
    return this.imageTag
  }

  void setImageTag(CharSequence imageTag) {
    this.imageTag = imageTag
  }

  CharSequence getImage() {
    return Paths.get(this.fqdn, this.imageName + (this.imageTag ? ':' + this.imageTag : '')).toString()
  }

  void setImage(CharSequence image) {
    // This is deprecated. Refactor your code to use setImage(CharSequence fqdn, CharSequence imageName, CharSequence imageTag).'
  }

  void setImage(CharSequence fqdn, CharSequence imageName, CharSequence imageTag) {
    this.fqdn = fqdn
    this.imageName = imageName
    this.imageTag = imageTag
  }

  Boolean hasImage() {
    return (this.getImage().trim()) as Boolean
  }

  Map<CharSequence, CharSequence> getVolumes() {
    return volumes
  }

  void setVolumes(Map<CharSequence, CharSequence> volumes) {
    this.volumes = volumes
  }

  void addVolumes(Map<CharSequence, CharSequence> additionalMounts) {
    this.volumes += additionalMounts
  }

  void removeVolume(CharSequence localPath) {
    this.volumes.remove(localPath)
  }

  Map<CharSequence, CharSequence> getEnvironmentVariables() {
    return environmentVariables
  }

  void setEnvironmentVariables(Map<CharSequence, CharSequence> environmentVariables) {
    this.environmentVariables = environmentVariables
  }

  void addEnvironmentVariables(Map<CharSequence, CharSequence> environmentVariables) {
    this.environmentVariables += environmentVariables
  }

  void removeEnvironmentVariable(CharSequence environmentVariableName) {
    this.environmentVariables.remove(environmentVariableName)
  }

  CharSequence getNetwork() {
    return network
  }

  void setNetwork(CharSequence network) {
    this.network = network
  }

  Boolean getRunHasDaemon() {
    return runHasDaemon
  }

  void setRunHasDaemon(Boolean runHasDaemon) {
    this.runHasDaemon = runHasDaemon
  }

  CharSequence getName() {
    return name
  }

  void setName(CharSequence name) {
    this.name = name
  }

  CharSequence getEntrypoint() {
    return entrypoint
  }

  void setEntrypoint(CharSequence entrypoint) {
    this.entrypoint = entrypoint
  }

  CharSequence getUserns() {
    return userns
  }

  void setUserns(CharSequence userns) {
    this.userns = userns
  }

  Boolean getRemoveAfterRun() {
    return removeAfterRun
  }

  void setRemoveAfterRun(Boolean removeAfterRun) {
    this.removeAfterRun = removeAfterRun
  }

  Boolean getPrivileged() {
    return privileged
  }

  void setPrivileged(Boolean privileged) {
    this.privileged = privileged
  }

  Boolean getBasepathAsWorkdir() {
    return this.basepathAsWorkdir
  }

  void setBasepathAsWorkdir(Boolean basepathAsWorkdir) {
    this.basepathAsWorkdir = basepathAsWorkdir
  }

  CharSequence getFallbackCommand() {
    return this.fallbackCommand
  }

  void setFallbackCommand(CharSequence fallbackCommand) {
    this.fallbackCommand = fallbackCommand
  }

  Boolean getQuiet() {
    return this.quiet
  }

  void setQuiet(Boolean quiet) {
    this.quiet = quiet
  }

  CharSequence getPull() {
    return pull
  }

  void setPull(CharSequence pull) {
    this.pull = pull
  }

  CharSequence getLogDriver() {
    return logDriver
  }

  void setLogDriver(CharSequence logDriver) {
    this.logDriver = logDriver
  }

  Boolean getReadOnly() {
    return readOnly
  }

  void setReadOnly(Boolean readOnly) {
    this.readOnly = readOnly
  }

  Boolean getTransientStore() {
    return transientStore
  }

  void setTransientStore(Boolean transientStore) {
    this.transientStore = transientStore
  }

  Boolean getAssumeRuntimeInstalled() {
    return assumeRuntimeInstalled
  }

  void setAssumeRuntimeInstalled(Boolean assumeRuntimeInstalled) {
    this.assumeRuntimeInstalled = assumeRuntimeInstalled
  }
}
