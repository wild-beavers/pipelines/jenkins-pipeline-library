package com.wildbeavers.container.data

class ContainerRegistryOption implements HasContainerOptions {
  final static DEFAULT_FQDN = 'docker.io'

  private CharSequence credentialId = ''
  private ContainerOptions credentialProcessContainerOptions
  private CharSequence credentialProcessContainerRunCommandArguments = 'echo "this is not a password"'
  private CharSequence credentialProcessUsername = ''
  private CharSequence fqdn = DEFAULT_FQDN

  ContainerRegistryOption(ContainerOptions credentialProcessContainerOptions) {
    this.credentialProcessContainerOptions = credentialProcessContainerOptions
  }

  CharSequence getFqdn() {
    return fqdn
  }

  void setFqdn(CharSequence fqdn) {
    this.fqdn = fqdn
  }

  CharSequence getCredentialProcessUsername() {
    return credentialProcessUsername
  }

  void setCredentialProcessUsername(CharSequence credentialProcessUsername) {
    this.credentialProcessUsername = credentialProcessUsername
  }

  Boolean hasCredentialId() {
    return (this.credentialId?.trim()) as Boolean
  }

  CharSequence getCredentialId() {
    return credentialId
  }

  void setCredentialId(CharSequence credentialId) {
    this.credentialId = credentialId
  }

  ContainerOptions getCredentialProcessContainerOptions() {
    return credentialProcessContainerOptions
  }

  void setCredentialProcessContainerOptions(ContainerOptions credentialProcessContainerOptions) {
    this.credentialProcessContainerOptions = credentialProcessContainerOptions
  }

  @Override
  ContainerOptions getContainerOptions() {
    return credentialProcessContainerOptions
  }

  @Override
  void setContainerOptions(ContainerOptions dockerOptions) {
    this.credentialProcessContainerOptions = dockerOptions
  }

  CharSequence getCredentialProcessContainerRunCommandArguments() {
    return credentialProcessContainerRunCommandArguments
  }

  void setCredentialProcessContainerRunCommandArguments(CharSequence credentialProcessContainerRunCommandArguments) {
    this.credentialProcessContainerRunCommandArguments = credentialProcessContainerRunCommandArguments
  }
}
