package com.wildbeavers.container.data
// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasContainerRegistryOptionsData {
  List<ContainerRegistryOption> getContainerRegistryOptions()

  void setContainerRegistryOptions(List<ContainerRegistryOption> registryOptions)

  void addContainerRegistryOption(ContainerRegistryOption registryOption)
}
