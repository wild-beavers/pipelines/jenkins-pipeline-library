package com.wildbeavers.terraform.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.FileProxy
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData

class TerraformDestroyListener extends EventListener {
  private FileProxy fileProxy
  private Terraform terraform
  private Debugger debugger
  private Script context

  TerraformDestroyListener(FileProxy fileProxy, Terraform terraform, Debugger debugger, Script context) {
    this.fileProxy = fileProxy
    this.terraform = terraform
    this.debugger = debugger
    this.context = context
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.CLEANUP]
  }

  @Override
  Boolean shouldRun(EventDataInterface eventData = null) {
    return !eventData.getMainEventData().isDeployment()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)
    return eventData
  }

  private void doRun(TerraformEventData eventData) {
    for (commandTarget in eventData.getCommandTargets()) {
      if (!this.fileProxy.exists(commandTarget + '/' + TerraformApplyListener.DEFAULT_STATE_FILE)) {
        this.debugger.printDebug("Skipping terraform destroy for ${commandTarget}, because no state file was found.")
        continue
      }

      def arguments = [
        '-chdir'       : commandTarget,
        '-auto-approve': true,
        '-refresh': 'false',
      ] + eventData.getDestroyOptions()

      try {
        this.terraform.run('destroy', arguments, eventData)
      } catch (Exception ignored) {
        this.debugger.print('First attempt to “terraform destroy” has failed. Trying once again, with a refresh.')

        if (!eventData.isDeployment()) {
          this.context.archiveArtifacts(
            allowEmptyArchive: true,
            artifacts: commandTarget + '/' + TerraformApplyListener.DEFAULT_STATE_FILE
          )
        }

        this.terraform.run('destroy', [
          '-chdir'       : commandTarget,
          '-auto-approve': true,
        ] + eventData.getDestroyOptions(), eventData)
      }
    }
  }
}
