package com.wildbeavers.terraform.listener

import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

import static com.wildbeavers.command.AbstractCommand.COMMAND_TARGET

class TerraformFmtListener extends EventListener {
  private Terraform terraform

  TerraformFmtListener(Terraform terraform) {
    this.terraform = terraform
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.LINT]
  }

  /**
   * @param TerraformEventData eventData
   * @return TerraformEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)

    return eventData
  }

  private void doRun(TerraformEventData eventData) {
    this.terraform.run('fmt', [
      '-check'        : true,
      '-recursive'    : true,
      (COMMAND_TARGET): '.',
    ] + eventData.getFmtOptions(), eventData)
  }
}
