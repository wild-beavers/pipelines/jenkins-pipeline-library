package com.wildbeavers.terraform.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.terraform.event_data.TerraformEventData

class TerraformArtifactCleanerListener extends EventListener<RunnerEventData> {
  private ExecuteProxy executeProxy

  TerraformArtifactCleanerListener(ExecuteProxy executeProxy) {
    this.executeProxy = executeProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.POST_CLEANUP]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)

    return eventData
  }

  private void doRun(TerraformEventData eventData) {
    this.executeProxy.execute("rm -f ${TerraformApplyListener.PLAN_OUT_FILE} || true")

    if (!eventData.isDeployment()) {
      this.executeProxy.execute('rm -f ' + eventData.getCommandTargets().join("/${TerraformApplyListener.DEFAULT_STATE_FILE} ") + "/${TerraformApplyListener.DEFAULT_STATE_FILE} || true")
    }
  }
}
