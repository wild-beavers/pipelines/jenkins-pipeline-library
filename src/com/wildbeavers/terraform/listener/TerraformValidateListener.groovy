package com.wildbeavers.terraform.listener

import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

class TerraformValidateListener extends EventListener {
  private Terraform terraform

  TerraformValidateListener(Terraform terraform) {
    this.terraform = terraform
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_TEST_UNIT]
  }

  @Override
  Boolean shouldRun(EventDataInterface eventData = null) {
    return eventData.getMainEventData().isValidateEnabled()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)
    return eventData
  }


  private void doRun(TerraformEventData eventData) {
    for (commandTarget in eventData.getCommandTargets()) {
      this.terraform.run(
        'validate', ['-chdir': commandTarget] + eventData.getValidateOptions(), eventData
      )
    }
  }
}
