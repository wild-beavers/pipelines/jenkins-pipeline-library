package com.wildbeavers.terraform.listener

import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

class TerraformInitListener extends EventListener {
  private Terraform terraform

  TerraformInitListener(Terraform terraform) {
    this.terraform = terraform
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.COMPILE]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)

    return eventData
  }

  private void doRun(TerraformEventData eventData) {
    for (commandTarget in eventData.getCommandTargets()) {
      this.terraform.run(
        'init', ['-chdir': commandTarget] + eventData.getInitOptions(), eventData
      )
    }
  }
}
