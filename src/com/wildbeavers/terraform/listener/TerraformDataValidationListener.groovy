package com.wildbeavers.terraform.listener

import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.terraform.event_data.TerraformEventData

class TerraformDataValidationListener extends EventListener<ControllerEventData> {
  public static final DEFAULT_MANDATORY_FILES = [
    '.gitignore',
    '.pre-commit-config.yaml',
  ]
  public static final DEFAULT_DEPLOY_MANDATORY_FILES = DEFAULT_MANDATORY_FILES + [
    TerraformEventData.DEPLOYMENT_FILENAME,
  ]
  public static final DEFAULT_MODULE_MANDATORY_FILES = DEFAULT_MANDATORY_FILES + [
    'CHANGELOG.md',
    'main.tf',
    'examples/default/deploy.tf',
    'examples/disable/deploy.tf',
  ]

  private GenericFactory<TerraformEventData> terraformEventDataGenericFactory
  private DataValidator dataValidator

  TerraformDataValidationListener(GenericFactory<TerraformEventData> terraformEventDataGenericFactory, DataValidator dataValidator) {
    this.terraformEventDataGenericFactory = terraformEventDataGenericFactory
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData) {
    Map config = eventData.getRawConfig()
    WildMap commonOptions = this.dataValidator.validate(config, 'terraform_commonOptions', WildMap, [:] as WildMap)

    TerraformEventData terraformEventData = this.terraformEventDataGenericFactory.instantiate()

    terraformEventData.setIsDeployment(
      this.dataValidator.validate(config, 'terraform_isDeployment', Boolean, terraformEventData.isDeployment())
    )
    terraformEventData.setWorkspace(
      this.dataValidator.validate(config, 'terraform_workspace', CharSequence, terraformEventData.getWorkspace())
    )
    terraformEventData.setExamplesDirectory(
      this.dataValidator.validate(config, 'terraform_examplesDirectory', CharSequence, terraformEventData.getExamplesDirectory())
    )
    terraformEventData.setFmtEnabled(
      this.dataValidator.validate(config, 'terraform_enableFmt', Boolean, terraformEventData.isFmtEnabled())
    )
    terraformEventData.setValidateEnabled(
      this.dataValidator.validate(config, 'terraform_enableValidate', Boolean, terraformEventData.isValidateEnabled())
    )
    terraformEventData.setIdempotencyCheckEnabled(
      this.dataValidator.validate(config, 'terraform_enableIdempotencyCheck', Boolean, terraformEventData.isIdempotencyCheckEnabled())
    )
    terraformEventData.setFmtOptions(
      this.dataValidator.validate(config, 'terraform_fmtOptions', WildMap, terraformEventData.getFmtOptions() + commonOptions)
    )
    terraformEventData.setValidateOptions(
      this.dataValidator.validate(config, 'terraform_validateOptions', WildMap, terraformEventData.getValidateOptions() + commonOptions)
    )

    terraformEventData.getContainerOptions().setImageName(
      this.dataValidator.validate(config, 'terraform_containerImageName', String, terraformEventData.getContainerOptions().getImageName())
    )
    terraformEventData.getContainerOptions().setImageTag(
      this.dataValidator.validate(config, 'terraform_containerImageTag', String, terraformEventData.getContainerOptions().getImageTag())
    )
    terraformEventData.getContainerOptions().setFqdn(
      this.dataValidator.validate(config, 'terraform_containerFqdn', String, terraformEventData.getContainerOptions().getFqdn())
    )
    terraformEventData.getContainerOptions().setVolumes(
      this.dataValidator.validate(config, 'terraform_containerVolumes', Map, terraformEventData.getContainerOptions().getVolumes())
    )
    terraformEventData.getContainerOptions().setEnvironmentVariables(
      this.dataValidator.validate(config, 'terraform_containerEnvironmentVariables', Map, terraformEventData.getContainerOptions().getEnvironmentVariables())
    )
    terraformEventData.getContainerOptions().setNetwork(
      this.dataValidator.validate(config, 'terraform_containerNetwork', String, terraformEventData.getContainerOptions().getNetwork())
    )

    if (!terraformEventData.isDeployment()) {
      terraformEventData.setInitOptions(
        this.dataValidator.validate(config, 'terraform_testInitOptions', WildMap, terraformEventData.getInitOptions() + commonOptions)
      )
      terraformEventData.setPlanOptions(
        this.dataValidator.validate(config, 'terraform_testPlanOptions', WildMap, terraformEventData.getPlanOptions() + commonOptions)
      )
      terraformEventData.setApplyOptions(
        this.dataValidator.validate(config, 'terraform_testApplyOptions', WildMap, terraformEventData.getApplyOptions() + commonOptions)
      )
      terraformEventData.setDestroyOptions(
        this.dataValidator.validate(config, 'terraform_testDestroyOptions', WildMap, terraformEventData.getDestroyOptions() + commonOptions)
      )

      if (!eventData.getRunnerEventData().isFileStandardIgnoreDefault()) {
        for (mandatoryFile in DEFAULT_MODULE_MANDATORY_FILES) {
          eventData.getRunnerEventData().getFileStandardData().addMandatoryFile(
            this.dataValidator.validate(mandatoryFile, ForwardRelativeLinuxFullPath)
          )
        }
      }
    } else {
      terraformEventData.setInitOptions(
        this.dataValidator.validate(config, 'terraform_deployInitOptions', WildMap, terraformEventData.getInitOptions() + commonOptions)
      )
      terraformEventData.setPlanOptions(
        this.dataValidator.validate(config, 'terraform_deployPlanOptions', WildMap, terraformEventData.getPlanOptions() + commonOptions)
      )
      terraformEventData.setApplyOptions(
        this.dataValidator.validate(config, 'terraform_deployApplyOptions', WildMap, terraformEventData.getApplyOptions() + commonOptions)
      )

      if (!eventData.getRunnerEventData().isFileStandardIgnoreDefault()) {
        for (mandatoryFile in DEFAULT_DEPLOY_MANDATORY_FILES) {
          eventData.getRunnerEventData().getFileStandardData().addMandatoryFile(
            this.dataValidator.validate(mandatoryFile, ForwardRelativeLinuxFullPath)
          )
        }
      }

      terraformEventData.setSensitiveResources(
        this.dataValidator.validate(config, 'terraform_sensitiveResources', List, terraformEventData.getSensitiveResources())
      )
    }

    eventData.getRunnerEventData().setMainEventData(terraformEventData)

    return eventData
  }
}
