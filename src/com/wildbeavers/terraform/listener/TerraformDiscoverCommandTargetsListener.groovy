package com.wildbeavers.terraform.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.terraform.event_data.TerraformEventData

class TerraformDiscoverCommandTargetsListener extends EventListener<RunnerEventData> {
  private ExecuteProxy executeProxy
  private Debugger debugger

  TerraformDiscoverCommandTargetsListener(ExecuteProxy executeProxy, Debugger debugger) {
    this.executeProxy = executeProxy
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.POST_PREPARE]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData) {
    return !eventData.getMainEventData().isDeployment()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    eventData.getMainEventData().setCommandTargets([])

    try {
      this.executeProxy.execute("[ -d ${eventData.getMainEventData().getExamplesDirectory()} ]")

      for (commandTarget in this.executeProxy.execute("ls ${eventData.getMainEventData().getExamplesDirectory()} | sed -e 's/.*/${eventData.getMainEventData().getExamplesDirectory()}\\/\\0/g'").getStdout().split()) {
        eventData.getMainEventData().addCommandTarget(commandTarget.toString())
      }
    } catch (CommandExecutionException error) {
      this.debugger.printDebug('Defaulting to current directory. Error: ' + error.toString())
      eventData.getMainEventData().setCommandTargets(TerraformEventData.DEFAULT_COMMAND_TARGETS)
    }

    if (eventData.getMainEventData().getCommandTargets().isEmpty()) {
      eventData.getMainEventData().addCommandTarget(TerraformEventData.DEFAULT_COMMAND_TARGETS)
    }

    return eventData
  }
}
