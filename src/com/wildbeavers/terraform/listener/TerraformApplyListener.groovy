package com.wildbeavers.terraform.listener

import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.FoolProofValidationHelper
import com.wildbeavers.proxy.PrintProxy
import com.wildbeavers.terraform.helper.TerraformPlanChangesCheckerHelper
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData

import static com.wildbeavers.command.AbstractCommand.COMMAND_TARGET

class TerraformApplyListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  static final PLAN_OUT_FILE = 'tfplan.out'
  static final DEFAULT_STATE_FILE = 'terraform.tfstate'

  private Script context
  private Terraform terraform
  private FoolProofValidationHelper foolProofValidationHelper
  private Debugger debugger
  private ScmInfo scmInfo
  private PrintProxy printProxy

  TerraformApplyListener(Script context, Terraform terraform, FoolProofValidationHelper foolProofValidationHelper, Debugger debugger, ScmInfo scmInfo, PrintProxy printProxy) {
    this.context = context
    this.terraform = terraform
    this.foolProofValidationHelper = foolProofValidationHelper
    this.debugger = debugger
    this.scmInfo = scmInfo
    this.printProxy = printProxy
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.DEPLOY]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)

    return eventData
  }

  private Boolean shouldRunApply(TerraformEventData eventData, CommandResult planResults) {
    return TerraformPlanChangesCheckerHelper.outputContainChanges(planResults) &&
      !this.projectIsDeploymentPullRequest(eventData)
  }

  private Boolean projectIsDeploymentPullRequest(TerraformEventData eventData) {
    return eventData.isDeployment() && !this.scmInfo.isPublishable()
  }

  private Boolean shouldRunPlanReplay(TerraformEventData eventData) {
    return !eventData.isDeployment() && eventData.isIdempotencyCheckEnabled()
  }

  private CommandResult plan(TerraformEventData eventData, CharSequence commandTarget) {
    return this.terraform.run('plan', [
      '-out'  : PLAN_OUT_FILE,
      '-chdir': commandTarget,
    ] + eventData.getPlanOptions(), eventData)
  }

  private void apply(TerraformEventData eventData, CharSequence commandTarget) {
    if (eventData.isDeployment()) {
      this.foolProofValidationHelper.foolProof()
    }

    this.terraform.run('apply', [
      '-chdir'        : commandTarget,
      (COMMAND_TARGET): PLAN_OUT_FILE,
    ] + eventData.getApplyOptions(), eventData)
  }

  private void doRun(TerraformEventData eventData) {
    this.debugger.printDebug(eventData.getCommandTargets())

    for (commandTarget in eventData.getCommandTargets()) {
      def planResults = this.plan(eventData, commandTarget)

      if (!this.shouldRunApply(eventData, planResults)) {
        this.debugger.printDebug(
          'Skipping terraform apply.' +
            "\nSCM isPublishable: " + this.scmInfo.isPublishable().toString() +
            "\nProject is deployment: " + eventData.isDeployment().toString() +
            "\nPlan show changes: " + TerraformPlanChangesCheckerHelper.outputContainChanges(planResults).toString()
        )

        continue
      }

      List sensitiveResourceToDestroy = TerraformPlanChangesCheckerHelper.getSensitiveResourceToDestroy(eventData.getSensitiveResources(), planResults)

      if (sensitiveResourceToDestroy.size() > 0) {
        this.printProxy.println("\u001b[33mAt least one resource planned for destruction deserves extra attention.\033[0m")
        this.printProxy.println("\u001b[33mSome important data may get destroyed or even be protected from deletion on the service provider side.\033[0m")
        this.printProxy.println("\u001b[33mPlease, double check:\033[0m")
      }
      sensitiveResourceToDestroy.each({
        this.printProxy.println("\u001b[33m- ${it}\033[0m")
      })

      try {
        this.apply(eventData, commandTarget)
      } catch (errorApply) {
        if (!eventData.isDeployment()) {
          this.context.archiveArtifacts(
            allowEmptyArchive: true,
            artifacts: commandTarget + '/' + DEFAULT_STATE_FILE
          )
        }
        throw (errorApply)
      }

      if (!this.shouldRunPlanReplay(eventData)) {
        this.debugger.printDebug('Skipping terraform idempotency check.')

        continue
      }

      if (TerraformPlanChangesCheckerHelper.outputContainChanges(
        this.plan(eventData, commandTarget)
      )) {
        this.context.error(
          'Idempotency ERROR: replaying the “terraform plan” contains new changes.' +
            'It is important to make sure terraform consecutive runs make no changes.'
        )
      }
    }
  }
}
