package com.wildbeavers.terraform.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData

import static com.wildbeavers.command.AbstractCommand.COMMAND_TARGET

class TerraformWorkspaceSelectListener extends EventListener<RunnerEventData> {
  private Terraform terraform
  private Debugger debugger

  TerraformWorkspaceSelectListener(Terraform terraform, Debugger debugger) {
    this.terraform = terraform
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.POST_COMPILE]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return '' != eventData.getMainEventData().getWorkspace() &&
      eventData.getMainEventData().isDeployment()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    this.doRun(eventData.getMainEventData() as TerraformEventData)

    return eventData
  }

  private void doRun(TerraformEventData eventData) {
    this.debugger.print('Switching Terraform workspace to ' + eventData.getWorkspace())

    this.terraform.run(
      'workspace select',
      [(COMMAND_TARGET): "-or-create ${eventData.getWorkspace()}"],
      eventData
    )

    if (this.debugger.debugVarExists()) {
      this.terraform.run('workspace list', [:], eventData)
    }
  }
}
