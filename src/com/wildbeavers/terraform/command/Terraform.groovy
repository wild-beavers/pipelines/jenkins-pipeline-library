package com.wildbeavers.terraform.command

import com.wildbeavers.command.AbstractCommand
import com.wildbeavers.data.CommandResult
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run terraform commands.
 */
class Terraform extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'terraform'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return '='
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [
      '-help'   : this.argumentHelp,
      '-chdir'  : this.argumentChdir,
      '-version': this.argumentVersion,
    ]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    [
      'apply'           : [
        '-auto-approve'    : this.argumentAutoApprove,
        '-backup'          : this.argumentBackup,
        '-compact-warnings': this.argumentCompactWarnings,
        (COMMAND_TARGET)   : this.argumentCommandTarget,
        '-lock'            : this.argumentLock,
        '-lock-timeout'    : this.argumentLockTimeout,
        '-input'           : this.argumentInput,
        '-noColor'         : this.argumentNoColor,
        '-parallelism'     : this.argumentParallelism,
        '-refresh'         : this.argumentRefresh,
        '-state'           : this.argumentState,
        '-state-out'       : this.argumentStateOut,
        '-target'          : this.argumentTargets,
        '-var'             : this.argumentVars,
        '-var-file'        : this.argumentVarFile,
      ],

      'destroy'         : [
        '-auto-approve'    : this.argumentAutoApprove,
        '-compact-warnings': this.argumentCompactWarnings,
        (COMMAND_TARGET)   : this.argumentCommandTarget,
        '-lock'            : this.argumentLock,
        '-lock-timeout'    : this.argumentLockTimeout,
        '-input'           : this.argumentInput,
        '-noColor'         : this.argumentNoColor,
        '-parallelism'     : this.argumentParallelism,
        '-refresh'         : this.argumentRefresh,
        '-state'           : this.argumentState,
        '-target'          : this.argumentTargets,
        '-var'             : this.argumentVars,
        '-var-file'        : this.argumentVarFile,
      ],

      'init'            : [
        '-backend'       : this.argumentBackend,
        '-backend-config': this.argumentBackendConfigs,
        (COMMAND_TARGET) : this.argumentCommandTarget,
        '-force-copy'    : this.argumentForceCopy,
        '-from-module'   : this.argumentFromModule,
        '-get'           : this.argumentGet,
        '-input'         : this.argumentInput,
        '-no-color'      : this.argumentNoColor,
        '-reconfigure'   : this.argumentReconfigure,
        '-plugin-dir'    : this.argumentPluginDirs,
        '-upgrade'       : this.argumentUpgrade,
        '-lockfile'      : this.argumentLockfile,
      ],

      'fmt'             : [
        (COMMAND_TARGET): this.argumentCommandTarget,
        '-list'         : this.argumentList,
        '-write'        : this.argumentWrite,
        '-diff'         : this.argumentDiff,
        '-check'        : this.argumentCheck,
        '-no-Color'     : this.argumentNoColor,
        '-recursive'    : this.argumentRecursive,
      ],

      'plan'            : [
        '-compact-warnings' : this.argumentCompactWarnings,
        (COMMAND_TARGET)    : this.argumentCommandTarget,
        '-detailed-exitcode': this.argumentDetailedExitcode,
        '-input'            : this.argumentInput,
        '-lock'             : this.argumentLock,
        '-lock-timeout'     : this.argumentLockTimeout,
        '-no-color'         : this.argumentNoColor,
        '-out'              : this.argumentOut,
        '-parallelism'      : this.argumentParallelism,
        '-refresh'          : this.argumentRefresh,
        '-state'            : this.argumentState,
        '-target'           : this.argumentTargets,
        '-var'              : this.argumentVars,
        '-var-File'         : this.argumentVarFile,
      ],

      'show'            : [
        (COMMAND_TARGET): this.argumentCommandTarget,
        '-no-color'     : this.argumentNoColor,
        '-json'         : this.argumentJson,
      ],

      'output'          : [
        (COMMAND_TARGET): this.argumentCommandTarget,
        '-state'        : this.argumentState,
        '-no-color'     : this.argumentNoColor,
        '-json'         : this.argumentJson,
        '-raw'          : this.argumentRaw,
      ],

      'validate'        : [
        '-no-color': this.argumentNoColor,
        '-json'    : this.argumentJson,
      ],

      'taint'           : [
        '-allow-missing'        : this.argumentAllowMissing,
        '-backup'               : this.argumentBackup,
        'commandTarget'         : this.argumentCommandTarget,
        '-lock'                 : this.argumentLock,
        '-lock-timeout'         : this.argumentLockTimeout,
        '-state'                : this.argumentState,
        '-state-out'            : this.argumentStateOut,
        '-ignore-remote-version': this.argumentIgnoreRemoteVersion,
      ],

      'state mv'        : [
        '-dry-run'              : this.argumentDryRun,
        '-backup'               : this.argumentBackup,
        '-backup-out'           : this.argumentBackupOut,
        (COMMAND_TARGET)        : this.argumentCommandTarget,
        '-lock'                 : this.argumentLock,
        '-lock-timeout'         : this.argumentLockTimeout,
        '-state'                : this.argumentState,
        '-state-out'            : this.argumentStateOut,
        '-ignore-remote-version': this.argumentIgnoreRemoteVersion,
      ],

      'state rm'        : [
        '-dry-run'              : this.argumentDryRun,
        '-backup'               : this.argumentBackup,
        (COMMAND_TARGET)        : this.argumentCommandTarget,
        '-lock'                 : this.argumentLock,
        '-lock-timeout'         : this.argumentLockTimeout,
        '-state'                : this.argumentState,
        '-ignore-remote-version': this.argumentIgnoreRemoteVersion,
      ],

      'refresh'         : [
        '-compact-warnings': this.argumentCompactWarnings,
        (COMMAND_TARGET)   : this.argumentCommandTarget,
        '-input'           : this.argumentInput,
        '-lock'            : this.argumentLock,
        '-lock-timeout'    : this.argumentLockTimeout,
        '-noColor'         : this.argumentNoColor,
        '-target'          : this.argumentTargets,
        '-var'             : this.argumentVars,
        '-var-file'        : this.argumentVarFile,
      ],

      'workspace select': [
        (COMMAND_TARGET): this.argumentCommandTarget,
      ],

      'workspace list'  : [
        (COMMAND_TARGET): this.argumentCommandTarget,
      ]
    ]
  }

  private argumentAutoApprove = [
    type       : Boolean,
    default    : null,
    description: 'Skip interactive approval of plan before applying.',
  ]
  private argumentAllowMissing = [
    type       : Boolean,
    default    : null,
    description: 'If specified, the command will succeed (exit code 0) even if the resource is missing.',
  ]
  private argumentBackend = [
    type       : Boolean,
    default    : true,
    description: 'Configure the backend for this configuration.',
  ]
  private argumentBackendConfigs = [
    type       : ArrayList,
    default    : '',
    description: 'This can be either a path to an HCL file with key/value assignments (same format as terraform.tfvars) or a \'key=value\' format. This is merged with what is in the configuration file. This can be specified multiple times. The backend type must be in the configuration itself.',
  ]
  private argumentBackup = [
    type       : CharSequence,
    default    : '',
    description: 'Path to backup the existing state file before modifying. Defaults to the "-state-out" path with .backup" extension. Set to "-" to disable backup.',
  ]
  private argumentBackupOut = [
    type       : CharSequence,
    default    : '',
    description: 'Path where Terraform should write the backup for the destination state. This can\'t be disabled. If not set, Terraform will write it to the same path as the destination state file with a backup extension. This only needs to be specified if -state-out is set to a different path than -state.',
  ]
  private argumentChdir = [
    type       : CharSequence,
    default    : '',
    description: 'Switch to a different working directory before executing the given subcommand.',
  ]
  private argumentCheck = [
    type       : Boolean,
    default    : null,
    description: 'Check if the input is formatted. Exit status will be 0 if all input is properly formatted and non-zero otherwise.',
  ]
  private argumentCompactWarnings = [
    type       : Boolean,
    default    : null,
    description: 'If Terraform produces any warnings that are not accompanied by errors, show them in a more compact form that includes only the summary messages.',
  ]
  private argumentDestroy = [
    type       : Boolean,
    default    : null,
    description: 'If set, a plan will be generated to destroy all resources managed by the given configuration and state.',
  ]
  private argumentDetailedExitcode = [
    type       : Boolean,
    default    : null,
    description: 'Return detailed exit codes when the command exits. This will change the meaning of exit codes to:\n' +
      '    0 - Succeeded, diff is empty (no changes)\n' +
      '    1 - Errored\n' +
      '    2 - Succeeded, there is a diff',
  ]
  private argumentDiff = [
    type       : Boolean,
    default    : null,
    description: 'Display diffs of formatting changes.',
  ]
  private argumentDryRun = [
    type       : Boolean,
    default    : null,
    description: 'If set, prints out what would\'ve been moved but doesn\'t actually move anything.',
  ]
  private argumentForceCopy = [
    type       : Boolean,
    default    : null,
    description: 'Suppress prompts about copying state data. This is equivalent to providing a "yes" to all confirmation prompts.',
  ]
  private argumentFromModule = [
    type       : CharSequence,
    default    : '',
    description: 'Copy the contents of the given module into the target directory before initialization.',
  ]
  private argumentGet = [
    type       : Boolean,
    default    : true,
    description: 'Download any modules for this configuration.',
  ]
  private argumentHelp = [
    type       : Boolean,
    default    : null,
    description: 'Show this help output, or the help for a specified subcommand.',
  ]
  private argumentIgnoreRemoteVersion = [
    type       : Boolean,
    default    : false,
    description: 'Continue even if remote and local Terraform versions are incompatible. This may result in an unusable workspace, and should be used with extreme caution.',
  ]
  private argumentInput = [
    type       : Boolean,
    default    : true,
    description: 'Ask for input if necessary. If false, will error if input was required.',
  ]
  private argumentJson = [
    type       : Boolean,
    default    : null,
    description: 'If specified, output the Terraform plan or state in a machine-readable form.',
  ]
  private argumentList = [
    type       : Boolean,
    default    : false,
    description: 'Don\'t list files whose formatting differs (always disabled if using STDIN).',
  ]
  private argumentLock = [
    type       : Boolean,
    default    : true,
    description: 'Lock the state file when locking is supported.',
  ]
  private argumentLockTimeout = [
    type       : CharSequence,
    default    : '0s',
    description: 'Duration to retry a state lock.',
  ]
  private argumentNoColor = [
    type       : Boolean,
    default    : null,
    description: 'If specified, output won\'t contain any color.',
  ]
  private argumentOut = [
    type       : CharSequence,
    default    : '',
    description: 'Write a plan file to the given path. This can be used as input to the "apply" command.',
  ]
  private argumentParallelism = [
    type       : Integer,
    default    : 10,
    description: 'Limit the number of concurrent operations.',
  ]
  private argumentPluginDirs = [
    type       : ArrayList,
    default    : '',
    description: 'Directory containing plugin binaries. This overrides all default search paths for plugins, and prevents the automatic installation of plugins. This flag can be used multiple times.',
  ]
  private argumentRaw = [
    type       : Boolean,
    default    : null,
    description: 'For value types that can be automatically converted to a string, will print the raw  string directly, rather than a human-oriented representation of the value.',
  ]
  private argumentReconfigure = [
    type       : Boolean,
    default    : null,
    description: 'Reconfigure the backend, ignoring any saved configuration.',
  ]
  private argumentRecursive = [
    type       : Boolean,
    default    : null,
    description: 'Also process files in subdirectories. By default, only the given directory (or current directory) is processed.',
  ]
  private argumentRefresh = [
    type       : CharSequence,
    default    : true,
    description: 'Update state prior to checking for differences.',
  ]
  private argumentState = [
    type       : CharSequence,
    default    : '',
    description: 'Path to the state file to read. Defaults to "terraform.tfstate".',
  ]
  private argumentStateOut = [
    type       : CharSequence,
    default    : '',
    description: 'Path to write state to that is different than "-state". This can be used to preserve the old state.',
  ]
  private argumentTargets = [
    type       : ArrayList,
    default    : '',
    description: 'Resource to target. Operation will be limited to this resource and its dependencies. This flag can be used multiple times.',
  ]
  private argumentUpgrade = [
    type       : Boolean,
    default    : false,
    description: 'If installing modules (-get) or plugins (-get-plugin), ignore previously-downloaded objects and install the latest version allowed within configured constraints.',
  ]
  private argumentLockfile = [
    type       : CharSequence,
    validValues: ['readonly'],
    default    : '',
    description: 'Set a dependency lockfile mode. Currently only "readonly" is valid.',
  ]
  private argumentVars = [
    type       : ArrayList,
    default    : '',
    description: 'Set a variable in the Terraform configuration. This flag can be set multiple times.',
  ]
  private argumentVarFile = [
    type       : CharSequence,
    default    : '',
    description: 'Set variables in the Terraform configuration from a file. If "terraform.tfvars" or any ".auto.tfvars" files are present, they will be automatically loaded.',
  ]
  private argumentVersion = [
    type       : Boolean,
    default    : null,
    description: 'An alias for the "version" subcommand.',
  ]
  private argumentWrite = [
    type       : Boolean,
    default    : false,
    description: 'Don\'t write to source files (always disabled if using STDIN or -check).',
  ]
  private argumentCommandTarget = [
    type       : CharSequence,
    default    : '',
    description: 'Command target.',
  ]

  Terraform(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }

  @Override
  CommandResult run(String subcommand, Map<String, Object> arguments, HasContainerOptions containerOptions) {
    if (this.debugger.debugVarExists()) {
      containerOptions.getContainerOptions().setEnvironmentVariables(containerOptions.getContainerOptions().getEnvironmentVariables())
    }

    return super.run(subcommand, arguments, containerOptions)
  }
}
