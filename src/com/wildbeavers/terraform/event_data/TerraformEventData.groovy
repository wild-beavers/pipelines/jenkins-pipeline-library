package com.wildbeavers.terraform.event_data

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.observer.EventDataInterface

// Uses interfaces instead of traits, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
class TerraformEventData implements EventDataInterface, HasContainerOptions {
  static final DEPLOYMENT_FILENAME = 'deploy.tf'
  static final DEFAULT_CONTAINER_TOOL = 'podman'
  static final DEFAULT_CONTAINER_FQDN = 'docker.io/hashicorp'
  static final DEFAULT_CONTAINER_IMAGE_NAME = 'terraform'
  static final DEFAULT_CONTAINER_IMAGE_TAG = 'latest'
  static final DEFAULT_CONTAINER_FALLBACK_COMMAND = 'terraform'
  static final DEFAULT_EXAMPLE_DIRECTORY = 'examples'
  static final DEFAULT_COMMAND_TARGETS = ['.']

  private List<String> commandTargets = DEFAULT_COMMAND_TARGETS
  private Boolean isDeployment = false
  private String workspace = ''
  private String examplesDirectory = DEFAULT_EXAMPLE_DIRECTORY
  private Boolean fmtEnabled = true
  private Boolean validateEnabled = true
  private Boolean idempotencyCheckEnabled = true

  private WildMap<String, Object> validateOptions = [:] as WildMap
  private WildMap<String, Object> fmtOptions = [:] as WildMap
  private WildMap<String, Object> initOptions = [:] as WildMap
  private WildMap<String, Object> planOptions = [:] as WildMap
  private WildMap<String, Object> applyOptions = [:] as WildMap
  private WildMap<String, Object> destroyOptions = [:] as WildMap
  private List<String> sensitiveResources = []

  TerraformEventData(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }

  List<String> getCommandTargets() {
    return commandTargets
  }

  void setCommandTargets(List<String> commandTargets) {
    this.commandTargets = commandTargets
  }

  void addCommandTarget(CharSequence commandTarget) {
    this.commandTargets.add(commandTarget.toString())
  }

  Boolean isDeployment() {
    return this.isDeployment
  }

  void setIsDeployment(Boolean isDeployment) {
    this.isDeployment = isDeployment
  }

  String getWorkspace() {
    return workspace
  }

  void setWorkspace(String workspace) {
    this.workspace = workspace
  }

  String getExamplesDirectory() {
    return this.examplesDirectory
  }

  void setExamplesDirectory(String examplesDirectory) {
    this.examplesDirectory = examplesDirectory
  }

  Boolean isFmtEnabled() {
    return this.fmtEnabled
  }

  void setFmtEnabled(Boolean fmtEnabled) {
    this.fmtEnabled = fmtEnabled
  }

  Boolean isValidateEnabled() {
    return this.validateEnabled
  }

  void setValidateEnabled(Boolean validateEnabled) {
    this.validateEnabled = validateEnabled
  }

  Boolean isIdempotencyCheckEnabled() {
    return this.idempotencyCheckEnabled
  }

  void setIdempotencyCheckEnabled(Boolean idempotencyCheckEnabled) {
    this.idempotencyCheckEnabled = idempotencyCheckEnabled
  }

  WildMap<String, Object> getValidateOptions() {
    return validateOptions
  }

  void setValidateOptions(WildMap<String, Object> validateOptions) {
    this.validateOptions = validateOptions
  }

  WildMap<String, Object> getFmtOptions() {
    return fmtOptions
  }

  void setFmtOptions(WildMap<String, Object> fmtOptions) {
    this.fmtOptions = fmtOptions
  }

  WildMap<String, Object> getInitOptions() {
    return initOptions
  }

  void setInitOptions(WildMap<String, Object> initOptions) {
    this.initOptions = initOptions
  }

  WildMap<String, Object> getPlanOptions() {
    return planOptions
  }

  void setPlanOptions(WildMap<String, Object> planOptions) {
    this.planOptions = planOptions
  }

  WildMap<String, Object> getApplyOptions() {
    return applyOptions
  }

  void setApplyOptions(WildMap<String, Object> applyOptions) {
    this.applyOptions = applyOptions
  }

  WildMap<String, Object> getDestroyOptions() {
    return destroyOptions
  }

  void setDestroyOptions(WildMap<String, Object> destroyOptions) {
    this.destroyOptions = destroyOptions
  }

  List<String> getSensitiveResources() {
    return sensitiveResources
  }

  void setSensitiveResources(List<String> sensitiveResources) {
    this.sensitiveResources = sensitiveResources
  }

  // HasContainerOptions

  private ContainerOptions containerOptions

  ContainerOptions getContainerOptions() {
    return containerOptions
  }

  void setContainerOptions(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }
}
