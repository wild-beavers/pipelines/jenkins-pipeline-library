package com.wildbeavers.terraform.helper

import com.wildbeavers.data.CommandResult

/**
 * Utilities for Terraform plans.
 * Analyse plan outputs.
 */
class TerraformPlanChangesCheckerHelper {
  /**
   * Checks whether or not a CommandResult of a Terraform plan has pending changes or not
   * @param commandResult
   * @return
   */
  static boolean outputContainChanges(CommandResult commandResult) {
    return !(
      commandResult.getStdout() =~ /.*(Infrastructure is up-to-date).*/ ||
        commandResult.getStdout() =~ /.*([^0-9]0 to register, 0 to change, 0 to destroy).*/ ||
        commandResult.getStdout() =~ /.*(Your infrastructure matches the configuration).*/
    )
  }

  /**
   * Returns a list of resources that were found in a Terraform plan’s CommandResult
   * @param commandResult
   * @return List
   */
  static List getSensitiveResourceToDestroy(List resourceNames, CommandResult commandResult) {
    return resourceNames.findAll({
      commandResult.getStdout() =~ /.*\.${it}\..* (destroyed|replaced)/
    })
  }
}
