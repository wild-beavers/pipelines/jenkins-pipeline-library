package com.wildbeavers.python

import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.data.PythonBuildData

class PythonBuildDataManager implements CanManagedPythonData<PythonBuildData, PythonBuilderType> {
  private ContainerOptionsManager dockerOptionsManager
  private String pythonDockerImageName

  static final String PYTHON_DOCKER_IMAGE_NAME = 'docker.io/library/python:latest'

  PythonBuildDataManager(ContainerOptionsManager dockerOptionsManager, pythonDockerImageName = PythonBuildDataManager.PYTHON_DOCKER_IMAGE_NAME) {
    this.dockerOptionsManager = dockerOptionsManager
    this.pythonDockerImageName = pythonDockerImageName
  }

  /**
   * {@inheritDoc}
   */
  PythonBuildData create(Map<String, Object> pythonBuildConfig) {
    PythonBuildData pythonBuildData = new PythonBuildData()

    pythonBuildConfig.each {
      switch (it.key) {
        case "appendCommand":
          pythonBuildData.setAppendCommand(it.value)
          break
        case "arguments":
          pythonBuildData.setArguments(it.value)
          break
        case "dockerOptions":
          pythonBuildData.setContainerOptions(this.dockerOptionsManager.create(it.value))
          break
        default:
          throw new Exception("The configuration key “" + it.key + "” is not a supported for python build. Supported types: “" + PythonBuildData.SUPPORTED_CONFIGURATION_KEYS + "”")
      }
    }

    if (null == pythonBuildData.getContainerOptions()) {
      pythonBuildData.setContainerOptions(this.dockerOptionsManager.create([:]))
    }

    if (! pythonBuildData.getContainerOptions().hasImage()) {
      pythonBuildData.getContainerOptions().setImage(this.pythonDockerImageName)
    }

    return pythonBuildData
  }

  /**
   * {@inheritDoc}
   */
  PythonBuilderType getType() {
    return PythonBuilderType.PYTHON_BUILD
  }
}
