package com.wildbeavers.python.data

import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.python.tests.PythonTestsType

interface HasPythonTestsData extends HasContainerOptions {
  Boolean getAppendCommand()

  void setAppendCommand(Boolean appendCommand)

  Map<String, Object> getArguments()

  void setArguments(Map<String, Object> arguments)

  PythonTestsType getType()

  String getDataName()

  void setName(String name)
}
