package com.wildbeavers.python.data

import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.python.builder.PythonBuilderType

class PythonData implements EventDataInterface {
  private HasPythonBuilderData pythonBuilderData
  private PythonBuilderType pythonBuilderType

  private List<HasPythonTestsData> pythonTestsDatas = []

  PythonBuilderType getPythonBuilderType() {
    return pythonBuilderType
  }

  void setPythonBuilderType(PythonBuilderType pythonBuilderType) {
    this.pythonBuilderType = pythonBuilderType
  }

  HasPythonBuilderData getPythonBuilderData() {
    return pythonBuilderData
  }

  void setPythonBuilderData(HasPythonBuilderData pythonBuilderData) {
    this.pythonBuilderData = pythonBuilderData
  }

  List<HasPythonTestsData> getPythonTestsDatas() {
    return pythonTestsDatas
  }

  void setPythonTestsDatas(List<HasPythonTestsData> pythonTestsDatas) {
    this.pythonTestsDatas = pythonTestsDatas
  }

  void addPythonTestsData(HasPythonTestsData pythonTestsData) {
    this.pythonTestsDatas.add(pythonTestsData)
  }

  HasPythonTestsData getPythonTestsDataByName(String name) {
    return this.pythonTestsDatas.find({
      it.getDataName() == name
    })
  }
}
