package com.wildbeavers.python.data


import com.wildbeavers.container.data.HasContainerOptions

interface HasPythonBuilderData extends HasContainerOptions {
  Boolean getAppendCommand()

  void setAppendCommand(Boolean appendCommand)

  Map<String, Object> getArguments()

  void setArguments(Map<String, Object> arguments)
}
