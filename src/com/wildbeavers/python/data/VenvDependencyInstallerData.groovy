package com.wildbeavers.python.data

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.data.DependencyInstallerData

/**
 * Data for VenvDependencyInstallerData
 */
class VenvDependencyInstallerData extends DependencyInstallerData implements HasContainerOptions, HasPipRequirementsData {
  final static String DESTINATION_DOCKER_FOLDER_PATH = "/opt/venv"
  final static String SOURCE_LOCAL_FOLDER_PATH = "venv"

  final static List<String> SUPPORTED_CONFIGURATION_KEYS = [
    "requirementFiles",
    "requirementEnvironments",
    "packageNames",
    "destinationDockerFolderPath",
    "sourceLocalFolderPath",
    "pathEnvironmentVariable",
    "dockerOptions"
  ]

  ContainerOptions dockerOptions = new ContainerOptions()

  private String destinationDockerFolderPath = DESTINATION_DOCKER_FOLDER_PATH
  private String sourceLocalFolderPath = SOURCE_LOCAL_FOLDER_PATH
  private String pathEnvironmentVariable

  private List<String> requirementFiles = []
  private List<String> requirementEnvironments = []
  private List<String> packageNames = []

  /**
   * {inheritDoc}
   */
  List<String> getRequirementFiles() {
    return requirementFiles
  }

  /**
   * {@inheritDoc}
   */
  void setRequirementFiles(List<String> requirementFiles) {
    this.requirementFiles = requirementFiles
  }

  /**
   * {@inheritDoc}
   */
  List<String> getRequirementEnvironments() {
    return this.requirementEnvironments
  }

  /**
   * {@inheritDoc}
   */
  void setRequirementEnvironments(List<String> requirementEnvironments) {
    this.requirementEnvironments = requirementEnvironments
  }

  /**
   * {@inheritDoc}
   */
  List<String> getPackageNames() {
    return packageNames
  }

  /**
   * {@inheritDoc}
   */
  void setPackageNames(List<String> packageNames) {
    this.packageNames = packageNames
  }

  String getDestinationDockerFolderPath() {
    return this.destinationDockerFolderPath
  }

  void setDestinationDockerFolderPath(String dockerFolderPath) {
    this.destinationDockerFolderPath = dockerFolderPath
  }

  String getSourceLocalFolderPath() {
    return this.sourceLocalFolderPath
  }

  void setSourceLocalFolderPath(String localFolderPath) {
    this.sourceLocalFolderPath = localFolderPath
  }

  String getPathEnvironmentVariable() {
    return this.pathEnvironmentVariable
  }

  void setPathEnvironmentVariable(String pathEnvironmentVariable) {
    this.pathEnvironmentVariable = pathEnvironmentVariable
  }

  /**
   * {@inheritDoc}
   */
  @Override
  ContainerOptions getContainerOptions() {
    return this.dockerOptions
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void setContainerOptions(ContainerOptions dockerOptions) {
    this.dockerOptions = dockerOptions
  }
}
