package com.wildbeavers.python.data


import com.wildbeavers.container.data.ContainerOptions

/**
 * Data used by PythonBuildBuilder.
 */
class PythonBuildData implements HasPythonBuilderData {
  private ContainerOptions dockerOptions
  private Boolean appendCommand = true
  private Map<String, Object> arguments = [:]

  final static List<String> SUPPORTED_CONFIGURATION_KEYS = [
    "appendCommand",
    "arguments",
    "dockerOptions"
  ]

  @Override
  ContainerOptions getContainerOptions() {
    return dockerOptions
  }

  @Override
  void setContainerOptions(ContainerOptions dockerOptions) {
    this.dockerOptions = dockerOptions
  }

  Boolean getAppendCommand() {
    return appendCommand
  }

  void setAppendCommand(Boolean appendCommand) {
    this.appendCommand = appendCommand
  }

  Map<String, Object> getArguments() {
    return arguments
  }

  void setArguments(Map<String, Object> arguments) {
    this.arguments = arguments
  }
}
