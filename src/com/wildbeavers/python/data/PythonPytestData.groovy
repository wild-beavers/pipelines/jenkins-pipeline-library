package com.wildbeavers.python.data


import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.python.tests.PythonTestsType

class PythonPytestData implements HasPythonTestsData {
  private ContainerOptions dockerOptions
  private Boolean appendCommand = true
  private Map<String, Object> arguments = [:]
  private String name = ''

  final static List<String> SUPPORTED_CONFIGURATION_KEYS = [
    "appendCommand",
    "arguments",
    "dockerOptions",
    "name"
  ]

  @Override
  ContainerOptions getContainerOptions() {
    return this.dockerOptions
  }

  @Override
  void setContainerOptions(ContainerOptions dockerOptions) {
    this.dockerOptions = dockerOptions
  }

  Boolean getAppendCommand() {
    return this.appendCommand
  }

  void setAppendCommand(Boolean appendCommand) {
    this.appendCommand = appendCommand
  }

  Map<String, Object> getArguments() {
    return this.arguments
  }

  void setArguments(Map<String, Object> arguments) {
    this.arguments = arguments
  }

  PythonTestsType getType() {
    return PythonTestsType.PYTEST
  }

  @Override
  String getDataName() {
    return name
  }

  @Override
  void setName(String name) {
    this.name = name
  }
}
