package com.wildbeavers.python.data

interface HasPipRequirementsData {
  /**
   * Get the list of requirement files
   */
  List<String> getRequirementFiles()

  /**
   * Set the list of requirement files to be installed
   * @param requirementFiles List of requirement file path to be install
   */
  void setRequirementFiles(List<String> requirementFiles)

  /**
   * Get requirement environments
   */
  List<String> getRequirementEnvironments()

  /**
   * Set the list of requirement environment to be installed
   * @param requirementEnvironments
   */
  void setRequirementEnvironments(List<String> requirementEnvironments)

  /**
   * List of packages to be installed
   */
  List<String> getPackageNames()

  /**
   * Set the list of packages to be installed
   * @param packageNames The list of packages to be installed
   */
  void setPackageNames(List<String> packageNames)
}
