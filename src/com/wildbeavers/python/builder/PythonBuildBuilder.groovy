package com.wildbeavers.python.builder

import com.wildbeavers.python.command.PythonBuild
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.python.data.HasPythonBuilderData

/**
 * Build a python project using Python module build.
 */
class PythonBuildBuilder implements PythonBuilder {
  private PythonBuild pythonBuild

  /**
   * @param pythonBuild
   */
  PythonBuildBuilder(PythonBuild pythonBuild) {
    this.pythonBuild = pythonBuild
  }

  @Override
  void build(HasPythonBuilderData pythonBuilderData) {
    if (pythonBuilderData.getAppendCommand()) {
      this.pythonBuild.runWithPrefix('', pythonBuilderData.getArguments(), pythonBuilderData)
      return
    }

    this.pythonBuild.run('', pythonBuilderData.getArguments(), pythonBuilderData)
  }

  @Override
  Boolean canBuild(ScmInfo scmInfo) {
    // This is not yet implemented
    // This can be done by parsing some files like pyproject.toml
    return true
  }
}
