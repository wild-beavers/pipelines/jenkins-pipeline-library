package com.wildbeavers.python.builder


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.composite.FetcherInterface
import com.wildbeavers.data.ScmInfo

class PythonBuilderFetcher implements FetcherInterface<PythonBuilderType, PythonBuilder> {
  private FetcherDeprecated fetcher

  PythonBuilderFetcher(FetcherDeprecated fetcher) {
    this.fetcher = fetcher
  }

  void register(PythonBuilderType type, PythonBuilder value) {
    this.fetcher.register(type, value)
  }

  PythonBuilder fetch(PythonBuilderType type) {
    return this.fetcher.fetch(type)
  }

  /**
   * Fetch a PythonBuilder class by its type.
   * @param type The class type to fetch
   * @param scmInfo The ScmInfo used to determine if the fetched class is able to build or not
   * @return null if the fetched class can not test the project, the fetched class otherwise
   */
  PythonBuilder fetch(PythonBuilderType type, ScmInfo scmInfo) {
    if (!this.fetch(type).canBuild(scmInfo)) {
      return null
    }

    return this.fetch(type)
  }
}
