package com.wildbeavers.python.builder

import com.wildbeavers.data.ScmInfo
import com.wildbeavers.python.data.HasPythonBuilderData

/**
 * Interface that describes a python project  build
 */
interface PythonBuilder {
  /**
   * Build a python project.
   * @param hasPythonBuilderData The data object needed to build the project
   */
  void build(HasPythonBuilderData hasPythonBuilderData)

  /**
   * Whether the class can build the project or not.
   * @param scmInfo The scmInfo object that contains the list of current repository files
   * @return
   */
  Boolean canBuild(ScmInfo scmInfo)
}
