package com.wildbeavers.python.command

import com.wildbeavers.command.AbstractCommand
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run python commands.
 */
class PythonBuild extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'python -m build'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return ' '
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [
      '--help'                  : this.argumentHelp,
      '-h'                      : this.argumentHelp,
      '--version'               : this.argumentVersion,
      '-V'                      : this.argumentVersion,
      '--sdist'                 : this.argumentSdist,
      '-s'                      : this.argumentSdist,
      '--wheel'                 : this.argumentWheel,
      '-w'                      : this.argumentWheel,
      '--outdir'                : this.argumentOutdir,
      '-o'                      : this.argumentOutdir,
      '--skip-dependency-check' : this.argumentSkipDependencyCheck,
      '-x'                      : this.argumentSkipDependencyCheck,
      '--no-isolation'          : this.argumentNoIsolation,
      '-n'                      : this.argumentNoIsolation,
      '--config-setting'        : this.argumentConfigSetting,
      '-C'                      : this.argumentConfigSetting,
      (COMMAND_TARGET)          : this.argumentCommandTarget,
    ]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    return [:]
  }


  private argumentHelp = [
    type       : Boolean,
    default    : null,
    description: 'Show this help output.',
  ]
  private argumentVersion = [
    type       : Boolean,
    default    : null,
    description: 'Get the version.',
  ]
  private argumentSdist = [
    type       : Boolean,
    default    : null,
    description: 'Build a source distribution (disables the default behavior).',
  ]
  private argumentWheel = [
    type       : Boolean,
    default    : null,
    description: 'Build a wheel (disables the default behavior).',
  ]
  private argumentOutdir = [
    type       : CharSequence,
    default    : null,
    description: 'Output directory (defaults to {srcdir}/dist).',
  ]
  private argumentSkipDependencyCheck = [
    type       : Boolean,
    default    : null,
    description: 'Do not check that build dependencies are installed.',
  ]
  private argumentNoIsolation = [
    type       : Boolean,
    default    : null,
    description: 'Do not isolate the build in a virtual environment.',
  ]
  private argumentConfigSetting = [
    type       : CharSequence,
    default    : null,
    description: 'Pass options to the backend. Options which begin with a hyphen must be in the form of “--config-setting=--opt(=value)” or “-C--opt(=value)”.',
  ]
  private argumentCommandTarget = [
    type       : CharSequence,
    default    : '',
    description: 'Command target.',
  ]

  PythonBuild(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }
}
