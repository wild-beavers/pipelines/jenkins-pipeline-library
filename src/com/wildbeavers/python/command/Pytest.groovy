package com.wildbeavers.python.command

import com.wildbeavers.command.AbstractCommand
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run PyTest commands.
 */
class Pytest extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'pytest'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return '='
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [
      '-k'                              : this.argumentTestCase,
      '-m'                              : this.argumentTestMatch,
      '--markers'                       : this.argumentMarkers,
      '--exitfirst'                     : this.argumentExitFirst,
      '-x'                              : this.argumentExitFirst,
      '--fixtures'                      : this.argumentShowFixtures,
      '--funcargs  '                    : this.argumentShowFixtures,
      '--fixtures-per-test'             : this.argumentShowFixturesPerTest,
      '--trace'                         : this.argumentTrace,
      '--capture'                       : this.argumentCapture,
      '-s'                              : this.argumentNoCapture,
      '--runxfail'                      : this.argumentRunXFail,
      '--lf'                            : this.argumentLastFail,
      '--last-failed'                   : this.argumentLastFail,
      '--ff'                            : this.argumentFailFirst,
      '--failed-first'                  : this.argumentFailFirst,
      '--nf'                            : this.argumentNewFirst,
      '--new-first'                     : this.argumentNewFirst,
      '--cache-show'                    : this.argumentCacheShow,
      '--cache-clear'                   : this.argumentClearCache,
      '--spec'                          : this.argumentSpec,
      '--pep8'                          : this.argumentPep8,


      '--durations'                     : this.argumentDurations,
      '--durations-min'                 : this.argumentDurationsMin,
      '--verbose'                       : this.argumentVerbose,
      '-v'                              : this.argumentVerbose,
      '--no-header'                     : this.argumentNoHeader,
      '--no-summary'                    : this.argumentNoSummary,
      '--quiet'                         : this.argumentQuiet,
      '-q'                              : this.argumentQuiet,
      '--verbosity'                     : this.argumentVerbosity,
      '--r'                             : this.argumentShowExtraTestSummary,
      '--disable-warnings'              : this.argumentDisableWarnings,
      '--disable-pytest-warnings'       : this.argumentDisableWarnings,
      '--showlocals'                    : this.argumentShowLocals,
      '-l'                              : this.argumentShowLocals,
      '--no-showlocals'                 : this.argumentNoShowLocals,
      '--tb'                            : this.argumentTracebackMode,
      '--show-capture'                  : this.argumentShowCapture,
      '--full-trace'                    : this.argumentFullTrace,
      '--color'                         : this.argumentColor,
      '--code-highlight'                : this.argumentCodeHighlight,
      '--pastebin'                      : this.argumentPastbin,
      '--junit-xml'                     : this.argumentJunitPath,
      '--junit-prefix'                  : this.argumentJunitPrefix,

      '--pythonwarnings'                : this.argumentPythonWarnings,
      '-W'                              : this.argumentPythonWarnings,
      '--maxfail'                       : this.argumentMaxFail,
      '--strict-config'                 : this.argumentStrictConfig,
      '--strict-markers'                : this.argumentStrictMarkers,
      '--strict'                        : this.argumentStrict,
      '-c'                              : this.argumentConfig,
      '--continue-on-collection-errors' : this.argumentContinueOnCollectionErrors,
      '--rootdir'                       : this.argumentRootDir,

      '--collect-only'                  : this.argumentCollectOnly,
      '--co'                            : this.argumentCollectOnly,
      '--pyargs'                        : this.argumentPyargs,
      '--ignore'                        : this.argumentIgnore,
      '--ignore-glob'                   : this.argumentIgnoreGlob,
      '--deselect'                      : this.argumentDeselect,
      '--confcutdir'                    : this.argumentConfcutdir,
      '--noconftest'                    : this.argumentNoConftest,
      '--keep-duplicates'               : this.argumentKeepDuplicates,
      '--collect-in-virtualenv'         : this.argumentCollectInVirtualenv,
      '--import-mode'                   : this.argumentImportMode,
      '--doctest-modules'               : this.argumentDoctestModules,
      '--doctest-report'                : this.argumentDoctestReport,
      '--doctest-glob'                  : this.argumentDoctestGlob,
      '--doctest-ignore-import-errors'  : this.argumentDoctestIgnoreImportErrors,
      '--doctest-continue-on-failure'   : this.argumentDoctestContinueOnFailure,

      '--basetemp'                      : this.argumentBasetemp,
      '--version'                       : this.argumentVersion,
      '-V'                              : this.argumentVersion,
      '--help'                          : this.argumentHelp,
      '-p'                              : this.argumentPlugin,
      '--trace-config'                  : this.argumentTraceConfig,
      '--debug'                         : this.argumentDebug,
      '-o'                              : this.argumentOverride,
      '--assert'                        : this.argumentAssert,
      '--setup-only'                    : this.argumentSetupOnly,
      '--setup-show'                    : this.argumentSetupShow,
      '--setup-plan'                    : this.argumentSetupPlan,

      '--log-level'                     : this.argumentLogLevel,
      '--log-format'                    : this.argumentLogFormat,
      '--log-date-format'               : this.argumentLogDateFormat,
      '--log-cli-level'                 : this.argumentLogCliLevel,
      '--log-cli-format'                : this.argumentLogCliFormat,
      '--log-cli-date-format'           : this.argumentLogCliDateFormat,
      '--log-file'                      : this.argumentLogFile,
      '--log-file-level'                : this.argumentLogFileLevel,
      '--log-file-format'               : this.argumentLogFileFormat,
      '--log-file-date-format'          : this.argumentLogFileDateFormat,
      '--log-auto-indent'               : this.argumentLogAutoIndent,

      '--cov'                           : this.argumentCov,
      '--cov-reset'                     : this.argumentCovReset,
      '--cov-report'                    : this.argumentCovReport,
      '--cov-config'                    : this.argumentCovConfig,
      '--no-cov'                        : this.argumentNoCov,
      '--cov-fail-under'                : this.argumentCovFailUnder,
      '--cov-append'                    : this.argumentCovAppend,
      '--cov-branch'                    : this.argumentCovBranch,
      '--cov-context'                   : this.argumentCovContext,

      (COMMAND_TARGET)            : this.argumentCommandTarget,
    ]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    return [:]
  }


  private argumentHelp = [
    type       : Boolean,
    default    : null,
    description: 'Show help message and configuration info.',
  ]
  private argumentTestCase = [
    type       : CharSequence,
    default    : null,
    description: 'Only run tests which match the given substring expression.',
  ]
  private argumentTestMatch = [
    type       : CharSequence,
    default    : null,
    description: 'Only run tests matching given mark expression. For example: -m "mark1 and not mark2".',
  ]
  private argumentMarkers = [
    type       : CharSequence,
    default    : null,
    description: 'Show markers (builtin, plugin and per-project ones).',
  ]
  private argumentExitFirst = [
    type       : Boolean,
    default    : null,
    description: 'Exit instantly on first error or failed test.',
  ]
  private argumentShowFixtures = [
    type       : Boolean,
    default    : null,
    description: 'Show available fixtures, sorted by plugin appearance (fixtures with leading "_" are only shown with "-v").',
  ]
  private argumentShowFixturesPerTest = [
    type       : Boolean,
    default    : null,
    description: 'Show fixtures per test.',
  ]
  private argumentTrace = [
    type       : Boolean,
    default    : null,
    description: 'Immediately break when running each test.',
  ]
  private argumentCapture = [
    type       : CharSequence,
    default    : null,
    description: 'Per-test capturing method: one of fd|sys|no|tee-sys.',
  ]
  private argumentNoCapture = [
    type       : Boolean,
    default    : null,
    description: 'Shortcut for --capture=no.',
  ]
  private argumentRunXFail = [
    type       : CharSequence,
    default    : null,
    description: 'Report the results of xfail tests as if they were not marked.',
  ]
  private argumentLastFail = [
    type       : Boolean,
    default    : null,
    description: 'Rerun only the tests that failed at the last run (or all if none failed).',
  ]
  private argumentFailFirst = [
    type       : Boolean,
    default    : null,
    description: 'Run all tests, but run the last failures first. This may re-order tests and thus lead to repeated fixture setup/teardown.',
  ]
  private argumentNewFirst = [
    type       : Boolean,
    default    : null,
    description: 'Run tests from new files first, then the rest of the tests sorted by file mtime.',
  ]
  private argumentCacheShow = [
    type       : CharSequence,
    default    : null,
    description: 'Show cache contents, don\'t perform collection or tests. Optional argument: glob (default: "*").',
  ]
  private argumentClearCache = [
    type       : Boolean,
    default    : null,
    description: 'Remove all cache contents at start of test run.',
  ]
  private argumentSpec = [
    type       : Boolean,
    default    : null,
    description: 'Print test result in specification format.'
  ]
  private argumentPep8 = [
    type       : Boolean,
    default    : null,
    description: 'Perform some pep8 sanity checks on .py files.'
  ]
  private argumentDurations = [
    type       : Integer,
    default    : null,
    description: 'Show N slowest setup/test durations (N=0 for all).',
  ]
  private argumentDurationsMin = [
    type       : Integer,
    default    : null,
    description: 'Minimal duration in seconds for inclusion in slowest list. Default: 0.005.',
  ]
  private argumentVerbose = [
    type       : Boolean,
    default    : null,
    description: 'Increase verbosity.',
  ]
  private argumentNoHeader = [
    type       : Boolean,
    default    : null,
    description: 'Disable header.',
  ]
  private argumentNoSummary = [
    type       : Boolean,
    default    : null,
    description: 'Disable summary.',
  ]
  private argumentQuiet = [
    type       : Boolean,
    default    : null,
    description: 'Decrease verbosity.',
  ]
  private argumentVerbosity = [
    type       : CharSequence,
    default    : null,
    description: 'Set verbosity. Default: 0.',
  ]
  private argumentShowExtraTestSummary = [
    type       : CharSequence,
    default    : null,
    description: 'Show extra test summary info as specified by chars: (f)ailed, (E)rror, (s)kipped, (x)failed, ' +
      '(X)passed, (p)assed, (P)assed with output, (a)ll except passed (p/P), or (A)ll. ' +
      '(w)arnings are enabled by default (see --disable-warnings), \'N\' can be used to reset the list. (default: \'fE\').',
  ]
  private argumentDisableWarnings = [
    type       : Boolean,
    default    : null,
    description: 'Disable warnings summary.',
  ]
  private argumentShowLocals = [
    type       : Boolean,
    default    : null,
    description: 'Show locals in tracebacks (disabled by default).',
  ]
  private argumentNoShowLocals = [
    type       : Boolean,
    default    : null,
    description: 'Hide locals in tracebacks (negate --showlocals passed through addopts).',
  ]
  private argumentTracebackMode = [
    type       : CharSequence,
    default    : null,
    description: 'Traceback print mode (auto/long/short/line/native/no).',
  ]
  private argumentShowCapture = [
    type       : CharSequence,
    default    : null,
    description: 'Controls how captured stdout/stderr/log is shown on failed tests. Default: all.',
  ]
  private argumentFullTrace = [
    type       : Boolean,
    default    : null,
    description: 'Don\'t cut any tracebacks (default is to cut).',
  ]
  private argumentColor = [
    type       : CharSequence,
    default    : null,
    description: 'Color terminal output (yes/no/auto).',
  ]
  private argumentCodeHighlight = [
    type       : CharSequence,
    default    : null,
    description: 'Whether code should be highlighted (only if --color is also enabled). Default: yes.',
  ]
  private argumentPastbin = [
    type       : CharSequence,
    default    : null,
    description: 'Send failed|all info to bpaste.net pastebin service.',
  ]
  private argumentJunitPath = [
    type       : CharSequence,
    default    : null,
    description: 'Create junit-xml style report file at given path.',
  ]
  private argumentJunitPrefix = [
    type       : CharSequence,
    default    : null,
    description: 'Prepend prefix to classnames in junit-xml output.',
  ]
  private argumentPythonWarnings = [
    type       : CharSequence,
    default    : null,
    description: 'Set which warnings to report, see -W option of Python itself.',
  ]
  private argumentMaxFail = [
    type       : Integer,
    default    : null,
    description: 'Exit after first num failures or errors.',
  ]
  private argumentStrictConfig = [
    type       : Boolean,
    default    : null,
    description: 'Any warnings encountered while parsing the `pytest` section of the configuration file raise errors.',
  ]
  private argumentStrictMarkers = [
    type       : Boolean,
    default    : null,
    description: 'Markers not registered in the `markers` section of the configuration file raise errors.',
  ]
  private argumentStrict = [
    type       : Boolean,
    default    : null,
    description: '(Deprecated) alias to --strict-markers.',
  ]
  private argumentConfig = [
    type       : CharSequence,
    default    : null,
    description: 'Load configuration from `file` instead of trying to locate one of the implicit configuration files.',
  ]
  private argumentContinueOnCollectionErrors = [
    type       : Boolean,
    default    : null,
    description: 'Force test execution even if collection errors occur.',
  ]
  private argumentRootDir = [
    type       : CharSequence,
    default    : null,
    description: 'Define root directory for tests. Can be relative path: \'root_dir\', \'./root_dir\', ' +
      '\'root_dir/another_dir/\'; absolute path: \'/home/user/root_dir\'; path with variables: \'$HOME/root_dir\'.',
  ]
  private argumentCollectOnly = [
    type       : Boolean,
    default    : null,
    description: 'Only collect tests, don\'t execute them.',
  ]
  private argumentPyargs = [
    type       : Boolean,
    default    : null,
    description: 'Try to interpret all arguments as Python packages.',
  ]
  private argumentIgnore = [
    type       : CharSequence,
    default    : null,
    description: 'Ignore path during collection (multi-allowed).',
  ]
  private argumentIgnoreGlob = [
    type       : CharSequence,
    default    : null,
    description: 'Ignore path pattern during collection (multi-allowed)',
  ]
  private argumentDeselect = [
    type       : CharSequence,
    default    : null,
    description: 'Deselect item (via node id prefix) during collection (multi-allowed)',
  ]
  private argumentConfcutdir = [
    type       : Boolean,
    default    : null,
    description: 'Only load conftest.py\'s relative to specified dir.',
  ]
  private argumentNoConftest = [
    type       : Boolean,
    default    : null,
    description: 'Don\'t load any conftest.py files.',
  ]
  private argumentKeepDuplicates = [
    type       : Boolean,
    default    : null,
    description: 'Keep duplicate tests.',
  ]
  private argumentCollectInVirtualenv = [
    type       : Boolean,
    default    : null,
    description: 'Don\'t ignore tests in a local virtualenv directory.',
  ]
  private argumentImportMode = [
    type       : CharSequence,
    default    : null,
    description: 'Prepend/append to sys.path when importing test modules and conftest files. Default: prepend.',
  ]
  private argumentDoctestModules = [
    type       : Boolean,
    default    : null,
    description: 'Run doctests in all .py modules.',
  ]
  private argumentDoctestReport = [
    type       : CharSequence,
    default    : null,
    description: 'Choose another output format for diffs on doctest failure.',
  ]
  private argumentDoctestGlob = [
    type       : CharSequence,
    default    : null,
    description: 'Doctests file matching pattern, default: test*.txt.',
  ]
  private argumentDoctestIgnoreImportErrors = [
    type       : Boolean,
    default    : null,
    description: 'Ignore doctest ImportErrors.',
  ]
  private argumentDoctestContinueOnFailure = [
    type       : Boolean,
    default    : null,
    description: 'For a given doctest, continue to run after the first failure.',
  ]
  private argumentBasetemp = [
    type       : CharSequence,
    default    : null,
    description: 'Base temporary directory for this test run. (Warning: this directory is removed if it exists.).',
  ]
  private argumentVersion = [
    type       : Boolean,
    default    : null,
    description: 'Display pytest version and information about plugins. When given twice, also display information about plugins.',
  ]
  private argumentPlugin = [
    type       : CharSequence,
    default    : null,
    description: 'Early-load given plugin module name or entry point (multi-allowed). ' +
      'To avoid loading of plugins, use the `no:` prefix, e.g. `no:doctest`.',
  ]
  private argumentTraceConfig = [
    type       : Boolean,
    default    : null,
    description: 'Trace considerations of conftest.py files.',
  ]
  private argumentDebug = [
    type       : CharSequence,
    default    : null,
    description: 'Store internal tracing debug information in this log file. ' +
      'This file is opened with \'w\' and truncated as a result, care advised. Default: pytestdebug.log.',
  ]
  private argumentOverride = [
    type       : CharSequence,
    default    : null,
    description: 'Override ini option with "option=value" style, e.g. `-o xfail_strict=True -o cache_dir=cache`.',
  ]
  private argumentAssert = [
    type       : CharSequence,
    default    : null,
    description: 'Control assertion debugging tools. \'plain\' performs no assertion debugging. ' +
      '\'rewrite\' (the default) rewrites assert statements in test modules on import to provide assert expression information.',
  ]
  private argumentSetupOnly = [
    type       : Boolean,
    default    : null,
    description: 'Only setup fixtures, do not execute tests.',
  ]
  private argumentSetupShow = [
    type       : Boolean,
    default    : null,
    description: 'Show setup of fixtures while executing tests.',
  ]
  private argumentSetupPlan = [
    type       : Boolean,
    default    : null,
    description: 'Show what fixtures and tests would be executed but don\'t execute anything.',
  ]
  private argumentLogLevel = [
    type       : CharSequence,
    default    : null,
    description: 'Level of messages to catch/display. Not set by default, ' +
      'so it depends on the root/parent log handler\'s effective level, where it is "WARNING" by default.',
  ]
  private argumentLogFormat = [
    type       : CharSequence,
    default    : null,
    description: 'Log format used by the logging module.',
  ]
  private argumentLogDateFormat = [
    type       : CharSequence,
    default    : null,
    description: 'Log date format used by the logging module.',
  ]
  private argumentLogCliLevel = [
    type       : CharSequence,
    default    : null,
    description: 'CLI logging level.',
  ]
  private argumentLogCliFormat = [
    type       : CharSequence,
    default    : null,
    description: 'Log format used by the logging module.',
  ]
  private argumentLogCliDateFormat = [
    type       : CharSequence,
    default    : null,
    description: 'Log date format used by the logging module.',
  ]
  private argumentLogFile = [
    type       : CharSequence,
    default    : null,
    description: 'Path to a file when logging will be written to',
  ]
  private argumentLogFileLevel = [
    type       : CharSequence,
    default    : null,
    description: 'Log file logging level',
  ]
  private argumentLogFileFormat = [
    type       : CharSequence,
    default    : null,
    description: 'Log format used by the logging module',
  ]
  private argumentLogFileDateFormat = [
    type       : CharSequence,
    default    : null,
    description: 'Log date format used by the logging module',
  ]
  private argumentLogAutoIndent = [
    type       : CharSequence,
    default    : null,
    description: 'Auto-indent multiline messages passed to the logging module. Accepts true|on, false|off or an integer.',
  ]
  /////////////////
  private argumentCov = [
    type       : Boolean,
    default    : null,
    description: 'Display code coverage report.',
  ]
  private argumentCovReset = [
    type       : Boolean,
    default    : null,
    description: 'Reset cov sources accumulated in options so far.',
  ]
  private argumentCovReport =[
    type       : CharSequence,
    default    : null,
    description: 'Type of report to generate: term, term-missing, annotate, html, xml, lcov (multi-allowed). term,' +
      ' term-missing may be followed by ":skip-covered". annotate, html, xml and lcov may be followed by ' +
      '":DEST" where DEST specifies the output location. Use --cov-report= to not generate any.',
  ]
  private argumentCovConfig =[
    type       : CharSequence,
    default    : null,
    description: 'Config file for coverage. Default: .coveragerc.',
  ]
  private argumentNoCov =[
    type       : Boolean,
    default    : null,
    description: 'Disable coverage report completely (useful for debuggers). Default: False.',
  ]
  private argumentCovFailUnder =[
    type       : Integer,
    default    : null,
    description: 'Fail if the total coverage is less than MIN.',
  ]
  private argumentCovAppend =[
    type       : Boolean,
    default    : null,
    description: 'Do not delete coverage but append to current. Default: False.',
  ]
  private argumentCovBranch =[
    type       : Boolean,
    default    : null,
    description: 'Enable branch coverage.',
  ]
  private argumentCovContext =[
    type       : CharSequence,
    default    : null,
    description: 'Dynamic contexts to use. "test" for now.',
  ]

  private argumentCommandTarget = [
    type       : CharSequence,
    default    : '',
    description: 'Command target.',
  ]

  Pytest(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }
}
