package com.wildbeavers.python

import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.data.PythonBuildData


interface CanManagedPythonData<K, V> {
    /**
     * Create a new python data
     * @param config
     * @return The python data
     * @throw Exception when an input option is not supported
     */

  K create(Map<String, Object> config)

    /**
     * Get the python data type
     * @return The supported dependency installer type
     */
  V getType()
}
