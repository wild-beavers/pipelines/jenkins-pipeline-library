package com.wildbeavers.python


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.composite.FetcherInterface
import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.data.HasPythonBuilderData

/**
 * Register and fetch PythonBuilderData by types
 */
class PythonBuildDataManagerFetcher implements FetcherInterface<PythonBuilderType, CanManagedPythonData<HasPythonBuilderData, PythonBuilderType>> {
  private FetcherDeprecated fetcher

  PythonBuildDataManagerFetcher(FetcherDeprecated fetcher) {
    this.fetcher = fetcher
  }

  void register(PythonBuilderType type, CanManagedPythonData value) {
    this.fetcher.register(type, value)
  }

  CanManagedPythonData fetch(PythonBuilderType type) {
    return this.fetcher.fetch(type)
  }
}
