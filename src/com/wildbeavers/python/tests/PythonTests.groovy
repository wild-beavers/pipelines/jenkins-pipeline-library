package com.wildbeavers.python.tests

import com.wildbeavers.data.ScmInfo
import com.wildbeavers.python.data.HasPythonTestsData

/**
 * Interface that describes a python project test
 */
interface PythonTests {
  /**
   * Test a python project.
   * @param hasPythonBuilderData The data object needed to test the project
   */
  void test(HasPythonTestsData hasPythonTestsData)

  /**
   * Whether the class can test the project or not.
   * @param scmInfo The scmInfo object that contains informations about the current repository
   * @return
   */
  Boolean canTest(ScmInfo scmInfo)
}
