package com.wildbeavers.python.tests


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.composite.FetcherInterface
import com.wildbeavers.data.ScmInfo

class PythonTestsFetcher implements FetcherInterface<PythonTestsType, PythonTests> {
  private FetcherDeprecated fetcher

  /**
   * Register and fetch PythonTests
   * @param debugger
   */
  PythonTestsFetcher(FetcherDeprecated fetcher) {
    this.fetcher = fetcher
  }

  /**
   * Fetch a PythonTests class by its type.
   * @param type The class type to fetch
   * @param scmInfo The ScmInfo used to determine if the fetched class is able to test or not
   * @return null if the fetched class can not test the project, the fetched class otherwise
   */
  PythonTests fetch(PythonTestsType type, ScmInfo scmInfo) {
    if (!this.fetch(type).canTest(scmInfo)) {
      return null
    }

    return this.fetch(type)
  }

  void register(PythonTestsType key, PythonTests value) {
    this.fetcher.register(key, value)
  }

  PythonTests fetch(PythonTestsType key) {
    return this.fetcher.fetch(key) as PythonTests
  }
}
