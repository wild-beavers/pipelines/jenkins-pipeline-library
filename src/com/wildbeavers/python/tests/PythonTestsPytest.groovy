package com.wildbeavers.python.tests

import com.wildbeavers.python.command.Pytest
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.python.data.HasPythonTestsData

class PythonTestsPytest implements PythonTests {
  private Pytest pytest

  PythonTestsPytest(Pytest pytest) {
    this.pytest = pytest
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void test(HasPythonTestsData pythonTestsData) {
    if (pythonTestsData.getAppendCommand()) {
      pytest.runWithPrefix('', pythonTestsData.getArguments(), pythonTestsData)
      return
    }

    pytest.run('', pythonTestsData.getArguments(), pythonTestsData)
  }

  /**
   * {@inheritDoc}
   */
  @Override
  Boolean canTest(ScmInfo scmInfo) {
    // This is not yet implemented
    // It can be done this parsing few files like pyproject.toml
    return true
  }
}
