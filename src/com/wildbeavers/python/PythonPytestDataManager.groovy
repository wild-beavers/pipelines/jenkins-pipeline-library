package com.wildbeavers.python

import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.python.data.PythonPytestData
import com.wildbeavers.python.tests.PythonTestsType

class PythonPytestDataManager implements CanManagedPythonData<PythonPytestData, PythonTestsType> {
  private ContainerOptionsManager dockerOptionsManager
  private String pythonDockerImageName

  PythonPytestDataManager(ContainerOptionsManager dockerOptionsManager, pythonDockerImageName = PythonBuildDataManager.PYTHON_DOCKER_IMAGE_NAME) {
    this.dockerOptionsManager = dockerOptionsManager
    this.pythonDockerImageName = pythonDockerImageName
  }

  /**
   * {@inheritDoc}
   */
  PythonPytestData create(Map<String, Object> pythonBuildConfig) {
    PythonPytestData pythonPytestData = new PythonPytestData()

    pythonBuildConfig.each {
      switch (it.key) {
        case "appendCommand":
          pythonPytestData.setAppendCommand(it.value)
          break
        case "arguments":
          pythonPytestData.setArguments(it.value)
          break
        case "dockerOptions":
          pythonPytestData.setContainerOptions(this.dockerOptionsManager.create(it.value))
          break
        case "name":
          pythonPytestData.setName(it.value)
          break
        default:
          throw new Exception("The configuration key “" + it.key + "” is not a supported for python pytest. Supported types: “" + PythonPytestData.SUPPORTED_CONFIGURATION_KEYS + "”")
      }
    }

    if (null == pythonPytestData.getContainerOptions()) {
      pythonPytestData.setContainerOptions(this.dockerOptionsManager.create([:]))
    }

    if (! pythonPytestData.getContainerOptions().hasImage()) {
      pythonPytestData.getContainerOptions().setImage(this.pythonDockerImageName)
    }

    return pythonPytestData
  }

  /**
   * {@inheritDoc}
   */
  PythonTestsType getType() {
    return PythonTestsType.PYTEST
  }
}
