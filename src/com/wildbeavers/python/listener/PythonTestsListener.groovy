package com.wildbeavers.python.listener

import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.python.tests.PythonTestsFetcher

/**
 * Test a python project.
 */
class PythonTestsListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  private PythonTestsFetcher pythonTestsFetcher
  private Debugger debugger
  private ScmInfo scmInfo

  PythonTestsListener(PythonTestsFetcher pythonTestsFetcher, Debugger debugger, ScmInfo scmInfo) {
    this.pythonTestsFetcher = pythonTestsFetcher
    this.debugger = debugger
    this.scmInfo = scmInfo
  }

  /**
   * {@inheritDoc}
   */
  @Override
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as PythonData)
    return eventData
  }

  /**
   * {@inheritDoc}
   */
  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.TEST_UNIT]
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  private void doRun(PythonData pythonData) {
    if (pythonData.getPythonTestsDatas() == null || pythonData.getPythonTestsDatas().size() == 0) {
      debugger.printDebug('No tests configuration found. Skipping.')
      return
    }

    pythonData.getPythonTestsDatas().each {
      pythonTestsFetcher.fetch(it.getType(), this.scmInfo).test(it)
    }
  }
}
