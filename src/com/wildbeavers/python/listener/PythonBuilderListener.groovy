package com.wildbeavers.python.listener

import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.python.builder.PythonBuilderFetcher

/**
 * Build a python project using python module build.
 */
class PythonBuilderListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  private PythonBuilderFetcher pythonBuilderFetcher
  private Debugger debugger
  private ScmInfo scmInfo

  PythonBuilderListener(PythonBuilderFetcher pythonBuilderFetcher, Debugger debugger, ScmInfo scmInfo) {
    this.pythonBuilderFetcher = pythonBuilderFetcher
    this.debugger = debugger
    this.scmInfo = scmInfo
  }

  /**
   * {@inheritDoc}
   */
  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.BUILD]
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  @Override
  RunnerEventData run(RunnerEventData eventData) {
    this.doRun(eventData.getMainEventData() as PythonData)
    return eventData
  }

  private void doRun(PythonData pythonData) {
    if (pythonData.getPythonBuilderType() == null) {
      debugger.printDebug('No builder type found. Skipping.')
      return
    }

    pythonBuilderFetcher.fetch(pythonData.getPythonBuilderType(), this.scmInfo).build(pythonData.getPythonBuilderData())
  }
}
