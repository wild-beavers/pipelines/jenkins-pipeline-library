package com.wildbeavers.python


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.composite.FetcherInterface
import com.wildbeavers.python.data.HasPythonTestsData
import com.wildbeavers.python.tests.PythonTestsType

/**
 * Register and fetch PythonTestsData by types
 */
class PythonTestDataManagerFetcher implements FetcherInterface<PythonTestsType, CanManagedPythonData<HasPythonTestsData, PythonTestsType>> {
  private FetcherDeprecated fetcher

  PythonTestDataManagerFetcher(FetcherDeprecated fetcher) {
    this.fetcher = fetcher
  }

  void register(PythonTestsType type, CanManagedPythonData<HasPythonTestsData, PythonTestsType> value) {
    this.fetcher.register(type, value)
  }

  CanManagedPythonData fetch(PythonTestsType type) {
    return this.fetcher.fetch(type)
  }
}
