package com.wildbeavers.python.dependency_installer

import com.wildbeavers.data.DependencyInstallerData
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.dependency_installer.CanManagedDependencyInstallerData
import com.wildbeavers.dependency_installer.DependencyInstallerType

class VenvDependencyInstallerDataManager implements CanManagedDependencyInstallerData {
  private ContainerOptionsManager dockerOptionsManager

  VenvDependencyInstallerDataManager(ContainerOptionsManager dockerOptionsManager) {
    this.dockerOptionsManager = dockerOptionsManager
  }

  @Override
  DependencyInstallerData create(Map<String, Object> config) {
    VenvDependencyInstallerData venvDependencyInstaller = new VenvDependencyInstallerData()

    config.each {
      // Because of Jenkins CPS, we can do something more dynamic.
      switch (it.key) {
        case "requirementFiles":
          venvDependencyInstaller.setRequirementFiles(it.value)
          break
        case "requirementEnvironments":
          venvDependencyInstaller.setRequirementEnvironments(it.value)
          break
        case "packageNames":
          venvDependencyInstaller.setPackageNames(it.value)
          break
        case "destinationDockerFolderPath":
          venvDependencyInstaller.setDestinationDockerFolderPath(it.value)
          break
        case "sourceLocalFolderPath":
          venvDependencyInstaller.setSourceLocalFolderPath(it.value)
          break
        case "pathEnvironmentVariable":
          venvDependencyInstaller.setPathEnvironmentVariable(it.value)
          break
        case "dockerOptions":
          venvDependencyInstaller.setContainerOptions(this.dockerOptionsManager.create(it.value))
          break
        default:
          throw new Exception("The configuration key “" + it.key + "” is not supported for venv. Supported types: “" + VenvDependencyInstallerData.SUPPORTED_CONFIGURATION_KEYS + "”")
      }
    }

    return venvDependencyInstaller
  }

  @Override
  DependencyInstallerType getType() {
    return DependencyInstallerType.VENV
  }
}
