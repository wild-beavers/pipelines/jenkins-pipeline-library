package com.wildbeavers.python.dependency_installer


import com.wildbeavers.data.DependencyInstallerData
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.dependency_installer.DependencyInstaller
import com.wildbeavers.dependency_installer.DependencyInstallerType
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.io.Debugger

/**
 * Implement a python venv dependency installer
 */
class Venv implements DependencyInstaller {
  final static String PATH_ENVIRONMENT_VARIABLE_NAME = "PATH"
  final static String PYTHON_CONTAINER_FQDN = "docker.io/library"
  final static String PYTHON_CONTAINER_IMAGE_NAME = "python"
  final static String PYTHON_CONTAINER_IMAGE_TAG = "latest"

  Debugger debugger
  ContainerRunner containerRunner
  ExecuteProxy executeProxy

  Venv(Debugger debugger, ContainerRunner containerRunner, ExecuteProxy executeProxy) {
    this.debugger = debugger
    this.containerRunner = containerRunner
    this.executeProxy = executeProxy
  }

  /**
   * {@inheritDoc}
   */
  void install(DependencyInstallerData dependencyInstaller) {
    this.doInstall(dependencyInstaller as VenvDependencyInstallerData)
  }

  /**
   * {@inheritDoc}
   */
  void uninstall(DependencyInstallerData dependencyInstaller) {
    this.doUninstall(dependencyInstaller as VenvDependencyInstallerData)
  }

  /**
   * {@inheritDoc}
   */
  DependencyInstallerType getType() {
    return DependencyInstallerType.VENV
  }

  private void doInstall(VenvDependencyInstallerData venvDependencyInstaller) {
    if (venvDependencyInstaller.isInstalled()) {
      this.debugger.print("The venv is already installed at location “" + venvDependencyInstaller.getSourceLocalFolderPath() + "”")
      return
    }

    if (!venvDependencyInstaller.getContainerOptions().hasImage()) {
      venvDependencyInstaller.getContainerOptions().setImage(PYTHON_CONTAINER_FQDN, PYTHON_CONTAINER_IMAGE_NAME, PYTHON_CONTAINER_IMAGE_TAG)
    }

    venvDependencyInstaller.getContainerOptions().addVolumes([(venvDependencyInstaller.getSourceLocalFolderPath()): venvDependencyInstaller.getDestinationDockerFolderPath()])
    this.containerRunner.run(venvDependencyInstaller.getContainerOptions(), "python -m venv install " + venvDependencyInstaller.getDestinationDockerFolderPath(), true)

    activateVenvInDockerOptions(venvDependencyInstaller.getContainerOptions(), venvDependencyInstaller.getSourceLocalFolderPath(), venvDependencyInstaller.getDestinationDockerFolderPath(), venvDependencyInstaller.getPathEnvironmentVariable())

    venvDependencyInstaller.getRequirementEnvironments().each {
      this.containerRunner.run(venvDependencyInstaller.getContainerOptions(), "python -m pip install -e " + it)
    }

    venvDependencyInstaller.getRequirementFiles().each {
      this.containerRunner.run(venvDependencyInstaller.getContainerOptions(), "python -m pip install -r " + it)
    }

    venvDependencyInstaller.getPackageNames().each {
      this.containerRunner.run(venvDependencyInstaller.getContainerOptions(), "python -m pip install " + it)
    }

    venvDependencyInstaller.setInstalled(true)
  }

  private doUninstall(VenvDependencyInstallerData venvHasDependencyInstaller) {
    if (!venvHasDependencyInstaller.isInstalled()) {
      this.debugger.print("No venv installed. Skipping.")
      return
    }

    // Because CPS doesn't allow us to fully manipulate files, we need to use a shell script
    this.executeProxy.execute("rm -rf " + venvHasDependencyInstaller.getSourceLocalFolderPath())

    venvHasDependencyInstaller.setInstalled(false)
  }

  /**
   * Alter a ContainerOptions object to activate the venv
   * @param dockerOptions The ContainerOptions object to alter
   * @param localFolderPath The path of the venv in the local filesystem
   * @param dockerFolderPath The path of the venv inside the container
   * @param pathEnvironmentVariable The original PATH environment variable
   */
  static void activateVenvInDockerOptions(ContainerOptions dockerOptions, String localFolderPath = VenvDependencyInstallerData.SOURCE_LOCAL_FOLDER_PATH, String dockerFolderPath = VenvDependencyInstallerData.DESTINATION_DOCKER_FOLDER_PATH, String defaultPathEnvironmentVariableValue = "/bin") {
    dockerOptions.addVolumes([(localFolderPath): dockerFolderPath])

    if (dockerOptions.getEnvironmentVariables().get(PATH_ENVIRONMENT_VARIABLE_NAME) != null) {
      dockerOptions.setEnvironmentVariables((PATH_ENVIRONMENT_VARIABLE_NAME): (dockerFolderPath + '/bin:' + dockerOptions.getEnvironmentVariables().get(PATH_ENVIRONMENT_VARIABLE_NAME)))
      return
    }

    dockerOptions.setEnvironmentVariables((PATH_ENVIRONMENT_VARIABLE_NAME): (dockerFolderPath + '/bin:' + defaultPathEnvironmentVariableValue))
  }

  /**
   * Alter a ContainerOptions object to deactivate the venv
   * @param dockerOptions The ContainerOptions object to alter
   * @param localFolderPath The path of the venv in the local filesystem
   */
  static void deactivateVenvInDockerOptions(ContainerOptions dockerOptions, String localFolderPath = VenvDependencyInstallerData.SOURCE_LOCAL_FOLDER_PATH) {
    dockerOptions.removeVolume(localFolderPath)
    dockerOptions.removeEnvironmentVariable(PATH_ENVIRONMENT_VARIABLE_NAME)
  }
}
