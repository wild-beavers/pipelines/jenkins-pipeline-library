package com.wildbeavers.decorator

/**
 * Map decorator
 */
class WildMap<K, V> extends LinkedHashMap<K, V> {
  WildMap(Map map) {
    super(map)
  }

  /**
   * Check the current object for invalid key.
   * @param keyToCheck
   * @param expectedValueType
   * @param defaultValue
   * @param keyUndefinedErrorMessage
   */
  void check(
    String keyToCheck,
    Class<?> expectedValueType,
    defaultValue,
    String keyUndefinedErrorMessage = ''
  ) {

    if ('' == keyToCheck) {
      throw new Exception('Cannot check a Map’s value with an empty key. Make sure the “keyToCheck” argument has a proper value.')
    }

    if (!this.containsKey(keyToCheck) && '' != keyUndefinedErrorMessage) {
      throw new Exception(keyUndefinedErrorMessage)
    }

    if (!(expectedValueType.isInstance(defaultValue)) && defaultValue != null) {
      throw new Exception('Default value not not match the expected type of “' + keyToCheck + '”.')
    }

    if (!(this.containsKey(keyToCheck)) || null == this[keyToCheck]) {
      this[keyToCheck] = defaultValue
    }

    if (!(expectedValueType.isInstance(this[keyToCheck])) && this[keyToCheck] != null) {
      throw new Exception(
        'Error when checking a Map object. The type of the value for key “' + keyToCheck + '” is expected to be “' + expectedValueType + '”, given: “' + this[keyToCheck].getClass().toString() + '”'
      )
    }
  }

  def merge(Map rhs) {
    rhs.inject(this) { map, entry ->
      if (map[entry.key] instanceof Map && entry.value instanceof Map) {
        map[entry.key] = this.withingMerge(map[entry.key], entry.value)
      } else if (map[entry.key] instanceof Collection && entry.value instanceof Collection) {
        map[entry.key] = (map[entry.key] + entry.value).unique()
      } else {
        map[entry.key] = entry.value
      }
      return map
    }
  }

  // This is *mostly* duplicated with merge()
  // For now there is no easy way to de-duplicate this
  private Map withingMerge(Object lhs, Map rhs) {
    return rhs.inject(lhs.clone()) { map, entry ->
      if (map[entry.key] instanceof Map && entry.value instanceof Map) {
        map[entry.key] = this.withingMerge(map[entry.key], entry.value)
      } else if (map[entry.key] instanceof Collection && entry.value instanceof Collection) {
        map[entry.key] = (map[entry.key] + entry.value).unique()
      } else {
        map[entry.key] = entry.value
      }
      return map
    }
  }
}
