package com.wildbeavers.deprecation

import java.time.LocalDate

class DeprecatedMessage {
  /**
   * Displays a warning related to a deprecated function
   * @param oldFunctionName
   * @param newFunctionName
   * @param deprecationDate
   */
  void displayWarningDeprecatedFunction(String oldFunctionName, String newFunctionName, LocalDate deprecationDate) {
    print(
      "\u001b[35m /!\\ DEPRECATION WARNING /!\\\n" +
        "“\033[0;4m\033[0;1m\u001b[35m${oldFunctionName}\u001b[0m\u001b[35m” is now deprecated and will be remove on \033[0;4m\033[0;1m\u001b[35m" + this.formatToEnglishDate(deprecationDate) + "\u001b[0m\u001b[35m, replaced by “\033[0;4m\033[0;1m\u001b[35m${newFunctionName}\u001b[0m\u001b[35m”" +
        "\u001B[0m"
    )
  }

  /**
   * Throws an error related to a deprecated function
   * @param oldFunctionName
   * @param newFunctionName
   * @param deletedDate
   */
  void throwErrorDeletedFunction(String oldFunctionName, String newFunctionName, LocalDate deletedDate) {
    print(
      "\u001b[35m /!\\ DELETION ERROR /!\\\n" +
        "“\033[0;4m\033[0;1m\u001b[35m${oldFunctionName}\u001b[0m\u001b[35m” is deleted since \033[0;4m\033[0;1m\u001b[35m" + this.formatToEnglishDate(deletedDate) + "\u001b[0m\u001b[35m, replaced by “\033[0;4m\033[0;1m\u001b[35m${newFunctionName}\u001b[0m\u001b[35m”" +
        "\u001B[0m"
    )

    throw new Exception("Function “${oldFunctionName}” is deleted since " + this.formatToEnglishDate(deletedDate) + ", replaced by “${newFunctionName}”")
  }

  private String formatToEnglishDate(LocalDate date) {
    return date.format("EEEE, MMMM dd YYYY")
  }
}
