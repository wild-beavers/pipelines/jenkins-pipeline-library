package com.wildbeavers.deprecation

import java.time.LocalDate

class DeprecatedFunction {
  private DeprecatedMessage deprecatedMessage

  DeprecatedFunction(DeprecatedMessage deprecatedMessage) {
    this.deprecatedMessage = deprecatedMessage
  }

  /**
   * Execute a 'legacyClosure' until 'deprecationDate' and display a warning message that the function is deprecated
   * @param legacyClosure
   * @param oldFunctionName Name of the deprecated function for message
   * @param newFunctionName Name of the new function to use for message
   * @param deprecationDateInput The format of the date should be "dd-MM-yyyy"
   * @return Anything the 'legacyClosure' returns
   */
  def execute(Closure legacyClosure, String oldFunctionName, String newFunctionName, String deprecationDateInput) {
    def deprecationDate = LocalDate.parse(deprecationDateInput, 'dd-MM-yyyy')
    def currentDate = LocalDate.now()

    if (deprecationDate < currentDate) {
      return this.deprecatedMessage.throwErrorDeletedFunction(oldFunctionName, newFunctionName, deprecationDate)
    }

    this.deprecatedMessage.displayWarningDeprecatedFunction(oldFunctionName, newFunctionName, deprecationDate)

    return legacyClosure()
  }
}
