package com.wildbeavers.composite

/**
 * Generic fetcher
 * @param <K> The key type
 * @param <V> The registered object type
 */
interface FetcherInterface<K, V> {
  /**
   * Register an object
   * @param key the key of the object to register
   * @param value the object to register
   */
  void register(K key, V value)

  /**
   * Fetch a registered object
   * @param key the key of the object to fetch
   * @return the object if the key is found, null otherwise
   */
  V fetch(K key)
}
