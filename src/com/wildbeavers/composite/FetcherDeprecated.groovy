package com.wildbeavers.composite

import com.wildbeavers.exception.FetcherTypeNotFoundException
import com.wildbeavers.io.Debugger

/**
 * Adds and fetch classes
 */
class FetcherDeprecated {
  protected Map registered = [:]

  protected Debugger debugger

  FetcherDeprecated(Debugger debugger) {
    this.debugger = debugger
  }

  /**
   * Register a class to the fetcher
   * @param type The class type do register
   * @param value The class to register
   */
  void register(Object type, Object value) {
    if (this.contains(type)) {
      this.debugger.printDebug("Warning: the type “" + type + "” already exist (Class: “" + registered.get(type).getClass() + "”). It will be overridden.")
    }

    this.registered.put(type, value)
  }

  /**
   * Fetch a class
   * @param type The class type to fetch
   * @return The fetched class
   * @throw FetcherTypeNotFoundException when the class type is not found
   */
  Object fetch(Object type) {
    if (!this.contains(type)) {
      throw new FetcherTypeNotFoundException("The type “" + type + "“ was not found or is null.")
    }

    return this.registered.get(type)
  }

  /**
   * Whether a type is added or not
   * @param type The type to check
   * @return true if the type is added, false otherwise
   */
  private Boolean contains(Object type) {
    return this.registered.get(type) != null
  }
}
