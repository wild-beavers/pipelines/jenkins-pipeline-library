package com.wildbeavers.composite

import com.wildbeavers.exception.FetcherTypeNotFoundException

/**
 * Generic Fetcher. Abstract chain of responsibility.
 * @param <Type> Type of Fetchable objects this Fetcher support
 * @param <Subject> Type of subject (discriminant) the <V> fetchable support
 * @param <Iid> Type of identifier of the <V> Fetchable support
 */
class Fetcher<Type extends Fetchable, Subject, Id extends Comparable<Id>> {
  protected LinkedHashSet<Type> objects = []

  /**
   * Registers a Fetchable object.
   * @param Fetchable <Subject, Id> object to register
   */
  void register(Type object) {
    this.objects.add(object)
  }

  /**
   * Fetches all registered Fetchable supporting the given $subject.
   * Opportunistic find: this method will not complain if no Fetchable is found.
   * @param <Subject> subject
   * @param <Id> only fetch Fetchable matching this id
   */
  LinkedHashSet<Type> fetchTrySupporting(Subject subject, Id id = null) {
    return this.objects.findAll({
      it.support(subject) && (id == null || it.getId() == id)
    })
  }

  /**
   * Fetches all registered Fetchable supporting the given $subject.
   * @param <Subject> subject
   * @param <Id> only fetch Fetchable matching this id
   * @throws FetcherTypeNotFoundException when no objects supports $subject
   */
  LinkedHashSet<Type> fetchSupporting(Subject subject, Id id = null) {
    LinkedHashSet<Type> results = this.fetchTrySupporting(subject, id)

    if (results.isEmpty()) {
      throw new FetcherTypeNotFoundException("“${subject.toString()}” was provided to fetch a supported ${Type}, but no registered objects support it." +
        " Verify that at least one registered objects (${this.fetchAll().toString()}) support the full “${subject.toString()}” subject." +
        " If a failure to support this subject is an intended behavior, use the “fetchTrySupporting” method instead.")
    }

    return results
  }

  /**
   * Fetches all the registered Fetchable, in order of registration
   */
  LinkedHashSet<Type> fetchAll() {
    return this.objects
  }

  /**
   * Fetches the first registered Fetchable supporting the given $subject.
   * @param <Subject> subject
   * @param <Id> only fetch Fetchable matching this id
   * @throws FetcherTypeNotFoundException when no objects supports $subject
   */
  Type fetchFirstSupporting(Subject subject, Id id = null) {
    return this.fetchSupporting(subject, id)[0]
  }

  @Override
  String toString() {
    return this.objects.collect({ it.toString() }).join(", ")
  }
}
