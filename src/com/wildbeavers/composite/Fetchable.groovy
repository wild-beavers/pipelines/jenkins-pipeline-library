package com.wildbeavers.composite

/**
 * Defines a Fetchable, compatible with Fetcher
 * @param <Subject> Type of object that can be tested on to determine what this fetchable supports
 * @param <Id> Type of object defining what this Fetchable is. Usually a Class or a CharSequence.
 */
interface Fetchable<Subject, Id> {
  /**
   * Whether this Fetchable supports $subject
   * @return Boolean
   */
  Boolean support(Subject subject)

  /**
   * @return The identifier of this Fetchable
   */
  Id getId()
}
