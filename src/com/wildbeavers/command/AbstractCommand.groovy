package com.wildbeavers.command

import com.wildbeavers.data.CommandResult
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.exception.CommandBuildingException
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Abstract class to allow to run system commands. Check arguments, build commands an run them.
 */
abstract class AbstractCommand {
  public static final COMMAND_TARGET = 'commandTarget'

  protected OptionStringFactory optionStringFactory
  protected ContainerRunner containerRunner
  protected Debugger debugger

  protected abstract String getCommand()

  protected abstract String getOptionStringDelimiter()

  protected abstract Map<String, Map> getValidArguments()

  protected abstract Map<String, Map<String, Map<String, Object>>> getValidSubArguments()

  AbstractCommand(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    this.optionStringFactory = optionStringFactory
    this.containerRunner = containerRunner
    this.debugger = debugger
  }

  /**
   * Runs the sub-class command with ${sub-command} and ${arguments} within a container image if ${containerOptions} has an image defined. Prefix with the command if enabled.
   * @param subcommand
   * @param arguments
   * @param containerOptions
   * @param prefixWithCommand
   * @return CommandResult
   */
  protected CommandResult doRun(String subcommand = '', Map<String, Object> arguments = [:], HasContainerOptions containerOptions, Boolean prefixWithCommand) {
    def fullCommand = (this.getMainCommandArguments(subcommand, arguments) + ' ' + this.getSubCommandLine(subcommand, arguments)).trim()

    if (prefixWithCommand == true) {
      fullCommand = this.getCommand().trim() + ' ' + fullCommand
    }

    try {
      return this.containerRunner.run(containerOptions.getContainerOptions(), fullCommand)
    } catch (Exception cmdError) {
      throw new Exception(
        "ERROR: ${command} ${subcommand} command has failed. \n ${cmdError.getMessage()}"
      )
    }
  }

  /**
   * Runs the sub-class command with ${sub-command} and ${arguments} within a container image if ${containerOptions} has an image defined.
   * @param subcommand
   * @param arguments
   * @param containerOptions
   * @return CommandResult
   */
  CommandResult run(String subcommand = '', Map<String, Object> arguments = [:], HasContainerOptions containerOptions) {
    return this.doRun(subcommand, arguments, containerOptions, false)
  }

  /**
   * Runs the sub-class command with ${sub-command} and ${arguments} within a container image if ${containerOptions} has an image defined. Prefix with the command.
   * @param subcommand
   * @param arguments
   * @param containerOptions
   * @return CommandResult
   */
  CommandResult runWithPrefix(String subcommand = '', Map<String, Object> arguments = [:], HasContainerOptions containerOptions) {
    return this.doRun(subcommand, arguments, containerOptions, true)
  }

  String getDocumentation() {
    return """
    COMMAND: ${this.getCommand()}

    ${this.extractDocumentation(this.getValidArguments())}
"""
  }

  String getDocumentation(String subcommand) {
    return """
    COMMAND: ${this.getCommand()} ${subcommand}

    ${this.extractDocumentation(this.getValidSubArguments()[subcommand])}
"""
  }

  protected String extractDocumentation(Map map) {
    return map.collect(
      {
        sub ->
          "${sub.key}:\n" + sub.value.collect(
            {
              arg ->
                "    ${arg.key}: ${arg.value}"
            }
          ).join("\n")
      }
    ).join("\n\n")
  }

  protected String getMainCommandArguments(String subcommand, Map<String, Object> arguments) {
    this.optionStringFactory.createOptionString(this.getOptionStringDelimiter())

    for (argument in arguments) {
      if (!this.isMainArg(argument.key) && '' != subcommand) {
        continue
      }

      this.checkArgument(argument.key, argument.value)

      if (this.argumentIsCommand(argument.key)) {
        this.optionStringFactory.addCommandTarget(argument.value as String)
        continue
      }

      if (this.isBooleanArgWithNoValue(argument.key, argument.value)) {
        this.optionStringFactory.addOption(argument.key, '')
        continue
      }

      this.optionStringFactory.addOption(argument.key, argument.value, this.getValidArguments()[argument.key]['type'])
    }

    return this.optionStringFactory.getOptionString().toString()
  }

  protected Boolean argumentIsCommand(String argumentKey) {
    return COMMAND_TARGET == argumentKey
  }

  protected Boolean isBooleanArgWithNoValue(String argumentKey, argumentValue) {
    return (
      Boolean == this.getValidArguments()[argumentKey]['type'] &&
        null == this.getValidArguments()[argumentKey]['default'] &&
        true == argumentValue
    )
  }

  /**
   * Gets the prepared, read-to-use command line.
   * @param subcommand
   * @param arguments
   * @param optionStringDelimiter
   */
  protected String getSubCommandLine(String subcommand, Map<String, Object> arguments) {
    if ('' == subcommand) {
      return ''
    }

    this.optionStringFactory.createOptionString(this.getOptionStringDelimiter())

    for (argument in arguments) {
      if (this.isMainArg(argument.key)) {
        continue
      }

      this.checkArgument(subcommand, argument.key, argument.value)

      if (this.argumentIsCommand(argument.key)) {
        this.optionStringFactory.addCommandTarget(argument.value as String)
        continue
      }

      if (this.isBooleanArgWithNoValue(subcommand, argument.key, argument.value)) {
        this.optionStringFactory.addOption(argument.key, '')
        continue
      }

      this.optionStringFactory.addOption(argument.key, argument.value, this.getValidSubArguments()[subcommand][argument.key]['type'])
    }

    return "${subcommand} ${this.optionStringFactory.getOptionString().toString()}"
  }

  protected Boolean isBooleanArgWithNoValue(String subcommand, String argumentKey, argumentValue) {
    return (
      Boolean == this.getValidSubArguments()[subcommand][argumentKey]['type'] &&
        null == this.getValidSubArguments()[subcommand][argumentKey]['default'] &&
        true == argumentValue
    )
  }

  protected Boolean isMainArg(String argumentKey) {
    return this.getValidArguments().containsKey(argumentKey)
  }

  protected void checkArgument(String argumentKey, argumentValue) {
    if (!this.getValidArguments().containsKey(argumentKey)) {
      throw new CommandBuildingException(
        """
      Argument “${argumentKey}” is not valid for command “${this.getCommand()}”
      Valid arguments are: ${this.getValidArguments().keySet().join(', ')}.
      It is possible that the argument exists, but it is yet to be implemented. Please see ${this.class.name}.
        """
      )
    }

    if (
    this.getValidArguments()[argumentKey].containsKey('validValues') &&
      !this.getValidArguments()[argumentKey]['validValues'].contains(argumentValue)
    ) {
      throw new CommandBuildingException(
        """
      Argument “${argumentKey}” does not have a valid value (given: “${argumentValue}”) for command “${this.getCommand()
        }”
      Valid values for this argument are: ${this.getValidArguments()[argumentKey]['validValues'].join(', ')}.
      It is possible that the value normally exists, but it is yet to be implemented. Please see ${this.class.name}.
        """
      )
    }
  }

  protected void checkArgument(String subcommand, String argumentKey, argumentValue) {
    this.checkSubCommand(subcommand)

    if (!this.getValidSubArguments()[subcommand].containsKey(argumentKey)) {
      throw new CommandBuildingException(
        """
      Argument “${argumentKey}” is not valid for sub command “${subcommand}” in command “${this.getCommand()}”.
      Valid arguments for this subcommand are: ${this.getValidSubArguments()[subcommand].keySet().join(', ')}.
      It is possible that the argument exists, but it is yet to be implemented. Please see ${this.class.name}.
        """
      )
    }

    if (this.getValidSubArguments()[subcommand][argumentKey].containsKey('validValues') &&
      !this.getValidSubArguments()[subcommand][argumentKey]['validValues'].contains(argumentValue)
    ) {
      throw new CommandBuildingException(
        """
      Argument “${argumentKey}” does not have a valid value (given: “${argumentValue}”) for sub command “${subcommand
        }” in command “${this.getCommand()}”.
      Valid values for this argument are: ${
          this.getValidSubArguments()[subcommand][argumentKey]['validValues'].join(', ')}.
      It is possible that the value normally exists, but it is yet to be implemented. Please see ${this.class.name}.
        """
      )
    }
  }

  protected void checkSubCommand(CharSequence subcommand) {
    if (!this.getValidSubArguments().containsKey(subcommand)) {
      throw new CommandBuildingException(
        """
          Subcommand “${subcommand}” is not valid for command “${this.getCommand()}”
          Valid sub commands are: ${this.getValidSubArguments().keySet().join(', ')}.
          It is possible that the subcommand exists, but it is yet to be implemented. Please see ${this.class.name}.
        """
      )
    }
  }
}
