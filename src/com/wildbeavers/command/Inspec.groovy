package com.wildbeavers.command

import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run inspec commands.
 */
class Inspec extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'inspec'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return '='
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [:]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    return [
      'exec'   : [
        (COMMAND_TARGET)    : this.argumentCommand,
        '--backend'         : this.argumentBackend,
        '-b'                : this.argumentBackend,
        '--backend-cache'   : this.argumentNoBackendCache,
        '--no-backend-cache': this.argumentNoBackendCache,
        '--bastion-host'    : this.argumentBastionHost,
        '--bastion-port'    : this.argumentBastionPort,
        '--bastion-user'    : this.argumentBastionUser,
        '--command-timeout' : this.argumentCommandTimeout,
        '--config'          : this.argumentConfig,
        '--controls'        : this.argumentControls,
        '--create-lockfile'             : this.argumentCreateLockfile,
        '--no-create-lockfile'          : this.argumentCreateLockfile,
        '--no-distinct-exit'            : this.argumentNoDistinctExit,
        '--docker-url'                  : this.argumentDockerUrl,
        '--filter-empty-profiles'       : this.argumentFilterEmptyProfiles,
        '--no-filter-empty-profiles'    : this.argumentHost,
        '--host'                        : this.argumentHost,
        '--input'                       : this.argumentInput,
        '--input-file'                  : this.argumentInputFile,
        '--insecure'                    : this.argumentInsecure,
        '--no-insecure'                 : this.argumentInsecure,
        '--key-files'                   : this.argumentKeyFiles,
        '-i'                            : this.argumentKeyFiles,
        '--password'                    : this.argumentPassword,
        '--port'                        : this.argumentPort,
        '-p'                            : this.argumentPort,
        '--profiles-path'               : this.argumentProfilePath,
        '--proxy-command'               : this.argumentProxyCommand,
        '--reporter'                    : this.argumentReporter,
        '--reporter-include-source'     : this.argumentReporterIncludeSource,
        '--shell'                       : this.argumentShell,
        '--no-shell'                    : this.argumentShell,
        '--shell-command'               : this.argumentShellCommand,
        '--shell-options'               : this.argumentShellOptions,
        '--show-progress'               : this.argumentShowProgress,
        '--no-show-progress'            : this.argumentShowProgress,
        '--silence-deprecations'        : this.argumentSilenceDeprecations,
        '--sudo'                        : this.argumentSudo,
        '--no-sudo'                     : this.argumentSudo,
        '--sudo-options'                : this.argumentSudoOptions,
        '--sudo-password'               : this.argumentSudoPassword,
        '--target'                      : this.argumentTarget,
        '-t'                            : this.argumentTarget,
        '--user'                        : this.argumentUser,
        '--vendor-cache'                : this.argumentVendorCache,
      ],
      'detect' : [
        '--backend'      : this.argumentBackend,
        '-b'             : this.argumentBackend,
        '--bastion-host' : this.argumentBastionHost,
        '--bastion-port' : this.argumentBastionPort,
        '--bastion-user' : this.argumentBastionUser,
        '--config'       : this.argumentConfig,
        '--docker-url'   : this.argumentDockerUrl,
        '--format'       : this.argumentFormat,
        '--host'         : this.argumentHost,
        '--insecure'     : this.argumentInsecure,
        '--no-insecure'  : this.argumentInsecure,
        '--key-files'    : this.argumentKeyFiles,
        '-i'             : this.argumentKeyFiles,
        '--password'     : this.argumentPassword,
        '--port'         : this.argumentPort,
        '-p'             : this.argumentPort,
        '--proxy-command': this.argumentProxyCommand,
        '--shell'        : this.argumentShell,
        '--no-shell'     : this.argumentShell,
        '--shell-command': this.argumentShellCommand,
        '--shell-options': this.argumentShellOptions,
        '--sudo'         : this.argumentSudo,
        '--no-sudo'      : this.argumentSudo,
        '--sudo-options' : this.argumentSudoOptions,
        '--sudo-password': this.argumentSudoPassword,
        '--target'       : this.argumentTarget,
        '-t'             : this.argumentTarget,
        '--user'         : this.argumentUser,
      ],
      'check'  : [
        (AbstractCommand.COMMAND_TARGET): this.argumentCommand,
        '--format'                      : this.argumentFormat,
        '--profiles-path'               : this.argumentProfilePath,
        '--vendor-cache'                : this.argumentVendorCache,
      ],
      'schema' : [],
      'env'    : [],
      'vendor' : [],
      'version': [
        '--format': this.argumentFormat,
      ]
    ]
  }

  private argumentBackend = [
    type       : CharSequence,
    validValues: ['local', 'ssh', 'winrm', 'docker'],
    default    : '',
    description: 'Choose a backend: local, ssh, winrm, docker.',
  ]
  private argumentNoBackendCache = [
    type       : Boolean,
    default    : null,
    description: 'Allow/Disallow caching for backend command output. default: allow',
  ]
  private argumentBastionHost = [
    type       : Boolean,
    default    : null,
    description: ' Specifies the bastion host if applicable',
  ]
  private argumentBastionPort = [
    type       : Boolean,
    default    : null,
    description: 'Specifies the bastion port if applicable',
  ]
  private argumentBastionUser = [
    type       : Boolean,
    default    : null,
    description: 'Specifies the bastion user if applicable',
  ]
  private argumentCommandTimeout = [
    type       : Integer,
    default    : null,
    description: 'Maximum seconds to allow a command to run.',
  ]
  private argumentConfig = [
    type       : CharSequence,
    default    : '',
    description: 'Read configuration from JSON file (- reads from stdin).',
  ]
  private argumentControls = [
    type       : CharSequence,
    default    : '',
    description: 'A list of control names to run, or a list of /regexes/ to match against control names. Ignore all other tests.',
  ]
  private argumentCreateLockfile = [
    type       : Boolean,
    default    : null,
    description: 'Write out a lockfile based on this execution (unless one already exists)',
  ]
  private argumentNoDistinctExit = [
    type       : Boolean,
    default    : null,
    description: 'Exit with code 101 if any tests fail, and 100 if any are skipped (default). If disabled, exit 0 on skips and 1 for failures.',
  ]
  private argumentDockerUrl = [
    type       : CharSequence,
    default    : '',
    description: 'Provides path to Docker API endpoint (Docker). Defaults to unix:///var/run/docker.sock on Unix systems and tcp://localhost:2375 on Windows.',
  ]
  private argumentFilterEmptyProfiles = [
    type       : Boolean,
    default    : null,
    description: 'Filter empty profiles (profiles without controls) from the report.',
  ]
  private argumentHost = [
    type       : CharSequence,
    default    : '',
    description: 'Specify a remote host which is tested.',
  ]
  private argumentInput = [
    type       : CharSequence,
    default    : '',
    description: 'Specify one or more inputs directly on the command line, as –input NAME=VALUE. Accepts single-quoted YAML and JSON structures.',
  ]
  private argumentInputFile = [
    type       : CharSequence,
    default    : '',
    description: 'Load one or more input files, a YAML file with values for the profile to use',
  ]
  private argumentInsecure = [
    type       : Boolean,
    default    : null,
    description: 'Disable/Enable SSL verification on select targets',
  ]
  private argumentKeyFiles = [
    type       : CharSequence,
    default    : '',
    description: 'Login key or certificate file for a remote scan.',
  ]
  private argumentPassword = [
    type       : CharSequence,
    default    : '',
    description: 'Login password for a remote scan, if required.',
  ]
  private argumentPort = [
    type       : Integer,
    default    : '',
    description: 'Specify the login port for a remote scan.',
  ]
  private argumentProfilePath = [
    type       : CharSequence,
    default    : '',
    description: 'Folder which contains referenced profiles.',
  ]
  private argumentProxyCommand = [
    type       : CharSequence,
    default    : '',
    description: 'Specifies the command to use to connect to the server',
  ]
  private argumentReporter = [
    type       : CharSequence,
    default    : '',
    description: 'Enable one or more output reporters: cli, documentation, html, progress, json, json-min, json-rspec, junit, yaml',
  ]
  private argumentReporterIncludeSource = [
    type       : CharSequence,
    default    : '',
    description: 'Include full source code of controls in the CLI report',
  ]
  private argumentShowProgress = [
    type       : Boolean,
    default    : null,
    description: 'Show progress while executing tests.',
  ]
  private argumentShell = [
    type       : Boolean,
    default    : null,
    description: 'Run scans in a subshell. Only activates on Unix.',
  ]
  private argumentShellCommand = [
    type       : CharSequence,
    default    : '',
    description: 'Specify a particular shell to use.',
  ]
  private argumentShellOptions = [
    type       : CharSequence,
    default    : '',
    description: 'Additional shell options.',
  ]
  private argumentSilenceDeprecations = [
    type       : CharSequence,
    default    : '',
    description: 'Suppress deprecation warnings. See install_dir/etc/deprecations.json for list of GROUPs or use ‘all’.',
  ]
  private argumentSudo = [
    type       : Boolean,
    default    : null,
    description: 'Run or not scans with sudo. Only activates on Unix and non-root user.',
  ]
  private argumentSudoOptions = [
    type       : CharSequence,
    default    : '',
    description: 'Additional sudo options for a remote scan.',
  ]
  private argumentSudoPassword = [
    type       : CharSequence,
    default    : '',
    description: 'Specify a sudo password, if it is required.',
  ]
  private argumentTarget = [
    type       : CharSequence,
    default    : '',
    description: 'Simple targeting option using URIs, e.g. ssh://user:pass@host:port',
  ]
  private argumentUser = [
    type       : CharSequence,
    default    : '',
    description: 'The login user for a remote scan.',
  ]
  private argumentVendorCache = [
    type       : CharSequence,
    default    : '',
    description: 'Use the given path for caching dependencies. (default: ~/.inspec/cache)',
  ]
  private argumentFormat = [
    type       : CharSequence,
    default    : '',
    description: '',
  ]

  private argumentCommand = [
    type       : CharSequence,
    default    : '',
    description: 'Locations',
  ]

  Inspec(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }
}
