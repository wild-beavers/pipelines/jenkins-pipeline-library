package com.wildbeavers.command

import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run gradle commands.
 */
class Gradle extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'gradle'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return '='
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [
      '-h'                       : this.argumentHelp,
      '--help'                   : this.argumentHelp,
      '-v'                       : this.argumentVersion,
      '-version'                 : this.argumentVersion,
      '-s'                       : this.argumentStacktrace,
      '--stacktrace'             : this.argumentStacktrace,
      '--scan'                   : this.argumentScan,
      '-Dorg.gradle.debug'       : this.argumentGradleDebug,
      '-Dorg.gradle.daemon.debug': this.argumentGradleDaemonDebug,
      '--build-cache'            : this.argumentBuildCache,
      '--no-build-cache'         : this.argumentBuildCache,
      '--max-workers'            : this.argumentMaxWorkers,
      '--parallel'               : this.argumentParallel,
      '--no-parallel'            : this.argumentParallel,
      '--priority'               : this.argumentPriority,
      '--daemon'                 : this.argumentDaemon,
      '--no-daemon'              : this.argumentDaemon,
      '--foreground'             : this.argumentForeground,
      '-q'                       : this.argumentQuiet,
      '--quiet'                  : this.argumentQuiet,
      '-w'                       : this.argumentWarn,
      '--warn'                   : this.argumentWarn,
      '-i'                       : this.argumentInfo,
      '--info'                   : this.argumentInfo,
      '-d'                       : this.argumentDebug,
      '--debug'                  : this.argumentDebug,
      '--console'                : this.argumentConsole,
      '--warning-mode'           : this.argumentWarningMode,
      '--offline'                : this.argumentOffline,
      '--refresh-dependencies'   : this.argumentRefreshDependencies,
      '--dry-run'                : this.argumentDryRun,
      '--write-locks'            : this.argumentWriteLocks,
    ]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    [
      'build'           : [:],
      'run'             : [:],
      'check'           : [:],
      'clean'           : [:],
      'tasks'           : [:],
      'projects'        : [:],
      'help'            : [:],
      'dependencies'    : [:],
      'buildEnvironment': [:]
    ]
  }

  private argumentBuildCache = [
    type       : Boolean,
    default    : null,
    description: 'Toggles the Gradle build cache. Gradle will try to reuse outputs from previous builds. Default is off.',
  ]
  private argumentConsole = [
    type       : CharSequence,
    default    : '',
    validValues: ['auto', 'plain', 'rich', 'verbose'],
    description: """
    Specifies which type of console output to generate.

    Set to plain to generate plain text only. This option disables all color and other rich output in the console output. This is the default when Gradle is not attached to a terminal.

    Set to auto (the default) to enable color and other rich output in the console output when the build process is attached to a console, or to generate plain text only when not attached to a console. This is the default when Gradle is attached to a terminal.

    Set to rich to enable color and other rich output in the console output, regardless of whether the build process is not attached to a console. When not attached to a console, the build output will use ANSI control characters to generate the rich output.

    Set to verbose to enable color and other rich output like the rich, but output task names and outcomes at the lifecycle log level, as is done by default in Gradle 3.5 and earlier.
"""
  ]
  private argumentDaemon = [
    type       : Boolean,
    default    : null,
    description: 'Use the Gradle Daemon to run the build. Starts the daemon if not running or existing daemon busy. Default is on.',
  ]
  private argumentDebug = [
    type       : Boolean,
    default    : null,
    description: 'Log in debug mode (includes normal stacktrace).',
  ]
  private argumentDryRun = [
    type       : Boolean,
    default    : null,
    description: 'Run Gradle with all task actions disabled. Use this to show which task would have executed.',
  ]
  private argumentForeground = [
    type       : Boolean,
    default    : null,
    description: 'Starts the Gradle Daemon in a foreground process.',
  ]
  private argumentGradleDebug = [
    type       : Boolean,
    default    : true,
    description: 'Debug Gradle client (non-Daemon) process. Gradle will wait for you to attach a debugger at localhost:5005 by default.',
  ]
  private argumentGradleDaemonDebug = [
    type       : Boolean,
    default    : true,
    description: 'Debug Gradle Daemon process.',
  ]
  private argumentHelp = [
    type       : Boolean,
    default    : null,
    description: 'Shows a help message with all available CLI options.',
  ]
  private argumentInfo = [
    type       : Boolean,
    default    : null,
    description: 'Set log level to info.',
  ]
  private argumentMaxWorkers = [
    type       : Integer,
    default    : null,
    description: 'Sets maximum number of workers that Gradle may use. Default is number of processors.',
  ]
  private argumentOffline = [
    type       : Boolean,
    default    : null,
    description: 'Specifies that the build should operate without accessing network resources. Learn more about options to override dependency caching.',
  ]
  private argumentParallel = [
    type       : Boolean,
    default    : null,
    description: 'Build projects in parallel. For limitations of this option, see Parallel Project Execution. Default is off.',
  ]
  private argumentPriority = [
    type       : CharSequence,
    default    : '',
    validValues: ['normal', 'low'],
    description: 'Specifies the scheduling priority for the Gradle daemon and all processes launched by it. Values are normal or low. Default is normal.',
  ]
  private argumentQuiet = [
    type       : Boolean,
    default    : null,
    description: 'Log errors only.',
  ]
  private argumentRefreshDependencies = [
    type       : Boolean,
    default    : null,
    description: 'Refresh the state of dependencies. Learn more about how to use this in the dependency management docs.',
  ]
  private argumentStacktrace = [
    type       : Boolean,
    default    : null,
    description: 'Print out the stacktrace also for user exceptions (e.g. compile error).',
  ]
  private argumentScan = [
    type       : Boolean,
    default    : null,
    description: 'Create a build scan with fine-grained information about all aspects of your Gradle build.',
  ]
  private argumentWriteLocks = [
    type       : Boolean,
    default    : null,
    description: 'Indicates that all resolved configurations that are lockable should have their lock state persisted. Learn more about this in dependency locking.',
  ]
  private argumentVersion = [
    type       : Boolean,
    default    : null,
    description: 'Prints Gradle, Groovy, Ant, JVM, and operating system version information.',
  ]
  private argumentWarn = [
    type       : Boolean,
    default    : null,
    description: 'Set log level to warn.',
  ]
  private argumentWarningMode = [
    type       : CharSequence,
    default    : '',
    validValues: ['summary', 'all', 'fail', 'none'],
    description: """
    Specifies how to log warnings. Default is summary.

    Set to all to log all warnings.

    Set to fail to log all warnings and fail the build if there are any warnings.

    Set to summary to suppress all warnings and log a summary at the end of the build.

    Set to none to suppress all warnings, including the summary at the end of the build.
""",
  ]

  Gradle(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }
}
