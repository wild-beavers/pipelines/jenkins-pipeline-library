package com.wildbeavers.exception

class OptionStringException extends Exception {
  OptionStringException(String message) {
    super(message)
  }
}
