package com.wildbeavers.exception

class FetcherTypeNotFoundException extends Exception {
  FetcherTypeNotFoundException(String message) {
    super(message)
  }
}
