package com.wildbeavers.exception

class MandatoryFileException extends Exception {
  MandatoryFileException(String message) {
    super(message)
  }
}
