package com.wildbeavers.exception

class JenkinsCredentialTransformerException extends Exception {
  JenkinsCredentialTransformerException(String message) {
    super(message)
  }
}
