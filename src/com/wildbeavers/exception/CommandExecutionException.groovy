package com.wildbeavers.exception

class CommandExecutionException extends Exception {
  CommandExecutionException(String message) {
    super(message)
  }
}
