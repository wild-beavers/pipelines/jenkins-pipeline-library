package com.wildbeavers.exception

class CommandBuildingException extends Exception {
  CommandBuildingException(String message) {
    super(message)
  }
}
