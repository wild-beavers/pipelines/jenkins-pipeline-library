package com.wildbeavers.exception

class EnvironmentVariableProxyException extends Exception {
  EnvironmentVariableProxyException(String message) {
    super(message)
  }
}
