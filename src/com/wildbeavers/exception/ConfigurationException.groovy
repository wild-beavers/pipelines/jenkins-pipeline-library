package com.wildbeavers.exception

class ConfigurationException extends Exception {
  ConfigurationException(String message) {
    super(message)
  }
}
