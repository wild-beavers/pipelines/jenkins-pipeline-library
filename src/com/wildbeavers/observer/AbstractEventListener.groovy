package com.wildbeavers.observer

abstract class AbstractEventListener<T extends EventDataInterface> {
  protected Boolean stopPropagation = false

  /**
   * Runs the event.
   * @param EventDataInterface eventData
   * @return EventDataInterface
   */
  abstract T run(T eventData = null)
  /**
   * Whether this listener should run given the current context and event data
   * @param EventDataInterface eventData
   * @return Boolean
   */
  abstract Boolean shouldRun(T eventData = null)

  /**
   * Returns explanations on why the event listener was not run given the current context.
   * All explanations will be printed on stdout.
   * @param EventDataInterface eventData
   * @return ArrayList<CharSequence>
   */
  abstract ArrayList<CharSequence> explainNoRun(T eventData = null)

  /**
   * Brief description of what the listener does
   * @return CharSequence
   */
  abstract CharSequence getDescription()

  /**
   * Returns the list of event names this listener listens to.
   * @return List<String>
   */
  abstract ArrayList<String> listenTo()

  /**
   * Checks whether this listener supports the kind of event passed as argument.
   * @param String event
   * @return Boolean
   */
  abstract Boolean supports(String event)

  /**
   * Instructs whether or not this event must be the last one of the series.
   * @return Boolean
   */
  Boolean stopPropagationAfterRun() {
    return this.stopPropagation
  }

  /**
   * Returns the order in which this event must be triggered.
   * @return Integer
   */
  Integer getOrder() {
    return 1000
  }
}
