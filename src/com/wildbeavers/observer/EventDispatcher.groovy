package com.wildbeavers.observer

import com.wildbeavers.di.IOC
import com.wildbeavers.io.Debugger
import com.wildbeavers.listener.wrapper.CanWrapListener

class EventDispatcher<T extends EventDataInterface> {
  private EventListenerBag eventListenerBag
  private List<CanWrapListener> eventListenerWrappers = []
  private Debugger debugger

  EventDispatcher(EventListenerBag eventListenerBag, Debugger debugger) {
    this.eventListenerBag = eventListenerBag
    this.debugger = debugger
  }

  /**
   * Dispatches an event
   * @param String eventName
   */
  public <T> T dispatch(String eventName, T eventData = null, Boolean exceptionMustStopPropagation = true) {
    RuntimeException runtimeException = new RuntimeException("Exception(s) occured during event “${eventName}”.")

    for (eventListener in this.eventListenerBag.findOrderedListenersForEvent(eventName)) {
      if (!eventListener.shouldRun(eventData)) {
        for (explanation in eventListener.explainNoRun(eventData)) {
          this.debugger.print(explanation)
        }
        continue
      }

      this.debugger.print("\u001b[0;36m~~~> Running\t“\033[0;4m\033[0;1m" + eventListener.getClass().getName() + "\u001b[0m\u001b[0;36m”\u001b[0m")

      CanWrapListener eventListenerWrapper = this.findMatchingWrapper(eventListener)

      if (eventListener.getDescription()) {
        this.debugger.print(eventListener.getDescription())
      }

      T runDataCopy = eventData

      try {
        if (null != eventListenerWrapper) {
          this.debugger.print("\u001b[0;36m~~~> Wrapped by\t“\033[0;4m\033[0;1m" + eventListenerWrapper.getClass().getName() + "\u001b[0m\u001b[0;36m”\u001b[0m")
          runDataCopy = eventListenerWrapper.wrap(eventListener, eventData) as T
        } else {
          runDataCopy = eventListener.run(eventData) as T
        }

        this.debugger.print("\u001b[0;36m~~~> End of \t“\033[0;4m\033[0;1m" + eventListener.class.getName() + "\u001b[0m\u001b[0;36m”\u001b[0m\n")
      } catch (Exception exception) {
        this.debugger.print("\u001b[0;31mxxx> Failed \t“\033[0;4m\033[0;1m" + eventListener.class.getName() + "\u001b[0m\u001b[0;31m”\u001b[0m\n")

        runtimeException.addSuppressed(exception)
      }

      if (exceptionMustStopPropagation && !runtimeException.getSuppressed().toList().isEmpty()) {
        throw runtimeException
      }

      if (null != runDataCopy) {
        eventData = runDataCopy
        runDataCopy = null
      }

      if (eventListener.stopPropagationAfterRun()) {
        break
      }
    }

    if (!runtimeException.getSuppressed().toList().isEmpty()) {
      throw runtimeException
    }

    return eventData
  }

  /**
   * Attaches an EventListener instance so it can be triggered when the listened events are dispatched.
   * @param AbstractEventListener eventListener EventListener instance
   */
  EventDispatcher attach(AbstractEventListener eventListener) {
    this.eventListenerBag.addEventListener(eventListener)

    return this
  }

  /**
   * Attaches an EventListener so it can be triggered when the listened events are dispatched.
   * @param Class <?> eventListenerName EventListener classes to attach to this dispatcher
   */
  EventDispatcher attachClass(Class<?> eventListenerClass) {
    return this.attach(IOC.get(eventListenerClass.getName()) as AbstractEventListener)
  }

  /**
   * Attaches multiple EventListeners, so they can be triggered when the listened events are dispatched.
   * @param ArrayList <Class<?>> eventListenerNames EventListener classes to attach to this dispatcher
   */
  EventDispatcher attachClasses(ArrayList<Class<?>> eventListenerClasses) {
    for (eventListenerClass in eventListenerClasses) {
      this.attachClass(eventListenerClass)
    }

    return this
  }

  /**
   * Attaches an EventListenerWrapper wrap the targeted listeners when events are dispatched.
   * @param CanWrapListener eventListener
   */
  EventDispatcher attachWrapper(CanWrapListener eventListenerWrapper) {
    this.eventListenerWrappers += [eventListenerWrapper]

    return this
  }

  /**
   * Attaches EventListenerWrapper wraps the targeted listeners when events are dispatched.
   * @param CanWrapListener eventListener
   */
  EventDispatcher attachWrappers(List<CanWrapListener> eventListenerWrappers) {
    this.eventListenerWrappers += eventListenerWrappers

    return this
  }

  private CanWrapListener findMatchingWrapper(AbstractEventListener eventListener) {
    for (eventListenerWrapper in this.eventListenerWrappers) {
      if (eventListenerWrapper.support(eventListener)) {
        return eventListenerWrapper
      }
    }

    return null
  }
}
