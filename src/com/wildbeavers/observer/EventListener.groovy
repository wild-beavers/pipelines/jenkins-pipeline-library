package com.wildbeavers.observer

abstract class EventListener<T extends EventDataInterface> extends AbstractEventListener<T> {
  /**
   * {@inheritDoc}
   */
  final Boolean supports(String event) {
    return this.listenTo().contains(event)
  }

  /**
   * {@inheritDoc}
   */
  Boolean shouldRun(T eventData = null) {
    return true
  }

  /**
   * {@inheritDoc}
   */
  ArrayList<CharSequence> explainNoRun(T eventData = null) {
    return []
  }

  /**
   * {@inheritDoc}
   */
  CharSequence getDescription() {
    return ''
  }
}
