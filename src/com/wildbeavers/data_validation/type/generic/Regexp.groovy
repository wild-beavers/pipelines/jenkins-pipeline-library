package com.wildbeavers.data_validation.type.generic

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.ValidationException

abstract class Regexp implements IsValidatorType<CharSequence> {
  protected CharSequence value

  Regexp(CharSequence input) {
    if (!(input ==~ this.getRegexp())) {
      throw new ValidationException("“${input.toString()}” is not ${this.getDescription()}")
    }

    this.value = input
  }

  @Override
  @NonCPS
   String getDescription() {
    return "a string matching /${this.getRegexp()}/."
  }

  abstract String getRegexp()

  @NonCPS
  @Override
  String toString() {
    return this.value
  }
}
