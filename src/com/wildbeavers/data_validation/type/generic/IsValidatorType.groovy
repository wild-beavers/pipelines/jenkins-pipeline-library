package com.wildbeavers.data_validation.type.generic

interface IsValidatorType<T> {
  String getDescription()
}
