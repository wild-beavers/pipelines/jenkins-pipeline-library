package com.wildbeavers.data_validation.type.generic

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.ValidationException

abstract class Enum implements IsValidatorType<CharSequence> {
  protected CharSequence value

  Enum(CharSequence input) {
    if (!this.getSupportedValues().contains(input)) {
      throw new ValidationException("“${input.toString()}” is not ${this.getDescription()}.")
    }

    this.value = input
  }

  @Override
  @NonCPS
  String getDescription() {
    return "one of these possible values: ${this.getSupportedValues().join(', ')}."
  }

  @NonCPS
  abstract List<CharSequence> getSupportedValues()

  @Override
  @NonCPS
  String toString() {
    return this.value
  }
}
