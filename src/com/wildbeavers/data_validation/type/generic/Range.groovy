package com.wildbeavers.data_validation.type.generic

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.ValidationException

abstract class Range implements IsValidatorType<Integer> {
  protected Integer value

  Range(Integer input) {
    if (input < this.getMin() || this.getMax() < input) {
      throw new ValidationException("“${input.toString()}” is not ${this.getDescription()}.")
    }

    this.value = input
  }

  @Override
  @NonCPS
  String getDescription() {
    return "a number between ${this.getMin()} and ${this.getMax()}."
  }

  abstract Integer getMax()

  abstract Integer getMin()

  @Override
  @NonCPS
  String toString() {
    return this.value as String
  }

  Integer toInt() {
    return this.value
  }
}
