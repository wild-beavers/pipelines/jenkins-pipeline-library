package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

/**
 * git clone URL validator
 */
class GitCloneURL extends Regexp {
  GitCloneURL(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^((git@([^:]+):([^/]+)/([^.]+))|(^(?:(?:(https?|git))://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\\u00a1-\\uffff](?:-)*)*(?:[a-z0-9\\u00a1-\\uffff])+)(?:\\.(?:[a-z0-9\\u00a1-\\uffff](?:-)*)*(?:[a-z0-9\\u00a1-\\uffff])+)*(?:\\.(?:[a-z0-9\\u00a1-\\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?))\\.git$'
  }
}
