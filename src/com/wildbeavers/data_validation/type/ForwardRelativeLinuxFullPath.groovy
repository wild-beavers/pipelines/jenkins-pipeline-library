package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

class ForwardRelativeLinuxFullPath extends Regexp {
  ForwardRelativeLinuxFullPath(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    // Does not contain '..'
    // Is not '/' nor '.'
    // Does not finish by '/'
    // Only contain printable characters
    // Contains at least one character
    return '^(?!.*/$|.*\\.\\..*|^\\.$)([ -~]+)$'
  }
}
