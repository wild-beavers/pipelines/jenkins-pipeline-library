package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

/**
 * Frivolous URL validator with a mandatory scheme
 */
class ShallowURL extends Regexp {
  ShallowURL(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^(?:[a-z]+[a-z\\d]*)://(?:[a-z\\d][^/\\n]+[a-z\\d])(?:[^@\\ \\$:;,\\n]*)$'
  }

  String getPath() {
    return new URI(this.value.toString()).getPath()
  }

  CharSequence getHost() {
    return new URI(this.value.toString()).getHost()
  }

  CharSequence getScheme() {
    return new URI(this.value.toString()).getScheme()
  }

  CharSequence getQuery() {
    return new URI(this.value.toString()).getQuery()
  }
}
