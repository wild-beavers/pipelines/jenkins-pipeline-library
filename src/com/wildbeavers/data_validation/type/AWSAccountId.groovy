package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

/**
 * AWS Account ID
 */
class AWSAccountId extends Regexp {
  AWSAccountId(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^[0-9]{12}$'
  }
}
