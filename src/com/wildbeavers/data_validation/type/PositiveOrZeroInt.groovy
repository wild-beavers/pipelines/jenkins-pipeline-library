package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Range

/**
 * Positive integer or zero integer
 */
class PositiveOrZeroInt extends Range {
  PositiveOrZeroInt(Integer input) {
    super(input)
  }

  @NonCPS
  Integer getMax() {
    // 32bit int maximum
    return 2147483647
  }

  @NonCPS
  Integer getMin() {
    return 0
  }
}
