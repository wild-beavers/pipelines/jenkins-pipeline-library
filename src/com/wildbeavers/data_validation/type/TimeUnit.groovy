package com.wildbeavers.data_validation.type


import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Enum

/**
 * java.util.concurrent.TimeUnit validation
 */
class TimeUnit extends Enum {
  TimeUnit(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  List<CharSequence> getSupportedValues() {
    return ['NANOSECONDS', 'MICROSECONDS', 'MILLISECONDS', 'SECONDS', 'MINUTES', 'HOURS', 'DAYS']
  }
}
