package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

/**
 * Version or tags validator
 */
class Version extends Regexp {
  Version(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^(|[\\da-z][\\da-z\\.\\-\\+]{0,63})$'
  }
}
