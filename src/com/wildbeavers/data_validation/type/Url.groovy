package com.wildbeavers.data_validation.type

import com.cloudbees.groovy.cps.NonCPS

/**
 * URL validator
 */
class Url extends ShallowURL {
  Url(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^(?:(?:(https?|ftp))://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)(?:\\.(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)*(?:\\.(?:[a-z0-9\u00a1-\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?$'
  }
}
