package com.wildbeavers.data_validation

class ValidationException extends RuntimeException {
  ValidationException(String message) {
    super(message)
  }
}
