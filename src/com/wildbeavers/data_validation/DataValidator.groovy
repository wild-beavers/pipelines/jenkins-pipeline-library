package com.wildbeavers.data_validation

import com.wildbeavers.data_validation.type.generic.IsValidatorType

class DataValidator {
  public <T> T validate(rawData, Class<T> requiredType) {
    if (!requiredType.isInstance(rawData)) {
      if (requiredType in IsValidatorType) {
        rawData = requiredType.newInstance(rawData) as T
      } else {
        throw new ValidationException("“${rawData.toString()}” has an incorrect type: not a “${requiredType.getName()}”.")
      }
    }

    return rawData
  }

  public <T> T validate(Map<String, ?> rawMap, String key, Class<T> requiredType, T defaultValue = null, List<String> mapScopes = []) {
    if (!rawMap.containsKey(key)) {
      if (null == defaultValue) {
        throw new ValidationException(
          "Oops! It seems the mandatory option “${key}” was not defined. " +
            "Please pass a ${getCurrentStanza(key, mapScopes)} stanza as options map when running this pipeline."
        )
      }

      try {
        return this.validate(defaultValue, requiredType) as T
      } catch (ValidationException exception) {
        throw new RuntimeException(
          "Default value for ${getCurrentStanza(key, mapScopes, defaultValue.toString())} failed validation. \n" + exception.getMessage()
        )
      }
    }

    try {
      return this.validate(rawMap[key], requiredType) as T
    } catch (ValidationException exception) {
      throw new ValidationException(
        "Oops! There was a problem with this option: ${getCurrentStanza(key, mapScopes, rawMap[key].toString())}.\n" + exception.getMessage()
      )
    }
  }

  public <T> T validateOrNull(Map<String, ?> rawMap, String key, Class<T> requiredType, T defaultValue = null, List<String> mapScopes = []) {
    if (!rawMap.containsKey(key) && null == defaultValue) {
      return defaultValue
    }

    return this.validate(rawMap, key, requiredType, defaultValue, mapScopes)
  }

  private static String getCurrentStanza(String key, List<String> mapScopes, String value = 'xxx') {
    return generateCurrentStanza("${key}: \"${value.toString()}\"", mapScopes.reverse())
  }

  private static String generateCurrentStanza(CharSequence value, List<String> scopes) {
    if (scopes.size()) {
      String scope = scopes.pop()

      value = """${scope}: [${"  ".repeat(scopes.size() + 1)}
${"  ".repeat(scopes.size() + 2)}${value}
${"  ".repeat(scopes.size() + 1)}]"""

      return generateCurrentStanza(value, scopes)
    }

    return "[\n  ${value}\n]"
  }
}
