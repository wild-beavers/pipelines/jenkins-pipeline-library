package com.wildbeavers.factory

import com.wildbeavers.data.OptionString
import com.wildbeavers.exception.OptionStringException

/**
 * DEPRECATED. Use OptionString directly.
 * @deprecated
 */
class OptionStringFactory {
  private OptionString optionString

  /**
   * @deprecated
   */
  String toString() {
    return this.optionString.toString()
  }

  /**
   * @deprecated
   */
  void addCommandTarget(CharSequence commandTarget) {
    this.optionString.addOption(commandTarget, '')
  }

  /**
   * @deprecated
   */
  void addOption(String optionName, Object optionValue = '', Class<?> expectedOptionType = CharSequence) {
    this.checkOptionString()
    this.checkOptionValueType(optionName, optionValue, expectedOptionType)

    if (optionValue instanceof AbstractCollection) {
      for (singleOptionValue in optionValue) {
        this.optionString.addOption(optionName, singleOptionValue as String)
      }

      return
    }

    this.optionString.addOption(optionName, optionValue as String)
  }

  /**
   * @deprecated
   */
  OptionString createOptionString(String delimiter) {
    this.optionString = new OptionString()
    this.optionString.setDelimiter(delimiter)

    return this.optionString
  }

  /**
   * @deprecated
   */
  OptionString getOptionString() {
    return optionString
  }

  /**
   * @deprecated
   */
  private void checkOptionString() {
    if (null == this.optionString) {
      throw new OptionStringException("Cannot call method on object ${this.getClass()} since “createOptionString” was not called. Please call “createOptionString” before manipulating this object.")
    }
  }

  /**
   * @deprecated
   */
  private void checkOptionValueType(String optionName, Object optionValue, Class<?> expectedOptionType) {
    if (!(expectedOptionType.isInstance(optionValue))) {
      throw new OptionStringException("Cannot register option with value “${optionName}” to the option string. It is expected to be “${expectedOptionType}”, given: “${optionValue.getClass()}”.")
    }
  }
}
