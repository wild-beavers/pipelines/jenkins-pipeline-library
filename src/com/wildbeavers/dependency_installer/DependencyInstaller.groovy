package com.wildbeavers.dependency_installer

import com.wildbeavers.data.DependencyInstallerData

interface DependencyInstaller {
  /**
   * Install dependencies
   * @param dependencyInstaller
   */
  void install(DependencyInstallerData dependencyInstaller)

  /**
   * Uninstall dependencies
   * @param dependencyInstaller
   */
  void uninstall(DependencyInstallerData dependencyInstaller)

  /**
   * Get the dependency installer type
   */
  DependencyInstallerType getType()
}
