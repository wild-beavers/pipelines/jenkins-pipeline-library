package com.wildbeavers.dependency_installer

import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.composite.FetcherInterface

class DependencyInstallerFetcher implements FetcherInterface<DependencyInstallerType, DependencyInstaller> {
  private FetcherDeprecated fetcher

  DependencyInstallerFetcher(FetcherDeprecated fetcher) {
    this.fetcher = fetcher
  }

  void register(DependencyInstaller dependencyInstaller) {
    this.register(dependencyInstaller.getType(), dependencyInstaller)
  }

  void register(DependencyInstallerType type, DependencyInstaller value) {
    this.fetcher.register(type, value)
  }

  DependencyInstaller fetch(DependencyInstallerType type) {
    return this.fetcher.fetch(type)
  }
}
