package com.wildbeavers.dependency_installer


import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.composite.FetcherInterface

class DependencyInstallerDataManagerFetcher implements FetcherInterface<DependencyInstallerType, CanManagedDependencyInstallerData> {
  private FetcherDeprecated fetcher

  DependencyInstallerDataManagerFetcher(FetcherDeprecated fetcher) {
    this.fetcher = fetcher
  }

  void register(CanManagedDependencyInstallerData canManagedDependencyInstallerData) {
    this.register(canManagedDependencyInstallerData.getType(), canManagedDependencyInstallerData)
  }

  void register(DependencyInstallerType type, CanManagedDependencyInstallerData value) {
    this.fetcher.register(type, value)
  }

  CanManagedDependencyInstallerData fetch(DependencyInstallerType type) {
    return this.fetcher.fetch(type)
  }
}
