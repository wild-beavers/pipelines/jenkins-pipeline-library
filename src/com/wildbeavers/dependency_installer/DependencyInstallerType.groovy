package com.wildbeavers.dependency_installer

enum DependencyInstallerType {
  VENV,
  EXISTING_VENV
}
