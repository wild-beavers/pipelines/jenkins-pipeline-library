package com.wildbeavers.dependency_installer

import com.wildbeavers.data.DependencyInstallerData

interface CanManagedDependencyInstallerData {
  /**
   * Create a new DependencyInstallerData
   * @param config
   * @return The DependencyInstallerData
   * @throw Exception when an input option is not supported
   */
  DependencyInstallerData create(Map<String, Object> config)

  /**
   * Get the dependencyInstallerType
   * @return The supported dependency installer type
   */
  DependencyInstallerType getType()
}
