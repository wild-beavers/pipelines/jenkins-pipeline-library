package com.wildbeavers.artifact

import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.artifact.ArtifactData

interface IsArtifactStore {
  /**
   * Non-thread safe actions to execute before downloading an artifact.
   * @param localArtifactPath
   * @param artifactData
   */
  void preDownloadActions(CharSequence localArtifactPath, ArtifactData artifactData)

  /**
   * Thread-safe downloading of an artifact
   * @param localArtifactPath
   * @param artifactData
   * @return
   */
  CommandResult downloadArtifact(CharSequence localArtifactPath, ArtifactData artifactData)

  /**
   * Non-thread safe actions to execute before uploading an artifact.
   * @param destinationArtifactURL
   * @param localArtifactPath
   * @param artifactData
   */
  void preUploadActions(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData)

  /**
   * Thread-safe uploading of an artifact
   * @param destinationArtifactURL
   * @param localArtifactPath
   * @param artifactData
   * @return
   */
  CommandResult uploadArtifact(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData)

  /**
   * Whether or not the artifactData is supported by the current ArtifactStore.
   * @param artifactData
   * @return
   */
  Boolean support(ArtifactData artifactData)
}
