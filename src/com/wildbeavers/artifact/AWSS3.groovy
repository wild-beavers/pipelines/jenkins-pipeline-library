package com.wildbeavers.artifact

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.File
import com.wildbeavers.proxy.FileProxy

/**
 * ArtifactStore implementation to get/put artifacts in S3.
 * Expects accessorName to be a ContainerOptions factory name.
 */
class AWSS3 implements IsArtifactStore {
  final static DEFAULT_ACCESSOR_NAME = 'aws'

  private FileProxy fileProxy
  private File file
  private ContainerRunner containerRunner
  private GenericFactory<ContainerOptions> containerOptionsGenericFactory
  private DataValidator dataValidator

  AWSS3(FileProxy fileProxy, File file, ContainerRunner containerRunner, GenericFactory<ContainerOptions> containerOptionsGenericFactory, DataValidator dataValidator) {
    this.fileProxy = fileProxy
    this.file = file
    this.containerRunner = containerRunner
    this.containerOptionsGenericFactory = containerOptionsGenericFactory
    this.dataValidator = dataValidator
  }

  void preDownloadActions(CharSequence localArtifactPath, ArtifactData artifactData) {
    this.containerRunner.pull(this.containerOptionsGenericFactory.instantiate(artifactData.getAccessorName().isEmpty() ? DEFAULT_ACCESSOR_NAME : artifactData.getAccessorName()))
  }

  @Override
  CommandResult downloadArtifact(CharSequence localArtifactPath, ArtifactData artifactData) {
    return this.containerRunner.run(
      this.containerOptionsGenericFactory.instantiate(artifactData.getAccessorName().isEmpty() ? DEFAULT_ACCESSOR_NAME : artifactData.getAccessorName()),
      "s3 cp \"${artifactData.getUrl()}\" \"${localArtifactPath}\"",
    )
  }

  @Override
  void preUploadActions(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData) {
    this.containerRunner.pull(this.containerOptionsGenericFactory.instantiate(artifactData.getAccessorName().isEmpty() ? DEFAULT_ACCESSOR_NAME : artifactData.getAccessorName()))
  }

  @Override
  CommandResult uploadArtifact(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData) {
    // This should be refactored: destinationArtifactURL is actually inside artifactData
    this.dataValidator.validate(destinationArtifactURL, ShallowURL)

    if (!this.fileProxy.exists(localArtifactPath)) {
      throw new FileNotFoundException("The file ${localArtifactPath} does not exists.")
    }

    return this.containerRunner.run(
      this.containerOptionsGenericFactory.instantiate(artifactData.getAccessorName().isEmpty() ? DEFAULT_ACCESSOR_NAME : artifactData.getAccessorName()),
      "s3 cp --content-type \"${this.file.guessMimeType(localArtifactPath)}\" \"${localArtifactPath}\" \"${destinationArtifactURL}\""
    )
  }

  @Override
  Boolean support(ArtifactData artifactData) {
    if (isSchemeS3(artifactData.getUrlStrict().getScheme()) || isHostS3(artifactData.getUrlStrict().getHost())) {
      return true
    }

    return false
  }

  private static Boolean isSchemeS3(CharSequence scheme) {
    return 's3' == scheme
  }

  private static Boolean isHostS3(CharSequence host) {
    return host =~ /.*(s3\..*amazonaws.com).*/
  }
}
