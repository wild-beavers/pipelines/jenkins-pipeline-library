package com.wildbeavers.artifact

import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.OptionString
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.proxy.ExecuteProxy

/**
 * ArtifactStore implementation to clone/push a git project
 */
class Git implements IsArtifactStore {
  ExecuteProxy executeProxy

  Git(ExecuteProxy executeProxy) {
    this.executeProxy = executeProxy
  }

  @Override
  void preDownloadActions(CharSequence localArtifactPath, ArtifactData artifactData) {
  }

  @Override
  CommandResult downloadArtifact(CharSequence localArtifactPath, ArtifactData artifactData) {
    OptionString optionString = new OptionString()
    if (artifactData.getVersion()) {
      optionString.addOption('--depth', '1')
      optionString.addOption('--branch', artifactData.getVersion().toString())
    }

    optionString.addOption("\"${artifactData.getUrl()}\"")
    optionString.addOption("\"${localArtifactPath}\"")

    this.executeProxy.executeWithTail("git clone ${optionString.toString()}")
  }

  @Override
  void preUploadActions(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData) {
  }

  @Override
  CommandResult uploadArtifact(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData) {
    throw new RuntimeException("${this.class.getName()} does not support uploading artifacts yet.")
  }

  @Override
  Boolean support(ArtifactData artifactData) {
    if (isSchemeGit(artifactData.getUrlStrict().getScheme()) || isGitRepository(artifactData.getUrlStrict().getPath())) {
      return true
    }

    return false
  }

  private static Boolean isSchemeGit(CharSequence scheme) {
    return 'git' == scheme
  }

  private static Boolean isGitRepository(CharSequence path) {
    return path =~ /.*\.git/
  }
}
