package com.wildbeavers.artifact


import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.artifact.ArtifactData

/**
 * Facade class to get/put artifacts.
 * It will use the relevant IsArtifactStore implementation.
 */
class ArtifactStore {
  private ArtifactStoreCollection artifactStoreCollection

  ArtifactStore(ArtifactStoreCollection artifactStoreCollection) {
    this.artifactStoreCollection = artifactStoreCollection
  }

  void preUploadActions(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData) {
    this.artifactStoreCollection.getSupportedArtifactStore(artifactData).preUploadActions(
      destinationArtifactURL, localArtifactPath, artifactData
    )
  }

  CommandResult uploadArtifact(String destinationArtifactURL, String localArtifactPath, ArtifactData artifactData) {
    return this.artifactStoreCollection.getSupportedArtifactStore(artifactData).uploadArtifact(
      destinationArtifactURL, localArtifactPath, artifactData
    )
  }

  void preDownloadActions(CharSequence localArtifactPath, ArtifactData artifactData) {
    this.artifactStoreCollection.getSupportedArtifactStore(artifactData).preDownloadActions(
      localArtifactPath, artifactData
    )
  }

  CommandResult downloadArtifact(CharSequence localArtifactPath, ArtifactData artifactData) {
    return this.artifactStoreCollection.getSupportedArtifactStore(artifactData).downloadArtifact(
      localArtifactPath, artifactData
    )
  }
}
