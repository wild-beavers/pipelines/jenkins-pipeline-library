package com.wildbeavers.artifact

import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.exception.ConfigurationException

/**
 * Collection of ArtifactStore objects.
 */
class ArtifactStoreCollection {
  private List<IsArtifactStore> artifactStores = []

  List<IsArtifactStore> getAllArtifactStores() {
    return this.artifactStores
  }

  IsArtifactStore getSupportedArtifactStore(ArtifactData artifactData) {
    for (artifactStore in this.artifactStores) {
      if (artifactStore.support(artifactData)) {
        return artifactStore
      }
    }

    throw new ConfigurationException("No artifact store supports artifact data “${artifactData.toString()}”.")
  }

  void addArtifactStore(IsArtifactStore artifactStore) {
    if (this.artifactStores.contains(artifactStore)) {
      return
    }

    this.artifactStores.add(artifactStore)
  }
}
