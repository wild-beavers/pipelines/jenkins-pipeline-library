package com.wildbeavers.listener

import com.wildbeavers.archiver.Archiver
import com.wildbeavers.artifact.ArtifactStore

import com.wildbeavers.data.ScmInfo
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.ParallelProxy

/**
 * Publishes artifacts in any supported Artifact store.
 */
class ArtifactPublisherListener extends EventListener<RunnerEventData> implements IOCRefreshable {
  final String TEMPORARY_ARCHIVE_NAME = "temp"

  private Debugger debugger
  private Archiver archiver
  private ExecuteProxy executeProxy
  private ArtifactStore artifactStore
  private ParallelProxy parallelProxy
  private ContainerRunner containerRunner
  private ScmInfo scmInfo
  private GenericFactory<ArtifactData> artifactDataGenericFactory

  ArtifactPublisherListener(Debugger debugger, Archiver archiver, ExecuteProxy executeProxy, ArtifactStore artifactStore, ParallelProxy parallelProxy, ContainerRunner containerRunner, ScmInfo scmInfo, GenericFactory<ArtifactData> artifactDataGenericFactory) {
    this.debugger = debugger
    this.archiver = archiver
    this.executeProxy = executeProxy
    this.artifactStore = artifactStore
    this.parallelProxy = parallelProxy
    this.containerRunner = containerRunner
    this.scmInfo = scmInfo
    this.artifactDataGenericFactory = artifactDataGenericFactory
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PUBLISH]
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return this.debugger.testCondition(null != eventData.getArtifactPublisherData().getBaseUrl() && !eventData.getArtifactPublisherData().getBaseUrl().isEmpty(), 'Will not publish artifact: no artifacts base URL given.') &&
      this.debugger.testCondition(this.scmInfo.isPublishableAsAnything(), 'Will not publish artifacts: the current revision is not publishable.')
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    this.debugger.print("Publishing artifacts to “${eventData.getArtifactPublisherData().getBaseUrl()}”")

    ArtifactData artifactData = this.artifactDataGenericFactory.instantiate()
    artifactData.setUrl(new ShallowURL(eventData.getArtifactPublisherData().getBaseUrl()))
    artifactData.setVersion(eventData.getArtifactPublisherData().getVersion())
    artifactData.setAccessorName(eventData.getArtifactPublisherData().getAccessorName())

    this.archiver.compress(this.getFilesToCompress(eventData.getArtifactPublisherData()), this.getTemporaryArchiveName(eventData.getArtifactPublisherData()))

    this.artifactStore.preUploadActions(eventData.getArtifactPublisherData().getBaseUrl(), this.getTemporaryArchiveName(eventData.getArtifactPublisherData()), artifactData)

    this.parallelProxy.parallel(
      this.scmInfo.getAllRevisionSemverAndFloatingTags().collectEntries({ tag ->
        [(tag): { this.upload("${tag}.${eventData.getArtifactPublisherData().getCompressionAlgorithm()}", eventData, artifactData) }]
      }) as Map<String, Closure>
    )

    return eventData
  }

  private getFilesToCompress(ArtifactPublisherData eventData) {
    List<String> localFiles = this.executeProxy.execute('ls').getStdout().split()

    return localFiles.collect({
      if (eventData.getPathToExclude().contains(it)) {
        return ''
      }

      return it
    }) - ''
  }

  private void upload(String destinationFilename, RunnerEventData eventData, ArtifactData artifactData) {
    this.artifactStore.uploadArtifact(
      "${eventData.getArtifactPublisherData().getBaseUrl()}/${this.scmInfo.getRepositoryName()}/${destinationFilename}",
      this.getTemporaryArchiveName(eventData.getArtifactPublisherData()),
      artifactData
    )
  }

  private String getTemporaryArchiveName(ArtifactPublisherData eventData) {
    return "${TEMPORARY_ARCHIVE_NAME}.${eventData.getCompressionAlgorithm()}"
  }
}
