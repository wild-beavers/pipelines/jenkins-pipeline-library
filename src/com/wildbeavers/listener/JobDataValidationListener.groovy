package com.wildbeavers.listener

import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.PositiveOrZeroInt
import com.wildbeavers.data_validation.type.TimeUnit
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate all user configuration related to the current job.
 */
class JobDataValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator

  JobDataValidationListener(DataValidator dataValidator) {
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    eventData.setJobProperties(this.dataValidator.validate(config, 'job_properties', List, eventData.getJobProperties()))
    eventData.setJobNodeLabel(this.dataValidator.validate(config, 'job_nodeLabel', CharSequence, eventData.getJobNodeLabel()))
    eventData.setJobTimeoutTime(this.dataValidator.validate(config, 'job_timeoutTime', PositiveOrZeroInt, eventData.getJobTimeoutTime()))
    eventData.setJobTimeoutUnit(this.dataValidator.validate(config, 'job_timeoutUnit', TimeUnit, eventData.getJobTimeoutUnit()))
    eventData.setDebuggingEnabled(this.dataValidator.validate(config, 'job_debuggingEnabled', Boolean, eventData.getDebuggingEnabled()))
    eventData.setDebuggingRuntimeToggleEnabled(this.dataValidator.validate(config, 'job_debuggingRuntimeToggleEnabled', Boolean, eventData.getDebuggingRuntimeToggleEnabled()))
    eventData.setDebuggingRuntimeToggleTimeout(this.dataValidator.validate(config, 'job_debuggingRuntimeToggleTimeout', PositiveOrZeroInt, eventData.getDebuggingRuntimeToggleTimeout()))

    return eventData
  }
}
