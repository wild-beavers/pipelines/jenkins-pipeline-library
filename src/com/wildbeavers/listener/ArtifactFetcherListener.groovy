package com.wildbeavers.listener

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.archiver.Archiver
import com.wildbeavers.artifact.ArtifactStore
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ParallelProxy

/**
 * Fetches artifacts in any Artifact store.
 */
class ArtifactFetcherListener extends EventListener<RunnerEventData> {
  private Debugger debugger
  private ArtifactStore artifactStore
  private Archiver archiver
  private ParallelProxy parallelProxy

  ArtifactFetcherListener(Debugger debugger, ArtifactStore artifactStore, Archiver archiver, ParallelProxy parallelProxy) {
    this.debugger = debugger
    this.artifactStore = artifactStore
    this.archiver = archiver
    this.parallelProxy = parallelProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PREPARE]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return !eventData.getArtifactFetchingData().getArtifactData().isEmpty()
  }

  @Override
  ArrayList<CharSequence> explainNoRun(RunnerEventData eventData) {
    return ['No artifacts to fetch were provided.']
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    Map<String, Closure> branches = [:]
    for (artifactData in eventData.getArtifactFetchingData().getArtifactData()) {
      // Object is cloned to prevent creating branches with the same reference of the same object.
      ArtifactData artifactDataClone = artifactData

      if (artifactData.toString().isEmpty()) {
        this.debugger.printDebug("Ignoring ${artifactData.getUrl()}. Empty URI path.")

        continue
      }

      String tempFile = UUID.randomUUID().toString() + "-" + artifactData.toString()

      this.artifactStore.preDownloadActions(tempFile, artifactDataClone)

      branches[artifactDataClone.toString()] = {
        this.fetch(tempFile, artifactDataClone)
      }
    }

    this.parallelProxy.parallel(branches)

    return eventData
  }

  private void fetch(String tempFile, ArtifactData artifactData) {
    this.debugger.print("Fetching ${artifactData.getUrl()}.")
    this.artifactStore.downloadArtifact(tempFile, artifactData)

    this.debugger.print("Unpacking ${artifactData.getUrl()}.")
    // This would ultimately need to become a packer/unpacker system, to better fit with situation when archives are not involved.
    this.archiver.uncompress(tempFile, artifactData.getTrimmedPath().toString())
  }
}
