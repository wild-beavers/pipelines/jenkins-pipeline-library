package com.wildbeavers.listener

import com.cloudbees.groovy.cps.SerializableScript
import com.wildbeavers.data_validation.type.TimeUnit
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.TimeoutProxy
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException

/**
 * Allows user to enable debugging during runtime.
 */
class JobRuntimeDebuggingListener extends EventListener<ControllerEventData> {
  private SerializableScript context
  private TimeoutProxy timeoutProxy
  private EnvironmentVariableProxy environmentVariableProxy

  JobRuntimeDebuggingListener(SerializableScript context, TimeoutProxy timeoutProxy, EnvironmentVariableProxy environmentVariableProxy) {
    this.context = context
    this.timeoutProxy = timeoutProxy
    this.environmentVariableProxy = environmentVariableProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.PRE_RUNNER]
  }

  @Override
  Boolean shouldRun(ControllerEventData eventData) {
    return eventData.getDebuggingEnabled() || eventData.getDebuggingRuntimeToggleEnabled()
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    if (eventData.getDebuggingEnabled()) {
      this.environmentVariableProxy.set('DEBUG', 'true')
      return eventData
    }

    try {
      this.timeoutProxy.timeout(eventData.getDebuggingRuntimeToggleTimeout(), new TimeUnit('SECONDS'), {
        this.context.input(message: '', ok: 'Enable Debugging')
        this.environmentVariableProxy.set('DEBUG', 'true')
      })
    } catch (FlowInterruptedException ignored) {
    }

    return eventData
  }
}
