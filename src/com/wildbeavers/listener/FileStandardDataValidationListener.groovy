package com.wildbeavers.listener


import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for fileStandard and fill data object.
 */
class FileStandardDataValidationListener extends EventListener<ControllerEventData> {
  final static DEFAULT_MANDATORY_FILES = [
    'LICENSE',
    'README.md',
  ]

  DataValidator dataValidator

  FileStandardDataValidationListener(DataValidator dataValidator) {
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    eventData.getRunnerEventData().setFileStandardEnabled(
      this.dataValidator.validate(config, 'fileStandard_enabled', Boolean, eventData.getRunnerEventData().isFileStandardEnabled())
    )
    eventData.getRunnerEventData().setFileStandardIgnoreDefault(
      this.dataValidator.validate(config, 'fileStandard_ignoreDefault', Boolean, eventData.getRunnerEventData().isFileStandardIgnoreDefault())
    )

    if (!eventData.getRunnerEventData().isFileStandardIgnoreDefault()) {
      for (mandatoryFile in DEFAULT_MANDATORY_FILES) {
        eventData.getRunnerEventData().getFileStandardData().addMandatoryFile(
          this.dataValidator.validate(mandatoryFile, ForwardRelativeLinuxFullPath)
        )
      }
    }

    for (mandatoryFile in this.dataValidator.validate(config, 'fileStandard_mandatoryFiles', List, [])) {
      eventData.getRunnerEventData().getFileStandardData().addMandatoryFile(
        this.dataValidator.validate(mandatoryFile, ForwardRelativeLinuxFullPath)
      )
    }

    for (forbiddenFile in this.dataValidator.validate(config, 'fileStandard_forbiddenFiles', List, [])) {
      eventData.getRunnerEventData().getFileStandardData().addForbiddenFile(
        this.dataValidator.validate(forbiddenFile, ForwardRelativeLinuxFullPath)
      )
    }

    return eventData
  }

  @Override
  Integer getOrder() {
    return 100
  }
}
