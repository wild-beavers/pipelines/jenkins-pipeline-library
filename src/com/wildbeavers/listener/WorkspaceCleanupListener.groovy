package com.wildbeavers.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

/**
 * Handles the cleanup of the workspace
 */
class WorkspaceCleanupListener extends EventListener {
  private Script context
  private Debugger debugger

  WorkspaceCleanupListener(Script context, Debugger debugger) {
    this.context = context
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.CLEANUP]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    try {
      this.context.cleanWs()
    } catch (NoSuchMethodError acceptableError) {
      this.debugger.printDebug('“cleanWs” pipeline step is not available.')
    }
  }
}
