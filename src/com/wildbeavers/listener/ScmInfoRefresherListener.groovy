package com.wildbeavers.listener

import com.wildbeavers.data_manager.ScmInfoManager
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener

/**
 * Refreshes SCM information.
 */
class ScmInfoRefresherListener extends EventListener<RunnerEventData> {
  private ScmInfoManager scmInfoManager

  ScmInfoRefresherListener(ScmInfoManager scmInfoManager) {
    this.scmInfoManager = scmInfoManager
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_PREPARE]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    this.scmInfoManager.refreshScmInfo()

    return eventData
  }

  Integer getOrder() {
    return 20
  }
}
