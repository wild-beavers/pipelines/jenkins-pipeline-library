package com.wildbeavers.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.FileProxy
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener

/**
 * Runs pre-commit hooks. Any fail on one hook will stop the pipeline.
 */
class PreCommitListener extends EventListener<RunnerEventData> {
  private ExecuteProxy executeProxy
  private FileProxy fileProxy
  private Debugger debugger
  private ContainerRunner containerRunner

  PreCommitListener(ExecuteProxy executeProxy, FileProxy fileProxy, Debugger debugger, ContainerRunner containerRunner) {
    this.executeProxy = executeProxy
    this.fileProxy = fileProxy
    this.debugger = debugger
    this.containerRunner = containerRunner
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.STATIC_ANALYSIS]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return (this.fileProxy.exists('.pre-commit-config.yaml') || this.fileProxy.exists('.pre-commit-config.yml')) &&
      eventData.getPreCommitData().isEnabled()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    try {
      this.containerRunner.run(eventData.getPreCommitData().getContainerOptions(), eventData.getPreCommitData().getRunCommandArguments())
    } catch (CommandExecutionException exception) {
      this.debugger.printDebug(this.executeProxy.execute('git diff').getStdout())

      throw exception
    }

    return eventData
  }
}
