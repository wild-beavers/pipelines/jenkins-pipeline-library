package com.wildbeavers.listener

import com.wildbeavers.dependency_installer.DependencyInstallerFetcher
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

/**
 * Runs dependency installer. Any fail will stop the pipeline
 */
class DependencyInstallerListener extends EventListener {
  private Script context
  private Debugger debugger
  private DependencyInstallerFetcher dependencyInstallFetcher

  DependencyInstallerListener(Script context, Debugger debugger, DependencyInstallerFetcher dependencyInstallFetcher) {
    this.context = context
    this.debugger = debugger
    this.dependencyInstallFetcher = dependencyInstallFetcher
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PREPARE]
  }

  @Override
  Boolean shouldRun(EventDataInterface eventData = null) {
    return eventData.getDependencyInstallerData().size() > 0
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    eventData.getDependencyInstallerData().each {
      it.value.each {dependencyInstallerData ->
        this.dependencyInstallFetcher.fetch(it.key).install(dependencyInstallerData)
      }
    }

    return eventData
  }
}
