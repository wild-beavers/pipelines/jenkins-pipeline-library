package com.wildbeavers.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.ExecuteProxy

/**
 * Handles SCM checkouts
 */
class ScmCheckoutListener extends EventListener<RunnerEventData> {
  private Script context
  private ExecuteProxy executeProxy

  ScmCheckoutListener(Script context, ExecuteProxy executeProxy) {
    this.context = context
    this.executeProxy = executeProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_PREPARE]
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    if (eventData == null) {
      throw new RuntimeException('Cannot checkout: event data was expected to get user options, but none were provided')
    }

    if (this.shouldCheckoutWithTag(eventData)) {
      this.checkoutWithTag(eventData)
    } else {
      this.doCheckout(eventData)
    }

    return eventData
  }

  private Boolean shouldCheckoutWithTag(RunnerEventData eventData) {
    return eventData?.getScmAlternativeCheckoutTag() != null && !eventData?.getScmAlternativeCheckoutTag()?.isEmpty()
  }

  private checkoutWithTag(RunnerEventData eventData) {
    this.doCheckout(eventData)

    def commandResult = this.executeProxy.executeAndAllowError(
      "git rev-parse -q --verify “refs/tags/${eventData.getScmAlternativeCheckoutTag()}”"
    )

    if ('' == commandResult.getStdout()) {
      throw new Exception("There is no tag “${eventData.getScmAlternativeCheckoutTag()}” in the repo “${this.getRepositoryURL(eventData)}“")
    }

    this.executeProxy.execute("git checkout ${eventData.getScmAlternativeCheckoutTag()}")
  }

  private void doCheckout(RunnerEventData eventData) {
    this.context.checkout([
      $class                           : 'GitSCM',
      branches                         : this.context.scm.branches,
      // Name, refspec and extensions could also have alternatives, the feature is not implemented yet.
      extensions                       : this.context.scm.extensions + [$class: 'CloneOption', noTags: false, shallow: false, depth: 0],
      userRemoteConfigs                : [[
                                            credentialsId: this.getCredentialId(eventData),
                                            name         : this.context.scm.getUserRemoteConfigs()[0].getName(),
                                            refspec      : '+refs/heads/*:refs/remotes/origin/*',
                                            url          : this.getRepositoryURL(eventData)
                                          ]],
      doGenerateSubmoduleConfigurations: false
    ])
  }

  private CharSequence getRepositoryURL(RunnerEventData eventData) {
    return eventData.getScmAlternativeCheckoutRepositoryURL() ?: this.context.scm.getUserRemoteConfigs()[0].getUrl()
  }

  private CharSequence getCredentialId(RunnerEventData eventData) {
    return eventData.getScmAlternativeCheckoutCredentialID() ?: this.context.scm.getUserRemoteConfigs()[0].getCredentialsId()
  }

  Integer getOrder() {
    return 10
  }
}
