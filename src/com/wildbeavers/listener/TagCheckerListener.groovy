package com.wildbeavers.listener

import com.wildbeavers.data.HasTagCheckerData
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.observer.EventListener

/**
 * Check the sanity of current project’s tag
 */
class TagCheckerListener extends EventListener<HasTagCheckerData> implements IOCRefreshable {
  ScmInfo scmInfo

  TagCheckerListener(ScmInfo scmInfo) {
    this.scmInfo = scmInfo
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.LINT]
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  @Override
  Boolean shouldRun(HasTagCheckerData eventData = null) {
    return eventData.getTagCheckEnabled() && (this.scmInfo.isCurrentBranchDefault() || !eventData.getTagCheckOnlyMainBranch())
  }

  @Override
  ArrayList<CharSequence> explainNoRun(HasTagCheckerData eventData) {
    return ['Tag checks are disabled.']
  }

  /**
   * @param HasTagCheckerData eventData
   * @return HasTagCheckerData
   */
  HasTagCheckerData run(HasTagCheckerData eventData = null) {
    LinkedHashSet<CharSequence> offendingTags = this.scmInfo.getAllRevisionTags().findAll({ !(it ==~ eventData.getTagCheckRegex()) })

    if (!offendingTags.isEmpty()) {
      throw new RuntimeException("Some non-compliant tags were found in this project: ${offendingTags.join(', ')}")
    }

    return eventData
  }

  @Override
  Integer getOrder() {
    return -10
  }
}
