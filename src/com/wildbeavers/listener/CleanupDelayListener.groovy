package com.wildbeavers.listener

import com.wildbeavers.data_validation.type.TimeUnit
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener
import com.wildbeavers.proxy.TimeoutProxy
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException

/**
 * Delays cleanup unless end user validates.
 */
class CleanupDelayListener extends EventListener<RunnerEventData> {
  private Script context
  private TimeoutProxy timeoutProxy
  private Debugger debugger

  CleanupDelayListener(Script context, TimeoutProxy timeoutProxy, Debugger debugger) {
    this.context = context
    this.timeoutProxy = timeoutProxy
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [
      RunnerEvents.POST_DEPLOY,
    ]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return eventData.getCleanupDelayEnabled()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    if (eventData == null) {
      throw new RuntimeException('Cannot delay cleanup: event data was expected to get user options, but none were provided')
    }

    try {
      this.timeoutProxy.timeout(eventData.getCleanupDelayTime(), new TimeUnit('MINUTES'), {
        this.debugger.print(eventData.getCleanupDelayHeader())
        this.context.input(
          message: eventData.getCleanupDelayMessage(),
          ok: eventData.getCleanupDelayButtonLabel(),
          submitterParameter: 'approver'
        )
      })
    } catch (FlowInterruptedException exception) {
      if (!eventData.getCleanupDelayContinueIfExpired()) {
        throw exception
      }
    }

    return eventData
  }

  @Override
  Integer getOrder() {
    return 9999
  }
}
