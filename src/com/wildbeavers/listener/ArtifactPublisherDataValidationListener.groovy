package com.wildbeavers.listener

import com.google.common.primitives.Chars
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.data_validation.type.Version
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for artifact fetching and fill data object.
 */
class ArtifactPublisherDataValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator
  private GenericFactory<ArtifactPublisherData> artifactPublisherGenericFactory

  ArtifactPublisherDataValidationListener(DataValidator dataValidator, GenericFactory<ArtifactPublisherData> artifactPublisherGenericFactory) {
    this.dataValidator = dataValidator
    this.artifactPublisherGenericFactory = artifactPublisherGenericFactory
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    ArtifactPublisherData artifactPublisherData = this.artifactPublisherGenericFactory.instantiate()

    artifactPublisherData.setPathToExclude(
      this.dataValidator.validate(config, 'artifactPublisher_pathsToExclude', List, artifactPublisherData.getPathToExclude(), ['artifactPublisher'])
    )
    artifactPublisherData.setCompressionEnabled(
      this.dataValidator.validate(config, 'artifactPublisher_compressionEnabled', Boolean, artifactPublisherData.getCompressionEnabled(), ['artifactPublisher'])
    )
    artifactPublisherData.setCompressionAlgorithm(
      this.dataValidator.validate( config, 'artifactPublisher_compressionAlgorithm', CharSequence, artifactPublisherData.getCompressionAlgorithm(), ['artifactPublisher'])
    )
    artifactPublisherData.setBaseUrl(
      this.dataValidator.validateOrNull(config, 'artifactPublisher_baseUrl', CharSequence, artifactPublisherData.getBaseUrl(), ['artifactPublisher'])
    )
    artifactPublisherData.setVersion(
      this.dataValidator.validateOrNull(config, 'artifactPublisher_version', Version, artifactPublisherData.getVersion(), ['artifactPublisher'])
    )
    artifactPublisherData.setAccessorName(
      this.dataValidator.validateOrNull(config, 'artifactPublisher_accessorName', CharSequence, artifactPublisherData.getAccessorName(), ['artifactPublisher'])
    )

    eventData.getRunnerEventData().setArtifactPublisherData(artifactPublisherData)

    return eventData
  }
}
