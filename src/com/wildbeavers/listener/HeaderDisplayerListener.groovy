package com.wildbeavers.listener


import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

/**
 * Displays a header message in the outputs at the beginning of the pipeline
 */
class HeaderDisplayerListener extends EventListener {
  private Debugger debugger

  HeaderDisplayerListener(Debugger debugger) {
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_PREPARE]
  }

  @Override
  Boolean shouldRun(EventDataInterface eventData = null) {
    return '' != eventData.getHeaderMessage()
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    this.debugger.print(eventData.getHeaderMessage())

    return eventData
  }

  Integer getOrder() {
    return 5
  }
}
