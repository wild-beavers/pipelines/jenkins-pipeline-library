package com.wildbeavers.listener.wrapper.terraform

import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.SSHKeySetupHelper
import com.wildbeavers.listener.wrapper.CanWrapListener
import com.wildbeavers.observer.AbstractEventListener
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.terraform.listener.TerraformInitListener

/**
 * Wraps AWS Terraform init calls with SSH keys ready to be used
 */
class AWSTerraformInitSSHWrapper implements CanWrapListener {
  private AWSTerraformInitWrapper awsTerraformInitWrapper
  private SSHKeySetupHelper sshKeySetupHelper
  private Script context
  private String credentialId
  private List sshHostKeys
  private Boolean useTerraformInitInternalWrapper

  AWSTerraformInitSSHWrapper(AWSTerraformInitWrapper awsTerraformInitWrapper, SSHKeySetupHelper sshKeySetupHelper, Script context, String credentialId, List sshHostKeys, Boolean useTerraformInitInternalWrapper = true) {
    this.awsTerraformInitWrapper = awsTerraformInitWrapper
    this.sshKeySetupHelper = sshKeySetupHelper
    this.context = context
    this.credentialId = credentialId
    this.sshHostKeys = sshHostKeys
    this.useTerraformInitInternalWrapper = useTerraformInitInternalWrapper
  }

  @Override
  EventDataInterface wrap(AbstractEventListener eventListener, EventDataInterface eventData) {
    return doWrap(eventListener, eventData as RunnerEventData)
  }

  EventDataInterface doWrap(AbstractEventListener eventListener, RunnerEventData eventData) {
    this.context.withCredentials([
      this.context.sshUserPrivateKey(
        credentialsId: this.credentialId,
        keyFileVariable: 'CVS_SSH_KEY_FILE',
        passphraseVariable: 'CVS_SSH_PASSPHRASE',
        usernameVariable: 'CVS_SSH_USERNAME')
    ]) {
      String destination = 'ssh${BUILD_NUMBER}'
      this.sshKeySetupHelper.setupPrivateKeyWithEnvVars('CVS_SSH_KEY_FILE', 'CVS_SSH_PASSPHRASE', this.sshHostKeys, destination)

      def mountsBeforeSSH = eventData.getMainEventData().getContainerOptions().getVolumes()

      eventData.getMainEventData().getContainerOptions().setVolumes(
        eventData.getMainEventData().getContainerOptions().getVolumes() + ['$(pwd)/ssh"${BUILD_NUMBER}"/': '/root/.ssh/']
      )

      def result

      if (this.useTerraformInitInternalWrapper) {
        result = this.awsTerraformInitWrapper.wrap(eventListener, eventData)
      } else {
        result = eventListener.run(eventData)
      }

      this.sshKeySetupHelper.cleanupPrivateKeyWithEnvVars(destination)

      eventData.getMainEventData().getContainerOptions().setVolumes(mountsBeforeSSH)

      return result
    }
  }

  @Override
  Boolean support(AbstractEventListener listener) {
    return listener instanceof TerraformInitListener
  }
}
