package com.wildbeavers.listener.wrapper.terraform


import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.terraform.listener.TerraformInitListener
import com.wildbeavers.listener.wrapper.CanWrapListener
import com.wildbeavers.observer.AbstractEventListener
import com.wildbeavers.observer.EventDataInterface

/**
 * Wraps AWS Terraform init calls with proper credentials
 */
class AWSTerraformInitWrapper implements CanWrapListener {
  private Script context
  private String credentialId

  AWSTerraformInitWrapper(Script context, String credentialId) {
    this.context = context
    this.credentialId = credentialId
  }

  @Override
  EventDataInterface wrap(AbstractEventListener eventListener, EventDataInterface eventData) {
    return doWrap(eventListener, eventData as RunnerEventData)
  }

  EventDataInterface doWrap(AbstractEventListener eventListener, RunnerEventData eventData) {
    this.context.withCredentials([
      this.context.usernamePassword(
        credentialsId: this.credentialId,
        usernameVariable: 'AWS_ACCESS_KEY_ID',
        passwordVariable: 'AWS_SECRET_ACCESS_KEY'
      )
    ]) {
      eventData.getMainEventData().getInitOptions().merge(['-backend-config': [
        'access_key=${AWS_ACCESS_KEY_ID}',
        'secret_key=${AWS_SECRET_ACCESS_KEY}',
      ]])

      return eventListener.run(eventData)
    }
  }

  @Override
  Boolean support(AbstractEventListener listener) {
    return listener instanceof TerraformInitListener
  }
}
