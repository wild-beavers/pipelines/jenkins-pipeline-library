package com.wildbeavers.listener.wrapper.terraform

import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.terraform.listener.TerraformDestroyListener
import com.wildbeavers.listener.wrapper.CanWrapListener
import com.wildbeavers.observer.AbstractEventListener
import com.wildbeavers.observer.EventDataInterface

/**
 * Wraps AWS Terraform plan/apply/destroy calls with proper credentials
 */
class AWSTerraformApplyWrapper implements CanWrapListener {
  private Script context
  private String AWSAccessKeyVariableName
  private String AWSSecretKeyVariableName
  private String credentialId

  AWSTerraformApplyWrapper(Script context, String AWSAccessKeyVariableName, String AWSSecretKeyVariableName, String credentialId) {
    this.context = context
    this.AWSAccessKeyVariableName = AWSAccessKeyVariableName
    this.AWSSecretKeyVariableName = AWSSecretKeyVariableName
    this.credentialId = credentialId
  }

  @Override
  EventDataInterface wrap(AbstractEventListener eventListener, EventDataInterface eventData) {
    return doWrap(eventListener, eventData as RunnerEventData)
  }

  EventDataInterface doWrap(AbstractEventListener eventListener, RunnerEventData eventData) {
    this.context.withCredentials([
      this.context.usernamePassword(
        credentialsId: this.credentialId,
        usernameVariable: 'TERRAFORM_AWS_ACCESS_KEY_ID',
        passwordVariable: 'TERRAFORM_AWS_SECRET_ACCESS_KEY'
      )
    ]) {
      eventData.getMainEventData().getPlanOptions().merge(['-var': [
        this.AWSAccessKeyVariableName + '=${TERRAFORM_AWS_ACCESS_KEY_ID}',
        this.AWSSecretKeyVariableName + '=${TERRAFORM_AWS_SECRET_ACCESS_KEY}',
      ]])
      eventData.getMainEventData().getDestroyOptions().merge(['-var': [
        this.AWSAccessKeyVariableName + '=${TERRAFORM_AWS_ACCESS_KEY_ID}',
        this.AWSSecretKeyVariableName + '=${TERRAFORM_AWS_SECRET_ACCESS_KEY}',
      ]])

      return eventListener.run(eventData)
    }
  }

  @Override
  Boolean support(AbstractEventListener listener) {
    return listener instanceof AWSTerraformApplyWrapper || listener instanceof TerraformDestroyListener
  }
}
