package com.wildbeavers.listener.wrapper.standard

import com.cloudbees.groovy.cps.SerializableScript
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.SSHKeySetupHelper
import com.wildbeavers.listener.AutoTaggerListener
import com.wildbeavers.listener.ScmInfoRefresherListener
import com.wildbeavers.listener.wrapper.CanWrapListener
import com.wildbeavers.observer.AbstractEventListener
import com.wildbeavers.observer.EventDataInterface

/**
 * Wraps listeners that need to access distant SCM requiring authentication.
 * When using SSH, this wrapper sets the GIT_SSH_COMMAND, available in the wrapped listeners.
 * When using SSH also, the eventData is updated containing that same ssh command, available in the wrapped listeners.
 */
class ScmCredentialWrapper implements CanWrapListener {
  private SSHKeySetupHelper sshKeySetupHelper
  private SerializableScript context
  private List sshHostKeys

  ScmCredentialWrapper(SSHKeySetupHelper sshKeySetupHelper, SerializableScript context, List sshHostKeys) {
    this.sshKeySetupHelper = sshKeySetupHelper
    this.context = context
    this.sshHostKeys = sshHostKeys
  }

  @Override
  EventDataInterface wrap(AbstractEventListener eventListener, EventDataInterface eventData) {
    return doWrap(eventListener, eventData as RunnerEventData)
  }

  EventDataInterface doWrap(AbstractEventListener eventListener, RunnerEventData eventData) {
    EventDataInterface result = null

    if (this.isProjectHTTP(eventData)) {
      this.context.withCredentials([
        this.context.gitUsernamePassword(
          credentialsId: this.getCredentialId(eventData),
        )
      ]) {
        result = eventListener.run(eventData)
      }
    } else {
      this.context.withCredentials([
        this.context.sshUserPrivateKey(
          credentialsId: this.getCredentialId(eventData),
          keyFileVariable: 'CVS_SSH_KEY_FILE',
          passphraseVariable: 'CVS_SSH_PASSPHRASE',
          usernameVariable: 'CVS_SSH_USERNAME')
      ]) {
        CharSequence destination = 'ssh_${BUILD_NUMBER}'
        String gitSSHCommand = 'ssh -o UserKnownHostsFile=' + destination + '/known_hosts -o IdentitiesOnly=yes ' + '-i ${CVS_SSH_KEY_FILE}'

        eventData.setScmSshConfigDirectory(destination)

        this.sshKeySetupHelper.setupPrivateKeyWithEnvVars('CVS_SSH_KEY_FILE', 'CVS_SSH_PASSPHRASE', this.sshHostKeys, destination)

        def previousGITSSHCommand = this.context.env.GIT_SSH_COMMAND
        this.context.env.GIT_SSH_COMMAND = gitSSHCommand
        eventData.setGitSSHCommand(gitSSHCommand)

        result = eventListener.run(eventData)

        this.sshKeySetupHelper.cleanupPrivateKeyWithEnvVars(destination)
        this.context.env.GIT_SSH_COMMAND = previousGITSSHCommand
        eventData.setGitSSHCommand('')
      }
    }

    return result
  }

  @Override
  Boolean support(AbstractEventListener listener) {
    return this.isPipelineSupportingAuthentication() && (listener instanceof AutoTaggerListener || listener instanceof ScmInfoRefresherListener)
  }

  Boolean isPipelineSupportingAuthentication() {
    try {
      return this.context.scm.getUserRemoteConfigs() != null && !this.context.scm.getUserRemoteConfigs().isEmpty() && this.context.scm.getUserRemoteConfigs()[0].getCredentialsId() != null && !this.context.scm.getUserRemoteConfigs()[0].getCredentialsId().isEmpty()
    } catch (MissingMethodException ignored) {
      return false
    } catch (MissingPropertyException ignored) {
      return false
    }
  }

  private Boolean isProjectHTTP(RunnerEventData eventData) {
    this.getRepositoryURL(eventData) ==~ /^http.*/
  }

  private CharSequence getRepositoryURL(RunnerEventData eventData) {
    return eventData.getScmAlternativeCheckoutRepositoryURL() ?: this.context.scm.getUserRemoteConfigs()[0].getUrl()
  }

  private CharSequence getCredentialId(RunnerEventData eventData) {
    return eventData.getScmAlternativeCheckoutCredentialID() ?: this.context.scm.getUserRemoteConfigs()[0].getCredentialsId()
  }
}
