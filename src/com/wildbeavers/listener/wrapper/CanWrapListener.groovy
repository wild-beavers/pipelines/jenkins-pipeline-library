package com.wildbeavers.listener.wrapper

import com.wildbeavers.observer.AbstractEventListener
import com.wildbeavers.observer.EventDataInterface

/**
 * Defines what a listener wrapper is.
 */
interface CanWrapListener {
  EventDataInterface wrap(AbstractEventListener eventListener, EventDataInterface eventData)
  Boolean support(AbstractEventListener listener)
}
