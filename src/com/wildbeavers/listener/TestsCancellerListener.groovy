package com.wildbeavers.listener


import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

/**
 * Cancels any Lint, Static Analysis, Unit Tests or Functional Tests events when the project is publishable.
 */
class TestsCancellerListener extends EventListener implements IOCRefreshable {
  private Debugger debugger
  private ScmInfo scmInfo

  TestsCancellerListener(Debugger debugger, ScmInfo scmInfo) {
    this.debugger = debugger
    this.scmInfo = scmInfo
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.getName()) as ScmInfo
  }

  @Override
  ArrayList<String> listenTo() {
    return [
      RunnerEvents.PRE_LINT,
      RunnerEvents.LINT,
      RunnerEvents.POST_LINT,
      RunnerEvents.PRE_STATIC_ANALYSIS,
      RunnerEvents.STATIC_ANALYSIS,
      RunnerEvents.POST_STATIC_ANALYSIS,
      RunnerEvents.PRE_TEST_UNIT,
      RunnerEvents.TEST_UNIT,
      RunnerEvents.POST_TEST_UNIT,
    ]
  }

  @Override
  Boolean shouldRun(EventDataInterface eventData = null) {
    return this.scmInfo.isPublishable() && eventData.getTestsCancellerEnabled()
  }

  /**
   * @param RunnerEvents eventData
   * @return RunnerEvents
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    this.debugger.printDebug('This current revision is publishable. Lint, static analysis and tests are hindered.')

    this.stopPropagation = true
    return eventData
  }

  @Override
  Integer getOrder() {
    return 1
  }
}
