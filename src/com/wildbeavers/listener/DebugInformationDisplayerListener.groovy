package com.wildbeavers.listener

import com.wildbeavers.di.IOC
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

/**
 * Displays some debugging information, to prevent having to display these information multiple at runtime
 */
class DebugInformationDisplayerListener extends EventListener<ControllerEventData> {
  private ExecuteProxy executeProxy
  private Debugger debugger

  DebugInformationDisplayerListener(ExecuteProxy executeProxy, Debugger debugger) {
    this.executeProxy = executeProxy
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.PRE_RUNNER]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    this.debugger.printDebug(IOC.debug())

    this.debugger.printDebug(eventData.getRawConfig())

    this.checkVersion('docker version')
    this.checkVersion('podman version')
    this.checkVersion('java -version')
    this.checkVersion('groovy -version')

    return eventData
  }

  @Override
  Boolean shouldRun(EventDataInterface eventData = null) {
    return this.debugger.debugVarExists()
  }

  private Boolean hasCommandInstalled(String command) {
    try {
      this.executeProxy.execute("which ${command}")
      return true
    } catch (ignored) {
      return false
    }
  }

  private void checkVersion(String commandToExecute) {
    if (this.hasCommandInstalled(commandToExecute.split(' ')[0])) {
      this.executeProxy.execute(commandToExecute)
    }
  }
}
