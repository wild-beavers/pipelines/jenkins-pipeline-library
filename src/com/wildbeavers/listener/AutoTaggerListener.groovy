package com.wildbeavers.listener

import com.cloudbees.groovy.cps.SerializableScript
import com.wildbeavers.auto_tagger.AutoTagger
import com.wildbeavers.data.ScmAutoTagMethod
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.exception.ConfigurationException
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener

/**
 * Auto-tag the current revision locally and remotely, according to user defined ScmAutoTagMethod.
 */
class AutoTaggerListener extends EventListener<RunnerEventData> {
  private AutoTagger autoTagger
  private Debugger debugger
  private SerializableScript context

  AutoTaggerListener(AutoTagger changelogAutoTaggerHelper, Debugger debugger, SerializableScript context) {
    this.autoTagger = changelogAutoTaggerHelper
    this.debugger = debugger
    this.context = context
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.POST_PREPARE]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return eventData.getScmAutoTagEnabled() && this.autoTagger.canAutoTag(eventData.getScmAutoTagChangelogFilePath(), eventData.getScmFloatingTagsEnabled())
  }

  @Override
  ArrayList<CharSequence> explainNoRun(RunnerEventData eventData) {
    return ['This commit is a candidate to be publishable, and therefore could be tagged, but auto-tag eligibility checks failed. For more details, enable DEBUG mode.']
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    switch (eventData.getScmAutoTagMethod()) {
      case ScmAutoTagMethod.TAG_ANNOTATED:
        this.context.lock('auto-tagger') {
          this.autoTagger.autoTag(eventData.getScmAutoTagChangelogFilePath(), eventData.getScmAutoTagLastTagRegex(), eventData.getScmFloatingTagsEnabled())
        }
        break
      default:
        throw new ConfigurationException("For now, auto tagger only support the type: ${ScmAutoTagMethod.TAG_ANNOTATED}. Sorry for the inconvenience. Contributions are welcome.")
    }

    return eventData
  }
}
