package com.wildbeavers.listener

import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.io.Debugger
import com.wildbeavers.observer.EventListener

/**
 * Set Jenkins properties for the current job
 */
class JobPropertiesListener extends EventListener<ControllerEventData> {
  private Script context
  private Debugger debugger

  JobPropertiesListener(Script context, Debugger debugger) {
    this.context = context
    this.debugger = debugger
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.PRE_RUNNER]
  }

  ControllerEventData run(ControllerEventData eventData) {
    List propertiesConfig = [
      this.context.buildDiscarder(
        this.context.logRotator(
          artifactDaysToKeepStr: '',
          artifactNumToKeepStr: '10',
          daysToKeepStr: '360',
          numToKeepStr: '10'
        )
      ),
      this.context.pipelineTriggers(
        [this.context.cron('@weekly')]
      )
    ]

    this.debugger.printDebug('Default properties: ' + propertiesConfig)
    this.debugger.printDebug('User-set properties: ' + eventData.getJobProperties())

    eventData.getJobProperties().eachWithIndex { property, index ->
      if ((property instanceof Set) && (property.containsKey['$class'])) {
        if (!eventData.getJobProperties().any { propertyElement ->
          propertyElement.containsKey('$class') ? propertyElement['$class'] == element['$class'] : false
        }) {
          propertiesConfig += property.clone()
        }
      } else {
        propertiesConfig.each { defaultProperty ->
          if (defaultProperty.getSymbol() == property.getSymbol()) {
            propertiesConfig.remove(defaultProperty)
          }
        }

        propertiesConfig += property
      }
    }

    this.debugger.printDebug('Computed properties: ' + propertiesConfig)
    this.context.properties(propertiesConfig)

    return eventData
  }
}
