package com.wildbeavers.listener


import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

import java.util.regex.Pattern

/**
 * Validate tagCheck user configuration.
 */
class TagCheckerValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator

  TagCheckerValidationListener(DataValidator dataValidator) {
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  Boolean shouldRun(ControllerEventData eventData) {
    return eventData.getRawConfig().get('tagCheck_enabled') != false
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    def config = eventData.getRawConfig()

    eventData.getRunnerEventData().setTagCheckEnabled(
      this.dataValidator.validate(config, 'tagCheck_enabled', Boolean, eventData.getRunnerEventData().getTagCheckEnabled())
    )
    eventData.getRunnerEventData().setTagCheckOnlyMainBranch(
      this.dataValidator.validate(config, 'tagCheck_onlyMainBranch', Boolean, eventData.getRunnerEventData().getTagCheckOnlyMainBranch())
    )
    eventData.getRunnerEventData().setTagCheckRegex(
      this.dataValidator.validate(config, 'tagCheck_regex', Pattern, eventData.getRunnerEventData().getTagCheckRegex())
    )

    return eventData
  }

  @Override
  Integer getOrder() {
    return 100
  }
}
