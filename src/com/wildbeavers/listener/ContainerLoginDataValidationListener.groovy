package com.wildbeavers.listener

import com.wildbeavers.container.data.ContainerRegistryOption
import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for container login and fill data object.
 */
class ContainerLoginDataValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator
  private GenericFactory<ContainerLoginData> containerLoginDataGenericFactory
  private GenericFactory<ContainerRegistryOption> containerRegistryOptionDataGenericFactory

  ContainerLoginDataValidationListener(DataValidator dataValidator, GenericFactory<ContainerLoginData> containerLoginDataGenericFactory, GenericFactory<ContainerRegistryOption> containerRegistryOptionDataGenericFactory) {
    this.dataValidator = dataValidator
    this.containerLoginDataGenericFactory = containerLoginDataGenericFactory
    this.containerRegistryOptionDataGenericFactory = containerRegistryOptionDataGenericFactory
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    ContainerLoginData containerLoginData = this.containerLoginDataGenericFactory.instantiate()

    containerLoginData.setEnabled(
      this.dataValidator.validate(config, 'containerLogin_enabled', Boolean, containerLoginData.isEnabled())
    )
    containerLoginData.setTool(
      this.dataValidator.validate(config, 'containerLogin_tool', ContainerTool, containerLoginData.getTool())
    )

    // These below are not correctly validated
    containerLoginData.setAuthfile(
      this.dataValidator.validate(config, 'containerLogin_authfile', CharSequence, containerLoginData.getAuthfile())
    )

    for (registryConfig in config.containerLogin_containerRegistries) {
      ContainerRegistryOption containerRegistryOption = this.containerRegistryOptionDataGenericFactory.instantiate()

      // These below are not correctly validated
      containerRegistryOption.setCredentialId(
        this.dataValidator.validate(registryConfig, 'credentialId', CharSequence, containerRegistryOption.getCredentialId())
      )
      containerRegistryOption.setCredentialProcessContainerRunCommandArguments(
        this.dataValidator.validate(registryConfig, 'credentialProcessContainerRunCommandArguments', CharSequence, containerRegistryOption.getCredentialProcessContainerRunCommandArguments())
      )
      containerRegistryOption.setCredentialProcessUsername(
        this.dataValidator.validate(registryConfig, 'credentialProcessUsername', CharSequence, containerRegistryOption.getCredentialProcessUsername())
      )
      containerRegistryOption.setFqdn(
        this.dataValidator.validate(registryConfig, 'fqdn', CharSequence, containerRegistryOption.getFqdn())
      )

      if (registryConfig.credentialProcessContainerOptions) {
        containerRegistryOption.getContainerOptions().setFqdn(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'fqdn', CharSequence, containerRegistryOption.getContainerOptions().getFqdn())
        )
        containerRegistryOption.getContainerOptions().setImageName(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'imageName', CharSequence, containerRegistryOption.getContainerOptions().getImageName())
        )
        containerRegistryOption.getContainerOptions().setImageTag(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'imageTag', CharSequence, containerRegistryOption.getContainerOptions().getImageTag())
        )
        containerRegistryOption.getContainerOptions().setEnvironmentVariables(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'environmentVariables', Map, containerRegistryOption.getContainerOptions().getEnvironmentVariables())
        )
        containerRegistryOption.getContainerOptions().setVolumes(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'volumes', Map, containerRegistryOption.getContainerOptions().getVolumes())
        )
        containerRegistryOption.getContainerOptions().setPrivileged(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'privileged', Boolean, containerRegistryOption.getContainerOptions().getPrivileged())
        )
        containerRegistryOption.getContainerOptions().setTool(
          this.dataValidator.validate(registryConfig.credentialProcessContainerOptions, 'tool', ContainerTool, containerRegistryOption.getContainerOptions().getTool())
        )
      }

      containerLoginData.addContainerRegistryOption(containerRegistryOption)
    }

    eventData.getRunnerEventData().setContainerLoginData(containerLoginData)

    return eventData
  }
}
