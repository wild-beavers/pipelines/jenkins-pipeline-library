package com.wildbeavers.listener

import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.OptionString
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.observer.EventListener

/**
 * Logins to one or multiple container registries.
 */
class ContainerLoginListener extends EventListener<RunnerEventData> {
  private Script context
  private ExecuteProxy executeProxy
  private ContainerRunner containerRunner

  ContainerLoginListener(Script context, ExecuteProxy executeProxy, ContainerRunner containerRunner) {
    this.context = context
    this.executeProxy = executeProxy
    this.containerRunner = containerRunner
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.PRE_PREPARE]
  }

  /**
   *
   * @param RunnerEventData eventData
   * @return
   */
  @Override
  Boolean shouldRun(RunnerEventData eventData = null) {
    return eventData.getContainerLoginData().isEnabled() && eventData.getContainerLoginData().getContainerRegistryOptions().size() != 0
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    for (containerRegistryOption in eventData.getContainerLoginData().getContainerRegistryOptions()) {
      if (containerRegistryOption.hasCredentialId()) {
        this.context.withCredentials([
          this.context.usernamePassword(
            credentialsId: containerRegistryOption.getCredentialId(),
            passwordVariable: 'CONTAINER_REGISTRY_PASSWORD',
            usernameVariable: 'CONTAINER_REGISTRY_USERNAME'
          )
        ]) {
          this.doLogin(
            eventData.getContainerLoginData(),
            '$CONTAINER_REGISTRY_USERNAME',
            '$CONTAINER_REGISTRY_PASSWORD',
            containerRegistryOption.getFqdn(),
          )
        }
      } else {
        containerRegistryOption.getCredentialProcessContainerOptions().setQuiet(true)

        CommandResult commandResult = this.containerRunner.run(
          containerRegistryOption.getCredentialProcessContainerOptions(),
          containerRegistryOption.getCredentialProcessContainerRunCommandArguments()
        )

        this.doLogin(
          eventData.getContainerLoginData(),
          containerRegistryOption.getCredentialProcessUsername(),
          commandResult.getStdout(),
          containerRegistryOption.getFqdn(),
        )
      }
    }

    return eventData
  }

  private doLogin(ContainerLoginData containerLoginData, String username, String password, String fqdn) {
    OptionString optionString = new OptionString()
    optionString.addOption('--username', username)
    optionString.addOption('--password-stdin')
    if (containerLoginData.getAuthfile()) {
      optionString.addOption('--authfile', containerLoginData.getAuthfile())
    }
    optionString.addOption(fqdn)

    // Password cannot be properly escaped because of executeProxy
    // This is known to be dangerous in an uncontrolled system
    this.executeProxy.basicExecute("echo \"${password}\" | ${containerLoginData.getTool()} login ${optionString.toString()}", false, false)
  }
}
