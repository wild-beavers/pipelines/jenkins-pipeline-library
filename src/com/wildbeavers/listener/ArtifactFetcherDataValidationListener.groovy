package com.wildbeavers.listener

import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.data_validation.type.Version
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for artifact fetching and fill data object.
 */
class ArtifactFetcherDataValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator
  private GenericFactory<ArtifactFetchingData> artifactFetchingDataGenericFactory
  private GenericFactory<ArtifactData> artifactDataGenericFactory

  ArtifactFetcherDataValidationListener(DataValidator dataValidator, GenericFactory<ArtifactFetchingData> artifactFetchingDataGenericFactory, GenericFactory<ArtifactData> artifactDataGenericFactory) {
    this.dataValidator = dataValidator
    this.artifactFetchingDataGenericFactory = artifactFetchingDataGenericFactory
    this.artifactDataGenericFactory = artifactDataGenericFactory
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    ArtifactFetchingData artifactFetchingData = this.artifactFetchingDataGenericFactory.instantiate()

    config.artifactFetching_artifacts = this.dataValidator.validate(config, 'artifactFetching_artifacts', List, [])

    for (artifactOption in config.artifactFetching_artifacts) {
      ArtifactData artifactData = this.artifactDataGenericFactory.instantiate()

      artifactData.setUrl(
        this.dataValidator.validate(artifactOption, 'url', ShallowURL, artifactData.getUrl(), ['artifactFetching_artifacts'])
      )
      artifactData.setVersion(
        this.dataValidator.validate(artifactOption, 'version', Version, artifactData.getVersion(), ['artifactFetching_artifacts'])
      )
      artifactData.setAccessorName(
        this.dataValidator.validate(artifactOption, 'accessorName', CharSequence, artifactData.getAccessorName(), ['artifactFetching_artifacts'])
      )

      artifactFetchingData.addArtifactData(artifactData)
    }

    eventData.getRunnerEventData().setArtifactFetchingData(artifactFetchingData)

    return eventData
  }
}
