package com.wildbeavers.listener

import com.wildbeavers.data.ScmAutoTagMethod
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.data_validation.type.GitCloneURL
import com.wildbeavers.data_validation.type.Version
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

import java.util.regex.Pattern

/**
 * Validate user configuration for SCM data.
 */
class ScmDataValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator

  ScmDataValidationListener(DataValidator dataValidator) {
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    eventData.getRunnerEventData().setScmAlternativeCheckoutCredentialID(
      this.dataValidator.validateOrNull(config, 'scm_alternativeCheckoutCredentialID', CharSequence, eventData.getRunnerEventData().getScmAlternativeCheckoutCredentialID())
    )
    eventData.getRunnerEventData().setScmAlternativeCheckoutDirectory(
      this.dataValidator.validateOrNull(config, 'scm_alternativeCheckoutDirectory', ForwardRelativeLinuxFullPath, eventData.getRunnerEventData().getScmAlternativeCheckoutDirectory())
    )
    eventData.getRunnerEventData().setScmAlternativeCheckoutRepositoryURL(
      this.dataValidator.validateOrNull(config, 'scm_alternativeCheckoutRepositoryURL', GitCloneURL, eventData.getRunnerEventData().getScmAlternativeCheckoutRepositoryURL())
    )
    eventData.getRunnerEventData().setScmAlternativeCheckoutTag(
      this.dataValidator.validateOrNull(config, 'scm_alternativeCheckoutTag', Version, eventData.getRunnerEventData().getScmAlternativeCheckoutTag())
    )
    eventData.getRunnerEventData().setScmFloatingTagsEnabled(
      this.dataValidator.validate(config, 'scm_floatingTagsEnabled', Boolean, eventData.getRunnerEventData().getScmFloatingTagsEnabled())
    )
    eventData.getRunnerEventData().setScmAutoTagEnabled(
      this.dataValidator.validate(config, 'scm_autoTagEnabled', Boolean, eventData.getRunnerEventData().getScmAutoTagEnabled())
    )
    eventData.getRunnerEventData().setScmAutoTagMethod(
      this.dataValidator.validate(config, 'scm_autoTagMethod', ScmAutoTagMethod, eventData.getRunnerEventData().getScmAutoTagMethod())
    )
    eventData.getRunnerEventData().setScmAutoTagChangelogFilePath(
      this.dataValidator.validate(config, 'scm_autoTagChangelogFilePath', ForwardRelativeLinuxFullPath, eventData.getRunnerEventData().getScmAutoTagChangelogFilePath())
    )
    eventData.getRunnerEventData().setScmAutoTagLastTagRegex(
      this.dataValidator.validate(config, 'scm_autoTagLastTagRegex', Pattern, eventData.getRunnerEventData().getScmAutoTagLastTagRegex())
    )

    return eventData
  }
}
