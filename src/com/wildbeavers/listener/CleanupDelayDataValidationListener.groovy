package com.wildbeavers.listener


import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for cleanup delay.
 */
class CleanupDelayDataValidationListener extends EventListener<ControllerEventData> {
  private DataValidator dataValidator

  CleanupDelayDataValidationListener(DataValidator dataValidator) {
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    Map config = eventData.getRawConfig()

    eventData.getRunnerEventData().setCleanupDelayEnabled(
      this.dataValidator.validate(config, 'cleanupDelay_enabled', Boolean, eventData.getRunnerEventData().getCleanupDelayEnabled())
    )
    eventData.getRunnerEventData().setCleanupDelayTime(
      this.dataValidator.validate(config, 'cleanupDelay_time', Integer, eventData.getRunnerEventData().getCleanupDelayTime())
    )
    eventData.getRunnerEventData().setCleanupDelayMessage(
      this.dataValidator.validate(config, 'cleanupDelay_message', CharSequence, eventData.getRunnerEventData().getCleanupDelayMessage())
    )
    eventData.getRunnerEventData().setCleanupDelayHeader(
      this.dataValidator.validate(config, 'cleanupDelay_header', CharSequence, eventData.getRunnerEventData().getCleanupDelayHeader())
    )
    eventData.getRunnerEventData().setCleanupDelayButtonLabel(
      this.dataValidator.validate(config, 'cleanupDelay_buttonLabel', CharSequence, eventData.getRunnerEventData().getCleanupDelayButtonLabel())
    )
    eventData.getRunnerEventData().setCleanupDelayContinueIfExpired(
      this.dataValidator.validate(config, 'cleanupDelay_continueIfExpired', Boolean, eventData.getRunnerEventData().getCleanupDelayContinueIfExpired())
    )

    return eventData
  }
}
