package com.wildbeavers.listener

import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.data.pre_commit.PreCommitData
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

/**
 * Validate user configuration for pre-commit and fill data object.
 */
class PreCommitDataValidationListener extends EventListener {
  private GenericFactory<PreCommitData> preCommitDataGenericFactory
  private DataValidator dataValidator

  PreCommitDataValidationListener(GenericFactory<PreCommitData> preCommitDataGenericFactory, DataValidator dataValidator) {
    this.preCommitDataGenericFactory = preCommitDataGenericFactory
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    Map config = eventData.getRawConfig()

    PreCommitData preCommitData = this.preCommitDataGenericFactory.instantiate()

    preCommitData.setEnabled(
      this.dataValidator.validate(config, 'preCommit_enabled', Boolean, preCommitData.getEnabled())
    )
    preCommitData.setRunCommandArguments(
      this.dataValidator.validate(config, 'preCommit_runCommandArguments', CharSequence, preCommitData.getRunCommandArguments())
    )
    preCommitData.getContainerOptions().setFqdn(
      this.dataValidator.validate(config, 'preCommit_containerFqdn', CharSequence, preCommitData.getContainerOptions().getFqdn())
    )
    preCommitData.getContainerOptions().setImageName(
      this.dataValidator.validate(config, 'preCommit_containerImageName', CharSequence, preCommitData.getContainerOptions().getImageName())
    )
    preCommitData.getContainerOptions().setImageTag(
      this.dataValidator.validate(config, 'preCommit_containerImageTag', CharSequence, preCommitData.getContainerOptions().getImageTag())
    )
    preCommitData.getContainerOptions().setEnvironmentVariables(
      this.dataValidator.validate(config, 'preCommit_containerEnvironmentVariables', Map, preCommitData.getContainerOptions().getEnvironmentVariables())
    )
    preCommitData.getContainerOptions().setVolumes(
      this.dataValidator.validate(config, 'preCommit_containerVolumes', Map, preCommitData.getContainerOptions().getVolumes())
    )
    preCommitData.getContainerOptions().setPrivileged(
      this.dataValidator.validate(config, 'preCommit_containerPrivileged', Boolean, preCommitData.getContainerOptions().getPrivileged())
    )
    preCommitData.getContainerOptions().setTool(
      this.dataValidator.validate(config, 'preCommit_containerTool', ContainerTool, preCommitData.getContainerOptions().getTool())
    )
    preCommitData.getContainerOptions().setUserns(
      this.dataValidator.validate(config, 'preCommit_containerUserNS', CharSequence, preCommitData.getContainerOptions().getUserns())
    )
    preCommitData.getContainerOptions().setFallbackCommand(
      this.dataValidator.validate(config, 'containerFallbackCommand', CharSequence, preCommitData.getContainerOptions().getFallbackCommand())
    )

    if (config.containerLogin_enabled && (config.containerLogin_containerRegistries?.size()) as Boolean) {
      String authfile = (config.containerLogin_authfile?.trim()) as Boolean ? config.containerLogin_authfile : ContainerDockerEventData.DEFAULT_AUTHFILE
      preCommitData.getContainerOptions().addVolumes([(authfile): this.dataValidator.validate(config, 'preCommit_containerConfigJsonDestinationPath', CharSequence, PreCommitData.DEFAULT_CONTAINER_CONFIG_JSON_DESTINATION_PATH)])
    }

    eventData.getRunnerEventData().setPreCommitData(preCommitData)

    return eventData
  }
}
