package com.wildbeavers.listener


import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.observer.EventListener

/**
 * Generic linter listener. Runs multiple linters based on user configuration.
 */
class LinterListener extends EventListener<RunnerEventData> {
  private ContainerRunner containerRunner

  LinterListener(ContainerRunner containerRunner) {
    this.containerRunner = containerRunner
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.LINT]
  }

  Boolean shouldRun(RunnerEventData eventData) {
    return eventData.isLinterEnabled() && eventData.getLinterData() != null && !eventData.getLinterData().isEmpty()
  }

  RunnerEventData run(RunnerEventData eventData) {
    for (linterData in eventData.getLinterData()) {
      this.containerRunner.run(linterData.getContainerOptions(), linterData.getCommandArguments())
    }

    return eventData
  }
}
