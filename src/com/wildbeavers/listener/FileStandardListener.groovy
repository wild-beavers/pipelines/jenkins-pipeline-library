package com.wildbeavers.listener

import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.proxy.FileProxy
import com.wildbeavers.observer.EventListener

/**
 * Check that the current project contains the correct expected files.
 */
class FileStandardListener extends EventListener<RunnerEventData> {
  FileProxy fileProxy

  FileStandardListener(FileProxy fileProxy) {
    this.fileProxy = fileProxy
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.STATIC_ANALYSIS]
  }

  @Override
  Boolean shouldRun(RunnerEventData eventData) {
    return eventData.isFileStandardEnabled() &&
      (eventData.getFileStandardData().getForbiddenFiles().size() > 1 || eventData.getFileStandardData().getMandatoryFiles().size() > 1)
  }

  @Override
  ArrayList<CharSequence> explainNoRun(RunnerEventData eventData) {
    ArrayList<CharSequence> explanations = []
    if (!eventData.getFileStandardData().getForbiddenFiles().size() && !eventData.getFileStandardData().getMandatoryFiles().size()) {
      explanations.add('No mandatory nor forbidden files were given.')
    }
    if (!eventData.isFileStandardEnabled()) {
      explanations.add('FileStandard step was explicitly disabled.')
    }

    return explanations
  }

  /**
   * @param RunnerEventData eventData
   * @return RunnerEventData
   */
  RunnerEventData run(RunnerEventData eventData = null) {
    List<CharSequence> errors = []

    eventData.getFileStandardData().getMandatoryFiles().each {
      if (!fileProxy.exists(it)) {
        errors.add("A file “${it}” is mandatory for this kind of project but it was not found.")
      }
    }

    eventData.getFileStandardData().getForbiddenFiles().each {
      if (fileProxy.exists(it)) {
        errors.add("The file “${it}” is forbidden for this kind of project but it was found.")
      }
    }

    if (errors.size() != 0) {
      throw new RuntimeException("FileStandard validations failed:\n${errors.join("\n")}")
    }

    return eventData
  }
}
