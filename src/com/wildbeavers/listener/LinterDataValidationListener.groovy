package com.wildbeavers.listener

import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

/**
 * Validate linter user configuration.
 */
class LinterDataValidationListener extends EventListener<ControllerEventData> {
  private GenericFactory<LinterData> linterDataFactory
  private DataValidator dataValidator

  LinterDataValidationListener(GenericFactory<LinterData> linterDataFactory, DataValidator dataValidator) {
    this.linterDataFactory = linterDataFactory
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  Boolean shouldRun(ControllerEventData eventData) {
    return eventData.getRawConfig().get('linter_enabled')
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData = null) {
    def config = eventData.getRawConfig()

    eventData.getRunnerEventData().setLinterEnabled(
      this.dataValidator.validate(config, 'linter_enabled', Boolean, eventData.getRunnerEventData().isLinterEnabled())
    )

    List data = this.dataValidator.validate(config, 'linter_data', List, eventData.getRunnerEventData().getLinterData())

    for (rawLinterData in data) {
      LinterData linterData = this.linterDataFactory.instantiate()

      linterData.getContainerOptions().setTool(
        this.dataValidator.validate(rawLinterData, 'containerTool', ContainerTool, linterData.getContainerOptions().getTool(), ['linter_data'])
      )
      linterData.getContainerOptions().setPrivileged(
        this.dataValidator.validate(rawLinterData, 'containerPrivileged', Boolean, linterData.getContainerOptions().getPrivileged(), ['linter_data'])
      )

      // These below are not correctly validated
      linterData.getContainerOptions().setFqdn(
        this.dataValidator.validate(rawLinterData, 'containerFqdn', String, linterData.getContainerOptions().getFqdn(), ['linter_data'])
      )
      linterData.getContainerOptions().setImageName(
        this.dataValidator.validate(rawLinterData, 'containerImageName', String, linterData.getContainerOptions().getImageName(), ['linter_data'])
      )
      linterData.getContainerOptions().setImageTag(
        this.dataValidator.validate(rawLinterData, 'containerImageTag', String, linterData.getContainerOptions().getImageTag(), ['linter_data'])
      )
      linterData.getContainerOptions().setVolumes(
        this.dataValidator.validate(rawLinterData, 'containerVolumes', Map, linterData.getContainerOptions().getVolumes(), ['linter_data'])
      )
      linterData.getContainerOptions().setEnvironmentVariables(
        this.dataValidator.validate(rawLinterData, 'containerEnvironmentVariables', Map, linterData.getContainerOptions().getEnvironmentVariables(), ['linter_data'])
      )
      linterData.getContainerOptions().setNetwork(
        this.dataValidator.validate(rawLinterData, 'containerNetwork', String, linterData.getContainerOptions().getNetwork(), ['linter_data'])
      )
      linterData.getContainerOptions().setEntrypoint(
        this.dataValidator.validate(rawLinterData, 'containerEntrypoint', String, linterData.getContainerOptions().getEntrypoint(), ['linter_data'])
      )
      linterData.getContainerOptions().setUserns(
        this.dataValidator.validate(rawLinterData, 'containerUserns', String, linterData.getContainerOptions().getUserns(), ['linter_data'])
      )
      linterData.getContainerOptions().setFallbackCommand(
        this.dataValidator.validate(rawLinterData, 'containerFallbackCommand', String, linterData.getContainerOptions().getFallbackCommand(), ['linter_data'])
      )
      linterData.setCommandArguments(
        this.dataValidator.validate(rawLinterData, 'commandArguments', String, linterData.getCommandArguments(), ['linter_data'])
      )

      eventData.getRunnerEventData().getLinterData().add(linterData)
    }

    return eventData
  }

  @Override
  Integer getOrder() {
    return 100
  }
}
