package com.wildbeavers.butane.event_data

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.observer.EventDataInterface

class ButaneEventData implements EventDataInterface, HasContainerOptions {
  static final DEFAULT_CONTAINER_TOOL = 'docker'
  static final DEFAULT_CONTAINER_FQDN = 'quay.io/coreos'
  static final DEFAULT_CONTAINER_IMAGE_NAME = 'butane'
  static final DEFAULT_CONTAINER_IMAGE_TAG = 'release'
  static final DEFAULT_CONTAINER_FALLBACK_COMMAND = 'butane'

  private ContainerOptions containerOptions

  ButaneEventData(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }

  ContainerOptions getContainerOptions() {
    return containerOptions
  }

  void setContainerOptions(ContainerOptions dockerOptions) {
    this.containerOptions = dockerOptions
  }
}
