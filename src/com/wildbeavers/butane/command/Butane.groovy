package com.wildbeavers.butane.command

import com.wildbeavers.command.AbstractCommand
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run butane commands.
 */
class Butane extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'butane'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return ' '
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [
      (COMMAND_TARGET): this.argumentCommandTarget,
      '-h'            : this.argumentHelp,
      '--help'        : this.argumentHelp,
      '-d'            : this.argumentFilesDir,
      '--files-dir'   : this.argumentFilesDir,
      '-o'            : this.argumentOutput,
      '--output'      : this.argumentOutput,
      '-p'            : this.argumentPretty,
      '--pretty'      : this.argumentPretty,
      '-r'            : this.argumentRaw,
      '--raw'         : this.argumentRaw,
      '-s'            : this.argumentStrict,
      '--strict'      : this.argumentStrict,
      '-v'            : this.argumentVersion,
      '--version'     : this.argumentVersion,
    ]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    ['': [:]]
  }

  private argumentCommandTarget = [
    type       : CharSequence,
    default    : '',
    description: 'Command target.',
  ]
  private argumentHelp = [
    type       : Boolean,
    default    : null,
    description: 'Show usage and exit.',
  ]
  private argumentFilesDir = [
    type       : CharSequence,
    default    : '',
    description: 'Allow embedding local files from this directory.',
  ]
  private argumentOutput = [
    type       : CharSequence,
    default    : '',
    description: 'Write to output file instead of stdout.',
  ]
  private argumentPretty = [
    type       : Boolean,
    default    : null,
    description: 'Output formatted json.',
  ]
  private argumentRaw = [
    type       : Boolean,
    default    : null,
    description: 'Never wrap in a MachineConfig; force Ignition output.',
  ]
  private argumentStrict = [
    type       : Boolean,
    default    : null,
    description: 'Fail on any warning.',
  ]
  private argumentVersion = [
    type       : Boolean,
    default    : null,
    description: 'Print the version and exit.',
  ]

  Butane(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }
}
