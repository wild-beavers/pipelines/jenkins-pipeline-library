package com.wildbeavers.butane.listener

import com.wildbeavers.butane.event_data.ButaneEventData
import com.wildbeavers.container.data_validation.type.ContainerTool
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

class ButaneDataValidationListener extends EventListener<ControllerEventData> {
  public static final DEFAULT_MANDATORY_FILES = [
    'CHANGELOG.md',
    '.pre-commit-config.yaml',
    '.gitignore',
    ButaneToIgnitionListener.BUTANE_FILE,
  ]
  public static final DEFAULT_FORBIDDEN_FILES = [
    ButaneToIgnitionListener.IGNITION_FILE
  ]

  private GenericFactory<ButaneEventData> butaneEventDataGenericFactory
  private DataValidator dataValidator

  ButaneDataValidationListener(GenericFactory<ButaneEventData> butaneEventDataGenericFactory, DataValidator dataValidator) {
    this.butaneEventDataGenericFactory = butaneEventDataGenericFactory
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData) {
    Map config = eventData.getRawConfig()

    ButaneEventData butaneEventData = this.butaneEventDataGenericFactory.instantiate()

    butaneEventData.getContainerOptions().setTool(
      this.dataValidator.validate(config, 'butane_containerTool', ContainerTool, butaneEventData.getContainerOptions().getTool())
    )
    butaneEventData.getContainerOptions().setImageName(
      this.dataValidator.validate(config, 'butane_containerImageName', String, butaneEventData.getContainerOptions().getImageName())
    )
    butaneEventData.getContainerOptions().setImageTag(
      this.dataValidator.validate(config, 'butane_containerImageTag', String, butaneEventData.getContainerOptions().getImageTag())
    )
    butaneEventData.getContainerOptions().setFqdn(
      this.dataValidator.validate(config, 'butane_containerFqdn', String, butaneEventData.getContainerOptions().getFqdn())
    )
    butaneEventData.getContainerOptions().setVolumes(
      this.dataValidator.validate(config, 'butane_containerVolumes', Map, butaneEventData.getContainerOptions().getVolumes())
    )
    butaneEventData.getContainerOptions().setEnvironmentVariables(
      this.dataValidator.validate(config, 'butane_containerEnvironmentVariables', Map, butaneEventData.getContainerOptions().getEnvironmentVariables())
    )
    butaneEventData.getContainerOptions().setNetwork(
      this.dataValidator.validate(config, 'butane_containerNetwork', String, butaneEventData.getContainerOptions().getNetwork())
    )
    butaneEventData.getContainerOptions().setUserns(
      this.dataValidator.validate(config, 'butane_containerUserns', String, butaneEventData.getContainerOptions().getUserns())
    )
    butaneEventData.getContainerOptions().setPrivileged(
      this.dataValidator.validate(config, 'butane_containerPrivileged', Boolean, butaneEventData.getContainerOptions().getPrivileged())
    )

    if (!eventData.getRunnerEventData().isFileStandardIgnoreDefault()) {
      for (mandatoryFile in DEFAULT_MANDATORY_FILES) {
        eventData.getRunnerEventData().getFileStandardData().addMandatoryFile(
          this.dataValidator.validate(mandatoryFile, ForwardRelativeLinuxFullPath)
        )
      }
      for (forbiddenFile in DEFAULT_FORBIDDEN_FILES) {
        eventData.getRunnerEventData().getFileStandardData().addForbiddenFile(
          this.dataValidator.validate(forbiddenFile, ForwardRelativeLinuxFullPath)
        )
      }
    }

    eventData.getRunnerEventData().setMainEventData(butaneEventData)

    return eventData
  }
}
