package com.wildbeavers.butane.listener

import com.wildbeavers.butane.command.Butane
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventListener

import static com.wildbeavers.command.AbstractCommand.COMMAND_TARGET

class ButaneToIgnitionListener extends EventListener {
  final static BUTANE_FILE = "main.yaml"
  final static IGNITION_FILE = "main.ign"

  private Script context
  private Butane butane

  ButaneToIgnitionListener(Script context, Butane butane) {
    this.context = context
    this.butane = butane
  }

  @Override
  ArrayList<String> listenTo() {
    return [RunnerEvents.BUILD]
  }

  /**
   * @param RunnerEvents eventData
   * @return RunnerEvents
   */
  EventDataInterface run(EventDataInterface eventData = null) {
    return this.doRun(eventData as RunnerEventData)
  }

  private RunnerEventData doRun(RunnerEventData eventData) {
    this.butane.run(
      '',
      ['--pretty': true, '--strict': true, '-o': IGNITION_FILE, (COMMAND_TARGET): "${BUTANE_FILE}"],
      eventData.getMainEventData()
    )

    this.context.archiveArtifacts(
      artifacts: BUTANE_FILE
    )

    this.context.archiveArtifacts(
      artifacts: IGNITION_FILE
    )

    return eventData
  }
}
