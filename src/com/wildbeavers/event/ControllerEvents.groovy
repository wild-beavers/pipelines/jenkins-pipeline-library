package com.wildbeavers.event

class ControllerEvents {
  public final static PRE_DATA_VALIDATION = 'preDataValidation'
  public final static DATA_VALIDATION = 'dataValidation'
  public final static POST_DATA_VALIDATION = 'postDataValidation'

  public final static PRE_RUNNER = 'preRunner'
  public final static POST_RUNNER = 'postRunner'
}
