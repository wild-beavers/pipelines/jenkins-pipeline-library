package com.wildbeavers.event

class RunnerEvents {
  public final static PRE_PREPARE = 'prePrepare'
  public final static PREPARE = 'prepare'
  public final static POST_PREPARE = 'postPrepare'

  public final static PRE_LINT = 'preLint'
  public final static LINT = 'lint'
  public final static POST_LINT = 'postLint'

  public final static PRE_STATIC_ANALYSIS = 'preStaticAnalysis'
  public final static STATIC_ANALYSIS = 'staticAnalysis'
  public final static POST_STATIC_ANALYSIS = 'postStaticAnalysis'

  public final static PRE_COMPILE = 'preCompile'
  public final static COMPILE = 'compile'
  public final static POST_COMPILE = 'postCompile'

  public final static PRE_TEST_UNIT = 'preTestUnit'
  public final static TEST_UNIT = 'testUnit'
  public final static POST_TEST_UNIT = 'postTestUnit'

  public final static PRE_TEST_FUNCTIONAL = 'preTestFunctional'
  public final static TEST_FUNCTIONAL = 'testFunctional'
  public final static POST_TEST_FUNCTIONAL = 'postTestFunctional'

  public final static PRE_BUILD = 'preBuild'
  public final static BUILD = 'build'
  public final static POST_BUILD = 'postBuild'

  public final static PRE_DEPLOY = 'preDeploy'
  public final static DEPLOY = 'deploy'
  public final static POST_DEPLOY = 'postDeploy'

  public final static PRE_TEST_INTEGRATION = 'preTestIntegration'
  public final static TEST_INTEGRATION = 'testIntegration'
  public final static POST_TEST_INTEGRATION = 'postTestIntegration'

  public final static PRE_PUBLISH = 'prePublish'
  public final static PUBLISH = 'publish'
  public final static POST_PUBLISH = 'postPublish'

  public final static PRE_RELEASE = 'preRelease'
  public final static RELEASE = 'release'
  public final static POST_RELEASE = 'postRelease'

  public final static PRE_CLEANUP = 'preCleanup'
  public final static CLEANUP = 'cleanup'
  public final static POST_CLEANUP = 'postCleanup'
}
