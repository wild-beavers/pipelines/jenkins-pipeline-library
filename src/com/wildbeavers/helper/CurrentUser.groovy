package com.wildbeavers.helper

/**
 * Provider current user information.
 */
class CurrentUser {
  private Script context

  CurrentUser(Script context) {
    this.context = context
  }

  String getName() {
    return this.context.currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause.class)?.getUserName()
  }

  List<String> getRoles() {
    def permissions = this.context.Hudson.instance.getAuthorizationStrategy().roleMaps.inject([:]) { map, it -> map + it.value.grantedRoles }
    return permissions.findAll { it.value.contains(currentJenkinsUser) }.collect { it.key.name }
  }
}
