package com.wildbeavers.helper

import com.wildbeavers.data_validation.type.TimeUnit
import com.wildbeavers.proxy.TimeoutProxy

/**
 * Ask end users questions to make sure the actions that follows are intended.
 */
class FoolProofValidationHelper {
  static final NUMBERS = [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine'
  ]
  static final AUTHORIZED_ATTEMPTS = 2

  private Script context
  private TimeoutProxy timeoutProxy
  private Random random
  private String challenge
  private String answer

  FoolProofValidationHelper(Script context, TimeoutProxy timeoutProxy, Random random) {
    this.context = context
    this.timeoutProxy = timeoutProxy
    this.random = random
  }

  /**
   * Challenge anyone with access to the pipeline
   * @param timeout Timeout in minutes
   * @param forbiddenApprovers
   * @return
   */
  void foolProof(Integer timeout = 20, List forbiddenApprovers = []) {
    String error = ''

    for (def attempts = 1; attempts <= AUTHORIZED_ATTEMPTS; attempts++) {
      this.generateChallenge()

      def foolProofInput = [:]
      this.timeoutProxy.timeout(timeout, new TimeUnit('MINUTES'), {
        foolProofInput = this.context.input(
          message: """
Actions that follow requires a human validation (attempt: ${attempts} / ${AUTHORIZED_ATTEMPTS}).\n
Enter the following numbers as digits:\n\n
${this.challenge}""",
          ok: 'Approve',
          parameters: [
            this.context.string(
              defaultValue: '',
              description: 'Example: if `one two three` is displayed, enter `123`',
              name: 'Response',
              trim: true
            )
          ],
          submitterParameter: 'approver'
        )
      })

      if (foolProofInput.Response != this.answer) {
        error = "${foolProofInput.approver} did not input the correct digits for validation."
        continue
      }

      if (forbiddenApprovers.contains(foolProofInput.approver)) {
        error = "${foolProofInput.approver} is not an authorized approver."
      }

      break
    }

    if ('' != error) {
      this.context.error(error)
    }
  }

  private String generateChallenge() {
    def firstNumber = this.random.nextInt(10)
    def secondNumber = this.random.nextInt(10)
    def thirdNumber = this.random.nextInt(10)

    this.challenge = "${NUMBERS[firstNumber]} ${NUMBERS[secondNumber]} ${NUMBERS[thirdNumber]}"
    this.answer = "${firstNumber}${secondNumber}${thirdNumber}"
  }
}
