package com.wildbeavers.helper

/**
 * Check whether or not a string follows the semver convention.
 * Manipulate a semver string.
 * Extract specific part of a semver string.
 */
class SemverHelper {
  public static final String SEMVER_REGEXP = ~"^${SEMVER_REGEXP_NOT_DELIMITED}\$"
  public static final String SEMVER_REGEXP_NOT_DELIMITED = /(?<FullPatch>(?<FullMinor>(?<Major>0|[1-9]\d*)\.(?<Minor>0|[1-9]\d*))\.(?<Patch>0|[1-9]\d*))(?<PreReleaseTagWithSeparator>-(?<PreReleaseTag>([a-z-][\da-z-]+|[\da-z-]+[a-z-][\da-z-]*|0|[1-9]\d*)(\.([a-z-][\da-z-]+|[\da-z-]+[a-z-][\da-z-]*|0|[1-9]\d*))*))?(?<BuildMetadataWithSeparator>\+(?<BuildMetadata>[\da-z-]+(\.[\da-z-]+)*))?/
  public static final String SEMVER_REGEXP_NO_PRERELEASE = /^(?<FullPatch>(?<FullMinor>(?<Major>0|[1-9]\d*)(\.(?<Minor>0|[1-9]\d*))?))(\.(?<Patch>0|[1-9]\d*))?/
  public static final String SEMVER_FLOATING_REGEXP = /^(?<FullPatch>(?<FullMinor>(?<Major>0|[1-9]\d*)(\.(?<Minor>0|[1-9]\d*))?))$/

  /**
   * Get only the major part of a ${tag} (MAJOR) without minor, patch and the pre-release.
   */
  static String getMajorTag(CharSequence tag) {
    return getMajorVersionNumber(tag)?.toString()
  }

  /**
   * Get only the MAJOR digit of a ${tag}. Equivalent to extractMajorTag(), except this return an Integer.
   */
  static Integer getMajorVersionNumber(CharSequence tag) {
    if (!isSemver(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP) ? (tag =~ SEMVER_REGEXP)[0][3].toInteger() : null
  }

  /**
   * Get the minor part of a ${tag} (MAJOR.MINOR) without patch and the pre-release.
   */
  static String getMinorTag(CharSequence tag) {
    if (!isSemver(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP)[0][2] instanceof CharSequence ? (tag =~ SEMVER_REGEXP)[0][2] : ''
  }

  /**
   * Get only the MINOR digit of a ${tag}.
   */
  static Integer getMinorVersionNumber(CharSequence tag) {
    if (!isSemver(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP) ? (tag =~ SEMVER_REGEXP)[0][4].toInteger() : null
  }

  /**
   * Get the full tag (MAJOR.MINOR.PATCH) from ${tag} but without the pre-release.
   */
  static String getPatchTag(CharSequence tag) {
    if (!isSemver(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP)[0][1] instanceof CharSequence ? (tag =~ SEMVER_REGEXP)[0][1] : ''
  }

  /**
   * Get only the PATCH digit of a ${tag}.
   */
  static Integer getPatchVersionNumber(CharSequence tag) {
    if (!isSemver(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP) ? (tag =~ SEMVER_REGEXP)[0][5].toInteger() : null
  }

  /**
   * Get the pre-release part of a ${tag} (PRE-RELEASE) without the dash, major, minor and patch digits.
   */
  static String getPreReleasePart(CharSequence tag) {
    if (!isSemver(tag) || !containsPreRelease(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP)[0][7] instanceof CharSequence ? (tag =~ SEMVER_REGEXP)[0][7] : ''
  }

  /**
   * Get the metadata part of a ${tag} (PRE-RELEASE) without the dash, major, minor and patch digits.
   */
  static String getMetadataPart(CharSequence tag) {
    if (!isSemver(tag) || !containsMetadata(tag)) {
      return null
    }

    return (tag =~ SEMVER_REGEXP)[0][7] instanceof CharSequence ? (tag =~ SEMVER_REGEXP)[0][7] : ''
  }

  /**
   * Checks whether a tag contains metadata, a special kind of "pre-release" that is only a digit increment.
   */
  static Boolean containsMetadata(CharSequence tag) {
    return tag ==~ /.*-[\d]+$/
  }

  /**
   * Checks whether a tag contains a valid pre-release part
   */
  static Boolean containsPreRelease(CharSequence tag) {
    return tag ==~ /.*-dev[\d]*$/ || tag ==~ /.*-rc[\d]*$/
  }

  /**
   * Checks whether or not the ${tag} follows the SEMVER standard.
   **/
  static Boolean isSemver(CharSequence tag) {
    return tag ==~ SEMVER_REGEXP
  }
}
