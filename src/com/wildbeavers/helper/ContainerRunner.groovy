package com.wildbeavers.helper

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.data.CommandResult
import com.wildbeavers.data.OptionString
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.ExecuteProxy

/**
 * Run a container (docker, podman, etc) with command line options or otherwise fallback to software installed in the current context.
 */
class ContainerRunner {
  final DEFAULT_SHARE_PATH = '/data'

  private Debugger debugger
  private ExecuteProxy executeProxy
  private String basepath

  ContainerRunner(Debugger debugger, ExecuteProxy executeProxy, String basepath) {
    this.debugger = debugger
    this.executeProxy = executeProxy
    this.basepath = basepath
  }

  /**
   * Pulls a ${ContainerOptions.getImage()}, granted it does not already exists on the current context.
   * Pulls anyway if ${forcePull}.
   *
   * @param ContainerOptions containerOptions
   * @param Boolean forcePull
   */
  void pull(ContainerOptions containerOptions, Boolean forcePull = false) {
    if (containerOptions.getImage() && (forcePull || this.executeProxy.executeAndAllowError("${containerOptions.getTool()} inspect \"${containerOptions.getImage()}\" 2> /dev/null").getStatusCode() != 0)) {
      this.executeProxy.execute("${containerOptions.getTool()} pull ${containerOptions.getImage()}")
      return
    }

    if (!containerOptions.getImage()) {
      this.debugger.print("Unable to pull, no image was provided")
      return
    }

    this.debugger.printDebug("Did not pull ${containerOptions.getImage()}. Image already exists on current context.")
  }

  /**
   * Runs a container defined with ${containerOptions} with ${arguments}.
   * Fallbacks to OS command when container runtime is not installed or no container image was provided.
   * When ${containerOptions.getFallbackCommand()} and ${arguments} matches, only ${arguments} will be used as fallback.
   *
   * @param containerOptions
   * @param arguments
   * @param forcePullImage
   *
   * @return CommandResult
   */
  CommandResult run(ContainerOptions containerOptions, String arguments = '', Boolean forcePullImage = false) {
    if (forcePullImage) {
      this.pull(containerOptions, true)
    }

    return this.doRun(containerOptions, this.prepareRunCommand(containerOptions, arguments))
  }

  private CommandResult doRun(ContainerOptions containerOptions, String runCommand) {
    if (containerOptions.getQuiet()) {
      return this.executeProxy.executeSecret(runCommand)
    }

    return this.executeProxy.executeWithTail(runCommand)
  }

  private String prepareRunCommand(ContainerOptions containerOptions, String arguments = null) {
    if (!containerOptions.getAssumeRuntimeInstalled() && !this.isCommandInstalled(containerOptions.getTool().toString())) {
      return this.returnFallbackCommand(containerOptions, arguments, "${containerOptions.getTool()} is not available in the current context.")
    }

    if (!containerOptions.hasImage()) {
      return this.returnFallbackCommand(containerOptions, arguments, "No container image was provided.")
    }

    OptionString optionString = new OptionString()
    if (containerOptions.getBasepathAsWorkdir()) {
      optionString.addOption('-w', this.DEFAULT_SHARE_PATH)
      optionString.addOption('-v', "\"${this.basepath}\":\"${this.DEFAULT_SHARE_PATH}\"")
    }

    if (containerOptions.getRemoveAfterRun()) {
      optionString.addOption('--rm')
    }

    if (containerOptions.getPrivileged()) {
      optionString.addOption('--privileged')
    }

    if (containerOptions.getTransientStore()) {
      optionString.addOption('--transient-store')
    }

    if (containerOptions.getReadOnly()) {
      optionString.addOption('--read-only')
    }

    if (containerOptions.getRunHasDaemon()) {
      optionString.addOption('-d')
    }

    if (containerOptions.getEntrypoint()) {
      optionString.addOption('--entrypoint', containerOptions.getEntrypoint())
    }

    if (containerOptions.getName()) {
      optionString.addOption('--name', containerOptions.getName())
    }

    if (containerOptions.getUserns()) {
      optionString.addOption('--userns', containerOptions.getUserns())
    }

    if (containerOptions.getNetwork()) {
      optionString.addOption('--network', containerOptions.getNetwork())
    }

    if (containerOptions.getPull()) {
      optionString.addOption('--pull', containerOptions.getPull())
    }

    if (containerOptions.getLogDriver()) {
      optionString.addOption('--log-driver', containerOptions.getLogDriver())
    }

    containerOptions.getVolumes().each { key, value ->
      optionString.addOption('-v', "\"${key}\":\"${value}\"")
    }

    containerOptions.getEnvironmentVariables().each { key, value ->
      if (value != null) {
        optionString.addOption('-e', "\"${key}\"=\"${value}\"")
      } else {
        optionString.addOption('-e', "\"${key}\"")
      }
    }

    optionString.addOption(containerOptions.getImage())
    optionString.addOption(arguments)

    return ([containerOptions.getTool().toString(), containerOptions.getToolOptions().toString(), 'run', optionString.toString()] - '').join(' ')
  }

  private String returnFallbackCommand(ContainerOptions containerOptions, CharSequence arguments, String reason) {
    if (!containerOptions.getFallbackCommand()) {
      throw new Exception(reason + " However, no fallback command was provided.")
    }

    if (!this.isCommandInstalled(containerOptions.getFallbackCommand())) {
      throw new Exception(reason + " However, “${containerOptions.getFallbackCommand()}” is not installed in the current context.")
    }

    this.debugger.printDebug(reason + " Assuming “${containerOptions.getFallbackCommand()}” is installed.")

    if (arguments?.toString() =~ /^${containerOptions.getFallbackCommand()} /) {
      return arguments?.toString()
    }

    return containerOptions.getFallbackCommand() + ' ' + arguments?.toString()
  }

  private Boolean isCommandInstalled(String containerCommand) {
    try {
      this.executeProxy.execute("which ${containerCommand}")
      return true
    } catch (Exception ignored) {
    }
    return false
  }
}
