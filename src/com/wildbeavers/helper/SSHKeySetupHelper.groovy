package com.wildbeavers.helper

import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.ExecuteProxy

/**
 * Setup an SSH existing private key in a specific $destination.
 * 1. Uses environment variable for the passphrase and the original key location. This first implementation is intended to be used within a sshUserPrivateKey pipeline syntax.
 */
class SSHKeySetupHelper {
  private Debugger debugger
  private ExecuteProxy executeProxy
  private EnvironmentVariableProxy environmentVariableProxy

  SSHKeySetupHelper(Debugger debugger, ExecuteProxy executeProxy, EnvironmentVariableProxy environmentVariableProxy) {
    this.debugger = debugger
    this.executeProxy = executeProxy
    this.environmentVariableProxy = environmentVariableProxy
  }

  /**
   * Setup a SSH private key using populated environment variables.
   * This method is intended to be used within the Jenkins `sshUserPrivateKey` pipeline syntax.
   *
   * @param keyFileVariableName Variable containing the path of the private key.
   * @param passphraseEnvVariableName Variable containing the the passphrase of the private key.
   * @param hostKeys List of known hosts to populate
   * @param destination Directory where to setup the private key
   *
   * @return The destination of the key.
   */
  String setupPrivateKeyWithEnvVars(String keyFileVariableName, String passphraseEnvVariableName, List hostKeys, String destination = '~/.ssh') {
    String sshAskPassExecutable = destination + '/getSSHKeyPassword'

    this.executeProxy.execute("mkdir -p \"${destination}\"")

    this.executeProxy.execute("echo \"!#/bin/bash\necho \\\$${passphraseEnvVariableName}\" | tee \"${sshAskPassExecutable}\"")
    this.executeProxy.execute("chmod 770 \"${sshAskPassExecutable}\"")
    this.environmentVariableProxy.set('SSH_ASKPASS', sshAskPassExecutable)

    if (this.sshAgentIsAvailable()) {
      this.debugger.print('SSH Agent is available on this executor. This feature is not available for now. Fallback to agentless configuration.')
    } else {
      this.debugger.print('SSH Agent is not available on this executor.')
    }

    this.executeProxy.executeSecret("cat \${${keyFileVariableName}} | tee \"${this.getSSHKeyFileName(destination, keyFileVariableName)}\"")
    this.executeProxy.execute("chmod 600 \"${this.getSSHKeyFileName(destination, keyFileVariableName)}\"")
    this.executeProxy.execute('echo "' + hostKeys.join("\" | tee -a \"${destination}/known_hosts\" && echo \"") + "\" | tee -a \"${destination}/known_hosts\"")

    return destination
  }

  /**
   * Remove the $destination: directory where the private SSH key was setup.
   *
   * @param destination
   * @return
   */
  String cleanupPrivateKeyWithEnvVars(String destination = '~/.ssh') {
    this.environmentVariableProxy.unset('SSH_ASKPASS')
    this.executeProxy.execute("rm -rf \"${destination}\"")
  }

  private String getSSHKeyFileName(String sshPath, String keyFile) {
    return sshPath + '/id_' + this.executeProxy.execute("ssh-keygen -l -f \$${keyFile} | rev | cut -d \' \' -f 1 | rev | tr -d \')(\' | tr \'[:upper:]\' \'[:lower:]\'").getStdout()
  }

  private Boolean sshAgentIsAvailable() {
    return (
      0 == this.executeProxy.executeAndAllowError('command -v ssh-add && ps -p ${SSH_AGENT_PID}').getStatusCode()
    )
  }
}
