package com.wildbeavers.di

import java.util.concurrent.ConcurrentHashMap

class IOC {
  protected static registry = [:]
  protected static registryForSingleton = [:]
  protected static Map<String, Map<String, Closure>> factoryClosuresRegistry = [:]
  protected static ConcurrentHashMap<String, Object> instances = [:]
  protected static globals = [:]

  static <T> void register(Class<T> definition, Closure<T> resolve) {
    register(definition.getName(), resolve)
  }

  static void register(CharSequence definition, Closure resolve) {
    registry[definition] = resolve
  }

  /**
   * Registers a new GenericFactory $ofClass
   */
  static <T> void registerFactory(Class<T> ofClass) {
    registerSingleton(ofClass.getName() + 'Factory', { return new GenericFactory<T>(ofClass) })
  }

  /**
   * Registers a Closure $resolve to instantiate a $definition, using IOC.get(<T>)
   * When $resolve is called the first time, its result is saved and served in subsequent IOC.get(<T>) calls.
   */
  static <T> void registerSingleton(Class<T> definition, Closure<T> resolve) {
    registerSingleton(definition.getName(), resolve)
  }

  /**
   * Registers a Closure $resolve to instantiate a class with the name: $definition, using IOC.get(...)
   * When $resolve is called the first time, its result is saved and served in subsequent IOC.get(...) calls.
   */
  static void registerSingleton(CharSequence definition, Closure resolve) {
    registryForSingleton[definition] = resolve
    resetSingleton(definition)
  }

  /**
   * Removes all the instantiated singleton instances
   */
  static void reset() {
    instances.clear()
  }

  /**
   * Removes all registered definitions
   */
  static void deregisterAll() {
    registry.clear()
    registryForSingleton.clear()
    factoryClosuresRegistry.clear()
    globals.clear()
    reset()
  }

  /**
   * Removes a specific instantiated singleton instance
   */
  static void resetSingleton(CharSequence definition) {
    if (instances.containsKey(definition)) {
      instances.remove(definition)
    }
  }

  /**
   * Replaces a singleton instance and calls a refresh of all the classes that implements IOCRefreshable
   * @param definition
   * @param resolve
   */
  static void refreshSingleton(CharSequence definition, Closure resolve) {
    registerSingleton(definition, resolve)

    instances.each {
      if (it.value instanceof IOCRefreshable) {
        it.value.refreshDependencies()
      }
    }
  }

  /**
   * Replaces a singleton instance and calls a refresh of all the classes that implements IOCRefreshable
   * @param definition
   * @param resolve
   */
  static <T> void refreshSingleton(Class<T> definition, Closure<T> resolve) {
    refreshSingleton(definition.getName(), resolve)
  }

  /**
   * Registers a global variable
   * @param globalName
   * @param value
   */
  static void registerGlobal(String globalName, value) {
    globals[globalName] = value
  }

  /**
   * Register a given Closure $resolve with name $factoryClosureName, to be further registered inside $factoryDefinition during its instantiation.
   * @param factoryDefinition
   * @param productName
   * @param productLine
   * @return
   */
  static <T> void bindProductLine(Class<T> factoryDefinition, String productName, Closure<T> productLine) {
    if (!isRegistered(factoryDefinition.getName() + 'Factory') && !isRegisteredAsSingleton(factoryDefinition.getName() + 'Factory')) {
      throw new IOCException('No factory was registered with the name “' + factoryDefinition.getName() + '”.')
    }

    if (isInstantiated(factoryDefinition.getName() + 'Factory')) {
      throw new IOCException("Attempted to bind a factory closure named “${productName}” to the “${factoryDefinition.getName()}” factory, but this factory is already instantiated; Register the closure directly in the instantiated factory.")
    }

    if (!factoryClosuresRegistry.containsKey(factoryDefinition.getName() + 'Factory')) {
      factoryClosuresRegistry[factoryDefinition.getName() + 'Factory'] = [:] as Map<String, Closure>
    }

    factoryClosuresRegistry[factoryDefinition.getName() + 'Factory'][productName] = productLine
  }

  /**
   * Instantiate a factory class <T> from previously registered closure, with $args
   * @param definition
   * @param args
   * @return
   */
  static <T> GenericFactory<T> getFactory(Class<T> definition, ... args) {
    return get(definition.getName() + 'Factory', *args) as GenericFactory<T>
  }

  /**
   * Instantiate a class <T> from previously registered closure, with $args
   * @param definition
   * @param args
   * @return
   */
  static <T> T get(Class<T> definition, ... args) {
    return get(definition.getName(), *args) as T
  }

  /**
   * Instantiate a class registered under $definition with given $args
   * @param definition
   * @param args
   * @return
   */
  static get(String definition, ... args) {
    if (!isRegisteredAsAnything(definition)) {
      throw new IOCException('No class or global variable registered with the definition “' + definition + '”.')
    }

    if (isRegisteredAsGlobal(definition)) {
      return globals[definition]
    }

    if (isRegisteredAsSingleton(definition)) {
      if (!isInstantiated(definition)) {
        instances[definition] = registryForSingleton[definition](*args)
      }

      return applyFactoryClosures(instances[definition], definition)
    }

    return applyFactoryClosures(registry[definition](*args), definition)
  }

  static String debug() {
    def displayInstances = ''
    for (instance in instances) {
      displayInstances += "\n---> ${instance.key}: ${instance.value.getClass().toString() + '#' + instance.value.hashCode().toString()}"
    }

    return """
      Existing instances:
---> ${displayInstances}

      Registered singletons classes:
---> ${registryForSingleton.keySet().join("\n---> ")}

      Registered normal classes:
---> ${registry.keySet().join("\n---> ")}

      Registered factory closures:
---> ${factoryClosuresRegistry.collect { it.key + "\n-----------> " + it.value.keySet().join("\n-----------> ") }.join("\n---> ")}
"""
  }

  static Boolean isRegistered(String definition) {
    return registry.containsKey(definition)
  }

  static Boolean isRegisteredAsSingleton(String definition) {
    return registryForSingleton.containsKey(definition)
  }

  static Boolean isRegisteredAsGlobal(String definition) {
    return globals.containsKey(definition)
  }

  static Boolean isRegisteredAsAnything(String definition) {
    return isRegistered(definition) || isRegisteredAsSingleton(definition) || isRegisteredAsGlobal(definition)
  }

  static Boolean isInstantiated(String definition) {
    return instances.containsKey(definition)
  }

  static private Object applyFactoryClosures(Object instance, String definition) {
    if (instance instanceof CanRegister && factoryClosuresRegistry.containsKey(definition))
      factoryClosuresRegistry.get(definition).each { String k, Closure v ->
        instance.register(k, v)
      }

    if (isRegisteredAsSingleton(definition)) {
      factoryClosuresRegistry.remove(definition)
    }
    return instance
  }
}
