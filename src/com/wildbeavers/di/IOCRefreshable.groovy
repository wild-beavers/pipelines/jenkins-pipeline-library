package com.wildbeavers.di

interface IOCRefreshable {
  void refreshDependencies()
}
