package com.wildbeavers.di

interface CanRegister<T> {
  /**
   * Registers a $closure with the $name
   */
  void register(String name, Closure<T> closure)
}
