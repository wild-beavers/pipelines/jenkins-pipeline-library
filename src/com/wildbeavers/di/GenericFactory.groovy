package com.wildbeavers.di

/**
 * Registers factories as closures and instantiate class<T> objects.
 */
class GenericFactory<T> implements CanRegister<T> {
  private Class<T> type
  private Map<String, Closure<T>> objectFactories = [:]

  GenericFactory(Class<T> type) {
    this.type = type
  }

  /**
   * Registers a Closure in charge of creating a new Class<T> object.
   * @param productLineName Fancy name of the ${objectFactoryClosure}
   * @param objectFactoryClosure Closure expected to return an instance of Class<T>
   */
  void register(String productLineName, Closure<T> objectFactoryClosure) {
    this.objectFactories[productLineName] = objectFactoryClosure
  }

  /**
   * Instantiate a new Class<T> object by calling a previously registered ${name} closure.
   * @param productLineName Fancy name of the closure to call
   * @param args Optional constructor arguments for instantiation
   * @return class<T>
   * @throw RuntimeException if no ${name} closure were registered
   * @throw IOCException if the ${name} closure does not return a class<T>
   */
  T instantiate(String productLineName = '', ... args) {
    if (!this.support(productLineName)) {
      throw new RuntimeException(
        "A GenericFactory instance of “${this.type}” does not support “${productLineName}”. " +
        "Supported product lines: “${this.getAllSupportedNames().join(",")}”"
      )
    }

    try {
      return this.type.cast(this.objectFactories[productLineName](*args))
    } catch (ClassCastException ignored) {
      throw new IOCException("“${productLineName}” name was registered in the GenericFactory “${this.type}” factory but does not return a valid object.")
    }
  }

  /**
   * Whether this factory supports an object factory named ${name}.
   * @param productLineName
   * @return True if this factory supports ${name}, False otherwise.
   */
  Boolean support(CharSequence productLineName) {
    return this.objectFactories?.containsKey(productLineName)
  }

  /**
   * Return all supported object factory names.
   * @return
   */
  List<String> getAllSupportedNames() {
    return this.objectFactories.keySet() as List<String>
  }
}
