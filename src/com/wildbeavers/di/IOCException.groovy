package com.wildbeavers.di

class IOCException extends Exception {
  IOCException(String message) {
    super(message)
  }
}
