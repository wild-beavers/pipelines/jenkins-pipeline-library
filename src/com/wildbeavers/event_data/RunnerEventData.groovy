package com.wildbeavers.event_data


import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.data.DependencyInstallerData
import com.wildbeavers.data.HasScmData
import com.wildbeavers.data.HasTagCheckerData
import com.wildbeavers.data.PipelineStatus
import com.wildbeavers.data.ScmAutoTagMethod
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.data.file_standard.HasFileStandardData
import com.wildbeavers.data.lint.HasLinterData
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.data.pre_commit.PreCommitData
import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.data_validation.type.GitCloneURL
import com.wildbeavers.data_validation.type.Version
import com.wildbeavers.dependency_installer.DependencyInstallerType
import com.wildbeavers.helper.SemverHelper
import com.wildbeavers.observer.EventDataInterface

import java.util.regex.Pattern

class RunnerEventData implements EventDataInterface, HasLinterData, HasFileStandardData, HasScmData, HasTagCheckerData {
  private EventDataInterface mainEventData

  private PreCommitData preCommitData

  private ForwardRelativeLinuxFullPath scmSshConfigDirectory = null

  private CharSequence scmAlternativeCheckoutCredentialID = ''
  private ForwardRelativeLinuxFullPath scmAlternativeCheckoutDirectory = null
  private GitCloneURL scmAlternativeCheckoutRepositoryURL = null
  private Version scmAlternativeCheckoutTag = null
  private Boolean scmFloatingTagsEnabled = true
  private Boolean scmAutoTagEnabled = true
  private ScmAutoTagMethod scmAutoTagMethod = ScmAutoTagMethod.TAG_ANNOTATED
  private ForwardRelativeLinuxFullPath scmAutoTagChangelogFilePath = new ForwardRelativeLinuxFullPath(DEFAULT_CHANGELOG_FILE_PATH)
  private Pattern scmAutoTagLastTagRegex = ~"(?!^#+\\s+)(${SemverHelper.SEMVER_REGEXP_NOT_DELIMITED})"

  private String dockerDataBasepath
  private Boolean dockerDataIsCurrentDirectory
  private ContainerLoginData containerLoginData
  private String headerMessage
  private PipelineStatus status
  private Boolean testsCancellerEnabled

  protected ArtifactFetchingData artifactFetchingData = null
  protected ArtifactPublisherData artifactPublisherData = null

  private CharSequence gitSSHCommand = ''

  // HasTagCheckerData
  protected Boolean tagCheckEnabled = true
  protected Boolean tagCheckOnlyMainBranch = true
  protected Pattern tagCheckRegex = ~(/(^latest$)|/ + SemverHelper.SEMVER_REGEXP_NO_PRERELEASE)

  // HasFileStandardData
  protected Boolean fileStandardEnabled = true
  protected Boolean fileStandardIgnoreDefault = false
  protected FileStandardData fileStandardData = null

  // HasGenericData
  protected Boolean linterEnabled = false
  protected List<LinterData> linterData = []

  private Boolean cleanupDelayEnabled = false
  private Integer cleanupDelayTime = 10
  private CharSequence cleanupDelayHeader = ''
  private CharSequence cleanupDelayMessage = ''
  private CharSequence cleanupDelayButtonLabel = 'Proceed'
  private Boolean cleanupDelayContinueIfExpired = true

  // HasTagCheckerData
  Boolean getTagCheckEnabled() {
    return tagCheckEnabled
  }

  void setTagCheckEnabled(Boolean tagCheckEnabled) {
    this.tagCheckEnabled = tagCheckEnabled
  }

  Boolean getTagCheckOnlyMainBranch() {
    return tagCheckOnlyMainBranch
  }

  void setTagCheckOnlyMainBranch(Boolean tagCheckOnlyMainBranch) {
    this.tagCheckOnlyMainBranch = tagCheckOnlyMainBranch
  }

  Pattern getTagCheckRegex() {
    return tagCheckRegex
  }

  void setTagCheckRegex(Pattern tagCheckRegex) {
    this.tagCheckRegex = tagCheckRegex
  }

  // DependencyInstaller
  Map<DependencyInstallerType, List<DependencyInstallerData>> dependencyInstallerData

  RunnerEventData(FileStandardData fileStandardData) {
    this.fileStandardData = fileStandardData
  }

  EventDataInterface getMainEventData() {
    return mainEventData
  }

  void setMainEventData(EventDataInterface mainEventData) {
    this.mainEventData = mainEventData
  }

  ArtifactFetchingData getArtifactFetchingData() {
    return artifactFetchingData
  }

  void setArtifactFetchingData(ArtifactFetchingData artifactFetchingData) {
    this.artifactFetchingData = artifactFetchingData
  }

  ArtifactPublisherData getArtifactPublisherData() {
    return artifactPublisherData
  }

  void setArtifactPublisherData(ArtifactPublisherData artifactPublisherData) {
    this.artifactPublisherData = artifactPublisherData
  }

  PreCommitData getPreCommitData() {
    return preCommitData
  }

  void setPreCommitData(PreCommitData preCommitData) {
    this.preCommitData = preCommitData
  }

  ContainerLoginData getContainerLoginData() {
    return containerLoginData
  }

  void setContainerLoginData(ContainerLoginData containerLoginData) {
    this.containerLoginData = containerLoginData
  }

  String getDockerDataBasepath() {
    return dockerDataBasepath
  }

  void setDockerDataBasepath(String dockerDataBasepath) {
    this.dockerDataBasepath = dockerDataBasepath
  }

  Boolean dockerDataIsCurrentDirectory() {
    return dockerDataIsCurrentDirectory
  }

  void setDockerDataIsCurrentDirectory(Boolean dockerDataIsCurrentDirectory) {
    this.dockerDataIsCurrentDirectory = dockerDataIsCurrentDirectory
  }

  String getHeaderMessage() {
    return headerMessage
  }

  void setHeaderMessage(String headerMessage) {
    this.headerMessage = headerMessage
  }

  PipelineStatus getStatus() {
    return status
  }

  void setStatus(PipelineStatus status) {
    this.status = status
  }

  Boolean getTestsCancellerEnabled() {
    return testsCancellerEnabled
  }

  void setTestsCancellerEnabled(Boolean testCancellerEnabled) {
    this.testsCancellerEnabled = testCancellerEnabled
  }

  CharSequence getGitSSHCommand() {
    return gitSSHCommand
  }

  void setGitSSHCommand(CharSequence gitSSHCommand) {
    this.gitSSHCommand = gitSSHCommand
  }

  CharSequence getScmSshConfigDirectory() {
    if (this.scmSshConfigDirectory == null) {
      return null
    }

    return scmSshConfigDirectory.toString()
  }

  void setScmSshConfigDirectory(CharSequence scmSshConfigDirectory) {
    this.scmSshConfigDirectory = new ForwardRelativeLinuxFullPath(scmSshConfigDirectory)
  }

  //
  // HasGenericData
  //

  @Override
  List<LinterData> getLinterData() {
    return this.linterData
  }

  @Override
  void setLinterData(List<LinterData> linterData) {
    this.linterData = linterData
  }

  @Override
  void addLinterData(LinterData linterData) {
    this.linterData.add(linterData)
  }

  @Override
  Boolean getLinterEnabled() {
    return this.linterEnabled
  }

  @Override
  Boolean isLinterEnabled() {
    return this.linterEnabled
  }

  @Override
  Boolean setLinterEnabled(Boolean enabled) {
    return this.linterEnabled = enabled
  }

  //
  // HasFileStandardData
  //

  @Override
  Boolean isFileStandardEnabled() {
    return this.fileStandardEnabled
  }

  void setFileStandardEnabled(Boolean fileStandardEnabled) {
    this.fileStandardEnabled = fileStandardEnabled
  }

  @Override
  Boolean isFileStandardIgnoreDefault() {
    return this.fileStandardIgnoreDefault
  }

  @Override
  void setFileStandardIgnoreDefault(Boolean fileStandardIgnoreDefault) {
    this.fileStandardIgnoreDefault = fileStandardIgnoreDefault
  }

  @Override
  FileStandardData getFileStandardData() {
    return this.fileStandardData
  }

  @Override
  void setFileStandardData(FileStandardData fileStandardData) {
    this.fileStandardData = fileStandardData
  }

  //DependencyInstaller
  Map<DependencyInstallerType, List<DependencyInstallerData>> getDependencyInstallerData() {
    return dependencyInstallerData
  }

  void setDependencyInstallerData(Map<DependencyInstallerType, List<DependencyInstallerData>> dependencyInstallerData) {
    this.dependencyInstallerData = dependencyInstallerData
  }

  // CleanupDelay
  Boolean getCleanupDelayEnabled() {
    return cleanupDelayEnabled
  }

  void setCleanupDelayEnabled(Boolean cleanupDelayEnabled) {
    this.cleanupDelayEnabled = cleanupDelayEnabled
  }

  Integer getCleanupDelayTime() {
    return cleanupDelayTime
  }

  void setCleanupDelayTime(Integer cleanupDelayTime) {
    this.cleanupDelayTime = cleanupDelayTime
  }

  CharSequence getCleanupDelayHeader() {
    return cleanupDelayHeader
  }

  void setCleanupDelayHeader(CharSequence cleanupDelayHeader) {
    this.cleanupDelayHeader = cleanupDelayHeader
  }

  CharSequence getCleanupDelayMessage() {
    return cleanupDelayMessage
  }

  void setCleanupDelayMessage(CharSequence cleanupDelayMessage) {
    this.cleanupDelayMessage = cleanupDelayMessage
  }

  CharSequence getCleanupDelayButtonLabel() {
    return cleanupDelayButtonLabel
  }

  void setCleanupDelayButtonLabel(CharSequence cleanupDelayButtonLabel) {
    this.cleanupDelayButtonLabel = cleanupDelayButtonLabel
  }

  Boolean getCleanupDelayContinueIfExpired() {
    return cleanupDelayContinueIfExpired
  }

  void setCleanupDelayContinueIfExpired(Boolean cleanupDelayContinueIfExpired) {
    this.cleanupDelayContinueIfExpired = cleanupDelayContinueIfExpired
  }

  // HasScmData
  CharSequence getScmAlternativeCheckoutCredentialID() {
    return scmAlternativeCheckoutCredentialID
  }

  void setScmAlternativeCheckoutCredentialID(CharSequence scmAlternativeCheckoutCredentialID) {
    this.scmAlternativeCheckoutCredentialID = scmAlternativeCheckoutCredentialID
  }

  CharSequence getScmAlternativeCheckoutDirectory() {
    return scmAlternativeCheckoutDirectory?.toString()
  }

  void setScmAlternativeCheckoutDirectory(ForwardRelativeLinuxFullPath scmAlternativeCheckoutDirectory) {
    this.scmAlternativeCheckoutDirectory = scmAlternativeCheckoutDirectory
  }

  CharSequence getScmAlternativeCheckoutRepositoryURL() {
    return scmAlternativeCheckoutRepositoryURL?.toString()
  }

  void setScmAlternativeCheckoutRepositoryURL(GitCloneURL scmAlternativeCheckoutRepositoryURL) {
    this.scmAlternativeCheckoutRepositoryURL = scmAlternativeCheckoutRepositoryURL
  }

  CharSequence getScmAlternativeCheckoutTag() {
    return scmAlternativeCheckoutTag?.toString()
  }

  void setScmAlternativeCheckoutTag(Version scmAlternativeCheckoutTag) {
    this.scmAlternativeCheckoutTag = scmAlternativeCheckoutTag
  }

  Boolean getScmFloatingTagsEnabled() {
    return scmFloatingTagsEnabled
  }

  void setScmFloatingTagsEnabled(Boolean scmFloatingTagsEnabled) {
    this.scmFloatingTagsEnabled = scmFloatingTagsEnabled
  }

  Boolean getScmAutoTagEnabled() {
    return scmAutoTagEnabled
  }

  void setScmAutoTagEnabled(Boolean scmAutoTagEnabled) {
    this.scmAutoTagEnabled = scmAutoTagEnabled
  }

  ScmAutoTagMethod getScmAutoTagMethod() {
    return scmAutoTagMethod
  }

  void setScmAutoTagMethod(ScmAutoTagMethod scmAutoTagMethod) {
    this.scmAutoTagMethod = scmAutoTagMethod
  }

  CharSequence getScmAutoTagChangelogFilePath() {
    return scmAutoTagChangelogFilePath.toString()
  }

  void setScmAutoTagChangelogFilePath(ForwardRelativeLinuxFullPath scmAutoTagChangelogFilePath) {
    this.scmAutoTagChangelogFilePath = scmAutoTagChangelogFilePath
  }

  Pattern getScmAutoTagLastTagRegex() {
    return scmAutoTagLastTagRegex
  }

  void setScmAutoTagLastTagRegex(Pattern scmAutoTagLastTagRegex) {
    this.scmAutoTagLastTagRegex = scmAutoTagLastTagRegex
  }
}
