package com.wildbeavers.event_data

import com.wildbeavers.data.HasRawConfigData
import com.wildbeavers.data_validation.type.PositiveOrZeroInt
import com.wildbeavers.data_validation.type.TimeUnit
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.observer.EventDataInterface

class ControllerEventData implements EventDataInterface, HasRawConfigData {
  RunnerEventData runnerEventData

  private List jobProperties = []
  private CharSequence jobNodeLabel = ''
  private PositiveOrZeroInt jobTimeoutTime = new PositiveOrZeroInt(10)
  private TimeUnit jobTimeoutUnit = new TimeUnit('HOURS')

  private Boolean debuggingEnabled = false
  private Boolean debuggingRuntimeToggleEnabled = false
  private PositiveOrZeroInt debuggingRuntimeToggleTimeout = new PositiveOrZeroInt(10)

  List getJobProperties() {
    return jobProperties
  }

  void setJobProperties(List jobProperties) {
    this.jobProperties = jobProperties
  }

  CharSequence getJobNodeLabel() {
    return jobNodeLabel
  }

  void setJobNodeLabel(CharSequence jobNodeLabel) {
    this.jobNodeLabel = jobNodeLabel
  }

  Integer getJobTimeoutTime() {
    return jobTimeoutTime?.toInt()
  }

  void setJobTimeoutTime(PositiveOrZeroInt jobTimeoutTime) {
    this.jobTimeoutTime = jobTimeoutTime
  }

  CharSequence getJobTimeoutUnit() {
    return jobTimeoutUnit?.toString()
  }

  void setJobTimeoutUnit(TimeUnit jobTimeoutUnit) {
    this.jobTimeoutUnit = jobTimeoutUnit
  }

  RunnerEventData getRunnerEventData() {
    return this.runnerEventData
  }

  void setRunnerEventData(RunnerEventData runnerEventData) {
    this.runnerEventData = runnerEventData
  }

  Boolean getDebuggingEnabled() {
    return debuggingEnabled
  }

  void setDebuggingEnabled(Boolean debuggingEnabled) {
    this.debuggingEnabled = debuggingEnabled
  }

  Boolean getDebuggingRuntimeToggleEnabled() {
    return debuggingRuntimeToggleEnabled
  }

  void setDebuggingRuntimeToggleEnabled(Boolean debuggingRuntimeToggleEnabled) {
    this.debuggingRuntimeToggleEnabled = debuggingRuntimeToggleEnabled
  }

  Integer getDebuggingRuntimeToggleTimeout() {
    return debuggingRuntimeToggleTimeout?.toInt()
  }

  void setDebuggingRuntimeToggleTimeout(PositiveOrZeroInt debuggingRuntimeToggleTimeout) {
    this.debuggingRuntimeToggleTimeout = debuggingRuntimeToggleTimeout
  }

  /////
  // HasRawConfigData
  /////

  private WildMap rawConfig

  WildMap getRawConfig() {
    return rawConfig
  }

  void setRawConfig(WildMap rawConfig) {
    this.rawConfig = rawConfig
  }
}
