package com.wildbeavers.auto_tagger


import com.wildbeavers.data.ScmInfo
import com.wildbeavers.data_manager.ScmInfoManager
import com.wildbeavers.di.IOC
import com.wildbeavers.di.IOCRefreshable
import com.wildbeavers.helper.SemverHelper
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.FileProxy

import java.util.regex.Pattern

class AutoTagger implements IOCRefreshable {
  private ExecuteProxy executeProxy
  private Debugger debugger
  private FileProxy fileProxy
  private ScmInfo scmInfo
  private ScmInfoManager scmInfoManager

  AutoTagger(ExecuteProxy executeProxy, Debugger debugger, FileProxy fileProxy, ScmInfo scmInfo, ScmInfoManager scmInfoManager) {
    this.executeProxy = executeProxy
    this.debugger = debugger
    this.fileProxy = fileProxy
    this.scmInfo = scmInfo
    this.scmInfoManager = scmInfoManager
  }

  @Override
  void refreshDependencies() {
    this.scmInfo = IOC.get(ScmInfo.class.getName()) as ScmInfo
  }

  /**
   * Whether or not the class is able to auto tag the current project.
   * @return True if the project can be auto-tagged, False otherwise.
   */
  Boolean canAutoTag(CharSequence changelogFileFullPath, Boolean floatingTagsEnabled) {
    return (
      this.debugger.testCondition(this.fileProxy.exists(changelogFileFullPath), "Cannot auto-tag this commit. No ${changelogFileFullPath} file exists in this project.") &&
        this.debugger.testCondition(this.scmInfo.isRevisionLatest(), 'Cannot auto-tag this commit. The current local revision is not the remote tip.') &&
        this.debugger.testCondition(this.scmInfo.isCurrentBranchDefault(), 'Cannot auto-tag this commit. The current branch is not the default branch.') &&
        this.debugger.testCondition(!this.scmInfo.isRevisionProperlyTagged(floatingTagsEnabled) || !this.scmInfo.isRemoteRevisionProperlyTagged(floatingTagsEnabled), "Cannot auto-tag this commit. This commit is already tagged properly according to the ${changelogFileFullPath}.") &&
        this.debugger.testCondition(!this.revisionTagIsOldInChangelog(changelogFileFullPath), "Cannot auto-tag this commit. This revision is improperly tagged, with an old semver in ${changelogFileFullPath}.") &&
        this.debugger.testCondition(this.scmInfo.getRemoteLatestSemverTag() != '', "Cannot auto-tag this commit. It seems the project have no existing previous semver tag. This is not supported.") &&
        this.debugger.testCondition(this.scmInfo.getAllRemoteTags() == this.scmInfo.getAllLocalTags(), "Cannot auto-tag this commit. Local tags and remote tags are different. This is not supported.") &&
        this.debugger.testCondition(this.scmInfo.getAllRevisionTags() == this.scmInfo.getAllRemoteRevisionTags(), "Cannot auto-tag this commit. Local revision have different tags that its corresponding remote revision. This is not supported.")
    )
  }

  /**
   * Tags the current revision according to the latest semver found in ${changelogFileFullPath} thanks to ${autoTagLastTagRegex} .\n
   * @param Boolean floatingTagsEnabled Whether to tag using floating version (MAJOR.MINOR, MAJOR and "latest")
   */
  void autoTag(CharSequence changelogFileFullPath, Pattern autoTagLastTagRegex, Boolean floatingTagsEnabled) {
    String lastKnownTagLineInChangelog = this.findLastKnownTaglineInChangelog(changelogFileFullPath)
    CharSequence newChangelogTag = this.findChangelogLatestTag(changelogFileFullPath, autoTagLastTagRegex)
    CharSequence newChangelogTagMessage = this.findChangelogNewTagMessage(changelogFileFullPath, lastKnownTagLineInChangelog)

    List tagsToAdd = [newChangelogTag, SemverHelper.getMinorTag(newChangelogTag), SemverHelper.getMajorTag(newChangelogTag), 'latest'].findAll({ version ->
      !this.scmInfo.getAllRevisionTags().contains(version) && (floatingTagsEnabled || newChangelogTag == version)
    })
    List tagsToDelete = tagsToAdd.findAll({ version ->
      this.scmInfo.getAllLocalTags().contains(version)
    })

    if (tagsToAdd.isEmpty()) {
      return
    }

    if (!tagsToDelete.isEmpty()) {
      this.executeProxy.execute("git tag -d ${tagsToDelete.join(' ')}")
    }

    tagsToAdd.each { tag ->
      this.executeProxy.execute("git tag -a \'${tag}\' -m \'${newChangelogTagMessage}\'")
    }

    this.scmInfoManager.refreshScmInfoLocalTags(this.scmInfo)

    this.executeProxy.execute("git config --global user.name \"WildBeaversAutoTagger\"")

    // This below could be simplified algorithmically within the current method, however, we want the push --delete…
    // … to be as late as possible and as close as possible to the push, so the remote does not remain without…
    // … its tags for a long time.
    if (!tagsToDelete.isEmpty()) {
      this.executeProxy.execute("git push --delete ${this.scmInfo.getRemoteName()} ${tagsToDelete.join(' ')} && git push --tags")
    } else {
      this.executeProxy.execute("git push --tags")
    }

    this.scmInfoManager.refreshScmInfoRemoteTags(this.scmInfo)
  }

  private Boolean revisionTagIsOldInChangelog(CharSequence changelogFileFullPath) {
    return this.scmInfo.isRevisionSemverTagged() && !this.lastKnownTagIsOnFirstLine(changelogFileFullPath)
  }

  private Boolean lastKnownTagIsOnFirstLine(CharSequence changelogFileFullPath) {
    return '1' == this.findLastKnownTaglineInChangelog(changelogFileFullPath)
  }

  private CharSequence findChangelogLatestTag(CharSequence changelogFileFullPath, Pattern autoTagLastTagRegex) {
    CharSequence latestSemverChangelogTag = this.fileProxy.read(changelogFileFullPath).find(autoTagLastTagRegex)

    this.debugger.printDebug("Latest semver tag defined in “${changelogFileFullPath}”: “${latestSemverChangelogTag}”")

    return latestSemverChangelogTag
  }

  private String findChangelogNewTagMessage(CharSequence changelogFileFullPath, String lastKnownTagLineInChangelog) {
    def newChangelogTagMessage = this.executeProxy.execute(
      "cat \'${changelogFileFullPath}\' | sed -e \"${lastKnownTagLineInChangelog},40000d;\" | sed -n -E '/(#.*|===+)/,\$p'  | tail -n +2 | sed '/^[[:space:]]*\$/d' | awk '{\$1=\$1;print}' | tr -d \"'\""
    ).getStdout()

    this.debugger.printDebug("Message linked with the new tag in ${changelogFileFullPath}:\n ${newChangelogTagMessage}")

    return newChangelogTagMessage
  }

  private String findLastKnownTaglineInChangelog(CharSequence changelogFileFullPath) {
    def latestTagLine = this.executeProxy.executeAndAllowError(
      "cat \'${changelogFileFullPath}\' | grep -n -E \"^#*\\s*${this.scmInfo.getRemoteLatestSemverTag()}\$\" | grep -Eo \'^[^:]+\'",
    ).getStdout()

    this.debugger.printDebug("Line containing the latest known semver tag in ${changelogFileFullPath}: ${latestTagLine}")

    return latestTagLine
  }
}
