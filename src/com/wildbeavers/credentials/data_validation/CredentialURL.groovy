package com.wildbeavers.credentials.data_validation

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

/**
 * Credential URL
 */
class CredentialURL extends Regexp {
  CredentialURL(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^([a-zA-Z_-]+:)?[a-zA-Z_-]+:[0-9a-zA-Z_/-]+$'
  }

  CharSequence getCredentialProvider() {
    return this.value.toString().split(':').length == 3 ? this.value.toString().split(':').reverse()[2] : ""
  }

  CharSequence getCredentialType() {
    return this.value.toString().split(':').reverse()[1]
  }

  CharSequence getCredentialId() {
    return this.value.toString().split(':').reverse()[0]
  }
}
