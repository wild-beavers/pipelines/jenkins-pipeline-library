package com.wildbeavers.credentials.data_validation

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.data_validation.type.generic.Regexp

/**
 * Exposition location URL for secrets
 */
class ExpositionLocation extends Regexp {
  ExpositionLocation(CharSequence input) {
    super(input.trim())
  }

  @NonCPS
  String getRegexp() {
    return '^[a-zA-Z_-]+:((?!:)[ -~])+$'
  }

  CharSequence getExpositionType() {
    return this.value.toString().tokenize(':')[0]
  }

  CharSequence getLocation() {
    return this.value.toString().tokenize(':')[1]
  }
}
