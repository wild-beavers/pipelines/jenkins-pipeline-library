package com.wildbeavers.credentials

import com.wildbeavers.composite.Fetcher
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exposer.CredentialExposer
import com.wildbeavers.credentials.provider.CredentialBackendProvider

/**
 * Loads Credentials from various credentials providers.
 * Exposes/conceals Credentials to various locations.
 */
class CredentialLoader {
  private Fetcher<CredentialBackendProvider, CredentialURL, String> credentialBackendProviderFetcher
  private CredentialExposer credentialExposer

  CredentialLoader(Fetcher<CredentialBackendProvider, CredentialURL, String> credentialBackendProviderFetcher, CredentialExposer credentialExposer) {
    this.credentialBackendProviderFetcher = credentialBackendProviderFetcher
    this.credentialExposer = credentialExposer
  }

  void setCredentialBackendProviderFetcher(Fetcher<CredentialBackendProvider, CredentialURL, String> credentialBackendProviderFetcher) {
    this.credentialBackendProviderFetcher = credentialBackendProviderFetcher
  }

  /**
   * @return all registered secret backend providers in order.
   */
  LinkedHashSet<CharSequence> getSupportedProviders() {
    return this.credentialBackendProviderFetcher.fetchAll().collect({ it.getId() })
  }

  /**
   * Returns a Credential according to $credentialURL: "provider:type:id".
   * Depending on registered providers, can return the credential opportunistically, in which case $credentialURL: "type:id".
   * @param credentialURL Formatted credential URL "provider:type:id" to force a specific provider; or "type:id" to search for the secret in all registered providers.
   */
  SecretBag load(CharSequence credentialURL) {
    CredentialURL credentialURLObject = new CredentialURL(credentialURL)

    for (credentialBackendProvider in this.credentialBackendProviderFetcher.fetchSupporting(credentialURLObject, credentialURLObject.getCredentialProvider().isEmpty() ? null : credentialURLObject.getCredentialProvider())) {
      try {
        return credentialBackendProvider.provide(credentialURLObject)
      } catch (CredentialNotFound ignored) {
      }
    }

    throw new CredentialNotFound("${credentialURL} cannot be found in any of the supported credential providers.")
  }

  /**
   * Exposes all credential’s secrets to defined locations
   * @param secretBag
   */
  void expose(SecretBag secretBag) {
    this.credentialExposer.expose(secretBag)
  }

  /**
   * Shortcut to load & expose a credential defined by $credentialURL.
   * @param credentialURL
   */
  SecretBag loadAndExpose(CharSequence credentialURL) {
    SecretBag secretBag = this.load(credentialURL)

    this.expose(secretBag)

    return secretBag
  }

  /**
   * Conceals all Credential’s secrets.
   * @param secretBag credential to conceal
   */
  void conceal(SecretBag secretBag) {
    this.credentialExposer.conceal(secretBag)
  }
}
