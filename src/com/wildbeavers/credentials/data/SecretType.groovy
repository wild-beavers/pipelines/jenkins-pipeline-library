package com.wildbeavers.credentials.data

enum SecretType {
  AWS_ACCESS_KEY_ID,
  AWS_ACCESS_SECRET_KEY,
  PASSPHRASE,
  PASSWORD,
  SSH_PRIVATE_KEY,
  TOKEN,
  UNKNOWN,
  USERNAME,
}
