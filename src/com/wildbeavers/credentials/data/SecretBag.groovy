package com.wildbeavers.credentials.data

/**
 * Bag of Secrets
 */
abstract class SecretBag {
  public static CharSequence ALIAS = 'UNDEFINED'

  protected Map<SecretType, Secret> secrets = [:]

  Map<SecretType, Secret> getSecrets() {
    return secrets
  }

  protected final void addSecret(SecretType secretType, Secret secret) {
    if (this.contains(secretType)) {
      throw new RuntimeException("Attempted to add a secret with type “${secretType}” twice. This can cause erratic behaviors.")
    }

    this.secrets[secretType] = secret
  }

  protected final Secret getSecret(SecretType secretType) {
    if (!this.contains(secretType)) {
      throw new RuntimeException("Attempted to get a secret with type “${secretType}”. However, it was not added or it is not supported.")
    }

    return this.secrets[secretType]
  }

  protected final Boolean contains(SecretType secretType) {
    return this.secrets.keySet().contains(secretType)
  }

  protected final Boolean contains(Secret secret) {
    return this.secrets.values().contains(secret)
  }

  @Override
  String toString() {
    return "SecretBag{" +
      "secrets=" + secrets.toString() +
      '}';
  }

  abstract CharSequence getId()
}
