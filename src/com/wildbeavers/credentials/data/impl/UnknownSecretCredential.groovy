package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer

/**
 * Bag of secret containing:
 * - a SecretType.UNKNOWN
 */
class UnknownSecretCredential extends SecretBag {
  static final ALIAS = 'unknown'
  static final CharSequence SECRET_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:WB_SECRET_UNKNOWN"

  Secret getUnknownSecret() {
    return this.getSecret(SecretType.UNKNOWN)
  }

  void setUnknownSecret(Secret secret) {
    this.addSecret(SecretType.UNKNOWN, secret)
  }

  @Override
  CharSequence getId() {
    return ALIAS
  }
}
