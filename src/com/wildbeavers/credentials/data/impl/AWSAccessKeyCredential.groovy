package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer

/**
 * Bag of secrets containing:
 * - a SecretType.AWS_ACCESS_KEY_ID
 * - a SecretType.AWS_ACCESS_SECRET_KEY
 */
class AWSAccessKeyCredential extends SecretBag {
  static final CharSequence ALIAS = 'awsAccessKey'

  static final CharSequence AWS_ACCESS_KEY_ID_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:AWS_ACCESS_KEY_ID"
  static final CharSequence AWS_ACCESS_SECRET_KEY_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:AWS_SECRET_ACCESS_KEY"

  Secret getAccessKeyId() {
    return this.getSecret(SecretType.AWS_ACCESS_KEY_ID)
  }

  void setAccessKeyId(Secret secret) {
    this.addSecret(SecretType.AWS_ACCESS_KEY_ID, secret)
  }

  Secret getAccessSecretKey() {
    return this.getSecret(SecretType.AWS_ACCESS_SECRET_KEY)
  }

  void setAccessSecretKey(Secret secret) {
    this.addSecret(SecretType.AWS_ACCESS_SECRET_KEY, secret)
  }

  @Override
  CharSequence getId() {
    return ALIAS
  }
}
