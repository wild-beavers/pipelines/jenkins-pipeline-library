package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer

/**
 * Bag of secrets containing:
 * - a SecretType.USERNAME
 * - a SecretType.PASSWORD
 */
class UsernamePasswordCredential extends SecretBag {
  static final ALIAS = 'usernamePassword'
  static final CharSequence USERNAME_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:WB_USERNAME"
  static final CharSequence PASSWORD_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:WB_PASSWORD"

  Secret getUsername() {
    return this.getSecret(SecretType.USERNAME)
  }

  void setUsername(Secret username) {
    this.addSecret(SecretType.USERNAME, username)
  }

  Secret getPassword() {
    return this.getSecret(SecretType.PASSWORD)
  }

  void setPassword(Secret password) {
    this.addSecret(SecretType.PASSWORD, password)
  }

  @Override
  CharSequence getId() {
    return ALIAS
  }
}
