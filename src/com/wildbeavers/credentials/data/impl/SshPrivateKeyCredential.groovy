package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer
import com.wildbeavers.credentials.exposer.impl.FileCredentialExposer

/**
 * Bag of secrets containing:
 * - a SecretType.USERNAME
 * - a SecretType.PASSPHRASE
 * - a SecretType.SSH_PRIVATE_KEY
 */
class SshPrivateKeyCredential extends SecretBag {
  static final CharSequence ALIAS = 'sshPrivateKey'
  static final CharSequence USERNAME_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:WB_SSH_USERNAME"
  static final CharSequence PASSPHRASE_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:WB_SSH_PASSPHRASE"
  static final CharSequence PRIVATE_KEY_SUGGESTED_LOCATION = "${FileCredentialExposer.ALIAS}:~/.ssh/id_wb"

  Secret getUsername() {
    return this.getSecret(SecretType.USERNAME)
  }

  void setUsername(Secret username) {
    this.addSecret(SecretType.USERNAME, username)
  }

  Secret getPassphrase() {
    return this.getSecret(SecretType.PASSPHRASE)
  }

  void setPassphrase(Secret passphrase) {
    this.addSecret(SecretType.PASSPHRASE, passphrase)
  }

  Secret getPrivateKey() {
    return this.getSecret(SecretType.SSH_PRIVATE_KEY)
  }

  void setPrivateKey(Secret privateKey) {
    this.addSecret(SecretType.SSH_PRIVATE_KEY, privateKey)
  }

  @Override
  CharSequence getId() {
    return ALIAS
  }
}
