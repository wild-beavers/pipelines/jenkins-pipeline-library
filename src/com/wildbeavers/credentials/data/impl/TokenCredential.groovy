package com.wildbeavers.credentials.data.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.SecretType
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer

/**
 * Bag of secrets containing:
 * - a SecretType.TOKEN
 */
class TokenCredential extends SecretBag {
  static final ALIAS = 'token'
  static final CharSequence TOKEN_SUGGESTED_LOCATION = "${EnvironmentVariableCredentialExposer.ALIAS}:WB_TOKEN"

  Secret getToken() {
    return this.getSecret(SecretType.TOKEN)
  }

  void setToken(Secret token) {
    this.addSecret(SecretType.TOKEN, token)
  }

  @Override
  CharSequence getId() {
    return ALIAS
  }
}
