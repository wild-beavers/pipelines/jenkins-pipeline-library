package com.wildbeavers.credentials.data

import com.wildbeavers.credentials.data_validation.ExpositionLocation

/**
 * Represent a secret and track status of its expositions.
 */
class Secret {
  private String content
  private String description
  private ExpositionLocation defaultExpositionLocation
  private Map<ExpositionLocation, Boolean> expositionLocations = [:]

  Secret(String content, String description, ExpositionLocation defaultExpositionLocation) {
    this.content = content
    this.description = description
    this.defaultExpositionLocation = defaultExpositionLocation
  }

  String getContent() {
    return content
  }

  String getDescription() {
    return description
  }

  ExpositionLocation getDefaultExpositionLocation() {
    return defaultExpositionLocation
  }

  Map<ExpositionLocation, Boolean> getExpositionLocations() {
    return expositionLocations
  }

  void addExpositionLocation(ExpositionLocation expositionLocation) {
    if (expositionLocations.containsKey(expositionLocation)) {
      return
    }

    expositionLocations[expositionLocation] = false
  }

  void addExpositionLocation(CharSequence expositionLocation) {
    this.addExpositionLocation(new ExpositionLocation(expositionLocation))
  }

  void removeExpositionLocation(ExpositionLocation expositionLocation) {
    if (!expositionLocations.containsKey(expositionLocation)) {
      return
    }

    expositionLocations.remove(expositionLocation)
  }

  ExpositionLocation getFirstExposedLocation() {
    return expositionLocations.find({ it.getValue() == true })?.getKey()
  }

  void markAsExposed(ExpositionLocation expositionLocation) {
    if (!expositionLocations.containsKey(expositionLocation)) {
      return
    }

    expositionLocations[expositionLocation] = true
  }

  void markAsConcealed(ExpositionLocation expositionLocation) {
    if (!expositionLocations.containsKey(expositionLocation)) {
      return
    }

    expositionLocations[expositionLocation] = false
  }

  Boolean isExposedInLocation(ExpositionLocation expositionLocation) {
    if (!expositionLocations.containsKey(expositionLocation)) {
      return false
    }

    return expositionLocations[expositionLocation]
  }

  Boolean isExposedAtAllLocations() {
    expositionLocations.values().every({ it == true })
  }

  Boolean hasExpositionLocations() {
    return !expositionLocations.isEmpty()
  }

  @Override
  String toString() {
    return "Secret{" +
      "description='" + description + '\'' +
      ", defaultExpositionLocation=" + defaultExpositionLocation +
      ", expositionLocations=" + expositionLocations +
      '}';
  }
}
