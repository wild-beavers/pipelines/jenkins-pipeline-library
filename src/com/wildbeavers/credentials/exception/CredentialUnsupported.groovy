package com.wildbeavers.credentials.exception

class CredentialUnsupported extends Exception {
  CredentialUnsupported(String message) {
    super(message)
  }
}
