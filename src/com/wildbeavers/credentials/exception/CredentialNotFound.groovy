package com.wildbeavers.credentials.exception

class CredentialNotFound extends Exception {
  CredentialNotFound(String message) {
    super(message)
  }
}
