package com.wildbeavers.credentials.exception

class SecretExpositionException extends Exception {
  SecretExpositionException(String message) {
    super(message)
  }
}
