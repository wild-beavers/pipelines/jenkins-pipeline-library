package com.wildbeavers.credentials.provider

import com.wildbeavers.composite.Fetchable
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exception.CredentialUnsupported

abstract class CredentialBackendProvider implements Fetchable<CredentialURL, CharSequence> {
  /**
   * Whether this class support the given $type
   * @param type Type of credential
   * @return Boolean
   */
  final Boolean support(CredentialURL credentialURL) {
    return this.getSupportedCredentialTypes().contains(credentialURL.getCredentialType())
  }

  /**
   * Provide a SecretBag of $type extracted from an $uri
   * @return SecretBag
   * @throws CredentialUnsupported when credential is not supported by this provider
   * @throws CredentialNotFound when credential was not found in the backend handled by this provider
   */
  final SecretBag provide(CredentialURL credentialURL) {
    if (!this.support(credentialURL)) {
      throw new CredentialUnsupported("The credential backend provider “${this.getClass()}” does not support secret of type “${credentialURL.getCredentialType()}”. Supported crendials are: ${this.getSupportedCredentialTypes().join(', ')}.")
    }

    return this.doProvide(credentialURL)
  }

  /**
   * Provide a corresponding SecretBag data object via a $credentialURL
   * @return SecretBag
   * @throws CredentialNotFound when credential was not found in the backend handled by this provider
   * @throws CredentialUnsupported when credential is not supported by this provider
   */
  abstract protected SecretBag doProvide(CredentialURL credentialURL)

  /**
   * @return List<CharSequence> List of Credentials (SecretBag) this class supports
   */
  abstract List<CharSequence> getSupportedCredentialTypes()
}
