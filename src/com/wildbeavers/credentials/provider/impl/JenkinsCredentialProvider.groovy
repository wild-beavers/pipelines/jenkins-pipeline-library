package com.wildbeavers.credentials.provider.impl


import com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey
import com.cloudbees.plugins.credentials.common.StandardUsernameCredentials
import com.cloudbees.plugins.credentials.impl.BaseStandardCredentials
import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exception.CredentialUnsupported
import com.wildbeavers.credentials.provider.CredentialBackendProvider
import com.wildbeavers.credentials.transformer.JenkinsCredentialDataTransformer

/**
 * Implements a CredentialBackendProvider with Jenkins internal vault
 */
class JenkinsCredentialProvider extends CredentialBackendProvider {
  final static String ALIAS = 'jenkins'

  private JenkinsCredentialDataTransformer jenkinsCredentialDataTransformer
  private JenkinsProviderAdapter jenkinsProviderAdapter

  JenkinsCredentialProvider(JenkinsCredentialDataTransformer jenkinsCredentialDataTransformer, JenkinsProviderAdapter jenkinsProviderAdapter) {
    this.jenkinsCredentialDataTransformer = jenkinsCredentialDataTransformer
    this.jenkinsProviderAdapter = jenkinsProviderAdapter
  }

  /**
   * {@inheritDoc}
   */
  @Override
  List<CharSequence> getSupportedCredentialTypes() {
    return [UsernamePasswordCredential.ALIAS, SshPrivateKeyCredential.ALIAS, TokenCredential.ALIAS]
  }

  /**
   * {@inheritDoc}
   */
  @Override
  CharSequence getId() {
    return ALIAS
  }

  @Override
  String toString() {
    return "${this.getClass().getName()};id:${this.getId()};supported:${this.getSupportedCredentialTypes().toString()}"
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected SecretBag doProvide(CredentialURL credentialURL) {
    switch (credentialURL.getCredentialType()) {
      case UsernamePasswordCredential.ALIAS:
        return this.fetchUsernamePassword(credentialURL.getCredentialId())
      case SshPrivateKeyCredential.ALIAS:
        return this.fetchSshPrivateKey(credentialURL.getCredentialId())
      case TokenCredential.ALIAS:
        return this.fetchToken(credentialURL.getCredentialId())
      default:
        throw new CredentialUnsupported('Unsupported credential. This is a bug, code should never be reached.')
    }
  }

  private UsernamePasswordCredential fetchUsernamePassword(CharSequence credentialId) {
    return this.jenkinsCredentialDataTransformer.transform(this.fetchCredential(StandardUsernameCredentials.class, credentialId) as UsernamePasswordCredentialsImpl)
  }

  private SshPrivateKeyCredential fetchSshPrivateKey(CharSequence credentialId) {
    return this.jenkinsCredentialDataTransformer.transform(this.fetchCredential(BasicSSHUserPrivateKey.class, credentialId) as BasicSSHUserPrivateKey)
  }

  private TokenCredential fetchToken(CharSequence credentialId) {
    // Could manage other types of token; no choice but to enumerate them
    for (classname in [
      "com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl",
      "io.jenkins.plugins.gitlabserverconfig.credentials.PersonalAccessTokenImpl"
    ]) {
      if (this.classExists(classname)) {
        return this.jenkinsCredentialDataTransformer.transformToken(this.fetchCredential(this.class.classLoader.loadClass(classname), credentialId))
      }
    }
  }

  private BaseStandardCredentials fetchCredential(Class type, CharSequence credentialId) {
    BaseStandardCredentials credential = this.jenkinsProviderAdapter.lookupCredentials(type).find {
      it.getId() == credentialId
    }

    if (null == credential) {
      throw new CredentialNotFound("Credential “${credentialId}” not found in ${this.getId()}.")
    }

    return credential
  }

  private classExists(String className) {
    try {
      this.class.classLoader.loadClass(className)
      return true
    } catch (ClassNotFoundException ignored) {
      return false
    }
  }
}
