package com.wildbeavers.credentials.provider.impl

import com.cloudbees.groovy.cps.SerializableScript
import com.cloudbees.plugins.credentials.CredentialsProvider
import com.cloudbees.plugins.credentials.impl.BaseStandardCredentials
import jenkins.model.Jenkins
import hudson.model.Job

/**
 * Adapt static Jenkins lookup credential method.
 */
class JenkinsProviderAdapter {
  private SerializableScript context

  JenkinsProviderAdapter(SerializableScript context) {
    this.context = context
  }

  List<BaseStandardCredentials> lookupCredentials(Class credentialClass) {
    return (CredentialsProvider.lookupCredentialsInItemGroup(credentialClass, Jenkins.get(), null) + CredentialsProvider.lookupCredentialsInItemGroup(credentialClass, Jenkins.get().getItemByFullName(this.context.env.JOB_NAME, Job.class).getParent(), null)) as List<BaseStandardCredentials>
  }
}
