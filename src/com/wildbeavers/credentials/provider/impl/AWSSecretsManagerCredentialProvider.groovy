package com.wildbeavers.credentials.provider.impl

import com.cloudbees.groovy.cps.NonCPS
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.AWSAccessKeyCredential
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UnknownSecretCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.credentials.data_validation.CredentialURL
import com.wildbeavers.credentials.exception.CredentialNotFound
import com.wildbeavers.credentials.exception.CredentialUnsupported
import com.wildbeavers.credentials.provider.CredentialBackendProvider
import com.wildbeavers.data.CommandResult
import com.wildbeavers.data_validation.type.AWSAccountId
import com.wildbeavers.data_validation.type.AWSRegion
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger
import groovy.json.JsonException
import groovy.json.JsonSlurper

/**
 * Implements a CredentialBackendProvider with AWS Secrets Manager service
 */
class AWSSecretsManagerCredentialProvider extends CredentialBackendProvider {
  final static String ALIAS = 'awsSecretsManager'
  final static List<String> EXPECTED_KEYS_USERNAME_PASSWORD = ['username', 'password']
  final static List<String> EXPECTED_KEYS_SSH_PRIVATE_KEY = ['username', 'private_key', 'passphrase']
  final static List<String> EXPECTED_KEYS_AWS_ACCESS_KEY = ['access_key_id', 'access_secret_key']
  final static List<String> EXPECTED_KEYS_TOKEN = ['token']

  private Debugger debugger
  private ContainerRunner containerRunner
  private GenericFactory<Secret> secretFactory
  private GenericFactory<SecretBag> secretBagFactory

  private ContainerOptions containerOptions
  private AWSAccountId awsAccountId
  private AWSRegion awsRegion

  AWSSecretsManagerCredentialProvider(Debugger debugger, ContainerRunner containerRunner, GenericFactory<Secret> secretFactory, GenericFactory<SecretBag> secretBagFactory, ContainerOptions containerOptions, CharSequence awsAccountId, CharSequence awsRegion) {
    this.debugger = debugger
    this.containerRunner = containerRunner
    this.secretFactory = secretFactory
    this.secretBagFactory = secretBagFactory
    this.containerOptions = containerOptions
    this.awsAccountId = new AWSAccountId(awsAccountId)
    this.awsRegion = new AWSRegion(awsRegion)
  }

  /**
   * {@inheritDoc}
   */
  @Override
  List<CharSequence> getSupportedCredentialTypes() {
    return [UsernamePasswordCredential.ALIAS, SshPrivateKeyCredential.ALIAS, AWSAccessKeyCredential.ALIAS, UnknownSecretCredential.ALIAS, TokenCredential.ALIAS]
  }

  /**
   * {@inheritDoc}
   */
  @Override
  CharSequence getId() {
    return ALIAS
  }

  @Override
  String toString() {
    return "${this.getClass().getName()};id:${this.getId()};supported:${this.getSupportedCredentialTypes().toString()}"
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected SecretBag doProvide(CredentialURL credentialURL) {
    switch (credentialURL.getCredentialType()) {
      case UsernamePasswordCredential.ALIAS:
        return this.fetchUsernamePassword(credentialURL.getCredentialId())
      case SshPrivateKeyCredential.ALIAS:
        return this.fetchSshPrivateKey(credentialURL.getCredentialId())
      case AWSAccessKeyCredential.ALIAS:
        return this.fetchAWSAccessKeyCredential(credentialURL.getCredentialId())
      case TokenCredential.ALIAS:
        return this.fetchTokenCredential(credentialURL.getCredentialId())
      case UnknownSecretCredential.ALIAS:
        return this.fetchUnknownSecretCredential(credentialURL.getCredentialId())
      default:
        throw new CredentialUnsupported('Unsupported credential. This is a bug, code should never be reached.')
    }
  }

  private CommandResult fetchCredential(CharSequence credentialId) {
    try {
      this.containerOptions.setQuiet(true)
      return this.containerRunner.run(this.containerOptions, "secretsmanager get-secret-value --region ${this.awsRegion} --secret-id 'arn:aws:secretsmanager:${this.awsRegion}:${this.awsAccountId}:secret:${credentialId}' --query 'SecretString' --output text")
    } catch (CommandExecutionException commandExecutionException) {
      String message = "Credential “${credentialId}” not found in “${this.getId()}”. Command failed: ${commandExecutionException.getMessage()}"
      this.debugger.printDebug(message)
      throw new CredentialNotFound(message)
    }
  }

  private Map fetchJSONCredential(CharSequence credentialId) {
    try {
      // For some obscure reasons, this expression below cannot be directly input into the JsonSlurper "parseText"
      // The intermediate "result" variable seems to be mandatory when run inside Jenkins CPS
      // Maybe because CPS interprets and performs checks before the groovy interpreter
      String result = this.fetchCredential(credentialId).getStdout()
      return new JsonSlurper().parseText(result) as Map
    } catch (JsonException | IllegalArgumentException ignored) {
      throw new CredentialUnsupported("Cannot parse credential: “${credentialId}” from “${this.getId()}”. Raw credential is not a valid JSON.")
    }
  }

  private verifyJSONExpectedKeys(CharSequence credentialId, List<String> expectedKeys, Map parsedJSON) {
    expectedKeys.each { String expectedKey ->
      if (!parsedJSON.keySet().contains(expectedKey)) {
        throw new CredentialUnsupported("Cannot parse credential: “${credentialId}” from “${this.getId()}”. “${expectedKey}” key is missing from the raw credential JSON.")
      }
      if (!(parsedJSON.get(expectedKey) instanceof CharSequence)) {
        throw new CredentialUnsupported("Cannot parse credential: “${credentialId}” from “${this.getId()}”. “${expectedKey}” key was found but does not contain a flat string.")
      }
    }
  }

  private UsernamePasswordCredential fetchUsernamePassword(CharSequence credentialId) {
    Map parsedJSON = this.fetchJSONCredential(credentialId)

    this.verifyJSONExpectedKeys(credentialId, EXPECTED_KEYS_USERNAME_PASSWORD, parsedJSON)

    return this.secretBagFactory.instantiate('UsernamePasswordCredential',
      this.secretFactory.instantiate('username', parsedJSON.get('username')),
      this.secretFactory.instantiate('password', parsedJSON.get('password')),
    )
  }

  private SshPrivateKeyCredential fetchSshPrivateKey(CharSequence credentialId) {
    Map parsedJSON = this.fetchJSONCredential(credentialId)

    this.verifyJSONExpectedKeys(credentialId, EXPECTED_KEYS_SSH_PRIVATE_KEY, parsedJSON)

    return this.secretBagFactory.instantiate('SshPrivateKeyCredential',
      this.secretFactory.instantiate('sshPrivateKey', parsedJSON.get('private_key')),
      this.secretFactory.instantiate('sshPrivateKeyUsername', parsedJSON.get('username')),
      this.secretFactory.instantiate('sshPassphrase', parsedJSON.get('passphrase')),
    )
  }

  private AWSAccessKeyCredential fetchAWSAccessKeyCredential(CharSequence credentialId) {
    Map parsedJSON = this.fetchJSONCredential(credentialId)

    this.verifyJSONExpectedKeys(credentialId, EXPECTED_KEYS_AWS_ACCESS_KEY, parsedJSON)

    return this.secretBagFactory.instantiate('AWSAccessKeyCredential',
      this.secretFactory.instantiate('awsAccessKeyId', parsedJSON.get('access_key_id')),
      this.secretFactory.instantiate('awsAccessSecretKey', parsedJSON.get('access_secret_key')),
    )
  }

  private TokenCredential fetchTokenCredential(CharSequence credentialId) {
    Map parsedJSON = this.fetchJSONCredential(credentialId)

    this.verifyJSONExpectedKeys(credentialId, EXPECTED_KEYS_TOKEN, parsedJSON)

    return this.secretBagFactory.instantiate('TokenCredential',
      this.secretFactory.instantiate('token', parsedJSON.get('token')),
    )
  }

  private UnknownSecretCredential fetchUnknownSecretCredential(CharSequence credentialId) {
    return this.secretBagFactory.instantiate('UnknownSecretCredential',
      this.secretFactory.instantiate('unknown', this.fetchCredential(credentialId).getStdout()),
    )
  }
}
