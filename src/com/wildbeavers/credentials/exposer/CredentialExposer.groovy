package com.wildbeavers.credentials.exposer

import com.wildbeavers.composite.Fetcher
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data_validation.ExpositionLocation

class CredentialExposer {
  public static CharSequence ALIAS = 'UNDEFINED'

  private Fetcher<SecretExposer, ExpositionLocation, String> secretExposerFetcher

  CredentialExposer(Fetcher<SecretExposer, ExpositionLocation, String> secretExposerFetcher) {
    this.secretExposerFetcher = secretExposerFetcher
  }

  final void expose(SecretBag secretBag) {
    for (secret in secretBag.getSecrets().values()) {
      if (secret.isExposedAtAllLocations()) {
        continue
      }

      if (!secret.hasExpositionLocations()) {
        secret.addExpositionLocation(secret.getDefaultExpositionLocation())
      }

      for (secretExpositionLocation in secret.getExpositionLocations().keySet()) {
        this.secretExposerFetcher.fetchFirstSupporting(secretExpositionLocation).expose(secret, secretExpositionLocation)
      }
    }
  }

  void conceal(SecretBag secretBag) {
    for (secret in secretBag.getSecrets().values()) {
      for (secretExpositionLocation in secret.getExpositionLocations().keySet()) {
        this.secretExposerFetcher.fetchFirstSupporting(secretExpositionLocation).conceal(secret, secretExpositionLocation)
      }
    }
  }
}
