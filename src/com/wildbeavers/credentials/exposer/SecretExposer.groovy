package com.wildbeavers.credentials.exposer

import com.wildbeavers.composite.Fetchable
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data_validation.ExpositionLocation

/**
 * Abstract class to expose/conceal a Secret to a specific ExpositionLocation/
 */
abstract class SecretExposer implements Fetchable<ExpositionLocation, CharSequence> {
  /**
   * Exposes a $secretToExpose (and mark it as exposed) to $expositionLocation.
   * Specific implementation is the doExpose method.
   * @throw SecretExpositionException if exposition failed
   */
  final void expose(Secret secretToExpose, ExpositionLocation expositionLocation) {
    if (!this.support(expositionLocation) || secretToExpose.isExposedInLocation(expositionLocation)) {
      return
    }

    this.doExpose(secretToExpose, expositionLocation)
    secretToExpose.markAsExposed(expositionLocation)
  }

  /**
   * Specific implementation to expose a $secret in $expositionLocation
   * @throw SecretExpositionException if exposition failed
   */
  abstract protected void doExpose(Secret secret, ExpositionLocation expositionLocation)

  /**
   * Conceals a $secretToExpose (and mark it as concealed) from $expositionLocation.
   * Specific implementation is the doConceal method.
   * @throw SecretExpositionException if concealing failed
   */
  void conceal(Secret secretToConceal, ExpositionLocation expositionLocation) {
    if (!this.support(expositionLocation) || !secretToConceal.isExposedInLocation(expositionLocation)) {
      return
    }

    this.doConceal(secretToConceal, expositionLocation)
    secretToConceal.markAsConcealed(expositionLocation)
  }

  /**
   * Specific implementation to conceal a $secret from $expositionLocation
   * @throw SecretExpositionException if concealing failed
   */
  abstract protected void doConceal(Secret secret, ExpositionLocation expositionLocation)
}
