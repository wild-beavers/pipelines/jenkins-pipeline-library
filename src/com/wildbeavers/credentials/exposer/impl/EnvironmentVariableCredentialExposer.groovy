package com.wildbeavers.credentials.exposer.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import com.wildbeavers.credentials.exposer.SecretExposer
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy

/**
 * Expose or conceal secret in system environment variables
 */
class EnvironmentVariableCredentialExposer extends SecretExposer {
  static final ALIAS = 'environmentVariable'

  private EnvironmentVariableProxy environmentVariableProxy
  private Debugger debugger

  EnvironmentVariableCredentialExposer(EnvironmentVariableProxy environmentVariableProxy, Debugger debugger) {
    this.environmentVariableProxy = environmentVariableProxy
    this.debugger = debugger
  }

  /**
   * {@inheritDoc}
   */
  @Override
  CharSequence getId() {
    return ALIAS
  }

  /**
   * {@inheritDoc}
   */
  @Override
  Boolean support(ExpositionLocation expositionLocation) {
    return expositionLocation.getExpositionType() == ALIAS
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void doExpose(Secret secret, ExpositionLocation expositionLocation) {
    this.environmentVariableProxy.set(expositionLocation.getLocation().toString(), secret.getContent())
  }

  /**
   * {@inheritDoc}
   */
  @Override
  void doConceal(Secret secret, ExpositionLocation expositionLocation) {
    if (!this.environmentVariableProxy.exist(expositionLocation.getLocation().toString())) {
      this.debugger.print("WARNING: Unable to conceal “${expositionLocation.getLocation()}”. Secret “" + secret + "” is not exposed as environment variable. Skipping concealing.")
      return
    }

    environmentVariableProxy.unset(expositionLocation.getLocation().toString())
  }
}
