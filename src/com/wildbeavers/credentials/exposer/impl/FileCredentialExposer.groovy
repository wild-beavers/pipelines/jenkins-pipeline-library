package com.wildbeavers.credentials.exposer.impl

import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import com.wildbeavers.credentials.exception.SecretExpositionException
import com.wildbeavers.credentials.exposer.SecretExposer
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.ExecuteProxy
import com.wildbeavers.proxy.FileProxy

/**
 * This implements a CredentialExposer by exposing to a file
 */
class FileCredentialExposer extends SecretExposer {
  static final ALIAS = 'file'

  private FileProxy fileProxy
  private ExecuteProxy executeProxy
  private Debugger debugger
  private EnvironmentVariableProxy environmentVariableProxy

  FileCredentialExposer(FileProxy fileProxy, ExecuteProxy executeProxy, Debugger debugger, EnvironmentVariableProxy environmentVariableProxy) {
    this.fileProxy = fileProxy
    this.executeProxy = executeProxy
    this.debugger = debugger
    this.environmentVariableProxy = environmentVariableProxy
  }

  /**
   * {@inheritDoc}
   */
  @Override
  CharSequence getId() {
    return ALIAS
  }

  /**
   * {@inheritDoc}
   */
  @Override
  Boolean support(ExpositionLocation expositionLocation) {
    return expositionLocation.getExpositionType() == ALIAS
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void doExpose(Secret secret, ExpositionLocation expositionLocation) {
    if (this.fileProxy.exists(expositionLocation.getLocation())) {
      throw new SecretExpositionException("The secret “${secret}” is not mark as exposed, but a file already exists at “${expositionLocation.getLocation()}”. To avoid data loss, no file were overwitten. Cancelling secret exposition.")
    }

    String randomEnvironmentVariableName = this.environmentVariableProxy.setWithRandomName(secret.getContent())
    this.executeProxy.execute("echo \${${randomEnvironmentVariableName}} |tee \"${expositionLocation.getLocation()}\"")
    this.environmentVariableProxy.unset(randomEnvironmentVariableName)

    this.executeProxy.execute("chmod 400 \"${expositionLocation.getLocation()}\"")
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void doConceal(Secret secret, ExpositionLocation expositionLocation) {
    if (!this.fileProxy.exists(expositionLocation.getLocation())) {
      this.debugger.print("WARNING: Unable to conceal “${expositionLocation.getLocation()}”. The secret “${secret}” is not mark as concealed but the file doesn't exist at the expected location “${expositionLocation.getLocation()}” and thus secret is already concealed.")
      return
    }

    this.executeProxy.execute("rm -f \"${expositionLocation.getLocation()}\"")
  }
}
