package com.wildbeavers.credentials.transformer

import com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey
import com.cloudbees.plugins.credentials.impl.BaseStandardCredentials
import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.SshPrivateKeyCredential
import com.wildbeavers.credentials.data.impl.TokenCredential
import com.wildbeavers.credentials.data.impl.UsernamePasswordCredential
import com.wildbeavers.di.GenericFactory
import hudson.util.Secret as HudsonSecret

/**
 * Transform Jenkins Credentials to WildBeavers Credentials
 */
class JenkinsCredentialDataTransformer {
  private GenericFactory<Secret> secretFactory
  private GenericFactory<SecretBag> secretBagFactory

  JenkinsCredentialDataTransformer(GenericFactory<Secret> secretFactory, GenericFactory<SecretBag> secretBagFactory) {
    this.secretFactory = secretFactory
    this.secretBagFactory = secretBagFactory
  }

  /**
   * Transform a BasicSSHUserPrivateKey object in SshPrivateKeyCredential object
   * @param basicSSHUserPrivateKey
   * @return SshPrivateKeyCredential
   */
  SshPrivateKeyCredential transform(BasicSSHUserPrivateKey basicSSHUserPrivateKey) {
    return this.secretBagFactory.instantiate('SshPrivateKeyCredential',
      this.secretFactory.instantiate('sshPrivateKey', basicSSHUserPrivateKey.getPrivateKeys()[0]),
      this.secretFactory.instantiate('sshPrivateKeyUsername', basicSSHUserPrivateKey.getUsername()),
      this.secretFactory.instantiate('sshPassphrase', HudsonSecret.toString(basicSSHUserPrivateKey.getPassphrase())),
    )
  }

  /**
   * Transform a UsernamePasswordCredentialsImpl object in UsernamePasswordCredential object
   * @param basicSSHUserPrivateKey
   * @return UsernamePasswordCredential
   */
  UsernamePasswordCredential transform(UsernamePasswordCredentialsImpl usernamePasswordCredentialsImpl) {
    return this.secretBagFactory.instantiate('UsernamePasswordCredential',
      this.secretFactory.instantiate('username', usernamePasswordCredentialsImpl.getUsername().toString()),
      this.secretFactory.instantiate('password', usernamePasswordCredentialsImpl.getPassword().getPlainText()),
    )
  }

  /**
   * Transform any kind of token credential object in TokenCredential object
   * @param BaseStandardCredentials
   * @return TokenCredential
   */
  TokenCredential transformToken(BaseStandardCredentials anyToken) {
    // anyToken argument can be any kind of BaseStandardCredentials and therefore we cannot strongly type it.
    // The problem is then to know what method returns the actual token.
    // The try/catch below attempt known-to-work method calls, but they may be something else.
    // There are no reliable way to know in advance what plugin’s credential object will name their methods.
    Secret token

    try {
      token = this.secretFactory.instantiate('token', anyToken.getToken().getPlainText())
    } catch (MissingMethodException ignored) {
      token = this.secretFactory.instantiate('token', anyToken.getApiToken().getPlainText())
    }

    return this.secretBagFactory.instantiate('TokenCredential', token) as TokenCredential
  }
}
