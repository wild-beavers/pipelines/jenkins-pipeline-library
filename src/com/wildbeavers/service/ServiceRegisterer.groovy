package com.wildbeavers.service

import com.wildbeavers.archiver.Archiver
import com.wildbeavers.archiver.ArchiverCollection
import com.wildbeavers.archiver.TarGzArchiver
import com.wildbeavers.artifact.AWSS3
import com.wildbeavers.artifact.ArtifactStore
import com.wildbeavers.artifact.ArtifactStoreCollection
import com.wildbeavers.artifact.Git
import com.wildbeavers.auto_tagger.AutoTagger
import com.wildbeavers.bash.event_data.BashEventData
import com.wildbeavers.bash.listener.BashDataValidationListener
import com.wildbeavers.butane.command.Butane
import com.wildbeavers.butane.event_data.ButaneEventData
import com.wildbeavers.butane.listener.ButaneDataValidationListener
import com.wildbeavers.butane.listener.ButaneToIgnitionListener
import com.wildbeavers.command.Inspec
import com.wildbeavers.composite.Fetcher
import com.wildbeavers.composite.FetcherDeprecated
import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.ContainerRegistryOption
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.container.event_data.ContainerEventData
import com.wildbeavers.container.event_data.ContainerLoginData
import com.wildbeavers.container.event_data.ContainerPodmanEventData
import com.wildbeavers.container.listener.*
import com.wildbeavers.credentials.CredentialLoader
import com.wildbeavers.credentials.data.Secret
import com.wildbeavers.credentials.data.SecretBag
import com.wildbeavers.credentials.data.impl.*
import com.wildbeavers.credentials.data_validation.ExpositionLocation
import com.wildbeavers.credentials.exposer.CredentialExposer
import com.wildbeavers.credentials.exposer.SecretExposer
import com.wildbeavers.credentials.exposer.impl.EnvironmentVariableCredentialExposer
import com.wildbeavers.credentials.exposer.impl.FileCredentialExposer
import com.wildbeavers.credentials.provider.impl.AWSSecretsManagerCredentialProvider
import com.wildbeavers.credentials.provider.impl.JenkinsCredentialProvider
import com.wildbeavers.credentials.provider.impl.JenkinsProviderAdapter
import com.wildbeavers.credentials.transformer.JenkinsCredentialDataTransformer
import com.wildbeavers.data.EmptyScmInfo
import com.wildbeavers.data.ScmInfo
import com.wildbeavers.data.artifact.ArtifactData
import com.wildbeavers.data.artifact.ArtifactFetchingData
import com.wildbeavers.data.artifact.ArtifactPublisherData
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.data.lint.LinterData
import com.wildbeavers.data.pre_commit.PreCommitData
import com.wildbeavers.data_manager.ContainerOptionsManager
import com.wildbeavers.data_manager.ScmInfoManager
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.dependency_installer.DependencyInstallerDataManagerFetcher
import com.wildbeavers.dependency_installer.DependencyInstallerFetcher
import com.wildbeavers.deprecation.DeprecatedFunction
import com.wildbeavers.deprecation.DeprecatedMessage
import com.wildbeavers.di.IOC
import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.helper.CurrentUser
import com.wildbeavers.helper.FoolProofValidationHelper
import com.wildbeavers.helper.SSHKeySetupHelper
import com.wildbeavers.io.Debugger
import com.wildbeavers.io.File
import com.wildbeavers.listener.*
import com.wildbeavers.listener.wrapper.standard.ScmCredentialWrapper
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformApplyWrapper
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformInitSSHWrapper
import com.wildbeavers.listener.wrapper.terraform.AWSTerraformInitWrapper
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.observer.EventListenerBag
import com.wildbeavers.proxy.*
import com.wildbeavers.python.PythonBuildDataManager
import com.wildbeavers.python.PythonBuildDataManagerFetcher
import com.wildbeavers.python.PythonPytestDataManager
import com.wildbeavers.python.PythonTestDataManagerFetcher
import com.wildbeavers.python.builder.PythonBuildBuilder
import com.wildbeavers.python.builder.PythonBuilderFetcher
import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.command.Pytest
import com.wildbeavers.python.command.PythonBuild
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.python.dependency_installer.Venv
import com.wildbeavers.python.dependency_installer.VenvDependencyInstallerDataManager
import com.wildbeavers.python.listener.PythonBuilderListener
import com.wildbeavers.python.listener.PythonTestsListener
import com.wildbeavers.python.tests.PythonTestsFetcher
import com.wildbeavers.python.tests.PythonTestsPytest
import com.wildbeavers.python.tests.PythonTestsType
import com.wildbeavers.terraform.command.Terraform
import com.wildbeavers.terraform.event_data.TerraformEventData
import com.wildbeavers.terraform.listener.*
import com.wildbeavers.undefined.data.UndefinedData

class ServiceRegisterer {
  static alreadyRegistered = false

  static void init(Script context) {
    if (alreadyRegistered) {
      return
    }

    registerAllGlobals()
    registerAllClasses(context)
  }

  static void registerAllClasses(Script context) {
    if (alreadyRegistered) {
      return
    }

    // Core framework
    registerCommand()
    registerComposite()
    registerContext(context)
    registerData()
    registerDataManagers()
    registerHelper()
    registerIO()
    registerObserver()
    registerProxy()

    // Components
    registerArchiver()
    registerArtifactStore()
    registerCredential()
    registerDataValidation()
    registerDependencyInstaller()
    registerDeprecation()
    registerEventListenerWrappers()
    registerListeners()

    // Pipelines
    registerBash()
    registerButane()
    registerContainer()
    registerPython()
    registerTerraform()
    registerUndefined()

    alreadyRegistered = true
  }

  /**
   * Register global variables. Avoid this as much as possible. Use them if needed in class constructor only
   */
  static void registerAllGlobals() {
    try {
      def systemEnv = System.getenv()

      def jenkinsLocal = false
      if (systemEnv['JENKINS_LOCAL'] != null) {
        jenkinsLocal = true
      }

      IOC.registerGlobal('JENKINS_LOCAL', jenkinsLocal)

      def dockerDataBasePath = '$(pwd)'
      if (systemEnv['JENKINS_DOCKER_DATA_BASEPATH'] != null) {
        dockerDataBasePath = systemEnv['JENKINS_DOCKER_DATA_BASEPATH']
      }

      IOC.registerGlobal('JENKINS_DOCKER_DATA_BASEPATH', dockerDataBasePath)
    } catch (ignored) {
      IOC.registerGlobal('JENKINS_LOCAL', false)
      IOC.registerGlobal('JENKINS_DOCKER_DATA_BASEPATH', '$(pwd)')
    }
  }

  static void registerCommand() {
    IOC.registerSingleton(Butane, {
      return new Butane(IOC.get(OptionStringFactory), IOC.get(ContainerRunner), IOC.get(Debugger))
    })
    IOC.registerSingleton(Inspec, {
      return new Inspec(IOC.get(OptionStringFactory), IOC.get(ContainerRunner), IOC.get(Debugger))
    })
    IOC.registerSingleton(Pytest, {
      return new Pytest(IOC.get(OptionStringFactory), IOC.get(ContainerRunner), IOC.get(Debugger))
    })
    IOC.registerSingleton(PythonBuild, {
      return new PythonBuild(IOC.get(OptionStringFactory), IOC.get(ContainerRunner), IOC.get(Debugger))
    })
    IOC.registerSingleton(Terraform, {
      return new Terraform(IOC.get(OptionStringFactory), IOC.get(ContainerRunner), IOC.get(Debugger))
    })
  }

  static void registerComposite() {
    IOC.register(Fetcher, {
      return new Fetcher()
    })
    IOC.register(FetcherDeprecated, {
      return new FetcherDeprecated(IOC.get(Debugger))
    })
  }

  static void registerContext(Script context) {
    IOC.registerSingleton('@context', {
      return context
    })
  }

  static void registerData() {
    IOC.register(ArtifactData, {
      return new ArtifactData()
    })
    IOC.registerFactory(ArtifactData)
    IOC.bindProductLine(ArtifactData, '', {
      return IOC.get(ArtifactData)
    })
    IOC.register(ArtifactFetchingData, {
      return new ArtifactFetchingData()
    })
    IOC.registerFactory(ArtifactFetchingData)
    IOC.bindProductLine(ArtifactFetchingData, '', {
      return IOC.get(ArtifactFetchingData)
    })
    IOC.register(ContainerLoginData, {
      return new ContainerLoginData()
    })
    IOC.registerFactory(ContainerLoginData)
    IOC.bindProductLine(ContainerLoginData, '', {
      return IOC.get(ContainerLoginData)
    })
    IOC.register(ContainerOptions, { CharSequence runtime = ContainerOptions.DEFAULT_TOOL, CharSequence fqdn = ContainerOptions.DEFAULT_FQDN, CharSequence imageName = ContainerOptions.DEFAULT_IMAGE_NAME, CharSequence imageTag = ContainerOptions.DEFAULT_IMAGE_TAG ->
      return new ContainerOptions(runtime, fqdn, imageName, imageTag)
    })
    IOC.registerFactory(ContainerOptions)
    IOC.bindProductLine(ContainerOptions, '', {
      return IOC.get(ContainerOptions, ContainerOptions.DEFAULT_TOOL, ContainerOptions.DEFAULT_FQDN, ContainerOptions.DEFAULT_IMAGE_NAME, ContainerOptions.DEFAULT_IMAGE_TAG)
    })
    IOC.bindProductLine(ContainerOptions, 'pre-commit', {
      ContainerOptions containerOptions = IOC.get(ContainerOptions, PreCommitData.DEFAULT_CONTAINER_TOOL, PreCommitData.DEFAULT_CONTAINER_FQDN, PreCommitData.DEFAULT_CONTAINER_IMAGE_NAME, PreCommitData.DEFAULT_CONTAINER_IMAGE_TAG)
      containerOptions.setUserns(PreCommitData.DEFAULT_CONTAINER_USERNS)
      containerOptions.setFallbackCommand(PreCommitData.DEFAULT_CONTAINER_FALLBACK_COMMAND)
      return containerOptions
    })
    IOC.bindProductLine(ContainerOptions, 'hadolint', {
      ContainerOptions containerOptions = IOC.get(ContainerOptions, ContainerOptions.DEFAULT_TOOL, ContainerEventData.DEFAULT_LINTER_CONTAINER_FQDN, ContainerEventData.DEFAULT_LINTER_CONTAINER_IMAGE_NAME, ContainerEventData.DEFAULT_LINTER_CONTAINER_IMAGE_TAG)
      containerOptions.setFallbackCommand(ContainerEventData.DEFAULT_LINTER_FALLBACK_COMMAND)
      return containerOptions
    })
    IOC.bindProductLine(ContainerOptions, 'terraform', {
      ContainerOptions containerOptions = IOC.get(ContainerOptions, TerraformEventData.DEFAULT_CONTAINER_TOOL, TerraformEventData.DEFAULT_CONTAINER_FQDN, TerraformEventData.DEFAULT_CONTAINER_IMAGE_NAME, TerraformEventData.DEFAULT_CONTAINER_IMAGE_TAG)
      containerOptions.addEnvironmentVariables(['TF_*': null])
      containerOptions.setFallbackCommand(TerraformEventData.DEFAULT_CONTAINER_FALLBACK_COMMAND)
      return containerOptions
    })
    IOC.bindProductLine(ContainerOptions, 'butane', {
      ContainerOptions containerOptions = IOC.get(ContainerOptions, ButaneEventData.DEFAULT_CONTAINER_TOOL, ButaneEventData.DEFAULT_CONTAINER_FQDN, ButaneEventData.DEFAULT_CONTAINER_IMAGE_NAME, ButaneEventData.DEFAULT_CONTAINER_IMAGE_TAG)
      containerOptions.setFallbackCommand(ButaneEventData.DEFAULT_CONTAINER_FALLBACK_COMMAND)
      return containerOptions
    })
    IOC.bindProductLine(ContainerOptions, 'aws', {
      ContainerOptions containerOptions = IOC.get(ContainerOptions, ContainerPodmanEventData.DEFAULT_TOOL, 'registry.gitlab.com/wild-beavers/docker', 'aws-cli', 'latest')
      containerOptions.setFallbackCommand('aws')
      containerOptions.addEnvironmentVariables(['AWS_*': null])
      // Podman seems to fail randomly with transient store
      // containerOptions.setTransientStore(true)
      containerOptions.setLogDriver('none')
      containerOptions.setAssumeRuntimeInstalled(true)
      containerOptions.setQuiet(true)
      return containerOptions
    })
    IOC.register(ContainerRegistryOption, {
      return new ContainerRegistryOption(IOC.getFactory(ContainerOptions).instantiate())
    })
    IOC.registerFactory(ContainerRegistryOption)
    IOC.bindProductLine(ContainerRegistryOption, '', {
      return IOC.get(ContainerRegistryOption)
    })
    IOC.register(FileStandardData, {
      return new FileStandardData()
    })
    IOC.registerFactory(FileStandardData)
    IOC.bindProductLine(FileStandardData, '', {
      return IOC.get(FileStandardData)
    })
    IOC.register(LinterData, { ContainerOptions containerOptions ->
      return new LinterData(containerOptions)
    })
    IOC.registerFactory(LinterData)
    IOC.bindProductLine(LinterData, '', {
      return IOC.get(LinterData, new ContainerOptions())
    })
    IOC.bindProductLine(LinterData, 'hadolint', {
      return IOC.get(LinterData, IOC.getFactory(ContainerOptions).instantiate('hadolint'))
    })
    IOC.register(PreCommitData, {
      return new PreCommitData(IOC.getFactory(ContainerOptions).instantiate('pre-commit'))
    })
    IOC.registerFactory(PreCommitData)
    IOC.bindProductLine(PreCommitData, '', {
      return IOC.get(PreCommitData)
    })
    IOC.registerSingleton(ScmInfo, {
      return new EmptyScmInfo()
    })

    IOC.register(ArtifactPublisherData, {
      return new ArtifactPublisherData()
    })
    IOC.registerFactory(ArtifactPublisherData)
    IOC.bindProductLine(ArtifactPublisherData, '', {
      return IOC.get(ArtifactPublisherData)
    })
  }

  static void registerDataManagers() {
    IOC.registerSingleton(ScmInfoManager, {
      return new ScmInfoManager(IOC.get('@context'), IOC.get(Debugger), IOC.get(ExecuteProxy), IOC.get(EnvironmentVariableProxy))
    })
    IOC.registerSingleton(ContainerOptionsManager, {
      return new ContainerOptionsManager()
    })
  }

  static void registerHelper() {
    IOC.registerSingleton(AutoTagger, {
      return new AutoTagger(IOC.get(ExecuteProxy), IOC.get(Debugger), IOC.get(FileProxy), IOC.get(ScmInfo), IOC.get(ScmInfoManager))
    })
    IOC.registerSingleton(ContainerRunner, {
      return new ContainerRunner(IOC.get(Debugger), IOC.get(ExecuteProxy), IOC.get('JENKINS_DOCKER_DATA_BASEPATH'))
    })
    IOC.registerSingleton(File, {
      return new File()
    })

    IOC.register(FoolProofValidationHelper, {
      return new FoolProofValidationHelper(IOC.get('@context'), IOC.get(TimeoutProxy), new Random())
    })
    IOC.registerSingleton(SSHKeySetupHelper, {
      return new SSHKeySetupHelper(IOC.get(Debugger), IOC.get(ExecuteProxy), IOC.get(EnvironmentVariableProxy))
    })

    IOC.register(OptionStringFactory, {
      return new OptionStringFactory()
    })
  }

  static void registerIO() {
    IOC.registerSingleton(Debugger, {
      return new Debugger(IOC.get(PrintProxy), IOC.get(EnvironmentVariableProxy))
    })
    IOC.registerSingleton(File, {
      return new File()
    })
  }

  static void registerObserver() {
    IOC.register(EventListenerBag, {
      return new EventListenerBag()
    })
    IOC.registerSingleton(EventDispatcher, {
      return new EventDispatcher(IOC.get(EventListenerBag), IOC.get(Debugger))
    })
  }

  static void registerProxy() {
    IOC.registerSingleton(CurrentUser, {
      return new CurrentUser(IOC.get('@context'))
    })
    IOC.registerSingleton(EnvironmentVariableProxy, {
      return new EnvironmentVariableProxy(IOC.get('@context'), IOC.get(PrintProxy))
    })
    IOC.registerSingleton(ExecuteProxy, {
      return new ExecuteProxy(IOC.get(Debugger), IOC.get('@context'))
    })
    IOC.registerSingleton(FileProxy, {
      return new FileProxy(IOC.get('@context'))
    })
    IOC.registerSingleton(ParallelProxy, {
      return new ParallelProxy(IOC.get('@context'))
    })
    IOC.registerSingleton(PrintProxy, {
      return new PrintProxy(IOC.get('@context'))
    })
    IOC.registerSingleton(TimeoutProxy, {
      return new TimeoutProxy(IOC.get('@context'))
    })
  }

  static void registerArchiver() {
    IOC.registerSingleton(Archiver, {
      return new Archiver(IOC.get(ArchiverCollection))
    })
    IOC.registerSingleton(ArchiverCollection, {
      def archiverCollection = new ArchiverCollection()
      archiverCollection.addArchiver(IOC.get(TarGzArchiver))
      return archiverCollection
    })
    IOC.registerSingleton(TarGzArchiver, {
      return new TarGzArchiver(IOC.get(ExecuteProxy))
    })
  }

  static void registerArtifactStore() {
    IOC.registerSingleton(ArtifactStore, {
      return new ArtifactStore(IOC.get(ArtifactStoreCollection))
    })
    IOC.registerSingleton(ArtifactStoreCollection, {
      def artifactStoreCollection = new ArtifactStoreCollection()
      artifactStoreCollection.addArtifactStore(IOC.get(AWSS3))
      artifactStoreCollection.addArtifactStore(IOC.get(Git))
      return artifactStoreCollection
    })

    IOC.registerSingleton(AWSS3, {
      return new AWSS3(IOC.get(FileProxy), IOC.get(File), IOC.get(ContainerRunner), IOC.getFactory(ContainerOptions), IOC.get(DataValidator))
    })
    IOC.registerSingleton(Git, {
      return new Git(IOC.get(ExecuteProxy))
    })

    IOC.registerSingleton(ArtifactFetcherDataValidationListener, {
      return new ArtifactFetcherDataValidationListener(IOC.get(DataValidator), IOC.getFactory(ArtifactFetchingData), IOC.getFactory(ArtifactData))
    })
    IOC.registerSingleton(ArtifactFetcherListener, {
      return new ArtifactFetcherListener(IOC.get(Debugger), IOC.get(ArtifactStore), IOC.get(Archiver), IOC.get(ParallelProxy))
    })
    IOC.registerSingleton(ArtifactPublisherListener, {
      return new ArtifactPublisherListener(IOC.get(Debugger), IOC.get(Archiver), IOC.get(ExecuteProxy), IOC.get(ArtifactStore), IOC.get(ParallelProxy), IOC.get(ContainerRunner), IOC.get(ScmInfo), IOC.getFactory(ArtifactData))
    })
  }

  static void registerCredential() {
    IOC.register(Secret, { String content, String description, ExpositionLocation expositionLocation ->
      return new Secret(content, description, expositionLocation)
    })
    IOC.registerFactory(Secret)
    IOC.bindProductLine(Secret, 'unknown', { CharSequence content ->
      return IOC.get(Secret, content, 'Unknown secret type', new ExpositionLocation(UnknownSecretCredential.SECRET_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'sshPrivateKeyUsername', { CharSequence content ->
      return IOC.get(Secret, content, 'SSH private key associated username', new ExpositionLocation(SshPrivateKeyCredential.USERNAME_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'sshPassphrase', { CharSequence content ->
      return IOC.get(Secret, content, 'SSH passphrase', new ExpositionLocation(SshPrivateKeyCredential.PASSPHRASE_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'sshPrivateKey', { CharSequence content ->
      return IOC.get(Secret, content, 'SSH private key', new ExpositionLocation(SshPrivateKeyCredential.PRIVATE_KEY_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'username', { CharSequence content ->
      return IOC.get(Secret, content, 'Username', new ExpositionLocation(UsernamePasswordCredential.USERNAME_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'password', { CharSequence content ->
      return IOC.get(Secret, content, 'Password', new ExpositionLocation(UsernamePasswordCredential.PASSWORD_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'awsAccessKeyId', { CharSequence content ->
      return IOC.get(Secret, content, 'AWS Access Key ID', new ExpositionLocation(AWSAccessKeyCredential.AWS_ACCESS_KEY_ID_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'awsAccessSecretKey', { CharSequence content ->
      return IOC.get(Secret, content, 'AWS Access Secret Key', new ExpositionLocation(AWSAccessKeyCredential.AWS_ACCESS_SECRET_KEY_SUGGESTED_LOCATION))
    })
    IOC.bindProductLine(Secret, 'token', { CharSequence content ->
      return IOC.get(Secret, content, 'Token', new ExpositionLocation(TokenCredential.TOKEN_SUGGESTED_LOCATION))
    })

    IOC.register(SshPrivateKeyCredential, { Secret username, Secret passphrase, Secret privateKey ->
      SshPrivateKeyCredential credential = new SshPrivateKeyCredential()
      credential.setUsername(username)
      credential.setPassphrase(passphrase)
      credential.setPrivateKey(privateKey)
      return credential
    })
    IOC.register(UnknownSecretCredential, { Secret secret ->
      UnknownSecretCredential credential = new UnknownSecretCredential()
      credential.setUnknownSecret(secret)
      return credential
    })
    IOC.register(UsernamePasswordCredential, { Secret username, Secret password ->
      UsernamePasswordCredential credential = new UsernamePasswordCredential()
      credential.setUsername(username)
      credential.setPassword(password)
      return credential
    })
    IOC.register(AWSAccessKeyCredential, { Secret accessKeyId, Secret accessSecretKey ->
      AWSAccessKeyCredential credential = new AWSAccessKeyCredential()
      credential.setAccessKeyId(accessKeyId)
      credential.setAccessSecretKey(accessSecretKey)
      return credential
    })
    IOC.register(TokenCredential, { Secret token ->
      TokenCredential credential = new TokenCredential()
      credential.setToken(token)
      return credential
    })
    IOC.registerFactory(SecretBag)
    IOC.bindProductLine(SecretBag, 'SshPrivateKeyCredential', { Secret privateKey, Secret username = null, Secret passphrase = null ->
      return IOC.get(SshPrivateKeyCredential, username, passphrase, privateKey)
    })
    IOC.bindProductLine(SecretBag, 'UnknownSecretCredential', { Secret secret ->
      return IOC.get(UnknownSecretCredential, secret)
    })
    IOC.bindProductLine(SecretBag, 'UsernamePasswordCredential', { Secret username, Secret password ->
      return IOC.get(UsernamePasswordCredential, username, password)
    })
    IOC.bindProductLine(SecretBag, 'AWSAccessKeyCredential', { Secret accessKeyId, Secret accessSecretKey ->
      return IOC.get(AWSAccessKeyCredential, accessKeyId, accessSecretKey)
    })
    IOC.bindProductLine(SecretBag, 'TokenCredential', { Secret token ->
      return IOC.get(TokenCredential, token)
    })

    IOC.registerSingleton(FileCredentialExposer, {
      return new FileCredentialExposer(IOC.get(FileProxy), IOC.get(ExecuteProxy), IOC.get(Debugger), IOC.get(EnvironmentVariableProxy))
    })
    IOC.registerSingleton(EnvironmentVariableCredentialExposer, {
      return new EnvironmentVariableCredentialExposer(IOC.get(EnvironmentVariableProxy), IOC.get(Debugger))
    })
    IOC.registerSingleton(CredentialExposer, {
      Fetcher<SecretExposer, ExpositionLocation, String> secretExposerFetcher = IOC.get(Fetcher)
      secretExposerFetcher.register(IOC.get(EnvironmentVariableCredentialExposer))
      secretExposerFetcher.register(IOC.get(FileCredentialExposer))
      return new CredentialExposer(secretExposerFetcher)
    })

    IOC.registerSingleton(JenkinsCredentialDataTransformer, {
      return new JenkinsCredentialDataTransformer(IOC.getFactory(Secret), IOC.getFactory(SecretBag))
    })

    IOC.register(AWSSecretsManagerCredentialProvider, { CharSequence containerOptionDefinitionName, CharSequence awsAccountId, CharSequence awsRegion ->
      return new AWSSecretsManagerCredentialProvider(IOC.get(Debugger), IOC.get(ContainerRunner), IOC.getFactory(Secret), IOC.getFactory(SecretBag), IOC.getFactory(ContainerOptions).instantiate(containerOptionDefinitionName), awsAccountId, awsRegion)
    })
    IOC.registerSingleton(JenkinsProviderAdapter, {
      return new JenkinsProviderAdapter(IOC.get('@context'))
    })
    IOC.registerSingleton(JenkinsCredentialProvider, {
      return new JenkinsCredentialProvider(IOC.get(JenkinsCredentialDataTransformer), IOC.get(JenkinsProviderAdapter))
    })

    IOC.registerSingleton(CredentialLoader, {
      return new CredentialLoader(IOC.get(Fetcher), IOC.get(CredentialExposer))
    })
  }

  static void registerDataValidation() {
    IOC.registerSingleton(DataValidator, {
      return new DataValidator()
    })
  }

  static void registerDependencyInstaller() {
    IOC.registerSingleton(DependencyInstallerDataManagerFetcher, {
      DependencyInstallerDataManagerFetcher dependencyInstallerDataManagerFetcher = new DependencyInstallerDataManagerFetcher(IOC.get(FetcherDeprecated))
      dependencyInstallerDataManagerFetcher.register(IOC.get(VenvDependencyInstallerDataManager))

      return dependencyInstallerDataManagerFetcher
    })
    IOC.registerSingleton(DependencyInstallerFetcher, {
      DependencyInstallerFetcher dependencyInstallFetcher = new DependencyInstallerFetcher(IOC.get(FetcherDeprecated))
      dependencyInstallFetcher.register(IOC.get(Venv))

      return dependencyInstallFetcher
    })

    IOC.registerSingleton(DependencyInstallerListener, {
      return new DependencyInstallerListener(IOC.get('@context'), IOC.get(Debugger), IOC.get(DependencyInstallerFetcher))
    })

    IOC.registerSingleton(Venv, {
      return new Venv(IOC.get(Debugger), IOC.get(ContainerRunner), IOC.get(ExecuteProxy))
    })
    IOC.register(VenvDependencyInstallerData, {
      return new VenvDependencyInstallerData()
    })
    IOC.registerSingleton(VenvDependencyInstallerDataManager, {
      return new VenvDependencyInstallerDataManager(IOC.get(ContainerOptionsManager))
    })
  }

  static void registerDeprecation() {
    IOC.registerSingleton(DeprecatedFunction, {
      return new DeprecatedFunction(IOC.get(DeprecatedMessage))
    })
    IOC.registerSingleton(DeprecatedMessage, {
      return new DeprecatedMessage()
    })
  }

  static void registerEventListenerWrappers() {
    IOC.registerSingleton(ScmCredentialWrapper, { List<String> sshHostKeys ->
      return new ScmCredentialWrapper(IOC.get(SSHKeySetupHelper), IOC.get('@context'), sshHostKeys)
    })
  }

  static void registerListeners() {
    IOC.registerSingleton(AutoTaggerListener, {
      return new AutoTaggerListener(IOC.get(AutoTagger), IOC.get(Debugger), IOC.get('@context'))
    })
    IOC.registerSingleton(CleanupDelayDataValidationListener, {
      return new CleanupDelayDataValidationListener(IOC.get(DataValidator))
    })
    IOC.registerSingleton(CleanupDelayListener, {
      return new CleanupDelayListener(IOC.get('@context'), IOC.get(TimeoutProxy), IOC.get(Debugger))
    })
    IOC.registerSingleton(ContainerLoginDataValidationListener, {
      return new ContainerLoginDataValidationListener(IOC.get(DataValidator), IOC.getFactory(ContainerLoginData), IOC.getFactory(ContainerRegistryOption))
    })
    IOC.registerSingleton(ContainerLoginListener, {
      return new ContainerLoginListener(IOC.get('@context'), IOC.get(ExecuteProxy), IOC.get(ContainerRunner))
    })
    IOC.registerSingleton(DebugInformationDisplayerListener, {
      return new DebugInformationDisplayerListener(IOC.get(ExecuteProxy), IOC.get(Debugger))
    })
    IOC.registerSingleton(FileStandardDataValidationListener, {
      return new FileStandardDataValidationListener(IOC.get(DataValidator))
    })
    IOC.registerSingleton(FileStandardListener, {
      return new FileStandardListener(IOC.get(FileProxy))
    })
    IOC.registerSingleton(ArtifactPublisherDataValidationListener, {
      return new ArtifactPublisherDataValidationListener(IOC.get(DataValidator), IOC.getFactory(ArtifactPublisherData))
    })
    IOC.registerSingleton(HeaderDisplayerListener, {
      return new HeaderDisplayerListener(IOC.get(Debugger))
    })
    IOC.registerSingleton(JobDataValidationListener, {
      return new JobDataValidationListener(IOC.get(DataValidator))
    })
    IOC.registerSingleton(JobPropertiesListener, {
      return new JobPropertiesListener(IOC.get('@context'), IOC.get(Debugger))
    })
    IOC.registerSingleton(JobRuntimeDebuggingListener, {
      return new JobRuntimeDebuggingListener(IOC.get('@context'), IOC.get(TimeoutProxy), IOC.get(EnvironmentVariableProxy))
    })
    IOC.registerSingleton(LinterDataValidationListener, {
      return new LinterDataValidationListener(IOC.getFactory(LinterData), IOC.get(DataValidator))
    })
    IOC.registerSingleton(LinterListener, {
      return new LinterListener(IOC.get(ContainerRunner))
    })
    IOC.registerSingleton(PreCommitDataValidationListener, {
      return new PreCommitDataValidationListener(IOC.getFactory(PreCommitData), IOC.get(DataValidator))
    })
    IOC.registerSingleton(PreCommitListener, {
      return new PreCommitListener(IOC.get(ExecuteProxy), IOC.get(FileProxy), IOC.get(Debugger), IOC.get(ContainerRunner))
    })
    IOC.registerSingleton(ScmCheckoutListener, {
      return new ScmCheckoutListener(IOC.get('@context'), IOC.get(ExecuteProxy))
    })
    IOC.registerSingleton(ScmDataValidationListener, {
      return new ScmDataValidationListener(IOC.get(DataValidator))
    })
    IOC.registerSingleton(ScmInfoRefresherListener, {
      return new ScmInfoRefresherListener(IOC.get(ScmInfoManager))
    })
    IOC.registerSingleton(TagCheckerListener, {
      return new TagCheckerListener(IOC.get(ScmInfo))
    })
    IOC.registerSingleton(TagCheckerValidationListener, {
      return new TagCheckerValidationListener(IOC.get(DataValidator))
    })
    IOC.registerSingleton(TestsCancellerListener, {
      return new TestsCancellerListener(IOC.get(Debugger), IOC.get(ScmInfo))
    })
    IOC.registerSingleton(WorkspaceCleanupListener, {
      return new WorkspaceCleanupListener(IOC.get('@context'), IOC.get(Debugger))
    })
  }

  static void registerBash() {
    IOC.register(BashEventData, {
      return new BashEventData()
    })
    IOC.registerFactory(BashEventData)
    IOC.bindProductLine(BashEventData, '', {
      return IOC.get(BashEventData)
    })

    IOC.registerSingleton(BashDataValidationListener, {
      return new BashDataValidationListener(IOC.getFactory(BashEventData), IOC.get(DataValidator))
    })
  }

  static void registerButane() {
    IOC.register(ButaneEventData, {
      return new ButaneEventData(IOC.getFactory(ContainerOptions).instantiate('butane'))
    })
    IOC.registerFactory(ButaneEventData)
    IOC.bindProductLine(ButaneEventData, '', {
      return IOC.get(ButaneEventData)
    })

    IOC.registerSingleton(ButaneDataValidationListener, {
      return new ButaneDataValidationListener(IOC.getFactory(ButaneEventData), IOC.get(DataValidator))
    })
    IOC.registerSingleton(ButaneToIgnitionListener, {
      return new ButaneToIgnitionListener(IOC.get('@context'), IOC.get(Butane))
    })
  }


  static void registerContainer() {
    IOC.registerFactory(ContainerEventData)
    IOC.bindProductLine(ContainerEventData, 'podman', {
      return new ContainerPodmanEventData()
    })
    IOC.bindProductLine(ContainerEventData, 'docker', {
      return new ContainerDockerEventData()
    })

    IOC.registerSingleton(ContainerAliasGuesserListener, {
      return new ContainerAliasGuesserListener(IOC.get(ScmInfo))
    })
    IOC.registerSingleton(ContainerBuildListener, {
      return new ContainerBuildListener(IOC.get(ExecuteProxy))
    })
    IOC.registerSingleton(ContainerDataValidationListener, {
      return new ContainerDataValidationListener(IOC.getFactory(ContainerEventData), IOC.getFactory(LinterData), IOC.get(DataValidator))
    })
    IOC.registerSingleton(ContainerInspectListener, {
      return new ContainerInspectListener(IOC.get(ExecuteProxy))
    })
    IOC.registerSingleton(ContainerPushListener, {
      return new ContainerPushListener(IOC.get(ExecuteProxy), IOC.get(ScmInfo), IOC.get(ParallelProxy))
    })
    IOC.registerSingleton(ContainerTagListener, {
      return new ContainerTagListener(IOC.get(ScmInfo), IOC.get(ExecuteProxy))
    })
  }

  static void registerPython() {
    IOC.register(PythonData, {
      return new PythonData()
    })

    IOC.registerSingleton(PythonBuildDataManager, {
      return new PythonBuildDataManager(IOC.get(ContainerOptionsManager))
    })
    IOC.registerSingleton(PythonBuildDataManagerFetcher, {
      PythonBuildDataManagerFetcher pythonBuildDataManagerFetcher = new PythonBuildDataManagerFetcher(IOC.get(FetcherDeprecated))
      pythonBuildDataManagerFetcher.register(PythonBuilderType.PYTHON_BUILD, IOC.get(PythonBuildDataManager))
      return pythonBuildDataManagerFetcher
    })
    IOC.registerSingleton(PythonPytestDataManager, {
      return new PythonPytestDataManager(IOC.get(ContainerOptionsManager))
    })
    IOC.registerSingleton(PythonTestDataManagerFetcher, {
      PythonTestDataManagerFetcher pythonTestDataManagerFetcher = new PythonTestDataManagerFetcher(IOC.get(FetcherDeprecated))
      pythonTestDataManagerFetcher.register(PythonTestsType.PYTEST, IOC.get(PythonPytestDataManager))
      return pythonTestDataManagerFetcher
    })

    IOC.registerSingleton(PythonBuildBuilder, {
      return new PythonBuildBuilder(IOC.get(PythonBuild))
    })
    IOC.registerSingleton(PythonBuilderFetcher, {
      PythonBuilderFetcher pythonBuilderFetcher = new PythonBuilderFetcher(IOC.get(FetcherDeprecated))
      pythonBuilderFetcher.register(PythonBuilderType.PYTHON_BUILD, IOC.get(PythonBuildBuilder))
      return pythonBuilderFetcher
    })
    IOC.registerSingleton(PythonTestsFetcher, {
      PythonTestsFetcher pythonTestsFetcher = new PythonTestsFetcher(IOC.get(FetcherDeprecated))
      pythonTestsFetcher.register(PythonTestsType.PYTEST, IOC.get(PythonTestsPytest))
      return pythonTestsFetcher
    })
    IOC.registerSingleton(PythonTestsPytest, {
      return new PythonTestsPytest(IOC.get(Pytest))
    })

    IOC.registerSingleton(PythonBuilderListener, {
      return new PythonBuilderListener(IOC.get(PythonBuilderFetcher), IOC.get(Debugger), IOC.get(ScmInfo))
    })
    IOC.registerSingleton(PythonTestsListener, {
      return new PythonTestsListener(IOC.get(PythonTestsFetcher), IOC.get(Debugger), IOC.get(ScmInfo))
    })
  }

  static void registerTerraform() {
    IOC.register(TerraformEventData, {
      return new TerraformEventData(IOC.getFactory(ContainerOptions).instantiate('terraform'))
    })
    IOC.registerFactory(TerraformEventData)
    IOC.bindProductLine(TerraformEventData, '', {
      return IOC.get(TerraformEventData)
    })

    IOC.registerSingleton(AWSTerraformApplyWrapper, { String AWSAccessKeyVariableName, String AWSSecretKeyVariableName, String credentialId ->
      return new AWSTerraformApplyWrapper(
        IOC.get('@context'), AWSAccessKeyVariableName, AWSSecretKeyVariableName, credentialId
      )
    })
    IOC.register(AWSTerraformInitSSHWrapper, { String terraformInitCredentialId, String credentialId, List sshHostKeys, Boolean useTerraformInitInternalWrapper = true ->
      return new AWSTerraformInitSSHWrapper(
        IOC.get(AWSTerraformInitWrapper, terraformInitCredentialId),
        IOC.get(SSHKeySetupHelper),
        IOC.get('@context'),
        credentialId,
        sshHostKeys,
        useTerraformInitInternalWrapper
      )
    })
    IOC.registerSingleton(AWSTerraformInitWrapper, { String credentialId ->
      return new AWSTerraformInitWrapper(
        IOC.get('@context'), credentialId
      )
    })

    IOC.registerSingleton(TerraformApplyListener, {
      return new TerraformApplyListener(IOC.get('@context'), IOC.get(Terraform), IOC.get(FoolProofValidationHelper), IOC.get(Debugger), IOC.get(ScmInfo), IOC.get(PrintProxy))
    })
    IOC.registerSingleton(TerraformArtifactCleanerListener, {
      return new TerraformArtifactCleanerListener(IOC.get(ExecuteProxy))
    })
    IOC.registerSingleton(TerraformDataValidationListener, {
      return new TerraformDataValidationListener(IOC.getFactory(TerraformEventData), IOC.get(DataValidator))
    })
    IOC.registerSingleton(TerraformDestroyListener, {
      return new TerraformDestroyListener(IOC.get(FileProxy), IOC.get(Terraform), IOC.get(Debugger), IOC.get('@context'))
    })
    IOC.registerSingleton(TerraformDiscoverCommandTargetsListener, {
      return new TerraformDiscoverCommandTargetsListener(IOC.get(ExecuteProxy), IOC.get(Debugger))
    })
    IOC.registerSingleton(TerraformFmtListener, {
      return new TerraformFmtListener(IOC.get(Terraform))
    })
    IOC.registerSingleton(TerraformInitListener, {
      return new TerraformInitListener(IOC.get(Terraform))
    })
    IOC.registerSingleton(TerraformValidateListener, {
      return new TerraformValidateListener(IOC.get(Terraform))
    })
    IOC.registerSingleton(TerraformWorkspaceSelectListener, {
      return new TerraformWorkspaceSelectListener(IOC.get(Terraform), IOC.get(Debugger))
    })
  }

  static void registerUndefined() {
    IOC.register(UndefinedData, {
      return new UndefinedData()
    })
  }
}
