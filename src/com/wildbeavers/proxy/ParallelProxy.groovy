package com.wildbeavers.proxy
/**
 * Proxy to the cloudbees context "parallel" command
 */
class ParallelProxy {
  private Script context

  ParallelProxy(Script context) {
    this.context = context
  }

  /**
   * Runs $closures in parallel
   * @param closures
   * @return
   */
  Object parallel(Map<String, Closure> closures) {
    return this.context.parallel(closures)
  }
}
