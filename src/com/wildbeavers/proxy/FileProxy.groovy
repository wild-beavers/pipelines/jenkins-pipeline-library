package com.wildbeavers.proxy

import com.wildbeavers.data_validation.type.generic.Regexp

import java.util.regex.Pattern

/**
 * Helps to perform checks and operations on files.
 */
class FileProxy {
  private Script context

  FileProxy(Script context) {
    this.context = context
  }

  /**
   * Checks that a given ${path} exists on the current system
   * @param path Full path (absolute or relative) of the file to check.
   * @return Boolean
   */
  Boolean exists(CharSequence path) {
    return this.context.fileExists(path)
  }

  /**
   * Read a given ${path} and return its content
   * @param path Full path (absolute or relative) of the file to read.
   * @return CharSequence
   */
  CharSequence read(CharSequence path) {
    return this.context.readFile(file: path)
  }

  /**
   * Finds a ${pattern} inside a given ${path}
   * @param path Full path (absolute or relative) of the file to read.
   * @param pattern
   * @return true if $pattern was found, false otherwise
   */
  Boolean findInFile(CharSequence path, Pattern pattern) {
    return this.context.readFile(file: path) =~ pattern
  }
}
