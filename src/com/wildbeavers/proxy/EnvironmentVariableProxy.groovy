package com.wildbeavers.proxy

import com.wildbeavers.exception.EnvironmentVariableProxyException

/**
 * Manage environment variable.
 */
class EnvironmentVariableProxy {
  private Script context
  private PrintProxy printProxy

  EnvironmentVariableProxy(Script context, PrintProxy printProxy) {
    this.context = context
    this.printProxy = printProxy
  }

  /**
   * Check if an environment variable is set in the current context.
   * @param name Name of the environment variable
   * @return Boolean true is the variable is not null, false otherwise
   * @throw Exception when the name is null
   */
  Boolean exist(String name) {
    if (name == null) {
      throw new Exception("The environment variable name can't be null")
    }

    return null != this.context.env[name]
  }

  /**
   * Set an environment variable in the current context.
   * @param name Name of the environment variable to be set
   * @param value Value of the environment variable
   */
  void set(String name, String value) {
    if (this.exist(name)) {
      this.printProxy.println(name + " environment variable already exist. It will be overridden.")
    }

    this.context.env[name] = value
  }

  /**
   * Get the value of an environment variable in the current context. Will throw an error if the environment variable is not set.
   * @param name Name of the environment variable to get
   * @return
   */
  String get(String name) {
    if (!this.exist(name)) {
      throw new EnvironmentVariableProxyException(name + " doesn't exist.")
    }

    return this.context.env[name]
  }

  /**
   * Get the value of an environment variable in the current context. Fallback to a default value if the variable is not set.
   * @param name Name of the environment variable to get
   * @param defaultValue Value that will be returned if the environment variable is not set
   * @return
   */
  String getIfExists(String name, String defaultValue = null) {
    try {
      return this.get(name)
    } catch (EnvironmentVariableProxyException ignored) {
      return defaultValue
    }
  }

  /**
   * Unset an environment variable.
   * @param name Name of the environment variable to unset
   */
  void unset(String name) {
    if (!this.exist(name)) {
      this.printProxy.println("“${name}” environment variable doesn't exist.")
      return
    }

    this.context.env[name] = null
  }

  /**
   * Sets an environment variable in the current context with a randomly generated UUID as its name
   * @param value Value of the environment variable
   * @return The name of the environment variable
   */
  String setWithRandomName(String value) {
    String randomName = this.generateValidEnvironmentVariableName()

    while (this.exist(randomName)) {
      randomName = this.generateValidEnvironmentVariableName()
    }

    this.set(randomName, value)

    return randomName
  }

  /**
   * Generate a POSIX random environment variable name ([a-zA-Z_][a-zA-Z0-9_]*)
   * @return String the random environment variable name
   */
  String generateValidEnvironmentVariableName() {
    return transformStringToValidPosixEnvironmentVariable(UUID.randomUUID().toString().replace('-', '_'))
  }

  /**
   * Transform a string into a POSIX environment variable name
   * @param name The name to transform
   * @return The POSIX environment variable name
   * @throw Exception when the name doesn't match “^[a-zA-Z0-9_]*”
   */
  static String transformStringToValidPosixEnvironmentVariable(String name) {
    if (!(name ==~ /^[a-zA-Z0-9_]*/)) {
      throw new Exception("A POSIX environment variable must only include alphanumeric and underscore “_” (actual value: “" + name + "”)")
    }

    return name.replaceFirst('^[0-9]+', '').replace('-', '_')
  }
}
