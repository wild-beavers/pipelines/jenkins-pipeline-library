package com.wildbeavers.proxy

/**
 * Proxy to print something on the standard output
 */
class PrintProxy {
  private Script context

  PrintProxy(Script context) {
    this.context = context
  }

  /**
   * Prints a string representation of ${something} on the standard output
   * @param something
   */
  void print(Object something) {
    this.context.print("\u001b[37m\u001b[3m" + something + "\u001b[0m")
  }

  /**
   * Prints a string representation of ${something} on the standard output. Suffix with a line return.
   * @param something
   */
  void println(Object something) {
    this.context.println("\u001b[37m\u001b[3m" + something + "\u001b[0m")
  }
}
