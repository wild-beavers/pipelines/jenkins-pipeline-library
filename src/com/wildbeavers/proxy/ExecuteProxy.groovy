package com.wildbeavers.proxy

import com.wildbeavers.data.CommandResult
import com.wildbeavers.exception.CommandExecutionException
import com.wildbeavers.io.Debugger

/**
 * Helps to execute shell commands in the underlying OS
 */
class ExecuteProxy {
  private Debugger debugger
  private Script context

  ExecuteProxy(Debugger debugger, Script context) {
    this.debugger = debugger
    this.context = context
  }

  /**
   * Executes a $command. Do not show any outputs on the console. Throws an error if the status code is not 0.
   * @param command Command to execute
   * @return CommandResult
   */
  CommandResult execute(String command) {
    return this.checkResultCode(this.doExecute(command))
  }

  /**
   * Executes a $command. Proxy to CPS cloudbees "execute" step
   * @param command Command to execute
   * @param printCommand Whether to display outputs
   * @param printStdout Whether to display outputs
   */
  Map basicExecute(String command, Boolean printCommand = false, Boolean printStdout = true, Boolean returnStatus = false) {
    return this.context.sh(
      script: !printCommand ? """#!/usr/bin/bash
    ${command}
""" : command,
      returnStdout: !printStdout,
      returnStatus: returnStatus
    )
  }

  /**
   * Executes a $command. Shows the outputs on the console. Throws an error if the status code is not 0.
   * @param command Command to execute
   * @return CommandResult
   */
  CommandResult executeWithTail(String command) {
    return this.checkResultCode(this.doExecute(command, true))
  }

  /**
   * Executes a $command. Output should never be shown on the console.
   * @param command Command to execute
   * @return CommandResult
   */
  CommandResult executeSecret(String command) {
    return this.checkResultCode(this.doExecute(command, false, true))
  }

  /**
   * Executes a $command. Do not show any outputs on the console. Do not throw any error whatever the return status.
   * @param command Command to execute
   * @return CommandResult
   */
  CommandResult executeAndAllowError(String command) {
    return this.checkResultCode(this.doExecute(command), true)
  }

  private CommandResult doExecute(String command, Boolean tailOutput = false, Boolean secretOutput = false) {
    String randomPrefix = UUID.randomUUID().toString()
    String script = """#!/usr/bin/bash
        (${command}) >/dev/shm/${randomPrefix}stdout.log 2>/dev/shm/${randomPrefix}stderr.log
        echo \$? > /dev/shm/${randomPrefix}statuscode.log
      """

    if (tailOutput && !secretOutput) {
      script = """#!/usr/bin/bash
        echo " \$ $command"
        echo ""
        (${command}) > >(tee -a /dev/shm/${randomPrefix}stdout.log) 2> >(tee -a /dev/shm/${randomPrefix}stderr.log >&2)
        echo \$? > /dev/shm/${randomPrefix}statuscode.log
      """
    }

    this.context.sh(
      returnStdout: false,
      returnStatus: false,
      script: script
    )

    CommandResult result = new CommandResult(
      command,
      this.context.readFile("/dev/shm/${randomPrefix}stdout.log").trim(),
      this.context.readFile("/dev/shm/${randomPrefix}stderr.log").trim(),
      this.context.readFile("/dev/shm/${randomPrefix}statuscode.log").trim().toInteger(),
    )

    this.context.sh(script: """#!/usr/bin/bash
      rm -rf /dev/shm/${randomPrefix}*
""")

    if (!secretOutput) {
      this.debugger.printDebug(result.toString())
    }

    return result
  }

  private CommandResult checkResultCode(CommandResult commandResult, Boolean showNotThrow = false) {
    if (0 != commandResult.getStatusCode()) {
      if (!showNotThrow) {
        throw new CommandExecutionException("\u001b[0mA shell execution returned an error.\n" +
          "Check logs above for more details, or enabled debug mode.\n\u001b[0m" +
          "\nCommand:\u001b[1;31m ${commandResult.getCommand()}\u001b[0m")
      }

      this.debugger.printDebug("A shell execution returned an error:\n${commandResult.toString()}")
    }

    return commandResult
  }
}
