package com.wildbeavers.proxy

import com.wildbeavers.data_validation.type.TimeUnit


/**
 * Proxy to a timeout implementation
 */
class TimeoutProxy {
  private Script context

  TimeoutProxy(Script context) {
    this.context = context
  }

  void timeout(Integer time, TimeUnit timeUnit, Closure closure) {
    this.context.timeout([time: time, unit: timeUnit.toString()], closure)
  }
}
