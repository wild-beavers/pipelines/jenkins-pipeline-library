package com.wildbeavers.data

import com.wildbeavers.helper.SemverHelper

class ScmInfo implements Serializable {
  public static final TAG_LATEST = 'latest'

  private CharSequence localTipCommitId
  private CharSequence remoteTipCommitId
  private CharSequence revisionCommitId

  private CharSequence branch
  private CharSequence defaultBranch

  private Boolean isPullRequest

  private CharSequence remoteName
  private CharSequence repositoryName
  private CharSequence repositoryURL

  private LinkedHashSet<CharSequence> allRevisionTags
  private LinkedHashSet<CharSequence> allLocalTags
  private LinkedHashSet<CharSequence> allRemoteRevisionTags
  private LinkedHashSet<CharSequence> allRemoteTags

  private CharSequence workspacePath
  private List<CharSequence> workspaceFileTree

  ScmInfo(CharSequence localTipCommitId, CharSequence remoteTipCommitId, CharSequence revisionCommitId, CharSequence branch, CharSequence defaultBranch, Boolean isPullRequest, CharSequence remoteName, CharSequence repositoryName, CharSequence repositoryURL, LinkedHashSet<CharSequence> allRevisionTags, LinkedHashSet<CharSequence> allLocalTags, LinkedHashSet<CharSequence> allRemoteRevisionTags, LinkedHashSet<CharSequence> allRemoteTags, CharSequence workspacePath, List<CharSequence> workspaceFileTree) {
    this.localTipCommitId = localTipCommitId
    this.remoteTipCommitId = remoteTipCommitId
    this.revisionCommitId = revisionCommitId

    this.branch = branch
    this.defaultBranch = defaultBranch
    this.isPullRequest = isPullRequest

    this.remoteName = remoteName
    this.repositoryName = repositoryName
    this.repositoryURL = repositoryURL

    this.allRevisionTags = allRevisionTags
    this.allLocalTags = allLocalTags
    this.allRemoteRevisionTags = allRemoteRevisionTags
    this.allRemoteTags = allRemoteTags

    this.workspacePath = workspacePath
    this.workspaceFileTree = workspaceFileTree
  }

  String toString() {
    return (
      "\nRevision commit ID: " + this.getRevisionCommitId() +
        "\nLocal tip commit ID: " + this.getLocalTipCommitId() +
        "\nRemote tip commit ID: " + this.getRemoteTipCommitId() +
        "\nRevision is remote’s latest: " + this.isRevisionLatest() +
        "\n--------------------------------------- " +
        "\nRevision branch name: " + this.getBranch() +
        "\nRevision branch as docker tag: " + this.getBranchAsDockerTag() +
        "\nDefault branch: " + this.getDefaultBranch() +
        "\nRevision branch is default: " + this.isCurrentBranchDefault() +
        "\nRevision is latest and on default branch: " + this.isCurrentCommitLatestAndOnDefaultBranch() +
        "\n--------------------------------------- " +
        "\nRevision is a merge request: " + this.isMergeRequest() +
        "\n--------------------------------------- " +
        "\nRemote name: " + this.getRemoteName() +
        "\nRepository name: " + this.getRepositoryName() +
        "\nRepository URL: " + this.getRepositoryURL() +
        "\n--------------------------------------- " +
        "\nRevision semver tag: " + this.getRevisionSemverTag() +
        "\nRevision floating tags: " + this.getAllRevisionFloatingTags() +
        "\nRevision all tags: " + this.getAllRevisionTags() +
        "\nRevision semver-only tags: " + this.getAllRevisionSemverAndFloatingTags() +
        "\nRevision is semver-tagged: " + this.isRevisionSemverTagged() +
        "\nRevision properly tagged: " + this.isRevisionProperlyTagged() +
        "\nRevision potential missing floating tags: " + this.getRevisionMissingFloatingTags() +
        "\nRevision tag contains metadata: " + this.doesRevisionTagContainsMetadata() +
        "\nRevision tag is a pre-release: " + this.isRevisionTagPreRelease() +
        "\nRevision tag is remote’s latest: " + this.isRevisionTagLatest() +
        "\nLocal branch tags: " + this.getAllLocalTags() +
        "\n--------------------------------------- " +
        "\nRemote revision semver tag: " + this.getRemoteRevisionSemverTag() +
        "\nRemote revision floating tags: " + this.getAllRemoteRevisionFloatingTags() +
        "\nRemote revision all tags: " + this.getAllRemoteRevisionTags() +
        "\nRemote revision semver-only tags: " + this.getAllRemoteRevisionSemverAndFloatingTags() +
        "\nRemote revision is semver-tagged: " + this.isRemoveRevisionSemverTagged() +
        "\nRemote revision properly tagged: " + this.isRemoteRevisionProperlyTagged() +
        "\nRemote revision potential missing floating tags: " + this.getRemoteRevisionMissingFloatingTags() +
        "\nRemote all tags: " + this.getAllRemoteTags() +
        "\nRemote latest semver tag: " + this.getRemoteLatestSemverTag() +
        "\n--------------------------------------- " +
        "\nRevision extracted patch version: " + this.extractPatchVersion() +
        "\nRevision extracted patch digit: " + this.extractPatchVersionNumber().toString() +
        "\nRevision extracted minor version: " + this.extractMinorVersion() +
        "\nRevision extracted minor digit: " + this.extractMinorVersionNumber().toString() +
        "\nRevision extracted major version: " + this.extractMajorVersion() +
        "\nRevision extracted major digit: " + this.extractMajorVersionNumber().toString() +
        "\nRevision extracted pre-release version: " + this.extractPreReleaseVersion() +
        "\nRevision extracted metadata version: " + this.extractMetadataVersion() +
        "\n--------------------------------------- " +
        "\nRevision is publishable in production: " + this.isPublishable() +
        "\nRevision is publishable in integration: " + this.isPublishableAsPreRelease() +
        "\nRevision is publishable in production or integration: " + this.isPublishableAsAnything() +
        "\n--------------------------------------- " +
        "\nWorkspace path: " + this.getWorkspacePath() +
        "\nWorkspace file tree: " + this.getWorkspaceFileTree()
    )
  }

  /**
   * @return current HEAD commit ID
   */
  CharSequence getRevisionCommitId() {
    return revisionCommitId
  }

  /**
   * @return current branch latest commit ID
   */
  CharSequence getLocalTipCommitId() {
    return localTipCommitId
  }

  /**
   * @return the remote branch (linked to the local branch) latest commit ID
   */
  CharSequence getRemoteTipCommitId() {
    return remoteTipCommitId
  }

  /**
   * @return whether the current revision (HEAD) is the latest commit on the remote branch
   */
  Boolean isRevisionLatest() {
    return (
      this.revisionCommitId == this.remoteTipCommitId
    )
  }

  /**
   * @return the current branch
   */
  CharSequence getBranch() {
    return branch
  }

  /**
   * @return the current revision branch formatted to be a valid docker tag
   */
  String getBranchAsDockerTag() {
    return branch.replace('/', '_')
  }

  /**
   * @return the remote "default" branch set by the CVS
   */
  CharSequence getDefaultBranch() {
    return defaultBranch
  }

  /**
   * Whether the current branch is also the "default" remote branch
   **/
  Boolean isCurrentBranchDefault() {
    return defaultBranch == branch
  }

  /**
   * Whether the current commit (HEAD) is exactly the same as the latest commit on remote branch, as well as on the "default" remote branch
   **/
  Boolean isCurrentCommitLatestAndOnDefaultBranch() {
    return this.isCurrentBranchDefault() && this.isRevisionLatest()
  }

  /**
   * @return whether the current branch is open as a pull request
   */
  Boolean isPullRequest() {
    return isPullRequest
  }

  /**
   * @return whether the current branch is open as a merge request (alias of isPullRequest())
   */
  Boolean isMergeRequest() {
    return isPullRequest
  }

  /**
   * @return the name of the remote
   */
  String getRemoteName() {
    return remoteName
  }

  /**
   * @return the name of the repository
   */
  String getRepositoryName() {
    return repositoryName
  }

  /**
   * @return the URL of the repository
   */
  String getRepositoryURL() {
    return repositoryURL
  }

  /**
   * @return the current revision main semver tag, if exists.
   */
  CharSequence getRevisionSemverTag() {
    return allRevisionTags.find { it ==~ SemverHelper.SEMVER_REGEXP } ?: ''
  }

  /**
   * @return the current revision floating tags, derived from the revision semver tags: MAJOR, MAJOR.MINOR and "latest", if exists
   */
  LinkedHashSet<CharSequence> getAllRevisionFloatingTags() {
    return allRevisionTags.findAll { it ==~ SemverHelper.SEMVER_FLOATING_REGEXP || it == TAG_LATEST } as LinkedHashSet
  }

  /**
   * @return the current revision tags, only semver and floating, if exists.
   */
  LinkedHashSet<CharSequence> getAllRevisionSemverAndFloatingTags() {
    if (this.getRevisionSemverTag().isEmpty()) {
      return this.getAllRevisionFloatingTags()
    }

    return this.getAllRevisionFloatingTags() + [this.getRevisionSemverTag()] as LinkedHashSet
  }

  /**
   * @return the current revision tags, regardless of what they are
   */
  LinkedHashSet<CharSequence> getAllRevisionTags() {
    return allRevisionTags
  }

  /**
   * @return whether current revision is correctly tagged with semver.
   */
  Boolean isRevisionSemverTagged() {
    return this.getRevisionSemverTag() != ''
  }

  /**
   * Whether a revision contains at least a proper semver tag.
   * @param Boolean floatingEnabled Also check the revision contains at least all possible floating versions: MAJOR, MAJOR.MINOR and "latest".
   */
  Boolean isRevisionProperlyTagged(Boolean withFloatingTags = true) {
    return this.isRevisionTagPreRelease() || (
      this.isRevisionSemverTagged() &&
        (withFloatingTags ? this.getRevisionMissingFloatingTags().isEmpty() : true)
    )
  }

  /**
   * Returns a list of missing floating tags on the revision according to the current semver tag.
   * Careful, this method does not check if the revision should be tagged with its result but merely potential missing floating tags.
   */
  LinkedHashSet<CharSequence> getRevisionMissingFloatingTags() {
    return ([this.extractMajorVersion(), this.extractMinorVersion(), TAG_LATEST] - this.allRevisionFloatingTags) - ''
  }

  /**
   * Whether a revision on the remote contains at least a proper semver tag.
   * @param Boolean floatingEnabled Also check the revision on the remote contains at least all possible floating versions: MAJOR, MAJOR.MINOR and "latest".
   */
  Boolean isRemoteRevisionProperlyTagged(Boolean withFloatingTags = true) {
    return this.isRevisionSemverTagged() && this.getRemoteRevisionSemverTag() == this.getRevisionSemverTag() &&
      (!this.isRevisionTagPreRelease() && withFloatingTags ? this.getRemoteRevisionMissingFloatingTags().isEmpty() : true)
  }

  /**
   * Return a list of missing floating tags on the remote revision according to the current semver tag.
   * Careful, this method does not check if the remote should be tagged with its result but merely potential missing floating tags.
   */
  LinkedHashSet<CharSequence> getRemoteRevisionMissingFloatingTags() {
    return ([this.extractMajorVersion(), this.extractMinorVersion(), TAG_LATEST] - this.allRemoteRevisionFloatingTags) - ''
  }

  /**
   * @return whether the current revision (HEAD) contains a valid semver metadata version
   */
  Boolean doesRevisionTagContainsMetadata() {
    return SemverHelper.containsMetadata(this.getRevisionSemverTag())
  }

  /**
   * @return whether the current revision (HEAD) is a valid semver pre-release version
   */
  Boolean isRevisionTagPreRelease() {
    return SemverHelper.containsPreRelease(this.getRevisionSemverTag())
  }

  /**
   * @return whether the current revision (HEAD) semver tag is also the remote’s tip semver tag
   */
  Boolean isRevisionTagLatest() {
    return this.getRemoteRevisionSemverTag() == this.getRevisionSemverTag()
  }

  /**
   * @return the current local tags of the current branch, regardless of what they are
   */
  LinkedHashSet<CharSequence> getAllLocalTags() {
    return allLocalTags
  }

  /**
   * @return the current revision commit’s main semver tag on the remote, if exists.
   */
  CharSequence getRemoteRevisionSemverTag() {
    return allRemoteRevisionTags.find { it ==~ SemverHelper.SEMVER_REGEXP } ?: ''
  }

  /**
   * @return whether current revision commit’s is correctly tagged with semver on the remote.
   */
  Boolean isRemoveRevisionSemverTagged() {
    return this.getRemoteRevisionSemverTag() != ''
  }

  /**
   * @return the current revision commit’s floating tags on the remote, derived from the revision semver tags: MAJOR, MAJOR.MINOR and "latest", if exists
   */
  LinkedHashSet<CharSequence> getAllRemoteRevisionFloatingTags() {
    return allRemoteRevisionTags.findAll { it ==~ SemverHelper.SEMVER_FLOATING_REGEXP || it == TAG_LATEST } as LinkedHashSet
  }

  /**
   * @return the current revision commit’s tags on the remote, regardless of what they are, if exists.
   */
  LinkedHashSet<CharSequence> getAllRemoteRevisionTags() {
    return allRemoteRevisionTags
  }

  /**
   * @return the current remote tags of the current branch, regardless of what they are
   */
  LinkedHashSet<CharSequence> getAllRemoteTags() {
    return allRemoteTags
  }

  /**
   * @return the remote’s latest semver tag if exists, that may not be linked to current revision
   */
  CharSequence getRemoteLatestSemverTag() {
    return allRemoteTags.find { it ==~ SemverHelper.SEMVER_REGEXP } ?: ''
  }

  /**
   * @return the current revision commit’s tags on the remote, only semver and floating, if exists.
   */
  LinkedHashSet<CharSequence> getAllRemoteRevisionSemverAndFloatingTags() {
    if (this.getRemoteRevisionSemverTag().isEmpty()) {
      return this.getAllRemoteRevisionFloatingTags()
    }
    return (this.getAllRemoteRevisionFloatingTags() + [this.getRemoteRevisionSemverTag()]) as LinkedHashSet
  }

  /**
   * @return whether the current revision (HEAD) is publishable as a tagged version in a production environment.
   */
  Boolean isPublishable() {
    return (
      this.isCurrentCommitLatestAndOnDefaultBranch() &&
        this.isRevisionSemverTagged() &&
        !this.isRevisionTagPreRelease() &&
        this.isRevisionTagLatest()
    )
  }

  /**
   * @reutnr whether the current revision (HEAD) is publishable as a development/testing/rc version
   */
  Boolean isPublishableAsPreRelease() {
    return (
      !this.isCurrentBranchDefault() &&
        this.isRevisionSemverTagged() &&
        this.isRevisionTagPreRelease()
    )
  }

  /**
   * @return whether the current revision (HEAD) is publishable regardless of where
   */
  Boolean isPublishableAsAnything() {
    return this.isPublishable() || this.isPublishableAsPreRelease()
  }

  /**
   * @return CharSequence extraction of the the patch part of the semver tag (MAJOR.MINOR.PATCH) without pre-release/metadata.
   */
  CharSequence extractPatchVersion() {
    return SemverHelper.getPatchTag(this.getRevisionSemverTag()) ?: ''
  }

  /**
   * @return Integer extraction of the the patch part of the semver tag (PATCH)
   */
  Integer extractPatchVersionNumber() {
    return SemverHelper.getPatchVersionNumber(this.getRevisionSemverTag())
  }

  /**
   * @return CharSequence extraction of the the minor part of the semver tag (MAJOR.MINOR) without patch and the pre-release/metadata.
   */
  CharSequence extractMinorVersion() {
    return SemverHelper.getMinorTag(this.getRevisionSemverTag()) ?: ''
  }

  /**
   * @return Integer extraction of the the minor part of the semver tag (MINOR)
   */
  Integer extractMinorVersionNumber() {
    return SemverHelper.getMinorVersionNumber(this.getRevisionSemverTag())
  }

  /**
   * @return CharSequence extraction of the the major part of the semver tag (MAJOR) without minor, patch and the pre-release/metadata.
   */
  CharSequence extractMajorVersion() {
    return SemverHelper.getMajorTag(this.getRevisionSemverTag()) ?: ''
  }

  /**
   * @return Integer extraction of the the major part of the semver tag (MAJOR)
   */
  Integer extractMajorVersionNumber() {
    return SemverHelper.getMajorVersionNumber(this.getRevisionSemverTag())
  }

  /**
   * @retur extraction of the the pre-release version (PRE-RELEASE) without major, minor and patch.
   */
  CharSequence extractPreReleaseVersion() {
    return SemverHelper.getPreReleasePart(this.getRevisionSemverTag()) ?: ''
  }

  /**
   * @return extraction of the metadata version (METADATA) without major, minor and patch.
   */
  CharSequence extractMetadataVersion() {
    return SemverHelper.getMetadataPart(this.getRevisionSemverTag()) ?: ''
  }

  /**
   * @return the workspace path
   */
  CharSequence getWorkspacePath() {
    return workspacePath
  }

  /**
   * @return the workspace files tree
   */
  List<CharSequence> getWorkspaceFileTree() {
    return workspaceFileTree
  }
}
