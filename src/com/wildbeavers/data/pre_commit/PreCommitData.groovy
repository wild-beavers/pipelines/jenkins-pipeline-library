package com.wildbeavers.data.pre_commit


import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.HasContainerOptions
import com.wildbeavers.observer.EventDataInterface

class PreCommitData implements EventDataInterface, HasContainerOptions {
  final static DEFAULT_ENABLED = true
  final static DEFAULT_RUN_COMMAND_ARGUMENTS = 'run -a --color=always'
  final static DEFAULT_CONTAINER_FQDN = 'registry.gitlab.com/wild-beavers/docker'
  final static DEFAULT_CONTAINER_IMAGE_NAME = 'pre-commit'
  final static DEFAULT_CONTAINER_FALLBACK_COMMAND = 'pre-commit'
  final static DEFAULT_CONTAINER_IMAGE_TAG = 'latest'
  final static DEFAULT_CONTAINER_TOOL = 'podman'
  final static DEFAULT_CONTAINER_USERNS = 'keep-id'
  final static DEFAULT_CONTAINER_CONFIG_JSON_DESTINATION_PATH = '/home/podman/.docker/config.json'

  private Boolean enabled = DEFAULT_ENABLED
  private String runCommandArguments = DEFAULT_RUN_COMMAND_ARGUMENTS

  PreCommitData(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }

  Boolean getEnabled() {
    return enabled
  }

  Boolean isEnabled() {
    return enabled
  }

  void setEnabled(Boolean enabled) {
    this.enabled = enabled
  }

  String getRunCommandArguments() {
    return runCommandArguments
  }

  void setRunCommandArguments(String runCommandArguments) {
    this.runCommandArguments = runCommandArguments
  }

  /////
  // HasContainerOptions
  /////

  ContainerOptions containerOptions

  @Override
  ContainerOptions getContainerOptions() {
    return containerOptions
  }

  @Override
  void setContainerOptions(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }
}
