package com.wildbeavers.data.lint

// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasLinterData {
  static final DEFAULT_LINTER_ENABLED = true

  List<LinterData> getLinterData()

  void setLinterData(List<LinterData> linterData)

  void addLinterData(LinterData linterData)

  Boolean getLinterEnabled()

  Boolean isLinterEnabled()

  Boolean setLinterEnabled(Boolean enabled)
}
