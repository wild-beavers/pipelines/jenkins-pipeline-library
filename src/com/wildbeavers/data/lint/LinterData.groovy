package com.wildbeavers.data.lint

import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.container.data.HasContainerOptions

class LinterData implements HasContainerOptions {
  private ContainerOptions containerOptions
  private String commandArguments = ''

  LinterData(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }

  String getCommandArguments() {
    return this.commandArguments
  }

  void setCommandArguments(String commandArguments) {
    this.commandArguments = commandArguments
  }

  // HasContainerOptions

  @Override
  ContainerOptions getContainerOptions() {
    return this.containerOptions
  }

  @Override
  void setContainerOptions(ContainerOptions containerOptions) {
    this.containerOptions = containerOptions
  }
}
