package com.wildbeavers.data

import com.wildbeavers.decorator.WildMap

// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasRawConfigData {
  WildMap getRawConfig()

  void setRawConfig(WildMap rawConfig)
}
