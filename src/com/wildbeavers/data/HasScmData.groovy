package com.wildbeavers.data


import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath
import com.wildbeavers.data_validation.type.GitCloneURL
import com.wildbeavers.data_validation.type.Version

import java.util.regex.Pattern

// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasScmData {
  static final DEFAULT_CHANGELOG_FILE_PATH = 'CHANGELOG.md'

  CharSequence getScmAlternativeCheckoutCredentialID()

  void setScmAlternativeCheckoutCredentialID(CharSequence scmAlternativeCheckoutCredentialID)

  CharSequence getScmAlternativeCheckoutDirectory()

  void setScmAlternativeCheckoutDirectory(ForwardRelativeLinuxFullPath scmAlternativeCheckoutDirectory)

  CharSequence getScmAlternativeCheckoutRepositoryURL()

  void setScmAlternativeCheckoutRepositoryURL(GitCloneURL scmAlternativeCheckoutRepositoryURL)

  CharSequence getScmAlternativeCheckoutTag()

  void setScmAlternativeCheckoutTag(Version scmAlternativeCheckoutTag)

  Boolean getScmFloatingTagsEnabled()

  void setScmFloatingTagsEnabled(Boolean scmFloatingTagsEnabled)

  Boolean getScmAutoTagEnabled()

  void setScmAutoTagEnabled(Boolean scmAutoTagEnabled)

  ScmAutoTagMethod getScmAutoTagMethod()

  void setScmAutoTagMethod(ScmAutoTagMethod scmAutoTagMethod)

  CharSequence getScmAutoTagChangelogFilePath()

  void setScmAutoTagChangelogFilePath(ForwardRelativeLinuxFullPath scmAutoTagChangelogFilePath)

  Pattern getScmAutoTagLastTagRegex()

  void setScmAutoTagLastTagRegex(Pattern scmAutoTagLastTagRegex)
}
