package com.wildbeavers.data

class JobInfo implements Serializable {
  private Script context

  JobInfo(Script context) {
    this.context = context
  }

  String toString() {
    return (
      "\nIs manually triggered: "+ this.isManuallyTriggered() +
      "\nTriggered user: "+ this.getTriggeredUser()
    )
  }

  /**
   * Checks whether or not the current job was manually triggered
   * @return Boolean
   */
  Boolean isManuallyTriggered() {
    for (cause in this.context.currentBuild.getBuildCauses()) {
      if (cause.containsKey('userId')) {
        return true
      }
    }
    return false
  }

  /**
   * Gets the user who triggered the current build
   * @return String
   */
  String getTriggeredUser() {
    for (cause in this.context.currentBuild.getBuildCauses()) {
      if (cause.containsKey('userId')) {
        return cause['userId']
      }
    }

    return ''
  }
}
