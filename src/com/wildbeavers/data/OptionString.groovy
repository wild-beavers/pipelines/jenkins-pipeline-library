package com.wildbeavers.data

class OptionString implements Serializable {
  private String value = ''
  private String delimiter

  OptionString(String delimiter = ' ') {
    this.delimiter = delimiter
  }

  String getValue() {
    return value
  }

  void setValue(String value) {
    this.value = value
  }

  void setDelimiter(String delimiter) {
    this.delimiter = delimiter
  }

  String getDelimiter() {
    return this.delimiter
  }

  String toString() {
    return this.value.trim()
  }

  void addOption(CharSequence optionName, Collection<String> optionValue, Boolean allowEmpty = true) {
    if (!allowEmpty && !optionValue) {
      return
    }

    for (singleOptionValue in optionValue) {
      this.addOption(optionName, singleOptionValue.toString())
    }
  }

  void addOption(CharSequence optionName, String optionValue = '', Boolean allowEmpty = true) {
    if (!allowEmpty && !optionValue) {
      return
    }

    this.value += optionName.toString()

    if (!optionValue.isEmpty()) {
      this.value += this.getDelimiter() + optionValue.toString()
    }

    this.value += ' '
  }

  void addOption(CharSequence optionName, Boolean optionValue) {
    if (!optionValue) {
      return
    }

    this.value += optionName.toString()

    this.value += ' '
  }
}
