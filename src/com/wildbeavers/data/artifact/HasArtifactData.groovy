package com.wildbeavers.data.artifact

interface HasArtifactData {
  List<ArtifactData> getArtifactData()

  void setArtifactData(List<ArtifactData> artifactDataList)

  void addArtifactData(ArtifactData artifactData)
}
