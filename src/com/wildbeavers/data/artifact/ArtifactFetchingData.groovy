package com.wildbeavers.data.artifact

class ArtifactFetchingData implements HasArtifactData {
  List<ArtifactData> artifactData = []

  @Override
  List<ArtifactData> getArtifactData() {
    return this.artifactData
  }

  @Override
  void setArtifactData(List<ArtifactData> artifactDataList) {
    this.artifactData = artifactDataList
  }

  @Override
  void addArtifactData(ArtifactData artifactData) {
    this.artifactData.add(artifactData)
  }
}
