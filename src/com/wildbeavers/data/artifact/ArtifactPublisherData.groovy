package com.wildbeavers.data.artifact

import com.wildbeavers.data_validation.type.Version

class ArtifactPublisherData {
  private List<CharSequence> pathToExclude = ['CHANGELOG.md', '.pre-commit-config.yaml', 'LICENSE', 'README.md', 'Jenkinsfile']
  private Boolean compressionEnabled = true
  private CharSequence compressionAlgorithm = "tar.gz"
  private CharSequence baseUrl
  private Version version
  private CharSequence accessorName

  List<CharSequence> getPathToExclude() {
    return pathToExclude
  }

  void setPathToExclude(List<CharSequence> pathToExclude) {
    this.pathToExclude = pathToExclude
  }

  Boolean getCompressionEnabled() {
    return compressionEnabled
  }

  void setCompressionEnabled(Boolean compressionEnabled) {
    this.compressionEnabled = compressionEnabled
  }

  CharSequence getCompressionAlgorithm() {
    return compressionAlgorithm
  }

  void setCompressionAlgorithm(CharSequence compressionAlgorithm) {
    this.compressionAlgorithm = compressionAlgorithm
  }

  CharSequence getBaseUrl() {
    return baseUrl
  }

  void setBaseUrl(CharSequence baseUrl) {
    this.baseUrl = baseUrl
  }

  Version getVersion() {
    return version
  }

  void setVersion(Version version) {
    this.version = version
  }

  CharSequence getAccessorName() {
    return accessorName
  }

  void setAccessorName(CharSequence accessorName) {
    this.accessorName = accessorName
  }
}
