package com.wildbeavers.data.artifact

import com.wildbeavers.data_validation.type.ShallowURL
import com.wildbeavers.data_validation.type.Version

class ArtifactData {
  Version version = new Version('')
  ShallowURL url = null
  CharSequence accessorName = ''

  CharSequence getVersion() {
    return this.version?.toString()
  }

  Version getVersionStrict() {
    return this.version
  }

  void setVersion(Version version) {
    this.version = version
  }

  CharSequence getUrl() {
    return this.url?.toString()
  }

  ShallowURL getUrlStrict() {
    return this.url
  }

  void setUrl(ShallowURL url) {
    this.url = url
  }

  CharSequence getAccessorName() {
    return accessorName
  }

  void setAccessorName(CharSequence accessorName) {
    this.accessorName = accessorName
  }

  CharSequence getTrimmedPath() {
    ArrayList uriPathSplit = this.getSplitPath()

    if (!this.url.getPath().toString().endsWith('/')) {
      uriPathSplit = uriPathSplit.dropRight(1)
    }

    return uriPathSplit.join('/')
  }

  CharSequence getArtifactName() {
    return this.getSplitPath().join('/').replace(this.getTrimmedPath(), '').replace('/', '')
  }

  String toString() {
    ArrayList nameElements = [this.getTrimmedPath(), this.getVersion(), this.getArtifactName()]
    nameElements.removeAll([null, ''])
    return ''.join('/', nameElements)
  }

  private ArrayList getSplitPath() {
    ArrayList uriPathSplit = this.url.getPath().toString().split('/')
    uriPathSplit.removeAll([null, ''])

    return uriPathSplit
  }
}
