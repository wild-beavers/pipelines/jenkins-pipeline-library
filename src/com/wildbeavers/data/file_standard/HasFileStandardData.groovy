package com.wildbeavers.data.file_standard
// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasFileStandardData {
  Boolean isFileStandardEnabled()

  void setFileStandardEnabled(Boolean fileStandardEnabled)

  Boolean isFileStandardIgnoreDefault()

  void setFileStandardIgnoreDefault(Boolean fileStandardIgnoreDefault)

  FileStandardData getFileStandardData()

  void setFileStandardData(FileStandardData fileStandardData)
}
