package com.wildbeavers.data.file_standard

import com.wildbeavers.data_validation.type.ForwardRelativeLinuxFullPath

class FileStandardData {
  List<ForwardRelativeLinuxFullPath> mandatoryFiles = []
  List<ForwardRelativeLinuxFullPath> forbiddenFiles = []

  List<CharSequence> getMandatoryFiles() {
    return this.mandatoryFiles.collect() { it.toString() }
  }

  void setMandatoryFiles(List<CharSequence> mandatoryFiles) {
    this.mandatoryFiles = mandatoryFiles.collect { new ForwardRelativeLinuxFullPath(it) }
  }

  void addMandatoryFile(CharSequence mandatoryFile) {
    this.mandatoryFiles.add(new ForwardRelativeLinuxFullPath(mandatoryFile))
  }

  void addMandatoryFile(ForwardRelativeLinuxFullPath mandatoryFile) {
    this.mandatoryFiles.add(mandatoryFile)
  }

  List<CharSequence> getForbiddenFiles() {
    return this.forbiddenFiles.collect() { it.toString() }
  }

  void addForbiddenFile(CharSequence forbiddenFile) {
    this.forbiddenFiles.add(new ForwardRelativeLinuxFullPath(forbiddenFile))
  }

  void addForbiddenFile(ForwardRelativeLinuxFullPath forbiddenFile) {
    this.forbiddenFiles.add(forbiddenFile)
  }

  void setForbiddenFiles(List<CharSequence> forbiddenFiles) {
    this.forbiddenFiles = forbiddenFiles.collect { new ForwardRelativeLinuxFullPath(it) }
  }
}
