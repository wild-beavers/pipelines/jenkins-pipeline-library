package com.wildbeavers.data

class CommandResult {
  private String command = ''
  private String stdout = ''
  private String stderr = ''
  private Integer statusCode

  CommandResult(String command, String stdout, String stderr, Integer statusCode) {
    this.command = command
    this.stdout = stdout
    this.stderr = stderr
    this.statusCode = statusCode
  }

  String toString() {
    return (
      "\nCommand: " + this.getCommand() +
        "\nStatus code: " + this.getStatusCode() +
        "\nstdout: " + this.getStdout() +
        "\nstderr: " + this.getStderr()
    )
  }

  String getCommand() {
    return command
  }

  String getStdout() {
    return stdout
  }

  String getStderr() {
    return stderr
  }

  Integer getStatusCode() {
    return statusCode
  }
}
