package com.wildbeavers.data
/**
 * Mock to ScmInfo. Will throw an exception whatever method is called.
 * CAVEAT:
 * This class is not robust as any change in ScmInfo will not be reflected here.
 * This is because ScmInfo.metaclass is not accessible as per Jenkins security limitations.
 * It means that any changes to ScmInfo will also required a change in this class
 */
class EmptyScmInfo extends ScmInfo {
  EmptyScmInfo(CharSequence localTipCommitId = '', CharSequence remoteTipCommitId = '', CharSequence revisionCommitId = '', CharSequence branch = '', CharSequence defaultBranch = '', Boolean isPullRequest = false, CharSequence remoteName = '', CharSequence repositoryName = '', CharSequence repositoryURL = '', LinkedHashSet<CharSequence> allRevisionTags = [], LinkedHashSet<CharSequence> allLocalTags = [], LinkedHashSet<CharSequence> allRemoteRevisionTags = [], LinkedHashSet<CharSequence> allRemoteTags = [], CharSequence workspacePath = '', List<CharSequence> workspaceFileTree = []) {
    super(localTipCommitId, remoteTipCommitId, revisionCommitId, branch, defaultBranch, isPullRequest, remoteName, repositoryName, repositoryURL, allRevisionTags, allLocalTags, allRemoteRevisionTags, allRemoteTags, workspacePath, workspaceFileTree)
  }

  String toString() {
    this.throwException()
  }

  CharSequence getRevisionCommitId() {
    this.throwException()
  }

  CharSequence getLocalTipCommitId() {
    this.throwException()
  }

  CharSequence getRemoteTipCommitId() {
    this.throwException()
  }

  Boolean isRevisionLatest() {
    this.throwException()
  }

  CharSequence getBranch() {
    this.throwException()
  }

  String getBranchAsDockerTag() {
    this.throwException()
  }

  CharSequence getDefaultBranch() {
    this.throwException()
  }

  Boolean isCurrentBranchDefault() {
    this.throwException()
  }

  Boolean isCurrentCommitLatestAndOnDefaultBranch() {
    this.throwException()
  }

  Boolean isPullRequest() {
    this.throwException()
  }

  Boolean isMergeRequest() {
    this.throwException()
  }

  String getRemoteName() {
    this.throwException()
  }

  String getRepositoryName() {
    this.throwException()
  }

  String getRepositoryURL() {
    this.throwException()
  }

  CharSequence getRevisionSemverTag() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRevisionFloatingTags() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRevisionSemverAndFloatingTags() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRevisionTags() {
    this.throwException()
  }

  Boolean isRevisionSemverTagged() {
    this.throwException()
  }

  Boolean isRevisionProperlyTagged(Boolean withFloatingTags = true) {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getRevisionMissingFloatingTags() {
    this.throwException()
  }

  Boolean isRemoteRevisionProperlyTagged(Boolean withFloatingTags = true) {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getRemoteRevisionMissingFloatingTags() {
    this.throwException()
  }

  Boolean doesRevisionTagContainsMetadata() {
    this.throwException()
  }

  Boolean isRevisionTagPreRelease() {
    this.throwException()
  }

  Boolean isRevisionTagLatest() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllLocalTags() {
    this.throwException()
  }

  CharSequence getRemoteRevisionSemverTag() {
    this.throwException()
  }

  Boolean isRemoveRevisionSemverTagged() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRemoteRevisionFloatingTags() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRemoteRevisionTags() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRemoteTags() {
    this.throwException()
  }

  LinkedHashSet<CharSequence> getAllRemoteRevisionSemverAndFloatingTags() {
    this.throwException()
  }

  Boolean isPublishable() {
    this.throwException()
  }

  Boolean isPublishableAsPreRelease() {
    this.throwException()
  }

  Boolean isPublishableAsAnything() {
    this.throwException()
  }

  CharSequence extractPatchVersion() {
    this.throwException()
  }

  Integer extractPatchVersionNumber() {
    this.throwException()
  }

  CharSequence extractMinorVersion() {
    this.throwException()
  }

  Integer extractMinorVersionNumber() {
    this.throwException()
  }

  CharSequence extractMajorVersion() {
    this.throwException()
  }

  Integer extractMajorVersionNumber() {
    this.throwException()
  }

  CharSequence extractPreReleaseVersion() {
    this.throwException()
  }

  CharSequence extractMetadataVersion() {
    this.throwException()
  }

  CharSequence getWorkspacePath() {
    this.throwException()
  }

  List<CharSequence> getWorkspaceFileTree() {
    this.throwException()
  }

  private void throwException() {
    throw new Exception(
      """
      Oops! You tried to use the “${ScmInfo.getName()}” object before it was initialized.
      What happened?
      You tried to call any method from the ${ScmInfo.getName()} object before the pipeline was started (or… see below).
      ${ScmInfo.getName()} is populated during the SCM checkout, a very early stage that happen inside a “pipeline” context.
      You can implement the "com.wildbeavers.di.IOCRefreshable" with the "refreshDependencies" method to reload ${ScmInfo.getName()}.
      However, make sure your listener is a Singleton, because the dependency injection component ignores factories.
      With factories, use the IOC to retrieve ${ScmInfo.getName()} at runtime.
"""
    )
  }
}
