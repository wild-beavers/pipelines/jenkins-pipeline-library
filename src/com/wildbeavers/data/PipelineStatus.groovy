package com.wildbeavers.data

enum PipelineStatus {
  INPROGRESS,
  SUCCESSFUL,
  FAILED;
}
