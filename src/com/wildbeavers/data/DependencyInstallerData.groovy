package com.wildbeavers.data

/**
 * Dependency installer data
 */
abstract class DependencyInstallerData {
  protected boolean installed = false

  /**
   * Whether the dependencies are installed or not
   * @return true is the dependencies are installed, false otherwise
   */
  boolean isInstalled() {
    return this.installed
  }

  void setInstalled(boolean installed) {
    this.installed = installed
  }
}
