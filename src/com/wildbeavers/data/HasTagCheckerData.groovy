package com.wildbeavers.data

import com.wildbeavers.observer.EventDataInterface

import java.util.regex.Pattern

// interface instead of trait, because of CPS error:
// -1: Unsupported array expression for CPS transformation in this context @ line -1, column -1
interface HasTagCheckerData extends EventDataInterface {
  void setTagCheckEnabled(Boolean tagCheckEnabled)

  void setTagCheckOnlyMainBranch(Boolean tagCheckOnlyMainBranch)

  void setTagCheckRegex(Pattern tagCheckRegex)

  Boolean getTagCheckEnabled()

  Boolean getTagCheckOnlyMainBranch()

  Pattern getTagCheckRegex()
}
