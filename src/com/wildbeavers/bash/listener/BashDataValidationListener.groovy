package com.wildbeavers.bash.listener

import com.wildbeavers.bash.event_data.BashEventData
import com.wildbeavers.data_validation.DataValidator
import com.wildbeavers.di.GenericFactory
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.observer.EventListener

class BashDataValidationListener extends EventListener<ControllerEventData> {
  private GenericFactory<BashEventData> bashEventDataGenericFactory
  private DataValidator dataValidator

  BashDataValidationListener(GenericFactory<BashEventData> bashEventDataGenericFactory, DataValidator dataValidator) {
    this.bashEventDataGenericFactory = bashEventDataGenericFactory
    this.dataValidator = dataValidator
  }

  @Override
  ArrayList<String> listenTo() {
    return [ControllerEvents.DATA_VALIDATION]
  }

  /**
   * @param ControllerEventData eventData
   * @return ControllerEventData
   */
  ControllerEventData run(ControllerEventData eventData) {
    BashEventData bashEventData = this.bashEventDataGenericFactory.instantiate()

    eventData.getRunnerEventData().setMainEventData(bashEventData)

    return eventData
  }
}
