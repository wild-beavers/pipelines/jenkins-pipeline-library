package com.wildbeavers.data_manager

import com.wildbeavers.container.data.ContainerOptions

class ContainerOptionsManager {
  /**
   * Create a ContainerOptions and populate it from a map of configuration
   * @param dockerOptionsConfig The docker option configuration to apply
   * @return The dockerOption with
   * @throw Exception when an option is not supported
   */
  ContainerOptions create(Map<String, Object> dockerOptionsConfig) {
    ContainerOptions dockerOptions = new ContainerOptions()

    dockerOptionsConfig.each {
      switch (it.key) {
        case "fqdn":
          dockerOptions.setFqdn(it.value)
          break
        case "imageName":
          dockerOptions.setImageName(it.value)
          break
        case "imageTag":
          dockerOptions.setImageTag(it.value)
          break
        case "volumes":
          dockerOptions.setVolumes(it.value)
          break
        case "environmentVariables":
          dockerOptions.setEnvironmentVariables(it.value)
          break
        case "network":
          dockerOptions.setNetwork(it.value)
          break
        case "runHasDaemon":
          dockerOptions.setRunHasDaemon(it.value)
          break
        case "name":
          dockerOptions.setName(it.value)
          break
        case "entrypoint":
          dockerOptions.setEntrypoint(it.value)
          break
        case "removeAfterRun":
          dockerOptions.setRemoveAfterRun(it.value)
          break
        case "privileged":
          dockerOptions.setPrivileged(it.value)
          break
        case "fallbackCommand":
          dockerOptions.setFallbackCommand(it.value)
          break
        default:
          throw new Exception("The configuration key “" + it.key + "” is not a supported as a container option.")
      }
    }

    return dockerOptions
  }
}
