package com.wildbeavers.data_manager

import com.wildbeavers.data.ScmInfo
import com.wildbeavers.di.IOC
import com.wildbeavers.io.Debugger
import com.wildbeavers.proxy.EnvironmentVariableProxy
import com.wildbeavers.proxy.ExecuteProxy

class ScmInfoManager {
  private Script context
  private Debugger debugger
  private ExecuteProxy executeProxy
  private EnvironmentVariableProxy environmentVariableProxy

  ScmInfoManager(Script context, Debugger debugger, ExecuteProxy executeProxy, EnvironmentVariableProxy environmentVariableProxy) {
    this.context = context
    this.debugger = debugger
    this.executeProxy = executeProxy
    this.environmentVariableProxy = environmentVariableProxy
  }

  void refreshScmInfo() {
    ScmInfo scmInfo = new ScmInfo(
      this.getLocalTipCommitId(),
      this.getRemoteTipCommitId(),
      this.getRevisionCommitId(),

      this.getBranch(),
      this.getDefaultBranch(),
      this.isPullRequest(),

      this.getRemoteName(),
      this.getRepositoryName(),
      this.getRepositoryURL(),

      this.getAllRevisionTags(),
      this.getAllLocalTags(),
      this.getAllRemoteRevisionTags(),
      this.getAllRemoteTags(),

      this.getWorkspacePath(),
      this.getWorkspaceFileTree()
    )

    this.debugger.printDebug('ScmInfo object was created/refreshed.')
    this.debugger.printDebug(scmInfo.toString())

    IOC.refreshSingleton(ScmInfo.getName(), {
      return scmInfo
    })
  }

  /**
   * Partially refresh ScmInfo, cloning ${scmInfo}, except fetching new local tags
   * @param sourceScmInfo source ScmInfo
   */
  void refreshScmInfoLocalTags(ScmInfo sourceScmInfo) {
    ScmInfo scmInfo = new ScmInfo(
      sourceScmInfo.getLocalTipCommitId(),
      sourceScmInfo.getRemoteTipCommitId(),
      sourceScmInfo.getRevisionCommitId(),

      sourceScmInfo.getBranch(),
      sourceScmInfo.getDefaultBranch(),
      sourceScmInfo.isPullRequest(),

      sourceScmInfo.getRemoteName(),
      sourceScmInfo.getRepositoryName(),
      sourceScmInfo.getRepositoryURL(),

      this.getAllRevisionTags(),
      this.getAllLocalTags(),
      sourceScmInfo.getAllRemoteRevisionTags(),
      sourceScmInfo.getAllRemoteTags(),

      sourceScmInfo.getWorkspacePath(),
      sourceScmInfo.getWorkspaceFileTree()
    )

    IOC.refreshSingleton(ScmInfo.getName(), {
      return scmInfo
    })
  }

  /**
   * Partially refresh ScmInfo, cloning ${scmInfo}, except fetching new remote tags
   * @param sourceScmInfo source ScmInfo
   */
  void refreshScmInfoRemoteTags(ScmInfo sourceScmInfo) {
    ScmInfo scmInfo = new ScmInfo(
      sourceScmInfo.getLocalTipCommitId(),
      sourceScmInfo.getRemoteTipCommitId(),
      sourceScmInfo.getRevisionCommitId(),

      sourceScmInfo.getBranch(),
      sourceScmInfo.getDefaultBranch(),
      sourceScmInfo.isPullRequest(),

      sourceScmInfo.getRemoteName(),
      sourceScmInfo.getRepositoryName(),
      sourceScmInfo.getRepositoryURL(),

      sourceScmInfo.getAllRevisionTags(),
      sourceScmInfo.getAllLocalTags(),
      this.getAllRemoteRevisionTags(),
      this.getAllRemoteTags(),

      sourceScmInfo.getWorkspacePath(),
      sourceScmInfo.getWorkspaceFileTree()
    )

    IOC.refreshSingleton(ScmInfo.getName(), {
      return scmInfo
    })
  }

  private CharSequence getLocalTipCommitId() {
    return this.getFirstNonErrorCommand([
      "git rev-parse ${this.getBranch()} --",

      // First command may fail because the current HEAD is detached.
      // If HEAD is detached, we assume local tip == HEAD
      'git rev-parse HEAD -- | head -n 1'
    ])
  }

  private CharSequence getRemoteTipCommitId() {
    return this.getFirstNonErrorCommand([
      "git rev-parse ${this.getRemoteName()}/${this.getBranch()} -- | head -n 1",

      // First command may fail because the current HEAD is detached.
      // If HEAD is detached, we assume remote tip == HEAD
      'git rev-parse HEAD -- | head -n 1'
    ])
  }

  private CharSequence getRevisionCommitId() {
    return this.executeCommand('git rev-parse HEAD -- | head -n 1')
  }

  private Boolean isPullRequest() {
    return this.environmentVariableProxy.exist('CHANGE_ID')
  }

  private CharSequence getBranch() {
    return this.getFirstNonErrorCommand([
      "echo \"${this.environmentVariableProxy.get('BRANCH_NAME')}\"",
      'git rev-parse --abbrev-ref HEAD'
    ])
  }

  private String getDefaultBranch() {
    if (this.environmentVariableProxy.exist('BRANCH_IS_PRIMARY')) {
      return this.getBranch()
    }

    return this.getFirstNonErrorCommand([
      // This below fails if not authenticated to the remote, but this is the most reliable way.
      'git ls-remote -q --symref | head -1 | cut -f 1 | cut -d " " -f 2 | cut -d "/" -f 3',

      // This is not robust because the PR target might not be the main branch, this is a wild guess.
      // It’s estimated most of the time this is gonna return the main branch.
      "echo ${this.environmentVariableProxy.getIfExists('CHANGE_TARGET', '')}",

      // This is even less robust, as “master” might not be the default branch.
      // However Jenkins is unable to get HEAD pointer on remote, thus making it hard to get default branch
      'echo master'
    ])
  }

  private CharSequence getRemoteName() {
    try {
      // This is a wild guess on the given refspec, but still the best way to get the remote name
      return this.context.scm.getUserRemoteConfigs()[0].getRefspec().tokenize(':').find { it.contains('remotes') } - "/${this.environmentVariableProxy.get('BRANCH_NAME')}" - "refs/remotes/"
    } catch (MissingMethodException ignored) {
    } catch (MissingPropertyException ignored) {
    } catch (NullPointerException ignored) {
    }

    return this.getFirstNonErrorCommand(["git remote | head -n 1"])
  }

  private CharSequence getRepositoryName() {
    try {
      return this.context.scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().tokenize('.').first()
    } catch (MissingMethodException ignored) {
    } catch (MissingPropertyException ignored) {
    }

    return this.getFirstNonErrorCommand([
      "basename -s .git `git config --get remote.origin.url`",
      "basename `git rev-parse --show-toplevel`"
    ])
  }

  private CharSequence getRepositoryURL() {
    try {
      return this.context.scm.getUserRemoteConfigs()[0].getUrl()
    } catch (MissingMethodException ignored) {
    } catch (MissingPropertyException ignored) {
    }

    return (this.environmentVariableProxy.get('JOB_NAME') - "/${this.getBranch()}").tokenize('/').last().tokenize('%2F').last()
  }

  private LinkedHashSet<CharSequence> getAllRevisionTags() {
    return this.executeCommand('git tag --sort=-version:refname --points-at HEAD').split("\n").findAll({!it.isEmpty()}) as LinkedHashSet
  }

  private LinkedHashSet<CharSequence> getAllLocalTags() {
    return this.executeCommand('git tag --sort=-version:refname').split("\n").findAll({!it.isEmpty()}) as LinkedHashSet
  }

  private LinkedHashSet<CharSequence> getAllRemoteRevisionTags() {
    return this.executeCommand("git tag --sort=-version:refname --points-at ${this.getRemoteName()}/${this.getDefaultBranch()}").split("\n").findAll({!it.isEmpty()}) as LinkedHashSet
  }

  private LinkedHashSet<CharSequence> getAllRemoteTags() {
    return this.executeCommand('git ls-remote --sort=-version:refname --tags origin | awk -F/ \'{print $3}\' | grep -v \'\\^{}\'').split("\n").findAll({!it.isEmpty()}) as LinkedHashSet
  }

  private CharSequence getWorkspacePath() {
    return this.executeCommand("pwd")
  }

  private List<CharSequence> getWorkspaceFileTree() {
    def result = this.executeCommand('find . -type f -print | grep -v \'^./.git\'')
    return result.split("\n") as List<String>
  }

  private String getFirstNonErrorCommand(List<String> commands) {
    for (command in commands) {
      def result = this.executeCommand(command)

      if ('' != result) {
        return result
      }
    }

    throw new Exception('Unable to gather data for ScmInfo.')
  }

  private String executeCommand(String command) {
    this.executeProxy.executeAndAllowError(command).getStdout()
  }
}
