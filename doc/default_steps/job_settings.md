---
layout: post
---

# Job Settings (job)

A [PRE_RUNNER]({{ "#raw-events" | relative_url }}) steps that sets user-defined and default job properties.
These steps optionally offers the end-user the ability to enable [debugging]({{ "/components/debugger.html" | relative_url }}) at runtime.

## Important classes

- `com.wildbeavers.listener.JobDataValidationListener`: validates user configuration for the current job.
- `com.wildbeavers.listener.JobPropertiesListener`: sets current job Jenkins properties.
- `com.wildbeavers.listener.JobRuntimeDebuggingListener`: dynamically enables debugging at runtime

## Configuration

Every configuration is under the `job` namespace:

| Name                          | Type                    | Description                                                                                                                                                                          | Default                            |
|-------------------------------|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| properties                    | List                    | Jenkins job properties                                                                                                                                                               | *see JobPropertiesListener.groovy* |
| nodeLabel                     | CharSequence            | Label of the node to use for the current job                                                                                                                                         | ''                                 |
| timeoutTime                   | PositiveOrZeroInt       | Maximum amount of time before the build will be cancelled (requires build-timeout plugin).                                                                                           | 10                                 |
| timeoutUnit                   | JenkinsBuildTimeoutUnit | Units as defined in the [Jenkins timeout documentation](https://www.jenkins.io/doc/pipeline/steps/workflow-basic-steps/#timeout-enforce-time-limit) (requires build-timeout plugin). | 'HOURS'                            |
| debuggingEnabled              | Boolean                 | Whether to enabled debugging.                                                                                                                                                        | false                              |
| debuggingRuntimeToggleEnabled | Boolean                 | Whether to enable the ability for users to enabled debugging at runtime, through a user input                                                                                        | false                              |
| debuggingRuntimeToggleTimeout | PositiveOrZeroInt       | How much time in seconds the pipeline will wait for user inputs to enable debugging at runtime                                                                                       | 10                                 |

## Example

*Jenkinsfile*:

```groovy
standardUndefined(
  [
    job_properties                   : [disableConcurrentBuilds()],
    job_nodeLabel                    : 'deployment',
    job_timeoutTime                  : 45,
    job_timeoutUnit                  : 'MINUTES',
    job_debuggingRuntimeToggleEnabled: true,
    job_debuggingRuntimeToggleTimeout: 15,
  ]
)
```
