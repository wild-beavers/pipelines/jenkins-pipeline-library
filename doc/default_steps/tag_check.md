---
layout: post
---

# Tag Check (tagCheck)

A step to verify the sanity of tags for the current project.

## Important classes

- `com.wildbeavers.listener.TagCheckListener`: verify tags found in the project and throw errors if invalid.
- `com.wildbeavers.listener.TagCheckerValidationListener`: validates user input data.

## Limitation

## Explanations

## Configuration

Every configuration is under the `tagCheck` namespace:

| Name           | Type    | Description                               | Default                                                   |
|----------------|---------|-------------------------------------------|-----------------------------------------------------------|
| enabled        | Boolean | Whether to enable the step.               | true                                                      |
| onlyMainBranch | Boolean | Only check the default branch.            | true                                                      |
| regex          | Pattern | Represents what is considered a valid tag | /(^latest$)\|/ + SemverHelper.SEMVER_REGEXP_NO_PRERELEASE |

## Example

*Jenkinsfile*:

```groovy
standardUndefined([
  tagCheck_enabled       : true,
  tagCheck_onlyMainBranch: false,
  tagCheck_regex         : ~/v[0-9]+/,
])
```
