---
layout: post
---

# File standard (fileStandard)

A step to check that a revision follows some basic standards.

## Important classes

- `com.wildbeavers.listener.FileStandardListener`: listener that checks revision to find standard files.

## Limitation

- Does not support wildcards

## Configuration

Every configuration is under the `fileStandard` namespace:

| Name           | Type                               | Description                                             | Default                   |
|----------------|------------------------------------|---------------------------------------------------------|---------------------------|
| enabled        | Boolean                            | Whether this step should run                            | true                      |
| ignoreDefault  | Boolean                            | Whether to ignore any default set by this library       | true                      |
| mandatoryFiles | List<ForwardRelativeLinuxFullPath> | Files that are expect to exists in the current revision | ['LICENSE', 'README.md']* |
| forbiddenFiles | List<ForwardRelativeLinuxFullPath> | Files that must not exist in the current revision       | []                        |

default varies, depending on the pipeline

## Examples

```groovy
standardUndefined(
  [
    fileStandard_enabled: false
  ]
)
```

```groovy
standardUndefined(
  [
    fileStandard_mandatoryFiles: ['CHANGELOG.md', '.pre-commit-config.yaml']
  ]
)
```
