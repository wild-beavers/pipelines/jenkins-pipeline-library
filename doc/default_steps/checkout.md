---
layout: post
---

# Checkout (checkout)

This step checkouts (clone/fetch) a remote repository based on multi branch configuration or given user options.
This step also handles SCMInfo data object refreshing.
This step also handles auto-tagging.

Auto tagging will specifically trigger depending on these factors:

- A changelog must exist and contains a new version, that is not already entirely tagged on the remote (see all tags below).
- The versioning must follow the [semver standard](https://semver.org/).
- The current checked-out commit must be the latest and be on the default branch.

Optionally, auto-tagging will create floating tags pointing to the same commit (e.g. 1.1.1):

- MAJOR.MINOR (`1.1`)
- MAJOR (`1`)
- `latest`

## Important classes

- `com.wildbeavers.listener.ScmDataValidationListener`: validates user input configuration related to SCM.
- `com.wildbeavers.listener.ScmCheckoutListener`: handles SCM checkouts.
- `com.wildbeavers.listener.ScmInfoRefresherListener`: handles information gathering about the current project.
- `com.wildbeavers.listener.AutoTaggerListener`: takes care of auto tagging
- `com.wildbeavers.helper.ChangelogAutoTaggerHelper`: helper that perform the tag commands

## Limitation

- Only works with git.
- Refspec is not customizable
- Can only checkout a single project.

## Explanations

## Configuration

Every configuration is under the `scm` namespace:

| Name                             | Type                         | Description                                                                     | Default                                                    |
|----------------------------------|------------------------------|---------------------------------------------------------------------------------|------------------------------------------------------------|
| alternativeCheckoutCredentialID  | CharSequence                 | Alternative credentials to use during the checkout.                             | null                                                       |
| alternativeCheckoutDirectory     | ForwardRelativeLinuxFullPath | Alternative directory to use during the checkout.                               | null                                                       |
| alternativeCheckoutRepositoryURL | GitCloneURL                  | Alternative repository URL to use during the checkout.                          | null                                                       |
| alternativeCheckoutTag           | Version                      | Alternative tag to checkout.                                                    | null                                                       |
| floatingTagsEnabled              | Boolean                      | Whether to handle floating semver tags.                                         | true                                                       |
| autoTagEnabled                   | Boolean                      | Whether to auto tag the current project, under the correct conditions.          | true                                                       |
| autoTagMethod                    | ScmAutoTagMethod             | What auto-tagging method to use                                                 | ScmAutoTagMethod.TAG_ANNOTATED                             |
| autoTagChangelogFilePath         | ForwardRelativeLinuxFullPath | Full path of the changelog file of the repository                               | 'CHANGELOG.md'                                             |
| autoTagLastTagRegex              | Pattern                      | Regular expression to find the latest tag within the 'autoTagChangelogFilePath' | ~"(?!^#+\s+)(${SemverHelper.SEMVER_REGEXP_NOT_DELIMITED})" |

## Example

*Jenkinsfile*:

```groovy
standardUndefined([
  scm_alternativeCheckoutCredentialID : 'checkout-pass',
  scm_alternativeCheckoutDirectory    : 'tmp/checkout',
  scm_alternativeCheckoutRepositoryURL: 'https://git.example.net/group/repo.git',
  scm_alternativeCheckoutTag          : 'v1',
  scm_floatingTagsEnabled             : false,
  scm_autoTagEnabled                  : false,
  scm_autoTagMethod                   : ScmAutoTagMethod.TAG,
  scm_autoTagChangelogFilePath        : 'VERSION',
])
```
