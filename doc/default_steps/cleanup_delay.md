---
layout: post
---

# Cleanup delay (cleanupDelay)

A POST_DEPLOY step that delays the cleanup by a given amount of time.

## Important classes

- `com.wildbeavers.listener.CleanupDelayValidationListener`: validates user configuration for the cleanup delay and fill data object.
- `com.wildbeavers.listener.CleanupDelayListener`: applies delay before cleanup.

## Configuration

Every configuration is under the `cleanupDelay` namespace:

| Name               | Type         | Description                                                                                                                                 | Default   |
|--------------------|--------------|---------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| enabled            | Boolean      | Whether to enable/disable the delay before cleanup.                                                                                         | false     |
| time               | CharSequence | Amount of time (in minutes) to wait for user input to terminate the delay.                                                                  | 10        |
| message            | CharSequence | What message to display alongside the user input                                                                                            | ''        |
| header             | CharSequence | Header message to display at the beginning of the delay                                                                                     | ''        |
| buttonLabel        | CharSequence | Label of the button alongside the user input.                                                                                               | 'Proceed' |
| continueIfExpired  | Boolean      | If `true`, the delay expiration will not throw any exceptions. If `false`, user input is mandatory, otherwise an exception will be raised.  | true      |

## Example

*Jenkinsfile*:

```groovy
standardUndefined(
  [
    cleanupDelay_enabled: true,
    cleanupDelay_time:    30,
    cleanupDelay_message: 'To proceed, click on the button below',
    cleanupDelay_header:  'Cleanup is delayed',
  ]
)
```
