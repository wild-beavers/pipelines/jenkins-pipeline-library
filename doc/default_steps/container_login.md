---
layout: post
---

# Container Login (containerLogin)

A PRE_PREPARE step that login to one or several container registries.

## Important classes

- `com.wildbeavers.listener.ContainerLoginDataValidationListener`: validates user configuration for container login and fill data objects.
- `com.wildbeavers.listener.ContainerLoginListener`: run `docker login` or equivalent commands depending on given configuration.

## Known Limitations

- Only the login credential secret/token can retrieved through external process. Username has to be set statically for now.

## Configuration

Every main configuration is under the `containerLogin` namespace:

| Name                | Type         | Description                                                                                                                                    | Default                                   |
|---------------------|--------------|------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| enabled             | Boolean      | Whether to enable/disable container login. With `false`, no login would be perform, even if registries are set.                                | true                                      |
| tool                | String       | Container tool that will perform the login.                                                                                                    | 'docker'                                  |
| authfile            | String       | Path of the authfile to create/update as a result of the login. Not compatible with `docker` runtime.                                          | ''                                        |
| containerRegistries | List         | Registries to login to, see `containerRegistries` below.                                                                                       | []                                        |

Each `containerRegistries` can have:

| Name                                          | Type         | Description                                                                                                                                           | Default                                   |
|-----------------------------------------------|--------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| credentialId                                  | Boolean      | Name of the usernamePassword credential to use to login. If set `credentialProcess`… options will be ignored.                                         | ''                                        |
| credentialProcessContainerOptions             | Map          | Container options for the process to retrieve the secret/token for the login. See `credentialProcessContainerOptions` below.                          | [:]                                       |
| credentialProcessContainerRunCommandArguments | String       | Command/Arguments to retrieve the secret/token for the login.                                                                                         | 'echo "this is not a password"'           |
| credentialProcessUsername                     | String       | Username to be used if `credentialProcess` is used instead of `credentialId`.                                                                         | ''                                        |
| fqdn                                          | String       | FQDN to login to.                                                                                                                                     | 'docker.io'                               |

`credentialProcessContainerOptions` are options for the container image to fetch the secret/token to login.

| Name                  | Type         | Description                                    | Default                |
|-----------------------|--------------|------------------------------------------------|------------------------|
| fqdn                  | String       | FQDN                                           | 'docker.io'            |
| imageName             | String       | Image name                                     | 'docker'               |
| imageTag              | String       | Image tag                                      | 'latest'               |
| environmentVariables  | Map<String>  | Additional environment variables               | [:]                    |
| volumes               | Map<String>  | Additional mount points                        | [:]                    |
| privileged            | Boolean      | Whether to run the container as privileged.    | false                  |
| tool                  | String       | Tool to run the container                      | 'docker'               |

## Example

```
standardUndefined(
  [
    containerLogin_runtime            : 'podman',
    containerLogin_containerRegistries: [
        [
          fqdn        : 'docker.io',
          credentialId: Config.DOCKER_REGISTRY_TOKEN_CREDENTIAL_ID,
        ]
    ]
  ]
)
```

Using credential process to login to AWS ECR:

```
standardUndefined([
  containerLogin_containerRegistries: [
    [
      fqdn                     :                     'XXXXXXXXXXXX.dkr.ecr.us-east-1.amazonaws.com/my_image',
      credentialProcessUsername:                     'AWS',
      credentialProcessContainerRunCommandArguments: "ecr get-login-password --region ${AWS_REGION}",
      credentialProcessContainerOptions: [
        fqdn:      'registry.gitlab.com/wild-beavers/docker',
        imageName: 'aws-cli',
        imageTag:  'latest'
      ]
    ],
  ]
])
```
