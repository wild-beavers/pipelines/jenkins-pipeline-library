---
layout: post
---

# Artifact Publishing (artifactPublisher)

A step that can publish artifacts in any supported Artifact stores.

## Important classes

- `com.wildbeavers.listener.ArtifactPublisherListener`: listener that takes care of publishing artifacts

## Limitation

- Depends on the [Artifact component]({{ "/components/artifact.html" | relative_url }}), thus sharing its limitations.

## Configuration

Every configuration is under the `artifactPublisher` namespace:

| Name                 | Type         | Description                                                                                                              | Default                                                             |
|----------------------|--------------|--------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------|
| baseUrl              | String       | Base URL where to publish artifacts. Will be suffixed by the repository name and artifacts files.                        | ''                                                                  |
| pathsToExclude       | List<String> | Blacklist of files/directory NOT to include in the artifact.                                                             | ['CHANGELOG.md', '.pre-commit-config.yaml', 'LICENSE', 'README.md'] |
| compressionEnabled   | Boolean      | Whether or not to enable compression of the artifacts before sending (for now, will not work otherwise).                 | true                                                                |
| compressionAlgorithm | String       | Algorithm (extension) to use for the compression of the artifacts                                                        | 'tar.gz'                                                            |
| version              | Version      | Version of the artifact to fetch                                                                                         | null                                                                |
| accessorName         | String       | Free value defining the method to access the artifact. Implementations are responsible to define what these methods are. | ''                                                                  |

## Example

*Jenkinsfile*:

```groovy
standardUndefined(
  [
    artifactPublisher_baseUrl: 's3://my-artifact-s3/bashscripts/',
    artifactPublisher_pathsToExclude: ['CHANGELOG.md', '.pre-commit-config.yaml', 'LICENSE', 'README.md', 'AUTHORS', 'tests', 'docs']
  ]
)
```
