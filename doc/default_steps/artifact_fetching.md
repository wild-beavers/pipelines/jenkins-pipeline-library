---
layout: post
---

# Artifact Fetching (artifactFetcher)

A step that can fetch artifacts from various locations.
Fetches are done in parallel.

## Important classes

- `com.wildbeavers.listener.ArtifactFetcherListener`: listener that takes care of fetching artifacts

## Limitation

- Depends on the [Artifact component]({{ "/components/artifact.html" | relative_url }}), thus sharing its limitations.
- Optionally depends on the [Archiver component]({{ "/components/archiver.html" | relative_url }}), thus sharing its limitations.
- The artifacts are uncompressed locally on a path corresponding to the URI only.

## Explanations

Given artifacts:

- `s3://s3-bucket-example/path/artifact1.tar.gz`, version: 3
- `s3://another-bucket-example/path/artifact2.tar.gz`
- `s3://s3-bucket-example/another/path/to/artifact3.tar.gz`, accessor: custom
- `https://gitlab.com/project/test.git`, version 1.0.0

In parallel:

- `artifact1.tar.gz`, `artifact2.tar.gz` and `artifact3.tar.gz` will be downloaded. `https://gitlab.com/project/test.git` will be cloned.
- `artifact1.tar.gz` will be uncompressed inside a `./path` directory.
- `artifact2.tar.gz` will be uncompressed inside a `./path/3` directory.
- `artifact3.tar.gz` will be uncompressed in a `./another/path/to` directory; the accessor "custom" will be used during the download process.
- `artifact4` will be cloned in a `./project/1.0.0/artifact4` directory.

## Configuration

Every configuration is under the `artifactFetcher` namespace:

| Name             | Type         | Description                              |         Default |
|------------------|--------------|---------------------------------------------------|---------|
| artifacts        | List<Map>    | List of the artifacts to download. See below      | []      |

Every `artifacts`:

| Name             | Type         | Description                                                                                         c                    | Default   |
|------------------|--------------|--------------------------------------------------------------------------------------------------------------------------|-----------|
| url              | ShallowURL   | URL of the artifact to fetch                                                                                             | null      |
| version          | Version      | Version of the artifact to fetch                                                                                         | null      |
| accessorName     | String       | Free value defining the method to access the artifact. Implementations are responsible to define what these methods are. | ''        |

## Example

*Jenkinsfile*:

```groovy
standardUndefined(
  [
    artifactFetching_artifacts: [
      [
        url:          's3://s3-bucket-example/path/test.tar.gz',
        accessorName: 'customAWSDocker',
      ],
      [
        url:     'https://example.com/path/artifact.txt',
        version: '3',
      ],
      [
        url:     'https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library.git',
        version: '9',
      ]
    ]
  ]
)
```
