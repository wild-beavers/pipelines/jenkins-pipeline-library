---
layout: post
---

# Pre-commit Run (preCommit)

A STATIC_ANALYSIS step that runs [pre-commit](https://pre-commit.com/).
This default step do run if a `.pre-commit-config.y(a)ml` configuration is found.

## Important classes

- `com.wildbeavers.listener.PreCommitDataValidationListener`: validates user configuration for pre-commit and fill data object.
- `com.wildbeavers.listener.PreCommitListener`: tuns pre-commit hooks. Any fail on one hook will stop the pipeline.

## Limitation

- Runs through `podman` by default to allow caching and rootless runs through `userns`, ultimately greatly improving performance.

## Configuration

Every configuration is under the `preCommit` namespace:

| Name                               | Type         | Description                                                                                                                                    | Default                                   |
|------------------------------------|--------------|------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| enabled                            | Boolean      | Whether to enable/disable pre-commit. With `false`, pre-commit will not be run, even if conf file exists.                                      | true                                      |
| runCommandArguments                | CharSequence | Arguments of the `pre-commit` command to run.                                                                                                  | 'run -a --color=always'                   |
| containerTool                      | CharSequence | Container tool that will run pre-commit.                                                                                                              | 'podman'                                  |
| containerFqdn                      | CharSequence | FQDN of the container that will run pre-commit.                                                                                                | 'registry.gitlab.com/wild-beavers/docker' |
| containerImageName                 | CharSequence | Image name of the container that will run pre-commit.                                                                                          | 'pre-commit'                              |
| containerImageTag                  | CharSequence | Image tag for given containerImageName of the container that will run pre-commit.                                                              | 'latest'                                  |
| containerEnvironmentVariables      | Map          | Additional environment variables to set for the container that will run pre-commit.                                                            | [:]                                       |
| containerVolumes                   | Map          | Additional mount points for the container that will run pre-commit.                                                                            | [:]                                       |
| containerPrivileged                | Boolean      | Whether to run the pre-commit container in privileged mode.                                                                                    | false                                     |
| containerUserNS                    | CharSequence | `--userns` argument for the container that will run pre-commit. Check what the runtime supports.                                               | 'keep-id'                                 |
| containerFallbackCommand           | CharSequence | Fallback command in case no container runtime is installed in the context.                                                                     | 'pre-commit'                              |
| containerConfigJsonDestinationPath | CharSequence | Destination path of the container cache configuration (containing credentials) in the pre-commit container, only if registry login is enabled. | '/home/podman/.docker/config.json'        |

## Example

*Jenkinsfile*:

```groovy
standardUndefined(
  [
    preCommit_enabled: false
  ]
)
```

```groovy
standardUndefined(
  [
    preCommit_containerTool:       'docker',
    preCommit_containerFqdn:       'docker.io',
    preCommit_containerImageName:  'precommit',
    preCommit_containerImageTag:   '1',
    preCommit_containerPrivileged: true,
  ]
)
```
