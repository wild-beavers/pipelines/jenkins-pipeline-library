---
layout: post
---

# Linting (linter)

A step that can perform some linting generically.
Linting defaults varies depending on the pipeline.

## Important classes

- `com.wildbeavers.listener.LinterListener`: listener performs a series of linting on the current code.

## Limitation

## Explanations

## Configuration

Every configuration is under the `linter` namespace:

| Name          | Type         | Description                              | Default |
|---------------|--------------|------------------------------------------|---------|
| enabled       | Boolean      | Whether to enable the step.              | false   |
| data          | List<String> | List of linters to run. See below.       | []      |

Each `data` can have:

| Name                          | Type         | Description                                            | Default                |
|-------------------------------|--------------|--------------------------------------------------------|------------------------|
| containerTool                 | String       | Tool to run the container                              | 'docker'               |
| containerFqdn                 | String       | FQDN                                                   | 'docker.io'            |
| containerImageName            | String       | Image name                                             | 'docker'               |
| containerImageTag             | String       | Image tag                                              | 'latest'               |
| containerEnvironmentVariables | Map<String>  | Additional environment variables                       | [:]                    |
| containerVolumes              | Map<String>  | Additional volumes                                     | [:]                    |
| containerPrivileged           | Boolean      | Whether to run the container as privileged.            | false                  |
| containerEntrypoint           | Boolean      | Entrypoint                                             | ''                     |
| containerUserns               | String       | User namespace option                                  | ''                     |
| containerFallbackCommand      | String       | Fallback command if no container runtime is installed. | ''                     |
| commandArguments              | String       | Full linting command                                   | ''                     |

## Example

*Jenkinsfile*:

```groovy
standardUndefined(
  [
    linter_enabled: true,
    linter_data   : [
      [
        containerTool            : 'podman',
        containerFqdn            : 'docker.io/koalaman',
        containerImageName       : 'shellcheck-alpine',
        containerImageTag        : 'latest',
        containerFallbackCommand : 'shellcheck',

        commandArguments  : 'sh -c "find . -type f -name \'*.sh\' -maxdepth 3 | xargs shellcheck -C -s bash"'
      ],
    ]
  ]
)
```
