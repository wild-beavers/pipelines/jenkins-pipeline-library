---
layout: post
---

# Observer Component

The observer component allows to inject code into any pipelines, at any time, without having to change the code baseline already existing.

Some benefits:

- Prevent side effects by introducing code shared by many pipelines.
- Allow separation of concerns: one class does one thing.
- Allow easy override of baseline pipeline by specific use cases.

## Important classes

- `com.wildbeavers.observer.EventDispatcher`: can register `EventListener`s and dispatch events
- `com.wildbeavers.observer.EventListener`: a parent class to inherit when to create a listener

## Explanations

Here is a very rough representation on how the observer component works.

- During the pipeline, multiple events are dispatched by the `EventDispatcher`.
  These events are decided in advance by the developer of the pipeline.
- Any [registered](dependency_injection.html) `EventListener` classes will be run if:
   1. it has been attached to the `EventDispatcher`
   1. it listens to the dispatched event

- Specifically, the method `run(EventDataInterface eventData)` within any `EventListener` is called.
- `EventListener`s gets their context data from `EventDataInterface eventData`.
  They also return the data, modified or not, so it can be consumed by the next `EventListener`s.

## Graphical explanation

![observer](../images/observer0.3.1.drawio.png)

## Example

### classes in client repo: src/com/client/listeners/

```groovy

// ####
// This kind of code can be in any pipeline, including client pipelines.
// This example shows many feature of the observer system: order, stop propagation…
// Note: take a close look at the listener execution order
// ####

class ProgramStartedListener extends EventListener<StartedProgramEvent> {
  StartedProgramEvent run(StartedProgramEvent eventData = null) {
    print("LISTENER 1 TRIGGERED, (second to run because getOrder returns the default:1000.)\n")
  }

  ArrayList<String> listenTo() {
    return [ProgramEvents.PROGRAM_STARTED]
  }
}

class ProgramStartedListener2 extends EventListener<StartedProgramEvent> {
  StartedProgramEvent run(StartedProgramEvent eventData = null) {
    print("LISTENER 2 TRIGGERED (first to run because getOrder returns 1.)\n")
  }

  ArrayList<String> listenTo() {
    return [ProgramEvents.PROGRAM_STARTED]
  }

  Integer getOrder() {
    return 1
  }
}

class ProgramStartedListener3 extends EventListener<StartedProgramEvent> {
  StartedProgramEvent run(StartedProgramEvent eventData = null) {
    print("LISTENER 3 TRIGGERED (should never be displayed.)\n")
  }

  ArrayList<String> listenTo() {
    return [ProgramEvents.PROGRAM_STARTED]
  }

  Integer getOrder() {
    return 5000
  }
}

class ProgramBeforeListener extends EventListener<BeforeProgramEvent> {
  Boolean shouldRun(BeforeProgramEvent eventData = null) {
    // Any condition here, depending on eventData
    return false
  }

  ArrayList<CharSequence> explainNoRun(RunnerEventData eventData) {
    return ['One reason this did not run.', 'Another reason this listener did not run.']
  }

  BeforeProgramEvent run(BeforeProgramEvent eventData = null) {
    print("LISTENER BEFORE TRIGGERED (should never be displayed.)\n")
  }

  ArrayList<String> listenTo() {
    return [ProgramEvents.PROGRAM_BEFORE]
  }
}

class ProgramSubscriber extends EventListener<EventDataInterface> {
  EventDataInterface run(EventDataInterface eventData) {
    print("ProgramSubscriber HAS RUN. (runs twice, before program and after. Nothing runs after because stopPropagationAfterRun() = true)\n")
    print("This is the data:" + eventData.getData() + "\n")

    return eventData
  }

  ArrayList<String> listenTo() {
    return [ProgramEvents.PROGRAM_STARTED, ProgramEvents.BEFORE_PROGRAM]
  }

  Boolean stopPropagationAfterRun() {
    return true;
  }
}
```

### classes in client repo: src/com/client/events/

```groovy

class ProgramEvents implements EventDataInterface {
  public final static BEFORE_PROGRAM = 'beforeProgram'
  public final static PROGRAM_STARTED = 'programStarted'
}
```

### classes in client repo: src/com/client/event_data/

```groovy
class BeforeProgramEvent implements EventDataInterface {
  public final static NAME = ProgramEvents.BEFORE_PROGRAM

  String getData() {
    return 'I am data for before program event'
  }
}

class StartedProgramEvent implements EventDataInterface {
  public final static NAME = ProgramEvents.PROGRAM_STARTED

  String getData() {
    return 'I am data for started program event'
  }
}
```

### code in client repo: vars/testPipeline.groovy

```groovy
// […]
EventDispatcher eventDispatcher = IOC.get(EventDispatcher)
eventDispatcher.attachClasses([
  ProgramBeforeListener,
  ProgramStartedListener,
  ProgramStartedListener2,
  ProgramStartedListener3,
  ProgramSubscriber,
])
// […]
```

### code in client repo:  any pipeline in vars/

```groovy
// Actual code inside a standard pipeline eventDispatcher.dispatch(ProgramEvents.BEFORE_PROGRAM, new BeforeProgramEvent()
print("START THE PROGRAM\n")
eventDispatcher.dispatch(ProgramEvents.PROGRAM_STARTED, new StartedProgramEvent())
```

### Actual output would be

```
One reason this did not run.
Another reason this listener did not run.
LISTENER 2 TRIGGERED (first to run because getOrder returns 1.)
SUBSCRIBER HAS RUN. (runs twice, before program and after. Nothing runs after because stopPropagationAfterRun() = true)
This is the data:I am data for before program event START THE PROGRAM LISTENER 2 TRIGGERED (first to run because
getOrder returns 1.)
LISTENER 1 TRIGGERED, (second to run because getOrder returns the default:1000.)
SUBSCRIBER HAS RUN. (runs twice, before program and after. Nothing runs after because stopPropagationAfterRun() = true)
This is the data:I am data for started program event
```
