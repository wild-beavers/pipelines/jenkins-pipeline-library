---
layout: post
---

# Debugger component

A Simple component to display debug information or run some code if debug conditions are met.

Some benefits:

- Allow to display some information only if a user wants to see the debug information.
- Prevents very verbose pipeline where it’s difficult to find relevant information.

## Important classes

- `com.wildbeavers.io.Debugger`: can show debug message and knows if debug var exists
- `com.wildbeavers.listener.DebugInformationDisplayerListener`: shows runtime debug information if debug var exists, like registered classes, user raw settings, current runtime software versions, etc.
- `com.wildbeavers.listener.JobRuntimeDebuggingListener`: dynamically enables debugging at runtime

## Explanations

Debugging is enabled through a global environment variable named `DEBUG`.
This environment variable can be set anywhere.

Alternatively, debugging can be enabled by end user during runtime, using [job settings]({{ "/default_steps/job_settings.html" | relative_url }}).

## Example

*Anywhere*:

```groovy
import com.wildbeavers.di.IOC

// […]
Debugger debugger = IOC.get(Debugger.getName())

// does not print anything, no DEBUG var set
debugger.printDebug('hello')

env.DEBUG=true

// prints "hello"
debugger.printDebug('hello')
// return true
assert debugger.debugVarExists() == true
```
