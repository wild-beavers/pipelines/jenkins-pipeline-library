---
layout: post
---

# Deprecation Component

This components allow you to handle code lifecycle, marking some code as deprecated.

Some benefits:

- Allow to gracefully remove code without breaking everything at the same time…
- … while still forcing maintainers to change their libraries by setting a hard-limit date.

## Important classes

- `com.wildbeavers.deprecation.DeprecatedFunction`: Wraps a function to mark it as deprecated.

## Explanations

`DeprecatedFunction` can wrap closure in its `execute` function.
Along the closure, a date is passed.
If the current date is before the passed date, any pipeline running this code will show a warning.
If the current date is after the passed date, any pipeline running this code will throw an exception.

## Example

```groovy
import com.wildbeavers.deprecation.DeprecatedFunction

// […]
DeprecatedFunction deprecatedFunction = IOC.get(DeprecatedFunction)

deprecatedFunction.execute({
  if ('' == keyToCheck) {
    error('Cannot check a Map’s value with an empty key. Make sure the “keyToCheck” argument has a proper value.')
  }

  if (!mapToCheck.containsKey(keyToCheck) && '' != keyUndefinedErrorMessage) {
    error(keyUndefinedErrorMessage)
  }

  if (!(mapToCheck.containsKey(keyToCheck)) || null == mapToCheck[keyToCheck]) {
    mapToCheck[keyToCheck] = defaultValue
  }

  if (!(expectedValueType.isInstance(mapToCheck[keyToCheck]))) {
    error("The type of the value for key “${keyToCheck}” is expected to be “${expectedValueType}”, given: “${mapToCheck[keyToCheck].getClass()}”")
  }
}, 'mapAttributeCheck', 'WildMap object', '01-10-2021')
```
