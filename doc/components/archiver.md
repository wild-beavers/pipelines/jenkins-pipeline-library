---
layout: post
---

# Archive component

A component to compress and uncompress files into/from archives.

Some benefits:

- Provides an interface to compression based on file names.
- Easily extendable to support all possible format.

## Important classes

- `com.wildbeavers.archiver.Archiver`: facade in charge to retrieve the correct concrete implementation of an `Archiver`.
- `com.wildbeavers.archiver.ArchiverCollection`: a collection of concrete `Archiver` implementations.
- `com.wildbeavers.archiver.*`: all other files in the namespace are implementations of `Archiver`s.

## Limitations

- The only implementation is `.tar.gz`. Component is easily extendable.

## Explanations

- The `ServiceRegisterer` will register all implementations of the `IsArchiver` interface. They will then be stored in the `ArchivedCollection` object.
- During a pipeline, any class (often: a listener class) can request an `Archiver` to compress or uncompress.
  For that, it will make a request to the `Archiver`, to either compress or uncompress a given file or directory.
- The `Archiver` will ask the `ArchiverCollection` to give the first implementation that support the given file or directory.
- The `Archiver` retrieve the given implementation and executes it to compress or uncompress.
  If no implementation supports the given file or directory, an exception is raised.

## Graphical explanation

![archiver](../images/archiver.png)

## Example

*in com.client.artifact.xxx* or *com.wildbeavers.archiver.xxx*:

```groovy
/**
 * Dummy Archiver implementation to compress/uncompress files
 */
class ZstdArchiver implements IsArchiver {
  @Override
  CommandResult compress(List<String> pathsToArchive, String archiveName) {
    // compress pathsToArchive into archiveName
  }

  @Override
  CommandResult uncompress(String archivePath, String destinationPath) {
    // uncompress archivePath to destinationPath
  }

  @Override
  Boolean requirementsInstalled() {
    // Whether current context contains tooling to perform actions of this class
  }

  @Override
  Boolean supportArchive(String archiveName) {
    // Whether this class support archiveName
  }

  @Override
  String getSupportedAlgorithm() {
    // return the name of the supportedAlgorithm
  }
}
```
