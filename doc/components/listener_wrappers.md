---
layout: post
---

# Listener Wrappers Component

Allow to wrap [listeners](observer.html).
There are two main usage of wrappers:

1. Jenkins groovy runs under the [CPS library](https://github.com/jenkinsci/workflow-cps-plugin), forcing code wrapping.  The observer component make it quite difficult, so these component extension fix this problem.
1. You may want to use the listeners provided by this library to avoid re-writing them, while changing some features for your use-case.

However, this component is not recommended as a replacement of listeners.
While you could technically achieve the same goal with either wrappers or listeners, the later are more agile and easier to understand compared to wrappers.
Only use wrappers if you *have to* wrap a listener call, for example if you need to use a groovy CPS credentials block.

DOCUMENTATION INCOMPLETE.

## Important classes

- `com.wildbeavers.listener.wrapper.CanWrapListener`: interface to define a wrapper
- `com.wildbeavers.observer.EventDispatcher`: can register `EventListener`s but also `CanWrapListener` wrappers

## Explanations

- Any wrapper that `support` a given type of listener will be executed *in place of* executing the listener directly.
  That is to say, the wrapper will effectively *wrap* the supported listener.
- The `EventDispatcher` is the class in charge of executing either listeners or wrappers.

## Example
