---
layout: post
---

# Command component

A component to interface a command line.

Some benefits:

- Allow to run command line in various context (bash, docker, etc)
- Regulates what the pipline allows or not as command line arguments

## Important classes

- `com.wildbeavers.command.AbtractCommand`: abstract class that creates and run command lines.

## Explanations

## Example

*in com.client.command*:

```groovy
package com.wildbeavers.command

import com.wildbeavers.factory.OptionStringFactory
import com.wildbeavers.helper.ContainerRunner
import com.wildbeavers.io.Debugger

/**
 * Allows to run dummy commands.
 */
class Dummy extends AbstractCommand {
  @Override
  protected String getCommand() {
    return 'dummy'
  }

  @Override
  protected String getOptionStringDelimiter() {
    return '='
  }

  @Override
  protected Map<String, Map> getValidArguments() {
    return [
      '-h' : [
        type       : Boolean,
        default    : null,
        description: 'Shows a help message with all available CLI options.',
      ]
    ]
  }

  @Override
  protected Map<String, Map<String, Map<String, Object>>> getValidSubArguments() {
    [
      'test' : [
        '--toggle': [
          type       : Boolean,
          default    : null,
          description: 'Toggles something.',
        ]
      ],
    ]
  }

  Dummy(OptionStringFactory optionStringFactory, ContainerRunner containerRunner, Debugger debugger) {
    super(optionStringFactory, containerRunner, debugger)
  }
}
```

*anywhere*:

```groovy
  Dummy dummy = IOC.get(Dummy.getName())
  dummy.run('test', ['--toggle': false])
```
