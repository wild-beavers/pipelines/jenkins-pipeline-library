---
layout: post
---

# Artifact component

A component to fetch and upload artifacts in various locations.

Some benefits:

- Provides an interface to follow to be able to manage a new artifact source.
- Abstract download and upload of artifacts files.

## Important classes

- `com.wildbeavers.artifact.ArtifactStore`: facade in charge to retrieve the correct concrete implementation of an `ArtifactStore`.
- `com.wildbeavers.artifact.ArtifactStoreCollection`: a collection of concrete `ArtifactStore` implementations.
- `com.wildbeavers.artifact.*`: all other files in the namespace are implementations of `ArtifactStore`s.

## Limitations

- The only implementation is AWS S3. Component is easily extendable.

## Explanations

- The `ServiceRegisterer` will register all implementations of the `IsArtifactStore` interface. They will then be stored in the `ArtifactStoreCollection` object.
- During a pipeline, any class (often: a listener class) can request an artifact to be downloaded or uploaded.
  For that, it will make a request to the `ArtifactStore`, to either download or upload to a given destination or source URL.
- The `ArtifactStore` will ask the `ArtifactStoreCollection` to give the first implementation that support the given URL.
- The `ArtifactStore` retrieve the given implementation and executes it to upload or download.
  If no implementation supports the given URL, an exception is raised.

## Graphical explanation

![artifact](../images/artifact0.1.0.png)

## Example

*in com.client.artifact.xxx* or *com.wildbeavers.artifact.xxx*:

```groovy
/**
 * Dummy ArtifactStore implementation to get/put artifacts in a SSH location
 */
class SSHStore implements IsArtifactStore {
  @Override
  CommandResult downloadArtifact(String sourceArtifactURL, String localArtifactPath, DockerOptions dockerOptions) {
    // sftp to download something from `sourceArtifactURL`, store it at `localArtifactPath`
  }

  @Override
  CommandResult uploadArtifact(String destinationArtifactURL, String localArtifactPath, DockerOptions dockerOptions) {
    // sftp to upload something from `destinationArtifactURL`, get it from `localArtifactPath`
  }

  @Override
  Boolean supportURL(String url) {
    // if url =~ ssh://myIP/artifact: true
    // else: false
  }
}
```
