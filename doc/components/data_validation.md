---
layout: post
---

# Data Validation component

A component to validate user input data.

Some benefits:

- Provides a simple business class to validate user inputs, even from complex maps.
- Provide an easy way to add validators, based on base validation types.

## Important classes

- `com.wildbeavers.data_validation.DataValidator`: business class to validate user inputs, even from complex maps.
- `com.wildbeavers.archiver.types.generic.*`: abstract classes to extend to easily create a new validator.

## Limitations

## Explanations

- The `DataValidator` offers methods to validate a given input, either raw of from a map and a key, and check the type of the given value.
- `DataValidator` throws a human-readable exception when the given input does not match the given type.
- If the given type extends the `IsValidatorType` interface, then `DataValidator` will try to instantiate the `IsValidatorType` implementation with the raw input.
- The instantiation process is expected to throw a `ValidationException` if the raw input is invalid.

## Example

in *com.wildbeavers.container.data_validation.type.ContainerType*:

```groovy
[…]
import com.wildbeavers.data_validation.type.generic.Enum

class ContainerTool extends Enum {
  ContainerTool(CharSequence input) {
    super(input)
  }

  @Override
  List<CharSequence> getSupportedValues() {
    return [ContainerDockerEventData.DEFAULT_TOOL, ContainerPodmanEventData.DEFAULT_TOOL]
  }
}
```

in *com.wildbeavers.container.listener.*:

```groovy
[…]
import com.wildbeavers.container.data_validation.type.ContainerType

class SomeDataValidationListener extends EventListener<ControllerEventData> {
  […]
  private DataValidator dataValidator

  SomeDataValidationListener(DataValidator dataValidator, ...) {
    this.dataValidator = dataValidator
    […]
  }

  […]

  ControllerEventData run(ControllerEventData eventData = null) {
    […]
    data.getContainerOptions().setTool(
      // This below validate the map value of config.containerRuntime
      // If config does not have a "containerTool" key, "data.getContainerOptions().getTool()" is used as default value
      // config.containerRuntime is checked to be of type "ContainerTool"
      // If config.containerRuntime is not of type "ContainerRuntime", new ContainerTool(config.containerTool) would be returned instead
      // If the instantiation fails, the validator will produce a human readable error
      this.dataValidator.validate(config, 'containerTool', ContainerTool, data.getContainerOptions().getTool())
    )
    […]
  }
```
