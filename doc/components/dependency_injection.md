---
layout: post
---

# Dependency Injection Component

Dependency injection takes care of class instances and instantiation.

Some benefits:

- Removes the need to create complex objects and their dependencies when a developer need them.
- Allow inversion of control (`IOC` class).
  Meaning implementation object can be injected, either at compile or runtime according to users needs.
- Brings automated instantiation of classes at runtime, not before.
- Provides a singleton feature, ensuring business object are created only once.
  It save up memory and compute time.

## Important classes

- `com.wildbeavers.di.IOC`: contains all the registered closures to instantiate classes at runtime as well as
  instantiated singleton objects.
- `com.wildbeavers.service.ServiceRegisterer`: register classes to the `IOC`.
- `com.wildbeavers.service.GenericFactory`: provide a generic factory class to create new classes at runtime

## Explanations

Without dependency injection, code looks like this.
Experience developers know it can become quite daunting:

```groovy
DepOne dep = new MyClassDependency()
DepOne dep2 = new MyClassDependency2(dep)
DepOne depAlt = new MyClassDependency("value")
Another another = new AnotherDependency()

MyService service = new MyService(another, dep2, depAlt)
```

Instead, one can simply use:

```groovy
import com.wildbeavers.di.IOC

MyService service = IOC.get(MyService)
```

To obtain such behavior, `MyService` needs to be registered within the `IOC` object.
(This task is handled by an object called `ServiceRegisterer` in this project).
Custom classes - like `ServiceRegisterer` - can be written to register custom classes as well as replace default ones.

```groovy
import com.wildbeavers.di.IOC

IOC.register(MyClassDependency, { determinant = null ->
  return new MyClassDependency(determinant)
})
// […]
IOC.register(MyService, {
  return new MyService(IOC.get(AnotherDependency), IOC.get(MyClassDependency), IOC.get(MyClassDependency, "value"))
})
```

`IOC` has only `static` methods and thus is globally available.
With the above partial example, one can instantiate new `MyService` trivially, without knowledge of dependencies:

```groovy
import com.wildbeavers.di.IOC

MyService service = IOC.get(MyService)

// Return true
// Every call of IOC.get(MyService) creates a new instance
assert service != IOC.get(MyService)
```

### Singleton

Most of the time, instantiating a class multiple time is useless.
Business classes for example are notoriously immutable, and thus would ideally be instantiated only once.
`IOC` offers a singleton feature.

```groovy
import com.wildbeavers.di.IOC

// […]
IOC.registerSingleton(MyService, {
  // This Closure will be run only once
  return new MyService(IOC.get(AnotherDependency), IOC.get(MyClassDependency), IOC.get(MyClassDependency, "value"))
})

// return true
// Many calls of IOC.get will always return the exact same object in memory
assert IOC.get(MyService) == IOC.get(MyService)
```

Singletons optimize performance and RAM usage drastically.
They should always be preferred over "normal" registration.

#### Factories

Another useful feature of the `IOC` is the ability to register factories:

```groovy
import com.wildbeavers.di.IOC

// Register a GenericFactory able to create Integer objects.
IOC.registerFactory(Integer)
```

On its own, it does nothing very useful, but `IOC` also allows to bind a named runtime Closures to factories:

```groovy
import com.wildbeavers.di.IOC

IOC.bindProductLine(Integer, 'a_one', {
  return 1
})
IOC.bindProductLine(Integer, 'a_two', {
  return 2
})
```

Then, at runtime, factories can instantiate classes by “product name”.
Instead of instantiating classes manually (e.g. `new MyData()` of even `IOC.get(MyData)`),
factories grants users the ability to add their own product lines to existing factories,
changing behavior of existing classes without modifications.

```groovy
import com.wildbeavers.di.IOC

IOC.getFactory(MyData).instantiate('a_one') == 1
IOC.getFactory(MyData).instantiate('a_two') == 2

// […]

IOC.bindProductLine(Integer, 'a_three', {
  return 3
})

IOC.getFactory(MyData).instantiate('a_three') == 3
```

#### IOCRefreshable

`IOC` can also refresh class dependencies of already-stored instances.
Because of singletons behavior, objects are sometime already instantiated with fixed dependencies.
`IOCRefreshable` allows to “reload” dependencies that change during runtime.
To do so, implement the `IOCRefreshable` interface in any singleton-registered class.
It will force you to implement a `refreshDependencies`.

A simple implementation would simply be:

```groovy
import com.wildbeavers.di.IOC

class MyAwesomeClass implements IOCRefreshable {
  private Dep dep

  MyAwesomeClass(Dep dep) {
    this.dep = dep
  }

  Dep getDep() {
    return this.dep
  }

  @Override
  void refreshDependencies() {
    // Before this call, the original dep object is used
    this.dep = IOC.get(Dep) as Dep
    // After this call, the new dep object is used
  }
}

// […]

IOC.registerSingleton(Dep, {
  new Dep()
})

IOC.registerSingleton(MyAwesomeClass, {
  new MyAwesomeClass(IOC.get(Dep))
})
```

When `MyAwesomeClass` is instantiated, it will come with a `Dep` associated object:

```groovy
MyAwesomeClass myAwesomeClass = IOC.get(MyAwesomeClass)
myAwesomeClass.getDep().toSring() // Dep@ID1
```

However, granted `Dep` needs to be changed after the code above, we are facing an issue:

```groovy
// This does not actually work, because of the issue with the assert below
IOC.registerSingleton(Dep, {
  new EnhancedDep()
})

// If the above was possible, this would fail
// It’s because the EnhancedDep object is not the same contained in previously instantiated myAwesomeClass
assert IOC.get(Dep).toString() == myAwesomeClass.getDep().toSring()
```

Instead we call `IOC.refreshSingleton(_)`, the `MyAwesomeClass` will now automatically get the new `dep` instance.

```groovy
import com.wildbeavers.di.IOC

// The following call will trigger MyAwesomeClass.refreshDependencies()
IOC.refreshSingleton(Dep, {
  new EnhancedDep()
})

// return true
assert IOC.get(Dep).toString() == myAwesomeClass.getDep().toSring()
```

**For obvious reasons, this feature is only available for singleton classes.**

## Graphical explanation

![dependency_injection](../images/dependency_injection.png)

## CAVEAT

1. There is no easy way in Jenkins CPS to execute a common script at the beginning of all the jobs.
   Therefore, a call to `com.wildbeavers.service.ServiceRegisterer.init(this)` is required to make the component usable.
   If not, users will typically encounter a “No class registered with the name” exception.
1. Every class needs a "manual" registration in the `com.wildbeavers.di.IOC`.
   Feature like auto-detection of classes is impossible in this context.
   This is because code reflexion is not available within Jenkins CPS for security purpose.

## Example

### in a client repo: src/com/client/service/ServiceRegisterer

```groovy
import com.client.service.MyAwesomeClass
import com.client.service.MyAwesomeDataClass
import com.wildbeavers.di.IOC

class ServiceRegisterer {
  static alreadyRegistered = false

  static init() {
    if (true == alreadyRegistered) {
      return
    }

    IOC.registerSingleton(MyAwesomeClass, {
      return new MyAwesomeClass(IOC.get(MyAwesomeDataClass))
    })
    IOC.register(MyAwesomeDataClass, {
      return new MyAwesomeDataClass()
    })

    alreadyRegistered = true
  }
}
```

### in a client repo: vars/whateverStep.groovy

```groovy
import com.wildbeavers.service.ServiceRegisterer as WBServiceRegisterer
import com.client.service.ServiceRegisterer
import com.wildbeavers.di.IOC
import com.client.service.MyAwesomeClass
import com.client.service.MyAwesomeDataClass

def call(Map config = [:]) {
  // This is to register WildBeaver classes
  WBServiceRegisterer.init(this)
  // This is to register client custom classes, defined above
  ServiceRegisterer.init()

  // […]
  MyAwesomeClass myClass = IOC.get(MyAwesomeClass)
  MyAwesomeDataClass dataClass = IOC.get(MyAwesomeDataClass)
}
```
