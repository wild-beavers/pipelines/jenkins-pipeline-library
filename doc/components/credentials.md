---
layout: post
---

# Credentials component

A component to load, expose and conceal credentials.

Some benefits:

- Provides a simple interface to load secrets from various sources, and expose them to various locations.
- Contrary to CPS, can procedurally load, manipulate and conceal secrets in different scopes.

## Supported implementations

### Credential classes & aliases

See `com.wildbeavers.credentials.data.impl` for an up-to-date list

- `AWSAccessKeyCredential` -> *awsAccessKey*
- `SshPrivateKeyCredential` -> *sshPrivateKey*
- `TokenCredential` -> *token*
- `UnknownSecretCredential` -> *unknown*
- `UsernamePasswordCredential` -> *usernamePassword*

### Credential Provider (Credential Backends) classes & aliases

See `com.wildbeavers.credentials.provider.impl` for an up-to-date list

- `JenkinsCredentialProvider` -> *jenkins*
- `AWSSecretsManagerCredentialProvider` -> *awsSecretsManager*

Providers may work in specific circumstances, for example, `AWSSecretsManagerCredentialProvider` can only parse very
specific JSON keys.
For more information, please refer to the class directly as it’s the best up-to-date source to get the information.

### Credential Exposers classes & aliases

See `com.wildbeavers.credentials.exposer.impl` for an up-to-date list

- `EnvironmentVariableCredentialExposer` -> *environmentVariable*
- `FileCredentialExposer` -> *file*

## Important classes

- `com.wildbeavers.credentials.data.Secret`: represent a secret
- `com.wildbeavers.credentials.data.SecretBag`: abstract credential class that contains a list of secret (e.g. for a
  username/password will contain a Secret type username and a Secret type password).
- `com.wildbeavers.credentials.data.impl.*`: all implementations of `SecretBag`.
- `com.wildbeavers.credentials.exposer.CredentialExposer`: able to expose all the `Secret` a `Credential` holds.
- `com.wildbeavers.credentials.exposer.SecretExposer`: abstract class to expose a `Secret` to all its define exposition
  locations.
- `com.wildbeavers.credentials.exposer.impl.*`: all implementations of `SecretExposer`
- `com.wildbeavers.credentials.provider.CredentialBackendProvider`: abstract class to provide raw secrets from backends.
- `com.wildbeavers.credentials.provider.impl.*`: all implementations of `CredentialBackendProvider`
- `com.wildbeavers.credentials.CredentialLoader`: user facade to load, expose and conceal implementations
  of `Credential`

## Explanations

- A `Credential` is a specific bag of one or multiple `Secret`, grouped in logical manner,
  like a password and its associated username.
  A `Secret` have a content, a description, multiple exposition locations as well as a default exposition location.
- `CredentialLoader` is a facade able to load, expose and conceal many `Credential`.
- `CredentialLoader` uses a chain of responsibility to determine what secret backend to use to load a `Credential`.
  A secret backend is a specific implementation of a `CredentialBackendProvider`,
  like Jenkins store or AWS Secret Manager.
- To load a `Credential`, users needs to pass a URL to the `CredentialLoader`.
  This URL contains the backend, the type of credential and the identifier of the credential.
  E.g. `jenkins:usernamePassword:my-password`:
  will load a Username/Password `Credential` that have the ID "my-password" from the Jenkins secret store
- URL can optionally omit the backend, e.g. `sshPrivateKey:service-ssh`
  In this case, all configured backends (all registered implementations of `CredentialBackendProvider`) will be checked
  in order of registration to load a secret with the ID "service-ssh" into a SSH private key kind of `Credential` until
  the secret is found.
- No implementations of `CredentialBackendProvider` are registered by default. See Examples below.
- Once loaded, `Credential` can be exposed (and later concealed) by the `CredentialLoader`.
  Much like credential backends, a `CredentialExposer` is charged to find the correct implementation of exposer
  depending on each `Secret` exposition locations.
  E.g `CredentialExposer` can write a SSH private key to a file, or a password inside an environment variable.

## Graphical explanation

![credentials](../images/credentials.png)
Rough representation of the credential system.

## Examples

First, `CredentialBackendFetcher` instances must be registered.
None are registered by default, since every project would use different backends for secrets.
Here is an example with Jenkins credential store and AWS Secret Manager:

```groovy
IOC.registerSingleton('MyCredentialLoader', {
  // Instantiate a new Fetcher for BackendProviders
  Fetcher<CredentialBackendProvider, ExpositionLocation, String> credentialBackendFetcher = IOC.get(Fetcher)

  // JenkinsCredentialProvider is registered first, and thus, will have priority
  credentialBackendFetcher.register(IOC.get(JenkinsCredentialProvider))

  // Two flavors of AWSSecretsManagerCredentialProvider are registered, for main and fallback AWS regions
  credentialBackendFetcher.register(IOC.get(AWSSecretsManagerCredentialProvider, 'aws', '123456789012', 'us-east-1'))
  credentialBackendFetcher.register(IOC.get(AWSSecretsManagerCredentialProvider, 'aws', '123456789012', 'ca-central-1'),)

  // Changing the default CredentialLoader backend provider fetcher to use the one we setup
  CredentialLoader credentialLoader = IOC.get(CredentialLoader)
  credentialLoader.setCredentialBackendProviderFetcher(credentialBackendFetcher)

  return credentialLoader
})
```

Along with the custom `CredentialLoader` registered above, it is now possible to load credentials:

```groovy
CredentialLoader credentialLoader = IOC.get('MyCredentialLoader')

// Fetch an "unknown" kind of secret. It first tries Jenkins credential store, then AWS Secret Manager
UnknownSecretCredential credential = credentialLoader.load('unknown:thename-of-mysecret-2V8BUa')
println(credential.getUnknownSecret().getContent())

// Fetch a "username/password" secret. Only tries AWS Secret Manager
UsernamePasswordCredential userPass = credentialLoader.load('AWSSecretsManager:usernamePassword:username-pass-json-2V8BUa')
println(userPass.getUsername().getContent())
println(userPass.getPassword().getContent())
```

See [Supported Implementations](#supported-implementations) above to help composing URL.

Credentials can be exposed to different locations, like environment variables or files:

```groovy
// Will expose username and password to their default locations: environment variables names WB_USERNAME, WB_PASSWORD
credentialLoader.expose(userPass)

// Remove the previously set environment variables
credentialLoader.conceal(userPass)
```

It is possible to setup custom exposition locations.
Multiple locations are possible for the same secret:

```groovy
userPass.getUsername().addExpositionLocation(new ExpositionLocation('file:.config/service/username'))
userPass.getPassword().addExpositionLocation(new ExpositionLocation('file:.config/service/password'))

// Will expose username and password to the files defined above
credentialLoader.expose(userPass)

// Will remove the files created above
credentialLoader.conceal(userPass)
```
