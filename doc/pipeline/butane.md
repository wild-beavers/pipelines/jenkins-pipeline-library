---
layout: post
---

# Butane/Ignition (butane)

Runs a pipeline for a butane (ignition) repository.

## Pipeline & Listeners

- `vars/standardButane`: Jenkins pipeline for butane
- `com.wildbeavers.butane.listener.ButaneToIgnitionListener`: Compiles a butane template to ignition

## Limitation

## Configuration

Every configuration is under the `butane` namespace:

| Name                             | Type                | Description                                                                         | Default                      |
|----------------------------------|---------------------|-------------------------------------------------------------------------------------|------------------------------|
| containerTool                    | String              | Container tool to run butane                                                        | 'docker'                     |
| containerImageName               | String              | Name of the container image containing the butane binary                            | 'butane'                     |
| containerImageTag                | String              | Image tag of the container image containing the butane binary                       | 'release'                    |
| containerFqdn                    | String              | FQDN of the container image containing the butane binary                            | 'quay.io/coreos'             |
| containerVolumes                 | Map<String, String> | List of additional volume mounts for container running the butane binary            | [:]                          |
| containerEnvironmentVariables    | Map<String, String> | List of additional environment variables for container running the butane binary    | [:]                          |
| containerNetwork                 | String              | Docker network configuration for the running the butane binary                      | ''                           |
| containerUserns                  | String              | Userns option to run the container for butane                                       | ''                           |
| containerPrivileged              | Boolean             | Whether to run the butane container as privileged                                   | false                           |

## Example

```groovy
standardButane(
  [
    'butane_containerTool': 'podman',
  ]
)
```
