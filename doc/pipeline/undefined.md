---
layout: post
---

# Undefined (undefined)

Runs a pipeline for an undefined repository.
This is a special pipeline that will stay free of any specific listeners but will still run the [default steps]({{ "/#default-steps" | relative_url }}).

## Pipeline & Listeners

- `vars/standardUndefined`: Jenkins pipeline for an undefined repository

## Limitation

No limitation

## Configuration

No configuration

## Example

```groovy
standardUndefined(
  [:]
)
```
