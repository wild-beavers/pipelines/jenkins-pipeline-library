---
layout: post
---

# Container (container)

Runs a pipeline for Docker/Podman/etc (container) code.

## Important classes

- `vars/standardContainer`: Jenkins pipeline for container.
- `com.wildbeavers.container.*`: All code specific for container pipeline.

## Limitation

## Configuration

Every configuration is under the `container` namespace (prefix with `container_`):

| Name                         | Type                | Description                                                                                                                                | Default                                   |
|------------------------------|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| container_tool               | String              | What container tool to use for default options the pipeline. (This will change many options defaults. Check corresponding data object)     | 'podman'                                  |
| container_authfile           | String              | Path of the authentication file.                                                                                                           | '${XDG_RUNTIME_DIR}/containers/auth.json' |
| container_makefilePath       | String              | Specifies a makefile which contains instructions for building the image                                                                    | 'Containerfile'                           |
| container_manifestName       | String              | Name of the manifest list to which the image is added. Creates the manifest list if it does not exist.                                     | null                                      |
| container_manifestFormat     | String              | Control the format for the built image’s manifest and configuration data.                                                                  | 'oci'                                     |
| container_buildPlatforms     | List<String>        | List of os/arch of the built image (and its base image, when using one) to the provided value instead of using the current OS architecture | ['linux/amd64', 'linux/arm64']            |
| container_buildCompress      | Boolean             | Compress the build context using gzip                                                                                                      | null                                      |
| container_buildArgFile       | String              | Specifies a file containing lines of build arguments of the form arg=value. The suggested file name is argfile.conf.                       | null                                      |
| container_buildCacheFrom     | String              | Repository to utilize as a potential cache source.                                                                                         | null                                      |
| container_buildCommand       | String              | Build command line                                                                                                                         | 'podman build'                            |
| container_buildNetwork       | String              | Sets the configuration for network namespaces when handling RUN instructions.                                                              | null                                      |
| container_buildPull          | String              | Pull image policy. See your buildCommand-specific documentation                                                                            | null                                      |
| container_buildTag           | String              | Specifies the name which is assigned to the resulting image if the build process completes successfully.                                   | null                                      |
| container_buildQuiet         | Boolean             | Suppress the build output and print image ID on success.                                                                                   | null                                      |
| container_buildTarget        | String              | Path where to make the build.                                                                                                              | '.'                                       |
| container_buildArgs          | Map<String, String> | Map of build-time variables.                                                                                                               | null                                      |
| container_tagCommand         | String       | Tag command line.                                                                                                                                 | 'podman tag'                              |
| container_inspectEnabled     | Boolean      | Display the inspect of the image/manifest before pushing                                                                                          | true                                      |
| container_inspectCommand     | String       | Inspect command                                                                                                                                   | 'podman inspect'                          |
| container_pushCommand                  | String       | Push command line.                                                                                                                      | 'podman build'                            |
| container_pushDestinations             | List<String> | List of registry/image where to push the built docker images                                                                            | null                                      |
| container_pushCertDir                  | String       | Use certificates at path (*.crt, *.cert, *.key) to connect to the registry.                                                             | null                                      |
| container_pushDigestfile               | String       | After copying the image, write the digest of the resulting image to the file.                                                           | null                                      |
| container_pushEncryptLayer             | String       | Layer(s) to encrypt: 0-indexed layer indices with support for negative indexing                                                         | null                                      |
| container_pushEncryptionKey            | String       | The [protocol:keyfile] specifies the encryption protocol, which can be JWE, PGP, PKCS7 and the key required for image encryption.       | null                                      |
| container_pushFormat                   | String       | Manifest Type (oci, v2s2, or v2s1) to use when pushing an image.                                                                        | null                                      |
| container_pushQuiet                    | Boolean      | When writing the output image, suppress progress output.                                                                                | null                                      |
| container_pushSignBy                   | String       | Add a “simple signing” signature at the destination using the specified key.                                                            | null                                      |
| container_pushSignBySigstore           | String       | Add a sigstore signature based on further options specified in a container’s sigstore parameter files passed here.                      | null                                      |
| container_pushSignBySigstorePrivateKey | String       | Add a sigstore signature at the destination using a private key at the specified path.                                                  | null                                      |
| container_pushSignPassphraseFile       | String       | If signing the image (using either --sign-by or --sign-by-sigstore-private-key), read the passphrase to use from the specified path.    | null                                      |

## Example

Using podman as a tool with docker-compatible options with only "amd64" arch:

```groovy
standardContainer([
    container_authfile:         com.wildbeavers.container.event_data.ContainerDockerEventData.DEFAULT_AUTHFILE,
    container_makefilePath:     'Dockerfile',
    container_manifestFormat:   'docker',
    container_buildTag:         'my_image',
    container_buildPlatforms:   ['linux/amd64'],
    container_buildPull:        'true',
    container_inspectEnabled:   false,
    container_pushDestinations: ['docker.io/mycompany/my_image'],
])
```

Using podman as a tool:

```groovy
standardContainer([
    container_manifestName:   'my_image',
    container_manifestFormat: 'oci',
    container_buildPull:      'newer',
    container_buildArgs: [
      LICENSE:   'GPL-2',
      VCS_URL:   'https://link-to-the-code/',
      VCS_TITLE: 'my_image',
    ],
    container_pushDestinations: ['docker.io/mycompany/my_image'],
])
```
