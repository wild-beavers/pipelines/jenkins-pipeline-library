---
layout: post
---

# Terraform (terraform)

Runs a pipeline for Terraform (terraform) code.

- Terraform module: if they contain an `examples`, the pipeline will read all `examples` sub-directories and deploy them as tests.
  The pipeline will also check idempotency by default.
  At the end of the `examples` deployments, all resources will be destroyed.
- Deployment: if the repository contains a file name `deploy.tf`, the pipeline will also expect a `providers.tf` and consider the repository to be a deployment.
  No tests will be run and the pipeline will deploy according to the providers defined in the code.

## Important classes

- `vars/standardTerraform`: Jenkins pipeline for terraform.
- `com.wildbeavers.terraform.listener.*`: All listeners for Terraform.
- `com.wildbeavers.terraform.command.Terraform`: Terraform command line wrapper.
- `com.wildbeavers.terraform.event_data.TerraformEventData`: Terraform event data.

## Limitation

## Configuration

Every configuration is under the `terraform` namespace (prefix with `terraform_`):

| Name                          | Type                | Description                                                                                                                                                                 | Default     |
|-------------------------------|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| isDeployment                  | Boolean             | Whether the project container providers to be deployed to real/final environment                                                                                            | [:]         |
| commonOptions                 | Map<String, String> | Command line options common to all commands                                                                                                                                 | [:]         |
| containerVolumes              | Map<String, String> | List of additional volume mounts for container running the terraform binary                                                                                                 | [:]         |
| containerEnvironmentVariables | Map<String, String> | List of additional environment variables for container running the terraform binary                                                                                         | [:]         |
| containerImageName            | String              | Name of the container image containing the terraform binary                                                                                                                 | 'terraform' |
| containerImageTag             | String              | Image tag of the container image containing the terraform binary                                                                                                            | 'latest'    |
| containerFqdn                 | String              | FQDN of the container image containing the terraform binary                                                                                                                 | 'hashicorp' |
| containerNetwork              | String              | Docker network configuration for the running the terraform binary                                                                                                           | ''          |
| fmtOptions                    | Map<String, String> | Command line options for the terraform `fmt` commands                                                                                                                       | [:]         |
| deployApplyOptions            | Map<String, String> | Command line options for the terraform `apply` commands, only for deployments                                                                                               | [:]         |
| deployInitOptions             | Map<String, String> | Command line options for the terraform `init` commands, only for deployments                                                                                                | [:]         |
| deployPlanOptions             | Map<String, String> | Command line options for the terraform `plan` commands, only for deployments                                                                                                | [:]         |
| testApplyOptions              | Map<String, String> | Command line options for the terraform `apply` commands, only for examples tests.                                                                                           | [:]         |
| testDestroyOptions            | Map<String, String> | Command line options for the terraform `destroy` commands, only for examples tests.                                                                                         | [:]         |
| testInitOptions               | Map<String, String> | Command line options for the terraform `init` commands, only for examples tests.                                                                                            | [:]         |
| testPlanOptions               | Map<String, String> | Command line options for the terraform `plan` commands, only for examples tests.                                                                                            | [:]         |
| validateOptions               | Map<String, String> | Command line options for the terraform `validate` commands                                                                                                                  | [:]         |
| workspace                     | String              | Terraform workspace to switch to.                                                                                                                                           | ''          |
| examplesDirectory             | String              | Directory containing the examples.                                                                                                                                          | 'examples'  |
| enableValidate                | Boolean             | Whether to enable `terraform validate`                                                                                                                                      | true        |
| enableFmt                     | Boolean             | Whether to enable `terraform fmt`                                                                                                                                           | true        |
| enableIdempotencyCheck        | Boolean             | Whether to enable a second `terraform plan`, to check idempotency                                                                                                           | true        |
| sensitiveResources            | List<String>        | List of Terraform resource’s names considered sensitive to destroy or replace. Users will be warned when such resources would be destroyed/replaced in a deployment’s plan. | []          |

## Example

For a Terraform module repository:

```groovy
// The examples here assume the secret variable are set
standardTerraform(
  [
    terraform_workspace              : 'test',
    terraform_testPlanOptions['-var']: [
      "aws_assume_role='${LAB_ASSUME_ROLE}'",
      "aws_access_key='${AWS_ACCESS_KEY}",
      "aws_secret_key='${AWS_SECRET_KEY}"
    ],
    terraform_containerImageTag      : '1.1',
    terraform_examplesDirectory      : 'tests',
    terraform_enableIdempotencyCheck : false
  ]
)
```
