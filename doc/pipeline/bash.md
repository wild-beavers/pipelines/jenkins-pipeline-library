---
layout: post
---

# Bash (bash)

Runs a pipeline for a bash repository.

## Pipeline & Listeners

- `vars/standardBash`: Jenkins pipeline for bash

## Limitation

- No tests
- No package publication

## Configuration

Every configuration is under the `bash` namespace:

| Name    | Type    | Description                         | Default |
|---------|---------|-------------------------------------|---------|

## Example

```groovy
standardBash(
  [:],
  [:]
)
```
