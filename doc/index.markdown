---
# Feel free to add content and custom Front Matter to this file. To modify the layout, see `https://jekyllrb.com/docs/themes/#overriding-theme-defaults`

layout: home
---

WildBeavers Pipeline Library is:

- A generic [Jenkins shared library](https://www.jenkins.io/doc/book/pipeline/shared-libraries/)
- An opinionated [framework](#inner-workings).

It attempts to “standardize” common pipelines, meaning it provides:

- A set of static ordered [stages](#stages), where code can be injected through [events](#raw-events).
- A set of [default steps](#default-steps) - running at different stages - offering common features like artifact management, job properties, basic linting, etc.
- Reusable pipelines by [technology](#supported-pipelines).
- Core features above standard Jenkins CPS, organized as [components](#components).

[For the full groovy-doc documentation of the code, click here](groovydoc)
[<img src="https://badgen.net/badge/Unit%20Test/Coverage/cyan">](unittestreport)


## Inner Workings

This section shows the big picture of the workflow this libraries offers.
When you are done reading, it’s advised to have a look at [components](#Components) to gain more knowledge of this library inner workings.

### User Configuration

This documentation and library calls "user configuration" a map of complex types passed to any kind of [pipeline](#supported-pipelines).
This user configuration tweaks how jobs will behave.
User configuration can be options for generic steps, like artifact fetching, auto-tagging, etc, but also for specific pipelines.
User configuration usually follows this format: *stepOrPipeline*_*descriptionOfOption*.
User configuration can be of any type, strings, boolean, number and sometimes more complex types.
User configuration can be changed by pipelines or listeners and sometimes humans within a `Jenkinsfiles`.
User configuration is passed to [pipelines](#pipelines).

E.G.

```groovy
def options = [
  scm_floatingTagsEnabled : true,
  scm_autoTagEnabled      : true,
])
```

### Pipelines

[Pipelines](#supported-pipelines) follow the [Jenkins shared library](https://www.jenkins.io/doc/book/pipeline/shared-libraries/) design.
They are stored in `/vars`.
It’s expected from users to create their [own pipelines](#how-to-use) or override the [default pipelines](#supported-pipelines) this library ships with.

This library intends to have *generic* pipelines per technology.
It means the library is supposed to adapt to any kind of code; but expect projects - for now - to be centered around a single programming language. E.g. python, Terraform, groovy, etc.
Users are invited to create their own pipeline following any convention they desire, or participate to this library to improve it.

To be compatible with this library, pipelines are expected to do only three things:

- (Re)set default [user configuration](#user-configuration)
- Attach pipeline-specific listeners.
- Eventually call `vars/job.groovy`, the backbone of this framework.

E.G.

```groovy
// e.g. see "vars/standardTerraform.groovy" for more information
standardTerraform([
  scm_floatingTagsEnabled : true,
  scm_autoTagEnabled      : true,
])
```

### vars/job.groovy

`vars/job.groovy` is the backbone of the framework.
It does two important things:

- Register generic listeners, offering all features of the default [steps](#default-steps)
- Dispatch all [events](#raw-events). These events are not configurable.

### Listeners

Listeners are ways for humans to add or change behavior of jobs/pipelines.
Each listener runs during one or more [events](#raw-events).
This library defines a lot of listeners: for the default [steps](#default-steps), but also for steps only run in specific pipelines, like Terraform.
Details are explained in the [observer documentation]({{ "/components/observer.html" | relative_url }}).

Like [user configuration](#user-configuration), listeners allow users to change pipeline behavior, but in an in-depth manner.
It’s the main way through which user can inject code and induce behavior in any pipeline.
Listeners are also more complex, because they require advanced Groovy (or Java) knowledge.

## Default steps

- [Artifact fetching]({{ "/default_steps/artifact_fetching.html" | relative_url }}): can fetch artifacts from various locations;
- [Artifact publishing]({{ "/default_steps/artifact_publishing.html" | relative_url }}): can publish artifacts to various locations;
- [Auto tag]({{ "/default_steps/checkout.html" | relative_url }}): can auto-tag a project by reading a sources changelog;
- [Cleanup delay]({{ "/default_steps/cleanup_delay.html" | relative_url }}): can delay the cleanup phase of the pipeline;
- [Container registry login]({{ "/default_steps/container_login.html" | relative_url }}): can login on a container registry, to use private docker images in the job;
- [File standard]({{ "/default_steps/file_standard.html" | relative_url }}): checks that a revision follows some basic file standards;
- [Job settings]({{ "/default_steps/job_settings.html" | relative_url }}): sets job options and properties;
- [Linter]({{ "/default_steps/linter.html" | relative_url }}): can run a series of lint against the current code;
- [Pre-commit run]({{ "/default_steps/pre_commit_run.html" | relative_url }}): can run pre-commit on project with pre-commit configuration;
- [SCM Checkout]({{ "/default_steps/checkout.html" | relative_url }}): checkout project of a multibranch pipeline;

## Supported pipelines

- [Bash]({{ "/pipeline/bash.html" | relative_url }})
- [Butane/Ignition]({{ "/pipeline/butane.html" | relative_url }})
- [Container]({{ "/pipeline/container.html" | relative_url }})
- [Terraform]({{ "/pipeline/terraform.html" | relative_url }})
- [Undefined]({{ "/pipeline/undefined.html" | relative_url }})

## Components

*Components are used internally to make this library works.
Components are an advanced subject, because they are used to fuel the default steps and the pipelines.
You can use these components yourself in your client library or extend them as you wish.*

- [Artifact]({{ "/components/artifact.html" | relative_url }})
- [Archiver]({{ "/components/archiver.html" | relative_url }})
- [Command]({{ "/components/command.html" | relative_url }})
- [Credentials]({{ "/components/credentials.html" | relative_url }})
- [Data Validation]({{ "/components/data_validation.html" | relative_url }})
- [Debugger]({{ "/components/debugger.html" | relative_url }})
- [Dependency Injection]({{ "/components/dependency_injection.html" | relative_url }})
- [Deprecation]({{ "/components/deprecation.html" | relative_url }})
- [Observer]({{ "/components/observer.html" | relative_url }})
- [Listener Wrappers]({{ "/components/listener_wrappers.html" | relative_url }})

## How to use

1. Load this shared library within any Jenkins project, multi-pipeline project or in the global configuration.
   This library must be loaded before any client specific library.

1. Use the existing pipelines in `vars` directly or:

1. Create your own client pipeline library to extend this library.
   You will be able to use your own configuration, credentials and even inject code without having to change any code here.

### Client Jenkins Shared Library Example

This is a basic example of a client library, inheriting this library.
Terraform is used in this example, but anything can be used.

#### com/client/service/ServiceRegisterer.groovy

```groovy
import com.wildbeavers.service.ServiceRegisterer as OGServiceRegisterer

class ServiceRegisterer {
  static Boolean alreadyRegistered = false

  static void init(Script context) {
    OGServiceRegisterer.init(context)

    if (alreadyRegistered) {
      return
    }

    registerAllGlobals()
    registerAllClasses()
  }
  // […]
```

#### vars/clientAWSContainer

```groovy
import com.wildbeavers.container.event_data.ContainerDockerEventData
import com.wildbeavers.di.IOC
import com.wildbeavers.observer.EventDispatcher
import com.client.container.listener.MyLocalListener
import com.client.service.ServiceRegisterer

def call(Map config = [:]) {
  ServiceRegisterer.init(this)
  registerListeners()

  Map config = [
    linter_enabled                    : true,
    linter_data                       : [
      [
        containerTool     : 'docker',
        containerImageFQDN: 'ghcr.io/hadolint',
        containerImageName: 'hadolint',
        containerImageTag : 'latest',
        command           : 'hadolint',
        commandArguments  : '< Containerfile'
      ],
      [
        containerTool     : 'podman',
        containerImageFQDN: 'docker.io/koalaman',
        containerImageName: 'shellcheck-alpine',
        containerImageTag : 'latest',
        command           : 'shellcheck',
        commandArguments  : 'sh -c "find resources -type f -name \'build\' -o -name \'*.sh\' -maxdepth 3 | xargs shellcheck -C -s bash"'
      ],
    ],

    containerLogin_containerRegistries: [
      [
        fqdn                                         : "748683470158.dkr.ecr.us-east-1.amazonaws.com/${config.container_manifestFile}",
        credentialProcessUsername                    : 'AWS',
        credentialProcessContainerRunCommandArguments: "ecr get-login-password --region $AWS_REGION",
        credentialProcessContainerOptions            : [
          fqdn     : 'registry.gitlab.com/wild-beavers/docker',
          imageName: 'aws-cli',
          imageTag : 'latest'
        ]
      ]
    ],

    container_authfile                : ContainerDockerEventData.DEFAULT_AUTHFILE,
    container_manifestFormat          : 'docker',
    container_buildPlatforms          : ['linux/amd64'],
    container_pushDestinations        : ["XXXXXXXXX.dkr.ecr.us-east-1.amazonaws.com/${config.container_manifestFile}"],
  ]

  config.headerMessage = """
Say hello to your job!
  """

  standardContainer(config)
}

private static void registerListeners() {
  EventDispatcher eventDispatcher = IOC.get(EventDispatcher)

  eventDispatcher.attachClasses([
    MyLocalListener,
  ])
}
```

## Raw Events

This is an exhaustive list of events that this library listens to.
For a more comprehensive explanations of events, see [Stages](#stages) and the [Observer component]({{ "/components/observer.html" | relative_url }}).

`ControllerEvents` run on the machine orchestrating jobs (e.g. Jenkins Master)
`RunnerEvents` run on the runners (e.g. Jenkins Slaves)

- ControllerEvents.PRE_DATA_VALIDATION
- ControllerEvents.DATA_VALIDATION
- ControllerEvents.POST_DATA_VALIDATION
- ControllerEvents.PRE_RUNNER
- RunnerEvents.PRE_PREPARE
- RunnerEvents.PREPARE
- RunnerEvents.POST_PREPARE
- RunnerEvents.PRE_LINT
- RunnerEvents.LINT
- RunnerEvents.POST_LINT
- RunnerEvents.PRE_STATIC_ANALYSIS
- RunnerEvents.STATIC_ANALYSIS
- RunnerEvents.POST_STATIC_ANALYSIS
- RunnerEvents.PRE_COMPILE
- RunnerEvents.COMPILE
- RunnerEvents.POST_COMPILE
- RunnerEvents.PRE_TEST_UNIT
- RunnerEvents.TEST_UNIT
- RunnerEvents.POST_TEST_UNIT
- RunnerEvents.PRE_TEST_FUNCTIONAL
- RunnerEvents.TEST_FUNCTIONAL
- RunnerEvents.POST_TEST_FUNCTIONAL
- RunnerEvents.PRE_BUILD
- RunnerEvents.BUILD
- RunnerEvents.POST_BUILD
- RunnerEvents.PRE_DEPLOY
- RunnerEvents.DEPLOY
- RunnerEvents.POST_DEPLOY
- RunnerEvents.PRE_TEST_INTEGRATION
- RunnerEvents.TEST_INTEGRATION
- RunnerEvents.POST_TEST_INTEGRATION
- RunnerEvents.PRE_PUBLISH
- RunnerEvents.PUBLISH
- RunnerEvents.POST_PUBLISH
- RunnerEvents.PRE_RELEASE
- RunnerEvents.RELEASE
- RunnerEvents.POST_RELEASE
- RunnerEvents.PRE_CLEANUP
- RunnerEvents.CLEANUP
- RunnerEvents.POST_CLEANUP
- ControllerEvents.POST_RUNNER

## Stages

Events and stages are expected to be used this way:

- `validation`: user inputs/configurations/options validation
- `lint`: basic linters, code syntax correctness check
- `static-analysis`: static contextual analysis, file structure and relationship checks
- `compile`: compilation (for compiled language) just sufficient to allow testing
- `test:unit`: file-by-file unit tests with no side effects
- `test:functional`: full program functional tests, with limited side effects
- `build`: artifact building, package building
- `deploy`: deployment to real environment, test or production, blue-green, rolling, etc.
- `test:integration`: integration test on deployment environment
- `publish`: artifacts publication
- `cleanup`: cleaning remaining deployment/integration tests, cache, etc.
- `release`: artifact release to be generally available, DNS switching

Events and stages are interdependent following this chart:

```
| - validation
| - lint
| - static-analysis
| - compile
    | - test:unit
    | - test:functional
| - build
    | - deploy
        | - test:integration
            | - cleanup
    | - publish
        | - release
```

However, this is mostly undesirable in the real life, because one wants pipeline to fail fast and avoid consuming too much resources.
This is what this project (might) tries to achieve:

1. validation
1. lint
1. static-analysis
1. compile
1. test:unit && test:functional
1. build
1. deploy
1. test:integration
1. publish && cleanup
1. release
