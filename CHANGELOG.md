## 11.19.2

- fix: warnings were showing because of ambiguous variable type

## 11.19.1

- refactor: (terraform) artifact cleaning now stops outputting in console
- refactor: (terraform) displays list of workspaces only in debug mode
- refactor: (artifactFetcher) log actions since they are prone to delays

## 11.19.0

- feat: always prints in light grey, to easily identify pipeline prints, from command results or jenkins outputs
- fix: command tailing was not working properly with stderr

## 11.18.1

- fix: stderr error was seen whenever a tail command would run

## 11.18.0

- feat: greatly simplify underlying bash command execution, with performance gains
- fix: (terraform) command targets were always printed out

## 11.17.0

- feat: adds some possible container options for podman
- feat: AWS artifacts fetcher now uses default option to slightly improve performances
- feat: improves performances to check an image exists

## 11.16.0

- feat: makes `ScmCredentialWrapper.isPipelineSupportingAuthentication()` public so one can use the class without having to check it would work externally.

## 11.15.0

- doc: improves `Fetcher` fetching error to be a bit more descriptive.
- feat: supports token credentials from various Jenkins plugins sources as a `TokenCredential`
- fix: (credential) also get Jenkins credentials from job context (directory) rather than only global creds

## 11.14.0

- feat: introduces `terraform_sensitiveResources` to warn users when these resources are destroyed or replaced in a deployment.
- test: removes JDK 11 testing as Jenkins fully migrated to JDK 17

## 11.13.0

- feat: (credential) implement TokenCredentials, supported by AWSSecretsManager provider

## 11.12.0

- feat: Add a new property `scmSshConfigDirectory` in RunnerEventData
- feat: (ScmCredentialWrapper) set the Git SSH config path in `RunnerEventData`

## 11.11.3

- fix: (tagCheck) was not running in `master` because of TestCanceller

## 11.11.2

- fix: (tagCheck) user data was not processed if not explicitly enabled
- fix: (scmInfo) tags sets were sometimes containing empty strings.

## 11.11.1

- fix: (tagCheck) were not properly registered to run

## 11.11.0

- feat: (tagCheck) adds new default step to check the sanity of tags (default to semver, without pre-release)

## 11.10.0

- feat: (terraform) do not refresh state during first attempt to terraform destroy
- refactor: (terraform) removes useless '--cgroup-manager'

## 11.9.3

- fix: wrong message was displayed for listener wrappers
- fix: circumvent obscure podman bug by using mandatory cgroups option for running `terraform`

## 11.9.2

- fix: no exception was thrown if a CLEANUP listener failed

## 11.9.1

- fix: cleanup listeners were not run at all costs, if previous cleanup listener failed.

## 11.9.0

- feat: (terraform) archives statefile when first destroy failed, as a precaution measure

## 11.8.0

- feat: `FileProxy` can now find pattern within a given file
- feat: (terraform) removes `TerraformRepositoryNameStandardListener` as it needs to be redone in a more generic manner
- feat: (terraform) passes all `TF_*` variables to default terraform container, thus makes podman the default container tool

## 11.7.0

- feat: (EnvironmentVariableProxy) adds getIfExists method
- fix: (ScmInfoManager) fix crash when `CHANGE_TARGET` environment variable is not defined

## 11.6.0

- doc: various small documentations fixes
- refactor: moves classes around to get closer to a `proxy` component
- fix: remote name was not reliably found in the refspec

## 11.5.0

- feat: modernize `IOC`:
   - can now register closures with unlimited number of arguments, thanks to recent version of CPS
   - can now register closures, to be registered in factories object at instantiation.
- feat: `GenericFactory` can now handles closures with arguments
- feat: default `aws` commands run inside container gets all `AWS_*` env vars from running hosts
- feat: (credential) implements a provider for AWS Secret Manager
- tech: runs any default aws-cli commands with podman instead of docker by default
- doc: fixes checkout table inputs not displaying correctly
- refactor: heavy changes on alpha component `credentials`, for example:
   - better scope classes under `credential` namespace
   - simplify and organize data classes
   - add low level validation
   - simplifies facade `CredentialHelper` into `CredentialLoader` with generic methods
- refactor: makes core library chain-of-responsibility system more generic

## 11.4.0

- refactor: (artifactPublisher) move artifact fetcher data validation from job to its listener. This introduce those user configuration properties renaming:
   - `artifactPublisher_baseURL` to `artifactPublisher_baseUrl`
- feat: implement `preUpload` method for AWSS3 artifact fetcher
- chore: bump pre-commit hooks

## 11.3.0

- refactor: changes artifact publishing to be more generic.
- refactor: `ConditionDebugger` moved into the `Debugger` class itself
- doc: better document inner workings of the library.

## 11.2.0

- feat: adds a listener to enabled debugging, controlled by:
   - `job_debuggingEnabled` => to enable debugging
   - `job_debuggingRuntimeToggleEnabled` => to allow user to enable debugging at runtime
   - `job_debuggingRuntimeToggleTimeout` => how much time to wait for user input to enable debugging at runtime

## 11.1.0

- feat: changes AutoTagger:
   - faster updates by regrouping tag deletion/additions
   - more pre-requisite checks to avoid breaking situations
   - handles user input changelog file, custom regexp and floating tags disabling

## 11.0.1

- fix: terraform init was failing when requiring SSH key

## 11.0.0

- feat: (BREAKING) changes user inputs to modernize validation:
   - `checkoutCredentialID` => `scm_alternativeCheckoutCredentialID`
   - `checkoutDirectory` => `scm_alternativeCheckoutDirectory`
   - `checkoutRepositoryURL` => `scm_alternativeCheckoutRepositoryURL`
   - `checkoutTag` => `scm_alternativeCheckoutTag`
   - `autoTagEnabled` => `scm_autoTagEnabled`
- feat: changes `AutoTaggerSSHWrapper` to more generic `ScmCredentialWrapper`, also wrapping `ScmInfoRefresherListener`
- feat: data validation now accept `null` values, even with custom validator
- feat: enhanced display of running listeners on the console
- refactor: (BREAKING) revamp `ScmInfo` data object

## 10.0.0

- feat: (BREAKING) renames input user configurations:
   - `config.properties` into `config.job_properties`
   - `config.label` into `config.job_nodeLabel`
   - `config.timeoutTime` into `config.job_timeoutTime`
   - `config.timeoutUnit` into `config.job_timeoutUnit`
- feat: validates user configuration for the job
- fix: `config.properties` was useless and not working
- fix: `PRE_RUNNER` events was run inside the runner, not inside the controller

## 9.7.1

- fix: (Autotagger) avoid re-tag when using metadata tags

## 9.7.0

- feat: adds `helper.CurrentUser` class to retrieve some user information in the current build
- feat: adds a new listener: `CleanupDelayListener` to delay cleanup for a given amount of time
- test: fixes gradle configuration to be ready for gradle 9
- fix: ScmInfo returns the latest tag following semver, but not being a pre-release tag
- fix: (terraform) adds `-or-create` options for workspace selection

## 9.6.1

- fix: bash pipeline missing import

## 9.6.0

- feat: handles versions with metadata, like `1.2.3-1`
- feat: adds first version for a git repository artifact fetcher
- refactor: removes deprecated `artifactsToFetch`XXX options, replaced by `artifactFetcher_`XXX

## 9.5.0

- feat: (container) adds `container_inspectEnabled` option to inspect manifest/image before pushing
- feat: (container) adds `container_inspectCommand` option to setup inspect command
- fix: do not display container command twice when command is not `--quiet`

## 9.4.0

- feat: when executing a command with trail, display the command.
- feat: (container) adds `container_containerTag` option to specify container tag command
- feat: (container) adds `ContainerTag` listener to tag built container depending on context
- feat: (container) adds `ContainerAliasGuesser` listener to set default tag when user didn’t
- fix: (container) do not print stdout for any login command

## 9.3.0

- doc: updates `observer` component documentation after recent changes
- doc: updates `observer` diagram

## 9.2.0

- refactor: moves internal classes around.
- refactor: removes `OptionsInputAsker`: not used
- refactor: removes `WildString`: not used
- refactor: removes `CheckoutData`: not used
- doc: adds first basic version of the `Checkout` default step

## 9.1.0

- feat: adds new generic step `fileStandard` to manage what files are mandatory or forbidden
- refactor: (terraform) updates pipeline to use new `fileStandard` step
- refactor: (butane) updates pipeline to use new `fileStandard` step
- doc: udpates CHANGELOG to recent new convention
- fix: fixes new `Regexp` validation type

## 9.0.0

- feat: (BREAKING) removes very old unused pipeline: `execute`, `awsNuke`, `standardDockerImage` and all its variants
- refactor: (BREAKING) Rename `DockerRunnerHelper` class to `ContainerRunner`
- refactor: (BREAKING) Simplifies `ContainerRunner` signature: ContainerOptions, command and forcePull
- refactor: (BREAKING) Moves data container classes under `data.container`
- refactor: (BREAKING) Changes `terraform` pipeline inputs: `docker` => `container`
- refactor: (BREAKING) Changes `butane` pipeline inputs: `docker` => `container`
- refactor: (BREAKING) Split `terraform` container `image` input to 3: `name`, `tag` and `fqdn`
- refactor: (BREAKING) Split `butane` container `image` input to 3: `name`, `tag` and `fqdn`
- refactor: (BREAKING) updates `standardTerraform` pipeline to comply to current standard: removes third config argument.
- refactor: (BREAKING) updates `standardButane` pipeline to comply to current standard: removes third config argument.
- refactor: Changes `g|setDockerOptions` to `g|setContainerOptions`
- refactor: replace `registerService` pipeline to `ServiceRegister.init(this)` method
- feat: add `data_validation` component with `DataValidator` and generic types
- feat: (terraform) refactor data validator to use `DataValidator` and generic types
- feat: (terraform) do not run `TerraformDiscoverCommandTargets` when project is deployment
- feat: (butane) modernize pipeline to use data validation listener and remove closures
- feat: (bash) modernize pipeline to use data validation listener and remove closures

## 8.4.0

- feat: adds `LinterListener` along with new use options to setup generic linters.

## 8.3.0

- doc: use one `##` in CHANGELOG instead of `####`

## 8.2.0

- feat: (container) adds `push` listener
- feat: (debugger) improves `DebugInformationDisplayer` to display many more info, like user input config
- fix: (container) fixes login when using AWS ECR
- fix: (container) do not add build args that are null or empty

## 8.1.1

- refactor: avoids using CPS fileExists directly in listeners as much as possible.

## 8.1.0

- feat: adds a `FileHelper` to wrap file checks and operations
- feat: adds a new factory feature to the dependency injection system, through `GenericFactory`
- feat: adds `standardContainer` pipeline
- feat: (container) adds `ContainerBuildListener` listener
- feat: allow `WildMap.check` to use `null` as a default value
- feat: optionally pass a `nullable` option when adding options to `OptionString`, to ignore `null` in specific cases
- doc: updates README with up-do-date information
- fix: now works even if `ansicolor` or `build-timeout` plugins are not installed
- fix: `ClosureHelper` now accept `null` for its closures

## 8.0.0

- refactor: (BREAKING) renames `DockerOptions` to `ContainerOptions`
- refactor: (BREAKING) (preCommit) uses authfile from `containerLogin` instead of requiring `preCommit_containerConfigJsonFilePath`
- feat: (BREAKING) (containerLogin) removes options: `dockerRegistry`, `dockerRegistryCredentialId` and `dockerRegistryLogin` in favor of new ones below.
- feat: (BREAKING) (Observer) change `EventListener` to use a generic for its data
- feat: (containerLogin) validates data in `ContainerLoginDataValidationListener` instead of doing it in `job`
- feat: (containerLogin) adds `ContainerLoginValidationListener` to validate user inputs for docker login listener.
- feat: (containerLogin) adds `ContainerLoginData` data object to handle container logins
- feat: (containerLogin) adds `ContainerRegistryOption` data object to handle container registry options
- refactor: (containerLogin) renames `DockerLoginListener` to `ContainerLoginListener`
- refactor: (gradle) empty `GradleCheckListener` because it’s not implemented yet
- doc: (containerLogin) adds documentation

## 7.0.0

- refactor: (BREAKING) (preCommit) changes configuration:
- `preCommitDockerImageName`: `preCommit_containerFQDN`, `preCommit_containerImageName` and `preCommit_containerImageTag`
- `preCommitDockerEnvironmentVariables`: `preCommit_containerEnvironmentVariables`
- `preCommitDockerAdditionalMounts`: `preCommit_containerAdditionalMounts`
- `preCommitConfigJsonFilePath`: `preCommit_containerConfigJsonFilePath`
- feat: (BREAKING) (preCommit) do not run as privileged by default anymore
- feat: (BREAKING) (DockerRunnerHelper) makes method `run` with first 'string' argument private, forcing all usage of the method to have a `DockerOptions`.
- feat: (DockerRunnerHelper) allows to change container runtime ("docker" still default) as well as setting `userns` option
- fix: (BREAKING) (DockerOptions) renames all `get|setPriviledge` to correct `get|setPrivileged`
- refactor: renames `PipelineEvents` to `RunnerEvents` to prepare for further refactors
- feat: (preCommit) adds new config `preCommit_enabled` and `preCommit_containerPrivileged` and `preCommit_runCommandArguments`
- feat: (preCommit) prepares data in its own listener: `PreCommitDataValidationListener` running in new `ControllerEvents.DATA_VALIDATION`
- feat: (job) introduce `ControllerEvents` run on the master with `DATA_VALIDATION` and `PRE_RUNNER`, `POST_RUNNER` events.
- doc: adds documentation for preCommit default step.
- fix: (WildMap) throw a nice error if the default value does not match expected type

## 6.4.0

- feat: (TerraformFileStandardListener) removes `variables.tf` amd `outputs.tf` as required files

## 6.3.0

- feat: allows to use `AWSTerraformInitSSHWrapper` without internal `AWSTerraformInitWrapper`

## 6.2.0

- feat: adds `ParallelProxy`, a proxy to the `parallel` cloudbees command
- feat: (artifacts) downloads artifacts in parallel to enhance performance
- feat: (artifacts) uploads artifacts in parallel to enhance performance
- refactor: makes `DockerRunHelper` stateless to help parallelization
- refactor: makes `TarGzArchiver` stateless to help parallelization
- maintenance: pins `aws-cli` wildbeavers image to `1`
- doc: extends documentation on artifact fetching and publishing
- doc: adds base documentation on the archiver component

## 6.1.1

- fix: fixes `AutoTaggerSSHWrapper`

## 6.1.0

- feat: adds `AutoTaggerSSHWrapper` to setup SSH credentials before tagging

## 6.0.1

- fix: brings back `AutoTaggerListener` to POST_PREPARE. The listener is not ready to be moved yet as it influences ScmInfo
- fix: removes `TerraformDeploymentReview` listener to include it in the `TerraformApply` one, for obvious order of events issue
- fix: make sure `testsCancellerEnabled` have a default `true` value

## 6.0.0

- feat: (BREAKING) removes the ability to run jobs within kind for now, because it’s untested and unmaintained
- feat: (BREAKING) removes the barely used `EventSubscriber`, in favor of the ability to listen to multiple events for `EventListener`
- feat: (BREAKING) runs `AutoTaggerListener` during PRE_PUBLISH and not PRE_BUILD
- feat: adds `ConditionDebugger` to help debug complex condition behaviors
- feat: adds a `TestCancellerListener` to be able to cancel lint, static analysis and tests if project is publishable; enabled by default.
- refactor: (BREAKING) `EventListener` must now `listenTo` an ArrayList, not just a String
- refactor: makes `AbstractEventListener.stopPropagationAfterRun` use `stopPropagation` protected property to act on it without duplication
- refactor: adds `attachClass` and `attachClasses` to `EventDispatcher` to lower `vars/` pipelines complexity
- refactor: makes `ServiceRegister` method static
- fix: `DockerLoginListener` actually checks that a credentialId exists before attempting to login
- test: adds unit test for `HeaderDisplayerListener`
- test: adds unit test for `EventDispatcher`

## 5.0.0

- feat: (BREAKING) update the pipeline with these steps:
- LINT: basic linters, code syntax correctness check
- STATIC_ANALYSIS: advanced static contextual analysis
- COMPILE: compilation, for compiled language, sufficient to allow testing
- TEST_UNIT: file-by-file unit tests with no side effects
- TEST_FUNCTIONAL: full program functional test on the compiled code, with limited side effects
- BUILD: artifact building, package building
- DEPLOY: deployment to real environment, test or production
- TEST_INTEGRATION: integration test on deployment environment
- PUBLISH: artifact publication
- CLEANUP: cleaning remaining deployment/integration tests
- RELEASE: artifact release to be generally available
- feat: (BREAKING) remove `test` and `notify` stages
- feat: (BREAKING) remove the file whitelist test for Terraform deployment, because it does not make sense for generic projects
- feat: (BREAKING) remove terraform events `destroy` and `fmt` to use standard `lint` and `cleanup`
- feat: (BREAKING) remove Inspec listener, to be replaced by a full pipeline
- feat: (BREAKING) refactor: `GEEAWSTerraformPlanWrapper` is now `GEEAWSTerraformApplyWrapper`
- feat: (BREAKING) removes deprecated pipelines - see version 4 to redo/update them:
- `cookstyle`
- `knife`
- `foodcritic`
- `fxInspecDockerImage`
- `gitea`
- `rocketchat`
- `helm`
- `awsSsm`, `awsSts`, `awsEcr`
- `ansible`
- `kitchen`
- `json2hcl`
- `nodeJsPipeline`
- `packer`
- `python`
- `stove`
- `task`
- `taskKubernetesSecret`
- `mapAttributeCheck`
- `yarn`
- `dockerImage`
- `pipelineChef`
- `pipelineNode`
- `pipelineHelm`
- `pipelinePacker`
- `pipelinePlaybook`
- `pipelinePython`
- `pipelineTask`
- `pipelineDocker`
- feat: adds dependency installer
- feat: adds pipeline status available in `PipelineEventData`
- feat: (composite) adds Fetcher abstract class
- feat: (python) adds python builder and its first implementation `PythonBuildBuilder`
- feat: (python) adds python tests and its first implementation `PythonTestsPytest`
- feat: (command) adds a new `runWithPrefix` method
- feat: (pipeline) adds `standardPython`
- feat: (terraform) adds the ability to set the `examples` directory name
- refactor: (terraform) simplifies `standardTerraform` to the minimum
- refactor: use Fetcher class on `CredentialExposerFetcher` and `CredentialProviderFetcher`
- refactor: (BREAKING, composite) rename `add` to `register` method
- test: extract coverage percentage
- test: adds many unit tests for terraform listeners
- test: removes JDK-8 tests, as it is not supported anymore by recent Jenkins
- maintenance: pre-commit deps bumps
- maintenance: pins `aws-cli` defaults to `0`
- doc: includes jacoco HTML reports in the documentation
- doc: (terraform) improves documentation

## 4.8.0

- feat: Modify Credentials implementations and add helper
- adds default exposer type to `Secret`
- (SecretBag) makes `getAllSecrets` public instead of protected
- (SecretBag) add `getExpositionLocation`
- (SshPrivateKeyCredential) adds `getUsernameExpositionLocation`, `getPassphraseExpositionLocation` and `getPrivateKeyExpositionLocation` to easily get the exposition location of a given type
- (UsernamePassword) adds `getUsernameExpositionLocation` and `getPasswordExpositionLocation` to easily get the exposition location of a given type
- (CredentialHelper) adds a helper to fetch, expose and conceal a SecretBag
- (EnvironmentVariableHelper) fixes `generateValidEnvironmentVariableName` to generate POSIX compliant name
- (EnvironmentVariableHelper) adds `transformStringToValidPosixEnvironmentVariable` static method to transform a string in a POSIX environment variable name
- (ServiceRegister) adds `CredentialProvider` implementations auto-register
- (ServiceRegister) adds `CredentialExposer` implementations auto-register
- doc: add credential component documentation

## 4.7.0

- feat: Finishing the Credentials implementation:
- moves `CredentialType` from package `credential.provider` to `credential.data`
- moves `JenkinsCredentialDataTransformer` from package `credential.provider` to `credential.data`
- adds `SecretBag` abstract class
- adds `SecretType` enum
- adds `SshPrivateKeyCredential` credential type implementation
- adds `UsernamePasswordCredential` credential type implementation
- changes `conceal` method signature in `CredentialExposer` interface to take a `Secret` parameter
- fixes `CredentialExposerType` and `CredentialProviderType` static attribute to String type
- adds `Debugger` attribute in `EnvironmentVariableCredentialExposer`
- changes `provide` method signature in `CredentialProvider` interface to take a `SecretBag` parameter
- changes `Secret` data class attribute from `String name` to `SecretType secretType` and its associated methods
- adds exposition data in `Secret` data class. This introduces a bunch of new method
- (helper/EnvironmentVariableHelper) fix: `exist` method now return a `Exception` if the parameter is null
- (helper/EnvironmentVariableHelper) fix: make sure `setWithRandomName` generate a POSIX environment variable name

## 4.6.0

- feat: adds `addAdditionalMounts` and `addEnvironmentVariables` method in `DockerOptions` data object
- feat: adds `preCommitDockerAdditionalMounts` and `preCommitDockerEnvironmentVariables` variables

## 4.5.1

- fix: () replace `do {} while()` structure to `while` because it is introduce in groovy 3, while jenkins run groovy 2.4 ([##JENKINS-69120](https://issues.jenkins.io/browse/JENKINS-69120))

## 4.5.0

- feat: Add a new way to fetch Credentials
- add `CredentialProvider` interface
- add `CredentialDataType` class
- add `CredentialProviderFetcher` to manage `CredentialProvider`
- add `CredentialProviderType` class
- add `JenkinsCredentialProvider` to fetch credentials from Jenkins vault
- add `JenkinsProviderAdapter`
- add `Credential` interface
- add `JenkinsCredentialDataTransformer` to transform Jenkins object to WildBeavers object
- add `SshPrivateKeyCredential` that implement `Credential` for SSH private key
- add `UsernamePasswordCredential` that implement `Credential` for username/password
- (credentials/exposer) add `CredentialExposerType` class
- (credentials/exposer) add `FileCredentialExposer` class to expose a `Secret` by file

## 4.4.1

- fix: (vars/execute) Use the `IOC` class to instantiate `Debugger`

## 4.4.0

- feat: Start adding a new way to expose credentials
- add `Secret` data class to store a secret
- add `CredentialExposer` interface
- add `CredentialExposerEnvironmentVariable` class to expose a `Secret` by environment variable
- add `CredentialExposerFetcher` to manage `CredentialExposer`
- feat: add `EnvironmentVariableHelper` to manage environment variables
- refactor: use `EnvironmentVariableHelper` in:
- `ScmInfoManager`
- `SSHKeySetupHelper`
- chore: optimize all imports

## 4.3.0

- feat (BREAKING): (PipelineEventData) add `preCommitConfigJsonFilePath` attribute
- feat: (TerraformRepositoryNameStandardListener) add `nexus` as valid repository name
- feat: (PreCommitListener) mount the `config.json` to the docker command
- feat: (job) add default `preCommitConfigJsonFilePath` config value to `${HOME}/.docker/config.json`
- test: fix gitlab-ci syntax for `cobertura`

## 4.2.0

- feat: (terraform) add `legacy.tf` and `moved.tf` authorized file for deployment
- chore: bump pre-commit hook version

## 4.1.0

- feat: add `SSHKeySetupHelper` to handle SSH credentials
- fix: tries to find default branch using new `BRANCH_IS_PRIMARY` env var
- fix: secure interpolate credentials for terraform
- fix: fixes mounts for all Terraform commands
- fix: fix script injection security issue with AutoTagger
- chore: updates gradle dependencies

## 4.0.0

- refactor: (BREAKING) prefix Terraform variables with `terraform_`
- feat: (terrafom) add the ability to switch workspace
- refactor: disable `TF_LOG` in debug mode as it begets unreadable display
- refactor: display docker command lines by default
- doc: add basic documentation for Terraform pipeline

## 3.3.2

- fix: (AutoTag) removes the forceful delete of the patch tags

## 3.3.1

- fix: (AutoTag) prevent tag to be empty if the commit is tagged, but incomplete.

## 3.3.0

- feat: adds `currentTags` to `ScmInfo` to get all the tags of a current revision.
- feat: (AutoTag) auto-tag with MAJOR, MINOR and latest commit that only have a single tag.

## 3.2.1

- fix: displays "auto-tag eligibility fails" only when commit is not tagged but could be.
- fix: makes sure `COMMAND_TARGET` is public in `AsbtractCommand` and available to all its childs.

## 3.2.0

- feat: adds a very basic bash pipeline
- feat: adds a `standardUndefined` pipeline to run only the standard steps
- feat: creates an `ArtifactPublisherListener` to publish any kind of archive anywhere, deprecating `ButanePublisherListener`
- feat: allows to toggle the `FileStandardListener`
- refactor: moves butane and Java listeners out of the `standard` namespace
- refactor: re-organize the `io/Archiver` into an Archiver component system to handle many kind of compress/uncompress algorithms
- refactor: makes ExecuteHelper to `printDebug` and error instead of printing it, if error are authorized
- refactor: change the `ArtifactFetcherListener` to use the new Archiver system
- refactor: change the X`FileStandardListener` to a standard step, for standard files

## 3.1.1

- chore: updates pre-commit
- refactor: change `instanciate` to `instantiate`

## 3.1.0

- feat: (Execute) adds a new `ExecuteHelper` class to eventually replace `vars/execute.groovy`
- feat: (AutoTagger) change the message when auto tag cannot be done whild the project is publishable
- feat: (printDebug) removes the printDebug function entirely, in favor of the Debugger
- feat: (Debugger) makes debug marks less visible and grey
- feat: (Debugger) allow the debugger to print a value, to prevent the use of context in addition of the Debugger just to print
- refactor: removes specific `AzureTerraformPipeline`
- doc: improve the base documentation, preparing new categories
- doc: add documentation for auto tagging
- doc: add documentation for artifact fetching
- doc: add documentation for the artifact component

## 3.0.0

- feat: (BREAKING:all) ANY PIPELINE BUT TERRAFORM/BUTANE IS UNTESTED AND PROBABLY BROKEN FOR NOW
- feat: Adds a listener to auto tag any main branch after a merge, following the CHANGELOG
- feat: Adds a `SemverHelper` static class to take care of handling tags
- feat: (all) Add an `ArtifactFetcherListener` to get and potentially uncompress artifacts at the prepare stage of pipelines.
- feat: (butane) Add a full butane pipeline with lint, build and publish listeners.
- feat: (IO) Add a File object to get information or manipulate files.
- feat: (IO) Add an Archiver class to archive/extract files.
- feat: (ArtifactStore) Add an ArtifactStore composite with the first implementation: S3 buckets.
- feat: (mapAttributeCheck) move `mapAttributeCheck` to `WildMap` decorator object
- feat: (dockerRunHelper) if image name is empty, use fallback command instead of crashing
- feat: (dockerRunHelper) allows to run image as priviledge
- feat: (execute) adds a `newReturnStyle` config option to return a `CommandResult` data object instead of a Map
- feat: (BREAKING:foolProofValidation) Moves `foolProofValidation` to its own class `FoolProofValidationHelper`
- feat: (mapAttributeCheck) Deprecates `mapAttributeCheck`
- feat: (scmInfo) throw a meaningful exception when `ScmInfo` is used too early
- feat: (scmInfo) implements a refreshing system to update ScmInfo even in already created object in IOC
- feat: (scmInfo) adds the entire remote name in the data object
- feat: (cleanup) adds listener to launch `cleanupWs()` if it exists
- feat: (DebugInformationDisplayer) adds listener `DebugInformationDisplayer` to display general debug information
- feat: (EventListenerWrapper) adds new interface `CanWrapListener` and a list of these interface in `EventDispatcher` to allow to wrap EventListeners
- feat: (Wrapper) add some basic reusable wrappers for Terraform
- feat: (IOC) adds `debug` method to show what class are registered and what instances were created
- feat: (Gradle) adds support for gradle command line
- feat: (PreCommit) Shows `git diff` (in debug mode) if anything fails to make debugging easier
- feat: (terraform) tries to destroy resources twice, in case first attempt failed after partial destroy
- refactor: (BREAKING:terraform) Gather every terraform pipeline steps to `standardTerraform`, otherwise use listeners
- refactor: (BREAKING:terraform) moves `terraform` step to a class
- refactor: (BREAKING:terraform) register Terraform listeners on `standardTerraform` instead of `registerListener`
- refactor: (BREAKING:terraform) moves inspec tests from `standardTerraform` to a listener `InspecExecListener`
- refactor: (BREAKING:jobinfo) Moves `jobinfo` step to its own class `JobInfo`
- refactor: (BREAKING:fxString) Move `FXString` class to `decorator/WildString`
- refactor: (terraform) Moves publish steps entirely in Observer system
- refactor: (BREAKING:fxjob/job) Splits the pipeline stages to: prepare, lint, build, test, publish, notification, cleanup
- refactor: (BREAKING:fxJob/job) calls closures to each stage to facilitate embedded code
- refactor: (BREAKING) renames everything fx to wildbeavers
- refactor: (BREAKING) removes all deprecated functions
- refactor: (BREAKING) removes all specific pipeline steps to FX
- refactor: (BREAKING:inspec) moves `inspec` step to a class `Inspec`
- refactor: (BREAKING:execute) makes `execute` step throw exception in case of error, instead of calling `error()`
- refactor: (BREAKING:registerListeners) removes `registerListener`
- refactor: (BREAKING:execute) throws `CommandExecutionException` instead of `Exception` when script error occurs
- refactor: (BREAKING:dockerRunHelper) uses a `DockerOptions` data object instead of asking for every single options as arguments
- refactor: (dockerRunHelper) allows to run without using `prepareRunCommand` before
- refactor: (fxJob/job) register basic listeners on `job` instead of `registerListener`
- refactor: (CheckoutListener) improves robustness of methods to gather as much informations as possible
- refactor: (CheckoutListener) splits the checkout (in the listener) with the `ScmInfo` data refresh in a new class `ScmInfoManager`
- test: use spock framework for tests
- test: updates gradle to handle more libs
- test: uses spock as testing framework
- test: adds .gitlab-ci for gitlab automated tests
- test: adds test for listeners
- test: create tests for any new class
- test: adds tests for `DeprecatedFunction`
- test: adds tests for `DeprecatedMessage`
- test: adds tests for `OptionStringFactory`
- test: adds tests for `ServiceRegisterer`
- fix: (dockerRunHelper) in case of fallback, returns fallback command and the arguments
- fix: (Checkout) only tolerate script execution errors
- fix: (OptionStringFactory) properly raise error when option value is wrong
- fix: (HeaderDisplay) fixes header not displaying
- fix: (TerraformFileStandard) Allow `LICENCE`, `data.tf` file in deployment
- chore: removes ACMF as it is handled by gitlab
- doc: documents `DeprecatedFunction.execute` method
- doc: adds a lot of technical documentation under `doc`
- doc: generate groovy-doc within CI/CD
- doc: publish the `doc` and groovydoc in a gitlab pages

## 2.0.1

- fix: (terraform) allows Jenkinsfile in deployments
- fix: (terraform) allows versions.tf in deployments
- fix: (DockerRunnerHelper) makes sure DockerRunnerHelper.run returns its execute results

## 2.0.0

- feat: (DI component) Only add a listener to the listeners bag when it’s not already inside
- feat: (DI component) shows debug information when env.DEBUG=true when calling a listener
- feat: (terraform) adds listeners for init, fmt, validate, plan, apply & destroy rather than using closure
- feat: (terraform) adds FX specific listeners for init, repo naming standard, file naming standard
- refactor: (terraform) changes fxTerraform to standardTerraform, first being FX specific, the other generic
- feat: moves fxCheckout() to a listener of INIT event to run it as soon as possible
- feat: (fxCheckout) makes better effort to try to find default branch
- feat: moves fxCheckoutTag() in the same listener than fxCheckout
- feat: (fxJob) moves docker login in its own listener
- feat: (fxJob) moves header display in its own listener
- feat: (fxJob) moves pre-commit in its own listener
- feat: (fxDockerImage) move from private pipeline to public
- feat: (fxDockerImage) create standardDockerImage for standardPipeline
- refactor: (runDockerCommand) Adds class to run docker command and deprecates dockerRunCommand
- refactor: (inspec) Rewrite function: allow --reporter option, use OptionStringFactory and DockerRunnerHelper

## 1.4.2

- fix: (scmInfo) makes sure the correct repository name is returned

## 1.4.1

- fix: (dockerRunCommand) wrong key value delimiter for environment

## 1.4.0

- feat: (dockerRunCommand) build arguments with optionStringFactory instead of manual
- fix: fixes error on all build due to the access to environment variable

## 1.3.0

- feat: (fxJob) allows to run fxJob locally
- feat: (fxJob) auto-set launchLocally to true if env var JENKINS_LOCAL exists
- feat: (fxJob) auto-set dockerDataBasepath to true if env var JENKINS_DOCKER_DATA_BASEPATH exists
- feat: (fxJob) disables cleanWs when run locally
- feat: (dockerRunCommand) allows to change docker run data basepath
- feat: (dockerRunCommand) allows to run docker commands as deamon
- feat: (dockerRunCommand) allows to add name and entrypoint params
- feat: (fxCheckout) fall back to `git branch` if BRANCH_NAME env var is not set

## 1.2.3

- fix: fxJob properties (permit override)
- chore: bump pre-commit hooks

## 1.2.2

- fix: regex for terraform deployment filenames

## 1.2.1

- fix: Resolves file name checking on terraform deployments

## 1.2.0

- feat: add cache volume on slaves

## 1.1.0

- feat: Add pod annotation on slaves

## 1.0.2

- fix: Some other fixes

## 1.0.1

- fix: Typo's to be fixed

## 1.0.0

- feat: (BREAKING) Add dockerhub login in fxJob()

## 0.30.0

- chore: Run pre-commit on the whole repository
- chore: Update pre-commit config
- feat: Allow terraform deployment to have multiple data files

## 0.29.1

- fix: Regex for TF deployment repo name

## 0.29.0

- feat: Allow for non empty plan on outputs

## 0.28.0

- feat: Add standardAwsNukeWithCredentials
- feat: Add standardAwsNukeWithAssumeRole

## 0.27.0

- feat: Add `data.tf` to autorized files on fxTerraform deployments
- feat: Add `versions.tf` to autorized files on fxTerraform deployments

## 0.26.0

- feat: Add `awsNuke` function
- feat: Add `pipelineAwsNuke` function

NOTE: To use `awsNuke` function, you need to install [MaskPassword pluggin](https://plugins.jenkins.io/mask-passwords)

## 0.25.3

- feat: Update the credentials for Azureterraform test pipeline.
- fxAzureTerraformPipeline:
- feat: Update the test credentials `fxazure-terraformtests-service-principal` to `fxazure-terraformtests-12K-service-principal` which will use fx 12K subscription.

## 0.25.2

- fix(terraform): Prevent repo names ending with - and allow first element of the name to be 2 digits min, not 3

## 0.25.1

- fix: Add `resgisterservice` to `fxCheckout` function
- fix: Bug in `pipelineCookbook` function

## 0.25.0

- fix: Review pod architecture to start a new container
- fix: Do not disply docker network when bridge
- feat: Add a waiting kind to be ready
- tech: pin fxinnovation/kind docker image to 0.2.0
- feat: Add an initPod to increase inotify max user watches to 524288 ([see this](https://github.com/kubernetes/test-infra/pull/13515))

## 0.24.0

- feat: Add a Dependency Injection component (`IOC` class)
- feat: Add gradle configuration file for library loading
- feat: Add first implementation of unit testing

## 0.23.1

- hotfix: Wrong default join on groovy Map

## 0.23.0

feat: Add possibility to run kind (Kubernetes IN Docker) on terraform pipeline. This allow use to test kubernetes generic module directly on a local test cluster.

- fxJob:
- feat: Add `podVolumes` option

- fxTerraform:
- feat: Add `runKind` option.

- terraform:
- feat: Add `dockerNetwork` option
- fix: missing global configuration check on `plan` and `apply` functions

- dockerRunCommand:
- feat: Add `network` option

## 0.22.1

- pipelinePlaybook: fix call to addOnlyIfNotExist -> addClosureOnlyIfNotDefined

## 0.22.0

- feat: Add `DeprecatedMessage` and `DeprecatedFunction` classes to manage library deprecation
- feat: Add `fxAzureTerraformPipeline` for specific tests on FX azure account
- feat: Add `standardAzureTerraformPipeline` for generic use
- Breaking : `fxAzureTerraform` is now deprecated and will be deleted in the future

## 0.21.0

- feat: Add classes to set Observer pattern (to replace closure system in the future)

## 0.20.1

- Fix: Wrong `pipelineTerraform.fmt` signature when called in `fxTerraform` method.

## 0.20.0

- Tech: refactor pipeline to use closureHelper when it's possible (issue ##36)

- ClosureHelper:
- Feat: Add method addClosure and addClosureOnlyIfNotDefined

- fxJob:
- Add possibility to change pod slave docker image (attribute `podImageName`) and version (attribute `podImageVersion`)

## 0.16.0

- fxJob:
- Feat: Add pod options: podCloud, podName, podNamespace and podNodeUsageMode
- Breaking: Rename \*notify closures to \*notification
- Breaking: Notification closure must now accept a String input argument
- Feat: Add two new closures, prePipeline and postPipeline
- Feat: Add new headerMessage variable to overwrite “TEDI announcement”

- fxTerraformWithUsernamePassword:
- Feat: Add possibility to add closures

- fxTerraform:
- Feat: Add possibility to add closures (except postNotificatio and pipeline)

- Release:
- fxInspecDockerImage
