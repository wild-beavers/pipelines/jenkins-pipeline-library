# Generic Jenkins Pipeline Library

Full documentation here: [https://wild-beavers.gitlab.io/pipelines/jenkins-pipeline-library/](https://wild-beavers.gitlab.io/pipelines/jenkins-pipeline-library/)

DevOps/SRE people rewrite their pipelines again and again.

This project attempts to provide generic pipelines for many technologies.
Thus, removing the need to redo the same kind of pipeline for many different cases and teams.

## Caveat

This project is still in development.

Input data validations is a work in progress, so make sure you don’t allow free access to user configuration to un-trustable sources.
This project was intended to be used with a sane reviews system before landing on a production system.

## Requirements

- Groovy 2.5+
- Allowed signatures (note: none of these cause a security threat):

```
new java.util.LinkedHashMap java.util.Map
new java.net.URI java.lang.String
method java.net.URI getScheme
method java.net.URI getPath
```

## Run tests on linux

Testing needs gradle, 8+.

```bash
gradle check -i
```

With podman or docker:

```bash
mkdir ~/.cache/gradle
podman run --rm -v $HOME/.cache/gradle:/home/gradle/.gradle -w /data -v $(pwd):/data docker.io/gradle:8-jdk17 gradle check -i
```
