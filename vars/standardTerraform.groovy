import com.wildbeavers.di.IOC
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.service.ServiceRegisterer
import com.wildbeavers.terraform.listener.*

def call(Map config = [:], String mainFunction = 'job') {
  ServiceRegisterer.init(this)
  registerListeners()

  config.headerMessage = (config.headerMessage ? config.headerMessage : '') + '''

      █▄_
      █████▄_
      ████████ ▄_             _▄
      ████████ ████▄       ▄████
      ▀▀██████ ███████▄ ▄███████
          ▀███ ████████ ████████
               ████████ ████████
               █▄`▀████ ████▀
               █████▄▀▀ ▀▀
               ████████
               ████████
               ▀███████
                  ▀▀███
                      ▀ TERRAFORM JOB
'''

  this."${mainFunction}"(config)
}

private static void registerListeners() {
  EventDispatcher eventDispatcher = IOC.get(EventDispatcher.class.getName()) as EventDispatcher

  eventDispatcher.attachClasses([
    TerraformDataValidationListener,

    TerraformDiscoverCommandTargetsListener,
    TerraformFmtListener,
    TerraformValidateListener,

    TerraformInitListener,

    TerraformApplyListener,
    TerraformWorkspaceSelectListener,

    TerraformArtifactCleanerListener,
    TerraformDestroyListener,
  ])
}
