import com.wildbeavers.container.listener.*
import com.wildbeavers.di.IOC
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.service.ServiceRegisterer

def call(Map config = [:], String mainFunction = 'job') {
  ServiceRegisterer.init(this)
  registerListeners()

  config.headerMessage = (config.headerMessage ? config.headerMessage : '') + '''
      ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒
      ▒████████████████████████████▒
      ▒███▒                    ▓███▒
      ▒███▒  ▓▓▓▓▓▓▓▓▓▓  ▓▓▓▓  ▓███▒
      ▒███▒  ▓█████████  ▓███  ▓███▒
      ▒███▒  ▓█████████        ▓███▒
      ▒███▒  ▓███              ▓███▒
      ▒███▒  ▓███        ▓███  ▓███▒
      ▒███▒  ▓███        ▓███  ▓███▒
      ▒███▒  ▓█████████  ▓███  ▓███▒
      ▒███▒  ▓▓▓▓▓▓▓▓▓▓  ▓▓▓▓  ▓███▒
      ▒███▒                    ▓███▒
      ▒████████████████████████████▒
      ▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒ Container Job

'''

  this."${mainFunction}"(config)
}

private static void registerListeners() {
  EventDispatcher eventDispatcher = IOC.get(EventDispatcher.class.getName()) as EventDispatcher

  eventDispatcher.attachClasses([
    ContainerDataValidationListener,
    ContainerAliasGuesserListener,
    ContainerBuildListener,
    ContainerTagListener,
    ContainerInspectListener,
    ContainerPushListener,
  ])
}
