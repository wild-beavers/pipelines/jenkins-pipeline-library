import com.wildbeavers.data.DependencyInstallerData
import com.wildbeavers.data.PipelineStatus
import com.wildbeavers.data.file_standard.FileStandardData
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.dependency_installer.DependencyInstallerDataManagerFetcher
import com.wildbeavers.dependency_installer.DependencyInstallerType
import com.wildbeavers.di.IOC
import com.wildbeavers.event.ControllerEvents
import com.wildbeavers.event.RunnerEvents
import com.wildbeavers.event_data.ControllerEventData
import com.wildbeavers.event_data.RunnerEventData
import com.wildbeavers.helper.ClosureHelper
import com.wildbeavers.io.Debugger
import com.wildbeavers.listener.*
import com.wildbeavers.observer.EventDataInterface
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.service.ServiceRegisterer

def call(Map config = [:] as WildMap, Map<CharSequence, Closure> closures = null, EventDataInterface eventData = null) {
  ServiceRegisterer.init(this)
  registerListeners()

  this.ansiColorJob {
    if (eventData != null) {
      print(
        "\u001b[35m /!\\ DEPRECATION WARNING /!\\\n" +
          "\033[0;4m\033[0;1m\u001b[35m“eventData” was passed to `job` and it will soon be ignored.\u001b[0m\u001b[35m" +
          "\033[0;4m\033[0;1m\u001b[35mInstead, data object must be instantiated within a “ControllerEvents.DATA_VALIDATION” listener.\u001b[0m\u001b[35mx<" +
          "\u001B[0m"
      )
    }
    if (closures != null) {
      print(
        "\u001b[35m /!\\ DEPRECATION WARNING /!\\\n" +
          "\033[0;4m\033[0;1m\u001b[35m“closures” were passed to `job` and it will soon be ignored.\u001b[0m\u001b[35m" +
          "\033[0;4m\033[0;1m\u001b[35mInstead, use EventListener to execute all your code.\u001b[0m\u001b[35m" +
          "\u001B[0m"
      )
    }

    Debugger debugger = IOC.get(Debugger.getName()) as Debugger
    EventDispatcher eventDispatcher = IOC.get(EventDispatcher.getName()) as EventDispatcher

    config = new WildMap(config)
    ControllerEventData controllerEventData = new ControllerEventData()
    controllerEventData.setRawConfig(config)
    controllerEventData.setRunnerEventData(new RunnerEventData(IOC.get(FileStandardData.getName() + 'Factory').instantiate()))

    controllerEventData = eventDispatcher.dispatch(ControllerEvents.PRE_DATA_VALIDATION, controllerEventData) as ControllerEventData
    controllerEventData = eventDispatcher.dispatch(ControllerEvents.DATA_VALIDATION, controllerEventData) as ControllerEventData
    controllerEventData = eventDispatcher.dispatch(ControllerEvents.POST_DATA_VALIDATION, controllerEventData) as ControllerEventData

    //
    // Validation below should be in event listeners for the events thrown above
    // See PreCommitDataValidationListener for example.
    //
    config.check('testsCancellerEnabled', Boolean, true)

    config.check('artifactsToFetch', List, [])

    config.check('fileStandard_enabled', Boolean, true)

    config.check('headerMessage', CharSequence, '''
   -> WildBeavers Pipeline v10 - https://wild-beavers.gitlab.io/pipelines/jenkins-pipeline-library/
  ''')
    config.check('listenerWrappers', List, [])
    config.check('dependencyInstaller', Map, [:])

    //
    // Validation above should be in event listeners for the events thrown above
    //

    eventDispatcher.attachWrappers(config.listenerWrappers)

    controllerEventData = eventDispatcher.dispatch(ControllerEvents.PRE_RUNNER, controllerEventData) as ControllerEventData

    node(controllerEventData.getJobNodeLabel()) {
      this.timoutPipeline({
        this.pipeline(config, closures, controllerEventData, eventData)
      }, controllerEventData.getJobTimeoutTime(), controllerEventData.getJobTimeoutUnit())
    }
  }
}


private void timoutPipeline(Closure callback, timeoutTime = null, timeoutUnit = null) {
  if (this.isPluginInstalled('build-timeout')) {
    timeout(
      time: timeoutTime,
      unit: timeoutUnit
    ) {
      callback()
    }
  } else {
    callback()
  }
}

private def pipeline(Map<CharSequence, Object> config, Map<CharSequence, Closure> closures, ControllerEventData controllerEventData, EventDataInterface mainEventData) {
  EventDispatcher<RunnerEventData> eventDispatcher = IOC.get(EventDispatcher)

  RunnerEventData runnerEventData = controllerEventData.getRunnerEventData()

  // This dependency installer initialization should not be here and be treated in a proper listener
  Map<DependencyInstallerType, List<DependencyInstallerData>> dependencyInstallerData = [:]
  config.dependencyInstaller.each { String type, Map<String, Map<String, Object>> value ->
    List<DependencyInstallerData> typeList = []

    value.each {
      typeList.add(IOC.get(DependencyInstallerDataManagerFetcher.getName()).fetch(type as DependencyInstallerType).create(it.value))
    }

    dependencyInstallerData.put(type as DependencyInstallerType, typeList)
  }
  // End - This dependency installer initialization should not be here and be treated in a proper listener

  // All assignations below should be in DATA_VALIDATION listeners.
  if (mainEventData) {
    runnerEventData.setMainEventData(mainEventData)
  }
  runnerEventData.setHeaderMessage(config.headerMessage)
  runnerEventData.setDockerDataBasepath(config.dockerDataBasepath)
  runnerEventData.setDockerDataIsCurrentDirectory(config.dockerDataIsCurrentDirectory)
  runnerEventData.setFileStandardEnabled(config.fileStandard_enabled)
  runnerEventData.setTestsCancellerEnabled(config.testsCancellerEnabled)
  runnerEventData.setDependencyInstallerData(dependencyInstallerData)
  // End - All assignations above should be in DATA_VALIDATION listeners.

  // This below is legacy. Every closures should be migrated to listeners.
  // Once done, this can be removed.
  ClosureHelper closureHelper = new ClosureHelper(this, IOC.get(Debugger), closures)

  try {
    stage("Prepare") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_PREPARE, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PREPARE, runnerEventData)
      // This below is legacy. Every closures should be migrated to listeners.
      // Once done, this can be removed.
      closureHelper.execute(RunnerEvents.PREPARE)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_PREPARE, runnerEventData)
    }

    stage("Lint") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_LINT, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.LINT, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_LINT, runnerEventData)
    }

    stage("Static Analysis") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_STATIC_ANALYSIS, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.STATIC_ANALYSIS, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_STATIC_ANALYSIS, runnerEventData)
    }

    stage("Compile") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_COMPILE, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.COMPILE, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_COMPILE, runnerEventData)
    }

    stage("Unit Tests") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_TEST_UNIT, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.TEST_UNIT, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_TEST_UNIT, runnerEventData)
    }

    stage("Func. Tests") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_TEST_FUNCTIONAL, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.TEST_FUNCTIONAL, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_TEST_FUNCTIONAL, runnerEventData)
    }

    stage("Build") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_BUILD, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.BUILD, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_BUILD, runnerEventData)
    }

    stage("Deploy") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_DEPLOY, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.DEPLOY, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_DEPLOY, runnerEventData)
    }

    stage("Intgr. Tests") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_TEST_INTEGRATION, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.TEST_INTEGRATION, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_TEST_INTEGRATION, runnerEventData)
    }

    stage("Publish") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_PUBLISH, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PUBLISH, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_PUBLISH, runnerEventData)
    }

    stage("Release") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_RELEASE, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.RELEASE, runnerEventData)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.POST_RELEASE, runnerEventData)
    }

    runnerEventData.setStatus(PipelineStatus.SUCCESSFUL)
  } catch (errorMessage) {
    runnerEventData.setStatus(PipelineStatus.FAILED)
    println(errorMessage.getMessage())
    errorMessage.getSuppressed().each {
      println('----------')
      println(it.getMessage())
    }
    error('')
  } finally {
    stage("Cleanup") {
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.PRE_CLEANUP, runnerEventData, false)
      runnerEventData = eventDispatcher.dispatch(RunnerEvents.CLEANUP, runnerEventData, false)
      eventDispatcher.dispatch(RunnerEvents.POST_CLEANUP, runnerEventData, false)
    }
  }

  eventDispatcher.dispatch(ControllerEvents.POST_RUNNER, controllerEventData)
}

private void ansiColorJob(Closure callback) {
  if (this.isPluginInstalled('ansicolor')) {
    ansiColor('xterm') {
      callback()
    }
  } else {
    callback()
  }
}

private Boolean isPluginInstalled(String pluginName) {
  for (plugin in jenkins.model.Jenkins.getInstanceOrNull().getPluginManager().getPlugins()) {
    if (plugin.toString() == "Plugin:${pluginName}".toString()) {
      return true
    }
  }

  return false
}

private static void registerListeners() {
  EventDispatcher eventDispatcher = IOC.get(EventDispatcher.class.getName()) as EventDispatcher

  eventDispatcher.attachClasses([
    ArtifactFetcherDataValidationListener,
    ArtifactPublisherDataValidationListener,
    CleanupDelayDataValidationListener,
    ContainerLoginDataValidationListener,
    DebugInformationDisplayerListener,
    FileStandardDataValidationListener,
    JobDataValidationListener,
    LinterDataValidationListener,
    PreCommitDataValidationListener,
    ScmDataValidationListener,

    ArtifactFetcherListener,
    AutoTaggerListener,
    CleanupDelayListener,
    ContainerLoginListener,
    DependencyInstallerListener,
    FileStandardListener,
    HeaderDisplayerListener,
    JobPropertiesListener,
    JobRuntimeDebuggingListener,
    LinterListener,
    PreCommitListener,
    ScmCheckoutListener,
    ScmInfoRefresherListener,
    TagCheckerListener,
    TagCheckerValidationListener,
    TestsCancellerListener,

    ArtifactPublisherListener,
    WorkspaceCleanupListener,
  ])
}
