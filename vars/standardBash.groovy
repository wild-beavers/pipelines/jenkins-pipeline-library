import com.wildbeavers.bash.listener.BashDataValidationListener
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.di.IOC
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.service.ServiceRegisterer

def call(Map config = [:] as WildMap, String mainFunction = 'job') {
  ServiceRegisterer.init(this)
  registerListeners()

  config.headerMessage = (config.headerMessage ? config.headerMessage : '') + '''

                 ,≡≡_
             _─`      ªw_
          æ"              "╖
         ∩                _▄█
        j              ▄█████
        j          ,▄████████
        j         j██▌▀██████
        j         j█▌╙▀██████
         ¥_       j██▀.█▀▄███
           `≈,    j██████▀
               "≡_ ██▀▀   BASH JOB
'''

  this."${mainFunction}"(config)
}

private void registerListeners() {
  EventDispatcher eventDispatcher = IOC.get(EventDispatcher.getName())

  eventDispatcher.attachClasses([
    BashDataValidationListener,
  ])
}
