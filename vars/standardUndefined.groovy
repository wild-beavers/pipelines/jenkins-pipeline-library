import com.wildbeavers.service.ServiceRegisterer

def call(Map config = [:], String mainFunction = 'job') {
  ServiceRegisterer.init(this)

  this."${mainFunction}"(config)
}
