import com.wildbeavers.container.data.ContainerOptions
import com.wildbeavers.python.data.VenvDependencyInstallerData
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.python.dependency_installer.Venv
import com.wildbeavers.di.IOC
import com.wildbeavers.python.data.PythonData
import com.wildbeavers.python.listener.PythonBuilderListener
import com.wildbeavers.python.listener.PythonTestsListener
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.python.PythonBuildDataManagerFetcher
import com.wildbeavers.python.PythonTestDataManagerFetcher
import com.wildbeavers.python.builder.PythonBuilderType
import com.wildbeavers.python.data.HasPythonBuilderData
import com.wildbeavers.python.tests.PythonTestsType
import com.wildbeavers.service.ServiceRegisterer

def call(Map config = [:] as WildMap, String mainFunction = 'job', Map mainConfig = [:] as WildMap) {
  final String VENV_CONFIGURATION_NAME = 'VENV'

  ServiceRegisterer.init(this)

  mainConfig.headerMessage = (mainConfig.headerMessage ? mainConfig.headerMessage : '') + '''

            _▄██████████▄
            █▌  ██████████
           j██████████████
      _╓▄▄▄▄▄▄▄▄▄▄▄███████ ▄▄▄▄_
     █████████████████████ ██████
    ▐█████████████████████.██████_
    ██████████▀▀▀▀▀▀▀▀▀▀_▄███████▌
    ███████▀╓████████████████████▌
     ██████ █████████████████████
     `█████j████████████████████
           j███████▄▄▄▄▄▄▄
           j██████████  ██
            ▀█████████▄▄█▀
               `▀▀▀▀▀▀    PYTHON JOB
'''

  this.registerListeners(IOC.get(EventDispatcher.class.getName()))

  config.check('python_builderType', String, PythonBuilderType.PYTHON_BUILD as String)
  config.check('python_builderConfiguration', WildMap, [:] as WildMap)
  config.check('python_builderDependencyType', CharSequence, '')
  config.check('python_builderDependencyName', CharSequence, '')

  config.python_testsConfigurations = config.python_testsConfigurations as WildMap
  config.check('python_testsConfigurations', WildMap, [:] as WildMap)

  config.python_testsDependencyTypes = config.python_testsDependencyTypes as WildMap
  config.check('python_testsDependencyTypes', WildMap, [:] as WildMap)

  config.python_testsDependencyNames = config.python_testsDependencyNames as WildMap
  config.check('python_testsDependencyNames', WildMap, [:] as WildMap)


  // Python build
  PythonData pythonData = IOC.get(PythonData.getName())
  pythonData.setPythonBuilderType(config.python_builderType as PythonBuilderType)

  HasPythonBuilderData hasPythonBuilderData = IOC.get(PythonBuildDataManagerFetcher.getName()).fetch(pythonData.getPythonBuilderType()).create(config.python_builderConfiguration)
  pythonData.setPythonBuilderData(hasPythonBuilderData)

  // Python tests
  config.python_testsConfigurations.each {WildMap<String, WildMap> it ->
    if (it.value.get('type', 'undefined') == 'undefined') {
      throw Exception('A “type” key must be defined for each “python_testsConfigurations”. Given on “' + it.name + '”: “' + it.value + '”')
    }

    PythonTestsType type = it.value.get('type') as PythonTestsType
    it.value.put('name', it.key)
    it.value.remove('type')

    def pythonTestsData = IOC.get(PythonTestDataManagerFetcher.class.getName()).fetch(type).create(it.value)
    pythonData.addPythonTestsData(pythonTestsData)
  }


  Map<CharSequence, Closure> closures = [
    'prepare': {
      if (config.python_builderDependencyType == VENV_CONFIGURATION_NAME) {
        activateVenv(config.python_builderDependencyName, mainConfig.dependencyInstaller.get(VENV_CONFIGURATION_NAME).get(config.python_builderDependencyName), pythonData.getPythonBuilderData().getContainerOptions())
      }
      config.python_testsDependencyNames.each {
        if (config.python_testsDependencyTypes.get(it.key) == VENV_CONFIGURATION_NAME) {
          activateVenv(it.value, mainConfig.dependencyInstaller.get(VENV_CONFIGURATION_NAME).get(it.value), pythonData.getPythonTestsDataByName(it.key).getContainerOptions())
        }
      }
    }
  ]

  this."${mainFunction}"(mainConfig, closures, pythonData)
}

private void registerListeners(EventDispatcher eventDispatcher) {
  eventDispatcher.attach(IOC.get(PythonBuilderListener.class.getName()))
  eventDispatcher.attach(IOC.get(PythonTestsListener.class.getName()))
}

private void activateVenv(String venvName, Map venvDependencyInstallerConfig, ContainerOptions dockerOptions) {
  if (venvName == '') {
    return
  }

  Venv.activateVenvInDockerOptions(
    dockerOptions,
    venvDependencyInstallerConfig.get('sourceLocalFolderPath', VenvDependencyInstallerData.SOURCE_LOCAL_FOLDER_PATH),
    venvDependencyInstallerConfig.get('destinationDockerFolderPath', VenvDependencyInstallerData.DESTINATION_DOCKER_FOLDER_PATH),
    venvDependencyInstallerConfig.get('pathEnvironmentVariable', '/bin')
  )
}
