import com.wildbeavers.butane.listener.ButaneDataValidationListener
import com.wildbeavers.butane.listener.ButaneToIgnitionListener
import com.wildbeavers.decorator.WildMap
import com.wildbeavers.di.IOC
import com.wildbeavers.observer.EventDispatcher
import com.wildbeavers.service.ServiceRegisterer

def call(Map config = [:] as WildMap, String mainFunction = 'job') {
  ServiceRegisterer.init(this)
  registerListeners()

  config.headerMessage = (config.headerMessage ? config.headerMessage : '') + '''

             _▄████▀███▄_
          ╓█████▀       `▀█▄
        .██████▌           ▀█_
       ┌███████   ███▄_      █▌
       ███████▌  ███████      █
      ▐███████▌  ███████      █▌
      j███████▌  `▀▀         ▄█▌
       ████████▄▄▄▄▄▄▄▄▄▄██████
        ██████████████████████
         ▀██████████████████▀
            ▀████████████▀▀
                 ▀▀▀▀     BUTANE/IGNITION JOB
'''

  this."${mainFunction}"(config)
}

private void registerListeners() {
  EventDispatcher eventDispatcher = IOC.get(EventDispatcher.class.getName()) as EventDispatcher

  eventDispatcher.attachClasses([
    ButaneDataValidationListener,

    ButaneToIgnitionListener,
  ])
}
